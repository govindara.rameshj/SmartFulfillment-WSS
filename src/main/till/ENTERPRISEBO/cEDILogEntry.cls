VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEDILogEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E935352036F"
'<CAMH>****************************************************************************************
'* Module : cEDILogEntry
'* Date   : 08/11/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cEDILogEntry.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 6/05/04 15:36 $ $Revision: 2 $
'* Versions:
'* 26/02/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cEDIFileControl"

Implements IBo
Implements ISysBo

'##ModelId=3E93548903D1
Private mFileCode As String

'##ModelId=3E93549400BF
Private mSequenceNumber As String

'##ModelId=3E9355270229
Private mSourceSequenceNumber As String

'##ModelId=3E9355400257
Private mDateCreated As Date

'##ModelId=3E93558B02B9
Private mTransmissionDate As Date

'##ModelId=3E9355940302
Private mTransmissionTime As String

'##ModelId=3E93559D02BF
Private mSentRecordCount As Long

'##ModelId=3E9355B400FF
Private mActualRecordCount As Long

'##ModelId=3E9356130016
Private mEndToEnd As Boolean

'##ModelId=3E9356200336
Private mReadyToProcess As Boolean

'##ModelId=3E9356340077
Private mDateReady As Date

'##ModelId=3E9356C40074
Private mTimeReady As String

'##ModelId=3E9356CC0315
Private mProcessed As Boolean

'##ModelId=3E9356D3004E
Private mDateProcessed As Date

'##ModelId=3E9356D901BF
Private mTimeProcessed As String

'##ModelId=3E9356E10080
Private mErrorCode As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3FAC252701AA
Public Property Get ErrorCode() As String
   Let ErrorCode = mErrorCode
End Property

'##ModelId=3FAC252700EC
Public Property Let ErrorCode(ByVal Value As String)
    Let mErrorCode = Value
End Property

'##ModelId=3FAC25270087
Public Property Get TimeProcessed() As String
   Let TimeProcessed = mTimeProcessed
End Property

'##ModelId=3FAC252603C5
Public Property Let TimeProcessed(ByVal Value As String)
    Let mTimeProcessed = Value
End Property

'##ModelId=3FAC25260361
Public Property Get DateProcessed() As Date
   Let DateProcessed = mDateProcessed
End Property

'##ModelId=3FAC252602B7
Public Property Let DateProcessed(ByVal Value As Date)
    Let mDateProcessed = Value
End Property

'##ModelId=3FAC2526025D
Public Property Get Processed() As Boolean
   Let Processed = mProcessed
End Property

'##ModelId=3FAC252601BC
Public Property Let Processed(ByVal Value As Boolean)
    Let mProcessed = Value
End Property

'##ModelId=3FAC25260158
Public Property Get TimeReady() As String
   Let TimeReady = mTimeReady
End Property

'##ModelId=3FAC252600C2
Public Property Let TimeReady(ByVal Value As String)
    Let mTimeReady = Value
End Property

'##ModelId=3FAC25260068
Public Property Get DateReady() As Date
   Let DateReady = mDateReady
End Property

'##ModelId=3FAC252503B0
Public Property Let DateReady(ByVal Value As Date)
    Let mDateReady = Value
End Property

'##ModelId=3FAC25250356
Public Property Get ReadyToProcess() As Boolean
   Let ReadyToProcess = mReadyToProcess
End Property

'##ModelId=3FAC252502BF
Public Property Let ReadyToProcess(ByVal Value As Boolean)
    Let mReadyToProcess = Value
End Property

'##ModelId=3FAC2525026F
Public Property Get EndToEnd() As Boolean
   Let EndToEnd = mEndToEnd
End Property

'##ModelId=3FAC252501D9
Public Property Let EndToEnd(ByVal Value As Boolean)
    Let mEndToEnd = Value
End Property

'##ModelId=3FAC25250189
Public Property Get ActualRecordCount() As Long
   Let ActualRecordCount = mActualRecordCount
End Property

'##ModelId=3FAC252500FD
Public Property Let ActualRecordCount(ByVal Value As Long)
    Let mActualRecordCount = Value
End Property

'##ModelId=3FAC252500A3
Public Property Get SentRecordCount() As Long
   Let SentRecordCount = mSentRecordCount
End Property

'##ModelId=3FAC25250020
Public Property Let SentRecordCount(ByVal Value As Long)
    Let mSentRecordCount = Value
End Property

'##ModelId=3FAC252403B8
Public Property Get TransmissionTime() As String
   Let TransmissionTime = mTransmissionTime
End Property

'##ModelId=3FAC2524032C
Public Property Let TransmissionTime(ByVal Value As String)
    Let mTransmissionTime = Value
End Property

'##ModelId=3FAC252402DC
Public Property Get TransmissionDate() As Date
   Let TransmissionDate = mTransmissionDate
End Property

'##ModelId=3FAC2524025A
Public Property Let TransmissionDate(ByVal Value As Date)
    Let mTransmissionDate = Value
End Property

'##ModelId=3FAC25240214
Public Property Get DateCreated() As Date
   Let DateCreated = mDateCreated
End Property

'##ModelId=3FAC25240192
Public Property Let DateCreated(ByVal Value As Date)
    Let mDateCreated = Value
End Property

'##ModelId=3FAC2524014B
Public Property Get SourceSequenceNumber() As String
   Let SourceSequenceNumber = mSourceSequenceNumber
End Property

'##ModelId=3FAC252400D3
Public Property Let SourceSequenceNumber(ByVal Value As String)
    Let mSourceSequenceNumber = Value
End Property

'##ModelId=3FAC2524008D
Public Property Get SequenceNumber() As String
   Let SequenceNumber = mSequenceNumber
End Property

'##ModelId=3FAC25240015
Public Property Let SequenceNumber(ByVal Value As String)
    Let mSequenceNumber = Value
End Property

'##ModelId=3FAC252303B7
Public Property Get FileCode() As String
   Let FileCode = mFileCode
End Property


'##ModelId=3FAC2523033F
Public Property Let FileCode(ByVal Value As String)
    Let mFileCode = Value
End Property

Friend Sub RecordAsProcessed(strFilename As String, strSequenceNo As String, lngNoRecords As Long)

Dim oRow   As IRow

    Call IBo_ClearLoadField
    Call IBo_ClearLoadFilter
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_TCFMASTER_FileCode, strFilename)
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_TCFMASTER_SequenceNumber, strSequenceNo)
    
    Call IBo_LoadMatches
    
    'Build up saving Row to update Done in TCFCTL table
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    mActualRecordCount = lngNoRecords
    mDateProcessed = Now
    mTimeProcessed = Format$(Now(), "HHNNSS")
    mProcessed = True
    Call DebugMsg(MODULE_NAME, "RecordAsProcessed", endlDebug, "Updating TCF Mas (" & mFileCode & strSequenceNo & ")")
    If Val(mSequenceNumber) < Val(strSequenceNo) Then mSequenceNumber = Format$(Val(strSequenceNo), "0000")
    Call oRow.Add(GetField(FID_TCFMASTER_FileCode))
    Call oRow.Add(GetField(FID_TCFMASTER_SequenceNumber))
    Call oRow.Add(GetField(FID_TCFMASTER_ActualRecordCount))
    Call oRow.Add(GetField(FID_TCFMASTER_DateProcessed))
    Call oRow.Add(GetField(FID_TCFMASTER_TimeProcessed))
    Call oRow.Add(GetField(FID_TCFMASTER_Processed))
    
    Call m_oSession.Database.SavePartialBo(Me, oRow)
       
End Sub

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim oTCFCTL As cEDIFileControl
    
    Save = False
    
    If IsValid() Then
        If LenB(mSourceSequenceNumber) = 0 Then mSourceSequenceNumber = "000000"
        If eSave = SaveTypeIfNew Then
            mDateCreated = Now()
            mTimeProcessed = "000000"
            
            Set oTCFCTL = m_oSession.Database.CreateBusinessObject(CLASSID_EDIFILECONTROL)
            'ensure that Last reflects new master entry being saved
            Call oTCFCTL.UpdateLast(mFileCode, mSequenceNumber)
            Set oTCFCTL = Nothing
        End If
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_TCFMASTER * &H10000) + 1 To FID_TCFMASTER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cEDILogEntry

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_TCFMASTER, FID_TCFMASTER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_TCFMASTER_FileCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFileCode
        Case (FID_TCFMASTER_SequenceNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSequenceNumber
        Case (FID_TCFMASTER_SourceSequenceNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSourceSequenceNumber
        Case (FID_TCFMASTER_DateCreated):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateCreated
        Case (FID_TCFMASTER_TransmissionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTransmissionDate
        Case (FID_TCFMASTER_TransmissionTime):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransmissionTime
        Case (FID_TCFMASTER_SentRecordCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSentRecordCount
        Case (FID_TCFMASTER_ActualRecordCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mActualRecordCount
        Case (FID_TCFMASTER_EndToEnd):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mEndToEnd
        Case (FID_TCFMASTER_ReadyToProcess):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mReadyToProcess
        Case (FID_TCFMASTER_DateReady):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateReady
        Case (FID_TCFMASTER_TimeReady):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTimeReady
        Case (FID_TCFMASTER_Processed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mProcessed
        Case (FID_TCFMASTER_DateProcessed):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateProcessed
        Case (FID_TCFMASTER_TimeProcessed):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTimeProcessed
        Case (FID_TCFMASTER_ErrorCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mErrorCode
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cEDILogEntry
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_TCFMASTER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "TCF Master " & mFileCode & " " & mSequenceNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_TCFMASTER_FileCode):          FileCode = oField.ValueAsVariant
            Case (FID_TCFMASTER_SequenceNumber):    SequenceNumber = oField.ValueAsVariant
            Case (FID_TCFMASTER_SourceSequenceNumber): SourceSequenceNumber = oField.ValueAsVariant
            Case (FID_TCFMASTER_DateCreated):       DateCreated = oField.ValueAsVariant
            Case (FID_TCFMASTER_TransmissionDate):  TransmissionDate = oField.ValueAsVariant
            Case (FID_TCFMASTER_TransmissionTime):  TransmissionTime = oField.ValueAsVariant
            Case (FID_TCFMASTER_SentRecordCount):   SentRecordCount = oField.ValueAsVariant
            Case (FID_TCFMASTER_ActualRecordCount): ActualRecordCount = oField.ValueAsVariant
            Case (FID_TCFMASTER_EndToEnd):          EndToEnd = oField.ValueAsVariant
            Case (FID_TCFMASTER_ReadyToProcess):    ReadyToProcess = oField.ValueAsVariant
            Case (FID_TCFMASTER_DateReady):         DateReady = oField.ValueAsVariant
            Case (FID_TCFMASTER_TimeReady):         TimeReady = oField.ValueAsVariant
            Case (FID_TCFMASTER_Processed):         Processed = oField.ValueAsVariant
            Case (FID_TCFMASTER_DateProcessed):     DateProcessed = oField.ValueAsVariant
            Case (FID_TCFMASTER_TimeProcessed):     TimeProcessed = oField.ValueAsVariant
            Case (FID_TCFMASTER_ErrorCode):         ErrorCode = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_TCFMASTER_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cEDILogEntry

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function




