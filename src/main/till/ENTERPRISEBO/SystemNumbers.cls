VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSystemNumbers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DA1A176015E"
'<CAMH>****************************************************************************************
'* Module : SystemNumbers
'* Date   : 07/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/SystemNumbers.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 14/01/04 13:37 $
'* $Revision: 11 $
'* Versions:
'* 07/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "SystemNumbers"

Implements IBo
Implements ISysBo

'##ModelId=3DA1A176015F
Private mSuffix(20) As String

'##ModelId=3DA1A1760190
Private mSequenceNo(20) As String

'ID_6
'##ModelId=3DA1A1760191
Private mPrefix(20) As String

'##ModelId=3DA1A17601CC
Private mWidth(20) As String

'##ModelId=3DA1A17601CD
Private mID(20) As Long

Dim m_oSession As Session

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

' Object Id is the row Id on the db
Private m_strObjectId As String

Const POS_PURCH_ORDER_NUMBER     As Long = 1
Const POS_SUPP_RETURN_NUMBER     As Long = 2
Const POS_CUST_ORDER_NUMBER      As Long = 15
Const POS_RECEIPT_NUMBER         As Long = 4
Const POS_IST_OUT_NUMBER         As Long = 4
Const POS_CONS_NOTE_NUMBER       As Long = 5
Const POS_SOQ_NUMBER             As Long = 6
Const POS_PICK_NOTE_NUMBER       As Long = 7
Const POS_DEPOSIT_NUMBER         As Long = 8
Const POS_HO_REFUND_CHECK_NUMBER As Long = 9
Const POS_AS400_SALES_REC_NUMBER As Long = 10
Const POS_STOCK_ADJ_NUMBER       As Long = 11
Const POS_REC_ADJUST_NUMBER      As Long = 12
Const POS_THC_COMM_SEQ_NUMBER    As Long = 13
Const POS_GIRO_STMT_NUMBER       As Long = 14
Const POS_CUST_NUMBER            As Long = 15
Const POS_CUST_CARD_NUMBER       As Long = 16
Const POS_EFTPOS_COMM_NUMBER     As Long = 17
Const POS_SALES_COMM_NUMBER      As Long = 18
Const POS_RECEIPT_COMM_NUMBER    As Long = 19
Const POS_STK_ADJ_COMM_NUMBER    As Long = 20
Const POS_CSH_BAL_COMM_NUMBER    As Long = 21
Const POS_ALLOC_ORDER_NUMBER     As Long = 22


Public Function GetNewPurchaseOrderNumber() As String

    GetNewPurchaseOrderNumber = GetNextSequenceNo(POS_PURCH_ORDER_NUMBER)

End Function

Public Function GetNewSupplierReturnNumber() As String

    GetNewSupplierReturnNumber = GetNextSequenceNo(POS_SUPP_RETURN_NUMBER)

End Function

Public Function GetNewCustomerOrderNumber() As String
    
    GetNewCustomerOrderNumber = GetNextSequenceNo(POS_CUST_ORDER_NUMBER)

End Function

Public Function GetNewReceiptNumber() As String
    
    GetNewReceiptNumber = GetNextSequenceNo(POS_RECEIPT_NUMBER)

End Function

Public Function GetNewISTOutNumber() As String
    
    GetNewISTOutNumber = GetNextSequenceNo(POS_IST_OUT_NUMBER)

End Function

Public Function GetNewConsignmentNoteNumber() As String
    
    GetNewConsignmentNoteNumber = GetNextSequenceNo(POS_CONS_NOTE_NUMBER)

End Function

Public Function GetNewSOQNumber() As String
    
    GetNewSOQNumber = GetNextSequenceNo(POS_SOQ_NUMBER)

End Function

Public Function GetNewPickNoteNumber() As String
    
    GetNewPickNoteNumber = GetNextSequenceNo(POS_PICK_NOTE_NUMBER)

End Function

Public Function GetNewDepositNumber() As String
    
    GetNewDepositNumber = GetNextSequenceNo(POS_DEPOSIT_NUMBER)

End Function

Public Function GetNewHORefundCheckNumber() As String
    
    GetNewHORefundCheckNumber = GetNextSequenceNo(POS_HO_REFUND_CHECK_NUMBER)

End Function

Public Function GetNewAS400SalesRecNumber() As String
    
    GetNewAS400SalesRecNumber = GetNextSequenceNo(POS_AS400_SALES_REC_NUMBER)

End Function

Public Function GetNewStockAdjustmentNumber() As String
    
    GetNewStockAdjustmentNumber = GetNextSequenceNo(POS_STOCK_ADJ_NUMBER)

End Function

Public Function GetNewReceiptAdjustmentNumber() As String
    
    GetNewReceiptAdjustmentNumber = GetNextSequenceNo(POS_REC_ADJUST_NUMBER)

End Function

Public Function GetNewTHCCommSeqNumber() As String
    
    GetNewTHCCommSeqNumber = GetNextSequenceNo(POS_THC_COMM_SEQ_NUMBER)

End Function

Public Function GetNewGiroStatementNumber() As String
    
    GetNewGiroStatementNumber = GetNextSequenceNo(POS_GIRO_STMT_NUMBER)

End Function

Public Function GetNewCustomerNumber() As String
    
    GetNewCustomerNumber = GetNextSequenceNo(POS_CUST_NUMBER)

End Function

Public Function GetNewCustomerCardNumber() As String
    
    GetNewCustomerCardNumber = m_oSession.CurrentEnterprise.IEnterprise_StoreNumber & "0" & GetNextSequenceNo(POS_CUST_CARD_NUMBER)
  
End Function

Public Function GetNewEFTPOSCommNumber() As String
    
    GetNewEFTPOSCommNumber = GetNextSequenceNo(POS_EFTPOS_COMM_NUMBER)

End Function

Public Function GetNewSalesCommNumber() As String
    
    GetNewSalesCommNumber = GetNextSequenceNo(POS_SALES_COMM_NUMBER)

End Function

Public Function GetNewReceiptCommNumber() As String
    
    GetNewReceiptCommNumber = GetNextSequenceNo(POS_RECEIPT_COMM_NUMBER)

End Function

Public Function GetNewStockAdjCommNumber() As String
    
    GetNewStockAdjCommNumber = GetNextSequenceNo(POS_STK_ADJ_COMM_NUMBER)

End Function

Public Function GetNewCashBalCommNumber() As String
    
    GetNewCashBalCommNumber = GetNextSequenceNo(POS_CSH_BAL_COMM_NUMBER)

End Function

Public Function GetNewAllocationOrderNumber() As String
    
    GetNewAllocationOrderNumber = GetNextSequenceNo(POS_ALLOC_ORDER_NUMBER)

End Function




'##ModelId=3DA1A17601FE
Private Function GetNextSequenceNo(ID As Long) As String

Dim nNo        As Long
Dim oRSelector As IRowSelector
Dim oRow       As IRow
Dim oGField    As CFieldGroup
Dim bRet       As Boolean
Dim sFormat    As String
    
    Call IBo_Load
    
    nNo = mSequenceNo(ID)
    sFormat = Left$("00000000", mWidth(ID))
    ' Now update to give the next available sequence number
    mSequenceNo(ID) = Format$(nNo + 1, sFormat)
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_SYSTEMNO_RecID))
    
    Set oGField = GetField(FID_SYSTEMNO_Value)
    ' Now select one occurrence
    oGField.Subscript = ID
    ' and add to the row
    Call oRow.Add(oGField)
    
    bRet = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert bRet
    If bRet Then GetNextSequenceNo = Format$(nNo, sFormat)
    Set oRow = Nothing

End Function

'##ModelId=3DA1A2280244
Public Property Get ID(Index As Variant) As Long
    Let ID = mID(Index)
End Property

'##ModelId=3DA1A22800FA
Public Property Let ID(Index As Variant, ByVal Value As Long)
    Let mID(Index) = Value
End Property

'##ModelId=3DA1A228001E
Public Property Get Width(Index As Variant) As Long
    Let Width = mWidth(Index)
End Property

'##ModelId=3DA1A22702BC
Public Property Let Width(Index As Variant, ByVal Value As Long)
    Let mWidth(Index) = Value
End Property

'##ModelId=3DA1A22701AE
Public Property Get Prefix(Index As Variant) As String
    Let Prefix = mPrefix(Index)
End Property

'##ModelId=3DA1A2270096
Public Property Let Prefix(Index As Variant, ByVal Value As String)
    Let mPrefix(Index) = Value
End Property

'##ModelId=3DA1A22603A2
Public Property Get SequenceNo(Index As Variant) As String
    Let SequenceNo = mSequenceNo(Index)
End Property

'##ModelId=3DA1A2260294
Public Property Let SequenceNo(Index As Variant, ByVal Value As String)
    Let mSequenceNo(Index) = Value
End Property

'##ModelId=3DA1A22601B8
Public Property Get Suffix(Index As Variant) As String
    Let Suffix = mSuffix(Index)
End Property


'##ModelId=3DA1A22600DC
Public Property Let Suffix(Index As Variant, ByVal Value As String)
    Let mSuffix(Index) = Value
End Property

' Object Id is the row Id on the db
Private Property Get ObjectId() As Long
   ObjectId = CLng(m_strObjectId)
End Property
Private Property Let ObjectId(ByVal Value As Long)
   m_strObjectId = Format$(Value, "00")
End Property

Private Sub Class_Initialize()
    ObjectId = 1          ' Only one record in the SYSNUM table
End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 14/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_SYSTEMNO_RecID):   ObjectId = oField.ValueAsVariant
'            Case (FID_SYSTEMNO_ID):     mID = oField.ValueAsVariant
            Case (FID_SYSTEMNO_Width):   Call LoadStringArray(mWidth, oField, m_oSession)
'            Case (FID_SYSTEMNO_Prefix): mPrefix = oField.ValueAsVariant
'            Case (FID_SYSTEMNO_Suffix): mSuffix = oField.ValueAsVariant
            Case (FID_SYSTEMNO_Value):   Call LoadStringArray(mSequenceNo, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow
Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "SystemNumbers - " & mID(0)

End Property
Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function


Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SYSTEMNO

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property
Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cSystemNumbers

End Function


Public Function IBo_Load() As Boolean
    
    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_SYSTEMNO_RecID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = m_strObjectId
        Case (FID_SYSTEMNO_ID):    Set GetField = SaveLongArray(mID, m_oSession)
        Case (FID_SYSTEMNO_Width): Set GetField = SaveStringArray(mWidth, m_oSession)
        Case (FID_SYSTEMNO_Prefix):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPrefix
        Case (FID_SYSTEMNO_Suffix):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSuffix
        Case (FID_SYSTEMNO_Value): Set GetField = SaveStringArray(mSequenceNo, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SYSTEMNO, FID_SYSTEMNO_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    moRowSel.Merge GetSelectAllRow
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


Private Function Initialise(oSession As ISession) As cSystemNumbers
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cSystemNumbers
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SYSTEMNO * &H10000) + 1 To FID_SYSTEMNO_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SYSTEMNO_END_OF_STATIC

End Function







