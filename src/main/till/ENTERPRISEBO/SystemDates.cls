VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSystemDates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D9036D1017C"
'<CAMH>****************************************************************************************
'* Module : cSystemDates
'* Date   : 24/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/SystemDates.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 2/06/03 1:52p $ $Revision: 12 $
'* Versions:
'* 24/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cSystemDates"

Private m_oSession As Session

Private mID                    As String
Private mNoDaysOpen            As Long
Private mWeekEndingDay         As Long
Private mDaysOpenPerPeriod(14) As Long
Private mTodaysDate            As Date
Private mTodaysDayNo           As Long
Private mNextOpenDate          As Date
Private mNextOpenDayNo         As Long
Private mDateInOneWeek         As Date
Private mWeekEndDates(13)      As Date
Private mStoreLiveDate         As Date
Private mUsePeriodSetNo        As Long
Private mPeriod1EndDates(14)   As Date
Private mPeriod1Ended(14)      As Boolean
Private mPeriod1EndProc(14)    As Boolean
Private mPeriod2EndDates(14)   As Date
Private mPeriod2Ended(14)      As Boolean
Private mPeriod2EndProc(14)    As Boolean
Private mCurrentPeriodEndDate  As Date
Private mPriorPeriodEndDate    As Date
Private mCurrentIsYearEnd      As Boolean
Private mPeriodEndNotProcessed As Boolean
Private mWeekEndNotProcessed   As Boolean
Private mNightTaskSetNo        As String
Private mCurrentTaskNo         As String
Private mInRetryMode           As Boolean
Private mSOQCycleLength        As Long
Private mCurrentSOQWeekNo      As Long
Private mAuditDate             As Date
Private mPriceChangeLeadDays   As Long
Private mLastDateCloseStarted  As Date

Dim moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Public Property Get ID() As String
    ID = mID
End Property

Public Property Let ID(ByVal Value As String)
    mID = Value
End Property

Public Property Get NoDaysOpen() As Long
    NoDaysOpen = mNoDaysOpen
End Property

Public Property Let NoDaysOpen(ByVal Value As Long)
    mNoDaysOpen = Value
End Property

Public Property Get WeekEndingDay() As Long
    WeekEndingDay = mWeekEndingDay
End Property

Public Property Let WeekEndingDay(ByVal Value As Long)
    mWeekEndingDay = Value
End Property

Public Property Get DaysOpenPerPeriod(Index As Long) As Long
    DaysOpenPerPeriod = mDaysOpenPerPeriod(Index)
End Property

Public Property Let DaysOpenPerPeriod(Index As Long, ByVal Value As Long)
    mDaysOpenPerPeriod(Index) = Value
End Property

Public Property Get TodaysDate() As Date
    TodaysDate = mTodaysDate
End Property

Public Property Let TodaysDate(ByVal Value As Date)
    mTodaysDate = Value
End Property

Public Property Get TodaysDayNo() As Long
    TodaysDayNo = mTodaysDayNo
End Property

Public Property Let TodaysDayNo(ByVal Value As Long)
    mTodaysDayNo = Value
End Property

Public Property Get NextOpenDate() As Date
    NextOpenDate = mNextOpenDate
End Property

Public Property Let NextOpenDate(ByVal Value As Date)
    mNextOpenDate = Value
End Property

Public Property Get NextOpenDayNo() As Long
    NextOpenDayNo = mNextOpenDayNo
End Property

Public Property Let NextOpenDayNo(ByVal Value As Long)
    mNextOpenDayNo = Value
End Property

Public Property Get DateInOneWeek() As Date
    DateInOneWeek = mDateInOneWeek
End Property

Public Property Let DateInOneWeek(ByVal Value As Date)
    mDateInOneWeek = Value
End Property

Public Property Get WeekEndDates(Index As Long) As Date
    WeekEndDates = mWeekEndDates(Index)
End Property

Public Property Let WeekEndDates(Index As Long, ByVal Value As Date)
    mWeekEndDates(Index) = Value
End Property

Public Property Get StoreLiveDate() As Date
    StoreLiveDate = mStoreLiveDate
End Property

Public Property Let StoreLiveDate(ByVal Value As Date)
    mStoreLiveDate = Value
End Property

Public Property Get UsePeriodSetNo() As Long
    UsePeriodSetNo = mUsePeriodSetNo
End Property

Public Property Let UsePeriodSetNo(ByVal Value As Long)
    mUsePeriodSetNo = Value
End Property

Public Property Get Period1EndDates(Index As Long) As Date
    Period1EndDates = mPeriod1EndDates(Index)
End Property

Public Property Let Period1EndDates(Index As Long, ByVal Value As Date)
    mPeriod1EndDates(Index) = Value
End Property

Public Property Get Period1Ended(Index As Long) As Boolean
    Period1Ended = mPeriod1Ended(Index)
End Property

Public Property Let Period1Ended(Index As Long, ByVal Value As Boolean)
    mPeriod1Ended(Index) = Value
End Property

Public Property Get Period1EndProc(Index As Long) As Boolean
    Period1EndProc = mPeriod1EndProc(Index)
End Property

Public Property Let Period1EndProc(Index As Long, ByVal Value As Boolean)
    mPeriod1EndProc(Index) = Value
End Property

Public Property Get Period2EndDates(Index As Long) As Date
    Period2EndDates = mPeriod2EndDates(Index)
End Property

Public Property Let Period2EndDates(Index As Long, ByVal Value As Date)
    mPeriod2EndDates(Index) = Value
End Property

Public Property Get Period2Ended(Index As Long) As Boolean
    Period2Ended = mPeriod2Ended(Index)
End Property

Public Property Let Period2Ended(Index As Long, ByVal Value As Boolean)
    mPeriod2Ended(Index) = Value
End Property

Public Property Get Period2EndProc(Index As Long) As Boolean
    Period2EndProc = mPeriod2EndProc(Index)
End Property

Public Property Let Period2EndProc(Index As Long, ByVal Value As Boolean)
    mPeriod2EndProc(Index) = Value
End Property

Public Property Get CurrentPeriodEndDate() As Date
    CurrentPeriodEndDate = mCurrentPeriodEndDate
End Property

Public Property Let CurrentPeriodEndDate(ByVal Value As Date)
    mCurrentPeriodEndDate = Value
End Property

Public Property Get PriorPeriodEndDate() As Date
    PriorPeriodEndDate = mPriorPeriodEndDate
End Property

Public Property Let PriorPeriodEndDate(ByVal Value As Date)
    mPriorPeriodEndDate = Value
End Property

Public Property Get CurrentIsYearEnd() As Boolean
    CurrentIsYearEnd = mCurrentIsYearEnd
End Property

Public Property Let CurrentIsYearEnd(ByVal Value As Boolean)
    mCurrentIsYearEnd = Value
End Property

Public Property Get PeriodEndNotProcessed() As Boolean
    PeriodEndNotProcessed = mPeriodEndNotProcessed
End Property

Public Property Let PeriodEndNotProcessed(ByVal Value As Boolean)
    mPeriodEndNotProcessed = Value
End Property

Public Property Get WeekEndNotProcessed() As Boolean
    WeekEndNotProcessed = mWeekEndNotProcessed
End Property

Public Property Let WeekEndNotProcessed(ByVal Value As Boolean)
    mWeekEndNotProcessed = Value
End Property

Public Property Get NightTaskSetNo() As String
    NightTaskSetNo = mNightTaskSetNo
End Property

Public Property Let NightTaskSetNo(ByVal Value As String)
    mNightTaskSetNo = Value
End Property

Public Property Get CurrentTaskNo() As String
    CurrentTaskNo = mCurrentTaskNo
End Property

Public Property Let CurrentTaskNo(ByVal Value As String)
    mCurrentTaskNo = Value
End Property

Public Property Get InRetryMode() As Boolean
    InRetryMode = mInRetryMode
End Property

Public Property Let InRetryMode(ByVal Value As Boolean)
    mInRetryMode = Value
End Property

Public Property Get SOQCycleLength() As Long
    SOQCycleLength = mSOQCycleLength
End Property

Public Property Let SOQCycleLength(ByVal Value As Long)
    mSOQCycleLength = Value
End Property

Public Property Get CurrentSOQWeekNo() As Long
    CurrentSOQWeekNo = mCurrentSOQWeekNo
End Property

Public Property Let CurrentSOQWeekNo(ByVal Value As Long)
    mCurrentSOQWeekNo = Value
End Property

Public Property Get AuditDate() As Date
    AuditDate = mAuditDate
End Property

Public Property Let AuditDate(ByVal Value As Date)
    mAuditDate = Value
End Property

Public Property Get PriceChangeLeadDays() As Long
    PriceChangeLeadDays = mPriceChangeLeadDays
End Property

Public Property Let PriceChangeLeadDays(ByVal Value As Long)
    mPriceChangeLeadDays = Value
End Property

Public Property Get LastDateCloseStarted() As Date
    LastDateCloseStarted = mLastDateCloseStarted
End Property

Public Property Let LastDateCloseStarted(ByVal Value As Date)
    mLastDateCloseStarted = Value
End Property



Public Function MaintainNightMaster(SetNumber As String, LastTaskNumber As String, RetryMode As Boolean) As Boolean

Dim oRow       As IRow
    
    MaintainNightMaster = False
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    mNightTaskSetNo = SetNumber
    mCurrentTaskNo = LastTaskNumber
    mInRetryMode = RetryMode
    
    Call oRow.Add(GetField(FID_SYSTEMDATES_ID))
    Call oRow.Add(GetField(FID_SYSTEMDATES_NightTaskSetNo))
    Call oRow.Add(GetField(FID_SYSTEMDATES_CurrentTaskNo))
    Call oRow.Add(GetField(FID_SYSTEMDATES_InRetryMode))
    
    MaintainNightMaster = m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Function 'MaintainNightMaster


Private Sub Class_Initialize()

    mID = "01"

End Sub

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cSystemDates

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "System Dates " & mID

End Property

Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function Initialise(oSession As ISession) As cSystemDates
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SYSTEMDATES

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    
    Select Case (lFieldID)
        Case (FID_SYSTEMDATES_ID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mID
        Case (FID_SYSTEMDATES_NoDaysOpen):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNoDaysOpen
        Case (FID_SYSTEMDATES_WeekEndingDay):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mWeekEndingDay
        Case (FID_SYSTEMDATES_DaysOpenPerPeriod):
                Set GetField = SaveLongArray(mDaysOpenPerPeriod, m_oSession)
        Case (FID_SYSTEMDATES_TodaysDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTodaysDate
        Case (FID_SYSTEMDATES_TodaysDayNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mTodaysDayNo
        Case (FID_SYSTEMDATES_NextOpenDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mNextOpenDate
        Case (FID_SYSTEMDATES_NextOpenDayNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNextOpenDayNo
        Case (FID_SYSTEMDATES_DateInOneWeek):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateInOneWeek
        Case (FID_SYSTEMDATES_WeekEndDates):
                Set GetField = SaveDateArray(mWeekEndDates, m_oSession)
        Case (FID_SYSTEMDATES_StoreLiveDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mStoreLiveDate
        Case (FID_SYSTEMDATES_UsePeriodSetNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mUsePeriodSetNo
        Case (FID_SYSTEMDATES_Period1EndDates):
                Set GetField = SaveDateArray(mPeriod1EndDates, m_oSession)
        Case (FID_SYSTEMDATES_Period1Ended):
                Set GetField = SaveBooleanArray(mPeriod1Ended, m_oSession)
        Case (FID_SYSTEMDATES_Period1EndProc):
                Set GetField = SaveBooleanArray(mPeriod1EndProc, m_oSession)
        Case (FID_SYSTEMDATES_Period2EndDates):
                Set GetField = SaveDateArray(mPeriod2EndDates, m_oSession)
        Case (FID_SYSTEMDATES_Period2Ended):
                Set GetField = SaveBooleanArray(mPeriod2Ended, m_oSession)
        Case (FID_SYSTEMDATES_Period2EndProc):
                Set GetField = SaveBooleanArray(mPeriod2EndProc, m_oSession)
        Case (FID_SYSTEMDATES_CurrentPeriodEndDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mCurrentPeriodEndDate
        Case (FID_SYSTEMDATES_PriorPeriodEndDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mPriorPeriodEndDate
        Case (FID_SYSTEMDATES_CurrentIsYearEnd):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCurrentIsYearEnd
        Case (FID_SYSTEMDATES_PeriodEndNotProcessed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPeriodEndNotProcessed
        Case (FID_SYSTEMDATES_WeekEndNotProcessed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mWeekEndNotProcessed
        Case (FID_SYSTEMDATES_NightTaskSetNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mNightTaskSetNo
        Case (FID_SYSTEMDATES_CurrentTaskNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrentTaskNo
        Case (FID_SYSTEMDATES_InRetryMode):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mInRetryMode
        Case (FID_SYSTEMDATES_SOQCycleLength):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSOQCycleLength
        Case (FID_SYSTEMDATES_CurrentSOQWeekNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mCurrentSOQWeekNo
        Case (FID_SYSTEMDATES_AuditDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mAuditDate
        Case (FID_SYSTEMDATES_PriceChangeLeadDays):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPriceChangeLeadDays
        Case (FID_SYSTEMDATES_LastDateCloseStarted):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mLastDateCloseStarted
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SYSTEMDATES, FID_SYSTEMDATES_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    moRowSel.Merge GetSelectAllRow
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
'##ModelId=3D749F8A038E
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_SYSTEMDATES_ID):                    mID = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_NoDaysOpen):            mNoDaysOpen = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_WeekEndingDay):         mWeekEndingDay = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_DaysOpenPerPeriod):     Call LoadLongArray(mDaysOpenPerPeriod, oField, m_oSession)
            Case (FID_SYSTEMDATES_TodaysDate):            mTodaysDate = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_TodaysDayNo):           mTodaysDayNo = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_NextOpenDate):          mNextOpenDate = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_NextOpenDayNo):         mNextOpenDayNo = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_DateInOneWeek):         mDateInOneWeek = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_WeekEndDates):          Call LoadDateArray(mWeekEndDates, oField, m_oSession)
            Case (FID_SYSTEMDATES_StoreLiveDate):         mStoreLiveDate = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_UsePeriodSetNo):        mUsePeriodSetNo = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_Period1EndDates):       Call LoadDateArray(mPeriod1EndDates, oField, m_oSession)
            Case (FID_SYSTEMDATES_Period1Ended):          Call LoadBooleanArray(mPeriod1Ended, oField, m_oSession)
            Case (FID_SYSTEMDATES_Period1EndProc):        Call LoadBooleanArray(mPeriod1EndProc, oField, m_oSession)
            Case (FID_SYSTEMDATES_Period2EndDates):       Call LoadDateArray(mPeriod2EndDates, oField, m_oSession)
            Case (FID_SYSTEMDATES_Period2Ended):          Call LoadBooleanArray(mPeriod2Ended, oField, m_oSession)
            Case (FID_SYSTEMDATES_Period2EndProc):        Call LoadBooleanArray(mPeriod2EndProc, oField, m_oSession)
            Case (FID_SYSTEMDATES_CurrentPeriodEndDate):  mCurrentPeriodEndDate = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_PriorPeriodEndDate):    mPriorPeriodEndDate = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_CurrentIsYearEnd):      mCurrentIsYearEnd = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_PeriodEndNotProcessed): mPeriodEndNotProcessed = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_WeekEndNotProcessed):   mWeekEndNotProcessed = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_NightTaskSetNo):        mNightTaskSetNo = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_CurrentTaskNo):         mCurrentTaskNo = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_InRetryMode):           mInRetryMode = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_SOQCycleLength):        mSOQCycleLength = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_CurrentSOQWeekNo):      mCurrentSOQWeekNo = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_AuditDate):             mAuditDate = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_PriceChangeLeadDays):   mPriceChangeLeadDays = oField.ValueAsVariant
            Case (FID_SYSTEMDATES_LastDateCloseStarted):  mLastDateCloseStarted = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Public Sub Retrieve()
    
    Call IBo_Load

End Sub


Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cSystemDates
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SYSTEMDATES * &H10000) + 1 To FID_SYSTEMDATES_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_SetLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_SetLoadField"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_SetLoadField = True

End Function 'IBo_SetLoadField



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SYSTEMDATES_END_OF_STATIC

End Function

Public Function StartTask(strTaskSetNumber As String, strTaskNumber As String, ByRef oNightTask As cNightMaster)

Dim oRow       As IRow
Dim oNightLog  As cNightLog
    
On Error GoTo Err_Handler

    StartTask = False
    
    ' and persist this new value in the database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    mNightTaskSetNo = strTaskSetNumber
    mCurrentTaskNo = strTaskNumber
    
    Call oRow.Add(GetField(FID_SYSTEMDATES_ID))
    Call oRow.Add(GetField(FID_SYSTEMDATES_NightTaskSetNo))
    Call oRow.Add(GetField(FID_SYSTEMDATES_CurrentTaskNo))
    
    Call m_oSession.Database.StartTransaction
    If m_oSession.Database.SavePartialBo(Me, oRow) = True Then
        Set oNightLog = m_oSession.Database.CreateBusinessObject(CLASSID_NIGHTLOG)
        oNightLog.LogDate = Now()
        oNightLog.SetNumber = strTaskSetNumber
        oNightLog.TaskNumber = strTaskNumber
        oNightLog.Description = oNightTask.Description
        oNightLog.ProgramPath = oNightTask.ProgramPath
        oNightLog.DateStarted = Now()
        oNightLog.TimeStarted = Format$(Now(), "HHMMSS")
        If oNightLog.IBo_SaveIfNew = True Then
            StartTask = True
            Call m_oSession.Database.CommitTransaction
        Else
            Call Err.Raise(OASYS_ERR_UNABLE_To_LOG_TASK, MODULE_NAME, "Failed to log Task Started " & strTaskSetNumber & "/" & strTaskNumber)
        End If
    Else
        Call Err.Raise(OASYS_ERR_UNABLE_To_LOG_TASK, MODULE_NAME, "Failed to log Task Started " & strTaskSetNumber & "/" & strTaskNumber)
    End If
    
    Set oNightLog = Nothing
    Set oRow = Nothing
    
    Exit Function
    
Err_Handler:

    Call m_oSession.Database.RollbackTransaction
    Call Err.Raise(OASYS_ERR_UNABLE_To_LOG_TASK, MODULE_NAME, "Failed to log Task Started " & strTaskSetNumber & "/" & strTaskNumber)
    
End Function
