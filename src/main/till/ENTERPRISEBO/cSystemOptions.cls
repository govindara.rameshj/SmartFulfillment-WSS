VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSystemOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DE5EB6C037A"
'<CAMH>****************************************************************************************
'* Module : cSystemOptions
'* Date   : 28/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/EnterpriseBO/cSystemOptions.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Richardc $
'* $Date: 6/05/04 17:28 $
'* $Revision: 9 $
'* Versions:
'* 28/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cSystemOptions"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3DE5EB9603C0
Private mID As String

'##ModelId=3DE5EB99028A
Private mStoreNumber As String

'##ModelId=3DE5EB9F001E
Private mStoreName As String

'##ModelId=3DE5EBA20302
Private mHeadOfficeName As String

'##ModelId=3DE5EBA80104
Private mHeadOfficeAddress1 As String

'##ModelId=3DE5EBB203CA
Private mHeadOfficeAddress2 As String

'##ModelId=3DE5EBBA0000
Private mHeadOfficeAddress3 As String

'##ModelId=3DE5EBC1035C
Private mHeadOfficeAddress4 As String

'##ModelId=3DE5EBC902C6
Private mHeadOfficeAddress5 As String

'##ModelId=3DE5EBD00320
Private mHeadOfficePostCode As String

'##ModelId=3DE5EBD9035C
Private mHeadOfficePhoneNo As String

'##ModelId=3DE5EBE201E0
Private mHeadOfficeFaxNumber As String

'##ModelId=3DE5EBEA037A
Private mVATNumber As String

'##ModelId=3DE5EBF30046
Private mMasterOutletNumber As String

'##ModelId=3DE5EBFE02C6
Private mHighestValidFunction As String

'##ModelId=3DE5EC3301B8
Private mMultipleSignOns As Boolean

'##ModelId=3DE5EC4E02E4
Private mPasswordValidFor As Long

'##ModelId=3DE5EC5801CC
Private mAuthorisationValidFor As Long

'##ModelId=3DE5EC6E021C
Private mTrackStockOnHand As Boolean

'##ModelId=3DE5EC79038E
Private mUseOtherInvoiceAddress As Boolean

'##ModelId=3DE5ECA7033E
Private mCostFigureAccessLevel As String

'##ModelId=3DE5ECB30168
Private mAddressStyle As String

'##ModelId=3DE5ECBA00AA
Private mPICDayCode As String

'##ModelId=3DE5ECDE0096
Private mPICUseHandhelds As Boolean

'##ModelId=3DE5ECF50370
Private mPICLastNoOfItems As Long

'##ModelId=3DE5ED090366
Private mPICSampleFrequency As Long

'##ModelId=3DE5ED1D00D2
Private mAutoApplyPriceChanges As Boolean

'##ModelId=3DE5ED240122
Private mAutoApplyNoDays As Long

'##ModelId=3DE5ED470262
Private mAutoPrintShelfLabels As Boolean

'##ModelId=3DE5ED63030C
Private mBulkUpdates As Boolean

'##ModelId=3DE5ED7402F8
Private mLocalPricingAllowed As String

'##ModelId=3DE5ED820140
Private mCardType As String

'##ModelId=3DE5ED8D0136
Private mCompanyGroup As String

'##ModelId=3DE5ED97019A
Private mTillLocalDrive As String

'##ModelId=3DE5EDA10352
Private mTillRemoteDrive As String

'##ModelId=3DE5EDAC000A
Private mTillPathName As String

'##ModelId=3DE5EDD7026C
Private mNoCopiesPricingDocs As Long

'##ModelId=3DE5EDFC02EE
Private mStoreMaintMinShelf As Boolean

'##ModelId=3DE5EE140212
Private mStoreMaintMaxShelf As Boolean

'##ModelId=3DE5EE31021C
Private mMaximumShelfMultiplier As Long

'##ModelId=3DE5EE4500C8
Private mAllowReceiptMaint As Boolean

'##ModelId=3DE5EE62038E
Private mReceiptMaintGrace As Long

Private mRunHiearchyLinkBuild As Boolean

Public Property Get RunHiearchyLinkBuild() As Boolean
   Let RunHiearchyLinkBuild = mRunHiearchyLinkBuild
End Property

Public Property Let RunHiearchyLinkBuild(ByVal Value As Boolean)
    Let mRunHiearchyLinkBuild = Value
End Property

'##ModelId=3DE5F48C01E0
Public Property Get ReceiptMaintGrace() As Long
   Let ReceiptMaintGrace = mReceiptMaintGrace
End Property

'##ModelId=3DE5F48B03A2
Public Property Let ReceiptMaintGrace(ByVal Value As Long)
    Let mReceiptMaintGrace = Value
End Property

'##ModelId=3DE5F48B0258
Public Property Get AllowReceiptMaint() As Boolean
   Let AllowReceiptMaint = mAllowReceiptMaint
End Property

'##ModelId=3DE5F48B006E
Public Property Let AllowReceiptMaint(ByVal Value As Boolean)
    Let mAllowReceiptMaint = Value
End Property

'##ModelId=3DE5F48A030C
Public Property Get MaximumShelfMultiplier() As Long
   Let MaximumShelfMultiplier = mMaximumShelfMultiplier
End Property

'##ModelId=3DE5F48A0122
Public Property Let MaximumShelfMultiplier(ByVal Value As Long)
    Let mMaximumShelfMultiplier = Value
End Property

'##ModelId=3DE5F48A000A
Public Property Get StoreMaintMaxShelf() As Boolean
   Let StoreMaintMaxShelf = mStoreMaintMaxShelf
End Property

'##ModelId=3DE5F48901CC
Public Property Let StoreMaintMaxShelf(ByVal Value As Boolean)
    Let mStoreMaintMaxShelf = Value
End Property

'##ModelId=3DE5F48900BE
Public Property Get StoreMaintMinShelf() As Boolean
   Let StoreMaintMinShelf = mStoreMaintMinShelf
End Property

'##ModelId=3DE5F48802B2
Public Property Let StoreMaintMinShelf(ByVal Value As Boolean)
    Let mStoreMaintMinShelf = Value
End Property

'##ModelId=3DE5F4880168
Public Property Get NoCopiesPricingDocs() As Long
   Let NoCopiesPricingDocs = mNoCopiesPricingDocs
End Property

'##ModelId=3DE5F4870366
Public Property Let NoCopiesPricingDocs(ByVal Value As Long)
    Let mNoCopiesPricingDocs = Value
End Property

'##ModelId=3DE5F4870258
Public Property Get TillPathName() As String
   Let TillPathName = mTillPathName
End Property

'##ModelId=3DE5F4870064
Public Property Let TillPathName(ByVal Value As String)
    Let mTillPathName = Value
End Property

'##ModelId=3DE5F486033E
Public Property Get TillRemoteDrive() As String
   Let TillRemoteDrive = mTillRemoteDrive
End Property

'##ModelId=3DE5F486014A
Public Property Let TillRemoteDrive(ByVal Value As String)
    Let mTillRemoteDrive = Value
End Property

'##ModelId=3DE5F4860000
Public Property Get TillLocalDrive() As String
   Let TillLocalDrive = mTillLocalDrive
End Property

'##ModelId=3DE5F4850230
Public Property Let TillLocalDrive(ByVal Value As String)
    Let mTillLocalDrive = Value
End Property

'##ModelId=3DE5F4850122
Public Property Get CompanyGroup() As String
   Let CompanyGroup = mCompanyGroup
End Property

'##ModelId=3DE5F4840316
Public Property Let CompanyGroup(ByVal Value As String)
    Let mCompanyGroup = Value
End Property

'##ModelId=3DE5F4840208
Public Property Get CardType() As String
   Let CardType = mCardType
End Property

'##ModelId=3DE5F4840050
Public Property Let CardType(ByVal Value As String)
    Let mCardType = Value
End Property

'##ModelId=3DE5F483032A
Public Property Get LocalPricingAllowed() As String
   Let LocalPricingAllowed = mLocalPricingAllowed
End Property

'##ModelId=3DE5F4830172
Public Property Let LocalPricingAllowed(ByVal Value As String)
    Let mLocalPricingAllowed = Value
End Property

'##ModelId=3DE5F4830028
Public Property Get BulkUpdates() As Boolean
   Let BulkUpdates = mBulkUpdates
End Property

'##ModelId=3DE5F4820258
Public Property Let BulkUpdates(ByVal Value As Boolean)
    Let mBulkUpdates = Value
End Property

'##ModelId=3DE5F4820140
Public Property Get AutoPrintShelfLabels() As Boolean
   Let AutoPrintShelfLabels = mAutoPrintShelfLabels
End Property

'##ModelId=3DE5F4810370
Public Property Let AutoPrintShelfLabels(ByVal Value As Boolean)
    Let mAutoPrintShelfLabels = Value
End Property

'##ModelId=3DE5F4810262
Public Property Get AutoApplyNoDays() As Long
   Let AutoApplyNoDays = mAutoApplyNoDays
End Property

'##ModelId=3DE5F48100DC
Public Property Let AutoApplyNoDays(ByVal Value As Long)
    Let mAutoApplyNoDays = Value
End Property

'##ModelId=3DE5F48003B6
Public Property Get AutoApplyPriceChanges() As Boolean
   Let AutoApplyPriceChanges = mAutoApplyPriceChanges
End Property

'##ModelId=3DE5F48001FE
Public Property Let AutoApplyPriceChanges(ByVal Value As Boolean)
    Let mAutoApplyPriceChanges = Value
End Property

'##ModelId=3DE5F4800122
Public Property Get PICSampleFrequency() As Long
   Let PICSampleFrequency = mPICSampleFrequency
End Property

'##ModelId=3DE5F47F0352
Public Property Let PICSampleFrequency(ByVal Value As Long)
    Let mPICSampleFrequency = Value
End Property

'##ModelId=3DE5F47F0276
Public Property Get PICLastNoOfItems() As Long
   Let PICLastNoOfItems = mPICLastNoOfItems
End Property

'##ModelId=3DE5F47F00BE
Public Property Let PICLastNoOfItems(ByVal Value As Long)
    Let mPICLastNoOfItems = Value
End Property

'##ModelId=3DE5F47E03CA
Public Property Get PICUseHandhelds() As Boolean
   Let PICUseHandhelds = mPICUseHandhelds
End Property

'##ModelId=3DE5F47E024E
Public Property Let PICUseHandhelds(ByVal Value As Boolean)
    Let mPICUseHandhelds = Value
End Property

'##ModelId=3DE5F47E0172
Public Property Get PICDayCode() As String
   Let PICDayCode = mPICDayCode
End Property

'##ModelId=3DE5F47D03A2
Public Property Let PICDayCode(ByVal Value As String)
    Let mPICDayCode = Value
End Property

'##ModelId=3DE5F47D02C6
Public Property Get AddressStyle() As String
   Let AddressStyle = mAddressStyle
End Property

'##ModelId=3DE5F47D0140
Public Property Let AddressStyle(ByVal Value As String)
    Let mAddressStyle = Value
End Property

'##ModelId=3DE5F47D0064
Public Property Get CostFigureAccessLevel() As String
   Let CostFigureAccessLevel = mCostFigureAccessLevel
End Property

'##ModelId=3DE5F47C02D0
Public Property Let CostFigureAccessLevel(ByVal Value As String)
    Let mCostFigureAccessLevel = Value
End Property

'##ModelId=3DE5F47C01F4
Public Property Get UseOtherInvoiceAddress() As Boolean
   Let UseOtherInvoiceAddress = mUseOtherInvoiceAddress
End Property

'##ModelId=3DE5F47C00AA
Public Property Let UseOtherInvoiceAddress(ByVal Value As Boolean)
    Let mUseOtherInvoiceAddress = Value
End Property

'##ModelId=3DE5F47B0384
Public Property Get TrackStockOnHand() As Boolean
   Let TrackStockOnHand = mTrackStockOnHand
End Property

'##ModelId=3DE5F47B023A
Public Property Let TrackStockOnHand(ByVal Value As Boolean)
    Let mTrackStockOnHand = Value
End Property

'##ModelId=3DE5F47B015E
Public Property Get AuthorisationValidFor() As Long
   Let AuthorisationValidFor = mAuthorisationValidFor
End Property

'##ModelId=3DE5F47A03C0
Public Property Let AuthorisationValidFor(ByVal Value As Long)
    Let mAuthorisationValidFor = Value
End Property

'##ModelId=3DE5F47A0320
Public Property Get PasswordValidFor() As Long
   Let PasswordValidFor = mPasswordValidFor
End Property

'##ModelId=3DE5F47A019A
Public Property Let PasswordValidFor(ByVal Value As Long)
    Let mPasswordValidFor = Value
End Property

'##ModelId=3DE5F47A00FA
Public Property Get MultipleSignOns() As Boolean
   Let MultipleSignOns = mMultipleSignOns
End Property

'##ModelId=3DE5F4790398
Public Property Let MultipleSignOns(ByVal Value As Boolean)
    Let mMultipleSignOns = Value
End Property

'##ModelId=3DE5F47902BC
Public Property Get HighestValidFunction() As String
   Let HighestValidFunction = mHighestValidFunction
End Property

'##ModelId=3DE5F4790172
Public Property Let HighestValidFunction(ByVal Value As String)
    Let mHighestValidFunction = Value
End Property

'##ModelId=3DE5F4790096
Public Property Get MasterOutletNumber() As String
   Let MasterOutletNumber = mMasterOutletNumber
End Property

'##ModelId=3DE5F4780334
Public Property Let MasterOutletNumber(ByVal Value As String)
    Let mMasterOutletNumber = Value
End Property

'##ModelId=3DE5F4780294
Public Property Get VATNumber() As String
   Let VATNumber = mVATNumber
End Property

'##ModelId=3DE5F478014A
Public Property Let VATNumber(ByVal Value As String)
    Let mVATNumber = Value
End Property

'##ModelId=3DE5F47800A0
Public Property Get HeadOfficeFaxNumber() As String
   Let HeadOfficeFaxNumber = mHeadOfficeFaxNumber
End Property

'##ModelId=3DE5F477033E
Public Property Let HeadOfficeFaxNumber(ByVal Value As String)
    Let mHeadOfficeFaxNumber = Value
End Property

'##ModelId=3DE5F477029E
Public Property Get HeadOfficePhoneNo() As String
   Let HeadOfficePhoneNo = mHeadOfficePhoneNo
End Property

'##ModelId=3DE5F4770154
Public Property Let HeadOfficePhoneNo(ByVal Value As String)
    Let mHeadOfficePhoneNo = Value
End Property

'##ModelId=3DE5F47700AA
Public Property Get HeadOfficePostCode() As String
   Let HeadOfficePostCode = mHeadOfficePostCode
End Property

'##ModelId=3DE5F4760384
Public Property Let HeadOfficePostCode(ByVal Value As String)
    Let mHeadOfficePostCode = Value
End Property

'##ModelId=3DE5F47602DA
Public Property Get HeadOfficeAddress5() As String
   Let HeadOfficeAddress5 = mHeadOfficeAddress5
End Property

'##ModelId=3DE5F4760190
Public Property Let HeadOfficeAddress5(ByVal Value As String)
    Let mHeadOfficeAddress5 = Value
End Property

'##ModelId=3DE5F4760082
Public Property Get HeadOfficeAddress4() As String
   Let HeadOfficeAddress4 = mHeadOfficeAddress4
End Property

'##ModelId=3DE5F4750320
Public Property Let HeadOfficeAddress4(ByVal Value As String)
    Let mHeadOfficeAddress4 = Value
End Property

'##ModelId=3DE5F4750276
Public Property Get HeadOfficeAddress3() As String
   Let HeadOfficeAddress3 = mHeadOfficeAddress3
End Property

'##ModelId=3DE5F4750168
Public Property Let HeadOfficeAddress3(ByVal Value As String)
    Let mHeadOfficeAddress3 = Value
End Property

'##ModelId=3DE5F47500FA
Public Property Get HeadOfficeAddress2() As String
   Let HeadOfficeAddress2 = mHeadOfficeAddress2
End Property

'##ModelId=3DE5F47403D4
Public Property Let HeadOfficeAddress2(ByVal Value As String)
    Let mHeadOfficeAddress2 = Value
End Property

'##ModelId=3DE5F474032A
Public Property Get HeadOfficeAddress1() As String
   Let HeadOfficeAddress1 = mHeadOfficeAddress1
End Property

'##ModelId=3DE5F474021C
Public Property Let HeadOfficeAddress1(ByVal Value As String)
    Let mHeadOfficeAddress1 = Value
End Property

'##ModelId=3DE5F4740172
Public Property Get HeadOfficeName() As String
   Let HeadOfficeName = mHeadOfficeName
End Property

'##ModelId=3DE5F4740096
Public Property Let HeadOfficeName(ByVal Value As String)
    Let mHeadOfficeName = Value
End Property

'##ModelId=3DE5F47303DE
Public Property Get StoreName() As String
   Let StoreName = mStoreName
End Property

'##ModelId=3DE5F47302C6
Public Property Let StoreName(ByVal Value As String)
    Let mStoreName = Value
End Property

'##ModelId=3DE5F4730226
Public Property Get StoreNumber() As String
   Let StoreNumber = mStoreNumber
End Property

'##ModelId=3DE5F473014A
Public Property Let StoreNumber(ByVal Value As String)
    Let mStoreNumber = Value
End Property

'##ModelId=3DE5F47300A0
Public Property Get ID() As String
   Let ID = mID
End Property


'##ModelId=3DE5F47203AC
Public Property Let ID(ByVal Value As String)
    Let mID = Value
End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Sub Class_Initialize()

    mID = "01"

End Sub

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_INVENTORY * &H10000) + 1 To FID_INVENTORY_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cSystemOptions

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SYSTEMOPTIONS, FID_SYSTEMOPTIONS_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_SYSTEMOPTIONS_ID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mID
        Case (FID_SYSTEMOPTIONS_StoreNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreNumber
        Case (FID_SYSTEMOPTIONS_StoreName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreName
        Case (FID_SYSTEMOPTIONS_HeadOfficeName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeName
        Case (FID_SYSTEMOPTIONS_HeadOfficeAddress1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeAddress1
        Case (FID_SYSTEMOPTIONS_HeadOfficeAddress2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeAddress2
        Case (FID_SYSTEMOPTIONS_HeadOfficeAddress3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeAddress3
        Case (FID_SYSTEMOPTIONS_HeadOfficeAddress4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeAddress4
        Case (FID_SYSTEMOPTIONS_HeadOfficeAddress5):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeAddress5
        Case (FID_SYSTEMOPTIONS_HeadOfficePostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficePostCode
        Case (FID_SYSTEMOPTIONS_HeadOfficePhoneNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficePhoneNo
        Case (FID_SYSTEMOPTIONS_HeadOfficeFaxNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHeadOfficeFaxNumber
        Case (FID_SYSTEMOPTIONS_VATNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mVATNumber
        Case (FID_SYSTEMOPTIONS_MasterOutletNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMasterOutletNumber
        Case (FID_SYSTEMOPTIONS_HighestValidFunction):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHighestValidFunction
        Case (FID_SYSTEMOPTIONS_MultipleSignOns):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mMultipleSignOns
        Case (FID_SYSTEMOPTIONS_PasswordValidFor):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPasswordValidFor
        Case (FID_SYSTEMOPTIONS_AuthorisationValidFor):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mAuthorisationValidFor
        Case (FID_SYSTEMOPTIONS_TrackStockOnHand):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mTrackStockOnHand
        Case (FID_SYSTEMOPTIONS_UseOtherInvoiceAddress):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mUseOtherInvoiceAddress
        Case (FID_SYSTEMOPTIONS_CostFigureAccessLevel):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCostFigureAccessLevel
        Case (FID_SYSTEMOPTIONS_AddressStyle):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressStyle
        Case (FID_SYSTEMOPTIONS_PICDayCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPICDayCode
        Case (FID_SYSTEMOPTIONS_PICUseHandhelds):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPICUseHandhelds
        Case (FID_SYSTEMOPTIONS_PICLastNoOfItems):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPICLastNoOfItems
        Case (FID_SYSTEMOPTIONS_PICSampleFrequency):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPICSampleFrequency
        Case (FID_SYSTEMOPTIONS_AutoApplyPriceChanges):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAutoApplyPriceChanges
        Case (FID_SYSTEMOPTIONS_AutoApplyNoDays):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mAutoApplyNoDays
        Case (FID_SYSTEMOPTIONS_AutoPrintShelfLabels):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAutoPrintShelfLabels
        Case (FID_SYSTEMOPTIONS_BulkUpdates):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mBulkUpdates
        Case (FID_SYSTEMOPTIONS_LocalPricingAllowed):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLocalPricingAllowed
        Case (FID_SYSTEMOPTIONS_CardType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCardType
        Case (FID_SYSTEMOPTIONS_CompanyGroup):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompanyGroup
        Case (FID_SYSTEMOPTIONS_TillLocalDrive):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillLocalDrive
        Case (FID_SYSTEMOPTIONS_TillRemoteDrive):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillRemoteDrive
        Case (FID_SYSTEMOPTIONS_TillPathName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillPathName
        Case (FID_SYSTEMOPTIONS_NoCopiesPricingDocs):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNoCopiesPricingDocs
        Case (FID_SYSTEMOPTIONS_StoreMaintMinShelf):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mStoreMaintMinShelf
        Case (FID_SYSTEMOPTIONS_StoreMaintMaxShelf):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mStoreMaintMaxShelf
        Case (FID_SYSTEMOPTIONS_MaximumShelfMultiplier):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mMaximumShelfMultiplier
        Case (FID_SYSTEMOPTIONS_AllowReceiptMaint):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAllowReceiptMaint
        Case (FID_SYSTEMOPTIONS_ReceiptMaintGrace):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReceiptMaintGrace
        Case (FID_SYSTEMOPTIONS_RunHierarchyLinkBuild):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRunHiearchyLinkBuild
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cSystemOptions
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SYSTEMOPTIONS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "System Options " & mStoreNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_SYSTEMOPTIONS_ID):                     mID = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_StoreNumber):            mStoreNumber = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_StoreName):              mStoreName = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeName):         mHeadOfficeName = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeAddress1):     mHeadOfficeAddress1 = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeAddress2):     mHeadOfficeAddress2 = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeAddress3):     mHeadOfficeAddress3 = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeAddress4):     mHeadOfficeAddress4 = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeAddress5):     mHeadOfficeAddress5 = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficePostCode):     mHeadOfficePostCode = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficePhoneNo):      mHeadOfficePhoneNo = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HeadOfficeFaxNumber):    mHeadOfficeFaxNumber = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_VATNumber):              mVATNumber = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_MasterOutletNumber):     mMasterOutletNumber = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_HighestValidFunction):   mHighestValidFunction = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_MultipleSignOns):        mMultipleSignOns = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_PasswordValidFor):       mPasswordValidFor = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_AuthorisationValidFor):  mAuthorisationValidFor = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_TrackStockOnHand):       mTrackStockOnHand = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_UseOtherInvoiceAddress): mUseOtherInvoiceAddress = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_CostFigureAccessLevel):  mCostFigureAccessLevel = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_AddressStyle):           mAddressStyle = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_PICDayCode):             mPICDayCode = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_PICUseHandhelds):        mPICUseHandhelds = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_PICLastNoOfItems):       mPICLastNoOfItems = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_PICSampleFrequency):     mPICSampleFrequency = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_AutoApplyPriceChanges):  mAutoApplyPriceChanges = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_AutoApplyNoDays):        mAutoApplyNoDays = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_AutoPrintShelfLabels):   mAutoPrintShelfLabels = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_BulkUpdates):            mBulkUpdates = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_LocalPricingAllowed):    mLocalPricingAllowed = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_CardType):               mCardType = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_CompanyGroup):           mCompanyGroup = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_TillLocalDrive):         mTillLocalDrive = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_TillRemoteDrive):        mTillRemoteDrive = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_TillPathName):           mTillPathName = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_NoCopiesPricingDocs):    mNoCopiesPricingDocs = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_StoreMaintMinShelf):     mStoreMaintMinShelf = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_StoreMaintMaxShelf):     mStoreMaintMaxShelf = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_MaximumShelfMultiplier): mMaximumShelfMultiplier = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_AllowReceiptMaint):      mAllowReceiptMaint = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_ReceiptMaintGrace):      mReceiptMaintGrace = oField.ValueAsVariant
            Case (FID_SYSTEMOPTIONS_RunHierarchyLinkBuild):  mRunHiearchyLinkBuild = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Public Function Interface(Optional eInterfaceType As Long) As cSystemOptions
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function


Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SYSTEMOPTIONS_END_OF_STATIC

End Function



