VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   5715
   ClientLeft      =   1590
   ClientTop       =   1545
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   ScaleHeight     =   5715
   ScaleWidth      =   6585
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

Dim oRoot As Object
Dim oBO   As Object

    Set oRoot = GetRoot
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_CURRENCY)
    Call oBO.Load
    Call oBO.ibo_loadMatches
    Set oBO = Nothing
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_EXCHANGE_RATE)
    Call oBO.Load
    Set oBO = Nothing
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_STORESTATUS)
    Call oBO.IBO_Load
    Set oBO = Nothing
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_ACTIVITYLOG)
    Call oBO.IBO_Load
    Set oBO = Nothing
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oBO.IBO_AddLoadFilter(CMP_EQUAL, FID_STOCKMOVE_PartCode, "123")
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_PARAMETER)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_SYSTEMNO)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_NIGHTLOG)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_NIGHTMASTER)
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_CASHIER)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_STORE)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oBO.IBO_Load
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_NIGHTMASTER)
    Call oBO.IBO_Load
    Set oBO = Nothing

End Sub
