VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cMsgBoxEx"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : cMsgBoxEx
'* Date   : 30/05/03
'* Author : KeithB
'*$Archive: /Projects/OasysV2/VB/Message Box Extended/cMsgBoxEx.cls $
'**********************************************************************************************
'* Summary: Message Box Extended Routine
'* Note:    This class uses frmMsgBoxEx
'**********************************************************************************************
'* $Author: Keithb $
'* $Date: 9/06/03 8:44 $
'* $Revision: 6 $
'* Versions:
'* 30/05/03    KeithB
'*             Header added.
'* 06/06/03(1) KeithB
'*         (A) change tab into 8 spaces
'*         (B) in the form check for button pressed WITHOUT Alt key e.g Y or y for "Yes"
'*         (C) in the form check for escape pressed
'*         (D) make the form height greater to allow for XP display which has taller form caption
'*         (E) width can go up to 75% of screen width
'* 06/06/03(2) KeithB
'*             Stop focus going to the Icon
'*             Set focus to correct default button
'* 06/06/03(3) KeithB
'*         (A) Fix bug when 3rd button is in use
'* 09/06/03    KeithB
'*             Fix vbYesNoCancel - 'OK' botton was being displayed instead of 'Yes'
'*
'</CAMH>***************************************************************************************
Option Explicit

Public mintResponse     As Integer

Const MODULE_NAME       As String = "cMsgBoxEx"

Const BUTTON_TXT_YES    As String = "&Yes"
Const BUTTON_TXT_NO     As String = "&No"
Const BUTTON_TXT_RETRY  As String = "&Retry"
Const BUTTON_TXT_CANCEL As String = "&Cancel"
Const BUTTON_TXT_OK     As String = "&OK"

Public Function MsgBoxEx(ByVal strMessage As String, _
                         Optional ByVal vbButtons As VbMsgBoxStyle = vbOKOnly, _
                         Optional ByVal strTitle As String = vbNullString, _
                         Optional ByVal strButton1Caption As String, _
                         Optional ByVal strButton2Caption As String, _
                         Optional ByVal strButton3Caption As String, _
                         Optional ByVal lngFontSize As Long = 14, _
                         Optional ByVal lngBackColor As Long = vbButtonFace, _
                         Optional ByVal lngHeightRequested As Long = 0, _
                         Optional ByVal lngWidthRequested As Long = 0) As Integer
    
' This function -
'  sets the options requested by the user
'  loads the form frmMsgBoxEx and displays the appropriate text & buttons.
' The response (e.g. vbOk) is returned to the user.

' Note: strButton1Caption, strButton2Caption & strButton3Caption are optional

'       If present they will override the normal button captions.
'       The response MUST be the same as for a normal message box
'       i.e. for vbOKCancel the default buttons are 'O.K' and 'Cancel'
'       Do NOT set strButton1Caption to 'Cancel It' and strButton2Caption to 'Accept'
'       as this would reverse the expected response codes!

' Note: lngHeightRequested is optional. If 0 the display will 'autosize'.
'       If it is too small, the program will override it.
'       It is recommended that this parameter is normally left at 0.

Dim frmMsgBox        As New frmMsgBoxEx

Dim intCmdButton     As Integer
Dim intNoOfButtons   As Integer
Dim intIconNumber    As Integer
Dim intDefaultButton As Integer
Dim intMessPosition  As Integer
Dim intButton1Code   As Integer
Dim intButton2Code   As Integer
Dim intButton3Code   As Integer
Dim lngSpaceDown     As Long
Dim lngTopLab        As Long
Dim lngTopButton     As Long
Dim lngFormHeight    As Long
Dim lngButtonSpace   As Long
Dim lngButtonLeft    As Long
Dim lngLabelLeft     As Long
Dim lngWidthDerived  As Long
Dim lngMaxLen        As Long
Dim strMessDisplay   As String


    On Error GoTo MsgBoxEx_Err
    
    If lngFontSize = 0 Then
        lngFontSize = 14
    End If
    
    If lngBackColor = 0 Then
        lngBackColor = vbButtonFace
    End If
     
    ' set no of buttons, button captions and response codes
    Call SetButtons(vbButtons, intNoOfButtons, _
                    strButton1Caption, strButton2Caption, strButton3Caption, _
                    intButton1Code, intButton2Code, intButton3Code)
                        
    ' see if an icon should be displayed
    intIconNumber = SetIcon(vbButtons)
    
    ' Get the default button
    intDefaultButton = SetDefaultButton(vbButtons)
    
    If intDefaultButton > intNoOfButtons Then
        intDefaultButton = intNoOfButtons
    End If
    
    Load frmMsgBox
    
    lngButtonSpace = 0
    For intCmdButton = 0 To intNoOfButtons - 1 Step 1
        ' accumulate the button widths
        lngButtonSpace = lngButtonSpace + frmMsgBox.cmdMsgBox(intCmdButton).Width
    Next intCmdButton

    
    ' put the message into the label box
    ' use a special routine to govern "AutoSize"
    ' NOTE: this may put extra carraige returns into the string
    
    ' first set the font size for the message
    frmMsgBox.lblMsgBox.FontSize = lngFontSize
    
    strMessDisplay = strMessage
    intMessPosition = 0
    
    ' ensure any requested width is reasonable
    If lngWidthRequested > 0 Then
        If lngWidthRequested > (Screen.Width * 0.75) Then
            lngWidthRequested = Screen.Width * 0.75
        End If
        If lngWidthRequested < (Screen.Width * 0.4) Then
            lngWidthRequested = Screen.Width * 0.4
        End If
    End If
    
    
    If lngWidthRequested > 0 Then
        ' user has requested a specific width
        '   label is % of requested width
        lngMaxLen = lngWidthRequested * 0.7
        lngMaxLen = lngWidthRequested - (frmMsgBox.picMsgIcon(0).Width * 2)
    Else
        ' no specific width requested
        '   we can use a width for the label up to 75% of screen
        lngMaxLen = (Screen.Width * 0.75) - (frmMsgBox.picMsgIcon(0).Width * 2)
    End If
    
    
    ' fill the autosizing box
    ' adjust the display up to max width lngMaxLen
    
    Do
    
        Call FillLabel(frmMsgBox.lblMsgBox, lngMaxLen, _
                           strMessDisplay, intMessPosition)
        
    Loop Until intMessPosition = -1
    
    
    lngWidthDerived = frmMsgBox.lblMsgBox.Width + _
                     (frmMsgBox.picMsgIcon(0).Width * 2)
                     
    ' width can not be less than that required by all the buttons
    ' plus a bit of extra space (extra = one button width)
    If lngWidthDerived < lngButtonSpace + frmMsgBox.cmdMsgBox(0).Width Then
        lngWidthDerived = lngButtonSpace + frmMsgBox.cmdMsgBox(0).Width
    End If
    
    ' width can not exceed the screen width!
    If lngWidthDerived > Screen.Width Then
        lngWidthDerived = Screen.Width
    End If
    
    frmMsgBox.Width = lngWidthDerived
    
    Call frmMsgBox.lblMsgBox.Refresh
    
    lngSpaceDown = frmMsgBox.cmdMsgBox(0).Height * 0.3
    
    lngTopLab = lngSpaceDown
    lngTopButton = (lngSpaceDown * 2) + frmMsgBox.lblMsgBox.Height
    ' go down one button width and 3 more spaces
    lngFormHeight = lngTopButton + _
                    frmMsgBox.cmdMsgBox(0).Height + _
                    (lngSpaceDown * 3)
                
    ' set the form height
    If lngFormHeight > lngHeightRequested Then
        frmMsgBox.Height = lngFormHeight
    Else
        frmMsgBox.Height = lngHeightRequested
    End If
    
    ' set top positions here
    frmMsgBox.lblMsgBox.Top = lngTopLab
    For intCmdButton = 0 To 2 Step 1
        frmMsgBox.cmdMsgBox(intCmdButton).Top = lngTopButton
    Next intCmdButton
    
    
    frmMsgBox.BackColor = lngBackColor
    frmMsgBox.Caption = strTitle
    
    If intIconNumber > 0 Then
        ' position & show the icon
        frmMsgBox.picMsgIcon(intIconNumber - 1).Left = 15
        ' put it 1/3rd down the form
        frmMsgBox.picMsgIcon(intIconNumber - 1).Top = frmMsgBox.Height * 0.2
        frmMsgBox.picMsgIcon(intIconNumber - 1).Visible = True
        ' ensure correct background color
        frmMsgBox.picMsgIcon(intIconNumber - 1).BackColor = lngBackColor
    End If
            
    ' position the autosized label
    ' ============================
    ' centre the label in the form
    lngLabelLeft = (frmMsgBox.Width - frmMsgBox.lblMsgBox.Width) / 2
    
    ' then move it right by 1/4 icon width
    If intIconNumber > 0 Then
        lngLabelLeft = lngLabelLeft + _
                       (frmMsgBox.picMsgIcon(intIconNumber - 1).Width / 4)
        ' too far right?
        If lngLabelLeft > frmMsgBox.picMsgIcon(intIconNumber - 1).Width * 2 Then
            ' put it back in the centre
            lngLabelLeft = (frmMsgBox.Width - frmMsgBox.lblMsgBox.Width) / 2
        End If
        
    End If
    
    If lngLabelLeft < 0 Then
        lngLabelLeft = 0
    End If
    
    frmMsgBox.lblMsgBox.Left = lngLabelLeft
    
    ' put captions & tags on buttons
    ' ==============================
    frmMsgBox.cmdMsgBox(0).Caption = strButton1Caption
    frmMsgBox.cmdMsgBox(0).Tag = intButton1Code
    
    frmMsgBox.cmdMsgBox(1).Caption = strButton2Caption
    frmMsgBox.cmdMsgBox(1).Tag = intButton2Code
    
    frmMsgBox.cmdMsgBox(2).Caption = strButton3Caption
    frmMsgBox.cmdMsgBox(2).Tag = intButton3Code
    
    lngButtonSpace = 0
    For intCmdButton = 0 To intNoOfButtons - 1 Step 1
        ' accumulate the button widths
        lngButtonSpace = lngButtonSpace + frmMsgBox.cmdMsgBox(intCmdButton).Width
    Next intCmdButton
    
    ' equal spaces between the buttons
    lngButtonSpace = (frmMsgBox.Width - lngButtonSpace) / (intNoOfButtons + 1)
    lngButtonLeft = lngButtonSpace
    
    For intCmdButton = 0 To intNoOfButtons - 1 Step 1
    
        frmMsgBox.cmdMsgBox(intCmdButton).Visible = True
        frmMsgBox.cmdMsgBox(intCmdButton).BackColor = lngBackColor
        Call frmMsgBox.cmdMsgBox(intCmdButton).Refresh
        
        frmMsgBox.cmdMsgBox(intCmdButton).Left = lngButtonLeft
        
        lngButtonLeft = lngButtonLeft + lngButtonSpace + _
                        frmMsgBox.cmdMsgBox(intCmdButton).Width
                
    Next intCmdButton
    
    
    ' set mintResponse to -1 in case user cancels
    mintResponse = -1
    
    Call frmMsgBox.Prepare(Me, intDefaultButton)
        
    frmMsgBox.Show vbModal
    
    If mintResponse = -1 Then
        mintResponse = vbCancel
    End If
    
MsgBoxEx_Done:
    On Error Resume Next
    Set frmMsgBox = Nothing
    
    MsgBoxEx = mintResponse
    
    Exit Function
    
MsgBoxEx_Err:
    strMessDisplay = "Error number " & Err.Number & vbCrLf & _
                     Err.Description
    Call MsgBox(strMessDisplay, vbCritical, "Error in routine MsgBoxEx")
    mintResponse = vbCancel
    Resume MsgBoxEx_Done
    
End Function

Friend Sub ReportSelection(ByVal intResponse As Integer)
    
    'Debug.Print intResponse
    
    mintResponse = intResponse
    
End Sub

Private Sub SetButtons(ByVal vbButtons As VbMsgBoxStyle, _
                       ByRef intNoOfButtons As Integer, _
                       ByRef strButton1Caption As String, _
                       ByRef strButton2Caption As String, _
                       ByRef strButton3Caption As String, _
                       ByRef intButton1Code As Integer, _
                       ByRef intButton2Code As Integer, _
                       ByRef intButton3Code As Integer)
                           
' This routine sets -
'       - no of buttons
'       - button captions
' and   - response codes
    
    intNoOfButtons = 0

    ' vbRetryCancel = 5
    If ButtonOption(vbButtons, vbRetryCancel) Then
        intNoOfButtons = 2
        intButton1Code = vbRetry
        intButton2Code = vbCancel
        If LenB(strButton1Caption) = 0 Then
            strButton1Caption = BUTTON_TXT_RETRY
        End If
        If LenB(strButton2Caption) = 0 Then
            strButton2Caption = BUTTON_TXT_CANCEL
        End If
        Exit Sub
    End If
    
    
    ' vbYesNo = 4
    If ButtonOption(vbButtons, vbYesNo) Then
        intNoOfButtons = 2
        intButton1Code = vbYes
        intButton2Code = vbNo
        If LenB(strButton1Caption) = 0 Then
            strButton1Caption = BUTTON_TXT_YES
        End If
        If LenB(strButton2Caption) = 0 Then
            strButton2Caption = BUTTON_TXT_NO
        End If
        Exit Sub
    End If

    
    ' vbYesNoCancel = 3
    If ButtonOption(vbButtons, vbYesNoCancel) Then
        intNoOfButtons = 3
        intButton1Code = vbYes
        intButton2Code = vbNo
        intButton3Code = vbCancel
        If LenB(strButton1Caption) = 0 Then
            strButton1Caption = BUTTON_TXT_YES
        End If
        If LenB(strButton2Caption) = 0 Then
            strButton2Caption = BUTTON_TXT_NO
        End If
        If LenB(strButton3Caption) = 0 Then
            strButton3Caption = BUTTON_TXT_CANCEL
        End If
        Exit Sub
    End If
    
    ' vbOKCancel = 2
    If ButtonOption(vbButtons, vbOKCancel) Then
        intNoOfButtons = 2
        intButton1Code = vbOK
        intButton2Code = vbCancel
        If LenB(strButton1Caption) = 0 Then
            strButton1Caption = BUTTON_TXT_OK
        End If
        If LenB(strButton2Caption) = 0 Then
            strButton2Caption = BUTTON_TXT_CANCEL
        End If
        Exit Sub
    End If
        
    ' vbOkOnly = 0
    intNoOfButtons = 1
    intButton1Code = vbOK
    If LenB(strButton1Caption) = 0 Then
        strButton1Caption = BUTTON_TXT_OK
    End If
    
End Sub

Private Function ButtonOption(ByVal vbButtons As VbMsgBoxStyle, _
                              ByVal vbButtonOption As VbMsgBoxStyle) As Boolean
    
    If (vbButtons And vbButtonOption) = vbButtonOption Then
        ButtonOption = True
    Else
        ButtonOption = False
    End If

End Function

Private Function SetIcon(ByVal vbButtons As VbMsgBoxStyle) As Integer

' Determine if an icon should be displayed

    ' vbInformation = 64
    If ButtonOption(vbButtons, vbInformation) Then
        SetIcon = 4
        Exit Function
    End If

    ' vbExclamation = 48
    If ButtonOption(vbButtons, vbExclamation) Then
        SetIcon = 3
        Exit Function
    End If

    ' vbQuestion = 32
    If ButtonOption(vbButtons, vbQuestion) Then
        SetIcon = 2
        Exit Function
    End If

    ' vbCritical = 16
    If ButtonOption(vbButtons, vbCritical) Then
        SetIcon = 1
        Exit Function
    End If

    ' No Icon required
    SetIcon = 0
    
End Function

Private Function SetDefaultButton(ByVal vbButtons As VbMsgBoxStyle) As Integer

    ' Set the default button

    ' vbDefaultButton4 = 768
    If ButtonOption(vbButtons, vbDefaultButton4) Then
        SetDefaultButton = 4
        Exit Function
    End If
    
    ' vbDefaultButton3 = 512
    If ButtonOption(vbButtons, vbDefaultButton3) Then
        SetDefaultButton = 3
        Exit Function
    End If
    
    ' vbDefaultButton2 = 256
    If ButtonOption(vbButtons, vbDefaultButton2) Then
        SetDefaultButton = 2
        Exit Function
    End If
    
    ' vbDefaultButton1 = 0
    SetDefaultButton = 1

End Function

Private Sub FillLabel(ByRef labAutosizeLabel As Label, _
                      ByVal lngMaxLen As Long, _
                      ByRef strMessageAmended As String, _
                      ByRef intMessPosition As Integer)

' This routine is used to fill the label which 'autosizes'
' Because we do not want to exceed the width of the form
' carraige returns may need inserting into the displayed message.

Dim strCurrentChar As String
Dim strLeft        As String
Dim strRight       As String
Dim intPnt         As Integer
Dim intLen         As Integer

    ' Debug.Print "intMessPosition="; intMessPosition
    ' Debug.Print "Caption "; labAutosizeLabel.Caption

    If intMessPosition = 0 Then
        labAutosizeLabel.Caption = vbNullString
    End If
    
    intMessPosition = intMessPosition + 1
    
    strCurrentChar = Mid$(strMessageAmended, intMessPosition, 1)
    strLeft = vbNullString
    strRight = vbNullString
    
    If strCurrentChar = vbTab Then
        ' change tab into 8 spaces
        intLen = Len(strMessageAmended)
        intPnt = intMessPosition - 1
        If intPnt > 0 Then
            strLeft = Left$(strMessageAmended, intPnt)
        End If
        
        If intMessPosition < intLen Then
            strRight = Right$(strMessageAmended, intLen - intMessPosition)
        End If
        strMessageAmended = strLeft & Space$(8) & strRight
        ' now the current character is a space
        strCurrentChar = " "
    End If
    
    
    
    
    labAutosizeLabel.Caption = labAutosizeLabel.Caption + strCurrentChar
    intPnt = 0
    
    If labAutosizeLabel.Width > lngMaxLen Then
        ' is the current character a space?
        Select Case strCurrentChar
        Case Is = " "
            ' it is - so replace it
            Mid$(strMessageAmended, intMessPosition, 1) = vbCrLf
        Case Else
            ' Not a space - move back to previous space
            intPnt = intMessPosition
            Do
              intPnt = intPnt - 1
              strCurrentChar = Mid$(strMessageAmended, intPnt, 1)
            Loop Until intPnt = 1 Or strCurrentChar = " "
            
            If intPnt > 1 Then
              Mid$(strMessageAmended, intPnt, 1) = vbCrLf
            Else
              ' unable to find a space
              Mid$(strMessageAmended, intMessPosition, 1) = vbCrLf
            End If
        End Select
        intMessPosition = 0 ' flag need to start again !
    End If
    
    ' have we reached the end of strMessageAmended ?
    If intMessPosition > 0 Then
        If intMessPosition = Len(strMessageAmended) Then
            intMessPosition = -1 ' flag finished
        End If
    End If

End Sub
