VERSION 5.00
Begin VB.Form frmtest 
   Caption         =   "frmTest - Test for MsgBoxEx"
   ClientHeight    =   4980
   ClientLeft      =   1770
   ClientTop       =   1965
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   ScaleHeight     =   4980
   ScaleWidth      =   6975
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   495
      Left            =   2880
      TabIndex        =   1
      Top             =   3240
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Test"
      Height          =   495
      Left            =   2880
      TabIndex        =   0
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label labResponse 
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   2400
      Width           =   2055
   End
End
Attribute VB_Name = "frmtest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmtest"

Private Sub Command1_Click()

Dim intResponse As Integer
Dim strMess As String
    
    strMess = "This is a test" & vbCrLf & _
              "AB" & vbTab & "BC" & vbCrLf & _
              "ab" & vbTab & "bc" & vbCrLf & _
              "and this is a rather long line that will probably overflow 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 because it goes on 1 2 3 4 5 6 7 8 9 for such a long long way."
    
    'strMess = "This is a test" & vbCrLf & "With normal 2nd line of characters!"
    'strMess = "This is a test"
    'strMess = "try again"
    
    ' + vbInformation + vbDefaultButton2
    
    ' Call MsgBox(strMess, vbYesNoCancel)
    
    ' intResponse = MsgBoxEx(strMess)
    
    intResponse = MsgBoxEx(strMess, _
                           vbYesNoCancel + vbQuestion, _
                           "Testing MsgBoxEx with a long title", _
                           "", "", "", 0)
    
    labResponse.Caption = "Response " & intResponse
    
End Sub

Private Sub Command2_Click()

Dim intResponse As Integer
    
    intResponse = MsgBox("Command 2", vbOKOnly)
    'Debug.Print intResponse
        
    intResponse = MsgBox("Command 2", vbOKCancel)
    'Debug.Print intResponse
        
    intResponse = MsgBox("Command 2", vbYesNoCancel)
    
    intResponse = MsgBox("Command 2", vbYesNo)
    
    intResponse = MsgBox("Command 2", vbRetryCancel)
    
    Call MsgBox("Command 2", vbCritical)
    Call MsgBox("Command 2", vbQuestion)
    Call MsgBox("Command 2", vbExclamation)
    Call MsgBox("Command 2", vbInformation)
    
    'Debug.Print vbDefaultButton1, vbDefaultButton2, vbDefaultButton3, vbDefaultButton4
End Sub

