VERSION 5.00
Begin VB.Form frmMsgBoxEx 
   BackColor       =   &H00C0C0FF&
   Caption         =   "frmMsgBoxEx"
   ClientHeight    =   4020
   ClientLeft      =   1785
   ClientTop       =   3390
   ClientWidth     =   6210
   Icon            =   "frmMsgBoxEx.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   6210
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picMsgIcon 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   495
      Index           =   3
      Left            =   120
      Picture         =   "frmMsgBoxEx.frx":0A96
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   6
      ToolTipText     =   "V"
      Top             =   2040
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.PictureBox picMsgIcon 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   495
      Index           =   2
      Left            =   120
      Picture         =   "frmMsgBoxEx.frx":1093
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   5
      ToolTipText     =   "V"
      Top             =   1320
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.PictureBox picMsgIcon 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   495
      Index           =   1
      Left            =   120
      Picture         =   "frmMsgBoxEx.frx":149C
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   4
      ToolTipText     =   "V"
      Top             =   720
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.PictureBox picMsgIcon 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   495
      Index           =   0
      Left            =   120
      Picture         =   "frmMsgBoxEx.frx":18C0
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   3
      ToolTipText     =   "V"
      Top             =   120
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton cmdMsgBox 
      Caption         =   "cmdMsgBox(2)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   2
      Left            =   3960
      TabIndex        =   2
      Top             =   2040
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CommandButton cmdMsgBox 
      Caption         =   "cmdMsgBox(1)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   1
      Left            =   2280
      TabIndex        =   1
      Top             =   2040
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CommandButton cmdMsgBox 
      BackColor       =   &H00C0C0FF&
      Caption         =   "cmdMsgBox(0)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   0
      Left            =   720
      MaskColor       =   &H0000FF00&
      TabIndex        =   0
      Top             =   2040
      UseMaskColor    =   -1  'True
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.Label lblMsgBox 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "lblMsgBox"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1680
      TabIndex        =   7
      Top             =   1080
      Width           =   1470
   End
End
Attribute VB_Name = "frmMsgBoxEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module: frmMsgBoxEx
'* Date  : 30/05/03
'* Author: KeithB
'**********************************************************************************************
'* Summary: Form that displays the message on screen
'**********************************************************************************************
'* Versions:
'* 30/05/03    KeithB
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME          As String = "frmMsgBoxEx"
Private moMsgBoxEx         As cMsgBoxEx
Private mintDefaultButton  As Integer
Private mstrButtonPress(2) As String

Private Sub cmdMsgBox_Click(Index As Integer)
    
    'Debug.Print Val(cmdMsgBox(Index).Tag)
    
    ' return the response to the Class Module
    Call moMsgBoxEx.ReportSelection(Val(cmdMsgBox(Index).Tag))
    
    Unload Me
    
End Sub

Public Sub Prepare(ByRef oMenuObj As cMsgBoxEx, ByVal intDefaultButton As Integer)

    ' reference to the Class Module
    Set moMsgBoxEx = oMenuObj
    
    mintDefaultButton = intDefaultButton
    
End Sub

Private Sub Form_Activate()

Dim intMsgBox         As Integer
Dim intPnt            As Integer
Dim intLen            As Integer
Dim strCaption        As String
Dim strNoUnderLine(2) As String
Dim strLettersFound   As String

    ' store the underlined letter from cmdMsgBox().Caption
    
    For intMsgBox = 0 To 2 Step 1
    
        mstrButtonPress(intMsgBox) = vbNullString
        
        If cmdMsgBox(intMsgBox).Visible = True Then
            strCaption = cmdMsgBox(intMsgBox).Caption & " "
            intLen = Len(strCaption)
            If intLen > 2 Then
                intPnt = InStr(1, strCaption, "&")
                If intPnt > 0 Then
                    mstrButtonPress(intMsgBox) = UCase$(Mid$(strCaption, intPnt + 1, 1))
                    strLettersFound = strLettersFound & mstrButtonPress(intMsgBox)
                End If
            End If
        
            If LenB(mstrButtonPress(intMsgBox)) = 0 Then
                strNoUnderLine(intMsgBox) = UCase$(Left$(strCaption, 1))
            End If
        
        End If
        
    Next intMsgBox
            
            
    ' if no underlined letter, use first letter in the string
    '   provided it is not already in use!
    For intMsgBox = 0 To 2 Step 1
        If (LenB(mstrButtonPress(intMsgBox)) = 0) And (LenB(strNoUnderLine(intMsgBox)) <> 0) Then
            If InStr(strLettersFound, strNoUnderLine(intMsgBox)) = 0 Then
                mstrButtonPress(intMsgBox) = strNoUnderLine(intMsgBox)
                strLettersFound = strLettersFound & mstrButtonPress(intMsgBox)
                ' show the caption as underlined
                cmdMsgBox(intMsgBox).Caption = "&" & cmdMsgBox(intMsgBox).Caption
            End If
        End If
    Next intMsgBox
    
    ' is there a default button?
    On Error Resume Next
    If mintDefaultButton > 0 Then
        cmdMsgBox(mintDefaultButton - 1).SetFocus
    End If
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

Dim strChar   As String
Dim intMsgBox As Integer

    On Error Resume Next
    
    If KeyAscii = vbKeyEscape Then
        Unload Me
        Exit Sub
    End If
    
    strChar = UCase$(Chr$(KeyAscii))
    
    For intMsgBox = 0 To 2
        If strChar = mstrButtonPress(intMsgBox) Then
            Call cmdMsgBox_Click(intMsgBox)
            Exit For
        End If
    Next intMsgBox
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Debug.Print "frmMsgBoxEx Form_Unload"
End Sub

Private Sub lblMsgBox_DblClick()

    lblMsgBox.ToolTipText = "MessageBoxEx Ver." & App.Major & "." & App.Minor & "." & App.Revision
    ' Debug.Print "lblMsgBox_DblClick"
    
End Sub
