VERSION 5.00
Begin VB.Form frmTOSEditorAdd 
   Caption         =   "Add New Type Of Sale"
   ClientHeight    =   1920
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4290
   LinkTopic       =   "Form1"
   ScaleHeight     =   1920
   ScaleWidth      =   4290
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   1440
      Width           =   1215
   End
   Begin VB.PictureBox pbxOne 
      BorderStyle     =   0  'None
      Height          =   1095
      Left            =   120
      ScaleHeight     =   1095
      ScaleWidth      =   3975
      TabIndex        =   4
      Top             =   120
      Width           =   3975
      Begin VB.TextBox txtTOSDSeq 
         Height          =   285
         Left            =   1440
         TabIndex        =   0
         Top             =   120
         Width           =   855
      End
      Begin VB.ComboBox cmbTOSCodes 
         Height          =   315
         Left            =   1440
         TabIndex        =   1
         Top             =   600
         Width           =   2415
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Type of sale code"
         Height          =   375
         Left            =   0
         TabIndex        =   6
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Display sequence"
         Height          =   375
         Left            =   0
         TabIndex        =   5
         Top             =   120
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmTOSEditorAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private mOTOSOption     As cTypesOfSaleOptions
Private mBlnRecordReady As Boolean
Private mStrDefaultDSEQ As String

' Main add function
Public Function UserEnterNewRecord(ByRef oRetTOSOption As cTypesOfSaleOptions) As Boolean
    
    Dim oCtrl As Control
    
    ' setup the dynamic user entries
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    pbxOne.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    
    For Each oCtrl In Me.Controls
    If TypeName(oCtrl) = "CommandButton" Then
        oCtrl.BackColor = Me.BackColor
    End If
    Next oCtrl

    txtTOSDSeq.Text = mStrDefaultDSEQ
    Call frmTOSEditor.LoadTOSCodesComboBox(cmbTOSCodes)
    cmbTOSCodes.ListIndex = 0
    
    Set mOTOSOption = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALEOPTIONS)
    
    ' get the new entries
    
    KeyPreview = True '       F keys for buttons

    Call Me.Show(vbModal)
            
    Set oRetTOSOption = mOTOSOption
    Set mOTOSOption = Nothing
    
    UserEnterNewRecord = mBlnRecordReady
    
End Function

Private Sub cmdAdd_Click()

    txtTOSDSeq.Text = TOSDSeqValidate(txtTOSDSeq.Text)
    If Len(txtTOSDSeq.Text) = 0 Then
        MsgBox "Invalid Display Sequence, please enter a number from 01 to 97"
        txtTOSDSeq.Text = mStrDefaultDSEQ
        Call txtTOSDSeq.SetFocus
        Exit Sub
    End If
    If frmTOSEditor.GetTOSOptionIndex(txtTOSDSeq.Text) >= 0 Then
        MsgBox "This Display Sequence already exists, please select another one"
        Call txtTOSDSeq.SetFocus
        Exit Sub
    
    End If
    
    mOTOSOption.DisplaySeq = txtTOSDSeq.Text
    mOTOSOption.Code = Left$(cmbTOSCodes.Text, 2)
    mOTOSOption.Description = Mid$(cmbTOSCodes.Text, 3)
    
    mBlnRecordReady = True
    Me.Hide
    Exit Sub
    
End Sub

Private Sub cmdCancel_Click()
    
    mBlnRecordReady = False
    Me.Hide

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    Dim cCtrl           As Control
    Dim cCtrlNew        As Control
    Dim lTmp            As Long
    Dim lTabCurrent     As Long
    Dim lTabClosest     As Long
    Dim lTabDistance    As Long
    
    'On Error GoTo LError:

    Select Case KeyAscii
        ' Make enter like tab
        Case vbKeyReturn:
            lTabCurrent = Me.ActiveControl.TabIndex
            lTabClosest = 0
            lTabDistance = 9999999
            Set cCtrlNew = txtTOSDSeq
            
            For Each cCtrl In Me.Controls
       
                If TypeName(cCtrl) = "CheckBox" _
                Or TypeName(cCtrl) = "TextBox" _
                Or TypeName(cCtrl) = "ComboBox" _
                And cCtrl.Enabled = True And cCtrl.Visible = True And cCtrl.TabIndex <> lTabCurrent Then
                    lTmp = cCtrl.TabIndex - lTabCurrent
                    If lTmp > 0 And lTmp < lTabDistance Then
                        lTabDistance = lTmp
                        lTabClosest = cCtrl.TabIndex
                        Set cCtrlNew = cCtrl
                    End If
                End If
       
            Next cCtrl
                    
            Call cCtrlNew.SetFocus
        'Case vbKeyF8:
        '    Call cmdDelete_Click
        'Case vbKeyF10:
        '    Call cmdExit_Click
    End Select
    
    If Me.ActiveControl = cmbTOSCodes Then
        If KeyAscii > 31 Or KeyAscii = vbKeyBack Then KeyAscii = 0
        cmbTOSCodes.SelStart = 0
        cmbTOSCodes.SelLength = 0
    End If
        
End Sub

Public Function TOSDSeqValidate(ByVal DSEQ As String) As String
        
    If Len(DSEQ) = 1 Then DSEQ = "0" & DSEQ
    If Len(DSEQ) > 2 Then DSEQ = vbNullString
    If DSEQ Like "[0-9][0-9]" = False Then DSEQ = vbNullString
    If DSEQ > "97" Then DSEQ = vbNullString
    
    TOSDSeqValidate = DSEQ

End Function

Private Sub Form_Load()
    Me.Icon = frmTOSEditor.Icon
End Sub
