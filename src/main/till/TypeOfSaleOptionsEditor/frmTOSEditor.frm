VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTOSEditor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Type Of Sale Options Editor"
   ClientHeight    =   7560
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   5025
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   1320
      TabIndex        =   27
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "F8-Delete"
      Height          =   375
      Left            =   2640
      TabIndex        =   28
      Top             =   6720
      Width           =   1095
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   3840
      TabIndex        =   29
      Top             =   6720
      Width           =   1095
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "Update"
      Height          =   375
      Left            =   120
      TabIndex        =   26
      Top             =   6720
      Width           =   1095
   End
   Begin VB.PictureBox pbxTenders 
      BorderStyle     =   0  'None
      Height          =   3615
      Left            =   120
      ScaleHeight     =   3615
      ScaleWidth      =   4815
      TabIndex        =   45
      TabStop         =   0   'False
      Top             =   3000
      Width           =   4815
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   25
         Left            =   1200
         TabIndex        =   41
         Top             =   1800
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   24
         Left            =   960
         TabIndex        =   40
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   23
         Left            =   600
         TabIndex        =   39
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   22
         Left            =   480
         TabIndex        =   38
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   21
         Left            =   600
         TabIndex        =   37
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   20
         Left            =   720
         TabIndex        =   36
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   19
         Left            =   480
         TabIndex        =   35
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   18
         Left            =   600
         TabIndex        =   34
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   17
         Left            =   600
         TabIndex        =   33
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   16
         Left            =   600
         TabIndex        =   32
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   15
         Left            =   600
         TabIndex        =   30
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   14
         Left            =   720
         TabIndex        =   25
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   13
         Left            =   840
         TabIndex        =   24
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   12
         Left            =   840
         TabIndex        =   23
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   11
         Left            =   960
         TabIndex        =   22
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   10
         Left            =   1080
         TabIndex        =   21
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   9
         Left            =   600
         TabIndex        =   20
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   8
         Left            =   720
         TabIndex        =   19
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   7
         Left            =   840
         TabIndex        =   18
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   6
         Left            =   720
         TabIndex        =   17
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   5
         Left            =   840
         TabIndex        =   16
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   4
         Left            =   600
         TabIndex        =   15
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   1560
         Width           =   975
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   2
         Left            =   480
         TabIndex        =   13
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   375
         Index           =   1
         Left            =   960
         TabIndex        =   12
         Top             =   1320
         Width           =   1095
      End
      Begin VB.CheckBox chkTenderType 
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   480
         Width           =   2415
      End
      Begin VB.Label lblTenderHeader 
         BackStyle       =   0  'Transparent
         Caption         =   "Tender Types Allowed:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         Height          =   15
         Left            =   2400
         TabIndex        =   10
         Top             =   960
         Width           =   375
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Label3"
         Height          =   135
         Left            =   240
         TabIndex        =   9
         Top             =   120
         Width           =   15
      End
   End
   Begin VB.PictureBox pbxTOSInfo 
      BorderStyle     =   0  'None
      Height          =   2775
      Left            =   120
      ScaleHeight     =   2775
      ScaleWidth      =   4815
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   120
      Width           =   4815
      Begin VB.CheckBox chkSupervisorOnly 
         Caption         =   "Supervisor Only"
         Height          =   255
         Left            =   1440
         TabIndex        =   7
         Top             =   2340
         Width           =   3195
      End
      Begin VB.ComboBox cmbTOSCodes 
         Height          =   315
         Left            =   1440
         TabIndex        =   2
         Top             =   810
         Width           =   2655
      End
      Begin VB.ComboBox cmbTOSDescription 
         Height          =   315
         Left            =   2040
         TabIndex        =   1
         Top             =   240
         Width           =   2055
      End
      Begin VB.ComboBox cmbTOSDSeq 
         Height          =   315
         ItemData        =   "frmTOSEditor.frx":0000
         Left            =   1440
         List            =   "frmTOSEditor.frx":0002
         TabIndex        =   0
         Top             =   240
         Width           =   615
      End
      Begin VB.CheckBox chkSpecialTender 
         Caption         =   "Special account tender allowed"
         Height          =   255
         Left            =   1440
         TabIndex        =   6
         Top             =   2100
         Width           =   2775
      End
      Begin VB.CheckBox chkZeroTransOpen 
         Caption         =   "Open drawer if zero transaction"
         Height          =   255
         Left            =   1440
         TabIndex        =   5
         Top             =   1860
         Width           =   2775
      End
      Begin VB.CheckBox chkSpecialPrt 
         Caption         =   "Special print"
         Height          =   255
         Left            =   1440
         TabIndex        =   4
         Top             =   1620
         Width           =   2775
      End
      Begin VB.CheckBox chkAskForRef 
         Caption         =   "Ask for document reference"
         Height          =   255
         Left            =   1440
         TabIndex        =   3
         Top             =   1380
         Width           =   2775
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Display sequence"
         Height          =   255
         Left            =   0
         TabIndex        =   42
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Type of sale code"
         Height          =   255
         Left            =   0
         TabIndex        =   43
         Top             =   810
         Width           =   1335
      End
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   31
      Top             =   7185
      Width           =   5025
      _ExtentX        =   8864
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmTOSEditor.frx":0004
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "21:39"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmTOSEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public colTOSControls       As Collection   ' (TOSCTL)
Public colTOSOptions        As Collection   ' (TOSOPT)
Public colTOSTenderLinks    As Collection   ' (TOSTEN)
Public colTenderControls    As Collection   ' (TENCTL)
Public colTenderOptions     As Collection   ' (TENOPT)

Private mbInitialized           As Boolean
Private mTOSOptionLastDSEQ      As String

' Pick TOS Option from DSEQ list
Private Sub cmbTOSDSeq_click()

    Call LoadTOSOption(cmbTOSDSeq.ListIndex)

End Sub
' Pick option from TOS Option description list
Private Sub cmbTOSDescription_click()

    If LoadTOSOption(cmbTOSDescription.ListIndex) = False Then cmbTOSDSeq.SetFocus

End Sub

Private Sub cmbTOSDSeq_GotFocus()

    cmbTOSDSeq.SelStart = 0
    cmbTOSDSeq.SelLength = 0

End Sub

Private Sub cmdAdd_Click()

    Dim oNewEntry   As cTypesOfSaleOptions
    Dim lIndex      As Long
    
    Set oNewEntry = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALEOPTIONS)
    
    Load frmTOSEditorAdd
    If frmTOSEditorAdd.UserEnterNewRecord(oNewEntry) = True Then
        
        If oNewEntry.SaveIfNew = False Then
            MsgBox "Error Updating TOS Options Table"
        Else
            Call TOSEditorLoadBO ' Reload collection for new entry
   
            lIndex = GetTOSOptionIndex(oNewEntry.DisplaySeq)
            Call LoadTOSOption(lIndex) ' Reload tables
        End If
    End If

    Unload frmTOSEditorAdd
    
    If cmbTOSDSeq.ListCount = 0 Then End
   
End Sub

Private Sub cmdDelete_Click()
    
    Dim oTOSOption      As cTypesOfSaleOptions
    Dim oTenderOption   As cTenderOptions
    Dim lIndex          As Long
    Dim lSaveIndex      As Long
    Dim strTmp          As String
    
    Set oTOSOption = colTOSOptions(cmbTOSDSeq.ListIndex + 1)
    
    If Not MsgBox("Delete  -""" & oTOSOption.DisplaySeq & " - " & oTOSOption.Description & """ (Y/N)?", vbYesNoCancel) = vbYes Then
        Exit Sub
    End If
        
    If oTOSOption.Delete = False Then
        MsgBox "Error deleting TOS entry"
    End If
    
    lSaveIndex = cmbTOSDSeq.ListIndex
    
    For lIndex = 0 To chkTenderType.UBound
        If chkTenderType(lIndex).Visible = True Then
            If chkTenderType(lIndex).Value = 1 Then
                If DeleteTOSTenderEntry(oTOSOption.DisplaySeq, Left$(chkTenderType(lIndex).Caption, 2)) = False Then
                    MsgBox "Error deleting TOSTEN entry"
                End If
            End If
        End If
    Next lIndex

    Call TOSEditorLoadBO ' Reload collection for removed entry
    
    If colTOSOptions.Count < 1 Then
        Call cmdAdd_Click
    Else
        If lSaveIndex > 0 Then lSaveIndex = lSaveIndex - 1
        Call LoadTOSOption(lSaveIndex) ' Reload tables after delete
    End If
     
End Sub

Private Sub cmdExit_Click()
    
    Unload Me
    
End Sub
' Update the current TOS Option entry
Private Sub cmdUpdate_Click()

    Dim oTOSOption      As cTypesOfSaleOptions
    Dim oTenderOption   As cTenderOptions
    Dim lIndex          As Long
    Dim strTmp          As String
    
    If Len(cmbTOSDescription.Text) = 0 Then
        MsgBox "Please enter a description"
        Exit Sub
    End If
        
    Set oTOSOption = colTOSOptions(cmbTOSDSeq.ListIndex + 1)
    
    oTOSOption.Description = cmbTOSDescription.Text
    oTOSOption.Code = Left$(cmbTOSCodes.Text, 2)
    
    oTOSOption.RequestDocRef = False
    If chkAskForRef.Value = 1 Then oTOSOption.RequestDocRef = True
    
    oTOSOption.SpecialPrint = False
    If chkSpecialPrt.Value = 1 Then oTOSOption.SpecialPrint = True
    
    oTOSOption.AllowSpecialAccTender = False
    If chkSpecialTender.Value = 1 Then oTOSOption.AllowSpecialAccTender = True
    
    oTOSOption.OpenOnZeroTran = False
    If chkZeroTransOpen.Value = 1 Then oTOSOption.OpenOnZeroTran = True
    
    oTOSOption.SupervisorRequired = False
    If chkSupervisorOnly.Value = 1 Then oTOSOption.SupervisorRequired = True
    
    If oTOSOption.SaveIfExists = False Then
        If oTOSOption.SaveIfNew = False Then
            MsgBox "Error Updating TOS Options Table"
        End If
    End If
    
    For lIndex = 0 To chkTenderType.UBound
        If chkTenderType(lIndex).Visible = True Then
            If chkTenderType(lIndex).Value = 1 Then
                Call AddTOSTenderEntry(oTOSOption.DisplaySeq, Left$(chkTenderType(lIndex).Caption, 2))
            Else
                Call DeleteTOSTenderEntry(oTOSOption.DisplaySeq, Left$(chkTenderType(lIndex).Caption, 2))
            End If
        End If
    Next lIndex
    
    Call LoadComboBoxes ' For changes to DSEQ description
    
    End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF8:
            cmdDelete_Click
        Case vbKeyF10:
            cmdExit_Click
    End Select
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    Dim cCtrl           As Control
    Dim cCtrlNew        As Control
    Dim lTmp            As Long
    Dim lTabCurrent     As Long
    Dim lTabClosest     As Long
    Dim lTabDistance    As Long
    
    On Error GoTo LError:

        ' Make enter like tab
    If KeyAscii = vbKeyReturn Then
        lTabCurrent = Me.ActiveControl.TabIndex
        lTabClosest = 0
        lTabDistance = 9999999
        Set cCtrlNew = cmbTOSDSeq
            
        For Each cCtrl In Me.Controls
       
            If TypeName(cCtrl) = "CheckBox" _
            Or TypeName(cCtrl) = "TextBox" _
            Or TypeName(cCtrl) = "ComboBox" _
            And cCtrl.Enabled = True And cCtrl.Visible = True And cCtrl.TabIndex <> lTabCurrent Then
                lTmp = cCtrl.TabIndex - lTabCurrent
                If lTmp > 0 And lTmp < lTabDistance Then
                    lTabDistance = lTmp
                    lTabClosest = cCtrl.TabIndex
                    Set cCtrlNew = cCtrl
                End If
            End If
       
        Next cCtrl
                    
        Call cCtrlNew.SetFocus
    End If
    
    If Me.ActiveControl = cmbTOSCodes Then
        If KeyAscii > 31 Or KeyAscii = vbKeyBack Then KeyAscii = 0
        cmbTOSCodes.SelStart = 0
        cmbTOSCodes.SelLength = 0
    ElseIf Me.ActiveControl = cmbTOSDSeq Then
        If KeyAscii > 31 Or KeyAscii = vbKeyBack Then KeyAscii = 0
        cmbTOSDSeq.SelStart = 0
        cmbTOSDSeq.SelLength = 0
    End If
    
LError:
        
End Sub

Private Sub Form_Load()
    
    GetRoot
    KeyPreview = True '       F keys for buttons
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

    Call TOSEditorLoadBO
    
    Call InitializeFormControls
    mbInitialized = True
    
    If cmbTOSCodes.ListCount = 0 Then
        MsgBox "There are no Types of Sale Controls, please enter them first"
        End
    End If
    If cmbTOSDSeq.ListCount = 0 Then
        Call cmdAdd_Click
        If cmbTOSDSeq.ListCount = 0 Then End
    End If
        
    Call LoadTOSOption(0)

End Sub

Private Sub InitializeFormControls()
    
    Dim oTOSOption      As cTypesOfSaleOptions
    Dim oTOSControl     As cTypesOfSaleControl
    Dim oTenderOption   As cTenderOptions
    Dim lIndex          As Long
    Dim lLeft           As Long
    Dim lTop            As Long
    Dim lBorder         As Long
    Dim oCtrl           As Control
    
    Call InitialiseStatusBar(sbStatus)
    
    pbxTOSInfo.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    pbxTenders.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    
    lBorder = lblTenderHeader.Top
     
    For Each oCtrl In Me.Controls
        If TypeName(oCtrl) = "CommandButton" Then
            oCtrl.BackColor = Me.BackColor
        ElseIf TypeName(oCtrl) = "CheckBox" Then
            oCtrl.BackColor = Me.pbxTenders.BackColor
        End If
    Next oCtrl
    
    ' Init the tender checkboxes
    For lIndex = 0 To chkTenderType.UBound
            chkTenderType(lIndex).Visible = False
            chkTenderType(lIndex).TabIndex = chkTenderType(0).TabIndex + lIndex
    Next lIndex
    
    ' Make a selection for each valid tender type
    lIndex = 0
    lLeft = chkTenderType(0).Left
    lTop = chkTenderType(0).Top
    
    For Each oTenderOption In colTenderOptions
        chkTenderType(lIndex).Caption = oTenderOption.TenderNo & " - " & oTenderOption.Description
        chkTenderType(lIndex).Value = 0
        chkTenderType(lIndex).Top = lTop
        chkTenderType(lIndex).Left = lLeft
        chkTenderType(lIndex).Width = chkTenderType(0).Width
        chkTenderType(lIndex).Height = chkTenderType(0).Height
        chkTenderType(lIndex).Visible = True
        chkTenderType(lIndex).Enabled = True
        lIndex = lIndex + 1
        If lIndex Mod 2 = 0 Then
            lTop = lTop + chkTenderType(0).Height
            lLeft = chkTenderType(0).Left
        Else
            lLeft = lLeft + chkTenderType(0).Width
        End If
    Next oTenderOption
    
    ' Adjust the form for the tender checkboxes
    pbxTenders.Height = lTop + chkTenderType(0).Height + lBorder
    lTop = pbxTOSInfo.Top + pbxTenders.Top + pbxTenders.Height
    
    cmdUpdate.Top = lTop
    cmdAdd.Top = lTop
    cmdDelete.Top = lTop
    cmdExit.Top = lTop
    Me.Height = lTop + 120 * 4 + cmdUpdate.Height + sbStatus.Height + lBorder

End Sub

Public Sub LoadTOSCodesComboBox(ByRef cmb As ComboBox)

    Dim oTOSControl     As cTypesOfSaleControl
    Dim lIndex          As Long
    Dim lSaveTOSCIndex  As Long
    
    lSaveTOSCIndex = cmbTOSCodes.ListIndex
    cmb.Clear
    For Each oTOSControl In colTOSControls
        Call cmb.AddItem(oTOSControl.Code & " - " & oTOSControl.Description, lIndex)
        lIndex = lIndex + 1
    Next oTOSControl
        
    If lSaveTOSCIndex >= 0 Then cmb.ListIndex = lSaveTOSCIndex

End Sub

' Load the type of sale display sequence and description combo boxes
Public Sub LoadComboBoxes()
    
    Dim oTOSOption      As cTypesOfSaleOptions
    Dim lIndex          As Long
    Dim lSaveTOSIndex   As Long
    
    lSaveTOSIndex = cmbTOSDSeq.ListIndex
    
    Call cmbTOSDSeq.Clear
    Call cmbTOSDescription.Clear
    
    For Each oTOSOption In colTOSOptions
        Call cmbTOSDSeq.AddItem(oTOSOption.DisplaySeq, lIndex)
        Call cmbTOSDescription.AddItem(oTOSOption.Description, lIndex)
        lIndex = lIndex + 1
    Next oTOSOption
    
    If lSaveTOSIndex >= cmbTOSDSeq.ListCount Then lSaveTOSIndex = cmbTOSDSeq.ListCount - 1
    If lSaveTOSIndex >= 0 Then
        cmbTOSDSeq.ListIndex = lSaveTOSIndex
        cmbTOSDescription.ListIndex = lSaveTOSIndex
    End If
    
    Call LoadTOSCodesComboBox(cmbTOSCodes)

End Sub
' Loads the tables into collections
Public Sub TOSEditorLoadBO()

    Dim oTOSControl     As cTypesOfSaleControl
    Dim oTOSOption      As cTypesOfSaleOptions
    Dim oTOSTenderLink  As cTypesOfSaleTenders
    Dim oTenderControl  As cTenderControl
    Dim oTenderOption   As cTenderOptions
    
    Dim oBoSortKeys     As OasysDbXfaceCom_Wickes.CBoSortKeys
    Dim oRowSel         As CRowSelector
    
    Dim lIndex As Long
    
    ' Load the Type of Sale Control table, valid for use entries only (TOSCTL)
    Set oTOSControl = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALECONTROL)
    
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_Code
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_Description
    oTOSControl.AddLoadField FID_TYPESOFSALECONTROL_ValidForUse
    oTOSControl.AddLoadFilter CMP_NOTEQUAL, FID_TYPESOFSALECONTROL_ValidForUse, False

    Set colTOSControls = oTOSControl.LoadMatches
     
    ' Load the type of sale options table (TOSOPT) sorted without >98
    Set oRowSel = goDatabase.CreateBoSelector(CLASSID_TYPESOFSALEOPTIONS)
    Set oBoSortKeys = New CBoSortKeys
    Call oBoSortKeys.Add(FID_TYPESOFSALEOPTIONS_DisplaySeq, False)
    Call oRowSel.AddSelectValue(CMP_LESSTHAN, FID_TYPESOFSALEOPTIONS_DisplaySeq, "98")

    Set colTOSOptions = goDatabase.GetSortedBoCollection(oRowSel, oBoSortKeys)
    
    ' Remove the invalid TOS options
    lIndex = 1
    While lIndex < colTOSOptions.Count
        Set oTOSOption = colTOSOptions(lIndex)
        If TOSControlIsValid(oTOSOption.Code) = False Then
            colTOSOptions.Remove (lIndex)
        Else
            lIndex = lIndex + 1
        End If
    Wend
    
    ' Load and sort the Type of Sale (TOS) to Tenders linkage table (TOSTEN)
    Set oRowSel = goDatabase.CreateBoSelector(CLASSID_TYPESOFSALETENDERS)
    Set oBoSortKeys = New CBoSortKeys
    Call oBoSortKeys.Add(FID_TYPESOFSALETENDERS_TenderNo, False)
    Call oBoSortKeys.Add(FID_TYPESOFSALETENDERS_TOSDisplaySeq, False)
    Set colTOSTenderLinks = goDatabase.GetSortedBoCollection(oRowSel, oBoSortKeys)
    
    ' Load the tender controls table
    Set oTenderControl = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
    oTenderControl.AddLoadField FID_TENDERCONTROL_TendType
    oTenderControl.AddLoadField FID_TENDERCONTROL_Description
    oTenderControl.AddLoadField FID_TENDERCONTROL_ValidForUse
    oTenderControl.AddLoadFilter CMP_NOTEQUAL, FID_TENDERCONTROL_ValidForUse, False
    
    Set colTenderControls = oTenderControl.LoadMatches
    
    ' Load the tender options table (TENOPT)
    
    'Set oTenderOption = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_CaptureCreditCard
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_DefaultRemainingAmount
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_Description
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_END_OF_STATIC
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_MaximumAccepted
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_MinimumAccepted
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_OpenCashDrawer
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_OverTenderAllowed
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_PrintChequeBack
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_PrintChequeFront
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_TenderNo
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_TendType
    'oTenderOption.AddLoadField FID_TENDEROPTIONS_UseEFTPOS
    
    Set oRowSel = goDatabase.CreateBoSelector(CLASSID_TENDEROPTIONS)
    Set oBoSortKeys = New CBoSortKeys
    Call oBoSortKeys.Add(FID_TENDEROPTIONS_TenderNo, False)
    Set colTenderOptions = goDatabase.GetSortedBoCollection(oRowSel, oBoSortKeys)

    ' Remove the invalid Tender options
    lIndex = 1
    While lIndex < colTenderOptions.Count
        Set oTenderOption = colTenderOptions(lIndex)
        If TenderControlIsValid(oTenderOption.TendType) = False Then
            colTenderOptions.Remove (lIndex)
        Else
            lIndex = lIndex + 1
        End If
    Wend

    Call LoadComboBoxes
    
End Sub

Private Function TOSControlIsValid(ByVal TOSC As String) As Boolean

    Dim oTOSControl As cTypesOfSaleControl
    
    For Each oTOSControl In colTOSControls
        If oTOSControl.Code = TOSC Then
            TOSControlIsValid = oTOSControl.ValidForUse
            Exit Function
        End If
    Next oTOSControl
    TOSControlIsValid = False
    
End Function
' Gets TOS Control description by TOSC
Private Function GetTOSControlDesc(TOSC As String) As String
    
    Dim oTOSControl As cTypesOfSaleControl
    
    For Each oTOSControl In colTOSControls
        If TOSC = oTOSControl.Code Then
            GetTOSControlDesc = oTOSControl.Code & " - " & oTOSControl.Description
        End If
    Next oTOSControl

End Function
' Gets index to TOS control collection
Private Function GetTOSControlIndex(TOSC As String) As Long
    
    Dim lIndex      As Integer
    Dim oTOSControl As cTypesOfSaleControl

    For Each oTOSControl In colTOSControls
        If oTOSControl.Code = TOSC Then
            GetTOSControlIndex = lIndex
            Exit Function
        End If
        lIndex = lIndex + 1
    Next oTOSControl
    GetTOSControlIndex = -1

End Function
' Gets index to TOS options collection
Public Function GetTOSOptionIndex(DSEQ As String) As Long
    
    Dim lIndex          As Long
    Dim oTOSOption      As cTypesOfSaleOptions

    For Each oTOSOption In colTOSOptions
        If oTOSOption.DisplaySeq = DSEQ Then
            GetTOSOptionIndex = lIndex
            Exit Function
        End If
        lIndex = lIndex + 1
    Next oTOSOption
    GetTOSOptionIndex = -1

End Function

' Gets index to the Tender options collection
Private Function TenderOptionIndex(TEND As String) As Long
    
    Dim lIndex          As Long
    Dim oTenderOption   As cTenderOptions

    For Each oTenderOption In colTenderOptions
        If oTenderOption.TenderNo = TEND Then
            TenderOptionIndex = lIndex
            Exit Function
        End If
        lIndex = lIndex + 1
    Next oTenderOption
    TenderOptionIndex = -1

End Function
' Looks through TOSTEN
Private Function TOSTenderSelectedIndex(ByVal DSEQ As String, ByVal TEND As String) As Long
    
    Dim oTOSTenderLink  As cTypesOfSaleTenders
    Dim lIndex          As Long
    
    For Each oTOSTenderLink In colTOSTenderLinks
        If oTOSTenderLink.TOSDisplaySeq = DSEQ And oTOSTenderLink.TenderNo = TEND Then
            TOSTenderSelectedIndex = lIndex
            Exit Function
        End If
    lIndex = lIndex + 1
    Next oTOSTenderLink
    TOSTenderSelectedIndex = -1
    
End Function
' Looks through TENCTL
Private Function TenderControlIsValid(ByVal TTID As String) As Boolean

    Dim oTenderControl As cTenderControl
    
    For Each oTenderControl In colTenderControls
        If oTenderControl.TendType = TTID Then
            TenderControlIsValid = oTenderControl.ValidForUse
            Exit Function
        End If
    Next oTenderControl
    TenderControlIsValid = False
    
End Function
' Adds records to TOSTEN
Private Function AddTOSTenderEntry(ByVal DSEQ, ByVal TEND) As Boolean
    
    Dim oTOSTEN As cTypesOfSaleTenders
    Dim lTmp    As Long
    
    Set oTOSTEN = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALETENDERS)
    oTOSTEN.TOSDisplaySeq = DSEQ
    oTOSTEN.TenderNo = TEND
    
    AddTOSTenderEntry = oTOSTEN.SaveIfNew
    If AddTOSTenderEntry = False Then AddTOSTenderEntry = oTOSTEN.SaveIfExists
    
    lTmp = TOSTenderSelectedIndex(DSEQ, TEND)
    If lTmp < 0 Then Call colTOSTenderLinks.Add(oTOSTEN)
        
End Function
' Deletes from TOSTEN
Private Function DeleteTOSTenderEntry(ByVal DSEQ, ByVal TEND) As Boolean
    
    Dim oTOSTEN As cTypesOfSaleTenders
    Dim lTmp    As Long
    
    Set oTOSTEN = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALETENDERS)
    oTOSTEN.TOSDisplaySeq = DSEQ
    oTOSTEN.TenderNo = TEND
    
    DeleteTOSTenderEntry = oTOSTEN.Delete
    
    lTmp = TOSTenderSelectedIndex(DSEQ, TEND)
    If lTmp >= 0 Then Call colTOSTenderLinks.Remove(lTmp + 1)
        
End Function

' Load the current TOS option from the table and put it in the form
' Selected from the DSEQ combo box index or text value
Private Function LoadTOSOption(TOSDseqIndex) As Boolean

    Dim oTOSOption      As cTypesOfSaleOptions
    Dim oTenderOption   As cTenderOptions
    Dim lIndex          As Long
    
    LoadTOSOption = False
    
    If mbInitialized = False Then Exit Function
            
    Set oTOSOption = colTOSOptions(TOSDseqIndex + 1)
    cmbTOSDSeq.ListIndex = TOSDseqIndex
    cmbTOSDescription.ListIndex = TOSDseqIndex
    If cmbTOSCodes.ListCount > 0 Then cmbTOSCodes.ListIndex = GetTOSControlIndex(oTOSOption.Code)
    
    chkAskForRef.Value = 0
    If oTOSOption.RequestDocRef = True Then chkAskForRef.Value = 1
    chkSpecialPrt.Value = 0
    If oTOSOption.SpecialPrint = True Then chkSpecialPrt.Value = 1
    chkSpecialTender.Value = 0
    If oTOSOption.AllowSpecialAccTender = True Then chkSpecialTender.Value = 1
    chkZeroTransOpen.Value = 0
    If oTOSOption.OpenOnZeroTran = True Then chkZeroTransOpen.Value = 1
    chkSupervisorOnly.Value = 0
    If oTOSOption.SupervisorRequired = True Then chkSupervisorOnly.Value = 1
    
    For lIndex = 0 To chkTenderType.UBound
        chkTenderType(lIndex).Value = 0
    Next lIndex
    
    For Each oTenderOption In colTenderOptions
        If TOSTenderSelectedIndex(oTOSOption.DisplaySeq, oTenderOption.TenderNo) >= 0 Then
            lIndex = TenderOptionIndex(oTenderOption.TenderNo)
        If lIndex >= 0 Then chkTenderType(lIndex).Value = 1
        End If
    Next oTenderOption
    
    LoadTOSOption = True
    mTOSOptionLastDSEQ = cmbTOSDSeq.Text

End Function

