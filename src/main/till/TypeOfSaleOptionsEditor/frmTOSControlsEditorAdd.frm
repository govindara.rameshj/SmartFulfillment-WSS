VERSION 5.00
Begin VB.Form frmTOSControlsEditorAdd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Add Type Of Sale Control"
   ClientHeight    =   2145
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4005
   Icon            =   "frmTOSControlsEditorAdd.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2145
   ScaleWidth      =   4005
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox pbxOne 
      BorderStyle     =   0  'None
      Height          =   1455
      Left            =   120
      ScaleHeight     =   1455
      ScaleWidth      =   3735
      TabIndex        =   5
      Top             =   120
      Width           =   3735
      Begin VB.TextBox txtDescription 
         Height          =   315
         Left            =   1080
         MaxLength       =   20
         TabIndex        =   1
         Top             =   600
         Width           =   2535
      End
      Begin VB.TextBox txtID 
         Height          =   285
         Left            =   1080
         MaxLength       =   2
         TabIndex        =   0
         Top             =   120
         Width           =   615
      End
      Begin VB.CheckBox chkInUse 
         Height          =   255
         Left            =   1080
         TabIndex        =   2
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Code"
         Height          =   255
         Left            =   0
         TabIndex        =   8
         Top             =   120
         Width           =   960
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         Height          =   375
         Left            =   0
         TabIndex        =   7
         Top             =   660
         Width           =   960
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "In Use"
         Height          =   255
         Left            =   0
         TabIndex        =   6
         Top             =   1080
         Width           =   960
      End
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1680
      Width           =   1215
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2520
      TabIndex        =   4
      Top             =   1680
      Width           =   1335
   End
End
Attribute VB_Name = "frmTOSControlsEditorAdd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmAddTenderType
'* Date   : 11/19/04
'* Author :
'**********************************************************************************************
'* Summary: Allows user edit of the tender controls (TENCTL) business object
'**********************************************************************************************
'* Versions:
'</CAMH>***************************************************************************************

Option Explicit

' These constants are from the cEnterprise class cParameter.cls and are
'  used to define parameter field types
Const TYPE_STRING = 0
Const TYPE_LONG = 1
Const TYPE_DOUBLE = 2
Const TYPE_BOOLEAN = 3
Const TYPE_COLOUR = 4 'same as long, but for editor - set using color selector

Private mTOSControlRec As cTypesOfSaleControl
Private mBlnRecordReady As Boolean

' Main add function
Public Function UserEnterNewRecord(ByRef oRetRec As cTypesOfSaleControl) As Boolean
    
    Dim oCtrl As Control
    
    ' setup the dynamic user entries
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    pbxOne.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    chkInUse.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    
    For Each oCtrl In Me.Controls
        If TypeName(oCtrl) = "CommandButton" Then
            oCtrl.BackColor = Me.BackColor
        End If
    Next oCtrl
    
    mBlnRecordReady = False             ' default to user cancel
    Set mTOSControlRec = goDatabase.CreateBusinessObject(CLASSID_TYPESOFSALECONTROL)
    
    ' get the new entries
    Call Me.Show(vbModal)
    Set oRetRec = mTOSControlRec
    Set mTOSControlRec = Nothing
    UserEnterNewRecord = mBlnRecordReady
    
End Function

' Add the new param and close the form
Private Sub btnAdd_Click()
    
    Dim strMsg As String
    
    txtID.Text = UCase(txtID.Text)
    If Len(txtID.Text) < 2 Then
        strMsg = "Please enter a unique, two character code"
        txtID.SetFocus
        GoTo ErrBadEntry
    End If
    
    If frmTOSControlsEditor.TOSCodeIsUnique(txtID.Text) = False Then
        strMsg = "Code is not unique, please enter a unique, two character code"
        txtID.SetFocus
        GoTo ErrBadEntry
    End If
    
    
    If txtDescription.Text = "" Then
        strMsg = "Please enter a description"
        txtDescription.SetFocus
        GoTo ErrBadEntry
    End If
    
    mTOSControlRec.Code = txtID.Text
    mTOSControlRec.Description = txtDescription.Text
    mTOSControlRec.ValidForUse = chkInUse.Value
    mBlnRecordReady = True
    Me.Hide
    Exit Sub
    
ErrBadEntry:
    MsgBox strMsg, vbOKOnly, Me.Caption
End Sub

' abort the add
Private Sub btnCancel_Click()
    
    mBlnRecordReady = False
    Me.Hide
    
End Sub

Private Sub Form_Load()
    Set Me.Icon = frmTOSControlsEditor.Icon
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then chkInUse.SetFocus
End Sub

Private Sub txtID_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then txtDescription.SetFocus
End Sub



