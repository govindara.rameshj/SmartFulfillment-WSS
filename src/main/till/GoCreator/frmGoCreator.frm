VERSION 5.00
Object = "{C8530F8A-C19C-11D2-99D6-9419F37DBB29}#1.1#0"; "ccrpprg6.ocx"
Begin VB.Form frmGoCreator 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Wickes .GO File Creator"
   ClientHeight    =   4815
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3615
   Icon            =   "frmGoCreator.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4815
   ScaleWidth      =   3615
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Store && PC List"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   120
      TabIndex        =   17
      Top             =   720
      Width           =   1575
      Begin VB.CheckBox Check12 
         Alignment       =   1  'Right Justify
         Caption         =   "Select All"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H008080FF&
         Height          =   255
         Left            =   360
         MaskColor       =   &H008080FF&
         TabIndex        =   12
         Top             =   3600
         Width           =   1095
      End
      Begin VB.CheckBox Check11 
         Caption         =   "Refund"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   3360
         Width           =   1095
      End
      Begin VB.CheckBox Check10 
         Caption         =   "Till 10"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   3120
         Width           =   1095
      End
      Begin VB.CheckBox Check9 
         Caption         =   "Till 9"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   2880
         Width           =   1095
      End
      Begin VB.CheckBox Check8 
         Caption         =   "Till 8"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   2640
         Width           =   1095
      End
      Begin VB.CheckBox Check7 
         Caption         =   "Till 7"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   2400
         Width           =   1095
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Till 6"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   2160
         Width           =   1095
      End
      Begin VB.CheckBox Check5 
         Caption         =   "Till 5"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1920
         Width           =   1095
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Till 4"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1680
         Width           =   1095
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Till 3"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Till 2"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Till 1"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   0
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Store:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Status"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1800
      TabIndex        =   15
      Top             =   3360
      Width           =   1695
      Begin CCRProgressBar6.ccrpProgressBar prog 
         Height          =   135
         Left            =   120
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   238
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label status 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Create .GO File"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1800
      TabIndex        =   13
      Top             =   4320
      Width           =   1695
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   ".GO Creator"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   3375
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   $"frmGoCreator.frx":058A
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   1800
      TabIndex        =   14
      Top             =   960
      Width           =   1695
   End
End
Attribute VB_Name = "frmGoCreator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/GoCreator/frmGoCreator.frm $
'**********************************************************************************************
'* $Author: Jeremy Janisch $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary:
'*
'**********************************************************************************************
'* Versions:
'*
'* 01/12/05 DaveF   v1.0.1  General coding tidy.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Sub Check12_Click()

    Check1.Value = Check12.Value
    Check2.Value = Check12.Value
    Check3.Value = Check12.Value
    Check4.Value = Check12.Value
    Check5.Value = Check12.Value
    Check6.Value = Check12.Value
    Check7.Value = Check12.Value
    Check8.Value = Check12.Value
    Check9.Value = Check12.Value
    Check10.Value = Check12.Value
    Check11.Value = Check12.Value

End Sub

Private Sub Command1_Click()

    Call DisableAll
    
    prog.Value = 0
    prog.Max = 20

    Call UpdateStats("Checking - 01")
    If Check1.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "01")
    Else
        Call UpdateStats("Skipped - 01")
    End If
    
    Call UpdateStats("Checking - 02")
    If Check2.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "02")
    Else
        Call UpdateStats("Skipped - 02")
    End If

    Call UpdateStats("Checking - 03")
    If Check3.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "03")
    Else
        Call UpdateStats("Skipped - 03")
    End If

    Call UpdateStats("Checking - 04")
    If Check4.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "04")
    Else
        Call UpdateStats("Skipped - 04")
    End If

    Call UpdateStats("Checking - 05")
    If Check5.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "05")
    Else
        Call UpdateStats("Skipped - 05")
    End If

    Call UpdateStats("Checking - 06")
    If Check6.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "06")
    Else
        Call UpdateStats("Skipped - 06")
    End If

    Call UpdateStats("Checking - 07")
    If Check7.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "07")
    Else
        Call UpdateStats("Skipped - 07")
    End If
    
    Call UpdateStats("Checking - 08")
    If Check8.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "08")
    Else
        Call UpdateStats("Skipped - 08")
    End If
    
    Call UpdateStats("Checking - 09")
    If Check9.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "09")
    Else
        Call UpdateStats("Skipped - 09")
    End If

    Call UpdateStats("Checking - 10")
    If Check10.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "10")
    Else
        Call UpdateStats("Skipped - 10")
    End If

    Call UpdateStats("Checking - 11")
    If Check11.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, "11")
    Else
        Call UpdateStats("Skipped - 11")
    End If

    Call UpdateStats("COMPLETED")
    EnableAll

End Sub
