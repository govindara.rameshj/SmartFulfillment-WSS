Attribute VB_Name = "modPoll"
Option Explicit

Public Function CreateGo(store As String, pc As String)

Dim strLocation    As String
Dim strMessage     As String
Dim strBadLocation As String

    On Error GoTo NoFolder_Error

    UpdateStats ("Sending - " & pc)
    strLocation = "\\wix" & store & "\VOL1\cts\Update\Workstations\ws" & pc & "\" & pc & ".GO"
    strMessage = "GO File Created by Wickes Version Checker - " & Date & " - " & Time
    Open strLocation For Output As #1
    Print #1, strMessage
    Close #1
    UpdateStats ("Done - " & pc)
    
    Exit Function
    
NoFolder_Error:

    strBadLocation = "\\wix" & store & "\VOL1\cts\Update\Workstations\ws" & pc & "\"
    MsgBox "Error! Unable to write to folder: " & strBadLocation

End Function

Public Function UpdateStats(info As String)

    frmGoCreator.status.Caption = info
    Call NextProg
    DoEvents
    frmGoCreator.Refresh

End Function

Public Function NextProg()

Dim Current As Integer
Dim NewVal  As Integer

    Current = frmGoCreator.prog.Value
    NewVal = Current + 1

    If frmGoCreator.prog.Value < frmGoCreator.prog.Max Then frmGoCreator.prog.Value = NewVal

End Function

Public Function StartPoll(pc As String)

    UpdateStats ("Checking - " & pc)
    If frmGoCreator.Check2.Value = 1 Then
        Call CreateGo(frmGoCreator.Text1.Text, pc)
    Else
        Call UpdateStats("Skipped - " & pc)
    End If

End Function

Public Function DisableAll()

    With frmGoCreator
        .Text1.Enabled = False
        .Check1.Enabled = False
        .Check2.Enabled = False
        .Check3.Enabled = False
        .Check4.Enabled = False
        .Check5.Enabled = False
        .Check6.Enabled = False
        .Check7.Enabled = False
        .Check8.Enabled = False
        .Check9.Enabled = False
        .Check10.Enabled = False
        .Check11.Enabled = False
    End With

End Function

Public Function EnableAll()

    With frmGoCreator
        .Text1.Enabled = True
        .Check1.Enabled = True
        .Check2.Enabled = True
        .Check3.Enabled = True
        .Check4.Enabled = True
        .Check5.Enabled = True
        .Check6.Enabled = True
        .Check7.Enabled = True
        .Check8.Enabled = True
        .Check9.Enabled = True
        .Check10.Enabled = True
        .Check11.Enabled = True
    End With

End Function
