VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{905F077A-73D3-48CB-BCBB-2EF90EBA28A1}#1.0#0"; "EditDateCtl.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Begin VB.Form frmEmployeeMaintenance 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Employee Maintenance"
   ClientHeight    =   6375
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9735
   Icon            =   "frmEmployeeMaintenance.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6375
   ScaleWidth      =   9735
   StartUpPosition =   2  'CenterScreen
   Begin LpLib.fpCombo fpcboUser 
      Height          =   360
      Left            =   2505
      TabIndex        =   0
      Top             =   960
      Width           =   4710
      _Version        =   196608
      _ExtentX        =   8308
      _ExtentY        =   635
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   0   'False
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Text            =   ""
      Columns         =   2
      Sorted          =   1
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   -1
      ColumnWidthScale=   2
      RowHeight       =   -1
      WrapList        =   0   'False
      WrapWidth       =   0
      AutoSearch      =   2
      SearchMethod    =   2
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   1
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   1
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   0
      BorderColor     =   -2147483642
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   2
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   2
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   -1  'True
      VScrollSpecialType=   2
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   1
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   1
      DataFieldList   =   ""
      ColumnEdit      =   -1
      ColumnBound     =   -1
      Style           =   2
      MaxDrop         =   8
      ListWidth       =   -1
      EditHeight      =   -1
      GrayAreaColor   =   -2147483633
      ListLeftOffset  =   0
      ComboGap        =   -2
      MaxEditLen      =   150
      VirtualPageSize =   0
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   -1  'True
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      ListPosition    =   0
      ButtonThreeDAppearance=   0
      OLEDragMode     =   0
      OLEDropMode     =   0
      Redraw          =   -1  'True
      AutoSearchFill  =   -1  'True
      AutoSearchFillDelay=   500
      EditMarginLeft  =   1
      EditMarginTop   =   1
      EditMarginRight =   0
      EditMarginBottom=   3
      ResizeRowToFont =   0   'False
      TextTipMultiLine=   0
      AutoMenu        =   -1  'True
      EditAlignH      =   0
      EditAlignV      =   0
      ColDesigner     =   "frmEmployeeMaintenance.frx":058A
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5 - Save User"
      Height          =   400
      Left            =   648
      TabIndex        =   11
      Top             =   5280
      Width           =   1750
   End
   Begin VB.CommandButton cmdAddUser 
      Caption         =   "F3 - Add User"
      Height          =   400
      Left            =   3992
      TabIndex        =   7
      Top             =   4560
      Width           =   1750
   End
   Begin VB.CommandButton cmdDelUser 
      Caption         =   "F2 - Delete User"
      Height          =   400
      Left            =   1737
      TabIndex        =   6
      Top             =   4560
      Width           =   1750
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   400
      Left            =   7336
      TabIndex        =   12
      Top             =   5280
      Width           =   1750
   End
   Begin VB.CommandButton cmdUserSecurity 
      Caption         =   "F6 - Set User Security"
      Height          =   400
      Left            =   2877
      TabIndex        =   9
      Top             =   5280
      Width           =   1750
   End
   Begin VB.TextBox txtFullName 
      Height          =   285
      Left            =   5040
      TabIndex        =   1
      Top             =   2184
      Width           =   3255
   End
   Begin VB.CommandButton cmdChangePassword 
      Caption         =   "F4 - Change Password"
      Height          =   400
      Left            =   6247
      TabIndex        =   8
      Top             =   4560
      UseMaskColor    =   -1  'True
      Width           =   1750
   End
   Begin VB.TextBox txtSupervisor 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   5040
      MaxLength       =   1
      TabIndex        =   5
      Top             =   3720
      Width           =   375
   End
   Begin VB.TextBox txtInitials 
      Height          =   285
      Left            =   5040
      MaxLength       =   3
      TabIndex        =   2
      Top             =   2568
      Width           =   735
   End
   Begin VB.CommandButton cmdSetPayrollNo 
      Caption         =   "F7 - Edit Payroll No"
      Height          =   400
      Left            =   5106
      TabIndex        =   10
      Top             =   5280
      UseMaskColor    =   -1  'True
      Width           =   1750
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   13
      Top             =   6000
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmEmployeeMaintenance.frx":094C
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9816
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:22"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ucEditDate.ucDateText dtxtValidUntilDate 
      Height          =   285
      Left            =   5040
      TabIndex        =   4
      Top             =   3336
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   503
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DateFormat      =   "DD/MM/YY"
      Text            =   ""
   End
   Begin VB.TextBox txtPosition 
      Height          =   285
      Left            =   5040
      TabIndex        =   3
      Top             =   2952
      Width           =   2055
   End
   Begin VB.Label lblUserID 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5040
      TabIndex        =   21
      Top             =   1800
      Width           =   735
   End
   Begin VB.Label lblId 
      BackStyle       =   0  'Transparent
      Caption         =   " ID:"
      Height          =   270
      Left            =   3480
      TabIndex        =   20
      Top             =   1815
      Width           =   1500
   End
   Begin VB.Label lblFullName 
      BackStyle       =   0  'Transparent
      Caption         =   " Full Name:"
      Height          =   270
      Left            =   3480
      TabIndex        =   19
      Top             =   2199
      Width           =   1500
   End
   Begin VB.Label lblPosition 
      BackStyle       =   0  'Transparent
      Caption         =   " Position:"
      Height          =   270
      Left            =   3480
      TabIndex        =   18
      Top             =   2967
      Width           =   1500
   End
   Begin VB.Label lblValidUntil 
      BackStyle       =   0  'Transparent
      Caption         =   " Valid Until:"
      Height          =   270
      Left            =   3480
      TabIndex        =   17
      Top             =   3351
      Width           =   1500
   End
   Begin VB.Label lblInitials 
      BackStyle       =   0  'Transparent
      Caption         =   " Initials:"
      Height          =   270
      Left            =   3480
      TabIndex        =   16
      Top             =   2583
      Width           =   1500
   End
   Begin VB.Label lblSupervisor 
      BackStyle       =   0  'Transparent
      Caption         =   " Supervisor:"
      Height          =   270
      Left            =   3480
      TabIndex        =   15
      Top             =   3735
      Width           =   1500
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      Caption         =   "Select User"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3240
      TabIndex        =   14
      Top             =   360
      Width           =   3255
   End
End
Attribute VB_Name = "frmEmployeeMaintenance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Employee Maintenance/frmEmployeeMaintenance.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application that is designed to read text files BCVSKU.txt and
'*                      BCVSUP.txt and import them to tables BCVSKU and BCVSUP. A text file
'*                      holding the date of the import is saved.
'*
'**********************************************************************************************
'* Versions:
'*
'* 02/11/05 DaveF   v1.0.0  Initial build. WIX1170.
'*
'* 09/11/05 DaveF   v1.0.1  WIX1170 Changed form frmGroupAccess to add the extra row at the
'*                              bottom of the Group list to include the Barcode Verification
'*                              group option which isn't as yet included in the AFGCTL table.
'*
'* 01/12/05 DaveF   v1.0.2  WIX1170 Updated form frmGroupAccess to show the correct information
'*                              in the status bar.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'*  (/u='001' -c='ffff')
'*
'*  u = User ID
'*
'*  c = Checksum (Not used at present)
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmEmployeeMaintenance"

Private mcnnDBConnection As ADODB.Connection
Private mobjUser         As cEnterprise_Wickes.cUser

Private mblnUserChanged  As Boolean
Private mblnNewUser      As Boolean
Private mblnSetup        As Boolean

' Form load event.
Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

Dim strErrSource  As String

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
'   Get the system setup.
    mblnSetup = True
    Call GetRoot
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Me.lblTitle.BackColor = Me.BackColor
    ' Get the list of users.
    Call GetUserList
        
'   Set up the database connection.
    Set mcnnDBConnection = goSession.Database.Connection
    If (mcnnDBConnection Is Nothing) Then
        Call MsgBox("The Database connection to DSN - " & goSession.Database.Connection.DefaultDatabase & " - Failed")
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    End If
    
    mblnUserChanged = False
    mblnNewUser = False
    
    mblnSetup = False
    
    Exit Sub
    
FormLoad_Error:

    If (Err.Source <> "") Then
        strErrSource = PROCEDURE_NAME & " - " & Err.Source
    Else
        strErrSource = PROCEDURE_NAME
    End If
    Call MsgBox("Module: " & strErrSource & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, App.EXEName)
    Call DebugMsg(MODULE_NAME, strErrSource, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
    Call Err.Report(MODULE_NAME, strErrSource, 0, False)
    
    Unload Me
    
End Sub

' Unload the form and empty all the used objects.
Private Sub Form_Unload(Cancel As Integer)

Const PROCEDURE_NAME As String = "Form_Unload"
    
    On Error GoTo Form_Unload_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Unload")
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Ended on " & Now())
    
    If (mblnUserChanged = True) Then
        If (MsgBox("The current user has been changed. Do you wish" & vbCrLf & _
                        " to lose the changes and exit the system?", vbQuestion + vbYesNo, _
                        "Lose Changes And Exit") = vbNo) Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Set mobjUser = Nothing
    
    If Not (mcnnDBConnection Is Nothing) Then
        If (mcnnDBConnection.State = adStateOpen) Then
            mcnnDBConnection.Close
        End If
    End If
    
    Set mcnnDBConnection = Nothing
    
    End

    Exit Sub
    
Form_Unload_Error:

    End
    
End Sub

' Form Key Down event.
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyF2
                KeyCode = 0
                cmdDelUser_Click        ' Delete user.
            
            Case vbKeyF3
                KeyCode = 0
                cmdAddUser_Click        ' Add user.
            
            Case vbKeyF4
                KeyCode = 0
                cmdChangePassword_Click ' Change the user password.
                
            Case vbKeyF5
                KeyCode = 0
                cmdSave_Click           ' Save the user data.
                
            Case vbKeyF6
                KeyCode = 0
                cmdUserSecurity_Click   ' Set the user security.
            
            Case vbKeyF7
                KeyCode = 0
                cmdSetPayrollNo_Click   ' Set the user payroll number.
            
            Case vbKeyF10
                KeyCode = 0
                cmdExit_Click           ' Exit system.
                
        End Select
    End If
    
End Sub

' Add a new user.
Private Sub cmdAddUser_Click()

Const PROCEDURE_NAME As String = "cmdAddUser_Click"
    
    On Error GoTo cmdAddUser_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdAddUser Click")
    
    ' Check if the current user has been changed.
    If (mblnUserChanged = True) Then
        If (MsgBox("Changes have made to the current user." & vbCrLf & vbCrLf & _
                        "Do you wish to discard the changes?", _
                        vbInformation + vbYesNo, "Discard changes") = vbNo) Then
            Call txtFullName.SetFocus
            Exit Sub
        End If
    End If
    
    ' Setup the form for the new user details.
    Call NewUserSetup

    Exit Sub
   
cmdAddUser_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Change the User password.
Private Sub cmdChangePassword_Click()

Const PROCEDURE_NAME As String = "cmdChangePassword_Click"

    On Error GoTo cmdChangePassword_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdChangePassword Click")
    
    Me.Visible = False
    Load frmPassword
    Call frmPassword.SetPassword(mobjUser, mblnNewUser)
    frmPassword.Show vbModal
    If (frmPassword.NewPassword = True) Then
        mblnUserChanged = True
    End If
    Unload frmPassword
    Me.Visible = True
    
    Exit Sub
    
cmdChangePassword_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Delete the user.
Private Sub cmdDelUser_Click()

Const PROCEDURE_NAME As String = "cmdDelUser_Click"

Dim lngListIndex As Long
Dim lngAnswer    As Long
    
    On Error GoTo cmdDelUser_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdDelUser Click")
    
    ' Ask if to delete user.
    lngAnswer = MsgBox("Do you want to delete user" & vbCrLf & vbCrLf & _
                  "         ID: " & Me.lblUserID.Caption & vbCrLf & _
                  "         Name: " & Me.txtFullName.Text, vbYesNo, "Confirm Delete User")
    Select Case (lngAnswer)
        Case vbNo
            Exit Sub
        Case vbYes
            If CheckPassword("Password Check :- For Deleting User") = True Then
                ' Delete the user.
                Call mobjUser.RecordAsDeleted(goSession.UserID, goSession.CurrentEnterprise.IEnterprise_WorkstationID)
                lngListIndex = fpcboUser.ListIndex
                Call fpcboUser.RemoveItem(lngListIndex)
                mblnSetup = True
                Me.fpcboUser.ListIndex = lngListIndex - 1
                mblnSetup = False
            End If
    End Select
    
    mblnUserChanged = False
    mblnNewUser = False
    
    Exit Sub

cmdDelUser_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Exit the system.
Private Sub cmdExit_Click()

Const PROCEDURE_NAME As String = "cmdExit_Click"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdExit Click")
        
    Unload Me
    
End Sub

' Save the
Private Sub cmdSave_Click()

Const PROCEDURE_NAME As String = "cmdSave_Click"

Dim blnSave   As Boolean 'check to see if function worked
Dim intCount   As Integer
Dim strControl As String

    On Error GoTo cmdSave_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdSave Click")

    blnSave = True
    
    ' Test for missing data.
    For intCount = 1 To 1
        If txtFullName.Text = "" Then
            strControl = "FullName"
            Exit For
        End If
        If txtInitials.Text = "" Then
            strControl = "Initials"
            Exit For
        End If
        If (Me.txtPosition = "") Then
            strControl = "Position"
            Exit For
        End If
        If (Me.dtxtValidUntilDate.Text = "") Then
            strControl = "ValidUntilDate"
            Exit For
        End If
        If (Me.txtSupervisor = "") Then
            strControl = "Supervisor"
            Exit For
        End If
    Next intCount
    If (strControl <> "") Then
        Call MsgBox("Unable to save changes due to missing information." & vbCrLf & vbCrLf & _
                            " The " & strControl & " value must be filled in.", vbCritical + vbOKOnly, _
                            "Missing User Data")
        If (strControl = "ValidUntilDate") Then
            Me.dtxtValidUntilDate.SetFocus
        Else
            Me.Controls("txt" & strControl).SetFocus
        End If
        Exit Sub
    End If
    
    ' Check if the user has a password set.
    If (mobjUser.Password = "") Then
        Call MsgBox("Unable to save changes as the User password" & vbCrLf & _
                            " has not been set.", vbCritical + vbOKOnly, _
                            "User Password Not Set")
        Exit Sub
    End If
    
    ' Check if the user payroll number has been set.
    If (mobjUser.PayrollNumber = "") Then
        Call MsgBox("Unable to save changes as the User payroll" & vbCrLf & _
                            " number has not been set.", vbCritical + vbOKOnly, _
                            "User Payroll Number Not Set")
        Exit Sub
    End If
    
    If (mblnNewUser = True) Then
        blnSave = CheckPassword("Password Check :- Creating New User")
    Else
        blnSave = CheckPassword("Password Check :- Updating User")
    End If
    
    If (blnSave = True) Then
        sbStatus.Panels(PANEL_INFO).Text = "Saving changes"
        mobjUser.EmployeeID = Me.lblUserID.Caption
        mobjUser.FullName = Me.txtFullName.Text
        mobjUser.Initials = Me.txtInitials.Text
        mobjUser.Position = Me.txtPosition
        mobjUser.PasswordValidTill = GetDate(dtxtValidUntilDate.Text)
        If (Me.txtSupervisor.Text = "Y") Then
            mobjUser.Supervisor = True
        Else
            mobjUser.Supervisor = False
        End If
        
        If (mblnNewUser = True) Then
            mobjUser.IBo_SaveIfNew
            Call fpcboUser.AddItem(mobjUser.EmployeeID & vbTab & Me.txtFullName.Text)
            mblnUserChanged = False
            ' Move the User list to the newly added user.
            With Me.fpcboUser
                ' Get the search string
                .SearchText = mobjUser.EmployeeID
                ' Search the first column
                .ColumnSearch = 0
                ' Search for partial matches
                .SearchMethod = SearchMethodExactMatch
                ' Start the search
                .Action = ActionSearch
                ' If a match is found, select the item
                If (.SearchIndex <> -1) Then
                    .ListIndex = .SearchIndex
                Else
                    ' If no match is found, reset the list
                    .ListIndex = 0
                End If
            End With
        Else
            mobjUser.IBo_SaveIfExists
            fpcboUser.List(fpcboUser.ListIndex) = (mobjUser.EmployeeID & vbTab & txtFullName.Text)
            mblnUserChanged = False
        End If
        sbStatus.Panels(PANEL_INFO).Text = ""
    Else
        Exit Sub
    End If

    mblnNewUser = False
    
    Call MsgBox("The User settings have been saved.", vbInformation + vbOKOnly, _
                    "User Settings Saved")
                    
    Exit Sub
    
cmdSave_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub cmdSetPayrollNo_Click()

Const PROCEDURE_NAME As String = "cmdSetPayrollNo_Click"

Dim strPayrollNo As String

    On Error GoTo cmdSetPayrollNo_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdSetPayrollNo Click")

    ' Store the current Payroll number to test if it changes.
    strPayrollNo = mobjUser.PayrollNumber
    Me.Visible = False
    Load frmEditPayrollNo
    Call frmEditPayrollNo.EditPayrollNumber(Me.lblUserID.Caption, Me.txtFullName.Text, strPayrollNo)
    If (strPayrollNo <> mobjUser.PayrollNumber) Then
        mobjUser.PayrollNumber = strPayrollNo
        mblnUserChanged = True
    End If
    Unload frmEditPayrollNo
    Me.Visible = True
    
    Exit Sub

cmdSetPayrollNo_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub cmdUserSecurity_Click()

Const PROCEDURE_NAME As String = "cmdUserSecurity_Click"

    On Error GoTo cmdUserSecurity_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdUserSecurity Click")

    Me.Visible = False
    Load frmGroupAccess
    Set frmGroupAccess.User = mobjUser
    Set frmGroupAccess.DBConnection = mcnnDBConnection
    frmGroupAccess.Show vbModal
    If (frmGroupAccess.DataChanged = True) Then
        mblnUserChanged = True
    End If
    Unload frmGroupAccess
    Me.Visible = True
    
    Exit Sub
    
cmdUserSecurity_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub GetUserList()

Const PROCEDURE_NAME As String = "GetUserList"

Dim objUser As cEnterprise_Wickes.cUser
Dim colUsers As Collection
Dim lngUserNo As Long

    On Error GoTo GetUserList_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get User List")

    ' Get list of active users.
    Set objUser = MakeUser
    With objUser
        .IBo_ClearLoadFilter
        Call .IBo_AddLoadFilter(CMP_EQUAL, FID_USER_Deleted, False)
    End With
    Set colUsers = objUser.IBo_LoadMatches
    
    ' Load users into combo ready for selection.
    fpcboUser.Clear
    For Each objUser In colUsers
        Call fpcboUser.AddItem(objUser.EmployeeID & vbTab & objUser.FullName)
    Next objUser
    
    Set objUser = Nothing
    Set colUsers = Nothing
    
    fpcboUser.ListIndex = 0
    
    Exit Sub
   
GetUserList_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Public Function MakeUser() As cEnterprise_Wickes.cUser

Const PROCEDURE_NAME As String = "MakeUser"

Dim objUser As cEnterprise_Wickes.cUser
    
    On Error GoTo MakeUser_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Make User")

    Set objUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Set MakeUser = objUser
    
    Set objUser = Nothing
    
    Exit Function
    
MakeUser_Error:

    Set objUser = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function

' Get the user information.
Private Sub fpcboUser_Click()

Const PROCEDURE_NAME As String = "fpcboUser_Click"

Dim strUserID As String
Dim lngId     As Long
    
    On Error GoTo fpcboUser_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "fpcboUser Click")

    mblnSetup = True
    
    ' Test if the current user has changed.
    If (mblnUserChanged = True) Then
        If (MsgBox("Changes have been made to the current user." & vbCrLf & vbCrLf & _
                        "Do you wish to discard the changes?", _
                        vbInformation + vbYesNo, "Discard changes") = vbNo) Then
            Me.fpcboUser.Text = mobjUser.EmployeeID & vbTab & mobjUser.FullName
            Call txtFullName.SetFocus
            Exit Sub
        Else
            mblnUserChanged = False
            mblnNewUser = False
        End If
    End If
    
    ' Get the user ID for the selected user.
    With Me.fpcboUser
        .Col = .ColFromName = "ID"
        strUserID = .ColText
    End With
    
    ' Get the user details.
    Set mobjUser = MakeUser
    With mobjUser
        .IBo_ClearLoadFilter
        Call .IBo_AddLoadFilter(CMP_EQUAL, FID_USER_Deleted, False)
        Call .IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, strUserID)
    End With
    Call mobjUser.IBo_LoadMatches
    
    ' Fill the screen with the details.
    Me.lblUserID.Caption = mobjUser.EmployeeID
    Me.txtFullName.Text = mobjUser.FullName
    Me.txtInitials.Text = mobjUser.Initials
    Me.dtxtValidUntilDate.Text = DisplayDate(mobjUser.PasswordValidTill, False)
    Me.txtPosition = mobjUser.Position
    If mobjUser.Supervisor = True Then
        Me.txtSupervisor.Text = "Y"
    Else
        Me.txtSupervisor.Text = "N"
    End If
            
    mblnSetup = False
    
    Exit Sub
    
fpcboUser_Click_Error:

    mblnSetup = False

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub dtxtValidUntilDate_Change()

    If (mblnSetup = False) Then
        mblnUserChanged = True
    End If
    
End Sub

Private Sub txtFullName_Change()

    If (mblnSetup = False) Then
        mblnUserChanged = True
    End If
    
End Sub

Private Sub txtInitials_Change()

    If (mblnSetup = False) Then
        mblnUserChanged = True
    End If

End Sub

Private Sub txtPosition_Change()
    
    If (mblnSetup = False) Then
        mblnUserChanged = True
    End If
    
End Sub

Private Sub txtSupervisor_Change()

    If (mblnSetup = False) Then
        Me.txtSupervisor = UCase(Me.txtSupervisor)
        If (Me.txtSupervisor <> "Y") And (Me.txtSupervisor <> "N") Then
            Call MsgBox("Please enter either Y or N for" & vbCrLf & _
                        " the Supervisor value.", vbInformation + vbOKOnly, "IncorrectSupervisor Value")
            Exit Sub
        End If
        mblnUserChanged = True
    End If
    
End Sub

' Setup the screen for a new user.
Private Sub NewUserSetup()

Const PROCEDURE_NAME As String = "NewUserSetup"

    On Error GoTo NewUserSetup_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "New User Setup")
    
    mblnSetup = True
    fpcboUser.Text = "000" & vbTab & "*Creating New User*"
    Set mobjUser = MakeUser
    
    ' Set User Id to zero as business objects will calculate the new ID on save.
    Me.lblUserID.Caption = "000"
    Me.dtxtValidUntilDate.Text = DisplayDate(DateAdd("d", 30, Date), False)
    Me.txtFullName.Text = ""
    Me.txtInitials.Text = ""
    Me.txtPosition = ""
    Me.txtSupervisor = ""
    Me.txtFullName.SetFocus
    
    mblnNewUser = True
    mblnUserChanged = True
        
    mblnSetup = False
    
    Exit Sub
    
NewUserSetup_Error:

    mblnSetup = False
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Check the password of the user.
Private Function CheckPassword(strCheckPrompt As String) As Boolean
    
    CheckPassword = False
    Call Load(frmCheckUser)
    frmCheckUser.SetCaption (strCheckPrompt)
    Call frmCheckUser.Show(vbModal)
    CheckPassword = frmCheckUser.PasswordValid
    Unload frmCheckUser
    
End Function
