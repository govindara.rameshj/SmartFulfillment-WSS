VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmGroupAccess 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Group Access"
   ClientHeight    =   7965
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8700
   ControlBox      =   0   'False
   Icon            =   "frmGroupAccess.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   8700
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   400
      Left            =   5455
      TabIndex        =   3
      Top             =   7080
      Width           =   1750
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5 - Save Settings"
      Height          =   400
      Left            =   1495
      TabIndex        =   2
      Top             =   7080
      Width           =   1750
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   7590
      Width           =   8700
      _ExtentX        =   15346
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmGroupAccess.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7990
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:19"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin FPSpreadADO.fpSpread fpsprdSecurity 
      Height          =   6065
      Left            =   255
      TabIndex        =   4
      Top             =   840
      Width           =   8190
      _Version        =   458752
      _ExtentX        =   14446
      _ExtentY        =   10698
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   4
      MaxRows         =   99
      ScrollBars      =   2
      SpreadDesigner  =   "frmGroupAccess.frx":2756
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      Caption         =   "Select The User Group Security Levels"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1703
      TabIndex        =   1
      Top             =   240
      Width           =   5295
   End
End
Attribute VB_Name = "frmGroupAccess"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Employee Maintenance/frmGroupAccess.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Designed to allow the user to set the access rights to the various groups.
'*
'**********************************************************************************************
'* Versions:
'*
'* 02/11/05 DaveF   v1.0.0  Initial build. WIX1170.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmGroupAccess"

Private mobjUser        As cEnterprise_Wickes.cUser
Private mcnnConnection  As ADODB.Connection
Private mblnDataChanged As Boolean
Private mblnChanged     As Boolean

' Form load event.
Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Me.lblTitle.BackColor = Me.BackColor
       
    Exit Sub
    
FormLoad_Error:
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Form Activate event.
Private Sub Form_Activate()

Const PROCEDURE_NAME As String = "Form_Activate"

    On Error GoTo Form_Activate_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Activate")
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    ' Read in the security settings for the current user.
    Call GetSecuritySettings
    
    mblnDataChanged = False
    
    Exit Sub
    
Form_Activate_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case vbKeyF5
                    KeyCode = 0
                    Call cmdSave_Click
            Case vbKeyF10
                    KeyCode = 0
                    Call cmdExit_Click
         End Select
     End If

End Sub

Private Sub cmdExit_Click()

Const PROCEDURE_NAME As String = "cmdExit_Click"

    On Error GoTo cmdExit_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdExit Click")

    If (mblnChanged = True) Then
        If (MsgBox("Changes have made to the current user settings." & vbCrLf & vbCrLf & _
                        "Do you wish to discard the changes?", _
                        vbInformation + vbYesNo, "Discard changes") = vbNo) Then
            Exit Sub
        End If
    End If
    
    Me.Hide

    Exit Sub
    
cmdExit_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Sub

' Save the users security settings.
Private Sub cmdSave_Click()

Const PROCEDURE_NAME As String = "cmdSave_Click"
    
Dim intGroupNumber As Integer
Dim intCounter     As Integer
Dim blnChecked     As Boolean

    On Error GoTo cmdSave_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdSave Click")

    ' Loop down the grid saving the settings.
    With Me.fpsprdSecurity
        If (.MaxRows <> 0) Then
            For intCounter = 1 To .MaxRows
                .Row = intCounter
                .Col = .GetColFromID("NUMB")
                intGroupNumber = .Text
                .Col = .GetColFromID("UseGroup")
                If (.Text = True) Then
                    blnChecked = True
                Else
                    blnChecked = False
                End If
                .Col = .GetColFromID("SecLevel")
                mobjUser.SecurityLevels(intGroupNumber) = .Text
                If (.Text <> "") And (blnChecked = True) Then
                    mobjUser.GroupLevels(intGroupNumber) = True
                Else
                    mobjUser.GroupLevels(intGroupNumber) = False
                End If
            Next intCounter
        End If
    End With
    
    mblnChanged = False
    mblnDataChanged = True
    
    Call MsgBox("The Security settings have been saved.", vbInformation + vbOKOnly, _
                    "Security Settings Saved")
    Exit Sub
    
cmdSave_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Store the User.
Public Property Set User(ByRef objUser As cEnterprise_Wickes.cUser)

    Set mobjUser = objUser
    
End Property

' Store the Database connnection to use.
Public Property Set DBConnection(ByVal cnnConnection As ADODB.Connection)

    Set mcnnConnection = cnnConnection
    
End Property

' Show if the user changed any of the security settings.
Public Property Get DataChanged() As Boolean

    DataChanged = mblnDataChanged
    
End Property

' Read in the security settings for the current user.
Private Sub GetSecuritySettings()

Const PROCEDURE_NAME As String = "GetSecuritySettings"

Dim recGroups  As ADODB.Recordset
Dim strSQL     As String
Dim blnSkipRow As Boolean

    On Error GoTo GetSecuritySettings_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Security Settings")

    Me.fpsprdSecurity.MaxRows = 0
    
    ' Get the list of Groups.
    strSQL = "SELECT NUMB, DESCR FROM AFGCTL ORDER BY NUMB"
    If (mcnnConnection.State = adStateClosed) Then
        mcnnConnection.Open
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQL)
    Set recGroups = mcnnConnection.Execute(strSQL, , adCmdText)
    Do Until (recGroups.EOF = True)
        With Me.fpsprdSecurity
            blnSkipRow = True
            ' Description.
            If Not IsNull(recGroups.Fields("DESCR")) Then
                If (Trim(recGroups.Fields("DESCR")) <> "") Then
                    .MaxRows = .MaxRows + 1
                    .Row = .MaxRows
                    .Col = .GetColFromID("Group")
                    .Text = Trim(recGroups.Fields("DESCR"))
                    ' Lock the cell.
                    .Lock = True
                    blnSkipRow = False
                End If
            End If
            ' Group Number, hidden.
            If (blnSkipRow = False) Then
                If Not IsNull(recGroups.Fields("NUMB")) Then
                    .Col = .GetColFromID("NUMB")
                    .Text = Trim(recGroups.Fields("NUMB"))
                    ' Get the users current security level.
                    .Col = .GetColFromID("SecLevel")
                    .Text = mobjUser.SecurityLevels(CInt(Trim(recGroups.Fields("NUMB"))))
                    .Col = .GetColFromID("UseGroup")
                    .Text = mobjUser.GroupLevels(CInt(Trim(recGroups.Fields("NUMB"))))
                End If
            End If
        End With
        recGroups.MoveNext
    Loop
    
    ' Add the extra row at the bottom of the list to include the Barcode Verification group option which isn't
    '   as yet included in the AFGCTL table.
    With Me.fpsprdSecurity
        .MaxRows = .MaxRows + 1
        .Row = .MaxRows
        .Col = .GetColFromID("Group")
        .Text = "Barcode Verification"
        ' Lock the cell.
        .Lock = True
        .Col = .GetColFromID("NUMB")
        .Text = 99
        ' Get the users current security level.
        .Col = .GetColFromID("SecLevel")
        .Text = mobjUser.SecurityLevels(99)
        .Col = .GetColFromID("UseGroup")
        .Text = mobjUser.GroupLevels(99)
    End With
    
    Set recGroups = Nothing
    
    Exit Sub
    
GetSecuritySettings_Error:

    Set recGroups = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub fpsprdSecurity_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    mblnChanged = True
    
    ' If unchecked the Use Group check box then clear the Security level value.
    With Me.fpsprdSecurity
        If (Col = .GetColFromID("UseGroup")) Then
            .Col = Col
            .Row = Row
            ' Test for True as not yet actually unchecked.
            If (.Text = True) Then
                .Col = .GetColFromID("SecLevel")
                .Text = ""
            End If
        End If
    End With
    
End Sub
