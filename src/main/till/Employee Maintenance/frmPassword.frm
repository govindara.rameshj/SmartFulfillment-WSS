VERSION 5.00
Begin VB.Form frmPassword 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change Password"
   ClientHeight    =   2205
   ClientLeft      =   1575
   ClientTop       =   1545
   ClientWidth     =   3420
   ControlBox      =   0   'False
   Icon            =   "frmPassword.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   3420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtOldPwd 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   480
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   375
      Left            =   2160
      TabIndex        =   10
      Top             =   1680
      Width           =   1095
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5 - Save"
      Height          =   375
      Left            =   840
      TabIndex        =   9
      Top             =   1680
      Width           =   1095
   End
   Begin VB.TextBox txtConfirmPwd 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   8
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox txtNewPwd 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   6
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label lblFullName 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1440
      TabIndex        =   2
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label lblUserID 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   600
      TabIndex        =   1
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "User"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Confirm Password"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "New Password"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Old Password"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   1455
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Employee Maintenance/frmPassword.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Designed to allow user entry of old then new password for the current user.
'*
'**********************************************************************************************
'* Versions:
'*
'* 07/11/05 DaveF   v1.0.0  Initial build. WIX1170.
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************Option Explicit

Const MODULE_NAME As String = "frmPassword"

Private mobjUser        As cEnterprise_Wickes.cUser
Private mblnNewPassword As Boolean

Public Sub SetPassword(ByVal oUser As Object, ByVal blnNewUser As Boolean)

Const PROCEDURE_NAME As String = "SetPassword"

Const PAD_PWD = "*****"
    
    On Error GoTo SetPassword_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Set Password")

    lblUserID.Caption = oUser.EmployeeID
    lblFullName.Caption = oUser.FullName
    If blnNewUser = True Then
        txtOldPwd.Enabled = False
        txtOldPwd.Text = "N/A"
        txtOldPwd.PasswordChar = ""
        Me.Caption = "Create Password"
    End If
    Set mobjUser = oUser

    Exit Sub
    
SetPassword_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub cmdExit_Click()

Const PROCEDURE_NAME As String = "cmdExit_Click"

    On Error GoTo cmdExit_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdExit Click")

    If txtOldPwd.Enabled = False Then
        If mobjUser.SetPassword("") Then
            Call MsgBox("User has been marked as expired - no logins will be accepted until password updated", _
                            vbInformation + vbOKOnly, "User Marked As Expired")
        End If
    End If
    
    mblnNewPassword = False
    
    Me.Hide

    Exit Sub
    
cmdExit_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub cmdSave_Click()

Const PROCEDURE_NAME As String = "cmdSave_Click"

Dim strPassword As String
    
    On Error GoTo cmdSave_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdSave Click")

    If (txtOldPwd.Text = "") And (txtOldPwd.Enabled = True) Then
        Call MsgBox("Current password not entered" & vbCrLf & "Enter password and re-save", vbExclamation, "Password Error")
        txtOldPwd.SetFocus
        Exit Sub
    End If
    
    If txtNewPwd.Text = "" Then
        Call MsgBox("No password entered" & vbCrLf & "Enter password and re-save", vbExclamation, "Password Error")
        txtNewPwd.SetFocus
        Exit Sub
    End If
    
    'pad password to 5 characters
    If txtOldPwd.Enabled = True Then
        strPassword = txtOldPwd.Text
        While (Len(strPassword) < 5)
            strPassword = "0" & strPassword
        Wend
        If strPassword <> mobjUser.Password Then
            Call MsgBox("Current password not matched" & vbCrLf & "Enter current password and re-save", vbExclamation, "Password Error")
            txtOldPwd.SetFocus
            Exit Sub
        End If
    End If

    'pad password to 5 characters
    strPassword = txtNewPwd.Text
    If Len(strPassword) < 5 Then
        While (Len(strPassword) < 5)
            strPassword = "0" & strPassword
        Wend
    End If

    If strPassword = mobjUser.Password Then
        Call MsgBox("Entered password must be different from existing password" & vbCrLf & "Enter unique password and re-save", vbExclamation, "Password Error")
        txtNewPwd.SetFocus
        Exit Sub
    End If
    
    If txtNewPwd.Text <> txtConfirmPwd.Text Then
        Call MsgBox("Entered password and confirmation do not match" & vbCrLf & "Re-enter passwords", vbExclamation, "Password Error")
        txtNewPwd.SetFocus
        Exit Sub
    End If
    
    If mobjUser.SetPassword(strPassword) Then
        Call MsgBox("Password successfully updated", vbInformation, "Password Updated")
        mblnNewPassword = True
        Me.Hide
    Else
        Call MsgBox("Attempt to update password failed", vbInformation, "Password Update")
    End If
    
    Exit Sub
    
cmdSave_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Sub

Private Sub cmdSave_GotFocus()
    
    cmdSave.FontBold = True

End Sub

Private Sub cmdSave_LostFocus()
    
    cmdSave.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                    If cmdSave.Visible Then
                        KeyCode = 0
                        Call cmdSave_Click
                    End If
            Case (vbKeyF10):
                    If cmdExit.Visible Then
                        KeyCode = 0
                        Call cmdExit_Click
                    End If
         End Select
     End If

End Sub

Private Sub Form_Load()

    Call CentreForm(Me)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
End Sub

Private Sub txtConfirmPwd_GotFocus()

    txtConfirmPwd.SelStart = 0
    txtConfirmPwd.SelLength = Len(txtConfirmPwd)
    txtConfirmPwd.BackColor = RGBEdit_Colour
    

End Sub

Private Sub txtConfirmPwd_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub txtConfirmPwd_LostFocus()

    txtConfirmPwd.BackColor = RGB_WHITE

End Sub

Private Sub txtNewPwd_GotFocus()
    
    txtNewPwd.SelStart = 0
    txtNewPwd.SelLength = Len(txtNewPwd)
    txtNewPwd.BackColor = RGBEdit_Colour

End Sub

Private Sub txtNewPwd_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub txtNewPwd_LostFocus()
    
    txtNewPwd.BackColor = RGB_WHITE

End Sub

Private Sub txtOldPwd_GotFocus()
    
    txtOldPwd.SelStart = 0
    txtOldPwd.SelLength = Len(txtOldPwd)
    txtOldPwd.BackColor = RGBEdit_Colour

End Sub

Private Sub txtOldPwd_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub txtOldPwd_LostFocus()
    
    txtOldPwd.BackColor = RGB_WHITE

End Sub

' Show if a new password was entered for the user.
Public Property Get NewPassword() As Variant

    NewPassword = mblnNewPassword
    
End Property
