VERSION 5.00
Begin VB.Form frmCheckUser 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Validate User"
   ClientHeight    =   1470
   ClientLeft      =   3330
   ClientTop       =   3885
   ClientWidth     =   4590
   ControlBox      =   0   'False
   Icon            =   "frmCheckUser.frx":0000
   LinkTopic       =   "Password Check"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1470
   ScaleWidth      =   4590
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtPassword 
      Alignment       =   2  'Center
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1748
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   0
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label lblPrompt 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   4455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Password is case sensitive!"
      Height          =   255
      Left            =   1088
      TabIndex        =   2
      Top             =   1080
      Width           =   2415
   End
   Begin VB.Label lblPassword 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter The Password then press return"
      Height          =   255
      Left            =   728
      TabIndex        =   1
      Top             =   480
      Width           =   3135
   End
End
Attribute VB_Name = "frmCheckUser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module: frmCheckUser
'* Date  : 01/10/03
'* Author: markp
'**********************************************************************************************
'* Summary:Password check for user to be laowe tosav or update delete
'**********************************************************************************************
'* Versions:
'* 01/10/03    markp
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmCheckUser"

Private mblnPasswordValid As Boolean

Private Sub Form_Activate()
    
    Call txtPassword.SetFocus
    
End Sub

Public Function PasswordValid() As Boolean

    PasswordValid = mblnPasswordValid

End Function

Public Sub SetCaption(strCaption As String)

    lblPrompt.Caption = strCaption

End Sub

Private Sub Form_Load()

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

End Sub


Private Sub txtPassword_GotFocus()

    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)

End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

Dim strId       As String
Dim oUsers      As Object
Dim colUsers    As Collection
    
   If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        strId = goSession.UserID
        Set oUsers = frmEmployeeMaintenance.MakeUser
        Call oUsers.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, strId)
        Call oUsers.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_Password, txtPassword.Text)
        Call oUsers.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_Deleted, False)
        Set colUsers = oUsers.IBo_LoadMatches
        
        If colUsers.Count = 0 Then
            Call MsgBox("Invalid password entered", vbCritical + vbOKOnly, Me.Caption)
            Call txtPassword_GotFocus
            Exit Sub
        Else
            mblnPasswordValid = True
            Call frmCheckUser.Hide
        End If
   End If

   If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        mblnPasswordValid = False
        Call frmCheckUser.Hide
   End If

End Sub
