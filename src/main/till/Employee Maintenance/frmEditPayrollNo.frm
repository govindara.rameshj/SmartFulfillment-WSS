VERSION 5.00
Begin VB.Form frmEditPayrollNo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Payroll No"
   ClientHeight    =   2190
   ClientLeft      =   3825
   ClientTop       =   4170
   ClientWidth     =   4095
   ControlBox      =   0   'False
   Icon            =   "frmEditPayrollNo.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2190
   ScaleWidth      =   4095
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5 - Save"
      Height          =   375
      Left            =   1200
      TabIndex        =   2
      Top             =   1560
      Width           =   975
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   375
      Left            =   2400
      TabIndex        =   3
      Top             =   1560
      Width           =   975
   End
   Begin VB.TextBox txtPayrollNo 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   1680
      MaxLength       =   20
      TabIndex        =   1
      Top             =   840
      Width           =   2055
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "User"
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label lblUserID 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   840
      TabIndex        =   5
      Top             =   240
      Width           =   615
   End
   Begin VB.Label lblFullName 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1680
      TabIndex        =   4
      Top             =   240
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Payroll Number"
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   840
      Width           =   1215
   End
End
Attribute VB_Name = "frmEditPayrollNo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmEditPayrollNo"

Private Sub cmdExit_Click()

    Call Me.Hide

End Sub

Private Sub cmdSave_Click()

    Call Me.Hide
    
End Sub

Public Sub EditPayrollNumber(strUserID As String, strFullName As String, ByRef strPayrollNo As String)

    lblUserID.Caption = strUserID
    lblFullName.Caption = strFullName
    txtPayrollNo.Text = strPayrollNo
    Me.Show vbModal
    strPayrollNo = txtPayrollNo.Text

End Sub

Private Sub Form_Activate()

    Call CentreForm(Me)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF5): 'if F5 then call save
                    KeyCode = 0
                    If cmdSave.Visible Then Call cmdSave_Click
                Case (vbKeyF10): 'if F10 then exit
                    KeyCode = 0
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select
    
End Sub

Private Sub Form_Load()
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

End Sub
