VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IVClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /VB/OasysRootCom/IVClient.cls $
' $Revision: 3 $
' $Date: 14/08/02 12:33 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Variant Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

Public Property Get Mnemonic() As String
End Property
Public Property Get Enterprise(Optional nId As Variant) As IVEnterprise
End Property
Public Property Get EnterpriseCount() As Integer
End Property
Public Property Get test(oClient As Client)
End Property

