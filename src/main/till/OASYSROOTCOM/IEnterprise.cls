VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IEnterprise"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/IEnterprise.cls $
' $Revision: 5 $
' $Date: 12/02/02 10:56a $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------
Public Property Get Id() As Integer
End Property
Public Property Get Mnemonic() As String
End Property
Public Property Get ShortName() As String
End Property
Public Property Get FullName() As String
End Property
Public Property Get StoreNumber() As String
End Property
Public Property Get WorkstationID() As String
End Property

