VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OasysRoot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Client"
Attribute VB_Ext_KEY = "Member1" ,"OasysComManager"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
#Const ccDebug = 0
Implements IOasysRoot
Implements ISysRoot
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/OasysRoot.cls $
' $Revision: 55 $
' $Date: 26/05/04 12:11 $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' This class implements the IOasysRoot interface which is defined in the
' OasysStartCom.dll and so requires an early binding to it.
' The OasysDbInterface requires no early binding to anything
'----------------------------------------------------------------------------
Private Const OASYS_VERSION_STRING = "2.0"
Private Const OASYS_CLIENT_STRING = "Wickes"
Private Type tRootProps
    ' The Oasys root is instatiated once per program.  Within a VB
    ' executable this implies one user and one session.  Within a
    ' web-server environment it implies many users, each with a session.
    ' It is useful to determine which environment we have.
    bIsRunningAsWebServer As Boolean
End Type
Private Props               As tRootProps
Private m_oSession          As Session          ' member object
Private m_oClient           As Client           ' member object
Private m_oComManager       As OasysComManager  ' member object
Private m_strCommandLine    As String           ' Raw, as provided by the exe
Private m_colClassNames     As Collection       ' BO Class names by ClassId

Private Sub InitialiseBoClassnames()
    Set m_colClassNames = New Collection
    AddSequentialClassName "cActivityLog", CLASSID_ACTIVITYLOG
    AddSequentialClassName "cAUDITMASTER", CLASSID_AUDITMASTER
    AddSequentialClassName "cCardInfo", CLASSID_CARDINFO
    AddSequentialClassName "cCashBalanceControl", CLASSID_CASHBALANCECONTROL
    AddSequentialClassName "cCashBalanceHeader", CLASSID_CASHBALANCEHDR
    AddSequentialClassName "cCashBalanceSummary", CLASSID_CASHBALANCESUMMARY
    AddSequentialClassName "cCASHIER", CLASSID_CASHIER
    AddSequentialClassName "cCashierCashBal", CLASSID_CASHIERCASHBAL
    AddSequentialClassName "cCashierTotals", CLASSID_CASHIERTOTALS
    AddSequentialClassName "cConsignmentHeader", CLASSID_CONSHEADER
    AddSequentialClassName "CCountedSku", CLASSID_COUNTEDSKU
    AddSequentialClassName "cDeliveryNote", CLASSID_DELNOTEHEADER
    AddSequentialClassName "cDeliveryNoteLine", CLASSID_DELNOTELINE
    AddSequentialClassName "cDelNoteNarrative", CLASSID_DELNOTENARRATIVE
    AddSequentialClassName "cDeposit", CLASSID_DEPOSIT
    AddSequentialClassName "cDeptAnalsysis", CLASSID_DEPTANALYSIS
    AddSequentialClassName "cDespatchMethod", CLASSID_DESPATCHMETHOD
    AddSequentialClassName "cEAN", CLASSID_EAN
    AddSequentialClassName "cEDIChangeCode", CLASSID_EDICHANGECODE
    AddSequentialClassName "cEDIDelHeader", CLASSID_EDIDELHEADER
    AddSequentialClassName "cEDIDelLine", CLASSID_EDIDELLINE
    AddSequentialClassName "cEDIFileControl", CLASSID_EDIFILECONTROL
    AddSequentialClassName "cEDIPOEarly", CLASSID_EDIPOEARLY
    AddSequentialClassName "cEDIPOLate", CLASSID_EDIPOLATE
    AddSequentialClassName "cEDIPOLines", CLASSID_EDIPOLINES
    AddSequentialClassName "cEDIPOQtyDiscrepancy", CLASSID_EDIPOQTYDISCREPANCY
    AddSequentialClassName "cEDIPOQtyDiscrepancyLines", CLASSID_EDIPOQTYDISCREPANCYLINES
    AddSequentialClassName "cGrossMargins", CLASSID_GROSSMARGINS
    AddSequentialClassName "cHierarchyCategory", CLASSID_HIERARCHYCATEGORY
    AddSequentialClassName "cHierarchyGroup", CLASSID_HIERARCHYGROUP
    AddSequentialClassName "cHierarchyStyle", CLASSID_HIERARCHYSTYLE
    AddSequentialClassName "cHierarchySubgroup", CLASSID_HIERARCHYSUBGROUP
    AddSequentialClassName "cINVENTORY", CLASSID_INVENTORY
    AddSequentialClassName "cInventoryText", CLASSID_INVENTORYINFO
    AddSequentialClassName "CINVENTORYS", CLASSID_INVENTORYS
    AddSequentialClassName "cISTInEDIHeader", CLASSID_ISTINEDIHEADER
    AddSequentialClassName "cISTInEDILine", CLASSID_ISTINEDILINE
    AddSequentialClassName "cISTInHeader", CLASSID_ISTINHEADER
    AddSequentialClassName "cISTLine", CLASSID_ISTLINE
    AddSequentialClassName "cISTOutHeader", CLASSID_ISTOUTHEADER
    AddSequentialClassName "cITEMGROUP", CLASSID_ITEMGROUP
    AddSequentialClassName "cLineReasonNotes", CLASSID_LINECOMMENTS
    AddSequentialClassName "cLOCATIONTYPE", CLASSID_LOCATIONTYPE
    AddSequentialClassName "cMenuItem", CLASSID_MENU
    AddSequentialClassName "cNIGHTLOG", CLASSID_NIGHTLOG
    AddSequentialClassName "cNIGHTMASTER", CLASSID_NIGHTMASTER
    AddSequentialClassName "cEventDealGroup", CLASSID_OPDEALGROUP
    AddSequentialClassName "cOpenDrawerCode", CLASSID_OPENDRAWERCODE
    AddSequentialClassName "cEventHeader", CLASSID_OPHEADER
    AddSequentialClassName "cEventMaster", CLASSID_OPMASTER
    AddSequentialClassName "cEventMixMatch", CLASSID_OPMIXMATCH
    AddSequentialClassName "cPaidInCode", CLASSID_PAIDINCODE
    AddSequentialClassName "cPaidOutCode", CLASSID_PAIDOUTCODE
    AddSequentialClassName "cParameter", CLASSID_PARAMETER
    AddSequentialClassName "cPOLines", CLASSID_POLINES
    AddSequentialClassName "cPOSAction", CLASSID_POSACTION
    AddSequentialClassName "cEventTranLine", CLASSID_POSEVENTLINE
    AddSequentialClassName "cPOSHeader", CLASSID_POSHEADER
    AddSequentialClassName "cPOSLine", CLASSID_POSLINE
    AddSequentialClassName "cPOSMessage", CLASSID_POSMESSAGE
    AddSequentialClassName "cPOSPayment", CLASSID_POSPAYMENT
    AddSequentialClassName "cPRICECHANGE", CLASSID_PRICECHANGE
    AddSequentialClassName "cPriceCusInfo", CLASSID_PRICECUSINFO
    AddSequentialClassName "cPriceLineInfo", CLASSID_PRICELINEINFO
    AddSequentialClassName "cPriceOverrideCode", CLASSID_PRICEOVERRIDE
    AddSequentialClassName "cPRODUCTGROUP", CLASSID_PRODUCTGROUP
    AddSequentialClassName "cPurchaseConsLine", CLASSID_PURCHASECONSLINE
    AddSequentialClassName "cPurchaseOrder", CLASSID_PURCHASEORDER
    AddSequentialClassName "cPurchaseLine", CLASSID_PURCHASEORDERLINE
    AddSequentialClassName "cPurHdrNarrative", CLASSID_PURHDRNARRATIVE
    AddSequentialClassName "cRELATEDITEM", CLASSID_RELATEDITEM
    AddSequentialClassName "cRetailOptions", CLASSID_RETAILEROPTIONS
    AddSequentialClassName "cRetNoteHeader", CLASSID_RETNOTEHEADER
    AddSequentialClassName "cReturnCode", CLASSID_RETURNCODE
    AddSequentialClassName "cReturnCust", CLASSID_RETURNCUST
    AddSequentialClassName "cReturnHeader", CLASSID_RETURNHDR
    AddSequentialClassName "cReturnLine", CLASSID_RETURNLINE
    AddSequentialClassName "cReturnNote", CLASSID_RETURNNOTE
    AddSequentialClassName "cReturnNoteLine", CLASSID_RETURNNOTELINE
    AddSequentialClassName "cSalesCustomer", CLASSID_SALESCUSTOMER
    AddSequentialClassName "cSalesLedger", CLASSID_SALESLEDGER
    AddSequentialClassName "cSaleTotals", CLASSID_SALETOTALS
    AddSequentialClassName "cStockAdjustmentCode", CLASSID_STOCKADJCODE
    AddSequentialClassName "cStockAdjustmentPend", CLASSID_STOCKADJPEND
    AddSequentialClassName "cStockAdjustReason", CLASSID_STOCKADJREASON
    AddSequentialClassName "cStockAdjustment", CLASSID_STOCKADJUST
    AddSequentialClassName "cStockCountHeader", CLASSID_STOCKCOUNTHEADER
    AddSequentialClassName "cStockCountItem", CLASSID_STOCKCOUNTLINE
    AddSequentialClassName "cStockLevels", CLASSID_STOCKLEVEL
    AddSequentialClassName "cSTOCKLOCATION", CLASSID_STOCKLOCATION
    AddSequentialClassName "cStockOtherLocation", CLASSID_STOCKLOCATION2
    AddSequentialClassName "cStockLog", CLASSID_STOCKLOG
    AddSequentialClassName "cStockMovement", CLASSID_STOCKMOVE
    AddSequentialClassName "cStockTake", CLASSID_STOCKTAKE
    AddSequentialClassName "cStockWriteOff", CLASSID_STOCKWOF
    AddSequentialClassName "CStore", CLASSID_STORE
    AddSequentialClassName "cStoreStatus", CLASSID_STORESTATUS
    AddSequentialClassName "cSupplier", CLASSID_SUPPLIER
    AddSequentialClassName "cSUPPLIERS", CLASSID_SUPPLIERS
    AddSequentialClassName "cSystemDates", CLASSID_SYSTEMDATES
    AddSequentialClassName "cSystemNumbers", CLASSID_SYSTEMNO
    AddSequentialClassName "cSystemOptions", CLASSID_SYSTEMOPTIONS
    AddSequentialClassName "cEDILogEntry", CLASSID_TCFMASTER
    AddSequentialClassName "cTenderControl", CLASSID_TENDERCONTROL
    AddSequentialClassName "cTenderOptions", CLASSID_TENDEROPTIONS
    AddSequentialClassName "cTillTotals", CLASSID_TILLTOTALS
    AddSequentialClassName "cTransactionDiscount", CLASSID_TRANSACTION_DISCOUNT
    AddSequentialClassName "cTypesOfSaleControl", CLASSID_TYPESOFSALECONTROL
    AddSequentialClassName "cTypesOfSaleOptions", CLASSID_TYPESOFSALEOPTIONS
    AddSequentialClassName "cTypesOfSaleTenders", CLASSID_TYPESOFSALETENDERS
    AddSequentialClassName "cUNITOFMEASURE", CLASSID_UNITOFMEASURE
    AddSequentialClassName "cUser", CLASSID_USER
    AddSequentialClassName "cVATRates", CLASSID_VATRATES
    AddSequentialClassName "cWorkStation", CLASSID_WORKSTATIONCONFIG
    AddSequentialClassName "cInventoryWickes", CLASSID_INVENTORYWICKES
    AddSequentialClassName "cPOSMissingEAN", CLASSID_POSMISSINGEAN
    AddSequentialClassName "cRefundCode", CLASSID_REFUNDCODE
    AddSequentialClassName "cTenderOverride", CLASSID_TENDEROVERRIDE
    AddSequentialClassName "cZreads", CLASSID_ZREADS
    AddSequentialClassName "cPOSGiftVoucher", CLASSID_POSGIFTVOUCHERS
    AddSequentialClassName "cCustOrderHeader", CLASSID_CUSTORDERHEADER
    AddSequentialClassName "cCustOrderLine", CLASSID_CUSTORDERLINE
    AddSequentialClassName "cGiftVoucherHotFile", CLASSID_GIFTVOUCHERHOTFILE
    AddSequentialClassName "cEventHSExclusions", CLASSID_EVENTHSEXCLUSIONS
    AddSequentialClassName "cColleagueCardHF", CLASSID_COLLEAGUECARDHOTFILE
    AddSequentialClassName "cQuoteHeader", CLASSID_QUOTEHEADER
    AddSequentialClassName "cQuoteLine", CLASSID_QUOTELINE
    AddSequentialClassName "cCustomer", CLASSID_CUSTOMER
    AddSequentialClassName "cWebEvent", CLASSID_WEBEVENT
    AddSequentialClassName "cWebHeader", CLASSID_WEBHEADER
    AddSequentialClassName "cWebLine", CLASSID_WEBLINE
    AddSequentialClassName "cWebPick", CLASSID_WEBPICK
    AddSequentialClassName "cWebPaid", CLASSID_WEBPAID
    AddSequentialClassName "cPrompts", CLASSID_PROMPT
    AddSequentialClassName "cGroupPrompts", CLASSID_GROUP_PROMPT
    AddSequentialClassName "cItemGrpPrompts", CLASSID_ITEM_GRPPROMPT
    AddSequentialClassName "cPIMItem", CLASSID_PIM_ITEM
    AddSequentialClassName "cColleagueCards", CLASSID_COLLEAGUECARD
    AddSequentialClassName "cTempFil", CLASSID_TEMPFIL
    AddSequentialClassName "cExchangeRate", CLASSID_EXCHANGE_RATE
    AddSequentialClassName "cCurrencies", CLASSID_CURRENCY
    AddSequentialClassName "cWEEERate", CLASSID_WEEE_RATE
    AddSequentialClassName "cPlangram", CLASSID_PLANGRAM
    AddSequentialClassName "cBarcodeBroken", CLASSID_BARCODEFAILED
    AddSequentialClassName "cEANCheck", CLASSID_EAN_CHECK
    AddSequentialClassName "cSKUReject", CLASSID_TRANREJECT
    AddSequentialClassName "cLineReversalCode", CLASSID_LINEREVERSAL
    AddSequentialClassName "cDTFMsgSend", CLASSID_DTF_SEND
    AddSequentialClassName "cDiscountCardScheme", CLASSID_DISCOUNTCARD_SCHEME_ID
    AddSequentialClassName "cDiscountCardList", CLASSID_DISCOUNTCARD_LIST
    AddSequentialClassName "cCustOrderText", CLASSID_ORDERTEXT
    AddSequentialClassName "cCustOrderInfo", CLASSID_CUSTORDERINFO
    AddSequentialClassName "cSupplierDet", CLASSID_SUPPLIERDET
    AddSequentialClassName "cMarkDownStock", CLASSID_MARKDOWNSTOCK
    AddSequentialClassName "cConResHeader", CLASSID_CONRESHEADER
    AddSequentialClassName "cConResDetail", CLASSID_CONRESDETAIL
    AddSequentialClassName "cCouponMaster", CLASSID_COUPONFMT
    AddSequentialClassName "cSoldCoupon", CLASSID_SOLDCOUPON
    AddSequentialClassName "cDayPrice", CLASSID_DAYPRICE
    AddSequentialClassName "cCouponText", CLASSID_COUPONTEXT
    AddSequentialClassName "cPosPaymentComm", CLASSID_POSPAYMENTCOMM
    AddSequentialClassName "cCustomerRefund", CLASSID_CUST_REFUND
    Debug.Print "Number of classnames = " & m_colClassNames.Count
End Sub
Private Function AddSequentialClassName(strName As String, nClassId As Integer)
    m_colClassNames.Add strName
    If nClassId <> m_colClassNames.Count Then
        Err.Raise OASYS_ERR_INCONSISTENT_OASYS_SETUP, "OasysRoot::Class_Initialize() ", "Business Object Id out of sequence for: " & strName
    End If
End Function

Private Sub Class_Initialize()
    IsRunningAsWebServer = False
    ' Create ComManager here as we'll use it frequently
    If ComManager Is Nothing Then
        Err.Raise OASYS_ERR_ROOT_NOT_INITIALISED, "OasysRoot::Class_Initialize() ", "ComManager cannot be created."
    End If
    
    ' Now check if we have the correct version of the OasysBaseCom.dll
    ' compared to the expected version the Installer noted in the registry.
    Dim oRegistry As oasyscbasecomlib.Registry
    
    Dim strKey, strValue As String
    Dim nResult As Long
    Set oRegistry = ComManager.GetComObject("Registry")
    strKey = RelativeRegistryKey
    oRegistry.GetCTSStringValue strKey, "ComVersion", strValue, nResult
    If nResult = 0 Then
        If strValue <> OASYS_BASE_COM_VERSION Then
            Err.Raise OASYS_ERR_ROOT_NOT_INITIALISED, "OasysRoot::Class_Initialize() ", _
                "OasysBaseCom version " & strValue & _
                " required but we have version " & OASYS_BASE_COM_VERSION
        End If
        Debug.Print "Root using COM version " & OASYS_BASE_COM_VERSION
    Else
        ' Nothing in registry, so let it pass I guess
    End If
    
    InitialiseBoClassnames
    
    Set oRegistry = Nothing
End Sub

Private Sub Class_Terminate()
    Set m_oClient = Nothing
    Set m_oComManager = Nothing
End Sub

Friend Function Initialise(Optional strCommandLine As String, Optional strQualifier As String) As Boolean
#If ccDebug = 1 Then
    MsgBox "Root Initialise() in app: " & App.EXEName
#End If
    ' As strCommandLine is not a variant, it will be "" if not passed
    m_strCommandLine = strCommandLine
    ' First time through the m_oClient member will be unset
    Initialise = (m_oClient Is Nothing)
    If Initialise Then
        ' Note that Client may use Session so create in session first!
        Set m_oSession = CreateSession
        ' Uninitialised, so initialise with Client() get
        Initialise = Not (IClient Is Nothing)
    Else
        ' Already initialised so Init has been called a second time.
        Err.Raise OASYS_ERR_ROOT_NOT_INITIALISED, "OasysRoot::Initialise()", "Initialise() or WebInitialise() previously called on Root, cannot repeat."
    End If
    If Initialise Then
        Debug.Print "Root Initialised Ok"
    Else
        Debug.Print "Root Initialise Fail******"
    End If
End Function

Friend Property Get BoClassNameFromFid(nFid As Long) As String
    ' ClassId is the topmost int (2 bytes)
    BoClassNameFromFid = BoClassName(nFid \ &H10000)
End Property
Friend Property Get BoClassName(nClassId As Integer) As String
    If nClassId > m_colClassNames.Count Then
        Err.Raise OASYS_ERR_INCONSISTENT_OASYS_SETUP, "OasysRoot::BoClassName() ", "Business Object Id " & nClassId & " > limit of " & m_colClassNames.Count
    Else
        BoClassName = m_colClassNames.Item(nClassId)
    End If
End Property

Friend Function CreateSession(Optional strCommandLine As String) As Session
    Dim oSession As Session
    If LenB(strCommandLine) = 0 Then
        ' Not passed or passed empty value, which implies 'use default'
        strCommandLine = m_strCommandLine
    End If
    ' Create object
    Set oSession = ComManager.CreateComObject("Session")
    ' We set the session immediately, as the session is referenced within initialise().
    ' First time, if no Client is set in the registry, we need to check IsInteractive
    ' and if so we can give a user dialog for an interactive choice, else we fail.
    ' to let the user choose which Client to select
    Set CreateSession = oSession
    If (oSession.Initialise(Me, strCommandLine)) Then
        IsRunningAsWebServer = oSession.IsRunningAsWebServer
        Debug.Print "Created session Ok"
    Else
        Debug.Print "Failed to Create session"
        Debug.Assert False
        Set CreateSession = Nothing
    End If
End Function

Friend Function CreateBusinessObject(nClassId As Integer, oSession As Session) As IBo
    Set CreateBusinessObject = oSession.Database.CreateBusinessObject(nClassId)
'    Set CreateBusinessObject = oSession.CreateBusinessObject(nClassId)
'    Dim oDefault As Object
'    Dim oBo As IBo
'    ' Create object
'    Set oBo = ComManager.CreateComObject(BoClassName(nClassId))
'    Set oDefault = oBo.Initialise(oSession)
'    If oBo Is Nothing Then
'        Err.Raise OASYS_ERR_INCONSISTENT_OBJECT, "OasysRoot::CreateBusinessObject()", "Requested Object Id " & nClassId & " Initialise returning Nothing"
'    End If
'    If nClassId <> oBo.ClassId Then
'        Err.Raise OASYS_ERR_INCONSISTENT_OBJECT, "OasysRoot::CreateBusinessObject()", "Requested Object Id " & nClassId & " but got " & oBo.ClassId
'    End If
'    Set CreateBusinessObject = oDefault
'    Debug.Print "Created object Ok, ClassId: " & oBo.ClassId & " " & oBo.ClassName
End Function
Public Function CreateBoColFromClassID(nClassId As Integer, oSession As ISession) As Collection

Dim oDefault As IBo
Dim oBO      As IBo
Dim oBOCol   As Collection
    
    ' Create object
    Set oBO = ComManager.CreateComObject(BoClassName(nClassId))
    Set oDefault = oBO.Initialise(oSession)
    If nClassId <> oBO.ClassId Then
        Err.Raise OASYS_ERR_INCONSISTENT_OBJECT, "OasysRoot::CreateBusinessObject()", "Requested Object Id " & nClassId & " but got " & oBO.ClassId
    End If
    
    ' Pass the selector to the database to get the data view
    Set oBOCol = m_oSession.Database.GetBoCollection(oBO.GetSelectAllRow)

    Set CreateBoColFromClassID = oBOCol
    Debug.Print "Created Bo Collection Ok, ClassId: " & oBO.ClassId & " " & oBO.ClassName & " No." & oBOCol.Count

End Function
Friend Static Function CreateBoFromRow(oRow As IRow, oSession As Session) As IBo
    Set CreateBoFromRow = oSession.Database.CreateBoFromRow(oRow)
'    Set CreateBoFromRow = oSession.CreateBoFromRow(oRow)
'    ' Note we return the default object interface, Not the IBo interface.
'    Dim nClassId As Integer
'    Dim oSysBo As ISysBo
'    Debug.Assert Not (oSession Is Nothing)
'    If oRow Is Nothing Then
'        Debug.Assert False
'        Set CreateBoFromRow = Nothing
'        Exit Function
'    End If
'    nClassId = oRow.Field(1).IField_Id \ &H10000
'    Set CreateBoFromRow = CreateBusinessObject(nClassId, oSession)
'    If Not CreateBoFromRow Is Nothing Then
'        Set oSysBo = CreateBoFromRow
'        oSysBo.LoadFromRow oRow
'    End If
End Function
Private Function GetStaticBoFromCid(nClassId As Integer) As IBo
' Get a Bo that matches the Class Id, BUT ONLY USE 'static' METHODS
' that do not change or reference the state of the Bo, which may be
' indeterminate or in use elsewhere in the system.
' Exception is GetField which returns an IField object of indeterminate
' value, which we must ensure is ignored or changed to a known value.
    Dim oBO                 As IBo
    Dim strName             As String
    Dim bWasInstantiated    As Boolean
    strName = BoClassName(nClassId)
    bWasInstantiated = ComManager.IsInstantiated(strName)
    ' Do a get rather than a Create.
    Set oBO = ComManager.GetComObject(strName)
    If Not (oBO Is Nothing) Then
        If Not bWasInstantiated Then
            ' We have created a Business Object and it requires Initialising
            ' We don't have a 'user' session but for getting a field we
            ' can simply use the Root session as context is not used.
            oBO.Initialise m_oSession
        End If
    End If
    Set GetStaticBoFromCid = oBO
End Function
Private Function CreateFieldFromFid(nFid As Long) As IField
    Dim oBO                 As IBo
    Set oBO = GetStaticBoFromCid(nFid \ &H10000)
    If Not (oBO Is Nothing) Then
        Set CreateFieldFromFid = oBO.GetField(nFid)
    End If
End Function

Friend Function CreateUtilityObject(strClass As String) As Object
    ' Create object
    Set CreateUtilityObject = ComManager.CreateComObject(strClass)
    Debug.Print "Created Utility object Ok: " & strClass
End Function

Friend Function CreateDatabase(Optional strType As String) As IBoDatabase
    ' Create object
    Set CreateDatabase = ComManager.CreateComObject("CBoDatabase")
    Debug.Print "Created Utility object Ok: " & "CBoDatabase"
End Function

Friend Property Get ComManager() As OasysComManager
    If m_oComManager Is Nothing Then
        ' Ensure we load the ComManager from the current dll
        Set m_oComManager = New OasysComManager
        If m_oComManager Is Nothing Then
            Err.Raise OASYS_ERR_ROOT_NOT_INITIALISED, "OasysRoot::ComManager() ", "ComManager cannot be created."
        Else
            m_oComManager.Initialise Me
        End If
    End If
    Set ComManager = m_oComManager
End Property
Friend Property Get SystemSession() As Session
    ' This session is local and just for the OasysBaseCom friend classes
    ' Created object in Initialise()
    Set SystemSession = m_oSession
End Property
Friend Property Get IClient() As IClient
    ' Create object just in time.
    If m_oClient Is Nothing Then
        Set m_oClient = m_oComManager.GetComObject("Client")
        m_oClient.Initialise Me
    End If
    Set IClient = m_oClient
End Property
Friend Property Get IVClient() As IVClient
    On Error Resume Next
    Set IVClient = IClient
    If Err.Number <> 0 Then
        Set IVClient = Nothing
    End If
End Property
Friend Property Get Version() As String
     Version = OASYS_VERSION_STRING
End Property
Friend Property Get Client() As String
    Client = OASYS_CLIENT_STRING
End Property
Friend Property Get RelativeRegistryKey() As String
    RelativeRegistryKey = "Oasys\" & Version & "\" & Client
End Property
Friend Property Get IsRunningAsWebServer() As Boolean
     IsRunningAsWebServer = Props.bIsRunningAsWebServer
End Property
Private Property Let IsRunningAsWebServer(bVal As Boolean)
    Props.bIsRunningAsWebServer = bVal
End Property
Friend Property Get CurrentSession() As ISession
    CurrentSession = m_oSession
End Property


'----------------------------------------------------------------------------
' Interface ISysRoot
'----------------------------------------------------------------------------
Public Function ISysRoot_CreateBoFromRow(oRow As IRow, oSession As ISession) As IBo
    Set ISysRoot_CreateBoFromRow = CreateBoFromRow(oRow, oSession)
End Function
Public Static Function ISysRoot_CreateFieldFromFid(nFid As Long) As IField
    Set ISysRoot_CreateFieldFromFid = CreateFieldFromFid(nFid)
End Function
Public Property Get ISysRoot_IOasysRoot() As IOasysRoot
    Set IOasysRoot = Me
End Property
Public Function ISysRoot_CreateUninitialisedBusinessObject(nClassId As Integer, oSysBo As ISysBo) As IBo
    ' We have been called by a system component that may not be early linked and so we coerce interfaces
    ' in here.  We return IBo and explicitly set ISysBo as an output parameter.
    Dim oDefault    As IBo
    Dim oBO         As IBo
    Dim oRealSysBo  As ISysBo
    Set oDefault = ComManager.CreateComObject(BoClassName(nClassId))
    ' Get the Bo and system interfaces, even if it is nothing
    Set oBO = oDefault
    Set oRealSysBo = oDefault
    Set oSysBo = oRealSysBo
    Set ISysRoot_CreateUninitialisedBusinessObject = oBO
End Function
Public Property Get ISysRoot_BoClassName(nClassId As Integer) As String
    ISysRoot_BoClassName = BoClassName(nClassId)
End Property
Public Function ISysRoot_GetStaticBoFromCid(nClassId As Integer) As IBo
    Set ISysRoot_GetStaticBoFromCid = GetStaticBoFromCid(nClassId)
End Function

'----------------------------------------------------------------------------
' Interface IOasysRoot
'----------------------------------------------------------------------------
Public Function IOasysRoot_Initialise(Optional strCommandLine As String, Optional strQualifier As String) As Boolean
    IOasysRoot_Initialise = Initialise(strCommandLine, strQualifier)
End Function
Public Function IOasysRoot_CreateSession(Optional strCommandLine As String) As ISession
    Set IOasysRoot_CreateSession = CreateSession(strCommandLine)
End Function
Public Function IOasysRoot_CreateBusinessObject(nClassId As Integer, oSession As ISession) As IBo
    ' Please be aware that this method is now obsolete and its use
    ' deprecated.  Kindly use the IBoDatabase method CreateBusinessObject
    ' or we'll send the boys round.  As an example, see cDepartment class.
    Debug.Assert False
    Set IOasysRoot_CreateBusinessObject = CreateBusinessObject(nClassId, oSession)
End Function
Public Function IOasysRoot_CreateUtilityObject(strClass As String) As Object
    Set IOasysRoot_CreateUtilityObject = CreateUtilityObject(strClass)
End Function
Public Property Get IOasysRoot_Client() As IClient
    If IsRunningAsWebServer Then
        Set IOasysRoot_Client = IVClient()
    Else
        Set IOasysRoot_Client = IClient()
    End If
End Property
Public Property Get IOasysRoot_Version() As String
    IOasysRoot_Version = Version()
End Property
Public Property Get IOasysRoot_IsRunningAsWebServer() As Boolean
    IOasysRoot_IsRunningAsWebServer = IsRunningAsWebServer()
End Property

Public Property Get IOasysRoot_CurrentSession() As Session
    Set IOasysRoot_CurrentSession = CurrentSession()
End Property
Public Property Get IOasysRoot_RelativeRegistryKey() As String
    IOasysRoot_RelativeRegistryKey = RelativeRegistryKey()
End Property
Public Property Get IOasysRoot_SystemSession() As ISession
    Set IOasysRoot_SystemSession = SystemSession
End Property
Public Function IOasysRoot_CreateDatabase(Optional strType As String) As OasysInterfaces_Wickes.IBoDatabase
    ' Create object
    Set IOasysRoot_CreateDatabase = ComManager.CreateComObject("CBoDatabase")
    Debug.Print "Created Utility object Ok: " & "CBoDatabase"
End Function
Public Function IOasysRoot_CreateBoColFromClassID(nClassId As Integer, oSession As ISession) As Collection
    'Added 27/9/02 M.Milne
    Set IOasysRoot_CreateBoColFromClassID = CreateBoColFromClassID(nClassId, oSession)
End Function


