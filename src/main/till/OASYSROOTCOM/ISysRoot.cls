VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ISysRoot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/ISysRoot.cls $
' $Revision: 5 $
' $Date: 15/10/02 15:35 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

'----------------------------------------------------------------------------
' Interface ISysRoot - Oasys system interface
' This interface is for system modules and must be kept hidden from
' user programs which can only use the IOasysRoot interface.
'----------------------------------------------------------------------------
Public Static Function CreateBoFromRow(oRow As Object, oSession As Object) As Object
End Function
Public Static Function CreateFieldFromFid(nFid As Long) As Object
End Function
Public Static Function GetStaticBoFromCid(nClassId As Integer) As Object
End Function
Public Property Get IOasysRoot() As IOasysRoot
' Change interface
End Property
Public Function CreateUninitialisedBusinessObject(nClassId As Integer, oSysBo As Object) As Object
End Function
Public Property Get BoClassName(nClassId As Integer) As String
End Property

