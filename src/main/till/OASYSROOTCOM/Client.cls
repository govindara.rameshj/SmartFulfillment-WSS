VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Client"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Member0" ,"Enterprise"
Option Explicit
Implements IClient
Implements IVClient
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/Client.cls $
' $Revision: 14 $
' $Date: 30/10/02 8:10 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Client object must not reference the Session object until it has
' been initialised, as the Session object may reference the Client
' whilst it initialises.
'----------------------------------------------------------------------------
Private oRoot As OasysRoot      ' Useful object reference

Private Type tClientProps
    strMnemonic As String
    Data As tClientBaseRecV1
End Type
Private Props As tClientProps
Private m_collEnterprises As Collection
'----------------------------------------------------------------------------

Private Sub Class_Initialize()
    Set m_collEnterprises = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_collEnterprises = Nothing
End Sub

Friend Function Initialise(Root As OasysRoot) As Boolean
    Set oRoot = Root
    Dim strClass As String
    Dim oTailor As ITailoredData ' OasysClientGeneric.TailoredData
    Dim udtTailorRec As tTailoringDataRec
    strClass = "OasysClient" & Mnemonic() & "_" & oRoot.Client & ".TailoredData"
    Set oTailor = CreateObject(strClass)
    oTailor.Initialise oRoot
    ' First we get the Client's tailored data
    Props.Data = oTailor.Record(TAILOR_CONTEXT_CLIENT).varBuffer
    Dim oEnterprise As Enterprise
    Dim nId As Integer
    For nId = 1 To oTailor.RecordCount(TAILOR_CONTEXT_ENTERPRISE)
        ' Ensure we load the Enterprise object from the current dll
        Set oEnterprise = oRoot.CreateUtilityObject("Enterprise")
        If oEnterprise.Initialise(oRoot, nId, oTailor) Then
            m_collEnterprises.Add oEnterprise, oEnterprise.Mnemonic
        End If
        Set oEnterprise = Nothing
    Next nId
    ' Finished with tailored data object, now Enterprise has used it.
    Set oTailor = Nothing
    Debug.Print "Client Initialised Ok"
    Initialise = True
End Function

Friend Property Get Mnemonic() As String
    If LenB(Props.Data.strMnemonic) = 0 Then
        If oRoot Is Nothing Then
            Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "Client::ClientMnemonic() ", "Root reference unset."
        Else
#If ccLateBound = 1 Then
            Dim oRegistry As Object
            Debug.Print "Client LateBound Registry"
#Else
            Dim oRegistry As oasyscbasecomlib.Registry
            Debug.Print "Client Early bound Registry"
#End If
            Dim nResult As Long
            Set oRegistry = oRoot.ComManager.GetComObject("Registry")
            oRegistry.GetCTSStringValue RelativeRegistryKey, "Mnemonic", Props.Data.strMnemonic, nResult
            If nResult <> 0 Then
                ' There is no Mnemonic in the registry, which implies that
                ' the product has not been formally installed or run yet.
                ' So we can default or ask the user to choose.
                Props.Data.strMnemonic = DefaultInstallMnemonic
                Debug.Print "No registry value for client mnemonic."
                oRegistry.SetCTSStringValue RelativeRegistryKey, "Mnemonic", Props.Data.strMnemonic, nResult
                If nResult <> 0 Then
                    Debug.Print "Failed to update registry"
                End If
            End If
            Set oRegistry = Nothing
            Debug.Print "Client mnemonic: " & Props.Data.strMnemonic
        End If
    End If
    Mnemonic = Props.Data.strMnemonic
End Property

Friend Property Get RelativeRegistryKey() As String
    If oRoot Is Nothing Then
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "Client::RelativeRegistryKey() ", "Root reference unset."
        RelativeRegistryKey = vbNullString
    Else
        RelativeRegistryKey = oRoot.RelativeRegistryKey & "\Client"
    End If
End Property

Friend Property Get Enterprise(Optional nId As Integer) As Enterprise
    If nId = 0 Then
        ' Optional param will be present (with value 0) if not a variant
        ' So either the user put 0 or optional param is missing
        nId = 1
    End If
    If (nId = 0 Or nId > EnterpriseCount) Then
        Debug.Assert False
        Err.Raise OASYS_ERR_INVALID_ENTERPRISE_ID, "Client::Enterprise() ", "Invalid Enterprise id=" & nId & ", must be 1 to " & m_collEnterprises.Count
        Set Enterprise = Nothing
    Else
        Set Enterprise = m_collEnterprises.Item(nId)
    End If
End Property

Friend Property Get EnterpriseCount() As Integer
    EnterpriseCount = m_collEnterprises.Count
End Property

Private Property Get DefaultInstallMnemonic() As String
    ' Oasys always needs to load a Client and does this via the Mnemonic
    ' which is held in the registry.  However, when Oasys hasn't been
    ' formally installed or run there is nothing there.  This is fine
    ' at a customer site I guess, but not too handy during testing when
    ' we want the system to be self-configuring where possible.
    ' So this method provides the default which will get written to
    ' the registry by the Client object so we won't come here again.
    
    ' Originally, we only checked the list of clients if we are interactive,
    ' but better to check the list and ask if we are interactive but simply take
    ' the first we find otherwise.  This makes for better resilience.
        
    Dim strClass As String
    Dim strMnemonics() As String
    Dim oTailor As ITailoredData
    Dim i As Integer
    strMnemonics = Split("Topps,Cha,FTG,Generic_Wickes", ",")
    For i = 0 To UBound(strMnemonics)
        strClass = "OasysClient" & strMnemonics(i) & ".TailoredData"
        On Error Resume Next
        Set oTailor = CreateObject(strClass)
        Select Case Err.Number
        Case Is = 0
        Case Is = 429
            ' Active X can't create object, ie. Not found
            'MsgBox "Active X can't create object: " & strClass
        Case Else
            Err.Source = "Client::DefaultInstallMnemonic()" & _
                vbCrLf & strClass
            Err.Raise Err.Number
        End Select
        On Error GoTo 0
        If Not (oTailor Is Nothing) Then
            ' We have a dll available to use
            Set oTailor = Nothing
            If oRoot.SystemSession.IsInteractive Then
                ' Hmm, we are interactive, so we could ask the user to choose
                Select Case (MsgBox( _
                    "Configure for " & strMnemonics(i) & "?", _
                    vbYesNoCancel + vbSystemModal, _
                    "Oasys Auto-configure"))
                Case Is = vbYes
                    ' Go with this strClass
                    DefaultInstallMnemonic = strMnemonics(i)
                    Exit For
                Case Is = vbNo
                    ' No this class, but continue to the next
                Case Else
                    ' Bomb out to give an error
                    Exit For
                End Select
            Else
                ' Not interactive, so we simply accept the first we have found.
                ' Go with this strClass
                DefaultInstallMnemonic = strMnemonics(i)
            End If
        End If
    Next i
    If oRoot.SystemSession.IsInteractive Then
        ' Let the usr know what has happened
        If LenB(DefaultInstallMnemonic) = 0 Then
            MsgBox "No client selected"
        Else
            MsgBox "Selected client: " & DefaultInstallMnemonic
        End If
    End If
    If LenB(DefaultInstallMnemonic) = 0 Then
        ' Nothing set so raise an error
        Err.Raise OASYS_ERR_NOT_INSTALLED, "Client::DefaultInstallMnemonic()", _
            "Client Mnemonic not set up in the Registry"
    End If
End Property

'----------------------------------------------------------------------------
' Interface IClient - using types Version 1
'----------------------------------------------------------------------------
Public Property Get IClient_Mnemonic() As String
    IClient_Mnemonic = Mnemonic
End Property
Public Property Get IClient_Enterprise(Optional nId As Integer) As IEnterprise
    Set IClient_Enterprise = Enterprise(nId)
End Property
Public Property Get IClient_EnterpriseCount() As Integer
    IClient_EnterpriseCount = EnterpriseCount
End Property
Public Property Get IClient_RelativeRegistryKey() As String
    ' This property is only available via IClient for use by ITailoredData objects
    IClient_RelativeRegistryKey = RelativeRegistryKey
End Property

'----------------------------------------------------------------------------
' Interface IVClient - using Variants Version 1
'----------------------------------------------------------------------------
Public Property Get IVClient_Mnemonic() As String
    IVClient_Mnemonic = Mnemonic
End Property
Public Property Get IVClient_Enterprise(Optional nId As Variant) As IVEnterprise
    If IsMissing(nId) Then
        Set IVClient_Enterprise = Enterprise(1)
    Else
        Set IVClient_Enterprise = Enterprise(CInt(nId))
    End If
End Property
Public Property Get IVClient_EnterpriseCount() As Integer
    IVClient_EnterpriseCount = EnterpriseCount
End Property
Public Property Get IVClient_test(oClient As IClient) As String
    Dim c As Client
    On Error Resume Next
    Set c = oClient
    If Err.Number <> 0 Then
        IVClient_test = "Error " & Err.Number & "  " & Err.Description
    Else
        Dim i As Integer
        i = 2
        On Error Resume Next
        IVClient_test = c.Enterprise(i).ShortName
        If Err.Number <> 0 Then
            IVClient_test = "Error2 " & Err.Number & "  " & Err.Description
        End If
    End If
End Property

