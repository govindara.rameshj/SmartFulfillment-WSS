VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /VB/OasysRootCom/IClient.cls $
' $Revision: 4 $
' $Date: 14/08/02 12:33 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Typed Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

Public Property Get Mnemonic() As String
End Property
Public Property Get Enterprise(Optional nId As Integer) As IEnterprise
End Property
Public Property Get EnterpriseCount() As Integer
End Property
Public Property Get RelativeRegistryKey() As String
    ' This property is only available via IClient for use by ITailoredData objects
End Property


