VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StartRoot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /VB/OasysRootCom/StartRoot.cls $
' $Revision: 2 $
' $Date: 14/08/02 12:33 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' OasysRoot can only be created by a friend and this is the
' friend that creates the root for OasysStartCom.
'----------------------------------------------------------------------------
Public Property Get IOasysRoot() As IOasysRoot
    Set IOasysRoot = New OasysRoot
End Property
