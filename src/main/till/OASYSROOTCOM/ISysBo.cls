VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ISysBo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
#Const ccEarlyBinding = 0
'
' $Archive: /VB/OasysRootCom/ISysBo.cls $
' $Revision: 1 $
' $Date: 16/08/02 9:02 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' The interface definition for use by the System only
'----------------------------------------------------------------------------

Public Function LoadFromRow(oRow As Object) As Boolean
    ' Friend: for use by system.  Where we implicitly assume oRow As CRow
End Function


