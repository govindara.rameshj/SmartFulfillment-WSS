VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBoSuperClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/CBoSuperClass.cls $
' $Revision: 6 $
' $Date: 22/11/02 15:35 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Logically, the IBo interfaces to this CBoSuperclass from which all the
' functional Business Objects are sub-classed.  VB doesn't directly support this.
' We simulate it with the IBo interfacing to the Bo sub-class and the sub-class
' containing the superclass as a member, whose methods it uses where it doen't
' want to override them.
'----------------------------------------------------------------------------
Option Explicit
Implements IBo
Implements ISysBo

Private Enum enSaveType              ' Must match enum in enSaveType in IBoDatabase
    SaveTypeAllCases = 0             ' Default value
    SaveTypeIfNew = 1
    SaveTypeIfExists = 2
    SaveTypeDelete = 3
End Enum

Public m_oSession       As Session      ' Public, so accessible to the sub-class
Public m_oSubClass      As IBo       ' Must be the native interface NOT IBo

'----------------------------------------------------------------------------
' Native Interface
'----------------------------------------------------------------------------

Private Function RaiseErrorNotImplemented()
    Err.Raise OASYS_ERR_NOT_IMPLEMENTED, "CBoSuperClass", _
                "CBoSuperClass" & vbCrLf & "Function needs overriding in: " & m_oSubClass.ClassName
End Function

Public Function CheckIsUnInitialised()
    ' Return true if there is a problem
    If m_oSession Is Nothing Then
        MsgBox "Invalid " & m_oSubClass.ClassName & " object; use IDatabase.CreateBusinessObject()"
        CheckIsUnInitialised = True
    End If
End Function

Public Function LoadLongArray(nValue() As Long, oGroupField As CFieldGroup) As Boolean
    ' Helper function for LoadFromRow
    Dim iRowNo As Integer
    Dim oRow   As IRow
    Dim oMyField As CFieldLong

    Debug.Assert (oGroupField.IField_FieldType = enFieldType.FieldTypeGroup)
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Debug.Assert False
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.LongValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.LongValue
        Next iRowNo
    End If
    
End Function

Public Function LoadStringArray(nValue() As String, oGroupField As CFieldGroup) As Boolean
    ' Helper function for LoadFromRow
    Dim iRowNo As Integer
    Dim oRow   As IRow
    Dim oMyField As CFieldString

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Debug.Assert False
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.StringValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.StringValue
        Next iRowNo
    End If
    
End Function

Public Function LoadDoubleArray(nValue() As Double, oGroupField As CFieldGroup) As Boolean
    ' Helper function for LoadFromRow
    Dim iRowNo As Integer
    Dim oRow   As IRow
    Dim oMyField As CFieldDouble

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.DoubleValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.DoubleValue
        Next iRowNo
    End If

End Function

Public Function LoadDateArray(nValue() As Date, oGroupField As CFieldGroup) As Boolean
    ' Helper function for LoadFromRow
    Dim iRowNo As Integer
    Dim oRow   As IRow
    Dim oMyField As CFieldDate

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Debug.Assert False
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.DateValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.DateValue
        Next iRowNo
    End If

End Function

Public Function LoadBooleanArray(nValue() As Boolean, oGroupField As CFieldGroup) As Boolean
    ' Helper function for LoadFromRow
    Dim iRowNo As Integer
    Dim oRow   As IRow
    Dim oMyField As CFieldBool

    Debug.Assert (oGroupField.IField_FieldType = 6)    ' 6 = FieldTypeGroup
    Set oRow = oGroupField.GroupRow
    iRowNo = oGroupField.Subscript
    If iRowNo > 0 Then
        ' We pick just one item from the array
        Debug.Assert False
        Set oMyField = oRow.Field(1)
        nValue(iRowNo) = oMyField.BoolValue
    Else
        ' We handle the whole array
        For iRowNo = 1 To oRow.Count
            Set oMyField = oRow.Field(iRowNo)
            nValue(iRowNo) = oMyField.BoolValue
        Next iRowNo
    End If
    
End Function

Public Function SaveLongArray(nValue() As Long) As CFieldGroup
    ' Helper function for GetField
    Dim iSeqNo      As Integer
    Dim oRow        As IRow
    Dim oSubField   As CFieldLong
    
    Set SaveLongArray = m_oSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldLong
    Set oRow = SaveLongArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.LongValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveStringArray(nValue() As String) As CFieldGroup
    ' Helper function for GetField
    Dim iSeqNo      As Integer
    Dim oRow        As IRow
    Dim oSubField   As CFieldString
    
    Set SaveStringArray = m_oSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldString
    Set oRow = SaveStringArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.StringValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveDoubleArray(nValue() As Double) As CFieldGroup
    ' Helper function for GetField
    Dim iSeqNo      As Integer
    Dim oRow        As IRow
    Dim oSubField   As CFieldDouble
    
    Set SaveDoubleArray = m_oSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldDouble
    Set oRow = SaveDoubleArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.DoubleValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveDateArray(nValue() As Date) As CFieldGroup
    ' Helper function for GetField
    Dim iSeqNo      As Integer
    Dim oRow        As IRow
    Dim oSubField   As CFieldDate
    
    Set SaveDateArray = m_oSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldDate
    Set oRow = SaveDateArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.DateValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function

Public Function SaveBooleanArray(nValue() As Boolean) As CFieldGroup
    ' Helper function for GetField
    Dim iSeqNo      As Integer
    Dim oRow        As IRow
    Dim oSubField   As CFieldBool
    
    Set SaveBooleanArray = m_oSession.Root.CreateUtilityObject("CFieldGroup")
    Set oSubField = New CFieldBool
    Set oRow = SaveBooleanArray.GroupRow
    
    For iSeqNo = 1 To UBound(nValue)
        oSubField.BoolValue = nValue(iSeqNo)
        oRow.Add oSubField.IField_Interface
    Next iSeqNo

End Function



'----------------------------------------------------------------------------
' IBo Interface
'----------------------------------------------------------------------------

Public Function IBo_Initialise(oSession As ISession) As IBo
    ' All Business Objects must be initialised with a session
    ' by the object database before they may be used.
    ' So don't try creating a new object yourself!
    Set m_oSession = oSession
    ' Check the sub-class has initialised us
    Debug.Assert Not (m_oSubClass Is Nothing)
    Set IBo_Initialise = Me         ' return native interface
End Function

Public Property Get IBo_ClassId() As Integer
    RaiseErrorNotImplemented
End Property
Public Property Get IBo_ClassName() As String
    RaiseErrorNotImplemented
End Property
Public Property Get IBo_Version() As String
    IBo_Version = App.Major & "." & App.Minor
End Property
Public Function IBo_GetField(nFid As Long) As IField
    RaiseErrorNotImplemented
End Function
Public Function IBo_GetSelectAllRow() As IRowSelector
    ' Returns a CRow object containing one SelectAll field selector for each property.
    Dim oRSelector  As IRowSelector
    Dim nFid        As Long
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    For nFid = (m_oSubClass.ClassId * &H10000) + 1 To m_oSubClass.EndOfStaticFieldId Step 1
        oRSelector.AddSelectAll nFid
    Next nFid
    Set IBo_GetSelectAllRow = oRSelector
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    Dim oRow    As IRow
    Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (m_oSubClass.ClassId * &H10000) + 1 To m_oSubClass.EndOfStaticFieldId Step 1
        Call oRow.Add(m_oSubClass.GetField(nFid))
    Next nFid
    Set IBo_GetAllFieldsAsRow = oRow
End Function

Public Function IBo_Load() As Boolean
    ' Update this objects properties from persistent storage
    ' identifying it explicitly or implicitly by the key value.
    ' Load up all properties from the database
    Dim oRSelector  As IRowSelector
    Dim oKeyField   As IField
    Dim oView       As IView
    Dim i           As Integer
    
    IBo_Load = False
    ' CheckIsUnInitialised() is in modCommonBo
    CheckIsUnInitialised
    
    ' First, we create a selector to select just our object
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    ' Get and add the key, or keys into the selector
    Set oKeyField = IBo_GetKeyField
    Debug.Assert Not (oKeyField Is Nothing)
    If oKeyField.FieldType <> 6 Then
        ' We have a simple field to select on
        oRSelector.AddSelection CMP_EQUAL, oKeyField
    Else
        ' We have a group field to select on
        ' Get CFieldGroup native interface
        Dim oMyGroupField As CFieldGroup
        Set oMyGroupField = oKeyField.Interface
        For i = 1 To oMyGroupField.GroupRow.Count
            oRSelector.AddSelection CMP_EQUAL, oMyGroupField.GroupRow.Field(i)
        Next i
    End If
    
    ' Second, merge with a selector for all the other properties
    oRSelector.Merge IBo_GetSelectAllRow
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oRSelector)
    If Not (oView Is Nothing) Then
        If oView.Count = 1 Then
            IBo_Load = IBo_LoadFromRow(oView.Row(1))
        End If
    End If
    Debug.Assert IBo_Load

End Function

Friend Function IBo_LoadFromRow(oRow As IRow) As Boolean
    RaiseErrorNotImplemented
End Function

Public Property Get IBo_DebugString() As String
    ' Method to return identifying information for display during debugging
    IBo_DebugString = "Bo Superclass for Object: " & m_oSubClass.ClassName & " ClassId: " & m_oSubClass.ClassId
End Property

Public Function IBo_CreateNew() As IBo
    ' A faster way of creating an object
    IBo_CreateNew = New CBoSuperClass
End Function

Public Function IBo_Save() As Boolean
    ' Save all properties to the database, create or update as appropriate.
    IBo_Save = False
    If m_oSubClass.IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        IBo_Save = m_oSession.Database.SaveBo(SaveTypeAllCases, m_oSubClass)
    End If
End Function

Public Function IBo_SaveIfNew() As Boolean
    ' If this object is not persistent yet, then persist it
    IBo_SaveIfNew = False
    If m_oSubClass.IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        IBo_SaveIfNew = m_oSession.Database.SaveBo(SaveTypeIfNew, m_oSubClass)
    End If
End Function

Public Function IBo_SaveIfExists() As Boolean
    ' If in persistent storage, then persist current object's values
    IBo_SaveIfExists = False
    If m_oSubClass.IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        IBo_SaveIfExists = m_oSession.Database.SaveBo(SaveTypeIfExists, m_oSubClass)
    End If
End Function

Public Function IBo_Delete() As Boolean
    ' Remove from persistent storage
    IBo_Delete = m_oSession.Database.SaveBo(SaveTypeDelete, m_oSubClass)
End Function

Public Function IBo_IsValid() As Boolean
    RaiseErrorNotImplemented
End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    ' Allows us to get the native or standard interface to a Bo (the sub-class!)
    ' With early linking it would be eInterfaceType As enBoInterfaceType
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = m_oSubClass
        Set IBo_Interface = oBO
    Case Else:
        Set IBo_Interface = m_oSubClass
    End Select
End Function

Public Function IBo_GetKeyField() As IField
    ' May be zero or more key fields, normally just one.
    ' If zero we return Nothing, if one we return the simple field and
    ' if more than one (composite key) we return a group field.
    ' By default, we assume there is one key and it is the first FID
    Set IBo_GetKeyField = m_oSubClass.GetField((m_oSubClass.ClassId * &H10000) + 1)
End Function

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    RaiseErrorNotImplemented
End Function
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    RaiseErrorNotImplemented
End Function
Public Sub IBo_ClearLoadFilter()
    RaiseErrorNotImplemented
End Sub
Public Sub IBo_AddLoadField(FieldID As Long)
    RaiseErrorNotImplemented
End Sub
Public Sub IBo_ClearLoadField()
    RaiseErrorNotImplemented
End Sub
Public Function IBo_EndOfStaticFieldId() As Long
    RaiseErrorNotImplemented
End Function

'----------------------------------------------------------------------------
' ISysBo Interface
'----------------------------------------------------------------------------
Public Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    RaiseErrorNotImplemented
End Function



