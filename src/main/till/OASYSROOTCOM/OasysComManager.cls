VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OasysComManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"OasysComClass"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
'<CAMH>****************************************************************************************
'* Module: OasysComManager
'* Date  : 16/08/02
'* Author: MartynW
'**********************************************************************************************
'* Summary:
'* The OasysComManager class translates a request for a classname into
'* and object reference via the GetComObject() or CreateComObject() methods.
'* This is the place we code the relationship between Application names
'* (ie. dll names) and class names.  For any new class, add an entry in
'* Class_Initialize() using the AddApp() method which takes parameters
'* representing the dll base name and a comma-delimited list of class
'* names.  The class name has :s appended if it is a singleton.
'* $Archive: /Projects/OasysV2/VB/OasysRootCom/OasysComManager.cls $
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/05/04 12:11 $ $Revision: 57 $
'* Versions:
'* 16/08/02    MartynW
'*             Revision 15
'* 29/08/02    MartynW
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "OasysComManager"
#Const ccDebug = 0

' Dictionary holds keys and associated OasysComClass object
Private m_oDictionary As Dictionary
' Qualifier is the COM dll name suffix for different versions of the product
Private m_strSuffix As String
Private m_oRoot     As OasysRoot

Private Sub Class_Initialize()
    Set m_oDictionary = New Dictionary
    ' We want case insensitive comparisons
    m_oDictionary.CompareMode = vbTextCompare
End Sub

Private Sub Class_Terminate()
    Set m_oDictionary = Nothing
End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean Initialise()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'*In/Out:oRoot  OasysRoot.
'*In/Out:strQualifier   String(Opt).
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 29/08/02    mauricem
'*             Header added.
'* 29/08/02    mauricem
'*             Extended Class Names to include all Class Modules as per UML Model
'</CACH>***************************************************************************************
Friend Function Initialise(oRoot As OasysRoot, Optional strQualifier As String) As Boolean
Const PROCEDURE_NAME As String = MODULE_NAME & ".Initialise"
    Set m_oRoot = oRoot
    m_strSuffix = strQualifier
    Debug.Print "ComManager loading Base as " & App.EXEName
    ' Ensure we load the Base objects from the current dll
'    AddApp App.EXEName, "OasysRoot:s,Client:s,Session"
    ' Empty app name implies local
    ' For flags such as :s and :f, see the comments in method AddApp().
    AddApp "", "OasysRoot:s,Client:s,Enterprise,Session,CBoSuperClass"
    AddApp "OasysCBaseCom", "Registry:s"
    AddApp "cStock", "cInventory,cStockMovement,cAuditMaster,cStockWriteOff,cStockAdjustment," & _
           "cStockCountItem,cLocationType,cDepartmentGroup,cStockAdjustmentPend,cPriceChange," & _
           "cRelatedItem,cProductGroup,cStockLocation,cStockOtherLocation,cItemGroup,cUnitOfMeasure," & _
           "cEAN,cStockAdjustmentCode,cInventorys,cStockCountHeader,cStockLog,cInventoryText,cStockAdjustReason," & _
           "cHierarchyCategory,cHierarchyGroup,cHierarchyStyle,cHierarchySubGroup,cInventoryWickes,cPIMItem," & _
           "cWEEERate,cPlangram,cMarkDownStock"
    AddApp "cPurchase", "cPurchaseOrder,cPurchaseLine,cDeliveryNote,cDeliveryNoteLine,cSupplier," & _
           "cConsignmentHeader,cISTInHeader,cISTOutHeader,cISTLine,cSuppliers,cDelNoteNarrative," & _
           "cISTInEDIHeader,cISTinEDILine,cPurchaseConsLine,cPurHdrNarrative,cLineReasonNotes,cEDIChangeCode,cSupplierDet"
    AddApp "cLookUps", "cDeptAnalysis,cDepartment,cDiscountRate,cVATRates"
    AddApp "cSale", "cDeposit,cPOSAction,cPOSHeader,cPOSLine,cPOSMessage,cPOSPayment," & _
           "cPriceOverrideCode,cReturnCust,cTrader,cSalesLedger,cTypesOfSaleOptions," & _
           "cTypesOfSaleControl,cTenderControl,cTenderOptions,cTypesOfSaleTenders," & _
           "cOpenDrawerCode,cPaidInCode,cPaidOutCode,cSalesCustomer,cTransactionDiscount,cItemMessage," & _
           "cCardInfo,cPriceCusInfo,cPriceLineInfo,cPOSMissingEAN,cRefundCode,cTenderOverride,cPOSGiftVoucher," & _
           "cGiftVoucherHotFile,cEANCheck,cSKUReject,cLineReversalCode,cSoldCoupon,cPOSPaymentComm"
    AddApp "cEnterprise", "cCashier,cStore,cPayHours,cSystemNumbers,cNightLog,cNightMaster," & _
           "cSystemDates,cParameter,cWorkStation,cUser,cRetailOptions,cSystemOptions,cActivityLog," & _
           "cStoreStatus,cEDIFileControl,cEDILogEntry,cColleagueCardHF,cTempFil,cExchangeRate,cCurrencies," & _
           "cBarcodeBroken,cDTFMsgSend,cDiscountCardScheme,cDiscountCardList"
    AddApp "cReturns", "cReturnHeader,cReturnLine,cReturnCode,cReturnNote,cReturnNoteLine,cDespatchMethod"
    AddApp "cSalesOrder", "cOrderHeader,cOrderLine,cOrderType,cOrderRange,cOrderRangeDiscount,cOrderRangeAcc"
    AddApp "cCashBalance", "cCashBalanceHeader,cCashBalanceControl,cCashBalanceSummary"
    AddApp "OasysMenu", "cMenuItem"
    AddApp "OasysDbXfaceCom", "CFieldLong,CFieldString,CRowSelector:f" & _
            ",CFieldBool,CFieldDate,CFieldDouble,CFieldGroup,CRow,CBoSortKeys"
    AddApp "OasysSqlDBCom", "CBoDatabase"
    AddApp "StockTakeBoCom", "CStockTake,CCountedSku"
    AddApp "SalesTotals", "cCashierTotals,cSaleTotals,cTillTotals,cCashierCashBal,cZReads,cColleagueCards"
    AddApp "prjLogon", "clsLogon:f"
    AddApp "cViews", "cPOLines,cStockLevels"
'    AddApp "OasysDbInterface2Com", "CFieldLong,CFieldString,CRowSelector,CBoDatabase"
'    AddObj "OasysMenu", "Menu", ObjectTypeMenu
'    AddObj "OasysMenu", "MenuItem", ObjectTypeMenuItem
    AddApp "cEDIDelNote", "cEDIDelHeader,cEDIDelLine"
    AddApp "cEDIPOViews", "cEDIPOEarly,cEDIPOLate,cEDIPOLines,cEDIPOQtyDiscrepancy,cEDIPOQtyDiscrepancyLines"
    AddApp "cOPEvents", "cEventMaster,cEventHeader,cEventMixMatch,cEventDealGroup,cEventTranLine,cEventHSExclusions," & _
            "cCouponMaster,cCouponText"
    AddApp "cSummaries", "cGrossMargins"
    AddApp "cCustomerOrders", "cCustOrderHeader,cCustOrderLine,cQuoteHeader,cQuoteLine,cCustomer,cCustOrderText,cCustOrderInfo,cCustomerRefund"
    AddApp "cWebOrder", "cWebEvent,cWebHeader,cWebLine,cWebPick,cWebPaid"
    AddApp "cTillPrompts", "cPrompts,cGroupPrompts,cItemGrpPrompts,cDayPrice"
    AddApp "cCustResBO", "cConResHeader,cConResDetail"
    
End Function 'Initialise

Public Function IsInstantiated(strClassName As String) As Boolean
    Dim oCom As OasysComClass
    If m_oDictionary.Exists(strClassName) Then
        Set oCom = m_oDictionary.Item(strClassName)
        IsInstantiated = oCom.IsInstantiated
    End If
End Function

Public Function GetComObject(strClassName As String) As Object
    Dim oCom As OasysComClass
    If m_oDictionary.Exists(strClassName) Then
        Set oCom = m_oDictionary.Item(strClassName)
        Set GetComObject = oCom.GetComObject(m_strSuffix)
    Else
        Set GetComObject = Nothing
        Err.Raise OASYS_ERR_CLASS_NOT_FOUND, "OasysComManager::GetComObject()", "Class not found: " & strClassName
    End If
End Function

Public Function CreateComObject(strClassName As String) As Object
    Dim oCom As OasysComClass
    If m_oDictionary.Exists(strClassName) Then
        Set oCom = m_oDictionary.Item(strClassName)
        Set CreateComObject = oCom.CreateComObject(m_strSuffix)
    Else
        Set CreateComObject = Nothing
        Err.Raise OASYS_ERR_CLASS_NOT_FOUND, "OasysComManager::CreateComObject()", "Class not found: " & strClassName
    End If
End Function

Private Sub AddApp(strApp As String, strList As String)
    ' Add an application that holds one or more classes that are
    ' NOT derived from IOasysObject
    ' The class may have flags introduced by a colon:
    ' :s means class is a singleton (only one instance)
    ' :f means a friend of the root with a LET SysRoot property.
    Dim strClasses() As String
    Dim oClass As OasysComClass
    Dim bIsLocal As Boolean
    Dim i As Integer
    If LenB(strApp) = 0 Then
        ' No app name implies local
        bIsLocal = True
        strApp = App.EXEName
    End If
    strClasses = Split(strList, ",")
    For i = 0 To UBound(strClasses)
        ' Ensure we load the OasysComClass object from the current dll
        Set oClass = New OasysComClass
        With oClass
            Do While Mid$(strClasses(i), Len(strClasses(i)) - 1, 1) = ":"
                ' We have a flag introduced by :
                Select Case Right$(strClasses(i), 1)
                Case Is = "s"
                    .IsSingleton = True
                Case Is = "f"
                    .SysRoot = m_oRoot
                Case Else
                End Select
                strClasses(i) = Left(strClasses(i), Len(strClasses(i)) - 2)
            Loop
            .IsLocal = bIsLocal
            .Name = strClasses(i)
            .App = strApp
            If Not m_oDictionary.Exists(.Name) Then
                m_oDictionary.Add .Name, oClass
'                MsgBox "Added class: " & .App & "." & .Name
            Else
                Err.Raise OASYS_ERR_DUPLICATE_CLASS, "OasysComManager::AddApp()", "Duplicate class: " & .App & "." & .Name
            End If
        End With
        Set oClass = Nothing
    Next i
End Sub

Private Sub AddObj(strApp As String, strClass As String, ObjectType As Long)
    ' Add an Oasys Business Object class that is derived from IOasysObject.
    Dim oClass As OasysComClass
    ' Ensure we load the OasysComClass object from the current dll
    ' We can't if the class attributes are Private
    Set oClass = New OasysComClass
    ' so we try a new instead.
    Set oClass = New OasysComClass
    With oClass
        .IsSingleton = (Right$(strClass, 2) = ":s")
        If .IsSingleton Then
            .Name = Left(strClass, Len(strClass) - 2)
        Else
            .Name = strClass
        End If
        .App = strApp
        If Not m_oDictionary.Exists(ObjectType) Then
            ' Key is not the class name but the Object Type
            m_oDictionary.Add ObjectType, oClass
'           MsgBox "Added class: " & .App & "." & .Name
        Else
            Err.Raise OASYS_ERR_DUPLICATE_CLASS, "OasysComManager::AddObj()", "Duplicate class Id: " & ObjectType & " for " & .App & "." & .Name
        End If
    End With
    Set oClass = Nothing
End Sub

