VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Enterprise"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit
#Const ccDebug = 1
Implements IEnterprise
Implements IVEnterprise
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/Enterprise.cls $
' $Revision: 14 $
' $Date: 20/04/04 16:46 $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Private m_oRoot       As IOasysRoot       ' Useful object interface reference
Private m_oRootFriend As OasysRoot   ' Same object with Friend interface
Private m_oDatabase   As IBoDatabase

Private Type tEnterpriseProps
    nId As Integer
    Data As tEnterpriseBaseRecV1
End Type
Private Props As tEnterpriseProps
Private m_blnDBInit As Boolean

Friend Function Initialise(Root As IOasysRoot, nId As Integer, oTailor As ITailoredData) As Boolean
    ' Initialise only works whilst there is persistent data available
    ' There must be one Enterprise, so if there is no data for nId = 1
    ' then we create it.  Note nId must not be zero.
    Set m_oRoot = Root
    Set m_oRootFriend = m_oRoot
    If m_oRoot Is Nothing Then
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "Enterprise::Initialise() ", "Root reference unset for item " & Props.nId
        Initialise = False
    Else
        Initialise = True
    End If
    Props.nId = nId
    Props.Data = oTailor.Record(TAILOR_CONTEXT_ENTERPRISE, nId).varBuffer
    ' Normally, the Enterprise data will come from the tailored data module,
    ' however, we may allow this to be overridden by the registry.
    ' If the database holds different data, then we can update the registry
    ' from the database and reload the system which will then use the new
    ' registry settings.
    ' Normally we would expect each Enterprise to have its own registry key.
    ' However, if there is only one Enterprise, then we can default its
    ' values to those of the Client.
    ' The key for an Enterprise has the Id appended, apart from the first Enterprise.
    If Not m_oRoot Is Nothing Then
#If ccLateBound = 1 Then
        Dim oRegistry As Object
        Debug.Print "Enterprise LateBound Registry"
#Else
        Dim oRegistry As oasyscbasecomlib.Registry
        Debug.Print "Enterprise Early bound Registry"
#End If
        Dim strKey, strValue As String
        Dim nResult As Long
        Set oRegistry = m_oRoot.CreateUtilityObject("Registry")
        strKey = RelativeRegistryKey
        oRegistry.GetCTSStringValue strKey, "Mnemonic", strValue, nResult
        If nResult = 0 Then
            Mnemonic = strValue
            Debug.Print "Enterprise " & Props.nId & " mnemonic from registry: " & Mnemonic
        End If
        If LenB(Mnemonic) = 0 Then
            ' Mnemonic not set so use a default
            If Id = 1 Then
                Mnemonic = m_oRoot.Client.Mnemonic  ' Special default for first
            Else
                Mnemonic = nId                      ' Default default
            End If
            Debug.Print "No enterprise mnemonic in registry, using: " & Mnemonic
        End If
        If Mnemonic <> strKey Then
            ' mnemonic changed, so update the registry
            If oTailor.RecordCount(TAILOR_CONTEXT_ENTERPRISE) = 1 And _
                            UCase$(Mnemonic) = UCase$("Generic") Then
                ' We don't bother updating the registry for default of "Generic"
            Else
                oRegistry.SetCTSStringValue strKey, "Mnemonic", Mnemonic, nResult
                If nResult <> 0 Then
                    Debug.Print "Failed to update registry"
                End If
            End If
        End If
        oRegistry.GetCTSStringValue strKey, "ShortName", strValue, nResult
        If nResult = 0 Then
            ShortName = strValue
        End If
        oRegistry.GetCTSStringValue strKey, "FullName", strValue, nResult
        If nResult = 0 Then
            FullName = strValue
        End If
        
        oRegistry.GetCTSStringValue strKey, "WorkstationID", strValue, nResult
        If nResult = 0 Then
            WorkstationID = strValue
        End If
        Set oRegistry = Nothing
    End If
    
    If Initialise Then
        Debug.Print "Enterprise " & Id & " - '" & Mnemonic & "' Initialised Ok."
    End If
End Function

Friend Function InitialiseDBSettings()
    ' Initialise only works whilst there is persistent data available
    ' There must be one Enterprise, so if there is no data for nId = 1
    ' then we create it.  Note nId must not be zero.
    
Dim oRetOpts  As IBo
Dim oDatabase As IBoDatabase

'    StoreNumber = strValue
    
    If ((m_oDatabase Is Nothing) = True) Then Set m_oDatabase = m_oRootFriend.SystemSession.Database
    Set oRetOpts = m_oDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oRetOpts.AddLoadField(FID_RETAILEROPTIONS_StoreNumber)
    Call oRetOpts.LoadMatches
    
    StoreNumber = CStr(oRetOpts.GetField(FID_RETAILEROPTIONS_StoreNumber).ValueAsVariant)
    
    Set oRetOpts = Nothing
            
    m_blnDBInit = True
    Debug.Print "Enterprise " & Id & " - '" & Mnemonic & "' Initialised DB Settings Ok."

End Function

Friend Property Get RelativeRegistryKey() As String
    If m_oRoot Is Nothing Then
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "Enterprise::RelativeRegistryKey() ", "Root reference unset."
        RelativeRegistryKey = vbNullString
    Else
        RelativeRegistryKey = m_oRootFriend.RelativeRegistryKey & "\Enterprise"
        If Id <> 1 Then
            ' First key is "Enterprise", the rest have the Id on the end.
            RelativeRegistryKey = RelativeRegistryKey & Id
        End If
    End If
End Property
Friend Property Get Id() As Integer
    Id = Props.nId
End Property
Friend Property Get Mnemonic() As String
    Mnemonic = Props.Data.strMnemonic
End Property
Private Property Let Mnemonic(strVal As String)
    ' Private, Mnemonic is read-only externally.
    Props.Data.strMnemonic = strVal
End Property
Friend Property Get ShortName() As String
    ShortName = Props.Data.strShortName
End Property
Private Property Let ShortName(strVal As String)
    ' Private, ShortName is read-only externally.
    Props.Data.strShortName = strVal
End Property
Friend Property Get FullName() As String
    FullName = Props.Data.strFullName
End Property
Friend Property Get WorkstationID() As String
    WorkstationID = Props.Data.strWorkstationID
End Property
Private Property Let WorkstationID(strVal As String)
    ' Private, ShortName is read-only externally.
    Props.Data.strWorkstationID = strVal
End Property
Friend Property Get StoreNumber() As String
    
    If m_blnDBInit = False Then Call InitialiseDBSettings
    StoreNumber = Props.Data.strStoreNo

End Property
Friend Property Let StoreNumber(strVal As String)
    Props.Data.strStoreNo = strVal
End Property
Private Property Let FullName(strVal As String)
    ' Private, FullName is read-only externally.
    Props.Data.strFullName = strVal
End Property
Friend Function CreateDatabase(oSession As Session) As IBoDatabase
    Dim oSysRoot        As ISysRoot
    Dim oRawDatabase    As IBoDatabase
    Set oSysRoot = m_oRoot      ' get system interface
    Set oRawDatabase = m_oRootFriend.CreateDatabase()
    If oRawDatabase Is Nothing Then
        Debug.Assert False
    Else
        ' We are a friend to the Root so we can pass the restricted system interface
        ' at the same time it returns an IBoDatabase interface rather than the default i/f.
        Set CreateDatabase = oRawDatabase.Initialise(oSession, oSysRoot, Me)
        Set m_oDatabase = CreateDatabase
    End If
End Function

'----------------------------------------------------------------------------
' Interface IEnterprise - using Types Version 1
'----------------------------------------------------------------------------
Public Property Get IEnterprise_RelativeRegistryKey() As String
    IEnterprise_RelativeRegistryKey = RelativeRegistryKey
End Property
Public Property Get IEnterprise_Id() As Integer
    IEnterprise_Id = Id
End Property
Public Property Get IEnterprise_Mnemonic() As String
    IEnterprise_Mnemonic = Mnemonic
End Property
Public Property Get IEnterprise_ShortName() As String
    IEnterprise_ShortName = ShortName
#If ccDebug = 1 Then
    IEnterprise_ShortName = IEnterprise_ShortName & " from IEnterprise"
#End If
End Property
Public Property Get IEnterprise_FullName() As String
    IEnterprise_FullName = FullName
End Property

Public Property Get IEnterprise_StoreNumber() As String
    IEnterprise_StoreNumber = StoreNumber
End Property
Public Property Get IEnterprise_WorkstationID() As String
    IEnterprise_WorkstationID = WorkstationID
End Property

'----------------------------------------------------------------------------
' Interface IVEnterprise - using Variants Version 1
'----------------------------------------------------------------------------
Public Property Get IVEnterprise_RelativeRegistryKey() As String
    IVEnterprise_RelativeRegistryKey = RelativeRegistryKey
End Property
Public Property Get IVEnterprise_Id() As Integer
    IVEnterprise_Id = Id
End Property
Public Property Get IVEnterprise_Mnemonic() As String
    IVEnterprise_Mnemonic = Mnemonic
End Property
Public Property Get IVEnterprise_ShortName() As String
    IVEnterprise_ShortName = ShortName & " from IVEnterprise"
#If ccDebug = 1 Then
    IVEnterprise_ShortName = IVEnterprise_ShortName & " from IVEnterprise"
#End If
End Property
Public Property Get IVEnterprise_FullName() As String
    IVEnterprise_FullName = FullName
End Property

Public Property Get IVEnterprise_StoreNumber() As String
    IVEnterprise_StoreNumber = StoreNumber
End Property

Public Property Get IVEnterprise_WorkstationID() As String
    IVEnterprise_WorkstationID = WorkstationID
End Property

