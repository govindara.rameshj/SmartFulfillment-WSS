VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OasysError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
#Const ccDebug = 0
'
' $Archive: /VB/OasysBaseCom/OasysError.cls $
' $Revision: 1 $
' $Date: 22/07/02 8:30 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Private m_strLocation As String
Private m_nSeverity As Integer
Public Function Raise()
    MsgBox Err.Number & vbCrLf & Err.Description
End Function
