VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Session"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/Session.cls $
' $Revision: 20 $
' $Date: 3/21/03 4:11p $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Implements ISession

Private m_oRoot As IOasysRoot       ' Useful object interface reference
Private m_oRootFriend As IOasysRoot  ' Same object with Friend interface

' Parameter Types for GetParameter
Const TYPE_STRING = 0
Const TYPE_LONG = 1
Const TYPE_DOUBLE = 2
Const TYPE_BOOLEAN = 3
Const TYPE_COLOUR = 4 'same as long, but for editor - set using color selector

Public Type tSessionTailoring
    nMajorVersion As Integer
    nMinorVersion As Integer
    nRelease      As Integer
    strShortName  As String
    strFullName   As String
End Type
Private Type tSessionProps
    ' Option: Head Office (true) or Store (false), default Store
    bIsHeadOffice        As Boolean
    ' Option: Interactive or Not, default Not
    bIsInteractive       As Boolean
    nCurrentEnterpriseId As Integer
    ' The Oasys root is instatiated once per program.  Within a VB
    ' executable this implies one user and one session.  Within a
    ' web-server environment it implies many users, each with a session.
    ' It is useful to determine which environment we have.
    bIsRunningAsWebServer As Boolean
End Type

Private Props As tSessionProps
' This is the user command line, minus the session junk from the front
Private m_strCommandLine   As String
Private m_oCurrentDatabase As IBoDatabase
' m_strUserID is only second-best for when m_oUser isn't there.
Private m_strUserID        As String
'Private m_strUserInitials  As String
'Private m_strUserFullName  As String
Private m_strProgramParams As String
Private m_oUser            As IBo

Private mIsOnline          As Boolean

Private m_oParameters As Collection
Public Function GetParameter(ParameterID As Long) As Variant
Dim vntParam   As Variant
Dim oParam     As IBo
Dim lParamType As Long
Dim lFieldNo   As Long
Dim colParams  As Collection

    If m_oParameters Is Nothing Then
        Set m_oParameters = New Collection
        Set oParam = m_oCurrentDatabase.CreateBusinessObject(CLASSID_PARAMETER)
        Call oParam.AddLoadFilter(CMP_GREATERTHAN, FID_PARAMETER_ID, 0)
        Set colParams = oParam.LoadMatches
        
        For Each oParam In colParams
            lParamType = oParam.GetField(FID_PARAMETER_Type).ValueAsVariant
            Select Case (lParamType)
                Case (TYPE_STRING):             Let lFieldNo = FID_PARAMETER_String
                Case (TYPE_LONG), (TYPE_COLOUR): Let lFieldNo = FID_PARAMETER_Long
                Case (TYPE_BOOLEAN):             Let lFieldNo = FID_PARAMETER_Boolean
                Case (TYPE_DOUBLE):              Let lFieldNo = FID_PARAMETER_Decimal
            End Select
            
            vntParam = oParam.GetField(lFieldNo).ValueAsVariant
            Call m_oParameters.Add(vntParam, CStr(oParam.GetField(FID_PARAMETER_ID).ValueAsVariant))
        Next oParam
        Set oParam = Nothing
        Set colParams = Nothing
    End If
    
    On Error Resume Next
    vntParam = m_oParameters.Item(CStr(ParameterID))
    If Err.Number <> 0 Then
        Err.Clear
        On Error GoTo 0
        Call Err.Raise(OASYS_ERR_PARAMETER_NOT_KNOWN, "GetParameter", "Parameter ID - " & ParameterID & " not recognised")
        Exit Function
    End If
    On Error GoTo 0
    GetParameter = vntParam

End Function

Public Function CreateCommandLine(ByVal strParameters As String) As String
    ' The command-line parameters are a form of serialisation of all the
    ' properties of the session object and allows the object to be recreated
    ' by passing the command-line into the Initialise() method.

    Dim strCommand As String

    strCommand = vbNullString
    If CurrentEnterpriseId > 0 Then strCommand = strCommand & "e=" & CurrentEnterpriseId
    If IsHeadOffice Then strCommand = strCommand & "h"
    If Not IsInteractive Then strCommand = strCommand & "i"
    If LenB(strParameters) <> 0 Then strCommand = strCommand & "P='" & strParameters & "'"
    If LenB(UserID) <> 0 Then strCommand = strCommand & "u='" & UserID & "'"
    If IsRunningAsWebServer Then strCommand = strCommand & "w"
    If (mIsOnline = False) Then strCommand = strCommand & "O"
    ' If we have any parameters, then set the parentheses round them.
    If LenB(strCommand) <> 0 Then
        strCommand = "(-" & strCommand & ")"
        ' Generate checksum
        AddChecksum strCommand
    End If
    
    CreateCommandLine = strCommand

End Function

Friend Function Initialise(Root As IOasysRoot, strCommandLine As String) As Boolean

Dim blnIsValidChecksum As Boolean
Dim strSessionFlags    As String
Dim intCharPos         As Integer
Dim strChar            As String ' s holds the current char
Dim strSense           As String 'strSense holds '+', '-' or '/'
    
    ' Interpret the Oasys system params and remove them.  We can then
    ' give the user just the original application params.
    Set m_oRoot = Root
    Set m_oRootFriend = m_oRoot
    ' By default we assume there is only one enterprise, but we don't set that here
    ' using "CurrentEnterpriseId = 1" as this would cause recursion, so we leave it
    ' as zero here and let the enterprise class handle it.
    
    ' Inherit Web server or VB executable setting
    IsRunningAsWebServer = m_oRoot.IsRunningAsWebServer
    Props.bIsHeadOffice = False
    Props.bIsInteractive = True
    blnIsValidChecksum = IsValidChecksum(strCommandLine)
    mIsOnline = True 'force system to connect Online first unless passed in the /O option
    
    If Len(strCommandLine) > 0 And Left$(strCommandLine, 1) = "(" Then
        On Error GoTo BadCommandLine
        ' Command line starts with optional system params within '(' and ')'
        ' format: ( [-, + or /]f... [[-, + or /]f...]... )
        ' where sense is -, + or /, f is Flag char (one or more)
        ' The above may be repeated after a space char.
        ' Alternatively, f may be of the form 'f=n' or 'f="sss"'
        For intCharPos = 2 To Len(strCommandLine)
            strChar = Mid$(strCommandLine, intCharPos, 1)
            If strChar = ")" Then
                ' The user command line starts here, if there is one.
                intCharPos = intCharPos + 1
                If intCharPos <= Len(strCommandLine) Then
                    m_strCommandLine = LTrim$(Mid$(strCommandLine, intCharPos))
                End If
                Exit For
            End If
            If strChar Like "[-+/]" Then
                ' Into to a bunch of flags
                strSense = strChar
            Else
                If LenB(strSense) <> 0 Then
                    Select Case strChar
                    Case Is = "e"
                        ' e = sets current Enterprise Id (format e=n)
                        If (intCharPos + 2) > Len(strCommandLine) Then
                            Debug.Print "Invalid format for option e=n"
                        Else
                            ' Move char ptr from 'e' past '=' onto 'n'
                            intCharPos = intCharPos + 2
                            CurrentEnterpriseId = Int(Mid$(strCommandLine, intCharPos, 1))
                            ' Validate it, we get an error if its wrong
                            If m_oRoot.Client.Enterprise(CurrentEnterpriseId) Is Nothing Then
                                Debug.Print "Invalid Enterprise Id option"
                            Else
                             Debug.Print "got option e=" & Props.nCurrentEnterpriseId
                            End If
                        End If
                    Case Is = "h"
                        ' h = head office, default is store
                        IsHeadOffice = True
                        Debug.Print "got option h"
                    Case Is = "i"
                        ' i = interactive off, default is on
                        IsInteractive = False
                        Debug.Print "got option i"
                    Case Is = "O"
                        ' O = work offline, do not attempt to connect to Online database
                        mIsOnline = False
                        Debug.Print "got option O"
                    Case Is = "P"
                        ' P = private data used for passing parameters to sub programs
                        Debug.Print "got option P"
                        If (intCharPos + 3) > Len(strCommandLine) Then
                            Debug.Print "Invalid format for option P='n'"
                        Else
                            intCharPos = intCharPos + 3
                            m_strProgramParams = Mid$(strCommandLine, intCharPos)
                            m_strProgramParams = Left$(m_strProgramParams, InStr(m_strProgramParams, "'") - 1)
                            intCharPos = intCharPos + Len(m_strProgramParams)
                        End If
                    Case Is = "u"
                        ' u = User code as logged in with
                        Debug.Print "got option u"
                        If (intCharPos + 3) > Len(strCommandLine) Then
                            Debug.Print "Invalid format for option u=n"
                        Else
                            intCharPos = intCharPos + 3
                            m_strUserID = Mid$(strCommandLine, intCharPos)
                            m_strUserID = Left$(m_strUserID, InStr(m_strUserID, "'") - 1)
                            intCharPos = intCharPos + Len(m_strUserID) + 1
                            If Not blnIsValidChecksum Then
                                ' Not authorised
                                Debug.Assert False
                                m_strUserID = vbNullString
                            End If
                        End If
                    Case Is = "w"
                        ' w = Web Server environment, default is off
                        ' Note web server environment must use the Variant interfaces
                        ' rather than the type parameter interfaces
                        IsRunningAsWebServer = True
                        Debug.Print "got option w"
                    Case Is = " "
                        ' Space delimits the end of a group of flags
                        strSense = " "
                    Case Else
                        Debug.Print "Unknown option: " & strChar
                    End Select
                End If
            End If
        Next intCharPos 'process next character, if any
    End If 'any command line to process
    Debug.Print "Session Initialised Ok"
    Initialise = True
    Exit Function
    
BadCommandLine:
    Call Err.Raise(OASYS_ERR_INVALID_PARAMETER_LINE, "Initialise Session", "Command line parameters error" & vbCrLf & vbTab & Err.Number & "-" & Err.Description)
End Function
Public Property Get CommandLine() As String
    CommandLine = m_strCommandLine
End Property
Public Property Get IsStore() As Boolean
    IsStore = Not IsHeadOffice
End Property
Public Property Get IsHeadOffice() As Boolean
    IsHeadOffice = Props.bIsHeadOffice
End Property
Public Property Let IsHeadOffice(bVal As Boolean)
    Props.bIsHeadOffice = bVal
End Property
Public Property Get UserID() As String
    If m_oUser Is Nothing Then
        UserID = m_strUserID
    Else
        UserID = CStr(m_oUser.GetField(FID_USER_EmployeeID).ValueAsVariant)
    End If
End Property
Public Property Get UserInitials() As String
    CheckUserLoaded
    If Not (m_oUser Is Nothing) Then
        UserInitials = CStr(m_oUser.GetField(FID_USER_Initials).ValueAsVariant)
    End If
End Property
Public Property Get UserFullName() As String
    CheckUserLoaded
    If Not (m_oUser Is Nothing) Then
        UserFullName = CStr(m_oUser.GetField(FID_USER_FullName).ValueAsVariant)
    End If
End Property
Private Function CheckUserLoaded() As Boolean
    ' Check if the user object exists and if not try creating and loading it.
    Dim oRSelector As IRowSelector
    If m_oUser Is Nothing Then
        If LenB(m_strUserID) <> 0 Then
            Set oRSelector = Database.CreateBoSelector(FID_USER_EmployeeID, CMP_EQUAL, m_strUserID)
            oRSelector.Merge Database.CreateBoSelector(FID_USER_FullName)
            oRSelector.Merge Database.CreateBoSelector(FID_USER_Initials)
            Set m_oUser = Database.GetBo(oRSelector)
            If m_oUser Is Nothing Then
                Debug.Assert False
            Else
                Set m_oUser = m_oUser.Interface
            End If
        End If
    End If

End Function
Public Property Get ProgramParams() As String
    ProgramParams = m_strProgramParams
End Property
Public Sub SetUserID(UserID As String, Initials As String, UserFullName As String)
    Debug.Assert False ' obsolete
    m_strUserID = UserID
'    m_strUserInitials = Initials
'    m_strUserFullName = UserFullName
End Sub
Public Function SetCurrentUser(oUser As IBo, oSysRoot As ISysRoot)
    ' This is a restricted function, that we only service for friends of the root,
    ' that is folk that can pass us a valid ISysRoot interface object.
    Dim strTmp As String
    On Error GoTo User_Error
    ' Test for friends
    strTmp = oSysRoot.BoClassName(CLASSID_USER)
    Debug.Assert (strTmp = "cUser")
    ' Passed the friend test if we got here
    If Not (oUser Is Nothing) And oUser.Interface(BO_INTERFACE_IBO).ClassId() = CLASSID_USER Then
        Set m_oUser = oUser
        Dim o As Object ' VERY Special Case!!
        Set o = oUser
        o.Protect
    Else
        Set m_oUser = Nothing
    End If
User_Error:
    ' We do nothing
End Function
Public Function GetCurrentUser() As IBo
    CheckUserLoaded
    Set GetCurrentUser = m_oUser
End Function
Public Property Get IsInteractive() As Boolean
    IsInteractive = Props.bIsInteractive
End Property
Public Property Let IsInteractive(bVal As Boolean)
    Props.bIsInteractive = bVal
End Property
Public Property Get IsRunningAsWebServer() As Boolean
    IsRunningAsWebServer = Props.bIsRunningAsWebServer
End Property
Private Property Let IsRunningAsWebServer(bVal As Boolean)
    Props.bIsRunningAsWebServer = bVal
End Property
Public Property Get Database() As IBoDatabase
    If m_oCurrentDatabase Is Nothing Then
        Set m_oCurrentDatabase = CurrentEnterprise.CreateDatabase(Me)
    End If
    If m_oCurrentDatabase Is Nothing Then
        Debug.Assert False
        Err.Raise OASYS_ERR_NO_DB_CONNECTION, "Session::Database() ", "We don't have a database connection that Oasys can use."
    End If
    Set Database = m_oCurrentDatabase
End Property
Public Property Get Root() As IOasysRoot
    Set Root = m_oRoot
End Property
Public Property Get CurrentEnterprise() As Enterprise
    Set CurrentEnterprise = m_oRoot.Client.Enterprise(CurrentEnterpriseId)
End Property
Public Property Get CurrentEnterpriseId() As Integer
    CurrentEnterpriseId = Props.nCurrentEnterpriseId
End Property
Public Property Let CurrentEnterpriseId(nEnterpriseId As Integer)
    If m_oRoot.Client.Enterprise(nEnterpriseId) Is Nothing Then
        ' Error, the client should have raised an error by now
        Debug.Assert False
    Else
        Props.nCurrentEnterpriseId = CurrentEnterpriseId
    End If
End Property
Private Function AddChecksum(strCommand As String) As Boolean
    ' Checksum calculation is currently only a placeholder
    If Right$(strCommand, 1) = ")" Then
        ' We have something, so remove end parenthesis
        strCommand = Left$(strCommand, Len(strCommand) - 1) & " -c='ffff')"
        AddChecksum = True
    End If
End Function
Private Function IsValidChecksum(strCommand As String) As Boolean
    ' Checksum calculation is currently only a placeholder
Dim intLength       As Integer
Dim tmpCommandLine  As String
Dim strTestChkSum   As String
    
    ' Find the checksum
    intLength = Len(strCommand) - 10
    If intLength > 0 Then
        If Mid$(strCommand, intLength, 5) = " -c='" Then
            ' We have it
            strTestChkSum = Mid$(strCommand, intLength + 5, 4)
            ' Now blank the checksum value and calculate the checksum for the string
            ' To do.
            tmpCommandLine = strCommand
            If strTestChkSum = "ffff" Then IsValidChecksum = True
        End If
    End If
End Function
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' ISession Interface Code
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Property Get ISession_Root() As IOasysRoot
    Set ISession_Root = Root
End Property

Public Property Get ISession_IsInteractive() As Boolean
    ISession_IsInteractive = IsInteractive
End Property

Public Property Get ISession_IsOnline() As Boolean
    ISession_IsOnline = mIsOnline
End Property

Public Property Let ISession_IsOnline(ByVal blnOnline As Boolean)
    mIsOnline = blnOnline
End Property
