VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IBo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
#Const ccEarlyBinding = 0
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/IBo.cls $
' $Revision: 12 $
' $Date: 11/19/02 11:05a $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' The 'real' interface definition
'----------------------------------------------------------------------------

Public Function Initialise(oSession As Object) As Object
End Function
Public Property Get ClassId() As Integer
End Property
Public Property Get ClassName() As String
End Property
Public Property Get Version() As String
End Property
Public Function GetField(nFid As Long) As Object
    ' Returns an IField object corresponding to the given FID
End Function
Public Function GetSelectAllRow() As Object
    ' Returns a CRow object containing one SelectAll field selector for each property.
End Function
Public Function LoadMatches(Optional oReturnRow As Object = Nothing) As Collection
End Function
Public Function Load() As Boolean
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    'added M.Milne 2/10/02 - to set all fields used by LoadMatches to perform db retrieval
End Function
Public Sub ClearLoadFilter()
    'added M.Milne 9/10/02 - to clear any Filters currently set
End Sub
Public Sub AddLoadField(FieldID As Long)
    'added M.Milne 9/10/02 - to set all fields used by LoadMatches to perform db retrieval
End Sub
Public Sub ClearLoadField()
    'added M.Milne 9/10/02 - to clear any Filters currently set
End Sub
Friend Function LoadFromRow(oRow As Object) As Boolean
    ' Friend: for use by system.  Where we implicitly assume oRow As CRow
End Function
Public Property Get DebugString() As String
End Property
Public Function CreateNew() As Object
    ' A faster way of creating an object
End Function
Public Function Save() As Boolean
End Function

Public Function SaveIfNew() As Boolean
End Function

Public Function SaveIfExists() As Boolean
End Function

Public Function Delete() As Boolean
End Function

Public Function IsValid() As Boolean
  'standardised wrapper to call Bo specific validation routine
End Function

' The following to be enabled, next time we change IBo
Public Function Interface(Optional eInterfaceType As Long) As Object
    ' Allows us to get the native or standard interface to a Bo
    ' With early linking it would be eInterfaceType As enBoInterfaceType
End Function
'Public Function GetKeyField() As Object
    ' May be zero or more key fields, normally just one.
    ' If zero we return Nothing, if one we return the simple field and
    ' if more than one (composite key) we return a group field.
'End Function

Public Function GetAllFieldsAsRow() As Object
' Returns a CRow object containing one field object for each property.
End Function
Public Function EndOfStaticFieldId() As Long
' Returns the END_OF_STATIC FId
End Function

