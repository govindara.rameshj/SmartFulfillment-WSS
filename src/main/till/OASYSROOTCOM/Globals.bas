Attribute VB_Name = "Globals"
Option Explicit
'
' $Archive: /VB/OasysRootCom/Globals.bas $
' $Revision: 9 $
' $Date: 14/08/02 12:33 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Everything should be encapsulated and so there should
' be no globals.  However, constants can usefully be global.
'----------------------------------------------------------------------------
Public Const OASYS_BASE_COM_VERSION = "0.1"

'Public Const TAILORED_DATA_CLIENT_MAJ_VERSION = 0
'Public Const TAILORED_DATA_CLIENT_MIN_VERSION = 0
'Public Const TAILORED_DATA_CLIENT_RELEASE = 1

Public Const TAILOR_CONTEXT_NOTHING = 0
Public Const TAILOR_CONTEXT_CLIENT = 1
Public Const TAILOR_CONTEXT_ENTERPRISE = 2


