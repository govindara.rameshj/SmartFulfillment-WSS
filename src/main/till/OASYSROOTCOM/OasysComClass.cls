VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OasysComClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysRootCom/OasysComClass.cls $
' $Revision: 10 $
' $Date: 12/11/02 11:32 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' This is a private class for use by OasysComManager only
'----------------------------------------------------------------------------
Private Type tComClassProps
    strName As String
    strApp As String
    oClass As Object
    ' bIsSingleton is true when we only create 1 instance and no more
    bIsSingleton As Boolean
    ' bIsLocal is true when we can do a new rather than CreateObject.
    bIsLocal As Boolean
End Type
Private Props       As tComClassProps
' If m_oSysRoot is other than nothing, then the object requires a SysRoot
' property setting after creation.
Private m_oSysRoot  As ISysRoot

Friend Property Let Name(Value As String)
    Props.strName = Value
End Property
Friend Property Get Name() As String
    Name = Props.strName
End Property
Friend Property Let App(Value As String)
    Props.strApp = Value
    If Props.strApp <> "OasysCBaseCom" Then
        Props.strApp = Props.strApp & "_Wickes"
    End If
End Property
Friend Property Get App() As String
    App = Props.strApp
End Property
Friend Property Let SysRoot(oRoot As OasysRoot)
    Set m_oSysRoot = oRoot
End Property
Friend Property Let IsSingleton(Value As Boolean)
    Props.bIsSingleton = Value
End Property
Friend Property Get IsSingleton() As Boolean
    IsSingleton = Props.bIsSingleton
End Property
Friend Property Let IsLocal(Value As Boolean)
    Props.bIsLocal = Value
End Property
Friend Property Get IsLocal() As Boolean
    IsLocal = Props.bIsLocal
End Property
Friend Property Get IsInstantiated() As Boolean
    ' Has an object of this type been created yet?  If not a GetObject will create.
    IsInstantiated = Not (Props.oClass Is Nothing)
End Property
Friend Function CreateComObject(strSuffix As String) As Object
'   We create an object, unless the class is a singleton and already created
    If Props.bIsSingleton And Not (Props.oClass Is Nothing) Then
        ' Singleton and already created, so return existing without creating new
        ' although we should never get here!
        Debug.Assert Props.strName = "Registry"
    Else
        ' Else we create the first instance or another for no-singletons
        On Error GoTo CreateComObject_Err
        If Props.bIsLocal Then
            Select Case Props.strName
            Case Is = "CBoSuperClass"
                Set Props.oClass = New CBoSuperClass
            Case Is = "Session"
                Set Props.oClass = New Session
            Case Is = "Client"
                Set Props.oClass = New Client
            Case Is = "Enterprise"
                Set Props.oClass = New Enterprise
            Case Is = "OasysRoot"
                Set Props.oClass = New OasysRoot
            Case Else
                Debug.Assert False
                Set Props.oClass = Nothing
            End Select
        Else
            If Props.oClass Is Nothing Then
                ' CreateObject is slow, but we have to do it once
                Set Props.oClass = CreateObject(Props.strApp & strSuffix & "." & Props.strName)
            Else
                ' we have already created one of these guys
                Dim oBo As IBo
                On Error Resume Next
                Set oBo = Props.oClass
                On Error GoTo CreateComObject_Err
                If oBo Is Nothing Then
                    Set Props.oClass = CreateObject(Props.strApp & strSuffix & "." & Props.strName)
                Else
                ' CreateObject is slow, but we have a faster way
                    Set Props.oClass = oBo.CreateNew()
                End If
            End If
        End If
    End If
    If Not (m_oSysRoot Is Nothing) Then
        ' If this property is set, it needs passing on down ...
        Set Props.oClass.SysRoot = m_oSysRoot
    End If
    Set CreateComObject = Props.oClass
    Exit Function
    
CreateComObject_Err:
    Err.Raise Err.Number, "OasysComClass::CreateComObject()", _
            Err.Description & ": " & Props.strApp & strSuffix & "." & Props.strName
End Function

Friend Function GetComObject(strSuffix As String) As Object
'   We create if no reference, else we return the previous object
    If Props.oClass Is Nothing Then
        CreateComObject strSuffix
    End If
    Set GetComObject = Props.oClass
End Function


