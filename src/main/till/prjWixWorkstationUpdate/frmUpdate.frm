VERSION 5.00
Begin VB.Form frmUpdate 
   Caption         =   "Wickes Workstation Update"
   ClientHeight    =   7215
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9180
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   9180
   Begin VB.ListBox lstStatus 
      Height          =   5715
      Left            =   120
      TabIndex        =   0
      Top             =   900
      Width           =   8955
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "IDLE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8955
   End
End
Attribute VB_Name = "frmUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/prjWixWorkstationUpdate/frmUpdate.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 12/08/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application to update the Workstations
'*
'**********************************************************************************************
'* Versions:
'*
'* 12/10/05 DaveF   v1.0.10  Changed to use a SHELLWAIT when calling REGSVRE32.EXE.
'*
'* 12/10/05 DaveF   v1.0.11  Changed to use a SHELLWAIT when calling REGSVRE32.EXE in the
'*                              correct method and remove the old Workstation update method. Also
'*                              checks to see if REGSVR32.EXE exists in the expected location.
'*
'* 12/10/05 DaveF   v1.0.12  Changed to add extra error checking.
'*
'* 12/10/05 DaveF   v1.0.13  Changed to add extra error checking.
'*
'* 12/10/05 DaveF   v1.0.14  Changed to add extra error checking.
'*
'* 12/10/05 DaveF   v1.0.16  Changed to call RegAll.BAT at the end of processing.
'*
'* 12/10/05 DaveF   v1.0.18  Changed to try and resolve the dll object creation error.
'*
'* 12/10/05 DaveF   v1.0.20  Changed in UpdateSystem() to add extra debugging around the
'*                              copy file call and to resume from error to call the registering
'*                              of the erred file, without the call to run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.21  Changed in UpdateSystem() to add extra debugging around the
'*                              copy file call and to resume from error to call the registering
'*                              of the erred file, with the call to run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.22  Changed in UpdateSystem() to not try and debugmsg when just
'*                              unregistered VBUtils.dll, without the call to
'*                              run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.24  Changed more debugging, without the call to
'*                              run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.25  Changed more debugging and version number in form caption,
'*                              with the call to run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.26  Changed the timeout on running RegAll.BAT to 20 minutes,
'*                              with the call to run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.27  Changed the timeout on running RegAll.BAT to 20 minutes and be visible,
'*                              with the call to run RegAll.Bat at the end.
'*
'* 12/10/05 DaveF   v1.0.28  Changed to Close the Process Thread in ShellWait() as well as Close
'*                              the Process Handle, with the call to run RegAll.Bat at the end.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Const MODULE_NAME As String = "WorkStation Update"

Dim mstrWindFlder       As String 'where windows is to access REGSVR32.EXE
Dim mstrAppFolder       As String
Dim mstrSourceFolder    As String
Dim mstrWSFolder        As String
Dim mstrMasterFolder    As String
Dim mstrWSID            As String
Dim moFSO               As New FileSystemObject

Private Sub Form_Load()

    Me.Caption = Me.Caption & " - v" & App.Major & "." & App.Minor & "." & App.Revision
    
    Me.Show
    DoEvents
    Call InitialiseSettings
    If (CheckForREGSVR = False) Then
        Unload Me
    End If
    lblStatus.Caption = "CHECKING FOLDERS"
    lblStatus.Refresh
    Call CheckForSourceFolder
    Call CheckForUpdateFolder
    Call CheckForMasterFolder
    Call CheckForWorkstationsFolder
    Call CheckForWorkstationFolder
    If CheckForGOFile = True Then
        If Mid$(Command$, 6) <> "" Then
            lblStatus.Caption = "MENU CLOSING:PLEASE WAIT"
            lblStatus.Refresh
            Call Wait(3)
        End If
        Call UpdateSystem
        Call DeleteGoFile
    End If
    lblStatus.Caption = "RECORDING VERSIONS"
    lblStatus.Refresh
    Call CheckForVerionFolder
    Call CreateVersionFile
    Call RecordVersions
    lblStatus.Caption = "REGISTERING FILES"
    lblStatus.Refresh
    Call RegAll
    Call lstStatus.AddItem("Process complete")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    
    If Left$(Command$, 4) = "EXIT" Then
        Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Exit from command line")
        On Error Resume Next
        Call Shell(Mid$(Command$, 6), vbNormalFocus)
        End
    End If
    lblStatus.Caption = "IDLE"
    lblStatus.Refresh
    
    Unload Me
    
End Sub 'Form_Load

Private Sub CheckForSourceFolder()

    Call lstStatus.AddItem("Checking for source folder" & vbTab & mstrSourceFolder)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FolderExists(mstrSourceFolder) = False Then
        Call moFSO.CreateFolder(mstrSourceFolder)
    End If

End Sub 'CheckForSourceFolder


Private Sub CheckForUpdateFolder()

    Call lstStatus.AddItem("Checking for update folder" & vbTab & _
        mstrSourceFolder & "\Update")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FolderExists(mstrSourceFolder & "\Update") = False Then
        Call moFSO.CreateFolder(mstrSourceFolder & "\Update")
    End If

End Sub 'CheckForUpdateFolder

Private Sub CheckForWorkstationsFolder()

    Call lstStatus.AddItem("Checking for workstations folder" & vbTab & _
        mstrSourceFolder & "\Update\Workstations")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FolderExists(mstrSourceFolder & "\Update\Workstations") = False Then
        Call moFSO.CreateFolder(mstrSourceFolder & "\Update\Workstations")
    End If

End Sub 'CheckForWorkstationFolder

Private Sub CheckForWorkstationFolder()

    mstrWSFolder = mstrSourceFolder & "\Update\Workstations\WS" & mstrWSID
    Call lstStatus.AddItem("Checking for workstation folder" & vbTab & _
        mstrWSFolder)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True

    If moFSO.FolderExists(mstrWSFolder) = False Then
        Call moFSO.CreateFolder(mstrWSFolder)
    End If

End Sub 'CheckForWorkstationFolder

Private Sub CheckForMasterFolder()

    mstrMasterFolder = mstrSourceFolder & "\Update\Master"
    
    Call lstStatus.AddItem("Checking for master folder" & vbTab & _
        mstrMasterFolder)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    
    If moFSO.FolderExists(mstrSourceFolder & "\Update\Master") = False Then
        Call moFSO.CreateFolder(mstrSourceFolder & "\Update\Master")
    End If

End Sub 'CheckForMasterFolder

Private Sub CheckForVerionFolder()

    Call lstStatus.AddItem("Checking for version folder" & vbTab & _
        mstrWSFolder & "\Version")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    
    If moFSO.FolderExists(mstrWSFolder & "\Version") = False Then
        Call moFSO.CreateFolder(mstrWSFolder & "\Version")
    End If

End Sub 'CheckForVersionFolder


Private Sub InitialiseSettings()
        
    Call lstStatus.AddItem("Initialising System")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "Initialise Settings", endlDebug, "Initialising System")
    DoEvents
    mstrWindFlder = vbExt.SystemFolderName
    Call lstStatus.AddItem("Initialising System " & vbTab & "mstrWindFlder = " & _
        mstrWindFlder)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "Initialise Settings", endlDebug, "mstrWindFlder = " & _
        mstrWindFlder)
    DoEvents
    mstrWSID = GetRegSetting("software\CTS Retail\Default\Oasys\2.0\Wickes\Enterprise", "WorkstationID", "", HKEY_LOCAL_MACHINE)
    Call lstStatus.AddItem("Initialising System " & vbTab & "mstrWSID = " & _
        mstrWSID)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "Initialise Settings", endlDebug, "mstrWSID = " & _
        mstrWindFlder)
    DoEvents
    mstrSourceFolder = "F:\CTS"
    Call lstStatus.AddItem("Initialising System " & vbTab & "mstrSourceFolder = " & _
        mstrSourceFolder)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "Initialise Settings", endlDebug, "mstrSourceFolder = " & _
        mstrSourceFolder)
    DoEvents
    mstrAppFolder = GetRegSetting("software\CTS Retail\Default\Oasys\2.0\Wickes", "BinPath", "", HKEY_LOCAL_MACHINE)
    Call lstStatus.AddItem("Initialising System " & vbTab & "mstrAppFolder = " & _
        mstrAppFolder)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "Initialise Settings", endlDebug, "mstrAppFolder = " & _
        mstrAppFolder)
    DoEvents
    
End Sub 'InitialiseSettings

Private Function CheckForGOFile() As Boolean

    CheckForGOFile = False
    Call lstStatus.AddItem("Checking for GO file " & vbTab & _
        mstrWSFolder & "\" & mstrWSID & ".go")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FileExists(mstrWSFolder & "\" & mstrWSID & ".go") Then
        CheckForGOFile = True
    End If

End Function 'CheckForGOFile

Private Sub UpdateSystem()

Dim oFiles          As Files
Dim oFile           As File
Dim oDestFile       As File
Dim strFileName     As String
Dim blnRegasmExists As Boolean
Dim blnIsDll        As Boolean
    
    On Error GoTo UpdateSystem_Error
    
    Call lstStatus.AddItem("Update the system with new dll's etc")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "Update the system with new dll's etc")
    
    'Ensure that ESocket system is shutdown
    While ProcessRunning("ESocket")
        Call lstStatus.AddItem("Closing ESocket Software:" & Format(Now(), "DD-MM-YYYY HH:MM:SS"))
        Call lstStatus.Refresh
        KillProcess ("ESocket")
        Wait (1) 'give the EXE a chance to shutdown
    Wend
    
    'Ensure that Fuzzy Matching/PIM system is shutdown
    While ProcessRunning("ItemFilter")
        Call lstStatus.AddItem("Closing Fuzzy Matching:" & Format(Now(), "DD-MM-YYYY HH:MM:SS"))
        Call lstStatus.Refresh
        KillProcess ("ItemFilter")
        Wait (1) 'give the EXE a chance to shutdown
    Wend
    
    With moFSO
        blnRegasmExists = .FileExists(Replace(mstrAppFolder & "\", "\\", "\") & "regasm.exe")
        Set oFiles = .GetFolder(mstrMasterFolder).Files
    End With
    
    For Each oFile In oFiles
        Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "File - " & oFile.Name)
        Call lstStatus.AddItem("File - " & oFile.Name)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        strFileName = oFile.Name
        If (Right$(mstrAppFolder, 1) <> "\") Then
            strFileName = "\" & strFileName
        End If
        '
        ' Now also using Dotnet COM wrappered dlls that need regasm rather than Regsvr32
        If PossibleDotNetCOMDll(strFileName, blnIsDll) And blnRegasmExists Then
            Call lstStatus.AddItem("UnRegistering " & oFile.Path)
            lstStatus.Selected(lstStatus.NewIndex) = True
            Call lstStatus.Refresh
            If Not RegisterDotNetAssembly(strFileName, True) Then
                Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "Failed to Unregister Dotnet COM visible " & mstrAppFolder & strFileName)
            End If
        ' Maybe a dll but not a Dotnet COM wrappered dll or its an ocx, use regsvr32 instead
        ElseIf blnIsDll Or (Right$(LCase(strFileName), 3) = "ocx") Then
            Call lstStatus.AddItem("UnRegistering " & oFile.Path)
            lstStatus.Selected(lstStatus.NewIndex) = True
            Call lstStatus.Refresh
            Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "UnRegistering " & mstrAppFolder & strFileName)
            'Unregister file
            ' 12 = Timeout for SHELLWAIT process completion, 5 second interval so 12 * 5 secs = 1 minute.
            Call ShellWait(mstrWindFlder & "\REGSVR32.EXE /u /s " & Chr(34) & mstrAppFolder & strFileName & Chr(34), vbHide, 12)
            If (UCase(oFile.Name) <> UCase("VbUtils.dll")) Then ' Can't debugmsg if VbUtils.dll has been unregistered.
                Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "UnRegistered " & mstrAppFolder & strFileName)
            End If
        End If
        Call lstStatus.AddItem("Copying " & vbTab & mstrMasterFolder & "\" & oFile.Name & " to " & _
            mstrAppFolder & "\" & oFile.Name)
        Call lstStatus.Refresh
        If (UCase(oFile.Name) <> UCase("VbUtils.dll")) Then ' Can't debugmsg if VbUtils.dll has been unregistered.
            Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "Copying " & mstrMasterFolder & "\" & oFile.Name & _
                        " to " & mstrAppFolder & "\" & oFile.Name)
        End If
        On Error Resume Next
        Set oDestFile = moFSO.GetFile(mstrAppFolder & "\" & oFile.Name)
        If ((oDestFile Is Nothing) = False) Then
            If ((oDestFile.Attributes And ReadOnly) = ReadOnly) Then oDestFile.Attributes = oDestFile.Attributes And (255 - ReadOnly)
        End If
        On Error GoTo UpdateSystem_Error
        Set oDestFile = Nothing
        
        Call moFSO.CopyFile(mstrMasterFolder & "\" & oFile.Name, mstrAppFolder & "\" & oFile.Name, True)
RegisterPermissionError_Resume:
        blnIsDll = False
        'Register file
        If PossibleDotNetCOMDll(strFileName, blnIsDll) And blnRegasmExists Then
            If Not RegisterDotNetAssembly(strFileName) Then
                Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "Failed to Register Dotnet COM visible " & mstrAppFolder & strFileName)
            End If
        ' Maybe a dll but not a Dotnet COM wrappered dll or its an ocx, use regsvr32 instead
        ElseIf blnIsDll Or (Right$(LCase(strFileName), 3) = "ocx") Then
            If (UCase(oFile.Name) <> UCase("VbUtils.dll")) Then ' Can't debugmsg if VbUtils.dll has been unregistered.
                Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "Registering " & mstrAppFolder & strFileName)
            End If
            Call ShellWait(mstrWindFlder & "\REGSVR32.EXE /s " & Chr(34) & mstrAppFolder & strFileName & Chr(34), vbHide, 12)
            If (UCase(oFile.Name) <> UCase("VbUtils.dll")) Then ' Can't debugmsg if VbUtils.dll has been unregistered.
                Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, "Registered " & mstrAppFolder & strFileName)
            End If
        End If
        lstStatus.Selected(lstStatus.NewIndex) = True
    Next

    'Call LaunchESocket
    
    Exit Sub
    
UpdateSystem_Error:

    If (Err.Number = 70) Then
        Call lstStatus.AddItem("Error 70 Catch - Error No. - " & Err.Number & " - Desc - " & Err.Description)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        If (UCase(oFile.Name) <> UCase("VbUtils.dll")) Then ' Can't debugmsg if VbUtils.dll has been unregistered.
            Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, _
                            "Error 70 Catch - Error No. - " & Err.Number & " - Desc - " & Err.Description)
        End If
        Resume RegisterPermissionError_Resume
    Else
        Call lstStatus.AddItem("Error No. - " & Err.Number & " - Desc - " & Err.Description)
        Call lstStatus.Refresh
        lstStatus.Selected(lstStatus.NewIndex) = True
        If (UCase(oFile.Name) <> UCase("VbUtils.dll")) Then ' Can't debugmsg if VbUtils.dll has been unregistered.
            Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, _
                            "Error No. - " & Err.Number & " - Desc - " & Err.Description)
        End If
        Call MsgBox("Error in - frmUpdate - UpdateSystem. Error No. - " & Err.Number & " - Desc - " & Err.Description, _
                        vbCritical + vbOKOnly, "Error in - frmUpdate - UpdateSystem")
    End If

End Sub 'UpdateSystem

Private Sub CreateVersionFile()

    Call lstStatus.AddItem("Creating " & vbTab & mstrWSFolder & "\Version\Version." & mstrWSID)
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    If moFSO.FileExists(mstrWSFolder & "\Version\Version." & mstrWSID) = False Then
        Call moFSO.CreateTextFile(mstrWSFolder & "\Version\Version." & mstrWSID, True)
    End If

End Sub 'CreateVersionFile

Private Sub RecordVersions()

Dim oFiles      As Files
Dim oFile       As File
Dim tsVersion   As TextStream
    
    Call lstStatus.AddItem("Recording versions")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Set oFiles = moFSO.GetFolder(mstrAppFolder).Files
    Set tsVersion = moFSO.OpenTextFile(mstrWSFolder & "\Version\Version." & mstrWSID, ForWriting)
    For Each oFile In oFiles
        Call tsVersion.WriteLine(oFile.Name & "," & modFileVersion.GetFileVersion(mstrAppFolder & "\" & oFile.Name))
    Next
    Call tsVersion.Close

End Sub 'RecordVersions

Private Sub DeleteGoFile()

    Call lstStatus.AddItem("Deleting " & vbTab & mstrWSFolder & "\" & mstrWSID & ".go")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    On Error Resume Next
    Call moFSO.DeleteFile(mstrWSFolder & "\" & mstrWSID & ".go", True)
    Err.Clear
    On Error GoTo 0

End Sub 'DeleteGoFile

' Check to see if REGSVR32.EXE exists in the expected location.
Private Function CheckForREGSVR() As Boolean

Dim objFSO As Scripting.FileSystemObject

    On Error GoTo CheckForREGSVR_Error
    
    Call lstStatus.AddItem("Checking for " & mstrWindFlder & "\REGSVR32.EXE")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "CheckForREGSVR", endlDebug, "Checking for " & mstrWindFlder & "\REGSVR32.EXE")
    
    Set objFSO = New Scripting.FileSystemObject
    If (objFSO.FileExists(mstrWindFlder & "\REGSVR32.EXE") = False) Then
        Call DebugMsg(MODULE_NAME, "CheckForREGSVR", endlDebug, "Error - " & mstrWindFlder & "\REGSVR32.EXE could not be found")
        Call MsgBox("The file REGSVR32.EXE could not be found in" & vbCrLf & _
                    " the location - " & mstrWindFlder & "\REGSVR32.EXE" & vbCrLf & vbCrLf & _
                    " Please check this problem.", vbCritical + vbOKOnly, "REGSVR32.EXE Not Found")
        CheckForREGSVR = False
    Else
        Call DebugMsg(MODULE_NAME, "CheckForREGSVR", endlDebug, "Success - " & mstrWindFlder & "\REGSVR32.EXE found")
        CheckForREGSVR = True
    End If

    Set objFSO = Nothing
    
    Exit Function
    
CheckForREGSVR_Error:

    Set objFSO = Nothing
    Call DebugMsg(MODULE_NAME, "CheckForREGSVR", endlDebug, _
                    "Error No. - " & Err.Number & " - Desc - " & Err.Description)
    Call MsgBox("Error in - frmUpdate - CheckForREGSVR. Error No. - " & Err.Number & " - Desc - " & Err.Description, _
                    vbCritical + vbOKOnly, "Error in - frmUpdate - CheckForREGSVR")

End Function

' Run the RegAll batch file.
Private Sub RegAll()

Dim objFSO      As Scripting.FileSystemObject
Dim strFileName As String

    On Error GoTo RegAll_Error
    
    Call lstStatus.AddItem("Call RegAll.BAT")
    Call lstStatus.Refresh
    lstStatus.Selected(lstStatus.NewIndex) = True
    Call DebugMsg(MODULE_NAME, "RegAll", endlDebug, "Call RegAll.BAT")
    
    Set objFSO = New Scripting.FileSystemObject
    
    ' If RegAll.BAT exists run it.
    strFileName = "RegAll.BAT"
    If (Right(mstrAppFolder, 1) <> "\") Then
        strFileName = "\" & strFileName
    End If
    If (objFSO.FileExists(mstrAppFolder & strFileName) = True) Then
            Call lstStatus.AddItem("Running RegAll")
            Call DebugMsg(MODULE_NAME, "RegAll", endlDebug, "Running RegAll.BAT")
            ' 240 = Timeout for SHELLWAIT process completion, 5 second interval so 12 * 5 secs = 02 minutes.
            Call ShellWait(Chr(34) & mstrAppFolder & strFileName & Chr(34), SW_SHOWNORMAL, 240)
            Call DebugMsg(MODULE_NAME, "RegAll", endlDebug, "RegAll.BAT processed")
    End If
    lstStatus.Selected(lstStatus.NewIndex) = True
    lstStatus.Refresh

    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, "RegAll", endlDebug, "RegAll.BAT completed")
    
    Exit Sub
    
RegAll_Error:

    Set objFSO = Nothing
    Call DebugMsg(MODULE_NAME, "RegAll", endlDebug, _
                    "Error No. - " & Err.Number & " - Desc - " & Err.Description)
    Call MsgBox("Error in - frmUpdate - RegAll. Error No. - " & Err.Number & " - Desc - " & Err.Description, _
                    vbCritical + vbOKOnly, "Error in - frmUpdate - RegAll")

End Sub

Private Sub LaunchESocket()
    
    If moFSO.FileExists("C:\postilion\esocket.pos\bin\run_xml.bat") Then
        On Error Resume Next
        Call Shell(Chr(34) & "C:\postilion\esocket.pos\bin\run_xml.bat" & Chr(34), vbMinimizedNoFocus)
        On Error GoTo 0
        Wait (6)
    End If
End Sub

' Now using Dotnet COM wrappered dlls that need regasm rather than Regsvr32
' Using naming convention of COM prefix to filenames for these COM wrappered dotnet dlls
' (except 3rd party COMLink.dll, which is not used any more so shouldn't be in master folder, but just to be sure)
Private Function PossibleDotNetCOMDll(ByVal strFileName As String, ByRef IsDll As Boolean) As Boolean

    If (Right$(LCase(strFileName), 3) = "dll") Then
        IsDll = True
        If (Left$(LCase(strFileName), 3) = "com" Or Left$(LCase(strFileName), 4) = "\com") And LCase(strFileName) <> "comlink.dll" Then
            PossibleDotNetCOMDll = True
        End If
    End If
End Function

' Register / unregister the assembly using regasm.exe
Private Function RegisterDotNetAssembly(strFileName As String, Optional blnUnregister As Boolean = False) As Boolean
    Dim strTypeLib As String
    Dim strFilepath As String
    
On Error GoTo Failed

    strFilepath = Replace(mstrAppFolder & "\" & strFileName, "\\", "\")
    Call DebugMsg(MODULE_NAME, "RegisterDotNetAssembly", endlDebug, IIf(blnUnregister, "Un", "") & "Registering Dotnet COM visible " & strFilepath)
    ' Make sure any existing .tlb (created by previous regasm) that goes with the com visible dll can be overwritten
    strTypeLib = Replace(strFilepath, ".dll", ".tlb")
    If moFSO.FileExists(strTypeLib) Then
        With moFSO.GetFile(strTypeLib)
            If (.Attributes And ReadOnly) = ReadOnly Then
                Call DebugMsg(MODULE_NAME, "RegisterDotNetAssembly", endlDebug, "Making type library " & strTypeLib & " writable.")
                .Attributes = .Attributes And (255 - ReadOnly)
            End If
        End With
    End If
    'Un/register file
    Call ShellWait(mstrAppFolder & "\REGASM.EXE " & IIf(blnUnregister, "/u ", "") & "/s /tlb /codebase " & Chr(34) & strFilepath & Chr(34), vbHide, 12)
    Call DebugMsg(MODULE_NAME, "UpdateSystem", endlDebug, IIf(blnUnregister, "Un", "") & "UnRegistered Dotnet COM visible " & strFilepath)
    RegisterDotNetAssembly = True
    
    Exit Function
Failed:
    RegisterDotNetAssembly = False
End Function


