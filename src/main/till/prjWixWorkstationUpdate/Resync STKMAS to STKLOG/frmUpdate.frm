VERSION 5.00
Begin VB.Form frmUpdate 
   Caption         =   "Topps - Create DLTOTS"
   ClientHeight    =   5610
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6150
   Icon            =   "frmUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   6150
   StartUpPosition =   1  'CenterOwner
   Begin VB.Label lblStockTake 
      BackStyle       =   0  'Transparent
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   4320
      Width           =   5535
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   5895
   End
End
Attribute VB_Name = "frmUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim madoConn            As ADODB.Connection

Private Sub ReverseSTKLOG()

Dim rsSTKLOG    As New Recordset
Dim rsSTKMAS    As New Recordset
Dim strSql      As String
Dim dteStkTake  As Date
Dim lngCount    As Long

Dim strAdjCode   As String
Dim dteDate     As Date
Dim strTranNo   As String

Dim lngStkONHA  As Long

On Error GoTo UpdateError

    Set madoConn = New ADODB.Connection
    Call madoConn.Open("OASYS")
    'create FileSystemObject
    
    lngCount = 0
    
    Call rsSTKLOG.Open("SELECT * FROM STKLOG WHERE DATE1='2007-12-20' AND TYPE='AD' AND KEYS LIKE '20/12/0704RF%' ORDER BY TKEY", madoConn)
    lblStatus.Caption = "Accessing List Of STKLOGs"
    Me.Refresh
    'read the context of the text file and add it to the text hold variable
    While Not rsSTKLOG.EOF
    
        lblStatus.Caption = "Updating STKMAS - SKU= " & rsSTKLOG.Fields("SKUN")
        Me.Refresh
        If (Trim$(rsSTKLOG.Fields("SPARE")) = "") Then
            Call rsSTKMAS.Open("SELECT SKUN,ONHA FROM STKMAS WHERE SKUN='" & rsSTKLOG.Fields("SKUN") & "'", madoConn)
            
            Call madoConn.Execute("INSERT INTO STKLOG (DATE1, TIME,SKUN,TYPE,KEYS,TILL,TRAN,QUAN,ONHA,CVAL,RVAL,NVAL,QPSU,STOR,""USER"",COMMHO,TRADER,CRAC,DESCR,LVAL,CORD,PAY_USER,SPARE) VALUES ('" & _
                Format(Date, "YYYY-MM-DD") & "','" & Format(Now(), "HHNNSS") & "','" & rsSTKLOG.Fields("SKUN") & "','" & rsSTKLOG.Fields("TYPE") & "','" & rsSTKLOG.Fields("KEYS") & "','" & rsSTKLOG.Fields("TILL") & _
                "','" & rsSTKLOG.Fields("TRAN") & "'," & rsSTKLOG.Fields("QUAN") * -1 & "," & rsSTKMAS.Fields("ONHA") & "," & rsSTKLOG.Fields("CVAL") * -1 & "," & rsSTKLOG.Fields("RVAL") * -1 & "," & _
                rsSTKLOG.Fields("NVAL") & "," & rsSTKLOG.Fields("QPSU") & ",'" & rsSTKLOG.Fields("STOR") & "','" & rsSTKLOG.Fields("USER") & "','0000','" & rsSTKLOG.Fields("TRADER") & "','" & rsSTKLOG.Fields("CRAC") & _
                "','SYSFIX-REV RF ADJ','" & rsSTKLOG.Fields("LVAL") & "','" & rsSTKLOG.Fields("CORD") & "','" & rsSTKLOG.Fields("PAY_USER") & "','" & rsSTKLOG.Fields("SPARE") & "')")
            
            Call madoConn.Execute("UPDATE STKMAS SET ONHA=ONHA - " & rsSTKLOG.Fields("QUAN") & " WHERE SKUN='" & rsSTKLOG.Fields("SKUN") & "'")
            Call rsSTKMAS.Close
            lblStatus.Refresh
            'Call DebugMsg("mm", "", endlDebug, "Updating STKMAS= " & lngStkONHA)
            
            Call madoConn.Execute("UPDATE STKLOG SET SPARE='SYSFIX' WHERE TKEY=" & rsSTKLOG.Fields("TKEY"))
        End If
        Call rsSTKLOG.MoveNext
    Wend
    
    'close the text file
    Call rsSTKLOG.Close
    
    Call madoConn.Close
    lblStatus.Caption = lblStatus.Caption & "Update Complete"
    
    Exit Sub
    
UpdateError:
    Call MsgBox(Err.Description)
    Call Err.Clear
    
    
End Sub 'Update


Private Sub UpdateFromSTKLOGStart()

Dim rsSTKLOGs    As New Recordset
Dim rsSTKMAS    As New Recordset
Dim rsSTKLOG    As New Recordset
Dim strSql      As String
Dim blnUpdateHdr As Boolean
Dim dteStkTake  As Date
Dim lngCount    As Long

Dim strAdjCode   As String
Dim dteDate     As Date
Dim strTranNo   As String

Dim lngStkONHA  As Long

On Error GoTo UpdateError

    Set madoConn = New ADODB.Connection
    Call madoConn.Open("WIX")
    'create FileSystemObject
    
    lngCount = 0
    
    Call rsSTKLOGs.Open("SELECT MAX(TKEY),SKUN FROM STKLOG WHERE DATE1>='2008-09-21' GROUP BY SKUN ORDER BY SKUN", madoConn)
    lblStatus.Caption = "Accessing List Of STKLOGs"
    Me.Refresh
    'read the context of the text file and add it to the text hold variable
    While Not rsSTKLOGs.EOF
    
        lblStatus.Caption = "Updating STKMAS - SKU= " & rsSTKLOGs.Fields("SKUN")
        Me.Refresh
        Call rsSTKLOG.Open("SELECT TKEY,ESTK,EPRI,ERET,EMDN,EWTF FROM STKLOG WHERE TKEY = " & rsSTKLOGs.Fields(0), madoConn)
        
'        Call DebugMsg("mm", "", endlDebug, "Starting STKMAS= " & lngStkONHA & ":" & rsSTKLOGs.Fields("SKUN"))
        
        'First record is the starting position so skip this one
        Call madoConn.Execute("UPDATE STKMAS SET ONHA=" & rsSTKLOG("ESTK") & ",PRIC=" & rsSTKLOG("EPRI") & ",RETQ=" & rsSTKLOG("ERET") & ",MDNQ=" & rsSTKLOG("EMDN") & ",WTFQ=" & rsSTKLOG("EWTF") & ",SPARE='TK=" & rsSTKLOGs(0) & "' WHERE SKUN='" & rsSTKLOGs.Fields("SKUN") & "'")
        rsSTKLOG.Close
        Call rsSTKLOGs.MoveNext
    Wend
    
    'close the text file
    Call rsSTKLOGs.Close
    
    Call madoConn.Close
    lblStatus.Caption = lblStatus.Caption & "Update Complete"
    
    Exit Sub
    
UpdateError:
    Call MsgBox(Err.Description)
    Call Err.Clear
    
    
End Sub 'Update

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
Dim oFSO As New FileSystemObject
Dim tsLogFile As TextStream
    
    Me.Show
    Call UpdateFromSTKLOGStart
    Set tsLogFile = oFSO.OpenTextFile(App.Path & "\" & App.EXEName & ".LOG", ForAppending, True)
    Call tsLogFile.Close
    End

End Sub 'form_load

