VERSION 5.00
Begin VB.Form frmUpdate 
   Caption         =   "Topps - Create DLTOTS"
   ClientHeight    =   5610
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6150
   Icon            =   "frmUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   6150
   StartUpPosition =   1  'CenterOwner
   Begin VB.Label lblStockTake 
      BackStyle       =   0  'Transparent
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   4320
      Width           =   5535
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   5895
   End
End
Attribute VB_Name = "frmUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim madoConn            As ADODB.Connection

Private Sub Update()

Dim rsWOrders    As New Recordset
Dim strSql       As String
Dim lngCount    As Long


On Error GoTo UpdateError

    Set madoConn = New ADODB.Connection
    Call madoConn.Open("OASYS")
    'create FileSystemObject
    
    lngCount = 0
    Call madoConn.Execute("UPDATE STKMAS SET WQTY=0")
    
    Call rsWOrders.Open("SELECT stkmas.skun, wqty, sum(webline.quan) as linetotal " & _
                "FROM webtots inner join webline " & _
                "ON webtots.date1 = webline.date1 " & _
                "AND webtots.till = webline.till " & _
                "AND webtots.tran = webline.tran " & _
                "AND webtots.stat in ('O','H') " & _
                "AND Webtots.tcod<>'RF' " & _
                "AND WebLine.Quan > 0 " & _
                "inner join stkmas " & _
                "ON webline.skun = stkmas.skun " & _
                "GROUP BY stkmas.skun,wqty " & _
                "HAVING wqty <> Sum(webline.quan)", madoConn)
                
    lblStatus.Caption = "Accessing List Of Open Web Orders"
    Me.Refresh
    'read the context of the text file and add it to the text hold variable
    While Not rsWOrders.EOF
    
        lblStatus.Caption = "Updating STKMAS - SKU= " & rsWOrders.Fields("SKUN")
        Me.Refresh
        'Preserve cost on STKADJ First
        Call madoConn.Execute("UPDATE STKMAS SET WQTY=" & rsWOrders.Fields("LineTotal") & " WHERE SKUN='" & rsWOrders.Fields("SKUN") & "'")
        Call rsWOrders.MoveNext
    Wend
    
    'close the text file
    Call rsWOrders.Close
    
    
    Call madoConn.Close
    lblStatus.Caption = lblStatus.Caption & "Update Complete"
    
    Exit Sub
    
UpdateError:
    Call MsgBox(Err.Description)
    Call Err.Clear
    
    
End Sub 'Update

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
Dim oFSO As New FileSystemObject
Dim tsLogFile As TextStream
    
    Me.Show
    Call Update
    Set tsLogFile = oFSO.OpenTextFile(App.Path & "\" & App.EXEName & ".LOG", ForAppending, True)
    Call tsLogFile.Close
    End

End Sub 'form_load

