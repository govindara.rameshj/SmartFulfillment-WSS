VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmAgeCheckEnquiry 
   Caption         =   "Age Check Enquiry"
   ClientHeight    =   7800
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11880
   Icon            =   "frmAgeCheckItemEnq.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7800
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraSorted 
      Height          =   555
      Left            =   1380
      TabIndex        =   22
      Top             =   6540
      Width           =   5415
      Begin VB.Label lblSortedBy 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1140
         TabIndex        =   26
         Top             =   180
         Width           =   4095
      End
      Begin VB.Label Label1 
         Caption         =   "Display Order"
         Height          =   255
         Left            =   60
         TabIndex        =   23
         Top             =   180
         Width           =   1155
      End
   End
   Begin VB.Frame fraCriteria 
      Caption         =   "Search Criteria"
      Height          =   1155
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   11745
      Begin VB.ComboBox cmbUserID 
         Height          =   315
         Left            =   5940
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   300
         Width           =   1995
      End
      Begin VB.TextBox txtSKU 
         Height          =   315
         Left            =   8340
         TabIndex        =   13
         Top             =   300
         Width           =   675
      End
      Begin VB.TextBox txtTranNo 
         Height          =   315
         Left            =   4980
         MaxLength       =   4
         TabIndex        =   9
         Top             =   300
         Width           =   495
      End
      Begin VB.ComboBox cmbTillID 
         Height          =   315
         Left            =   3840
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   300
         Width           =   675
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3-Reset"
         Height          =   375
         Left            =   10500
         TabIndex        =   18
         Top             =   660
         Width           =   915
      End
      Begin VB.ComboBox cmbReasonCode 
         Height          =   315
         Left            =   720
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   720
         Width           =   6855
      End
      Begin ucEditDate.ucDateText dtxtEndDate 
         Height          =   315
         Left            =   2580
         TabIndex        =   5
         Top             =   300
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "22/01/07"
      End
      Begin VB.ComboBox cmbDateCrit 
         Height          =   315
         Left            =   540
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   300
         Width           =   915
      End
      Begin ucEditDate.ucDateText dtxtStartDate 
         Height          =   315
         Left            =   1500
         TabIndex        =   3
         Top             =   300
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "22/01/07"
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Apply"
         Height          =   375
         Left            =   9360
         TabIndex        =   17
         Top             =   660
         Width           =   915
      End
      Begin VB.Label Label8 
         Caption         =   "User"
         Height          =   315
         Left            =   5520
         TabIndex        =   10
         Top             =   300
         Width           =   375
      End
      Begin VB.Label lblSKUDesc 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   9060
         TabIndex        =   14
         Top             =   300
         Width           =   2295
      End
      Begin VB.Label Label6 
         Caption         =   "SKU"
         Height          =   315
         Left            =   7980
         TabIndex        =   12
         Top             =   300
         Width           =   375
      End
      Begin VB.Label Label5 
         Caption         =   "Tran"
         Height          =   255
         Left            =   4560
         TabIndex        =   8
         Top             =   300
         Width           =   435
      End
      Begin VB.Label Label3 
         Caption         =   "Till"
         Height          =   315
         Left            =   3540
         TabIndex        =   6
         Top             =   300
         Width           =   375
      End
      Begin VB.Label Label4 
         Caption         =   "Result"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   780
         Width           =   615
      End
      Begin VB.Label lblTolbl 
         Caption         =   "to"
         Height          =   255
         Left            =   2400
         TabIndex        =   4
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label2 
         Caption         =   "Date"
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   435
      End
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   4680
      TabIndex        =   20
      Top             =   2280
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9 - Print"
      Height          =   435
      Left            =   8280
      TabIndex        =   24
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   435
      Left            =   120
      TabIndex        =   21
      Top             =   6660
      Width           =   1155
   End
   Begin FPSpreadADO.fpSpread sprdReport 
      Height          =   4635
      Left            =   60
      TabIndex        =   19
      Top             =   1320
      Visible         =   0   'False
      Width           =   14415
      _Version        =   458752
      _ExtentX        =   25426
      _ExtentY        =   8176
      _StockProps     =   64
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   8
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmAgeCheckItemEnq.frx":0A96
      UserResize      =   2
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   25
      Top             =   7425
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmAgeCheckItemEnq.frx":0F48
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13785
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "14:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmAgeCheckEnquiry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 26/07/10
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Labels Request/frmAgeCheckItemEnquiry.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************
Option Explicit

Const COL_DATE          As Long = 1
Const COL_TILLID        As Long = 2
Const COL_TRANNO        As Long = 3
Const COL_OVCTRANNO        As Long = 4
Const COL_USERID        As Long = 5
Const COL_SKU           As Long = 6
Const COL_REASONCODE    As Long = 7
Const COL_SORTDATE      As Long = 8

Private Const CRIT_EQUALS        As Long = 0
Private Const CRIT_GREATERTHAN   As Long = 1
Private Const CRIT_LESSTHAN      As Long = 2
Private Const CRIT_ALL           As Long = 3
Private Const CRIT_FROM          As Long = 4
Private Const CRIT_WEEKENDING    As Long = 5

Const PRM_TRANSNO   As Long = 911

Private mdteSystemDate  As Date
Private mblnAutoApply   As Boolean

Private mblnFromNightlyClose As Boolean
Private mstrDestCode         As Boolean
Private mblnCtrlHeld         As Boolean

Private mlngDatedPos    As Long
Private mstrSortList    As String

Private Sub cmbReasonCode_GotFocus()
    
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbReasonCode_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbDateCrit_Click()
    
    If cmbDateCrit.ListIndex = -1 Then Exit Sub
    dtxtEndDate.Visible = False
    lblTolbl.Visible = False
    If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) = CRIT_ALL Then
        dtxtStartDate.Visible = False
    Else
        dtxtStartDate.Visible = True
        If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) = CRIT_FROM Then
            dtxtEndDate.Visible = True
            lblTolbl.Visible = True
        End If
    End If
    
End Sub

Private Sub cmbDateCrit_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbDateCrit_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmbSortBy_GotFocus()
    
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbTillID_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbUserID_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdApply_Click()

    Call LoadAgeChecks

End Sub
Private Sub cmdApply_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdExit_Click()

    End

End Sub

Private Sub cmdPrint_Click()

Dim strHeader   As String
Dim oSysOptBO   As cSystemOptions
Dim lngLineNo   As Long

    Screen.MousePointer = vbHourglass
    'If printed in PlanoGram order then hide all non Primary Planogram Locations
    
    Set oSysOptBO = goSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreNumber)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreName)
    Call oSysOptBO.LoadMatches

    ' 02/05/03  KVB  remove Printed dd/mm/yy from strHeader
    strHeader = "Store No: " & oSysOptBO.StoreNumber & " " & _
                oSysOptBO.StoreName & "/n/cAge Check Report"
            strHeader = strHeader & "/n/cDated "
            Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
                Case (CRIT_ALL): strHeader = strHeader & ": ALL"
                Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtStartDate.Text & " to " & dtxtEndDate.Text
                Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtStartDate.Text
                Case Else: strHeader = strHeader & cmbDateCrit.Text & ": " & dtxtStartDate.Text
            End Select
    
    ' /c is Centre text      /fb1 is Font bold on
    sprdReport.PrintHeader = "/c/fb1" & strHeader & IIf(lblSortedBy.Caption = "", "", "   Ordered by: " & lblSortedBy.Caption)
    sprdReport.PrintHeader = sprdReport.PrintHeader & IIf(cmbUserID.Text = "ALL", "", "   For : " & cmbUserID.Text)
    
    sprdReport.PrintJobName = "Age Check Report"
       
    sprdReport.PrintFooter = GetFooter

    sprdReport.PrintOrientation = PrintOrientationLandscape
    
    sprdReport.Refresh
    
    sprdReport.PrintFooter = GetFooter
    sprdReport.PrintRowHeaders = False
    sprdReport.Refresh
    sprdReport.PrintSheet
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdReset_Click()
        
    cmbDateCrit.Enabled = True
    dtxtStartDate.Enabled = True
    dtxtEndDate.Enabled = True
    cmbReasonCode.Enabled = True
    sprdReport.MaxRows = 0
    cmbTillID.Enabled = True
    txtTranNo.Enabled = True
    cmbUserID.Enabled = True
    txtSKU.Enabled = True
    cmbDateCrit.SetFocus
    cmdPrint.Visible = False
    cmbDateCrit.ListIndex = mlngDatedPos
    cmbTillID.ListIndex = 0
    txtTranNo.Text = ""
    cmbUserID.ListIndex = 0
    txtSKU.Text = ""
    cmbReasonCode.ListIndex = 0

End Sub

Private Sub dtxtEndDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtStartDate_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

    Call DebugMsg("x", "s", endlDebug, KeyCode & "," & Shift & "-" & mblnCtrlHeld)
    If (KeyCode = 17) And (Shift = 0) Then mblnCtrlHeld = False

End Sub

Private Sub Form_Load()

Dim oSysDat As cSystemDates
Dim lngOptNo    As Long
Dim lngItemNo   As Long

    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    
    Screen.MousePointer = vbHourglass
    Call InitialiseStatusBar(sbStatus)
    
    ' Date Combo Box
    ' ==============
    cmbDateCrit.Clear
    Call cmbDateCrit.AddItem("After")
    cmbDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbDateCrit.AddItem("Before")
    cmbDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbDateCrit.AddItem("Dated")
    cmbDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbDateCrit.AddItem("All")
    cmbDateCrit.ItemData(3) = CRIT_ALL
    Call cmbDateCrit.AddItem("From")
    cmbDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItemNo = 0 To cmbDateCrit.ListCount Step 1
        If cmbDateCrit.ItemData(lngItemNo) = CRIT_EQUALS Then
            mlngDatedPos = lngItemNo
            cmbDateCrit.ListIndex = lngItemNo
            Exit For
        End If
    Next lngItemNo
    
    Call Me.Show
    DoEvents
    
    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.LoadMatches
    
    mdteSystemDate = oSysDat.TodaysDate
    dtxtStartDate.Text = DisplayDate(Date, False)
    dtxtEndDate.Text = DisplayDate(Date, False)
    Set oSysDat = Nothing
    
    Call LoadReasons
    
    Call LoadWorkstations
    
    Call LoadUsers
    
    Call DecodeParameters(goSession.ProgramParams)
    
    Screen.MousePointer = vbNormal
           
    If (mblnFromNightlyClose = True) Then
        Call cmdApply_Click
        Call cmdPrint_Click
        End
    End If
    
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 4500) Then
        Me.Width = 4500
        Exit Sub
    End If
    
    sprdReport.Width = Me.Width - sprdReport.Left * 4
    sprdReport.Height = Me.Height - sprdReport.Top - cmdPrint.Height - 720 - sbStatus.Height
    cmdPrint.Top = sprdReport.Top + sprdReport.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdPrint.Left = sprdReport.Left + sprdReport.Width - cmdPrint.Width
    fraSorted.Top = cmdExit.Top - 60
    ucpbProgress.Left = (Me.Width - ucpbProgress.Width) / 2
    ucpbProgress.Top = (Me.Height - 480 - ucpbProgress.Height) / 2

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF3): 'if F3 then reset
                    If (cmdReset.Visible = True) Then Call cmdReset_Click
                Case (vbKeyF7): 'if F9 then retrieve report
                    If (cmdApply.Visible = True) Then Call cmdApply_Click
                Case (vbKeyF9): 'if F9 then print report
                    If (cmdPrint.Visible = True) Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
         Case (2):
            If KeyCode = 17 Then mblnCtrlHeld = True
    End Select

End Sub
    
Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11

Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case (CALLED_FROM):
                If Mid(strSection, 3) = CF_CLOSE Then
                    mblnFromNightlyClose = True
                    blnSetOpts = True
                End If
        End Select
    Next lngSectNo

End Sub

Private Sub LoadAgeChecks()

Dim oItemBO     As cStock_Wickes.cInventory
Dim oHeaderBO    As cSale_Wickes.cPOSHeader
Dim oSKURejectBO As cSale_Wickes.cSKUReject
Dim colSKUReject As Collection
Dim lngRowNo    As Long
Dim lngComp     As Long
Dim lngRCodeNo  As Long

Dim dteDateValue    As Date
    
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving Items"
    ucpbProgress.Refresh
    sprdReport.MaxRows = 0
    Set oSKURejectBO = goDatabase.CreateBusinessObject(CLASSID_TRANREJECT)
        
    'Set Date criteria
    If cmbDateCrit.ItemData(cmbDateCrit.ListIndex) <> CRIT_ALL Then
        dteDateValue = GetDate(dtxtStartDate.Text)
        If Year(dteDateValue) > 1899 Then
            Select Case (cmbDateCrit.ItemData(cmbDateCrit.ListIndex))
                Case Is = CRIT_LESSTHAN
                    lngComp = CMP_LESSTHAN
                Case CRIT_GREATERTHAN
                    lngComp = CMP_GREATERTHAN
                Case CRIT_FROM
                    lngComp = CMP_GREATEREQUALTHAN
                Case Else
                    lngComp = CMP_EQUAL
            End Select
            Call oSKURejectBO.IBo_AddLoadFilter(lngComp, FID_TRANREJECT_TranDate, dteDateValue)
        End If
        If (cmbDateCrit.ItemData(cmbDateCrit.ListIndex) = CRIT_FROM) Then
            Call oSKURejectBO.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_TRANREJECT_TranDate, GetDate(dtxtEndDate.Text))
        End If
    End If
    
    If (Val(cmbReasonCode.ItemData(cmbReasonCode.ListIndex)) <> 0) Then
        Call oSKURejectBO.IBo_AddLoadFilter(CMP_LIKE, FID_TRANREJECT_RejectText, vbCrLf & Replace(cmbReasonCode.Text, "|", vbCrLf) & "%")
    Else
        Call oSKURejectBO.IBo_AddLoadFilter(CMP_LIKE, FID_TRANREJECT_RejectText, vbCrLf & "%")
    End If
    
    If (cmbTillID.Text <> "ALL") Then
        Call oSKURejectBO.IBo_AddLoadFilter(CMP_EQUAL, FID_TRANREJECT_TranTillID, cmbTillID.Text)
    End If
    
    If (txtTranNo.Text <> "") Then
        Call oSKURejectBO.IBo_AddLoadFilter(CMP_EQUAL, FID_TRANREJECT_TranNo, txtTranNo.Text)
    End If
    
    If (cmbUserID.Text <> "ALL") Then
        Call oSKURejectBO.IBo_AddLoadFilter(CMP_EQUAL, FID_TRANREJECT_UserID, Left(cmbUserID.Text, 3))
    End If
    
    If (txtSKU.Text <> "") Then
        Call oSKURejectBO.IBo_AddLoadFilter(CMP_EQUAL, FID_TRANREJECT_PartCode, txtSKU.Text)
    End If
    
    Set colSKUReject = oSKURejectBO.LoadMatches
    
    For lngRowNo = 1 To colSKUReject.Count Step 1
        ucpbProgress.Value = lngRowNo
        Set oSKURejectBO = colSKUReject(lngRowNo)
        sprdReport.MaxRows = sprdReport.MaxRows + 1
        sprdReport.Row = sprdReport.MaxRows
        sprdReport.Col = COL_DATE
        sprdReport.Text = DisplayDate(oSKURejectBO.TranDate, False)
        sprdReport.Col = COL_TILLID
        sprdReport.Text = oSKURejectBO.TranTillID
        sprdReport.Col = COL_TRANNO
        sprdReport.Text = oSKURejectBO.TranNo
        sprdReport.Col = COL_OVCTRANNO
        Set oHeaderBO = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
        Call oHeaderBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, oSKURejectBO.TranNo)
        Call oHeaderBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, oSKURejectBO.TranTillID)
        Call oHeaderBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, oSKURejectBO.TranDate)
        Call oHeaderBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_Source, "OVC")
        Call oHeaderBO.AddLoadField(FID_POSHEADER_ReceiptBarcode)
        Call oHeaderBO.LoadMatches
        sprdReport.Text = oHeaderBO.ReceiptBarcode
        sprdReport.Col = COL_USERID
        sprdReport.Text = oSKURejectBO.UserID
        sprdReport.Col = COL_REASONCODE
        sprdReport.Text = Replace(Mid$(oSKURejectBO.RejectText, 3), vbCrLf, ",")
        sprdReport.Col = COL_SKU
        Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oSKURejectBO.PartCode)
        Call oItemBO.AddLoadField(FID_INVENTORY_Description)
        Call oItemBO.LoadMatches
        sprdReport.Text = oItemBO.PartCode & "-" & oItemBO.Description
        sprdReport.Col = COL_SORTDATE
        sprdReport.Text = Format(oSKURejectBO.TranDate, "YYYYMMDD")
    Next lngRowNo
    
    sprdReport.ColWidth(COL_SKU) = sprdReport.MaxTextColWidth(COL_SKU) + 1
    sprdReport.ColWidth(COL_REASONCODE) = sprdReport.MaxTextColWidth(COL_REASONCODE) + 1
    ucpbProgress.Visible = False
    If (sprdReport.MaxRows > 0) Then
        cmbDateCrit.Enabled = False
        dtxtStartDate.Enabled = False
        dtxtEndDate.Enabled = False
        cmbReasonCode.Enabled = False
        cmbTillID.Enabled = False
        txtTranNo.Enabled = False
        cmbUserID.Enabled = False
        txtSKU.Enabled = False
        sprdReport.Visible = True
        cmdPrint.Visible = True
        sprdReport.SetFocus
    Else
        Call MsgBoxEx("No Age Check entries found for specified criteria", vbOKOnly, "Retrieve Records", , , , , RGBMSGBox_PromptColour)
        Call cmdReset_Click
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub LoadReasons()

Dim oTranRejBO  As cSale_Wickes.cSKUReject
Dim lngReasonNo As Long
Dim vntList     As Variant
Dim colReasons As New Collection
Dim strReason   As String
    
    Set oTranRejBO = goDatabase.CreateBusinessObject(CLASSID_TRANREJECT)
    
    Call cmbReasonCode.Clear
        
    vntList = goDatabase.GetAggregateValue(AGG_DIST, oTranRejBO.GetField(FID_TRANREJECT_RejectText), Nothing)
    
    cmbReasonCode.AddItem ("ALL")
    cmbReasonCode.ListIndex = 0
    If (IsNull(vntList) = False) Then
        For lngReasonNo = 0 To UBound(vntList) Step 1
            If (Left$(vntList(lngReasonNo), 2) = vbCrLf) Then
            strReason = ""
            On Error Resume Next
            strReason = colReasons(Mid$(Trim$(Replace(vntList(lngReasonNo), vbCrLf, "|")), 2))
            On Error GoTo 0
            Err.Clear
            If strReason = "" Then
                cmbReasonCode.AddItem (Mid$(Trim$(Replace(vntList(lngReasonNo), vbCrLf, "|")), 2))
                cmbReasonCode.ItemData(cmbReasonCode.NewIndex) = 1
                Call colReasons.Add("" & Mid$(Trim$(Replace(vntList(lngReasonNo), vbCrLf, "|")), 2), "" & Mid$(Trim$(Replace(vntList(lngReasonNo), vbCrLf, "|")), 2))
                End If
            End If
        Next lngReasonNo
    End If

    Set oTranRejBO = Nothing

End Sub

Private Sub LoadWorkstations()

Dim oWStationBo  As cEnterprise_Wickes.cWorkStation
Dim lngWStationNo  As Long
Dim colWStations   As Collection
    
    Set oWStationBo = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWStationBo.AddLoadField(FID_WORKSTATIONCONFIG_id)
    Set colWStations = oWStationBo.LoadMatches
    
    Call cmbTillID.Clear
        
    cmbTillID.AddItem ("ALL")
    cmbTillID.ListIndex = 0
    For Each oWStationBo In colWStations
        cmbTillID.AddItem (oWStationBo.Id)
    Next

    Set oWStationBo = Nothing

End Sub

Private Sub LoadUsers()

Dim oUserBO  As cEnterprise_Wickes.cUser
Dim colUsers   As Collection
    
    Set oUserBO = goDatabase.CreateBusinessObject(CLASSID_USER)
    Set colUsers = oUserBO.LoadMatches
    
    Call cmbUserID.Clear
        
    cmbUserID.AddItem ("ALL")
    cmbUserID.ListIndex = 0
    For Each oUserBO In colUsers
        cmbUserID.AddItem (oUserBO.EmployeeID & "-" & oUserBO.FullName)
    Next

    Set oUserBO = Nothing

End Sub

Private Sub sprdReport_Click(ByVal Col As Long, ByVal Row As Long)

Dim strColID    As String
Dim strOrder    As String
Dim lngOrderPos As Long
Dim lngColNo    As Long
Dim vntCols     As Variant

    If (Row = 0) Then 'clicked on Col Header so perform Sorting
        If (Col = COL_DATE) Then Col = COL_SORTDATE 'catch dates as they are displayed as strings
        strColID = "|" & Col & "*"
        If (InStr(mstrSortList, strColID) > 0) Then  'already in list so just flip asc/desc flag
            If (mblnCtrlHeld = False) And (InStr(2, mstrSortList, "|") > 0) Then mstrSortList = strColID & "D"
            lngOrderPos = InStr(mstrSortList, strColID) + Len(strColID)
            strOrder = Mid$(mstrSortList, lngOrderPos, 1)
            If (strOrder = "A") Then
                strOrder = "D"
            Else
                strOrder = "A"
            End If
            mstrSortList = Left$(mstrSortList, lngOrderPos - 1) & strOrder & Mid$(mstrSortList, lngOrderPos + 1)
        Else 'not in list so add
            If (mblnCtrlHeld = False) Then
                mstrSortList = strColID & "A"
            Else
                mstrSortList = mstrSortList & strColID & "A"
                'Append column to end of list
            End If
        End If
        
        'Now sort report in selected sort criteria
        lblSortedBy.Caption = ""
        'Reset Sort Criteria
        For lngOrderPos = 1 To sprdReport.MaxCols Step 1
            sprdReport.SortKey(lngOrderPos) = -1
        Next lngOrderPos
        'If Sort Criteria selected then step through and apply
        If (mstrSortList <> "") Then
            vntCols = Split(mstrSortList, "|")
            sprdReport.Row = 0
            For lngOrderPos = 1 To UBound(vntCols) Step 1
                lngColNo = Val(vntCols(lngOrderPos))
                sprdReport.Col = lngColNo
                sprdReport.SortKey(lngOrderPos) = lngColNo
                'Update Sorted By description
                lblSortedBy.Caption = lblSortedBy.Caption & sprdReport.Text
                'Determine if column should be asc/desc
                If Right$(vntCols(lngOrderPos), 1) = "A" Then
                    sprdReport.SortKeyOrder(lngOrderPos) = SortKeyOrderAscending
                    lblSortedBy.Caption = lblSortedBy.Caption & ","
                Else
                    sprdReport.SortKeyOrder(lngOrderPos) = SortKeyOrderDescending
                    lblSortedBy.Caption = lblSortedBy.Caption & "(D),"
                End If
            Next lngOrderPos
            lblSortedBy.Caption = Left$(lblSortedBy.Caption, Len(lblSortedBy.Caption) - 1)
            Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        End If
    End If

End Sub

Private Sub txtSKU_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtTranNo_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub
