VERSION 5.00
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmPOEnquiry 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Purchase Order Enquiry"
   ClientHeight    =   6330
   ClientLeft      =   195
   ClientTop       =   1395
   ClientWidth     =   14025
   Icon            =   "frmPOrderEnq.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6330
   ScaleWidth      =   14025
   WindowState     =   2  'Maximized
   Begin VB.PictureBox fraCriteria 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   120
      ScaleHeight     =   1185
      ScaleWidth      =   11625
      TabIndex        =   10
      Top             =   120
      Width           =   11655
      Begin LpLib.fpCombo cmbSupplier 
         Height          =   285
         Left            =   3045
         TabIndex        =   11
         Top             =   285
         Width           =   4380
         _Version        =   196608
         _ExtentX        =   7726
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   1
         BorderColor     =   12164479
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   1
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmPOrderEnq.frx":058A
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Apply"
         Height          =   375
         Left            =   10245
         TabIndex        =   20
         Top             =   285
         Width           =   1335
      End
      Begin VB.TextBox txtFilterOrderNo 
         Height          =   285
         Left            =   1125
         TabIndex        =   19
         Top             =   285
         Width           =   1095
      End
      Begin VB.ComboBox cmbDueDateCrit 
         Height          =   315
         ItemData        =   "frmPOrderEnq.frx":0993
         Left            =   4965
         List            =   "frmPOrderEnq.frx":09A6
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   765
         Width           =   840
      End
      Begin VB.ComboBox cmbOrderDateCrit 
         Height          =   315
         ItemData        =   "frmPOrderEnq.frx":09C0
         Left            =   1125
         List            =   "frmPOrderEnq.frx":09D3
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   765
         Width           =   840
      End
      Begin VB.ComboBox cmbStatus 
         Height          =   315
         ItemData        =   "frmPOrderEnq.frx":09ED
         Left            =   8805
         List            =   "frmPOrderEnq.frx":09FD
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   285
         Width           =   1335
      End
      Begin VB.ComboBox cmbGroupBy 
         Height          =   315
         ItemData        =   "frmPOrderEnq.frx":0A1F
         Left            =   8805
         List            =   "frmPOrderEnq.frx":0A2C
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   765
         Width           =   1335
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3-Reset"
         Height          =   375
         Left            =   10245
         TabIndex        =   12
         Top             =   765
         Visible         =   0   'False
         Width           =   1335
      End
      Begin ucEditDate.ucDateText dtxtStDueDate 
         Height          =   285
         Left            =   5805
         TabIndex        =   13
         Top             =   765
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "10/01/03"
      End
      Begin ucEditDate.ucDateText dtxtStOrderDate 
         Height          =   285
         Left            =   1965
         TabIndex        =   14
         Top             =   765
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "10/01/03"
      End
      Begin ucEditDate.ucDateText dtxtEndOrderDate 
         Height          =   285
         Left            =   3165
         TabIndex        =   21
         Top             =   765
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "10/01/03"
      End
      Begin ucEditDate.ucDateText dtxtEndDueDate 
         Height          =   285
         Left            =   7005
         TabIndex        =   22
         Top             =   765
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "10/01/03"
      End
      Begin VB.Label lblfraCriteria 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Select Order Filter Criteria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   204
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   15
         TabIndex        =   31
         Top             =   15
         Width           =   2220
      End
      Begin VB.Label Label44 
         BackStyle       =   0  'Transparent
         Caption         =   "&Order No"
         Height          =   255
         Left            =   285
         TabIndex        =   30
         Top             =   345
         Width           =   855
      End
      Begin VB.Label Label42 
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier"
         Height          =   255
         Left            =   2445
         TabIndex        =   29
         Top             =   345
         Width           =   735
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Order Date"
         Height          =   255
         Left            =   285
         TabIndex        =   28
         Top             =   825
         Width           =   975
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Due Date"
         Height          =   255
         Left            =   4245
         TabIndex        =   27
         Top             =   825
         Width           =   735
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Status"
         Height          =   255
         Left            =   8085
         TabIndex        =   26
         Top             =   345
         Width           =   615
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Group By"
         Height          =   255
         Left            =   8085
         TabIndex        =   25
         Top             =   825
         Width           =   855
      End
      Begin VB.Label lblOrderTolbl 
         BackStyle       =   0  'Transparent
         Caption         =   "to"
         Height          =   255
         Left            =   2985
         TabIndex        =   24
         Top             =   825
         Width           =   255
      End
      Begin VB.Label lblDueTolbl 
         BackStyle       =   0  'Transparent
         Caption         =   "to"
         Height          =   255
         Left            =   6840
         TabIndex        =   23
         Top             =   825
         Width           =   255
      End
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Index           =   1
      Left            =   2880
      TabIndex        =   9
      Top             =   2160
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdDetails 
      Caption         =   "F11-Enquiry"
      Height          =   405
      Left            =   5040
      TabIndex        =   8
      ToolTipText     =   "Details of current order"
      Top             =   5400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdShowdetails 
      Caption         =   "F4-Show Details"
      Height          =   405
      Left            =   6600
      TabIndex        =   7
      ToolTipText     =   "Show details for all orders"
      Top             =   5400
      Visible         =   0   'False
      Width           =   1320
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Index           =   0
      Left            =   2880
      TabIndex        =   1
      Top             =   2160
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   405
      Left            =   120
      TabIndex        =   2
      Top             =   5400
      Width           =   975
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   405
      Left            =   8640
      TabIndex        =   5
      Top             =   5400
      Visible         =   0   'False
      Width           =   975
   End
   Begin FPSpreadADO.fpSpread sprdOrders 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   1440
      Width           =   9495
      _Version        =   458752
      _ExtentX        =   16748
      _ExtentY        =   6588
      _StockProps     =   64
      ColsFrozen      =   5
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   35
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmPOrderEnq.frx":0A50
      UserResize      =   1
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   5955
      Width           =   14025
      _ExtentX        =   24739
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmPOrderEnq.frx":194B
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16933
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:45"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblNoMatches 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "N/A"
      Height          =   255
      Left            =   2400
      TabIndex        =   4
      Top             =   5520
      Width           =   1575
   End
   Begin VB.Label lblNoMatcheslbl 
      BackStyle       =   0  'Transparent
      Caption         =   "No of Matches :"
      Height          =   255
      Left            =   1200
      TabIndex        =   3
      Top             =   5520
      Width           =   1215
   End
End
Attribute VB_Name = "frmPOEnquiry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmPOEnquiry
'* Date   : 13/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Purchase Order Enquiry/frmPOrderEnq.frm $
'**********************************************************************************************
'* Summary: Enquiry facility to enquire of Purchase Orders.  Can be called, passing in
'           parameters to pre-select criteria to force specific enquiries and auto-print enquiry
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/06/04 10:28 $ $Revision: 22 $
'* Versions:
'* 13/11/02    mauricem
'*             Header added.
'* 20/06/03    KeithB
'*         Various modifications requested by John Coles including-
'*         (A) Modify display
'*         (B) At the moment can't get weight without going to line level
'*             Instruction from M.M - hide the column for now.
'*         (C) Add details form
'*         (D) Change date drop downs
'*
'</CAMH>***************************************************************************************
Option Explicit

Const APP_NAME As String = "POEnquiry"
Const MODULE_NAME As String = "frmPOEnquiry"

Dim mlngHideSdColumns(20)  As Long
Dim mstrViewerType         As String
Dim mblnIncParked          As Boolean

Const COL_SD_LINENO    As Long = COL_PO_ONO_RELNO
Const COL_SD_PARTCODE  As Long = COL_PO_SUPPNO
Const COL_SD_DESC      As Long = COL_PO_SUPPNAME
Const COL_SD_SIZE      As Long = COL_PO_ORDDATE
Const COL_SD_ORDERQTY  As Long = COL_PO_DUEDATE
Const COL_SD_DELIVERED As Long = COL_PO_QTY
Const COL_SD_LASTDEL   As Long = COL_PO_WEIGHT
Const COL_SD_SHORTAGE  As Long = COL_PO_COST
Const COL_SD_BACKORD   As Long = COL_PO_STATUS
Const COL_SD_CUSTORDNO As Long = COL_PO_INIT
Const COL_SD_CUSTNAME  As Long = COL_PO_INIT + 1
Const COL_SD_CUSTOQTY  As Long = COL_PO_INIT + 2

'Const CRIT_EQUAL     As Long = 0 <<<<<<<<<<<<<<<<
'Const CRIT_GREATER   As Long = 1
'Const CRIT_LESSTHAN  As Long = 2
'Const CRIT_ALL       As Long = 3
'Const CRIT_FROM      As Long = 4

Private Const CRIT_EQUALS        As Long = 0
Private Const CRIT_GREATERTHAN   As Long = 1
Private Const CRIT_LESSTHAN      As Long = 2
Private Const CRIT_ALL           As Long = 3
Private Const CRIT_FROM          As Long = 4
Private Const CRIT_WEEKENDING    As Long = 5


Const TOTAL_NO_TXT   As String = "Total No :"

Private lngAllDue    As Long
Private lngAllOrder  As Long
Private lngNoHeaders As Long
Private lngAllStatus As Long
Private lngOldColour As Long

Private mstrDestCode    As String
Dim mlngPayAmtDecPlaces As Long
Dim mlngQtyDecPlaces    As Long
Dim mstrValueFormat     As String

Dim mcolUsers  As Collection

Private Sub SwitchToParkedOrderView()

    sprdOrders.Col = COL_PO_ONO_RELNO
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_DUEDATE
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_STATUS
    sprdOrders.ColHidden = True

End Sub


Private Sub cmbDueDateCrit_Click()

    If cmbDueDateCrit.ListIndex = -1 Then Exit Sub
    dtxtEndDueDate.Visible = False
    lblDueTolbl.Visible = False
    If cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex) = CRIT_ALL Then
        dtxtStDueDate.Visible = False
    Else
        dtxtStDueDate.Visible = True
        If cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex) = CRIT_FROM Then
            dtxtEndDueDate.Visible = True
            lblDueTolbl.Visible = True
            If GetDate(dtxtEndDueDate.Text) < GetDate(dtxtStDueDate.Text) Then dtxtEndDueDate.Text = dtxtStDueDate.Text
        End If
    End If
    
End Sub

Private Sub cmbDueDateCrit_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbDueDateCrit_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbGroupBy_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbGroupBy_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbOrderDateCrit_Click()
    
    If cmbOrderDateCrit.ListIndex = -1 Then Exit Sub
    dtxtEndOrderDate.Visible = False
    lblOrderTolbl.Visible = False
    If cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex) = CRIT_ALL Then
        dtxtStOrderDate.Visible = False
    Else
        dtxtStOrderDate.Visible = True
        If cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex) = CRIT_FROM Then
            dtxtEndOrderDate.Visible = True
            lblOrderTolbl.Visible = True
        End If
    End If
    
End Sub

Private Sub cmbOrderDateCrit_GotFocus()

    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbOrderDateCrit_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbStatus_GotFocus()
    
    Call SendKeys(KEY_DROPDOWN)

End Sub

Private Sub cmbStatus_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmbSupplier_CloseUp()
    
    cmdReset.Visible = True
    'if supplier selected then access orders for them
'    If cmbSupplier.Text <> "*ALL*" & vbTab & "*ALL*" & vbTab Then Call cmdApply_Click

End Sub

Private Sub cmbSupplier_GotFocus()

    cmbSupplier.ListDown = True

End Sub

Private Sub cmbSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyDown And cmbSupplier.ListDown = False Then
        cmbSupplier.ListDown = True
    End If
    If (KeyCode = vbKeyReturn) And (cmbSupplier.ListDown = True) Then
        Call cmbSupplier_KeyPress(vbKeyReturn)
    End If

End Sub

Private Sub cmbSupplier_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

'<CACH>****************************************************************************************
'* Sub:  cmdApply_Click()
'**********************************************************************************************
'* Description: This uses the selected criteria to retrieve the data and to display it.
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 10/12/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub cmdApply_Click()

Dim oOrder       As cPurchaseOrder
Dim colMatches   As Collection
Dim strSuppNo    As String
Dim dteDateValue As Date
Dim lngComp      As Long
Dim intPnt       As Integer

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdApply_Click"

On Error GoTo Bad_Apply
            
    lngNoHeaders = 0
    sprdOrders.MaxRows = 0
    sprdOrders.MaxCols = COL_PO_KEY
    
    For intPnt = 1 To sprdOrders.MaxCols
        sprdOrders.Col = intPnt
        sprdOrders.Visible = True
    Next intPnt
    
    
    cmdShowdetails.Visible = False
    cmdDetails.Visible = False

    If dtxtStOrderDate.BackColor = RGB_RED Then 'invalid Order date entered so raise prompt
        Call MsgBox("Invalid Order Date value entered" & vbCrLf & "Clear out value or enter valid date", vbExclamation, "Perform Enquiry")
        dtxtStOrderDate.SetFocus
        Exit Sub
    End If
    If dtxtStDueDate.BackColor = RGB_RED Then 'invalid Order date entered so raise prompt
        Call MsgBox("Invalid Due Date value entered" & vbCrLf & "Clear out value or enter valid date", vbExclamation, "Perform Enquiry")
        dtxtStDueDate.SetFocus
        Exit Sub
    End If
    
    If cmbGroupBy.ListIndex = -1 Then cmbGroupBy.ListIndex = 0
    
    'Perform retrieval
    Screen.MousePointer = vbHourglass
    ucpbProgress(0).Caption1 = "Accessing data"
    ucpbProgress(0).Visible = True
    
    lblNoMatches.Caption = "Searching..."
    
    DoEvents
    If goDatabase Is Nothing Then Call Initialise
    
    ' read database CLASSID_PURCHASEORDER  - PURHDR
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
            
    'ignore all parked orders
    If mblnIncParked = True Then
        Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReleaseNumber, "  ")
    Else
        Call oOrder.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_PURCHASEORDER_ReleaseNumber, "  ")
    
        'Set Purchase Order criteria
        Select Case (cmbStatus.ItemData(cmbStatus.ListIndex))
            Case (STATUS_PO_ALL):
            Case (STATUS_PO_OPEN):
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_DeletedByMaint, False)
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReceivedAll, False)
            Case (STATUS_PO_SHORT):
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_DeletedByMaint, False)
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReceivedPart, True)
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReceivedAll, False)
            Case (STATUS_PO_CANCEL):
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_DeletedByMaint, True)
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReceivedAll, False)
        End Select
        
        'Set Due date criteria
        If cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex) <> CRIT_ALL Then
            dteDateValue = GetDate(dtxtStDueDate.Text)
            If Year(dteDateValue) > 1899 Then
                Select Case (cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex))
                    Case Is = CRIT_LESSTHAN
                        lngComp = CMP_LESSEQUALTHAN
                    Case CRIT_GREATERTHAN, CRIT_FROM
                        lngComp = CMP_GREATEREQUALTHAN
                    Case Else
                        lngComp = CMP_EQUAL
                End Select
                Call oOrder.IBo_AddLoadFilter(lngComp, FID_PURCHASEORDER_DueDate, dteDateValue)
            End If
            If (cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex) = CRIT_FROM) Then
                Call oOrder.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_PURCHASEORDER_DueDate, GetDate(dtxtEndDueDate.Text))
            End If
        End If
        
        'Set Order date criteria
        If cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex) <> CRIT_ALL Then
            dteDateValue = GetDate(dtxtStOrderDate.Text)
            If Year(dteDateValue) > 1899 Then
                Select Case (cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex))
                    Case Is = CRIT_LESSTHAN
                        lngComp = CMP_LESSEQUALTHAN
                    Case CRIT_GREATERTHAN, CRIT_FROM
                        lngComp = CMP_GREATEREQUALTHAN
                    Case Else
                        lngComp = CMP_EQUAL
                End Select
                Call oOrder.IBo_AddLoadFilter(lngComp, FID_PURCHASEORDER_OrderDate, dteDateValue)
            End If
            If (cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex) = CRIT_FROM) Then
                Call oOrder.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_PURCHASEORDER_OrderDate, GetDate(dtxtEndOrderDate.Text))
            End If
        End If
        
        'Set Order Number criteria
        If LenB(txtFilterOrderNo.Text) <> 0 Then
            If InStr(txtFilterOrderNo.Text, "%") > 0 Then
                Call oOrder.IBo_AddLoadFilter(CMP_LIKE, FID_PURCHASEORDER_OrderNumber, txtFilterOrderNo.Text)
            Else
                If Len(txtFilterOrderNo.Text) < DOCNO_LEN Then txtFilterOrderNo.Text = Left$(DOCNO_PAD, DOCNO_LEN - Len(txtFilterOrderNo.Text)) & txtFilterOrderNo.Text
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_OrderNumber, txtFilterOrderNo.Text)
            End If
        End If
            
        'ignore all parked orders
        Call oOrder.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_PURCHASEORDER_ReleaseNumber, "  ")
        
        'Set Supplier criteria
        If cmbSupplier.Text <> "*ALL*" & vbTab & "*ALL*" & vbTab Then
            cmbSupplier.Col = 0
            Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_SupplierNo, cmbSupplier.ColText)
        End If
    End If
    
    'Create list of fields that should be selected from database
    Call SetPOrderBOLoadFields(oOrder)
    
    'Get the data as a collection of Purchase Orders
    Set colMatches = oOrder.IBo_LoadMatches
    
    'Display number of matches found
    lblNoMatches.Caption = colMatches.Count
    ucpbProgress(0).Value = 0
    If colMatches.Count > 0 Then ucpbProgress(0).Max = colMatches.Count
    ucpbProgress(0).Caption1 = "Displaying data"
    DoEvents
    cmbSupplier.SearchMethod = SearchMethodPartialMatch
    cmbSupplier.SearchIgnoreCase = True
    
    'Now that data has been accessed, display on screen
    Call DisplayPOData(colMatches, sprdOrders, cmbSupplier, ucpbProgress(0))
    
    On Error Resume Next
    If sprdOrders.MaxRows > 0 Then
        ' SORT the grid
        Call cmbGroupBy_Click
        sprdOrders.Row = 1
        sprdOrders.Col = COL_PO_ONO
        Debug.Print sprdOrders.Text
        sprdOrders.Col = COL_PO_ONO_RELNO
        Debug.Print sprdOrders.Text
        Call sprdOrders_Click(sprdOrders.Col, sprdOrders.Row)
        If sprdOrders.Visible = True Then sprdOrders.SetFocus
        cmdPrint.Visible = True
        cmdShowdetails.Visible = True
        cmdDetails.Visible = True
    Else
        If sprdOrders.Visible = True Then txtFilterOrderNo.SetFocus
        cmdPrint.Visible = False
    End If
    
    ' Hide columns
    Call HidePOColumns(sprdOrders)
        
    Screen.MousePointer = vbNormal
    
    ucpbProgress(0).Visible = False
    cmdReset.Visible = True
    Exit Sub
    
Bad_Apply:
    
    Screen.MousePointer = vbNormal
    ucpbProgress(0).Visible = False
    lblNoMatches.Caption = "Error..." & Err.Description
    lblNoMatches.ToolTipText = lblNoMatches.Caption
    Exit Sub
    Resume Next

End Sub 'cmdApply_Click

Private Sub cmdApply_GotFocus()
    
    cmdApply.FontBold = True

End Sub

Private Sub cmdApply_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then Call SendKeys("+{tab}")

End Sub

Private Sub cmdApply_LostFocus()
    
    cmdApply.FontBold = False

End Sub

Private Sub cmdDetails_Click()
    
' F11 - show details of one order
' ===============================
    
Dim lngSelectedRow         As Long
Dim lngLastLineTransferred As Long
Dim strOrderNo             As String

    lngSelectedRow = sprdOrders.ActiveRow
    
    If lngSelectedRow = -1 Then
        Call MsgBox("Please select an order by clicking on the grid", vbInformation, "Unable to display details")
        Exit Sub
    End If
    
    sprdOrders.Col = COL_PO_LINETYPE
    sprdOrders.Row = lngSelectedRow
    If Val(sprdOrders.Text) = 0 Then
        Call MsgBox("Summary line selected", vbInformation, "Unable to display details")
        Exit Sub
    End If
    
    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.Row = lngSelectedRow
    
    ' the user may have clicked in the middle of an expanded order so locate header
    If LenB(sprdOrders.TypeButtonText) = 0 Then
        Do
            lngSelectedRow = lngSelectedRow - 1
            sprdOrders.Row = lngSelectedRow
        Loop Until (LenB(sprdOrders.TypeButtonText) <> 0) Or (lngSelectedRow = 1)
    End If
    
    Select Case sprdOrders.TypeButtonText
        Case Is = "Retrieve"
            sprdOrders.Col = COL_PO_KEY
            Call DisplayOrderLines(sprdOrders.Text, True)
            
        'Case Is = "Expand"
         '   Call ShowLineItems
        
        'Case Is = "Shrink"
         '   Call HideLineItems
    End Select
    
    
    sprdOrders.Row = lngSelectedRow
    sprdOrders.Col = COL_PO_ONO
    strOrderNo = sprdOrders.Text
    
    ' load frmPoDetails
    ' then initialise it & copy headings from frmSoViewer
    Call LoadAndPrepareDetailsForm("Enquiry", mlngHideSdColumns(), sprdOrders, ucpbProgress(0))
    
    Call TransferHeaderInfo(lngSelectedRow, sprdOrders)
            
    Call AddOrderLinesSubHeading
    
    Call TransferLinesInfo(lngSelectedRow, False, lngLastLineTransferred, sprdOrders)
    
    Call LockPoDetails
    
    'Sprd details starts at the top row - RWC 02/10/03
    frmPoDetails.sprdDetails.TopRow = 1
    'Unfrezes the top row
    frmPoDetails.sprdDetails.RowsFrozen = 3
    frmPoDetails.sprdDetails.OperationMode = OperationModeRow
    
    frmPoDetails.Caption = "Purchase Order Enquiry - No:" & strOrderNo & " Details"
    frmPoDetails.mstrPrintCriteria = vbNullString
    frmPoDetails.Show vbModal
    
    sprdOrders.Row = lngSelectedRow
    Call HideLineItems
    
    Unload frmPoDetails
     

End Sub

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub cmdExit_GotFocus()

    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub cmdPrint_GotFocus()
    
    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub cmdReset_Click()

    Screen.MousePointer = vbHourglass
    
    txtFilterOrderNo.Text = vbNullString
    cmbSupplier.Text = "*ALL*" & vbTab & "*ALL*" & vbTab
    sprdOrders.MaxRows = 0
    lblNoMatches.Caption = "0"
    
    If (txtFilterOrderNo.Visible = True) And (txtFilterOrderNo.Enabled = True) Then txtFilterOrderNo.SetFocus
    If (cmbDueDateCrit.Enabled = True) Then cmbDueDateCrit.ListIndex = lngAllDue
    If (cmbOrderDateCrit.Enabled = True) Then cmbOrderDateCrit.ListIndex = lngAllOrder
    dtxtStOrderDate.Text = "--/--/--"
    dtxtStDueDate.Text = "--/--/--"
    dtxtEndOrderDate.Text = "--/--/--"
    dtxtEndDueDate.Text = "--/--/--"
    cmdDetails.Visible = False
    cmdReset.Visible = False
    cmdShowdetails.Visible = False
    Screen.MousePointer = vbNormal

End Sub
Private Sub cmdReset_GotFocus()
    
    cmdReset.FontBold = True

End Sub

Private Sub cmdReset_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then Call SendKeys("+{tab}")

End Sub

Private Sub cmdReset_LostFocus()
    
    cmdReset.FontBold = False

End Sub

Private Sub dtxtEndOrderDate_GotFocus()
    
    dtxtEndOrderDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtStDueDate_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtStOrderDate_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtEndDueDate_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub dtxtEndOrderDate_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdShowdetails_Click()

' F4 - Show details of all orders
' == = ==========================

Dim strHeading             As String
Dim lngSelectedRow         As Long
Dim lngLastLineTransferred As Long
Dim lngRow                 As Long
Dim strPrintHeader         As String

    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.ColHidden = False

    If sprdOrders.MaxRows < 1 Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    cmdPrint.Enabled = False

    ucpbProgress(1).Value = 0
    'ucpbProgress(1).Top = 10
    'ucpbProgress(1).Left = 0
    ucpbProgress(1).Max = Val(lblNoMatches.Caption) + 1
    ucpbProgress(1).Visible = True
    ucpbProgress(1).Caption1 = "Processing Purchase Orders"
    Call ucpbProgress(1).Refresh
    
    lngSelectedRow = 0
    strHeading = "Details"
    Call LoadAndPrepareDetailsForm(strHeading, mlngHideSdColumns(), sprdOrders, ucpbProgress(1))
    sprdOrders.Redraw = False
    
    Do
    
        lngSelectedRow = lngSelectedRow + 1
        
        sprdOrders.Row = lngSelectedRow
        sprdOrders.Col = COL_PO_SHOW
        
        If LenB(sprdOrders.TypeButtonText) <> 0 Then
        
            ucpbProgress(1).Value = ucpbProgress(1).Value + 1
            
            Select Case sprdOrders.TypeButtonText
            Case Is = "Retrieve"
                sprdOrders.Col = COL_PO_KEY
                Call DisplayOrderLines(sprdOrders.Text, True)
            
'            Case Is = "Expand"
'                Call ShowLineItems
            
            'Case Is = "Shrink"
             '   Call HideLineItems
            End Select

            sprdOrders.Row = lngSelectedRow
                        
            ' show details is currently selected
            ' ==================================
            Call TransferHeaderInfo(lngSelectedRow, sprdOrders)
            Call AddOrderLinesSubHeading
            Call TransferLinesInfo(lngSelectedRow, True, lngLastLineTransferred, sprdOrders)
            ' we have moved down the grid
            lngSelectedRow = lngLastLineTransferred
            Call DrawLineAcross(frmPoDetails.sprdDetails)
        
        End If
    
    Loop Until lngSelectedRow >= sprdOrders.MaxRows
    
    Call DrawLineAcross(frmPoDetails.sprdDetails)
    
    ' display totals
    ' ==============
    Call IncrementSprdDetailsRow
    
    On Error Resume Next
    ucpbProgress(1).Visible = False
    Call ucpbProgress(1).Refresh
    Call HidePOColumns(sprdOrders) ', mlngOrderColToHide())
    sprdOrders.Redraw = True
    sprdOrders.Refresh
    Call LockPoDetails
    Screen.MousePointer = vbDefault
    
    'Sprd details starts at the top row - RWC 02/10/03
    frmPoDetails.sprdDetails.TopRow = 1
    'Unfrezes the top row
    frmPoDetails.sprdDetails.RowsFrozen = 0
    
    frmPoDetails.Caption = "Purchase Order Enquiry Report (Detailed)"
    
    'Set up Selection Criteria string for printing at head of Reports
    strPrintHeader = "/n/cStatus Report on "
    
    If LenB(txtFilterOrderNo.Text) = 0 Then strPrintHeader = strPrintHeader & " on Order No : " & txtFilterOrderNo.Text
    'Add supplier details
    If cmbSupplier.ColText = "*ALL*" Then
        strPrintHeader = strPrintHeader & "All Suppliers"
    Else
        strPrintHeader = strPrintHeader & Replace$(cmbSupplier.Text, vbTab, "-")
    End If
    strPrintHeader = strPrintHeader & "/nFor status - '" & cmbStatus.Text & "'"
    If mblnIncParked = False Then
        'Add date criteria
        If cmbOrderDateCrit.Enabled = True Then
            strPrintHeader = strPrintHeader & " /nDated "
            Select Case (cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex))
                Case (CRIT_ALL): strPrintHeader = strPrintHeader & ": ALL"
                Case (CRIT_FROM): strPrintHeader = strPrintHeader & "From " & dtxtStOrderDate.Text & " to " & dtxtEndOrderDate.Text
                Case (CRIT_EQUALS): strPrintHeader = strPrintHeader & ": " & dtxtStOrderDate.Text
                Case Else: strPrintHeader = strPrintHeader & cmbOrderDateCrit.Text & ": " & dtxtStOrderDate.Text
            End Select
            strPrintHeader = strPrintHeader & Space$(20) & " Due Dates "
        Else
            strPrintHeader = strPrintHeader & " /nDue Dates "
        End If
        Select Case (cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex))
            Case (CRIT_ALL): strPrintHeader = strPrintHeader & ": ALL"
            Case (CRIT_FROM): strPrintHeader = strPrintHeader & "From " & dtxtStDueDate.Text & " to " & dtxtEndDueDate.Text
            Case (CRIT_EQUALS): strPrintHeader = strPrintHeader & ": " & dtxtStDueDate.Text
            Case Else: strPrintHeader = strPrintHeader & cmbDueDateCrit.Text & ": " & dtxtStDueDate.Text
        End Select
    End If 'not the Parked Orders enquiry - so show date range used
    frmPoDetails.mstrPrintCriteria = strPrintHeader
    
    frmPoDetails.Show vbModal

    
    'closes the details of the orders
    'RWC 01/10/03
    
    For lngRow = 1 To sprdOrders.MaxRows Step 1
        sprdOrders.Row = lngRow
        sprdOrders.Col = COL_PO_SHOW
        
        Call DebugMsg(MODULE_NAME, "cmdShowDetails_click", endlDebug, "Testing-" & sprdOrders.Row)
        If sprdOrders.CellType = CellTypeButton Then
            Call DebugMsg(MODULE_NAME, "cmdShowDetails_click", endlDebug, "FOUND-" & sprdOrders.Row)
            sprdOrders.TypeButtonText = "Shrink" 'force the hiding of lines
            Call sprdOrders_ButtonClicked(COL_PO_SHOW, lngRow, 0)
           
        End If
    Next lngRow
    
    'Hides colums
    sprdOrders.Col = COL_PO_LINENO
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_PARTCODE
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_DESC
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_SIZE
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_ORDERQTY
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_QTYDEL
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_LASTDEL
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_QTYBORD
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_CUSTORDNO
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_CUSTNAME
    sprdOrders.ColHidden = True
    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.ColHidden = True
            
    cmdPrint.Enabled = True
    sprdOrders.SetFocus

End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case Is = vbKeyF3 'if F3 then clear criteria
                If cmdReset.Visible = True Then Call cmdReset_Click
            Case Is = vbKeyF4    ' if F4 show details
                If cmdShowdetails.Visible = True And cmdShowdetails.Enabled = True Then
                    cmdShowdetails_Click
                End If
            Case Is = vbKeyF7   'if F7 then start searching
                Call cmdApply_Click
            Case Is = vbKeyF9   'if F9 then call reprint document
                Call cmdPrint_Click
            Case Is = vbKeyF10  'if F10 then exit
                Call cmdExit_Click
            Case Is = vbKeyF11  'if F10 then exit
                If cmdDetails.Visible = True Then Call cmdDetails_Click
        End Select 'Key pressed with no Shift/Alt combination
    End If

End Sub

Private Sub Form_Load()

Dim lngItemNo As Long

    Call GetRoot
    sprdOrders.MaxRows = 0
    
    Call InitialiseStatusBar(sbStatus)
    Call Me.Show
    Call sprdOrders.SetRefStyle(2) 'critical to make formulas work
    
    mstrViewerType = "Purchase Order"
    ' columns to be hidden in sprdOrders
    
    mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    dtxtStOrderDate.DateFormat = mstrDateFmt
    dtxtStDueDate.DateFormat = mstrDateFmt
        
    ' get display format for pounds & pence from param file
    mlngPayAmtDecPlaces = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    mlngQtyDecPlaces = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    mstrValueFormat = String$(mlngPayAmtDecPlaces, "0")
    If LenB(mstrValueFormat) <> 0 Then
        mstrValueFormat = "0." & mstrValueFormat
    End If

    Call SetUpHidePOColumns
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    cmdExit.BackColor = Me.BackColor
    cmdDetails.BackColor = Me.BackColor
    cmdPrint.BackColor = Me.BackColor
    cmdShowdetails.BackColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    
    Call Initialise
    
    Call SetPOQuantityPlaces(sprdOrders, mlngQtyDecPlaces)

    ' Due Date Combo Box
    ' ==================
    cmbDueDateCrit.Clear
    Call cmbDueDateCrit.AddItem("After")
    cmbDueDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbDueDateCrit.AddItem("Before")
    cmbDueDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbDueDateCrit.AddItem("Dated")
    cmbDueDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbDueDateCrit.AddItem("All")
    cmbDueDateCrit.ItemData(3) = CRIT_ALL
    Call cmbDueDateCrit.AddItem("From")
    cmbDueDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItemNo = 0 To cmbDueDateCrit.ListCount Step 1
        If cmbDueDateCrit.ItemData(lngItemNo) = CRIT_ALL Then
            lngAllDue = lngItemNo
            cmbDueDateCrit.ListIndex = lngItemNo
            Exit For
        End If
    Next lngItemNo
    
    ' Order Date combo box
    ' ====================
    cmbOrderDateCrit.Clear
    Call cmbOrderDateCrit.AddItem("After")
    cmbOrderDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbOrderDateCrit.AddItem("Before")
    cmbOrderDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbOrderDateCrit.AddItem("Dated")
    cmbOrderDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbOrderDateCrit.AddItem("All")
    cmbOrderDateCrit.ItemData(3) = CRIT_ALL
    Call cmbOrderDateCrit.AddItem("From")
    cmbOrderDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItemNo = 0 To cmbOrderDateCrit.ListCount Step 1
        If cmbOrderDateCrit.ItemData(lngItemNo) = CRIT_ALL Then
            lngAllOrder = lngItemNo
            cmbOrderDateCrit.ListIndex = lngItemNo
            Exit For
        End If
    Next lngItemNo
    
    For lngItemNo = 1 To cmbStatus.ListCount Step 1
        If cmbStatus.ItemData(lngItemNo - 1) = 0 Then
            lngAllStatus = lngItemNo - 1
            cmbStatus.ListIndex = lngAllStatus
            Exit For
        End If
    Next lngItemNo
    
    'Set Date Fields to show format
    dtxtStOrderDate.Text = "--/--/--"
    dtxtStDueDate.Text = "--/--/--"
    dtxtEndOrderDate.Text = "--/--/--"
    dtxtEndDueDate.Text = "--/--/--"
    
    'set up printing options for report
    sprdOrders.PrintRowHeaders = False
    sprdOrders.PrintGrid = False
    
    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.ColHidden = True
    
    Call DecodeParameters(goSession.ProgramParams)
    
    If mstrDestCode = DEST_REPORT_CODE_PREVIEW Then
        Call cmdPrint_Click
        End
    End If
    If mstrDestCode = DEST_REPORT_CODE_PRINTER Then
        Call cmdPrint_Click
        End
    End If
    
End Sub

Private Sub DecodeParameters(strCommand As String)

Const ORDER_START_DATE As String = "OS"
Const ORDER_END_DATE   As String = "OD"
Const DUE_START_DATE   As String = "DS"
Const DUE_END_DATE     As String = "DD"
Const VIEW_MODE        As String = "VM"
Const VIEW_MODE_EXPDEL As String = "ED"
Const VIEW_MODE_PARKED As String = "PO"

Dim strTempDate  As String
Dim strSignFound As String
Dim strVMCode    As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim blnSetOpts   As Boolean
Dim lngItem      As Long

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        
        Select Case (Left$(strSection, 2))
            Case (ORDER_START_DATE):
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtStOrderDate.Text = Format$(dteTempDate, mstrDateFmt)
                strTempDate = Mid$(strSection, 3) ' strip off the OS
                strSignFound = Left$(strTempDate, Len(strTempDate) - 8)
                blnSetOpts = True
                cmbOrderDateCrit.ListIndex = SetDateCrit(strSignFound, cmbOrderDateCrit)
            
            Case (ORDER_END_DATE):
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtEndOrderDate.Text = Format$(dteTempDate, mstrDateFmt)
                blnSetOpts = True
            
            Case (DUE_START_DATE):
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtStDueDate.Text = Format$(dteTempDate, mstrDateFmt)
                strTempDate = Mid$(strSection, 3) ' strip off the DS
                strSignFound = Left$(strTempDate, Len(strTempDate) - 8)
                blnSetOpts = True
                cmbDueDateCrit.ListIndex = SetDateCrit(strSignFound, cmbDueDateCrit)
            
            Case (DUE_END_DATE):
                dteTempDate = GetParamDate(Right$(strSection, 8))
                dtxtEndDueDate.Text = Format$(dteTempDate, mstrDateFmt)
                blnSetOpts = True
            
            Case (ORDER_STATUS):
                strTempDate = Mid$(strSection, 3)
                For lngItem = 0 To cmbStatus.ListCount Step 1
                    If Left$(cmbStatus.List(lngItem), 1) = strTempDate Then
                        cmbStatus.ListIndex = lngItem
                        Exit For
                    End If
                Next lngItem
                blnSetOpts = True
            
            Case (GROUP_BY):
                strTempDate = Mid$(strSection, 3)
                For lngItem = 0 To cmbGroupBy.ListCount Step 1
                    If Left$(cmbGroupBy.List(lngItem), 1) = strTempDate Then
                        cmbGroupBy.ListIndex = lngItem
                        Exit For
                    End If
                Next lngItem
                blnSetOpts = True
            
            Case (DEST_CODE):
                strTempDate = Mid$(strSection, 3)
                mstrDestCode = strTempDate
                blnSetOpts = True
            
            Case (DISP_LINES):
                Call HidePOLineItems(sprdOrders)
                
        Case (VIEW_MODE):
                strVMCode = Mid$(strSection, 3)
                Select Case (strVMCode)
                    Case (VIEW_MODE_EXPDEL):
                        txtFilterOrderNo.Enabled = False
                        cmbSupplier.Enabled = False
                        cmbStatus.Enabled = False
                        cmbOrderDateCrit.Enabled = False
                        cmbDueDateCrit.Enabled = False
                        'Changed to allow editing of due date
                        'dtxtStDueDate.Enabled = False
                        
                        mstrViewerType = "Expected Deliveries"
                        blnSetOpts = False
                        
                    Case (VIEW_MODE_PARKED) 'Tested with 'DL0,GRO,VMPO,DCS'
                        Call SwitchToParkedOrderView
                        mblnIncParked = True
                        fraCriteria.Visible = False
                        sprdOrders.Top = fraCriteria.Top
                        Call Form_Resize
                        mstrViewerType = "Parked Orders"
                        blnSetOpts = True
                End Select
                    
        End Select
        
    Next lngSectNo
    
    Me.Caption = mstrViewerType & " Enquiry"
    If blnSetOpts = True Then Call cmdApply_Click

End Sub



Private Sub SelectCriteria(ByVal strCriteria As String)

Dim lngStatus As Long


    Select Case (strCriteria)
        Case ("S"): lngStatus = STATUS_PO_ALL
        Case ("O"): lngStatus = STATUS_PO_OPEN
        Case (STATUS_PO_SHORT):
        Case (STATUS_PO_CANCEL):
    End Select

End Sub

Private Sub Form_Resize()

    If Me.WindowState = vbMinimized Then Exit Sub
    
    If Me.Height < 4000 Then
        Me.Height = 4000
        Exit Sub
    End If
    
    If Me.Width < 5000 Then
        Me.Width = 5000
        Exit Sub
    End If
    sprdOrders.Width = Me.Width - sprdOrders.Left * 3
    sprdOrders.Height = (Me.Height - (sbStatus.Height * 2) - sprdOrders.Top - cmdPrint.Height - 240)
    cmdExit.Top = sprdOrders.Top + sprdOrders.Height + 120
    cmdPrint.Top = cmdExit.Top
    cmdPrint.Left = sprdOrders.Left + sprdOrders.Width - cmdPrint.Width
    cmdShowdetails.Top = cmdExit.Top
    cmdDetails.Top = cmdExit.Top
    
    lblNoMatches.Top = cmdExit.Top
    lblNoMatcheslbl.Top = cmdExit.Top
    
    'Move both progess bars next to each other and centralize
    ucpbProgress(0).Top = (Me.Height - (ucpbProgress(0).Height * 2)) / 2
    ucpbProgress(0).Left = (Me.Width - ucpbProgress(0).Width) / 2
    
    ucpbProgress(1).Left = ucpbProgress(0).Left
    ucpbProgress(1).Top = ucpbProgress(0).Top + ucpbProgress(0).Height
End Sub

Private Sub cmdPrint_Click()

Dim strHeader   As String
Dim strFooter   As String
Dim oPrintStore As cStore

    ' get name and address for current store to put at top of document
    If oPrintStore Is Nothing Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.IBo_Load
    End If
   
    strHeader = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & _
                oPrintStore.AddressLine1 & "  -  " & mstrViewerType & " Report" & "/n/cStatus Report on "
    
    If LenB(txtFilterOrderNo.Text) = 0 Then strHeader = strHeader & " on Order No : " & txtFilterOrderNo.Text
    'Add supplier details
    If cmbSupplier.ColText = "*ALL*" Then
        strHeader = strHeader & "All Suppliers"
    Else
        strHeader = strHeader & Replace$(cmbSupplier.Text, vbTab, "-")
    End If
    strHeader = strHeader & "/nFor status - '" & cmbStatus.Text & "'"
    If mblnIncParked = False Then
        'Add date criteria
        If cmbOrderDateCrit.Enabled = True Then
            strHeader = strHeader & " /nDated "
            Select Case (cmbOrderDateCrit.ItemData(cmbOrderDateCrit.ListIndex))
                Case (CRIT_ALL): strHeader = strHeader & ": ALL"
                Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtStOrderDate.Text & " to " & dtxtEndOrderDate.Text
                Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtStOrderDate.Text
                Case Else: strHeader = strHeader & cmbOrderDateCrit.Text & ": " & dtxtStOrderDate.Text
            End Select
            strHeader = strHeader & Space$(20) & " Due Dates "
        Else
            strHeader = strHeader & " /nDue Dates "
        End If
        Select Case (cmbDueDateCrit.ItemData(cmbDueDateCrit.ListIndex))
            Case (CRIT_ALL): strHeader = strHeader & ": ALL"
            Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtStDueDate.Text & " to " & dtxtEndDueDate.Text
            Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtStDueDate.Text
            Case Else: strHeader = strHeader & cmbDueDateCrit.Text & ": " & dtxtStDueDate.Text
        End Select
    End If 'not the Parked Orders enquiry - so show date range used
    ' /c is Centre text         /fb1 is Font Bold on
    sprdOrders.PrintHeader = "/c/fb1" & strHeader
    
    sprdOrders.PrintJobName = mstrViewerType & " Enquiry (Summary)"
    
    ' Set up Footer - program version no date/time printed
    sprdOrders.PrintFooter = GetFooter
    sprdOrders.PrintSmartPrint = True
    Select Case (mstrDestCode)
        Case (DEST_REPORT_CODE_PRINTER):
                Call sprdOrders.PrintSheet
                End
        Case (DEST_REPORT_CODE_PREVIEW)
                Load frmPreview
                Call frmPreview.SetPreview("View Order Enquiry Preview", sprdOrders)
                frmPreview.chkSmartPrint.Value = 1
                Call frmPreview.Show(vbModal)
                End
        Case Else
                Call sprdOrders.PrintSheet
    End Select
    
    ' clean up
    On Error Resume Next
    Set oPrintStore = Nothing
    Call cmdReset_Click
    
End Sub
'<CACH>****************************************************************************************
'* Sub:  DisplayOrderLines()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DisplayOrderLines(ByVal strOrderKey As String, _
                              Optional ByVal blnHideChanges As Boolean = True)

Dim oPOrder      As cPurchaseOrder
Dim oPart        As cInventory
Dim lngLineNo    As Long
Dim lngTopRow    As Long
Dim lngNextRow   As Long
Dim strOrderNo   As String
Dim strOrderDate As String
Dim strCustONo   As String
Dim strDueDate   As String
Dim strSuppNo    As String
Dim oConsolLines As cPurchaseConsLine 'used to extract the consolidation of Customer Order Lines
Dim oPOLine      As cPurchaseLine 'used to access Line as it is displayed

'Variables to retrieve and display any shortages for Purchase Order
Dim oShortNote   As cReturnNote
Dim oShortLine   As cReturnNoteLine
Dim colShortages As Collection
Dim lngShortLine As Long
Dim lngShortageNo As Long
Dim dblShortQty  As Double

Dim colConsol    As Collection

Dim lngConsolNo  As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayOrderLines"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    sprdOrders.SetFocus
    
    ucpbProgress(0).Visible = True
    Call ucpbProgress(0).Refresh
    
    sprdOrders.MaxCols = COL_PO_CONSLINENO
    
    lngTopRow = sprdOrders.Row 'mark top row for later setting of border lines
    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.TypeButtonText = "Shrink"
    Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
    Call oPOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_Key, strOrderKey)
    oPOrder.GetAllLines = True
    If oPOrder.IBo_Load Then
        ucpbProgress(0).Max = oPOrder.Lines.Count + 1
        ucpbProgress(0).Caption1 = "Retrieving Lines for Order"
        If blnHideChanges = True Then sprdOrders.Redraw = False
        Call ucpbProgress(0).Refresh
        lngNextRow = sprdOrders.Row 'add lines to current position
        'Get data that is sorted upon from header column to copy to sub lines
        sprdOrders.Col = COL_PO_ONO
        strOrderNo = sprdOrders.Text
        sprdOrders.Col = COL_PO_ORDDATEFMT
        strOrderDate = sprdOrders.Text
        sprdOrders.Col = COL_PO_DUEDATEFMT
        strDueDate = sprdOrders.Text
        sprdOrders.Col = COL_PO_SUPPNO
        strSuppNo = sprdOrders.Text
        
        If oPOrder.Lines.Count > 1 Then 'insert space to hold lines
            sprdOrders.MaxRows = sprdOrders.MaxRows + oPOrder.Lines.Count - 1
            Call sprdOrders.InsertRows(lngNextRow + 1, oPOrder.Lines.Count - 1)
        End If
        'Colour in Line Items when there is only 1 line
        sprdOrders.Col = COL_PO_LINENO
        sprdOrders.Col2 = COL_PO_QTYBORD
        sprdOrders.Row = lngNextRow
        sprdOrders.Row2 = lngNextRow + oPOrder.Lines.Count - 1
        sprdOrders.BlockMode = True
        sprdOrders.BackColor = RGB_WHITE
        sprdOrders.BlockMode = False
        
'        Set oConsolLines = oPOrder.Consolidate
        'Check if there is a Shortage Note for Purchase Order
        Set oShortNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
        Call oShortNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_OrigPONumber, oPOrder.OrderNumber)
        Call oShortNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_ShortageNote, True)
        Set colShortages = oShortNote.IBo_LoadMatches
        
        For lngLineNo = 1 To oPOrder.Lines.Count Step 1
            
            ucpbProgress(0).Value = lngLineNo
            sprdOrders.Row = lngNextRow
            
            sprdOrders.Col = COL_PO_LINENO
            Set oPOLine = oPOrder.Lines(CLng(lngLineNo))
            sprdOrders.Text = oPOLine.LineNo
            
            sprdOrders.Col = COL_PO_PARTCODE
            sprdOrders.Text = oPOLine.PartCode
            
            ' get  Size           & Description
            '  &   QuantityAtHand & QuantityOnOrder
            ' from CLASSID_INVENTORY
            ' ===================================================
            
            Set oPart = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
            Call oPart.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oPOLine.PartCode)
            Call oPart.IBo_AddLoadField(FID_INVENTORY_Description)
            Call oPart.IBo_LoadMatches
            
            sprdOrders.Col = COL_PO_DESC
            'sprdOrders.Text = oPOrder.Lines(CLng(lngLineNo)).Description
            sprdOrders.Text = oPart.Description
            
            sprdOrders.Col = COL_PO_SIZE
            'sprdOrders.Text = oPart.SizeDescription
            
            sprdOrders.Col = COL_PO_ORDERQTY
            sprdOrders.Text = oPOLine.OrderQuantity
            sprdOrders.Col = COL_PO_QTYDEL
            sprdOrders.Text = oPOLine.QuantityReceived
            
            sprdOrders.Col = COL_PO_LASTDEL
            If oPOLine.DateLastReceipt = "00:00:00" Then
                sprdOrders.Text = "  -"
            Else
                sprdOrders.Text = DisplayDate(oPOLine.DateLastReceipt, False)
            End If
            
            sprdOrders.Col = COL_PO_QTYBORD
            sprdOrders.Text = oPOLine.QuantityBackordered
            
            'Check if SKU can be found in Shortages Note
            dblShortQty = 0
            If colShortages.Count > 0 Then
                For lngShortageNo = 1 To colShortages.Count Step 1 'through each note
                    colShortages(lngShortageNo).GetLines = True
                    'compare against each line to find matching SKU
                    For lngShortLine = 1 To colShortages(lngShortageNo).Lines.Count Step 1
                        Set oShortLine = colShortages(lngShortageNo).Lines(lngShortLine)
                        If oShortLine.PartCode = oPOLine.PartCode Then
                            dblShortQty = dblShortQty + oShortLine.ReturnQty
                        End If
                    Next lngShortLine
                Next lngShortageNo
                
            End If
            sprdOrders.Col = COL_PO_QTYSHORT
            sprdOrders.Text = dblShortQty
            
            sprdOrders.Col = COL_PO_CUSTORDNO
            'sprdOrders.Text = oPOLine.CustomerOrderNo
            
            'Stock item so mask out Customer Name and Quantity
            sprdOrders.Col = COL_PO_CUSTNAME
            sprdOrders.BackColor = RGB_GREY
            sprdOrders.Col = COL_PO_CUSTQTY
            sprdOrders.BackColor = RGB_GREY
            lngNextRow = lngNextRow + 1
            
        Next lngLineNo
        
        If oPOrder.Lines.Count > 0 Then
            
            sprdOrders.BlockMode = True
            'Set line markers
            sprdOrders.Row = lngTopRow
            sprdOrders.Row2 = lngNextRow - 1
            sprdOrders.Col = COL_PO_LINETYPE
            sprdOrders.Col2 = COL_PO_LINETYPE
            sprdOrders.Text = 1
            sprdOrders.Col = COL_PO_ONO
            sprdOrders.Col2 = COL_PO_ONO
            sprdOrders.Text = strOrderNo
            sprdOrders.Col = COL_PO_SUPPNO
            sprdOrders.Col2 = COL_PO_SUPPNO
            sprdOrders.Text = strSuppNo
            sprdOrders.Col = COL_PO_KEY
            sprdOrders.Col2 = COL_PO_KEY
            sprdOrders.Text = strOrderKey
            sprdOrders.Col = COL_PO_ORDDATEFMT
            sprdOrders.Col2 = COL_PO_ORDDATEFMT
            sprdOrders.Text = strOrderDate
            sprdOrders.Col = COL_PO_DUEDATEFMT
            sprdOrders.Col2 = COL_PO_DUEDATEFMT
            sprdOrders.Text = strDueDate
            
            sprdOrders.Col = COL_PO_LINENO
            sprdOrders.Col2 = COL_PO_KEY
            sprdOrders.Row = lngTopRow
            sprdOrders.Row2 = lngNextRow - 1
            ' show all columns here - but hide specific ones later
'            sprdOrders.ColHidden = False 'Commented out M.Milne 12/10/03
            
            If oPOrder.Lines.Count > 1 Then
                sprdOrders.Col = 1
                sprdOrders.Col2 = COL_PO_SHOW
                sprdOrders.Row = lngTopRow + 1
                sprdOrders.Row2 = lngNextRow - 1
                sprdOrders.CellType = CellTypeStaticText
                sprdOrders.BackColor = RGB_GREY
                sprdOrders.ForeColor = RGB_GREY
            End If
        End If
        sprdOrders.BlockMode = False
    End If
    
    'hide specific columns
    If blnHideChanges = True Then
        Call HidePOColumns(sprdOrders) ', mlngOrderColToHide())
'        sprdOrders.ReDraw = True
'        sprdOrders.Refresh
    End If
    
    
    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report(APP_NAME, PROCEDURE_NAME, Erl)
    Call Err.Clear
    Exit Sub
    Resume Next

Finally:
    'Perform tidy up here
    ucpbProgress(0).Visible = False
    sprdOrders.Enabled = True
    sprdOrders.SetFocus
    Return
    '</CAEH>

End Sub 'DisplayOrderLines

Private Sub sprdOrders_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)

    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.Row = Row
    
    Call DebugMsg(MODULE_NAME, "Hide/Show", endlDebug, sprdOrders.TypeButtonText)
    Select Case (sprdOrders.TypeButtonText)
        Case ("Retrieve"):
            sprdOrders.Col = COL_PO_KEY
            Call DisplayOrderLines(sprdOrders.Text)
        Case ("Expand"):
            Call ShowLineItems
        Case ("Shrink"):
            Call HideLineItems
    End Select

End Sub

Private Sub ShowLineItems()

Dim lngNoLines As Long
Dim lngTopRow  As Long
Dim strKey     As String
Dim lngNoItems As Long

    sprdOrders.Col = COL_PO_KEY
    strKey = sprdOrders.Text
    lngTopRow = sprdOrders.Row
    lngNoItems = 1
    For lngNoLines = sprdOrders.Row + 1 To sprdOrders.MaxRows Step 1
        sprdOrders.Row = lngNoLines
        If sprdOrders.Text <> strKey Then
            Exit For
        Else
            lngNoItems = lngNoItems + 1
        End If
    Next lngNoLines
    sprdOrders.BlockMode = True
    sprdOrders.Col = COL_PO_LINENO
    sprdOrders.Col2 = COL_PO_KEY
    sprdOrders.Row = lngTopRow + 1
    sprdOrders.Row2 = lngTopRow + lngNoItems - 1
    sprdOrders.RowHidden = False
    sprdOrders.Row = lngTopRow
    sprdOrders.BackColor = RGB_WHITE
    sprdOrders.ForeColor = RGB_BLACK
    sprdOrders.BlockMode = False
    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.TypeButtonText = "Shrink"

End Sub

Private Sub HideLineItems()

Dim lNoLines As Long
Dim lTopRow  As Long
Dim strKey   As String
Dim lNoItems As Long

    sprdOrders.Col = COL_PO_KEY
    strKey = sprdOrders.Text
    lTopRow = sprdOrders.Row
    lNoItems = 1
    For lNoLines = sprdOrders.Row + 1 To sprdOrders.MaxRows Step 1
        Call DebugMsg(MODULE_NAME, "HideLineItems", endlDebug, "Checking-" & lNoLines & " at " & lNoItems)
        sprdOrders.Row = lNoLines
        If sprdOrders.Text <> strKey Then
            Exit For
        Else
            lNoItems = lNoItems + 1
        End If
    Next lNoLines
    sprdOrders.BlockMode = True
    sprdOrders.Col = COL_PO_LINENO
    sprdOrders.Col2 = COL_PO_KEY
    sprdOrders.Row = lTopRow + 1
    sprdOrders.Row2 = lTopRow + lNoItems - 1
    sprdOrders.RowHidden = True
    sprdOrders.Row = lTopRow
'    sprdOrders.BackColor = RGB_GREY
'    sprdOrders.ForeColor = RGB_GREY
    sprdOrders.BlockMode = False
    sprdOrders.Col = COL_PO_SHOW
    sprdOrders.TypeButtonText = "Expand"

End Sub
Private Sub dtxtStDueDate_GotFocus()

    dtxtStDueDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtEndDueDate_GotFocus()

    dtxtEndDueDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtStDueDate_LostFocus()
    
    If LenB(dtxtStDueDate.Text) <> 0 Then
        If dtxtStDueDate.Text <> "--/--/--" Then
            If Year(GetDate(dtxtStDueDate.Text)) = 1899 Then
                dtxtStDueDate.BackColor = RGB_RED
            Else
                dtxtStDueDate.BackColor = RGB_WHITE
            End If
        Else
            dtxtStDueDate.BackColor = RGB_RED
        End If
    Else
        dtxtStDueDate.BackColor = RGB_WHITE
    End If

End Sub

Private Sub dtxtEndDueDate_LostFocus()
    
    If LenB(dtxtEndDueDate.Text) <> 0 Then
        If Year(GetDate(dtxtEndDueDate.Text)) = 1899 Then
            dtxtEndDueDate.BackColor = RGB_RED
        Else
            dtxtEndDueDate.BackColor = RGB_WHITE
        End If
    Else
        dtxtEndDueDate.BackColor = RGB_WHITE
    End If

End Sub

Private Sub sprdOrders_Click(ByVal Col As Long, ByVal Row As Long)
    
    On Error GoTo sprdorders_Click_Err
    sprdOrders.Row = Row
    sprdOrders.Col = COL_PO_ONO
    cmdDetails.ToolTipText = "Display details for order no " & sprdOrders.Text
    Exit Sub
    
sprdorders_Click_Err:
    cmdDetails.ToolTipText = "Display details"

End Sub

Private Sub txtFilterOrderNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtFilterOrderNo_GotFocus()

    lngOldColour = txtFilterOrderNo.BackColor
    txtFilterOrderNo.BackColor = RGBEdit_Colour
    txtFilterOrderNo.SelStart = 0
    txtFilterOrderNo.SelLength = Len(txtFilterOrderNo.Text)
    
End Sub


Private Sub txtFilterOrderNo_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If KeyAscii = vbKeyEscape Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Public Function Initialise() As Variant

Dim oSupp      As cSuppliers
Dim oUser      As cUser
Dim oBOCol     As Collection
Dim lngItemNo  As Long
Dim strSuppKey As String

    ' Set up the object and field that we wish to select on
    Set goRoot = GetRoot
    
    If cmbSupplier.Enabled Then
        Call cmbSupplier.AddItem("*ALL*" & vbTab & "*ALL*" & vbTab)
        cmbSupplier.Row = cmbSupplier.NewIndex
        cmbSupplier.Text = cmbSupplier.List(cmbSupplier.Row)
        Set oSupp = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERS)
        
        Call oSupp.GetList
        While Not oSupp.EndOfList
            strSuppKey = Left$(oSupp.EntryString, InStr(oSupp.EntryString, vbTab) - 1)
            strSuppKey = Left$(strSuppKey, 2) & Val(Mid$(strSuppKey, 3))
            Call cmbSupplier.AddItem(oSupp.EntryString & vbTab & strSuppKey)
            Call oSupp.MoveNext
        Wend
        Set oBOCol = Nothing
    End If
    
    Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oUser.IBo_AddLoadField(FID_USER_EmployeeID)
    Call oUser.IBo_AddLoadField(FID_USER_FullName)
    Set oBOCol = oUser.IBo_LoadMatches
    
    Set mcolUsers = New Collection
    For lngItemNo = 1 To oBOCol.Count Step 1
        Dim user As cUser
        Set user = oBOCol(lngItemNo)
        Call mcolUsers.Add(CStr(user.FullName), CStr(user.EmployeeID))
    Next lngItemNo
    
    Set oBOCol = Nothing
    cmdReset.Visible = False

End Function

Private Sub cmbGroupBy_Click()

Const NO_HDR_LINES As Long = 3

Dim lngRowNo    As Long
Dim lngLastHdr  As Long
Dim strOldValue As String
Dim lngHdrPos   As Long

    If cmbGroupBy.ListIndex = -1 Then Exit Sub
    If sprdOrders.MaxRows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    sprdOrders.Redraw = False
    'Delete all group headers first
    If lngNoHeaders > 0 Then
        For lngRowNo = 1 To sprdOrders.MaxRows Step 1
            sprdOrders.Row = lngRowNo
            sprdOrders.Col = COL_PO_LINETYPE
            If Val(sprdOrders.Text) = 0 Then
                Call sprdOrders.DeleteRows(lngRowNo, NO_HDR_LINES)
            
            End If
        Next lngRowNo
        sprdOrders.MaxRows = sprdOrders.MaxRows - lngNoHeaders
    End If
    lngNoHeaders = 0
    Select Case (cmbGroupBy.ItemData(cmbGroupBy.ListIndex))
        Case (0):
            sprdOrders.SortKey(1) = COL_PO_SUPPNO
            sprdOrders.SortKeyOrder(1) = SortKeyOrderAscending
            sprdOrders.SortKey(2) = COL_PO_ONO
            sprdOrders.SortKeyOrder(2) = SortKeyOrderDescending
            sprdOrders.SortKey(3) = COL_PO_LINENO
            sprdOrders.SortKeyOrder(3) = SortKeyOrderAscending
        Case (1):
            sprdOrders.SortKey(1) = COL_PO_ORDDATEFMT
            sprdOrders.SortKeyOrder(1) = SortKeyOrderDescending
            sprdOrders.SortKey(2) = COL_PO_ONO
            sprdOrders.SortKeyOrder(2) = SortKeyOrderDescending
            sprdOrders.SortKey(3) = COL_PO_LINENO
            sprdOrders.SortKeyOrder(3) = SortKeyOrderAscending
        Case (2):
            sprdOrders.SortKey(1) = COL_PO_DUEDATEFMT
            sprdOrders.SortKeyOrder(1) = SortKeyOrderDescending
            sprdOrders.SortKey(2) = COL_PO_ONO
            sprdOrders.SortKeyOrder(2) = SortKeyOrderDescending
            sprdOrders.SortKey(3) = COL_PO_LINENO
            sprdOrders.SortKeyOrder(3) = SortKeyOrderAscending
    End Select

    sprdOrders.SortKey(4) = COL_PO_CONSLINENO
    sprdOrders.SortKeyOrder(4) = SortKeyOrderAscending
    Call sprdOrders.Sort(-1, -1, -1, -1, SortByRow)
    For lngRowNo = sprdOrders.MaxRows To 1 Step -1
        'step through first key and detect changes
        sprdOrders.Col = sprdOrders.SortKey(1)
        sprdOrders.Row = lngRowNo
        If strOldValue <> sprdOrders.Text Then 'new group so display totals for old group
            strOldValue = sprdOrders.Text
            sprdOrders.MaxRows = sprdOrders.MaxRows + NO_HDR_LINES
            Call sprdOrders.InsertRows(lngRowNo + 1, NO_HDR_LINES)
            sprdOrders.RowHeight(lngRowNo + 1) = 1
            sprdOrders.RowHeight(lngRowNo + 3) = 1
            'Mark added lines as group totals lines so they are deleted when new Group selected
            For lngHdrPos = 1 To NO_HDR_LINES Step 1
                sprdOrders.Row = lngRowNo + lngHdrPos
                sprdOrders.Col = COL_PO_LINETYPE
                sprdOrders.Text = 0
                sprdOrders.Col = COL_PO_SHOW
                sprdOrders.CellType = CellTypeStaticText
            Next lngHdrPos

            'Display totals
            sprdOrders.Row = lngRowNo + 2
            sprdOrders.Col = COL_PO_SUPPNAME
            sprdOrders.Text = TOTAL_NO_TXT
            sprdOrders.FontBold = True
            sprdOrders.Col = COL_PO_ORDDATE
            sprdOrders.Text = "Total :"
            sprdOrders.FontBold = True
            'Upon exit - put in calculation of totals as group start and end is known
            If lngLastHdr > 0 Then
                Call GroupTotals(lngLastHdr + NO_HDR_LINES, lngLastHdr, lngRowNo)
                
'                sprdOrders.Col = COL_PO_QTY
'                sprdOrders.Row = lngLastHdr + NO_HDR_LINES
'                sprdOrders.Text = "0"
'                sprdOrders.Formula = "SUM(R[-2]C:R[-" & lngLastHdr - lngRowNo - 1 & "]C)"
'                sprdOrders.FontBold = True
'
'                sprdOrders.Col = COL_PO_COST
'                sprdOrders.Row = lngLastHdr + NO_HDR_LINES
'                sprdOrders.CellType = CellTypeNumber
'                sprdOrders.TypeNumberDecPlaces = mlngPayAmtDecPlaces
'                sprdOrders.Text = "0"
'                sprdOrders.Formula = "SUM(R[-2]C:R[-" & lngLastHdr - lngRowNo - 1 & "]C)"
'                sprdOrders.FontBold = True
'
'                sprdOrders.Col = COL_PO_ORDDATE
'                sprdOrders.CellType = CellTypeNumber
'                sprdOrders.TypeNumberDecPlaces = 0
'                sprdOrders.Text = lngLastHdr - lngRowNo - 2
'                sprdOrders.FontBold = True
            End If
            'Store group starting position
            lngNoHeaders = lngNoHeaders + NO_HDR_LINES
            lngLastHdr = lngRowNo + 2
        End If
    Next lngRowNo
    
    'Upon exit - put in calculation of totals as current line is start of group
    If lngLastHdr > 0 Then
        
        Call GroupTotals(lngLastHdr, lngLastHdr, lngRowNo)
'
'        sprdOrders.Col = COL_PO_QTY
'        sprdOrders.Row = lngLastHdr
'        sprdOrders.Text = "0"
'        sprdOrders.Formula = "SUM(R[-2]C:R[-" & lngLastHdr - lngRowNo - 1 & "]C)"
'        sprdOrders.FontBold = True
'        sprdOrders.Col = COL_PO_ORDDATE
'        sprdOrders.Text = lngLastHdr - lngRowNo - 2
'        sprdOrders.CellType = CellTypeNumber
'        sprdOrders.TypeNumberDecPlaces = 0
'        sprdOrders.FontBold = True

    End If
            
    lngHdrPos = sprdOrders.MaxRows + 1
    sprdOrders.MaxRows = sprdOrders.MaxRows + NO_HDR_LINES + 1
    
    sprdOrders.RowHeight(lngHdrPos) = 5
    sprdOrders.RowHeight(lngHdrPos + 1) = 2
    sprdOrders.RowHeight(lngHdrPos + 3) = 2
            
    'Mark added lines as group totals lines so they are deleted when new Group selected
    For lngHdrPos = lngHdrPos To sprdOrders.MaxRows Step 1
        sprdOrders.Row = lngHdrPos
        sprdOrders.Col = COL_PO_LINETYPE
        sprdOrders.Text = 0
        sprdOrders.Col = COL_PO_SHOW
        sprdOrders.CellType = CellTypeStaticText
    Next lngHdrPos
            
    strOldValue = "SUM("
    For lngHdrPos = 1 To sprdOrders.MaxRows - 4 Step 1
        sprdOrders.Row = lngHdrPos
        sprdOrders.Col = COL_PO_LINETYPE
        If Val(sprdOrders.Text) = 0 Then
            strOldValue = strOldValue & "R" & lngHdrPos + 1 & "C+"
            lngHdrPos = lngHdrPos + 2
        End If
    Next lngHdrPos
    strOldValue = strOldValue & "0)"
    
    'Display totals
    sprdOrders.Row = sprdOrders.MaxRows - 1
    sprdOrders.Col = COL_PO_SUPPNAME
    sprdOrders.Text = "Grand Totals :"
    sprdOrders.FontBold = True
    sprdOrders.Col = COL_PO_QTY
    sprdOrders.Formula = strOldValue
    sprdOrders.FontBold = True
    
    sprdOrders.Col = COL_PO_COST
    sprdOrders.CellType = CellTypeNumber
    sprdOrders.TypeNumberDecPlaces = mlngPayAmtDecPlaces
    sprdOrders.Text = "0"
    sprdOrders.Formula = strOldValue
    sprdOrders.FontBold = True
    
    sprdOrders.Col = COL_PO_DUEDATE
    sprdOrders.Text = "Totals :"
    sprdOrders.FontBold = True
    sprdOrders.Col = COL_PO_ORDDATE
    sprdOrders.CellType = CellTypeNumber
    sprdOrders.TypeNumberDecPlaces = 0
    sprdOrders.Text = "0"
    sprdOrders.Formula = strOldValue
    sprdOrders.FontBold = True
    
    sprdOrders.Col = COL_PO_WEIGHT
    sprdOrders.CellType = CellTypeNumber
    sprdOrders.TypeNumberDecPlaces = 0
    sprdOrders.Text = "0"
    sprdOrders.Formula = strOldValue
    sprdOrders.FontBold = True
    lngNoHeaders = lngNoHeaders + NO_HDR_LINES + 1

    sprdOrders.Redraw = True
    Screen.MousePointer = vbNormal

End Sub

Private Sub txtFilterOrderNo_LostFocus()
    
    txtFilterOrderNo.BackColor = lngOldColour

End Sub

Private Sub dtxtStOrderDate_GotFocus()
    
    dtxtStOrderDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtStOrderDate_LostFocus()

    If LenB(dtxtStOrderDate.Text) <> 0 Then
        If Year(GetDate(dtxtStOrderDate.Text)) = 1899 Then
            dtxtStOrderDate.BackColor = RGB_RED
        Else
            dtxtStOrderDate.BackColor = RGB_WHITE
        End If
    Else
        dtxtStOrderDate.BackColor = RGB_WHITE
    End If

End Sub
Private Sub dtxtEndOrderDate_LostFocus()

    If LenB(dtxtEndOrderDate.Text) <> 0 Then
        If Year(GetDate(dtxtEndOrderDate.Text)) = 1899 Then
            dtxtEndOrderDate.BackColor = RGB_RED
        Else
            dtxtEndOrderDate.BackColor = RGB_WHITE
        End If
    Else
        dtxtEndOrderDate.BackColor = RGB_WHITE
    End If

End Sub

Private Sub GroupTotals(ByVal lRow As Long, ByVal lngLastHdr As Long, ByVal lngRowNo As Long)

Dim lngRelativeRow As Long

    lngRelativeRow = lngLastHdr - lngRowNo - 1

    sprdOrders.Row = lRow
    
    sprdOrders.Col = COL_PO_QTY
    sprdOrders.Text = "0"
    sprdOrders.Formula = "SUM(R[-2]C:R[-" & lngRelativeRow & "]C)"
    sprdOrders.FontBold = True
    
    sprdOrders.Col = COL_PO_WEIGHT
    sprdOrders.Text = "0"
    sprdOrders.Formula = "SUM(R[-2]C:R[-" & lngRelativeRow & "]C)"
    sprdOrders.FontBold = True
    
    sprdOrders.Col = COL_PO_COST
    sprdOrders.CellType = CellTypeNumber
    sprdOrders.TypeNumberDecPlaces = mlngPayAmtDecPlaces
    sprdOrders.Text = "0"
    sprdOrders.Formula = "SUM(R[-2]C:R[-" & lngRelativeRow & "]C)"
    sprdOrders.FontBold = True
    
    ' put count of orders in order date column
    sprdOrders.Col = COL_PO_ORDDATE
    sprdOrders.CellType = CellTypeNumber
    sprdOrders.TypeNumberDecPlaces = 0
    sprdOrders.Text = lngLastHdr - lngRowNo - 2
    sprdOrders.FontBold = True

End Sub

Private Sub DrawLineAcross(ByVal sprdSheet As fpSpread)

    sprdSheet.MaxRows = sprdSheet.MaxRows + 1
    sprdSheet.Row = sprdSheet.MaxRows
    
    sprdSheet.Col = -1
    ' set the height of the row specified to 1 with background colour of black
     sprdSheet.RowHeight(sprdSheet.Row) = 1
    ' sprdSheet.FontSize = 4
    sprdSheet.BackColor = vbBlack
    sprdSheet.ForeColor = vbWhite
    
    'Call sprdSheet.AddCellSpan(COL_TIME, sprdSheet.Row, COL_TDATE, 1)
        
End Sub

Private Function SetDateCrit(ByVal strSign As String, cmbDateRange As ComboBox) As Long

Dim intItemValue As Integer
Dim intItem      As Integer

    SetDateCrit = -1
    
    Select Case UCase$(Trim$(strSign))
    Case Is = ">"
      intItemValue = CRIT_GREATERTHAN
    Case Is = "<"
      intItemValue = CRIT_LESSTHAN
    Case Is = "="
      intItemValue = CRIT_EQUALS
    Case Is = "ALL" ' all
      intItemValue = CRIT_ALL
    Case Is = ">="
      intItemValue = CRIT_FROM
    Case Is = "WE"
      intItemValue = CRIT_WEEKENDING
    Case Else
      Call MsgBox("Invalid sign " & strSign & vbCrLf & _
                  "passed to date selection.", vbInformation, "Error decoding date parameter")
      Exit Function
    End Select
    
    ' look for the value intItemValue in cmbDateRange.ItemData(intItem)
    For intItem = 0 To cmbDateRange.ListCount - 1 Step 1
        If cmbDateRange.ItemData(intItem) = intItemValue Then
            ' found intItemValue set the pointer to the ListIndex
            SetDateCrit = intItem
            Exit For
        End If
    Next intItem
    
End Function


