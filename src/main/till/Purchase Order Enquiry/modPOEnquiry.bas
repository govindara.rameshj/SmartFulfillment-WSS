Attribute VB_Name = "modPOEnquiry"
Option Explicit

Public Const COL_PO_ONO        As Long = 1
Public Const COL_PO_RELNO      As Long = 2
Public Const COL_PO_ONO_RELNO  As Long = 3 ' KVB added 16/06/03
Public Const COL_PO_SUPPNO     As Long = 4
Public Const COL_PO_SUPPNAME   As Long = 5
Public Const COL_PO_ORDDATE    As Long = 6
Public Const COL_PO_DUEDATE    As Long = 7
Public Const COL_PO_CARTON     As Long = 8
Public Const COL_PO_QTY        As Long = 9
Public Const COL_PO_WEIGHT     As Long = 10 ' KVB added 16/06/03
Public Const COL_PO_COST       As Long = 11 ' KVB added 16/06/03
Public Const COL_PO_STATUS     As Long = 12 ' KVB added 17/06/03
Public Const COL_PO_INIT       As Long = 13
Public Const COL_PO_PART       As Long = 14
Public Const COL_PO_FULL       As Long = 15
Public Const COL_PO_DELETE     As Long = 16
Public Const COL_PO_LINETYPE   As Long = 17
Public Const COL_PO_ORDDATEFMT As Long = 18
Public Const COL_PO_DUEDATEFMT As Long = 19
Public Const COL_PO_SHOW       As Long = 20
Public Const COL_PO_LINENO     As Long = 21
Public Const COL_PO_PARTCODE   As Long = 22
Public Const COL_PO_DESC       As Long = 23
Public Const COL_PO_SIZE       As Long = 24
Public Const COL_PO_ORDERQTY   As Long = 25
Public Const COL_PO_QTYDEL     As Long = 26
Public Const COL_PO_LASTDEL    As Long = 27
Public Const COL_PO_QTYSHORT   As Long = 28 'Added 17/10/03 to display Shortages per line
Public Const COL_PO_QTYRET     As Long = 29 'Added 17/10/03 to display Returns per line
Public Const COL_PO_QTYBORD    As Long = 30
Public Const COL_PO_CUSTORDNO  As Long = 31
Public Const COL_PO_CUSTNAME   As Long = 32
Public Const COL_PO_CUSTQTY    As Long = 33 'Added to show breakdown of Customer Order
Public Const COL_PO_KEY        As Long = 34
Public Const COL_PO_CONSLINENO As Long = 35

Public Const COL_SD_LINENO      As Long = COL_PO_ONO_RELNO
Public Const COL_SD_PARTCODE    As Long = COL_PO_SUPPNO
Public Const COL_SD_DESC        As Long = COL_PO_SUPPNAME
Public Const COL_SD_SIZE        As Long = COL_PO_ORDDATE
Public Const COL_SD_ORDERQTY    As Long = COL_PO_DUEDATE
Public Const COL_SD_DELIVERED   As Long = COL_PO_QTY
Public Const COL_SD_LASTDEL     As Long = COL_PO_WEIGHT
Public Const COL_SD_SHORTAGE    As Long = COL_PO_COST
Public Const COL_SD_BACKORD     As Long = COL_PO_STATUS
Public Const COL_SD_CUSTORDNO   As Long = COL_PO_INIT
Public Const COL_SD_CUSTNAME    As Long = COL_PO_INIT + 1
Public Const COL_SD_CUSTOQTY    As Long = COL_PO_INIT + 2

Public Const STATUS_PO_ALL     As Long = 0
Public Const STATUS_PO_OPEN    As Long = 1
Public Const STATUS_PO_SHORT   As Long = 2
Public Const STATUS_PO_CANCEL  As Long = 3

Dim mlngOrderColToHide(20) As Long

Public Sub SetUpHidePOColumns()

    mlngOrderColToHide(1) = COL_PO_ONO
    mlngOrderColToHide(2) = COL_PO_RELNO
'    mlngOrderColToHide(3) = COL_PO_INIT
    mlngOrderColToHide(4) = COL_PO_CARTON
    mlngOrderColToHide(5) = COL_PO_PART
    mlngOrderColToHide(6) = COL_PO_FULL
    mlngOrderColToHide(7) = COL_PO_DELETE
    mlngOrderColToHide(8) = COL_PO_SIZE
    mlngOrderColToHide(9) = COL_PO_LASTDEL
    mlngOrderColToHide(10) = COL_PO_CUSTORDNO
    mlngOrderColToHide(11) = COL_PO_KEY
    mlngOrderColToHide(12) = COL_PO_CONSLINENO
    mlngOrderColToHide(13) = COL_PO_QTYSHORT
    mlngOrderColToHide(14) = COL_PO_QTYRET

End Sub
    
Public Sub HidePOLineItems(ByRef sprdPOData As fpSpread)

    sprdPOData.Col = COL_PO_LINENO
    sprdPOData.ColHidden = True
    sprdPOData.Col = COL_PO_PARTCODE
    sprdPOData.ColHidden = True
    sprdPOData.Col = COL_PO_DESC
    sprdPOData.ColHidden = True
    sprdPOData.Col = COL_PO_ORDERQTY
    sprdPOData.ColHidden = True
    sprdPOData.Col = COL_PO_QTYDEL
    sprdPOData.ColHidden = True
    sprdPOData.Col = COL_PO_QTYBORD
    sprdPOData.ColHidden = True

End Sub

Public Sub SetPOQuantityPlaces(ByRef sprdPOData As fpSpread, ByVal lngQtyDecPlaces As Long)

    sprdPOData.Row = -1
    sprdPOData.Col = COL_PO_QTY
    sprdPOData.TypeNumberDecPlaces = lngQtyDecPlaces
    sprdPOData.Col = COL_PO_ORDERQTY
    sprdPOData.TypeNumberDecPlaces = lngQtyDecPlaces
    sprdPOData.Col = COL_PO_QTYDEL
    sprdPOData.TypeNumberDecPlaces = lngQtyDecPlaces
    sprdPOData.Col = COL_PO_QTYBORD
    sprdPOData.TypeNumberDecPlaces = lngQtyDecPlaces
    sprdPOData.Col = COL_PO_CUSTQTY
    sprdPOData.TypeNumberDecPlaces = lngQtyDecPlaces
    sprdPOData.Col = COL_PO_QTYSHORT
    sprdPOData.TypeNumberDecPlaces = lngQtyDecPlaces
    
End Sub


Public Sub DisplayPOData(ByRef colPOrders As Collection, ByRef sprdPOData As fpSpread, ByRef fpcmbSuppliers As fpCombo, Optional ByRef ucpbProgress As ucpbProgressBar = Nothing)

Dim lngMatchNo  As Long
Dim strSuppNo   As String
Dim strSuppName As String
Dim strStatus   As String
Dim strUserName As String
Dim lngPayAmtDecPlaces As Long

    lngPayAmtDecPlaces = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    
    'Loop through Purchase Orders and display details in spreadsheet
    For lngMatchNo = 1 To colPOrders.Count Step 1
    
        If ((ucpbProgress Is Nothing) = False) Then ucpbProgress.Value = lngMatchNo
        sprdPOData.MaxRows = sprdPOData.MaxRows + 1
        sprdPOData.Row = sprdPOData.MaxRows
        sprdPOData.Col = COL_PO_ONO
        sprdPOData.Text = colPOrders(lngMatchNo).OrderNumber
        sprdPOData.Col = COL_PO_RELNO
        sprdPOData.Text = colPOrders(lngMatchNo).ReleaseNumber
        ' column order no / rel no added 16.06/03
        sprdPOData.Col = COL_PO_ONO_RELNO
        sprdPOData.Text = colPOrders(lngMatchNo).OrderNumber & "\" & colPOrders(lngMatchNo).ReleaseNumber
        
        sprdPOData.Col = COL_PO_SUPPNO
        sprdPOData.Text = colPOrders(lngMatchNo).SupplierNo
        fpcmbSuppliers.Col = 0
        'Display supplier name using Supplier number to look up in selection combo
        If (strSuppNo <> sprdPOData.Text) Then
            fpcmbSuppliers.SearchText = sprdPOData.Text
            fpcmbSuppliers.Action = ActionSearch
            fpcmbSuppliers.Col = 1
            sprdPOData.Col = COL_PO_SUPPNAME
            If fpcmbSuppliers.SearchIndex >= 0 Then
                strSuppName = fpcmbSuppliers.List(fpcmbSuppliers.SearchIndex)
                strSuppNo = Left$(strSuppName, InStr(strSuppName, vbTab) - 1)
                strSuppName = Mid$(strSuppName, InStr(strSuppName, vbTab) + 1)
                strSuppName = Left$(strSuppName, InStr(strSuppName, vbTab) - 1)
                sprdPOData.Text = strSuppName
            End If
        Else
            sprdPOData.Col = COL_PO_SUPPNAME
            sprdPOData.Text = strSuppName
        End If
        
        ' put the 'Value' into the Cost
        sprdPOData.Col = COL_PO_COST
        'sprdPOData.Text = Format$(colPOrders(lngMatchNo).Value, mstrValueFormat)
        sprdPOData.Text = colPOrders(lngMatchNo).Value
        sprdPOData.CellType = CellTypeNumber
        sprdPOData.TypeNumberDecPlaces = lngPayAmtDecPlaces

        ' sprdPOData.TypeHAlign = TypeHAlignRight
        
        sprdPOData.Col = COL_PO_ORDDATE
        sprdPOData.Text = DisplayDate(colPOrders(lngMatchNo).OrderDate, False)
        sprdPOData.Col = COL_PO_DUEDATE
        sprdPOData.Text = DisplayDate(colPOrders(lngMatchNo).DueDate, False)
        sprdPOData.Col = COL_PO_INIT
        sprdPOData.Text = colPOrders(lngMatchNo).RaisedBy
        sprdPOData.Col = COL_PO_CARTON
        sprdPOData.Text = colPOrders(lngMatchNo).NoOfCartons
        sprdPOData.Col = COL_PO_QTY
        sprdPOData.Text = colPOrders(lngMatchNo).QuantityOnOrder
'        sprdPOData.Col = COL_PO_QTYSHORT
'        sprdPOData.Text = colPOrders(lngMatchNo).ShortageQty
        sprdPOData.Col = COL_PO_WEIGHT
        sprdPOData.Text = colPOrders(lngMatchNo).Weight
        
        strStatus = "Due (" & DisplayDate(colPOrders(lngMatchNo).DueDate, False) & ")"
        sprdPOData.Col = COL_PO_PART
        If colPOrders(lngMatchNo).ReceivedPart Then
            sprdPOData.Text = "Yes"
            strStatus = "Part In"
        Else
            sprdPOData.Text = "No"
        End If
        sprdPOData.Col = COL_PO_FULL
        If colPOrders(lngMatchNo).ReceivedAll Then
            sprdPOData.Text = "Yes"
            strStatus = "Comp (" & DisplayDate(colPOrders(lngMatchNo).CompletedDate, False) & ")"
        Else
            sprdPOData.Text = "No"
        End If
        sprdPOData.Col = COL_PO_DELETE
        If colPOrders(lngMatchNo).DeletedByMaint Then
            sprdPOData.Text = "Yes"
            strUserName = Format$(Val(colPOrders(lngMatchNo).CancelledBy), "000")
            strUserName = GetFullName(strUserName)
            strStatus = "Del (" & strUserName & "-" & DisplayDate(colPOrders(lngMatchNo).CompletedDate, False) & ")"
        Else
            sprdPOData.Text = "No"
        End If
        
        ' column added 17/06/03
        sprdPOData.Col = COL_PO_STATUS
        sprdPOData.Text = strStatus
        
        'Set additional columns used as part of the Grouping System
        sprdPOData.Col = COL_PO_LINETYPE
        sprdPOData.Text = 1
        sprdPOData.Col = COL_PO_ORDDATEFMT
        sprdPOData.Text = Format$(colPOrders(lngMatchNo).OrderDate, "YYYYMMDD")
        sprdPOData.Col = COL_PO_DUEDATEFMT
        sprdPOData.Text = Format$(colPOrders(lngMatchNo).DueDate, "YYYYMMDD")
        sprdPOData.Col = COL_PO_KEY
        sprdPOData.Text = colPOrders(lngMatchNo).Key
        
    Next lngMatchNo

End Sub

Public Sub HidePOColumns(ByRef sprdControl As fpSpread)

Dim intPnt As Integer

    sprdControl.Row = -1
    
    For intPnt = 1 To UBound(mlngOrderColToHide) Step 1
        If mlngOrderColToHide(intPnt) > 0 Then
            sprdControl.Col = mlngOrderColToHide(intPnt)
            sprdControl.ColHidden = True
        End If
    Next intPnt
    
End Sub

Public Sub SetPOrderBOLoadFields(ByRef oItemBo As cPurchaseOrder)

    Call oItemBo.AddLoadField(FID_PURCHASEORDER_Key)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_OrderNumber)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_ReleaseNumber)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_SupplierNo)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_OrderDate)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_DueDate)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_RaisedBy)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_NoOfCartons)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_QuantityOnOrder)
'    Call oItemBo.AddLoadField(FID_PURCHASEORDER_ShortageQty)
'    Call oItemBo.AddLoadField(FID_PURCHASEORDER_ReturnQty)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_ReceivedPart)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_ReceivedAll)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_DeletedByMaint)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_Key)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_Weight)
    ' 16/06/03 KVB cost added - this is FID_PURCHASEORDER_Value
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_Value)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_CompletedDate)
    Call oItemBo.AddLoadField(FID_PURCHASEORDER_CancelledBy)
    
End Sub

Public Sub DrawLineAcross(ByVal sprdSheet As fpSpread)

    sprdSheet.MaxRows = sprdSheet.MaxRows + 1
    sprdSheet.Row = sprdSheet.MaxRows
    
    sprdSheet.Col = -1
    ' set the height of the row specified to 1 with background colour of black
     sprdSheet.RowHeight(sprdSheet.Row) = 1
    sprdSheet.BackColor = vbBlack
    sprdSheet.ForeColor = vbWhite

End Sub

Public Sub TransferToSprdDetail(ByVal intColFrom As Integer, _
                                 ByVal lngRowFrom As Long, _
                                 ByVal intColTo As Integer, _
                                 ByVal lngRowTo As Long, _
                                 ByVal blnSetRowHeight As Boolean, _
                                 ByRef sprdData As fpSpread)

        sprdData.Col = intColFrom
        sprdData.Row = lngRowFrom
        
        With frmPoDetails.sprdDetails
        
            .Col = intColTo
            .Row = lngRowTo
            
            .CellType = sprdData.CellType
            .ForeColor = sprdData.ForeColor
            .BackColor = sprdData.BackColor
            .TypeHAlign = sprdData.TypeHAlign
            .FontStrikethru = sprdData.FontStrikethru
            
            If blnSetRowHeight = True Then .RowHeight(lngRowTo) = sprdData.RowHeight(lngRowFrom)
            
            .Value = sprdData.Value
            
            .TypeNumberDecPlaces = sprdData.TypeNumberDecPlaces
        End With

End Sub

Public Sub IncrementSprdDetailsRow(Optional intNoOfRows As Integer = 1)
    
    With frmPoDetails.sprdDetails
        .MaxRows = .MaxRows + intNoOfRows
        .Row = .MaxRows
    End With
    
End Sub

Public Sub LockPoDetails()

    ' Lock block of cells
    
    With frmPoDetails.sprdDetails
        ' Specify the block of cells
        .Col = 1
        .Col2 = .MaxCols
        .Row = 1
        .Row2 = .MaxRows
        .BlockMode = True
        .Lock = True ' Lock cells
    
        .Protect = True ' Protect the cells from being edited
        ' Turn block mode off
        .BlockMode = False
        
    End With

End Sub

Public Sub TransferLinesInfo(ByVal lngSelectedRow As Long, ByVal blnShowTotals As Boolean, _
                              ByRef lngLastLineTransferred As Long, ByRef sprdData As fpSpread)

Dim strOrderKey As String
Dim lngLineNo   As Long

    sprdData.Col = COL_PO_KEY
    sprdData.Row = lngSelectedRow
    
    lngLastLineTransferred = lngSelectedRow
    
    strOrderKey = sprdData.Text
    
    For lngLineNo = lngSelectedRow To sprdData.MaxRows Step 1
    
        sprdData.Row = lngLineNo
        sprdData.Col = COL_PO_KEY
        
        Debug.Print sprdData.Text, strOrderKey
        If sprdData.Text <> strOrderKey Then
            ' there are narrow lines, total lines etc with no key
            ' so keep going until we hit a key
            If LenB(sprdData.Text) <> 0 Then
                Exit Sub
            End If
            
            lngLastLineTransferred = lngLineNo
            If blnShowTotals = True Then
                Call IncrementSprdDetailsRow
                ' Transferring Group Totals Line
                Call TransferToSprdDetail(COL_PO_SUPPNAME, lngLineNo, COL_SD_DESC, frmPoDetails.sprdDetails.MaxRows, True, sprdData)
                Call TransferToSprdDetail(COL_PO_DUEDATE, lngLineNo, COL_SD_SIZE, frmPoDetails.sprdDetails.MaxRows, True, sprdData)
                Call TransferToSprdDetail(COL_PO_QTY, lngLineNo, COL_SD_DELIVERED, frmPoDetails.sprdDetails.MaxRows, True, sprdData)
                Call TransferToSprdDetail(COL_PO_COST, lngLineNo, COL_SD_SHORTAGE, frmPoDetails.sprdDetails.MaxRows, True, sprdData)
                Call TransferToSprdDetail(COL_PO_ORDDATE, lngLineNo, COL_SD_SIZE, frmPoDetails.sprdDetails.MaxRows, True, sprdData)
                Call TransferToSprdDetail(COL_PO_WEIGHT, lngLineNo, COL_SD_LASTDEL, frmPoDetails.sprdDetails.MaxRows, True, sprdData)
            End If
        Else
            ' We have a PO line
            ' ==================
            ' so transfer the line to the grid
            lngLastLineTransferred = lngLineNo
            Call IncrementSprdDetailsRow
            
            Call TransferToSprdDetail(COL_PO_LINENO, lngLineNo, COL_SD_LINENO, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_PARTCODE, lngLineNo, COL_SD_PARTCODE, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_DESC, lngLineNo, COL_SD_DESC, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_SIZE, lngLineNo, COL_SD_SIZE, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_ORDERQTY, lngLineNo, COL_SD_ORDERQTY, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_QTYDEL, lngLineNo, COL_SD_DELIVERED, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_QTYSHORT, lngLineNo, COL_SD_SHORTAGE, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_LASTDEL, lngLineNo, COL_SD_LASTDEL, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_QTYBORD, lngLineNo, COL_SD_BACKORD, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_CUSTORDNO, lngLineNo, COL_SD_CUSTORDNO, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_CUSTNAME, lngLineNo, COL_SD_CUSTNAME, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
            Call TransferToSprdDetail(COL_PO_CUSTQTY, lngLineNo, COL_SD_CUSTOQTY, frmPoDetails.sprdDetails.MaxRows, False, sprdData)
        
        End If
            
    Next lngLineNo
    
End Sub

Public Sub AddOrderLinesSubHeading()

    ' heading for order lines
    Call IncrementSprdDetailsRow(1)
    Call IncrementSprdDetailsRow(1)
        
    With frmPoDetails.sprdDetails
    
        If .MaxCols < COL_SD_CUSTORDNO Then
            .MaxCols = COL_SD_CUSTORDNO
        End If
        
        .Col = COL_SD_LINENO
        .Text = "Line No"
        .Col = COL_SD_PARTCODE
        .Text = "SKU"
        .Col = COL_SD_DESC
        .Text = "Description"
        .Col = COL_SD_SIZE
        .Text = "Size"
        .Col = COL_SD_ORDERQTY
        .Text = "Order Qty"
        .Col = COL_SD_DELIVERED
        .Text = "Delivered"
        .Col = COL_SD_DELIVERED
        .Text = "Delivered"
        .Col = COL_SD_LASTDEL
        .Text = "Last Deliver"
        .Col = COL_SD_SHORTAGE
        .Text = "Shortage"
        .Col = COL_SD_BACKORD
        .Text = "Back Order"
        .Col = COL_SD_CUSTORDNO
        .Text = "Cust Ord No"
        .Col = COL_SD_CUSTNAME
        .Text = "Cust Name"
        .Col = COL_SD_CUSTOQTY
        .Text = "Cust Ord Qty"
        
    End With
    
End Sub

Public Sub TransferHeaderInfo(ByVal lngSelectedRow As Long, ByRef sprdData As fpSpread)

Dim intCol As Integer

    Call IncrementSprdDetailsRow
    
    ' copy the line from frmPoEnquiry.sprdData to frmPoDetails.sprdDetails
    With frmPoDetails
    
        sprdData.Row = lngSelectedRow
        ' copy headings
        For intCol = 1 To COL_PO_INIT Step 1
            
            sprdData.Row = lngSelectedRow
            .sprdDetails.Row = .sprdDetails.MaxRows
            sprdData.Col = intCol
            .sprdDetails.Col = intCol
   
            .sprdDetails.CellType = sprdData.CellType
            .sprdDetails.ForeColor = sprdData.ForeColor
            .sprdDetails.BackColor = sprdData.BackColor
            .sprdDetails.TypeHAlign = sprdData.TypeHAlign
            .sprdDetails.FontStrikethru = sprdData.FontStrikethru
            If sprdData.CellType = CellTypeNumber Then .sprdDetails.TypeNumberDecPlaces = sprdData.TypeNumberDecPlaces
            
        Next intCol
        
    End With ' end of frmPoDetails
    
    Call sprdData.SetSelection(1, lngSelectedRow, COL_PO_INIT, lngSelectedRow)
    Call sprdData.ClipboardCopy
'    frmPoDetails.sprdDetails.Col = 1
'    frmPoDetails.sprdDetails.Row = frmPoDetails.sprdDetails.MaxRows
    Call frmPoDetails.sprdDetails.SetActiveCell(1, frmPoDetails.sprdDetails.MaxRows)
    Call frmPoDetails.sprdDetails.ClipboardPaste
    frmPoDetails.sprdDetails.Col = 14
    frmPoDetails.sprdDetails.Text = vbNullString
    frmPoDetails.sprdDetails.Col = 15
    frmPoDetails.sprdDetails.Text = vbNullString

End Sub

Public Sub LoadAndPrepareDetailsForm(ByVal strHeading As String, _
                                      ByRef lngColumnsToHide() As Long, _
                                      ByRef sprdData As fpSpread, _
                                      ByRef ucpbProgress As ucpbProgressBar)
Dim intCol         As Integer

    Load frmPoDetails
    
    With frmPoDetails
    
        .BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
        .ucProgress.FillColor = ucpbProgress.FillColor
        .sprdDetails.MaxRows = 0
        '.sprdDetails.MaxCols = COL_PO_STATUS
        .sprdDetails.MaxCols = COL_PO_FULL
        .Caption = "Purchase Order Viewer"
        If LenB(strHeading) <> 0 Then
            .Caption = .Caption & " - " & strHeading
        End If
        
        ' hide unwanted columns
'        Call HideColumns(.sprdDetails, lngColumnsToHide())
                    
        ' copy headings
        For intCol = 1 To COL_PO_INIT Step 1
            
            sprdData.Row = 0
            sprdData.Col = intCol
            
            .sprdDetails.Row = 0
            .sprdDetails.Col = intCol
            
            ' set the column width & copy the header text
            .sprdDetails.ColWidth(intCol) = sprdData.ColWidth(intCol)
            .sprdDetails.Text = sprdData.Text
            
'            ' copy the text, setting cell type, background color etc
'
'            sprdData.Row = lngSelectedRow
'            .sprdDetails.Row = .sprdDetails.MaxRows
'
'            .sprdDetails.CellType = sprdData.CellType
'            .sprdDetails.ForeColor = sprdData.ForeColor
'            .sprdDetails.BackColor = sprdData.BackColor
'            .sprdDetails.TypeHAlign = sprdData.TypeHAlign
'            .sprdDetails.FontStrikethru = sprdData.FontStrikethru
'            If sprdData.CellType = CellTypeNumber Then .sprdDetails.TypeNumberDecPlaces = sprdData.TypeNumberDecPlaces
            
        Next intCol
        
        'clear out extra customer order headers
        .sprdDetails.Row = 0
        .sprdDetails.Col = COL_SD_CUSTNAME
        .sprdDetails.Text = " "
        .sprdDetails.Col = COL_SD_CUSTOQTY
        .sprdDetails.Text = " "
            
        ' set the column width
        .sprdDetails.ColWidth(COL_SD_CUSTNAME) = 20
        .sprdDetails.ColWidth(COL_SD_CUSTOQTY) = 8
 
    End With ' end of frmPoDetails

End Sub
