VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmPoDetails 
   Caption         =   "Purchase Order Details"
   ClientHeight    =   5115
   ClientLeft      =   1155
   ClientTop       =   2460
   ClientWidth     =   10095
   Icon            =   "frmPODetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5115
   ScaleWidth      =   10095
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdEnquiry 
      Caption         =   "F11-SKU Enquiry"
      Height          =   375
      Left            =   1260
      TabIndex        =   5
      Top             =   4260
      Width           =   1575
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1815
      Left            =   2520
      TabIndex        =   4
      Top             =   1650
      Visible         =   0   'False
      Width           =   5055
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      FillColor       =   16744703
      Object.Width           =   5055
      Object.Height          =   1815
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "F12-Close"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   4260
      Width           =   975
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   9000
      TabIndex        =   1
      Top             =   4260
      Width           =   975
   End
   Begin FPSpreadADO.fpSpread sprdDetails 
      Height          =   3975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9855
      _Version        =   458752
      _ExtentX        =   17383
      _ExtentY        =   7011
      _StockProps     =   64
      ColsFrozen      =   2
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   5
      MaxRows         =   1
      RowHeaderDisplay=   0
      RowsFrozen      =   1
      SpreadDesigner  =   "frmPODetails.frx":058A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   4740
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmPODetails.frx":0847
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9975
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPoDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME           As String = "frmPODetails"
Const BORDER_WIDTH          As Long = 60
Const COL_PARTCODE          As Long = 4
Public mstrPrintCriteria    As String

Private Sub cmdClose_Click()
    Unload Me

End Sub

Private Sub cmdEnquiry_Click()
Dim strPartCode   As String
Dim strEnquiryEXE As String
    
    On Error GoTo Bad_Enquiry
    
    sprdDetails.Col = COL_PARTCODE
    sprdDetails.Row = sprdDetails.ActiveRow
    strPartCode = sprdDetails.Text
    If Val(strPartCode) = 0 Then
        Call MsgBoxEx("No SKU selected for enquiry" & vbCrLf & "Move selection bar to required line", vbExclamation, "Call SKU Enquiry", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    
    End If
    
    strEnquiryEXE = goSession.GetParameter(PRM_ENQUIRY_EXE)
    strEnquiryEXE = strEnquiryEXE & " " & goSession.CreateCommandLine("SKU=" & strPartCode)
    Call DebugMsg(MODULE_NAME, "callMenuOption", endlDebug, strEnquiryEXE)
    Call ShellWait(strEnquiryEXE, SW_MAX)
    Exit Sub
    
    If strPartCode = 0 Then
        Call MsgBoxEx("Access to Item Enquiry failed", vbExclamation, "Call SKU Enquiry", , , , , RGBMsgBox_WarnColour)
        Exit Sub
        
    End If
    
    Exit Sub
    
Bad_Enquiry:

    Call MsgBox("Invalid set-up for item enquiry" & vbCrLf & "  " & strEnquiryEXE & vbCrLf & Err.Number & "-" & Err.Description, vbExclamation, "Select Option")
    Call Err.Clear
    Resume Next
    
End Sub

Private Sub cmdPrint_Click()
Dim oPrintStore As cStore
Dim strHeader   As String

    ' get name and address for current store to put at top of document
    Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
    Call oPrintStore.AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
    Call oPrintStore.Load
   
    
    strHeader = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & _
                oPrintStore.AddressLine1 & "  -  " & Me.Caption & mstrPrintCriteria

    Screen.MousePointer = vbHourglass
    cmdPrint.Enabled = False
    
    sprdDetails.PrintSmartPrint = True
    sprdDetails.PrintFooter = GetFooter
    sprdDetails.PrintHeader = "/c/fb1" & strHeader
    sprdDetails.PrintJobName = "Purchase Order List(Detailed)"
    Call sprdDetails.PrintSheet
    cmdPrint.Enabled = True
    Screen.MousePointer = vbDefault
    cmdClose.Value = True
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case Is = vbKeyF9
                    If cmdPrint.Visible Then cmdPrint.Value = True
                    
                Case Is = vbKeyF10
                    KeyCode = 0
                    
                Case Is = vbKeyF11
                    KeyCode = 0
                    cmdEnquiry.Value = True
                    
                Case Is = vbKeyF12
                    cmdClose.Value = True
                    
            End Select
    End Select

End Sub

Private Sub Form_Load()
    Call InitialiseStatusBar(sbStatus)

End Sub

Private Sub Form_Resize()
Dim lngSpacing As Long
Dim lngMin     As Long
 
    If Me.WindowState = vbMinimized Then Exit Sub
    lngSpacing = sprdDetails.Left
    
    'Check form is not below minimum width
    lngMin = cmdPrint.Width + cmdClose.Width + (lngSpacing * 4)
    If Me.Width < lngMin Then
        Me.Width = lngMin
        Exit Sub
        
    End If
    'Check form is not below minimum height
    lngMin = 3000
    If Me.Height < lngMin Then
        Me.Height = lngMin
        Exit Sub
        
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    'start resizing
    sprdDetails.Width = Me.Width - sprdDetails.Left * 2 - BORDER_WIDTH * 2
    sprdDetails.Height = Me.Height - (cmdClose.Height + sprdDetails.Top + (lngSpacing * 3) + (BORDER_WIDTH * 4) + sbStatus.Height)
    cmdClose.Top = sprdDetails.Top + sprdDetails.Height + lngSpacing
    cmdPrint.Top = cmdClose.Top
    cmdEnquiry.Top = cmdClose.Top
    
    'lblNoLineslbl.Top = cmdClose.Top + lngSpacing / 2
    'lblNoLines.Top = lblNoLineslbl.Top
    
    cmdPrint.Left = (sprdDetails.Width - cmdPrint.Width) + sprdDetails.Left

End Sub

