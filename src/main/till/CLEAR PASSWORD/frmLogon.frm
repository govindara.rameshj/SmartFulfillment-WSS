VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmClearPwd 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Clear user password"
   ClientHeight    =   2325
   ClientLeft      =   1590
   ClientTop       =   1545
   ClientWidth     =   6585
   Icon            =   "frmLogon.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2325
   ScaleWidth      =   6585
   Tag             =   "116"
   Begin VB.TextBox txtAuthPwd 
      Height          =   285
      Left            =   3840
      MaxLength       =   5
      TabIndex        =   8
      Top             =   1320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "F10-Cancel"
      Height          =   375
      Left            =   5040
      TabIndex        =   11
      Tag             =   "114"
      Top             =   1440
      Width           =   1215
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "F5-Clear"
      Height          =   375
      Left            =   5040
      TabIndex        =   10
      Tag             =   "113"
      Top             =   960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtAuthID 
      Enabled         =   0   'False
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1680
      PasswordChar    =   "*"
      TabIndex        =   6
      Top             =   1320
      Width           =   735
   End
   Begin VB.TextBox txtUserID 
      Height          =   285
      Left            =   1680
      TabIndex        =   1
      Top             =   360
      Width           =   735
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   9
      Top             =   1950
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmLogon.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4260
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:11"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblAuthPwd 
      BackStyle       =   0  'Transparent
      Caption         =   "Auth Code"
      Height          =   255
      Left            =   2880
      TabIndex        =   7
      Tag             =   "115"
      Top             =   1320
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblFullName 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2520
      TabIndex        =   4
      Top             =   840
      Width           =   2175
   End
   Begin VB.Label lblInitials 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1680
      TabIndex        =   3
      Top             =   840
      Width           =   735
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Authorisation ID"
      Height          =   255
      Left            =   480
      TabIndex        =   5
      Tag             =   "112"
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Tag             =   "111"
      Top             =   840
      Width           =   855
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Clear User ID"
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Tag             =   "110"
      Top             =   360
      Width           =   1095
   End
End
Attribute VB_Name = "frmClearPwd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmClearPwd
'* Date   : 26/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Clear Password/frmLogon.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 9/04/03 8:44 $ $Revision: 2 $
'* Versions:
'* 26/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmClearPwd"

Dim moUser      As Object
Dim moAuthUser  As Object
Dim sFailedMsg  As String

Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub cmdCancel_GotFocus()
    
    cmdCancel.FontBold = True

End Sub

Private Sub cmdCancel_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub cmdCancel_LostFocus()
    
    cmdCancel.FontBold = False

End Sub

Private Sub cmdClear_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdLogon_Click"

    Screen.MousePointer = vbHourglass
    If (Not moUser Is Nothing) And (LenB(lblInitials.Caption) <> 0) Then
        If txtAuthPwd.Text = moAuthUser.Password Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "User password clear")
            If moUser.SetPassword(vbNullString) Then
                cmdClear.Visible = False
                txtUserID.SetFocus
                DoEvents
                txtUserID.Text = vbNullString
                lblInitials.Caption = vbNullString
                lblFullName.Caption = vbNullString
                txtAuthPwd.Text = vbNullString
                txtAuthID.Text = vbNullString
                txtAuthID.Enabled = False
                txtAuthPwd.Visible = False
                lblAuthPwd.Visible = False
            End If
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "User Failed")
            Call MsgBox("Invalid authorisation code entered", vbExclamation, Me.Caption)
        End If
    End If
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdClear_GotFocus()

    cmdClear.FontBold = True

End Sub

Private Sub cmdClear_LostFocus()
    
    cmdClear.FontBold = False

End Sub

Private Sub Form_GotFocus()

    cmdClear.Visible = True

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                    If cmdClear.Visible Then
                        KeyCode = 0
                        Call cmdClear_Click
                    End If
            Case (vbKeyF10):
                    If cmdCancel.Visible Then
                        KeyCode = 0
                        Call cmdCancel_Click
                    End If
         End Select
     End If

End Sub

Private Sub Form_Load()

Dim oTrans   As CTSTranslator.clsTranslator
Dim lCtlNo   As Long

    If LenB(Command) = 0 Then
        Call MsgBox("This option can only be accessed from the Menu", vbCritical, "Clear password")
        End
    End If
    
    Set oTrans = New clsTranslator
    'Translate form caption first
'    Me.Caption = oTrans.TranslateStr(Me.Tag, Me.Caption)
    'Step through each control and translate
    For lCtlNo = 0 To Me.Controls.Count - 1 Step 1
'        If Val(Controls(lCtlNo).Tag) > 0 Then Controls(lCtlNo).Caption = oTrans.TranslateStr(Controls(lCtlNo).Tag, Controls(lCtlNo).Caption)
        Debug.Print Controls(lCtlNo).Name
    Next lCtlNo
    sFailedMsg = oTrans.TranslateStr(117, "Invalid password for User ID, re-enter password")
    Set oTrans = Nothing
    
    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    Call InitialiseStatusBar(sbStatus)
    Call CentreForm(Me)

End Sub

Private Sub txtAuthID_GotFocus()
    
    txtAuthID.SelStart = 0
    txtAuthID.SelLength = Len(txtAuthID.Text)
    txtAuthID.BackColor = RGBEdit_Colour

End Sub

Private Sub txtAuthID_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub txtAuthID_LostFocus()
    
Const PROCEDURE_NAME As String = "txtAuthID_LostFocus"
    
    txtAuthID.BackColor = lblInitials.BackColor
    
    If Len(txtAuthID.Text) <> 3 Then txtAuthID.Text = Left$("000", 3 - Len(txtAuthID.Text)) & txtAuthID.Text
        
    'Get User details
    Screen.MousePointer = vbHourglass
    Set moAuthUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    moAuthUser.EmployeeID = txtAuthID.Text
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Auth UserID Validated")
    If moAuthUser.IBo_Load = True Then
        If moAuthUser.Supervisor = True Then
            lblAuthPwd.Visible = True
            txtAuthPwd.Visible = True
            DoEvents
            Call txtAuthPwd.SetFocus
        Else
            Call MsgBox("User does not have Supervisor status", vbExclamation, "Verify authorisation user")
            lblAuthPwd.Visible = False
            txtAuthPwd.Visible = False
        End If
    Else
        lblAuthPwd.Visible = False
        txtAuthPwd.Visible = False
        txtAuthPwd.Text = vbNullString
        Set moAuthUser = Nothing
    End If
    Screen.MousePointer = vbNormal
    txtAuthID.BackColor = lblInitials.BackColor


End Sub

Private Sub txtAuthPwd_GotFocus()
    
    txtAuthPwd.SelStart = 0
    txtAuthPwd.SelLength = Len(txtAuthPwd.Text)
    txtAuthPwd.BackColor = RGBEdit_Colour
    cmdClear.Visible = True

End Sub

Private Sub txtAuthPwd_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then Call cmdClear_Click
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+" & vbTab)
    End If

End Sub

Private Sub txtAuthPwd_LostFocus()
    
    txtAuthPwd.BackColor = lblInitials.BackColor

End Sub

Private Sub txtUserID_Change()

    lblInitials.Caption = vbNullString
    lblFullName.Caption = vbNullString
    
End Sub

Private Sub txtUserID_GotFocus()

    txtUserID.SelStart = 0
    txtUserID.SelLength = Len(txtUserID.Text)
    txtUserID.BackColor = RGBEdit_Colour

End Sub

Private Sub txtUserID_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub


Private Sub txtUserID_LostFocus()

Const PROCEDURE_NAME As String = "txtUserID_LostFocus"
    
    If Len(txtUserID.Text) <> 3 Then txtUserID.Text = Left$("000", 3 - Len(txtUserID.Text)) & txtUserID.Text
        
    'Get User details
    Screen.MousePointer = vbHourglass
    Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    moUser.EmployeeID = txtUserID.Text
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "UserID Validated")
    If moUser.IBo_Load = True Then
        lblInitials.Caption = moUser.Initials
        lblFullName.Caption = moUser.FullName
        txtAuthID.Enabled = True
        Call txtAuthID.SetFocus
    Else
        txtAuthID.Enabled = False
        lblInitials.Caption = vbNullString
        lblFullName.Caption = "INVALID ID"
        Set moUser = Nothing
    End If
    Screen.MousePointer = vbNormal
    txtUserID.BackColor = lblInitials.BackColor

End Sub
