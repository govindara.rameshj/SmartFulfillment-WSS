VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IOasysRoot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysStartCom/IOasysRoot.cls $
' $Revision: 6 $
' $Date: 10/11/02 2:43p $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' Interface only, so no data or procedure please!
'----------------------------------------------------------------------------

' The qualifier parameter to Initialise is reserved for future use.
' One likely use is to flag a Web-server environment.
Public Function Initialise(Optional strCommandLine As String, Optional strQualifier As String) As Boolean
End Function
Public Function CreateSession(Optional strCommandLine As String) As Object
End Function
Public Function CreateBusinessObject(nClassId As Integer, oSession As Object) As Object
End Function
Public Function CreateUtilityObject(strClass As String) As Object
End Function
Public Property Get Client() As Object
End Property
Public Property Get Version() As String
End Property
Public Property Get IsRunningAsWebServer() As Boolean
End Property
' Should use the current enterprise relative registry key
'Public Property Get RelativeRegistryKey() As String
'End Property
Public Function CreateBoColFromClassID(nClassId As Integer, oSession As Object) As Collection
End Function
