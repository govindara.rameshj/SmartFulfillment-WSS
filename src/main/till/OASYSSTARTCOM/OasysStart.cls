VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OasysStart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
#Const ccDebug = 0
'
' $Archive: /Projects/OasysV2/VB/OasysStartCom/OasysStart.cls $
' $Revision: 8 $
' $Date: 7/06/04 10:04 $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' The Start object is the fixed starting place from which we get an
' Oasys root object.  By default we simply get the root object held
' in the OasysRootCom app.
' However we can redirect to a different Oasys version OasysRootQQQQCom
' where QQQQ is a qualifier that is given in the registry.
' The calling program can optionally give some qualification, but
' this is reserved for future use.
' We may use it to specify stuff like the minimum version nn.nn.nn or
' some other attribute where there is a choice of dlls available.
' Perhaps stuff like if it is a Web environment.
' Note that GetRoot() return an OaysyRoot Object so we must ensure that
' the interface does not change so as to disrupt existing references.
'----------------------------------------------------------------------------
Private Const OASYS_START_VERSION_STRING = "1.0"
'Private Const OASYS_ERR_UNRECOGNISED_REGISTRY_ENTRY = vbObjectError + 9

Public Function GetRoot(Optional strCommandLine As String, Optional strUserQualifier As String) As IOasysRoot
    On Error GoTo GetRoot_Load_Err
#If ccLateBound = 1 Then
    Dim oRegistry As Object
    Dim oStartRoot As Object
    
    Debug.Print "OasysStart LateBound Registry"
#Else
    Dim oRegistry As Registry
    Dim oStartRoot As OasysRootCom_Wickes.StartRoot
    
    Debug.Print "OasysStart Early bound Registry"
#End If
    Dim strValue As String
    Dim strKey As String
    Dim strQualifier As String
    Dim nResult As Long
    
    Set GetRoot = Nothing   ' Default is fail
    Call DebugMsg("START", "GetRoot", endlDebug, "Get Registry")
    Set oRegistry = New OASYSCBASECOMLib.Registry
    If oRegistry Is Nothing Then
        Debug.Print "*** OasysStart failed to create a Registry (& root) object"
        Exit Function
    End If
    Call DebugMsg("START", "GetRoot", endlDebug, "Get Registry Value")
    oRegistry.GetCTSKeyList "OASYS", strValue, nResult
    If nResult = 0 Then
        ' strValue may be empty but not null
        ' We assume that there is only one Oasys version installed
        ' under this config.  But for resilience we cater for more
        ' with comma delimiters.
        nResult = InStr(strValue, ",")
        If nResult <> 0 Then
            strValue = Left$(strValue, nResult)
        End If
        ' strValue now holds the Oasys version, so we can tailor
        ' the default qualifier.
        Select Case strValue
        Case Is = "2.0"
            ' This is the starting point, so empty qualifier
        Case Else
            ' All unknown versions default to an empty qualifier
        End Select
        ' We can now build the relative key to look for a qualifier
        strKey = "OASYS\" & strValue
        oRegistry.GetCTSStringValue strKey, "ComQualifier", strValue, nResult
        If nResult = 0 Then
            ' We got one which overrides the default
            strQualifier = strValue
        End If
    End If
    Set oRegistry = Nothing
    ' Note that the OasysRoot is private, the only createable guy is StartRoot.
    Call DebugMsg("START", "GetRoot", endlDebug, "Create Root")
    strValue = "OasysRoot" & strQualifier & "Com.StartRoot"
#If ccDebug = 1 Then
    MsgBox "OasysStart about to create root from: " & strValue
#End If
    Set oStartRoot = New OasysRootCom_Wickes.StartRoot ' CreateObject(strValue)    ' get StartRoot
    If Not (oStartRoot Is Nothing) Then
        Set GetRoot = oStartRoot.IOasysRoot    ' get IOasysRoot
    End If
    Call DebugMsg("START", "GetRoot", endlDebug, "Create Root - Initialise")
    Set oStartRoot = Nothing
    If Not (GetRoot Is Nothing) Then
        If Not GetRoot.Initialise(strCommandLine) Then
            Set GetRoot = Nothing
        End If
    End If
    If GetRoot Is Nothing Then
        Debug.Print "*** OasysStart failed to create a root object"
    Else
        Debug.Print "OasysStart created root from: " & strValue
#If ccDebug = 1 Then
        MsgBox "OasysStart created root from: " & strValue
#End If
    End If
    Call DebugMsg("START", "GetRoot", endlDebug, "DONE")
    Exit Function
    
GetRoot_Load_Err:
    If Err.Number <> 0 Then
        Select Case Err.Number
        Case Is = 0
        Case Is = 13, Is = 429
            '  13 - Type mismatch, ie. Wrong interface ID
            ' 429 - Active X can't create object, ie. Not found
            Err.Description = Err.Description & ": " & strValue
        Case Else
            Err.Description = Err.Description & ": " & strValue
        End Select
        Err.Source = "Client::DefaultInstallMnemonic()"
        Err.Description = Err.Source & vbCrLf & Err.Description
        Err.Raise Err.Number
    End If
    Resume Next
End Function
