VERSION 5.00
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Begin VB.Form frmMatchCode 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Address"
   ClientHeight    =   4065
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9885
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4065
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin LpLib.fpCombo cmbAddresses 
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9855
      _Version        =   196608
      _ExtentX        =   17383
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   0   'False
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Text            =   ""
      Columns         =   2
      Sorted          =   1
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   0
      ColumnWidthScale=   2
      RowHeight       =   -1
      WrapList        =   0   'False
      WrapWidth       =   0
      AutoSearch      =   2
      SearchMethod    =   1
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   0
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   0
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   1
      BorderColor     =   12164479
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   1
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   0
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   0   'False
      VScrollSpecialType=   0
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   2
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   3
      DataFieldList   =   ""
      ColumnEdit      =   0
      ColumnBound     =   -1
      Style           =   1
      MaxDrop         =   8
      ListWidth       =   -1
      EditHeight      =   -1
      GrayAreaColor   =   -2147483633
      ListLeftOffset  =   0
      ComboGap        =   -2
      MaxEditLen      =   150
      VirtualPageSize =   0
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   -1  'True
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      ListPosition    =   0
      ButtonThreeDAppearance=   0
      OLEDragMode     =   0
      OLEDropMode     =   0
      Redraw          =   -1  'True
      AutoSearchFill  =   -1  'True
      AutoSearchFillDelay=   500
      EditMarginLeft  =   1
      EditMarginTop   =   1
      EditMarginRight =   0
      EditMarginBottom=   3
      ResizeRowToFont =   -1  'True
      TextTipMultiLine=   0
      AutoMenu        =   -1  'True
      EditAlignH      =   0
      EditAlignV      =   0
      ColDesigner     =   "frmMatchCode.frx":0000
   End
End
Attribute VB_Name = "frmMatchCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type tPAFAddress
    Organisation As String
    SubBuilding As String
    BuildingName As String
    BuildingNumber As String
    DepStreet As String
    Street As String
    DepLocality As String
    Locality As String
    PostTown As String
    County As String
    PostCode As String
End Type

Public Event Click(Address As Variant)
Public Event Duress()

Public DataPath   As String
Public ErrLogPath As String
Public Cancel     As Boolean
Public NoMatches  As Boolean

Private mintDuressKeyCode As Integer

Private AddressLookupResponse As COMTPWickes_InterOp_Implementation.AddressLookupResponse

Private AddressLookupService As COMTPWickes_InterOp_Implementation.AddressLookupService

Public Function GetAddress(PostCode As String) As String()

    NoMatches = False
    Cancel = False
    
    Dim apiAddress As String
    apiAddress = goSession.GetParameter(PRM_SITE_ADDRESS)
    
    If AddressLookupService Is Nothing Then
        Set AddressLookupService = New COMTPWickes_InterOp_Implementation.AddressLookupService
    End If
    
    Call AddressLookupService.Initialize(apiAddress)
    Set AddressLookupResponse = AddressLookupService.ResolvePostCode(PostCode)
    
    Call ProcessAddressLookupResponse
End Function

Private Sub cmbAddresses_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        Cancel = True
        Me.Hide
    End If
    
    If KeyAscii = vbKeyReturn Then
        If (cmbAddresses.ListIndex = -1) Then Exit Sub
        cmbAddresses.Row = cmbAddresses.ListIndex
        Me.Hide
        
        Dim id As String
        id = GetIdByDescription(cmbAddresses.ColText)
        
        If AddressLookupService Is Nothing Then
            Set AddressLookupService = New COMTPWickes_InterOp_Implementation.AddressLookupService
        End If
    
        Set AddressLookupResponse = AddressLookupService.RefinePickListItem(id)
    
        Call ProcessAddressLookupResponse
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub

Private Sub Form_Load()
    
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    
    cmbAddresses.ApplyTo = ApplyToListOnly
    cmbAddresses.ListApplyTo = ListApplyToEvenRows
    cmbAddresses.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    cmbAddresses.ListApplyTo = ListApplyToOddRows
    cmbAddresses.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    
    cmbAddresses.ApplyTo = ApplyToEditOnly
    cmbAddresses.BackColor = goSession.GetParameter(PRM_EDITCOLOUR)
    
    cmbAddresses.Height = Me.Height - 480

End Sub

Private Sub ProcessAddressLookupResponse()
  
    If AddressLookupResponse Is Nothing Then
        Call MsgBoxEx("The requested information can't be retrieved right now. Please, fill in the details manually.", vbOKOnly + vbExclamation, "Postcode Entry", , , , , RGBMSGBox_PromptColour)
        NoMatches = True
        MousePointer = vbDefault
    Exit Sub
        Else
        If AddressLookupResponse.Status = COMTPWickes_InterOp_Implementation.AddressLookupStatus_ExactMatchFound Then
            Dim wkReturn(0 To 3) As String
            wkReturn(0) = AddressLookupResponse.Address.AddressLine1
            wkReturn(1) = AddressLookupResponse.Address.AddressLine2
            wkReturn(2) = AddressLookupResponse.Address.Town
            wkReturn(3) = AddressLookupResponse.Address.PostCode
                
            RaiseEvent Click(wkReturn)
            MousePointer = vbDefault
            Exit Sub
        ElseIf AddressLookupResponse.Status = COMTPWickes_InterOp_Implementation.AddressLookupStatus_SeveralMatchesFound Then
            cmbAddresses.Clear
            cmbAddresses.Text = ""
            cmbAddresses.Col = -1
            cmbAddresses.Row = -1
            cmbAddresses.AlignH = AlignHLeft
                
            Dim PickupItem As COMTPWickes_InterOp_Implementation.PickupItem
            For Each PickupItem In AddressLookupResponse.PickupItems
                Call cmbAddresses.AddItem(PickupItem.description)
                cmbAddresses.Row = cmbAddresses.NewIndex
                cmbAddresses.ItemData = cmbAddresses.ListCount
            Next
            
            cmbAddresses.ListIndex = -1
            MousePointer = vbDefault
            Me.Show vbModal
        ElseIf AddressLookupResponse.Status = COMTPWickes_InterOp_Implementation.AddressLookupStatus_NotFound Then
            Call MsgBoxEx("There are no results for this search. Please check the postcode.", vbOKOnly + vbExclamation, "Postcode Entry", , , , , RGBMSGBox_PromptColour)
            NoMatches = True
            MousePointer = vbDefault
            Exit Sub
        ElseIf AddressLookupResponse.Status = COMTPWickes_InterOp_Implementation.AddressLookupStatus_TooManyMatches Then
            Call MsgBoxEx("There are too many results for this search. Please try again with more specific postcode.", vbOKOnly + vbExclamation, "Postcode Entry", , , , , RGBMSGBox_PromptColour)
            NoMatches = True
            MousePointer = vbDefault
            Exit Sub
        End If
    End If
End Sub

Private Function GetIdByDescription(description As String) As String

    Dim PickupItem As COMTPWickes_InterOp_Implementation.PickupItem
    For Each PickupItem In AddressLookupResponse.PickupItems
        If PickupItem.description = description Then
            GetIdByDescription = PickupItem.id
            Exit Function
        End If
    Next
End Function
