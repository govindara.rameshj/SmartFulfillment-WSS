VERSION 5.00
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.UserControl ucCaptureCust 
   ClientHeight    =   7110
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9540
   KeyPreview      =   -1  'True
   ScaleHeight     =   7110
   ScaleWidth      =   9540
   Begin VB.PictureBox fraAddress 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   4755
      Left            =   120
      ScaleHeight     =   4725
      ScaleWidth      =   9225
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   9255
      Begin VB.TextBox txtEmail 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   2280
         MaxLength       =   50
         TabIndex        =   15
         Top             =   3720
         Width           =   6735
      End
      Begin VB.TextBox txtWorkTelNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2280
         MaxLength       =   15
         TabIndex        =   14
         Top             =   3120
         Width           =   2655
      End
      Begin VB.PictureBox fraButtons 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   120
         ScaleHeight     =   495
         ScaleWidth      =   8955
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   3240
         Width           =   8955
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5220
            TabIndex        =   46
            Top             =   0
            Width           =   1815
         End
         Begin VB.CommandButton cmdReset 
            Caption         =   "Reset"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   0
            TabIndex        =   45
            Top             =   0
            Width           =   1815
         End
         Begin VB.CommandButton cmdSave 
            Caption         =   "Save"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   7080
            TabIndex        =   44
            Top             =   0
            Width           =   1875
         End
      End
      Begin VB.TextBox txtMobileNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   6360
         TabIndex        =   13
         Top             =   2640
         Width           =   2655
      End
      Begin VB.TextBox txtPostCode 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   2280
         MaxLength       =   10
         TabIndex        =   5
         Top             =   720
         Width           =   2295
      End
      Begin VB.TextBox txtAddress 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   1
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   7
         Top             =   1200
         Width           =   6735
      End
      Begin VB.TextBox txtAddress 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   2
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   8
         Top             =   1680
         Width           =   6735
      End
      Begin VB.TextBox txtAddress 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   3
         Left            =   2280
         MaxLength       =   20
         TabIndex        =   9
         Top             =   2160
         Width           =   6735
      End
      Begin VB.TextBox txtTelNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   2280
         MaxLength       =   20
         TabIndex        =   11
         Top             =   2640
         Width           =   2655
      End
      Begin VB.TextBox txtName 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   2
         Top             =   240
         Width           =   6255
      End
      Begin VB.Label lblEmail 
         BackStyle       =   0  'Transparent
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   50
         Top             =   3720
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.Label lblWorkTelNo 
         BackStyle       =   0  'Transparent
         Caption         =   "Work Tel No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   49
         Top             =   3240
         Width           =   1815
      End
      Begin VB.Label lblMobileNo 
         BackStyle       =   0  'Transparent
         Caption         =   "Mobile"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5160
         TabIndex        =   12
         Top             =   2640
         Width           =   1095
      End
      Begin VB.Label lblAddressLbl4 
         BackStyle       =   0  'Transparent
         Caption         =   "Town"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   48
         Top             =   2160
         Width           =   2055
      End
      Begin VB.Label lblAddressLbl3 
         BackStyle       =   0  'Transparent
         Caption         =   "Address 2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   47
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label lblTelNo 
         BackStyle       =   0  'Transparent
         Caption         =   "Tel No"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   10
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label lblPostCodelbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Post Code"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   4
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label lblAddressLbl2 
         BackStyle       =   0  'Transparent
         Caption         =   "Address 1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   1200
         Width           =   2235
      End
      Begin VB.Label lblName 
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   1
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.PictureBox fraOrderDetails 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   120
      ScaleHeight     =   705
      ScaleWidth      =   9225
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   120
      Visible         =   0   'False
      Width           =   9255
      Begin VB.TextBox txtOrderNo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         TabIndex        =   3
         Top             =   240
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label lblOrderDate 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3960
         TabIndex        =   19
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblOrderNo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         TabIndex        =   18
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblTraderNo 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7320
         TabIndex        =   22
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Order No :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   17
         Top             =   240
         Width           =   2775
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Trader No :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5640
         TabIndex        =   20
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.PictureBox fraOrderFooter 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3375
      Left            =   120
      ScaleHeight     =   3345
      ScaleWidth      =   9225
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   3600
      Visible         =   0   'False
      Width           =   9255
      Begin VB.TextBox txtRemarks 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   3
         Left            =   2280
         MaxLength       =   40
         TabIndex        =   31
         Top             =   2160
         Width           =   6735
      End
      Begin VB.TextBox txtRemarks 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   2
         Left            =   2280
         MaxLength       =   40
         TabIndex        =   30
         Top             =   1680
         Width           =   6735
      End
      Begin VB.TextBox txtRemarks 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Index           =   1
         Left            =   2280
         MaxLength       =   40
         TabIndex        =   29
         Top             =   1200
         Width           =   6735
      End
      Begin VB.TextBox txtRemarks 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   0
         Left            =   2280
         MaxLength       =   40
         TabIndex        =   28
         Top             =   720
         Width           =   6735
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Remarks :-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   27
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Order Status"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3960
         TabIndex        =   25
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Deposit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   23
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label lblOrderStatus 
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6000
         TabIndex        =   26
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label lblDepositValue 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2280
         TabIndex        =   24
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.PictureBox fraOriginalTran 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3855
      Left            =   120
      ScaleHeight     =   3825
      ScaleWidth      =   9225
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   120
      Visible         =   0   'False
      Width           =   9255
      Begin LpLib.fpCombo cmbOrigPmtType 
         Height          =   555
         Left            =   2880
         TabIndex        =   38
         Top             =   1200
         Width           =   2535
         _Version        =   196608
         _ExtentX        =   4471
         _ExtentY        =   979
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   2
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "ucCaptureCust.ctx":0000
      End
      Begin LpLib.fpCombo cmbDiscReason 
         Height          =   555
         Left            =   2880
         TabIndex        =   40
         Top             =   1800
         Width           =   6135
         _Version        =   196608
         _ExtentX        =   10821
         _ExtentY        =   979
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   2
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "ucCaptureCust.ctx":0396
      End
      Begin LpLib.fpCombo cmbRefundStore 
         Height          =   555
         Left            =   2880
         TabIndex        =   42
         Top             =   2400
         Width           =   6135
         _Version        =   196608
         _ExtentX        =   10821
         _ExtentY        =   979
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   2
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "ucCaptureCust.ctx":072C
      End
      Begin VB.TextBox txtOrigTranID 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   2880
         MaxLength       =   40
         TabIndex        =   34
         Top             =   240
         Width           =   1935
      End
      Begin ucEditDate.ucDateText dtxtOrigTranDate 
         Height          =   495
         Left            =   2880
         TabIndex        =   36
         Top             =   720
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   873
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "14/01/04"
      End
      Begin VB.Label Label17 
         BackStyle       =   0  'Transparent
         Caption         =   "Refund Store :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   41
         Top             =   2520
         Width           =   2655
      End
      Begin VB.Label Label16 
         BackStyle       =   0  'Transparent
         Caption         =   "Discount Reason :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   39
         Top             =   1920
         Width           =   2655
      End
      Begin VB.Label Label15 
         BackStyle       =   0  'Transparent
         Caption         =   "Payment Method :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   37
         Top             =   1320
         Width           =   2655
      End
      Begin VB.Label Label14 
         BackStyle       =   0  'Transparent
         Caption         =   "Date :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   35
         Top             =   780
         Width           =   2535
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Transaction No :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   33
         Top             =   240
         Width           =   2535
      End
   End
End
Attribute VB_Name = "ucCaptureCust"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : ucCaptureCust
'* Date   : 14/10/03
'* Author : mauricem
'*$Archive:$
'**********************************************************************************************
'* Summary: This provides a control for capturing Customer details from within the till.  As
'*          there may be other information required, custom methods can be added to extend the
'*          amount of details captured.
'**********************************************************************************************
'* $ Author: $ $ Date: $ $ Revision: $
'* Versions:
'* 14/10/03    mauricem
'*             Header added.
'* 6/1/09      Wix1345 MO'C amended to allow Concern resolution Customer details.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "ucCaptureCust"

Const OFFSET As Integer = 135

Dim mblnInit         As Boolean 'Flag to ensure initialise can only be called once
Dim moParent         As Form
Dim mblnPreview      As Boolean
Dim mblnShowOrder    As Boolean 'true if Order Details shown
Dim mblnShowOrigTran As Boolean 'true if original transaction details shown
Dim mblnNameOnly     As Boolean
Dim mblnPCodeEdited  As Boolean

Dim mlngSearchKeyCode As Long
Dim mlngResetKeyCode  As Long
Dim mlngCloseKeyCode  As Long
Dim mlngUseKeyCode    As Long

Dim mblnEditDetails As Boolean

Dim mblnSelectOrder As Boolean
Dim mblnEntryWaiting As Boolean
Dim mblnForceTelWarning As Boolean

Public Event Save(Name As String, PostCode As String, Address As String, TelNo As String, MobileNo As String, WorkTelNo As String, Email As String)
Public Event SaveCustRes(Salutaion As String, ForeName As String, Surname As String, Company As String, PostCode As String, HouseNo As String, Address1 As String, Address2 As String, Town As String, County As String, Country As String)
Public Event UseOrder(OrderNumber As String)
Public Event Cancel()
Public Event Resize()

Private WithEvents frmMatchCode As frmMatchCode
Attribute frmMatchCode.VB_VarHelpID = -1

Public Property Get Remarks() As String
    
    Remarks = txtRemarks(0).Text & vbCrLf & txtRemarks(1).Text & vbCrLf & txtRemarks(2).Text & vbCrLf & txtRemarks(3).Text & vbCrLf

End Property

Public Property Get OrigTranID() As String
    
    OrigTranID = txtOrigTranID.Text

End Property

Public Property Get OrigTranDate() As Date
    
    OrigTranDate = GetDate(dtxtOrigTranDate.Text)

End Property

Public Property Get OrigTranStore() As String
    
    OrigTranStore = cmbRefundStore.Text

End Property

Public Property Get OrigPmtType() As String
    
    OrigPmtType = cmbOrigPmtType.Text

End Property

Public Property Let Remarks(strValue As String)
    
Dim vntRemarks
Dim lngRemarkNo As Long

    'split up single remarks into seperate lines, marked by Carraige Return
    vntRemarks = Split(strValue, vbCrLf)
    For lngRemarkNo = 1 To UBound(vntRemarks) Step 1
        txtRemarks(lngRemarkNo).Text = vntRemarks(lngRemarkNo)
    Next lngRemarkNo

End Property

Public Property Let NameOnly(Value As Boolean)
    mblnNameOnly = Value
End Property

'***********************************************************************
'MO'C 06-01-2009 - WIX1345 - Added routine below along with new controls
'                  to allow capture of the Customers details for Concern
'                  Resolution Log.
'***********************************************************************

Public Sub CaptureAddress(Name As String, PostCode As String, Address As String, TelNo As String)

Dim vntRemarks
Dim lngRemarkNo As Long

    mblnSelectOrder = False
    lblOrderNo.Caption = vbNullString
    lblOrderDate.Caption = vbNullString
    lblTraderNo.Caption = vbNullString
    lblDepositValue.Caption = vbNullString
    lblOrderStatus.Caption = vbNullString
    fraOrderDetails.Visible = False
    fraOrderFooter.Visible = False
    fraAddress.Top = 120
    fraAddress.Height = txtTelNo.Top + 240 + cmdSave.Height
    fraButtons.Top = fraAddress.Top + fraAddress.Height - fraButtons.Height - 240
    mblnShowOrder = False
    Call UserControl_Resize
    
    txtName.Enabled = True
    txtAddress(1).Enabled = True
    txtAddress(2).Enabled = True
    txtAddress(3).Enabled = True
    txtTelNo.Enabled = True
    txtMobileNo.Enabled = True
    txtPostCode.Enabled = True
    txtOrderNo.Visible = False
    cmdSave.Visible = False
    cmdReset.Visible = False
    
    txtTelNo.Visible = False
    lblTelNo.Visible = False
    txtMobileNo.Visible = False
    lblMobileNo.Visible = False
    
    Call cmdReset_Click
    RaiseEvent Resize
    
    If txtName.Visible = True Then txtName.SetFocus
    mblnEntryWaiting = True
    While mblnEntryWaiting = True
        DoEvents
    Wend
    
    Name = txtName.Text
    PostCode = txtPostCode.Text
    Address = txtAddress(1).Text & vbCrLf & txtAddress(2).Text & vbCrLf & txtAddress(3).Text & vbCrLf
    TelNo = txtTelNo.Text
'    Call FillInOrderList

End Sub

Public Sub CaptureWarrantyAddress(Name As String, PostCode As String, Address As String, TelNo As String)

Dim vntRemarks
Dim lngRemarkNo As Long

    mblnSelectOrder = False
    lblOrderNo.Caption = vbNullString
    lblOrderDate.Caption = vbNullString
    lblTraderNo.Caption = vbNullString
    lblDepositValue.Caption = vbNullString
    lblOrderStatus.Caption = vbNullString
    fraOrderDetails.Visible = False
    fraOrderFooter.Visible = False
    fraAddress.Top = 120
    fraAddress.Height = txtTelNo.Top + txtTelNo.Height + 120 + cmdSave.Height
    fraButtons.Top = fraAddress.Height - fraButtons.Height - 60
    mblnShowOrder = False
    Call UserControl_Resize
    
    txtName.Enabled = True
    txtAddress(1).Enabled = True
    txtAddress(2).Enabled = True
    txtAddress(3).Enabled = True
    txtTelNo.Enabled = True
    txtPostCode.Enabled = True
    txtOrderNo.Visible = False
    cmdSave.Visible = False
    cmdReset.Visible = False
    
    txtTelNo.Visible = False
    lblTelNo.Visible = False
    lblMobileNo.Visible = False
    txtMobileNo.Visible = False
    
    Call cmdReset_Click
    RaiseEvent Resize
    
    If txtName.Visible = True Then txtName.SetFocus
    mblnEntryWaiting = True
    While mblnEntryWaiting = True
        DoEvents
    Wend
    
    Name = txtName.Text
    PostCode = txtPostCode.Text
    Address = txtAddress(1).Text & vbCrLf & txtAddress(2).Text & vbCrLf & txtAddress(3).Text & vbCrLf
    TelNo = txtTelNo.Text
'    Call FillInOrderList

End Sub

Public Sub CaptureOrderCustomer(ByRef strName As String, _
                                ByRef strPostCode As String, _
                                ByRef strAddress As String, _
                                ByRef strTelNo As String, _
                                ByRef strRemarks As String, _
                                ByVal strOrderNumber As String, _
                                ByVal dteOrderDate As Date, _
                                ByVal strTraderNumber As String, _
                                ByVal dblDepositValue As Double, _
                                ByVal strOrderStatus As String)

Dim vntRemarks
Dim lngRemarkNo As Long

    'Display Current Order details
    mblnForceTelWarning = True
    lblOrderNo.Caption = strOrderNumber
    lblOrderDate.Caption = DisplayDate(dteOrderDate, False)
    lblTraderNo.Caption = strTraderNumber
    lblDepositValue.Caption = Format$(dblDepositValue, "0.00")
    lblOrderStatus.Caption = strOrderStatus
    fraOrderDetails.Visible = True
    fraOrderFooter.Visible = True
    
    'Display frames required to capture address and show order
    fraAddress.Top = fraOrderDetails.Height
    fraAddress.Height = cmdSave.Top + 120 + cmdSave.Height
    fraOrderFooter.Top = fraAddress.Top + fraAddress.Height - 120
    fraButtons.Top = fraOrderFooter.Top + fraOrderFooter.Height - fraButtons.Height - 120
    'split up single remarks into seperate lines, marked by Carraige Return
    vntRemarks = Split(strRemarks, vbCrLf)
    For lngRemarkNo = 1 To UBound(vntRemarks) Step 1
        txtRemarks(lngRemarkNo).Text = vntRemarks(lngRemarkNo)
    Next lngRemarkNo
    mblnShowOrder = True
    mblnSelectOrder = False
    
    'enable buttons to allow editing of values
    txtName.Enabled = True
    txtAddress(1).Enabled = True
    txtAddress(2).Enabled = True
    txtAddress(3).Enabled = True
    txtTelNo.Enabled = True
    txtPostCode.Enabled = True
    txtOrderNo.Visible = False
    cmdSave.Visible = False
    cmdReset.Visible = False
    
    Call UserControl_Resize
    RaiseEvent Resize
    
    'Now start the wait for the user to finish editing before returning values
    mblnEntryWaiting = True
    While mblnEntryWaiting = True
        DoEvents
    Wend
    strName = txtName.Text
    strPostCode = txtPostCode.Text
    strAddress = txtAddress(1).Text & vbCrLf & txtAddress(2).Text & vbCrLf & txtAddress(3).Text & vbCrLf
    strTelNo = txtTelNo.Text
    strRemarks = txtRemarks(0).Text & vbCrLf & txtRemarks(1).Text & vbCrLf & txtRemarks(2).Text & vbCrLf & txtRemarks(3).Text & vbCrLf

End Sub

Public Function SelectOrder() As String
                            
Dim vntRemarks
Dim lngRemarkNo As Long

    mblnSelectOrder = True
    lblOrderNo.Caption = vbNullString
    lblOrderDate.Caption = vbNullString
    lblTraderNo.Caption = vbNullString
    lblDepositValue.Caption = vbNullString
    lblOrderStatus.Caption = vbNullString
    fraOrderDetails.Visible = True
    fraOrderFooter.Visible = True
    fraAddress.Top = fraOrderDetails.Height
    fraAddress.Height = cmdSave.Top + 120 + cmdSave.Height
    fraOrderFooter.Top = fraAddress.Top + fraAddress.Height - 120
    fraButtons.Top = fraOrderFooter.Top + fraOrderFooter.Height - fraButtons.Height - 120
    mblnShowOrder = True
    Call UserControl_Resize
    
    Call cmdReset_Click
    
    txtName.Enabled = False
    txtAddress(1).Enabled = False
    txtAddress(2).Enabled = False
    txtAddress(3).Enabled = False
    txtTelNo.Enabled = False
    txtPostCode.Enabled = False
    'split up single remarks into seperate lines, marked by Carraige Return
    For lngRemarkNo = 1 To 4 Step 1
        txtRemarks(lngRemarkNo - 1).Enabled = False
    Next lngRemarkNo
    txtOrderNo.Visible = True
    cmdSave.Visible = False
    cmdReset.Visible = False
    
    RaiseEvent Resize
    
'    Call FillInOrderList

End Function

Public Sub HideOrderDetails()
    
    fraOrderDetails.Visible = False
    fraOrderFooter.Visible = False
    mblnShowOrder = False
    fraAddress.Top = 120
    Call UserControl_Resize
    RaiseEvent Resize

End Sub

Public Sub ShowRefundDetails(strTranNumber As String, _
                            dteTranDate As Date, _
                            strPaymentMethod As String, _
                            strOrigStoreNo As String, _
                            blnLockDetails As Boolean, _
                            blnShowMobile As Boolean, _
                            Optional blnOrder As Boolean = False)
                            
Dim colStores As Collection
Dim oStore     As cStore
Dim lngStoreNo As Long
                            
    'check if list of Refund stores has been filled in yet
    mblnForceTelWarning = blnOrder
    If cmbRefundStore.ListCount = 0 Then
        Set oStore = goDatabase.CreateBusinessObject(CLASSID_STORE)
        Call oStore.AddLoadField(FID_STORE_StoreNumber)
        Call oStore.AddLoadField(FID_STORE_AddressLine1)
        Set colStores = oStore.LoadMatches
        
        For lngStoreNo = 1 To colStores.Count Step 1
            Set oStore = colStores(lngStoreNo)
            If oStore.StoreNumber <> goSession.CurrentEnterprise.IEnterprise_StoreNumber Then
                Call cmbRefundStore.AddItem(oStore.StoreNumber & vbTab & oStore.AddressLine1 & vbTab & Val(oStore.StoreNumber))
                cmbRefundStore.ItemData(cmbRefundStore.NewIndex) = lngStoreNo 'point reference to locate Store in Collection
            End If
        Next lngStoreNo
    End If
    cmbRefundStore.Enabled = blnLockDetails
    
    'ensure that Control is reset before starting
    Call HideOrderDetails
    txtOrigTranID.Text = strTranNumber
    txtOrigTranID.Enabled = blnLockDetails
    dtxtOrigTranDate.Text = DisplayDate(dteTranDate, False)
    dtxtOrigTranDate.Enabled = blnLockDetails
    cmbOrigPmtType.Text = strPaymentMethod
    cmbOrigPmtType.Enabled = False
    'fraOriginalTran.Visible = True
    'fraOriginalTran.Top = fraAddress.Height
    'fraButtons.Top = fraOriginalTran.Top + cmbRefundStore.Top + cmbRefundStore.Height + 120
    fraAddress.Top = 120
    If blnOrder Then
        fraAddress.Height = txtEmail.Top + txtEmail.Height + 120 + cmdSave.Height
    Else
        fraAddress.Height = txtTelNo.Top + txtTelNo.Height + 120 + cmdSave.Height
    End If
    fraButtons.Top = fraAddress.Height - fraButtons.Height - 60
    mblnShowOrigTran = False
    Call UserControl_Resize
    RaiseEvent Resize

    txtName.Enabled = True
    txtAddress(1).Enabled = True
    txtAddress(2).Enabled = True
    txtAddress(3).Enabled = True
    txtTelNo.Enabled = True
    txtMobileNo.Enabled = True
    txtPostCode.Enabled = True
    txtOrderNo.Visible = False
    cmdReset.Visible = False
    lblMobileNo.Visible = blnShowMobile
    txtMobileNo.Visible = blnShowMobile
    lblWorkTelNo.Visible = blnOrder
    txtWorkTelNo.Visible = blnOrder
    lblEmail.Visible = blnOrder
    txtEmail.Visible = blnOrder
'    txtTelNo.Visible = False
    If txtName.Visible = True Then txtName.SetFocus
    'Added 7/4/10 to force focus onto empty field
    If (txtName.Text <> "") And (txtPostCode.Text <> "") And (txtTelNo.Visible = True) Then txtTelNo.SetFocus
    
End Sub

Public Sub HideRefundDetails()
    
    fraOriginalTran.Visible = False
    mblnShowOrigTran = False
    fraAddress.Top = 120
    Call UserControl_Resize
    RaiseEvent Resize

End Sub

Public Sub Reset()

    txtName.Text = vbNullString
    txtAddress(1).Text = vbNullString
    txtAddress(2).Text = vbNullString
    txtAddress(3).Text = vbNullString
    txtPostCode.Text = vbNullString
    txtTelNo.Text = vbNullString
    txtRemarks(0).Text = vbNullString
    txtRemarks(1).Text = vbNullString
    txtRemarks(2).Text = vbNullString
    txtRemarks(3).Text = vbNullString
    txtMobileNo.Text = vbNullString
    txtWorkTelNo.Text = vbNullString
    txtEmail.Text = vbNullString
    lblOrderNo.Caption = vbNullString
    lblDepositValue.Caption = vbNullString
    lblOrderDate.Caption = vbNullString
    lblOrderStatus.Caption = vbNullString
    lblTraderNo.Caption = vbNullString
    
    cmdReset.Visible = False

End Sub

Public Sub Show()
    
    If Not moParent Is Nothing Then
        mblnPreview = moParent.KeyPreview
        moParent.KeyPreview = False
        Call DebugMsg(MODULE_NAME, "Show", endlDebug, "Parent set KeyPreview=" & mblnPreview)
    Else
        Call DebugMsg(MODULE_NAME, "Show", endlDebug, "No Parent to set KeyPreview")
    End If
    If (txtOrderNo.Visible = True) And (txtOrderNo.Enabled = True) Then
        If txtOrderNo.Visible = True Then Call txtOrderNo.SetFocus
    Else
        If (txtName.Visible = True) And (txtName.Enabled = True) Then Call txtName.SetFocus
        'Added 7/4/10 to force focus onto empty field
        If (txtName.Text <> "") And (txtPostCode.Text <> "") And (txtTelNo.Visible = True) Then txtTelNo.SetFocus
    End If

End Sub

Public Sub Initialise(ByRef CurrentSession As ISession, Optional ByRef ParentForm As Object = Nothing)

Dim oBOCol  As Collection
Dim lItemNo As Long
Dim strKey  As String

    If mblnInit = True Then Exit Sub
    ' Set up the object and field that we wish to select on
    mblnForceTelWarning = False
    
    Screen.MousePointer = vbHourglass
    Set moParent = ParentForm
    
    Set goSession = CurrentSession
    Set goDatabase = goSession.Database
    
    Call CreateErrorObject(SYSTEM_NAME & "{" & App.Title & "}", VBA.Err, Err)
    
    UserControl.BackColor = goSession.GetParameter(PRM_QUERY_BORDERCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    
    If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
        lblAddressLbl2.Caption = "Street Address"
        lblAddressLbl3.Caption = "Town"
        lblAddressLbl4.Caption = "County"
        lblAddressLbl2.Visible = True
        lblAddressLbl3.Visible = True
        lblAddressLbl4.Visible = True
        lblPostCodelbl.Visible = False
        txtPostCode.Visible = False
        lblAddressLbl2.Top = lblAddressLbl2.Top - 300
        lblAddressLbl3.Top = lblAddressLbl3.Top - 240
        lblAddressLbl4.Top = lblAddressLbl4.Top - 180
        txtAddress(1).Top = txtAddress(1).Top - 300
        txtAddress(2).Top = txtAddress(2).Top - 240
        txtAddress(3).Top = txtAddress(3).Top - 180
        txtName.Left = 2580
        txtAddress(1).Left = txtName.Left
        txtAddress(2).Left = txtName.Left
        txtAddress(3).Left = txtName.Left
        txtTelNo.Left = txtName.Left
        txtAddress(1).Width = 6435
        txtAddress(2).Width = txtAddress(1).Width
        txtAddress(3).Width = txtAddress(1).Width
    End If
        
    fraOrderDetails.BackColor = RGBQuery_BackColour
    fraOrderFooter.BackColor = RGBQuery_BackColour
    fraAddress.BackColor = RGBQuery_BackColour
    fraButtons.BackColor = RGBQuery_BackColour
    fraOriginalTran.BackColor = RGBQuery_BackColour
    
    strKey = goSession.GetParameter(PRM_KEY_SEARCH)
    If Left$(strKey, 1) = "F" Then
        mlngSearchKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngSearchKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_RESET)
    cmdReset.Caption = strKey & "-" & cmdReset.Caption
    If Left$(strKey, 1) = "F" Then
        mlngResetKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngResetKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_CLOSE)
    cmdCancel.Caption = strKey & "-" & cmdCancel.Caption
    If Left$(strKey, 1) = "F" Then
        mlngCloseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngCloseKeyCode = AscW(UCase$(strKey))
    End If
    
    strKey = goSession.GetParameter(PRM_KEY_SAVE)
    cmdSave.Caption = strKey & "-" & cmdSave.Caption
    If Left$(strKey, 1) = "F" Then
        mlngUseKeyCode = Val(Mid$(strKey, 2)) + vbKeyF1 - 1
    Else
        mlngUseKeyCode = AscW(UCase$(strKey))
    End If
    
    cmdReset.Visible = False
    mblnInit = True
    
    Set frmMatchCode = New frmMatchCode
    Load frmMatchCode
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdCancel_Click()

    If (txtName.Visible = True) Then txtName.SetFocus
    RaiseEvent Cancel
    mblnEntryWaiting = False

End Sub

Private Sub cmdReset_Click()

Dim lngRemarkNo As Long
    
    txtName.Text = vbNullString
    lblTraderNo.Caption = vbNullString
    txtAddress(1).Text = vbNullString
    txtAddress(2).Text = vbNullString
    txtAddress(3).Text = vbNullString
    txtTelNo.Text = vbNullString
    txtPostCode.Text = vbNullString
    lblDepositValue.Caption = vbNullString
    txtMobileNo.Text = vbNullString
    txtWorkTelNo.Text = vbNullString
    txtEmail.Text = vbNullString
    'split up single remarks into seperate lines, marked by Carraige Return
    For lngRemarkNo = 1 To 4 Step 1
        txtRemarks(lngRemarkNo - 1).Text = vbNullString
    Next lngRemarkNo
    txtOrderNo.Text = vbNullString
    lblOrderNo.Caption = vbNullString
    lblOrderDate.Caption = vbNullString
    cmdReset.Visible = False
    If (txtName.Enabled = True) And (txtName.Visible = True) Then
        If txtName.Visible = True Then Call txtName.SetFocus
    End If
    If txtName.Enabled = False Then
        txtOrderNo.Visible = True
        txtOrderNo.SetFocus
        cmdSave.Visible = False
    End If
    mblnPCodeEdited = False

End Sub

Private Sub cmdSave_Click()

Dim strAddress As String

    txtPostCode.Text = UCase(Trim(txtPostCode.Text))
    If mblnEntryWaiting = True Then
        mblnEntryWaiting = False
    Else
        If mblnSelectOrder = False Then
            If (txtPostCode.Visible = True) And (Not txtPostCode.Text Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]") Then
                Call MsgBoxEx("Invalid postcode", vbOKOnly + vbInformation, "Customer Information")
                If txtPostCode.Visible = True Then txtPostCode.SetFocus
                Exit Sub
            End If
            If mblnNameOnly = False Then
                If (LenB(Trim(txtName.Text)) = 0) Then
                    Call MsgBoxEx("Name required", vbOKOnly + vbInformation, "Customer Information")
                    If (LenB(Trim(txtName.Text)) = 0) Then
                        If txtName.Visible = True Then txtName.SetFocus
                    Else
                        If txtAddress(0).Visible = True Then txtAddress(0).SetFocus
                    End If
                    Exit Sub
                End If
                'WIX1381 - QOD Take Now Requires at least one phone number
                If mblnForceTelWarning = True Then
                    If (LenB(Trim(txtTelNo.Text)) = 0) And (LenB(Trim(txtMobileNo.Text)) = 0) And (LenB(Trim(txtWorkTelNo.Text)) = 0) Then
                        Call MsgBoxEx("At least one phone number required.", vbOKOnly + vbInformation, "Customer Information")
                        If txtTelNo.Visible = True Then txtTelNo.SetFocus
                        Exit Sub
                    End If
                End If
                
                If (Len(Trim(txtTelNo.Text)) > 0 And Len(Trim(txtTelNo.Text)) < 11) Then
                        Call MsgBoxEx("Invalid phone number, must include the STD code.", vbOKOnly + vbInformation, "Customer Information")
                        If txtTelNo.Visible = True Then txtTelNo.SetFocus
                        Exit Sub
                End If
                
                If (Len(Trim(txtWorkTelNo.Text)) > 0 And Len(Trim(txtWorkTelNo.Text)) < 11) Then
                        Call MsgBoxEx("Invalid phone number, must include the STD code.", vbOKOnly + vbInformation, "Customer Information")
                        If txtWorkTelNo.Visible = True Then txtWorkTelNo.SetFocus
                        Exit Sub
                End If
                
                If (Len(Trim(txtMobileNo.Text)) > 0 And Len(Trim(txtMobileNo.Text)) < 11) Or Mid(txtMobileNo.Text, 2, 1) <> "7" Then
                    If LenB(Trim(txtMobileNo.Text)) > 0 Then
                        Call MsgBoxEx("Invalid mobile phone number, must have a 7 as second number and be at least 11 numbers in length.", vbOKOnly + vbInformation, "Customer Information")
                        If txtMobileNo.Visible = True Then txtMobileNo.SetFocus
                        Exit Sub
                    End If
                End If
                
                If goSession.ISession_IsOnline = False Or goSession.GetParameter(PRM_MCD_ACTIVE) = False Then
                    If (txtPostCode.Visible = True) And (Not txtPostCode.Text Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]") And (txtPostCode.Visible = True) Then
                        Call MsgBoxEx("Invalid postcode", vbOKOnly + vbInformation, "Customer Information", , , , , RGBMsgBox_WarnColour)
                        If txtPostCode.Visible = True Then txtPostCode.SetFocus
                        Exit Sub
                    End If
                End If
                
                'WIX1380 - Mandatory fields 1(Address1) & 3 (Town) must have data
                If (LenB(Trim(txtAddress(1).Text)) = 0) Then
                    Call MsgBoxEx(lblAddressLbl2.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                    If txtAddress(1).Visible = True Then txtAddress(1).SetFocus
                    Exit Sub
                End If
                
                If (LenB(Trim(txtAddress(3).Text)) = 0) Then
                    Call MsgBoxEx(lblAddressLbl4.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                    If txtAddress(3).Visible = True Then txtAddress(3).SetFocus
                    Exit Sub
                End If
                        
                If ValidEmail(txtEmail.Text) = False And txtEmail.Text <> "" Then
                   Call MsgBoxEx("Invalid email address.", vbOKOnly + vbInformation, "Customer Information")
                   If txtEmail.Visible = True Then txtEmail.SetFocus
                   Exit Sub
                End If
                
                If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
                    If (LenB(Trim(txtAddress(2).Text)) = 0) Then
                        Call MsgBoxEx(lblAddressLbl3.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                        If txtAddress(2).Visible = True Then txtAddress(2).SetFocus
                        Exit Sub
                    End If
                End If
            Else
                If (LenB(Trim(txtName.Text)) = 0) Then
                    Call MsgBoxEx("Name required", vbOKOnly + vbInformation, "Customer Information")
                    If txtName.Visible = True Then txtName.SetFocus
                    Exit Sub
                End If
            End If
            strAddress = txtAddress(1).Text & vbCrLf & txtAddress(2).Text & vbCrLf & txtAddress(3).Text
            If txtName.Visible = True Then txtName.SetFocus
            RaiseEvent Save(txtName.Text, txtPostCode.Text, strAddress, txtTelNo.Text, txtMobileNo.Text, txtWorkTelNo.Text, txtEmail.Text)
        Else
            If txtName.Visible = True Then txtName.SetFocus
            RaiseEvent UseOrder(lblOrderNo.Caption)
        End If
    End If
    mblnSelectOrder = False
    
End Sub

Private Sub frmMatchCode_Click(Address As Variant)

    txtPostCode.Text = Address(3)
    txtAddress(1).Text = Address(0)
    txtAddress(2).Text = Address(1)
    txtAddress(3).Text = Address(2)
End Sub

Private Sub txtEmail_GotFocus()
    txtEmail.SelStart = 0
    txtEmail.SelLength = Len(txtEmail.Text)
    txtEmail.BackColor = RGBEdit_Colour

End Sub

Private Sub txtEmail_KeyPress(KeyAscii As Integer)
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtEmail_LostFocus()
    txtEmail.BackColor = RGB_WHITE
End Sub

Private Sub txtOrderNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        If Len(txtOrderNo.Text) < 6 Then txtOrderNo.Text = Right$("000000" & txtOrderNo.Text, 6)
    End If

End Sub

Private Sub txtPostCode_Change()

Dim strAddress As String
Dim lngAddNo   As Long

    cmdReset.Visible = True
    mblnPCodeEdited = True
    If (mblnEntryWaiting = True) Then
        For lngAddNo = txtAddress.LBound To txtAddress.UBound Step 1
            strAddress = strAddress & txtAddress(lngAddNo).Text
        Next lngAddNo
        If (LenB(txtName.Text) <> 0) And (LenB(txtPostCode.Text) <> 0) And (LenB(Trim$(strAddress)) <> 0) Then
            cmdSave.Visible = True
        Else
            cmdSave.Visible = False
        End If
    End If

End Sub

Private Sub txtPostCode_GotFocus()

    txtPostCode.SelStart = 0
    txtPostCode.SelLength = Len(txtPostCode.Text)
    txtPostCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPostCode_KeyPress(KeyAscii As Integer)
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtPostCode_LostFocus()

    txtPostCode.BackColor = RGB_WHITE
    
    If ((ActiveControl Is Nothing) = False) Then
        If Left$(ActiveControl.Name, 3) <> "cmd" Then
            If (LenB(Trim$(txtPostCode.Text)) <> 0) And (goSession.ISession_IsOnline) Then
                If goSession.GetParameter(PRM_MCD_ACTIVE) And (mblnPCodeEdited = True) Then
                    mblnPCodeEdited = False
                    Call frmMatchCode.GetAddress(txtPostCode.Text)
                    If frmMatchCode.Cancel Then
                        If (txtPostCode.Visible = True) Then txtPostCode.SetFocus
                        mblnPCodeEdited = False
                        Exit Sub
                    ElseIf frmMatchCode.NoMatches Then
                        If (txtPostCode.Visible = True) Then txtPostCode.SetFocus
                        Exit Sub
                    Else
                        If (txtTelNo.Visible = True) Then txtTelNo.SetFocus
                    End If
                End If 'Post Code has been edited and Look Up enabled
            End If
        End If
    End If

End Sub

Private Sub txtRemarks_GotFocus(Index As Integer)
    
    txtRemarks(Index).SelStart = 0
    txtRemarks(Index).SelLength = Len(txtRemarks(Index).Text)
    txtRemarks(Index).BackColor = RGBEdit_Colour

End Sub

Private Sub txtRemarks_KeyPress(Index As Integer, KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtRemarks_LostFocus(Index As Integer)

    txtRemarks(Index).BackColor = RGB_WHITE

End Sub

Private Sub txtTelNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtTelNo_GotFocus()

    txtTelNo.SelStart = 0
    txtTelNo.SelLength = Len(txtTelNo.Text)
    txtTelNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtTelNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    Select Case KeyAscii
        Case 48 To 57
        Case 32
        Case 8
        
        Case Else
            KeyAscii = 0
    End Select

End Sub

Private Sub txtTelNo_LostFocus()

    txtTelNo.BackColor = RGB_WHITE
    
End Sub

Private Sub txtMobileNo_Change()

    cmdReset.Visible = True

End Sub

Private Sub txtMobileNo_GotFocus()

    txtMobileNo.SelStart = 0
    txtMobileNo.SelLength = Len(txtMobileNo.Text)
    txtMobileNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtMobileNo_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If
    
    Select Case KeyAscii
        Case 48 To 57
        Case 32
        Case 8
        
        Case Else
            KeyAscii = 0
    End Select
End Sub

Private Sub txtMobileNo_LostFocus()

    txtMobileNo.BackColor = RGB_WHITE
    
End Sub

Private Sub txtName_Change()

Dim strAddress As String
Dim lngAddNo   As Long

    cmdReset.Visible = True
    If (mblnEntryWaiting = True) Then
        For lngAddNo = txtAddress.LBound To txtAddress.UBound Step 1
            strAddress = strAddress & txtAddress(lngAddNo).Text
        Next lngAddNo
        If (LenB(txtName.Text) <> 0) And (LenB(txtPostCode.Text) <> 0) And (LenB(Trim$(strAddress)) <> 0) Then
            cmdSave.Visible = True
            If txtTelNo = True Then txtTelNo.SetFocus
            Exit Sub
        Else
            cmdSave.Visible = False
        End If
    End If
    
End Sub

Private Sub txtName_GotFocus()

    txtName.SelStart = 0
    txtName.SelLength = Len(txtName.Text)
    txtName.BackColor = RGBEdit_Colour

End Sub

Private Sub txtName_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtName_LostFocus()

    txtName.BackColor = RGB_WHITE
    
End Sub

Private Sub txtAddress_Change(Index As Integer)

Dim strAddress As String
Dim lngAddNo   As Long

    cmdReset.Visible = True
    If (mblnEntryWaiting = True) Then
        For lngAddNo = txtAddress.LBound To txtAddress.UBound Step 1
            strAddress = strAddress & txtAddress(lngAddNo).Text
        Next lngAddNo
        If (LenB(txtName.Text) <> 0) And (LenB(txtPostCode.Text) <> 0) And (LenB(Trim$(strAddress)) <> 0) Then
            cmdSave.Visible = True
        Else
            cmdSave.Visible = False
        End If
    End If

End Sub

Private Sub txtAddress_GotFocus(Index As Integer)

    txtAddress(Index).SelStart = 0
    txtAddress(Index).SelLength = Len(txtAddress(Index).Text)
    txtAddress(Index).BackColor = RGBEdit_Colour

End Sub

Private Sub txtAddress_KeyPress(Index As Integer, KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtAddress_LostFocus(Index As Integer)

    txtAddress(Index).BackColor = RGB_WHITE
    If Index = 0 Then
        If LenB(Trim(txtPostCode.Text)) <> 0 And LenB(Trim(txtAddress(1).Text)) <> 0 Then
            If txtTelNo.Visible = True Then txtTelNo.SetFocus
        End If
    End If
End Sub

Private Sub txtWorkTelNo_GotFocus()
    txtWorkTelNo.SelStart = 0
    txtWorkTelNo.SelLength = Len(txtWorkTelNo.Text)
    txtWorkTelNo.BackColor = RGBEdit_Colour
End Sub

Private Sub txtWorkTelNo_KeyPress(KeyAscii As Integer)
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    Select Case KeyAscii
        Case 48 To 57
        Case 32
        Case 8
        
        Case Else
            KeyAscii = 0
    End Select

End Sub

Private Sub txtWorkTelNo_LostFocus()
    txtWorkTelNo.BackColor = RGB_WHITE
End Sub

Private Sub UserControl_Hide()
    
    Call DebugMsg(MODULE_NAME, "UC_Hide", endlDebug, "Resetting preview")
    If Not moParent Is Nothing Then moParent.KeyPreview = mblnPreview

End Sub

Private Sub UserControl_Initialize()

    Call DebugMsg(MODULE_NAME, "UC_Initialize", endlTraceIn)
    fraButtons.Top = txtTelNo.Top + txtTelNo.Height + fraAddress.Top + 120
    Call DebugMsg(MODULE_NAME, "UC_Initialize", endlTraceOut)

End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (KeyCode)
        Case (mlngResetKeyCode):
            If cmdReset.Visible Then Call cmdReset_Click
        Case (mlngCloseKeyCode):
            If cmdCancel.Visible Then Call cmdCancel_Click
        Case (mlngUseKeyCode):
            If cmdSave.Visible Then Call cmdSave_Click
        Case vbKeyTab
            If Shift = 0 Then
                FirstActiveControl.SetFocus
            ElseIf Shift = 1 Then
                LastActiveControl.SetFocus
            End If
            KeyCode = 0
    End Select

End Sub

Private Sub UserControl_Resize()

    UserControl.Width = 9510
    
    If mblnShowOrder = True Then UserControl.Height = fraAddress.Height + fraOrderDetails.Height + fraOrderFooter.Height + 240
    If mblnShowOrigTran = True Then UserControl.Height = fraAddress.Height + fraOriginalTran.Height + 240
    If (mblnShowOrigTran = False) And (mblnShowOrder = False) Then UserControl.Height = fraAddress.Height + 240

End Sub

Private Function FirstActiveControl() As Control

Dim lowTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    lowTab = -1
    
    On Error Resume Next
    For Each Ctrl In Controls
        If Ctrl.Visible = True And Ctrl.Enabled = True And Ctrl.TabStop = True Then
            If Err.Number = 0 Then
                If (Ctrl.TabIndex < lowTab) Or (lowTab = -1) Then
                    lowTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                End If
            End If
            Err.Clear
        End If
    Next Ctrl
    
    On Error GoTo 0
    Set FirstActiveControl = retCtrl
    
End Function

Private Function LastActiveControl() As Control

Dim highTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    highTab = -1
    
    On Error Resume Next
    For Each Ctrl In Controls
        If Ctrl.Visible = True And Ctrl.Enabled = True And Ctrl.TabStop = True Then
            If Err.Number = 0 Then
                If (Ctrl.TabIndex > highTab) Or (highTab = -1) Then
                    highTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                End If
            End If
            Err.Clear
        End If
    Next Ctrl
    
    On Error GoTo 0
    Set LastActiveControl = retCtrl
    
End Function

Private Function ValidCustomerData() As Boolean
    
    If mblnSelectOrder = False Then
        If (txtPostCode.Visible = True) And (txtPostCode.Text <> "") And (Not txtPostCode.Text Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]") Then
            Call MsgBoxEx("Invalid postcode", vbOKOnly + vbInformation, "Customer Information")
            If txtPostCode.Visible = True Then txtPostCode.SetFocus
            ValidCustomerData = False
            Exit Function
        End If
        If mblnNameOnly = False Then
            If (LenB(Trim(txtName.Text)) = 0) Then
                Call MsgBoxEx("Name required", vbOKOnly + vbInformation, "Customer Information")
                If (LenB(Trim(txtName.Text)) = 0) Then
                    If txtName.Visible = True Then txtName.SetFocus
                End If
                ValidCustomerData = False
                Exit Function
            End If
            If goSession.ISession_IsOnline = False Or goSession.GetParameter(PRM_MCD_ACTIVE) = False Then
                If (txtPostCode.Visible = True) And (Not txtPostCode.Text Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]") And (txtPostCode.Visible = True) Then
                    Call MsgBoxEx("Invalid postcode", vbOKOnly + vbInformation, "Customer Information", , , , , RGBMsgBox_WarnColour)
                    If txtPostCode.Visible = True Then txtPostCode.SetFocus
                    ValidCustomerData = False
                    Exit Function
                End If
            End If
            If (goSession.GetParameter(PRM_COUNTRY_CODE) = "IE") Then
                If (LenB(Trim(txtAddress(1).Text)) = 0) Then
                    Call MsgBoxEx(lblAddressLbl2.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                    If txtAddress(1).Visible = True Then txtAddress(1).SetFocus
                    ValidCustomerData = False
                    Exit Function
                End If
                If (LenB(Trim(txtAddress(2).Text)) = 0) Then
                    Call MsgBoxEx(lblAddressLbl3.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                    If txtAddress(2).Visible = True Then txtAddress(2).SetFocus
                    ValidCustomerData = False
                    Exit Function
                End If
                If (LenB(Trim(txtAddress(3).Text)) = 0) Then
                    Call MsgBoxEx(lblAddressLbl4.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                    If txtAddress(3).Visible = True Then txtAddress(3).SetFocus
                    ValidCustomerData = False
                    Exit Function
                End If
            End If
        Else
            If (LenB(Trim(txtName.Text)) = 0) Then
                Call MsgBoxEx(lblName.Caption & " required", vbOKOnly + vbInformation, "Customer Information")
                If txtName.Visible = True Then txtName.SetFocus
                ValidCustomerData = False
                Exit Function
            End If
        End If
        
        If txtName.Visible = True Then txtName.SetFocus
    End If
    ValidCustomerData = True
End Function

Function ValidEmail(ByVal strCheck As String) As Boolean

Dim bCK As Boolean
Dim strDomainType As String
Dim strDomainName As String
Const sInvalidChars As String = "!#$%^&*()=+{}[]|\;:'/?>,< "
Dim i As Integer

    bCK = Not InStr(1, strCheck, Chr(34)) > 0 'Check to see if there is a double quote
    If Not bCK Then GoTo ExitFunction
    
    bCK = Not InStr(1, strCheck, "..") > 0 'Check to see if there are consecutive dots
    If Not bCK Then GoTo ExitFunction
    
    ' Check for invalid characters.
    If Len(strCheck) > Len(sInvalidChars) Then
        For i = 1 To Len(sInvalidChars)
            If InStr(strCheck, Mid(sInvalidChars, i, 1)) > 0 Then
                bCK = False
                GoTo ExitFunction
            End If
        Next
    Else
        For i = 1 To Len(strCheck)
            If InStr(sInvalidChars, Mid(strCheck, i, 1)) > 0 Then
                bCK = False
                GoTo ExitFunction
            End If
        Next
    End If
    
    If InStr(1, strCheck, "@") > 1 Then 'Check for an @ symbol
        bCK = Len(Left(strCheck, InStr(1, strCheck, "@") - 1)) > 0
    Else
        bCK = False
    End If
    If Not bCK Then GoTo ExitFunction
    
    strCheck = Right(strCheck, Len(strCheck) - InStr(1, strCheck, "@"))
    bCK = Not InStr(1, strCheck, "@") > 0 'Check to see if there are too many @'s
    If Not bCK Then GoTo ExitFunction
    
    strDomainType = Right(strCheck, Len(strCheck) - InStr(1, strCheck, "."))
    bCK = Len(strDomainType) > 0 And InStr(1, strCheck, ".") < Len(strCheck)
    If Not bCK Then GoTo ExitFunction
    
    If Len(strCheck) - Len(strDomainType) - 1 > 0 Then
        strCheck = Left(strCheck, Len(strCheck) - Len(strDomainType) - 1)
        Do Until InStr(1, strCheck, ".") <= 1
            If Len(strCheck) >= InStr(1, strCheck, ".") Then
                strCheck = Left(strCheck, Len(strCheck) - (InStr(1, strCheck, ".") - 1))
            Else
                bCK = False
                GoTo ExitFunction
            End If
        Loop
        If strCheck = "." Or Len(strCheck) = 0 Then bCK = False
    Else
        bCK = False
    End If

ExitFunction:
    ValidEmail = bCK
End Function

Public Function LoadData(strName, strAddress1, strAddress2, strAddress3, strAddress4, strPostCode, strTel, strMobile, strWorkTel, strEmail)
    txtName.Text = strName
    txtPostCode.Text = strPostCode
    txtAddress(1).Text = strAddress2
    txtAddress(2).Text = strAddress3
    txtAddress(3).Text = strAddress4
    txtPostCode.Text = strPostCode
    txtTelNo.Text = strTel
    txtMobileNo.Text = strMobile
    txtWorkTelNo.Text = strWorkTel
    txtEmail.Text = strEmail
    'Me.Refresh
    DoEvents
End Function

Public Sub SetFocusFirst()

    Call DebugMsg(MODULE_NAME, "SetFocusFirst:" & (txtName.Text <> "") & (txtPostCode.Text <> "") & (txtTelNo.Visible = True), endlTraceIn)
    If (txtName.Text <> "") And (txtPostCode.Text <> "") And (txtTelNo.Visible = True) Then txtTelNo.SetFocus

End Sub


