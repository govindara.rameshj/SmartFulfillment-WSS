VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cReturnHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D538B510208"
'<CAMH>****************************************************************************************
'* Module : cReturnHeader
'* Date   : 09/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/ReturnsBO/ReturnHeader.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Item Return Header
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 25/07/03 12:26 $ $Revision: 5 $
'* Versions:
'* 09/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cReturnHeader"

Implements IBo
Implements ISysBo

'##ModelId=3D538B6F01FE
Private mReturnID As Long

'##ModelId=3D538B7C035C
Private mSupplierReturnNo As String

'##ModelId=3D538B9301EA
Private mSupplierNo As String

'##ModelId=3D538B9803A2
Private mDateCreated As Date

'##ModelId=3D538BA00348
Private mCollectionDate As Date

'##ModelId=3D538BA80280
Private mPurchaseOrderNo As String

'##ModelId=3D538BB4030C
Private mDRLNo As String

'##ModelId=3D538BBA00D2
Private mReturnValue As Currency

'##ModelId=3D538BBE0302
Private mPrinted As Boolean

Private m_oSession As Session

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private mcolLines As Collection

Private mGetLines As Boolean

Private mastrUpdatedLines() As String

Public Function AddItem(PartCode As String) As cReturnLine

Dim oLine As cReturnLine

    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_RETURNLINE)
    oLine.PartCode = PartCode
    
    If mcolLines Is Nothing Then Set mcolLines = New Collection
    Call mcolLines.Add(oLine)
    oLine.LineNo = mcolLines.Count
    Set AddItem = oLine

End Function
Public Function Lines() As Collection

    If (mcolLines Is Nothing) And (mGetLines = True) Then
        Call RetrieveLines
    End If
    Set Lines = mcolLines
    
End Function
Public Property Get GetLines() As Boolean
   Let GetLines = mGetLines
End Property

Public Property Let GetLines(ByVal Value As Boolean)
    Let mGetLines = Value
End Property
Private Sub RetrieveLines()

Dim oLine As cReturnLine

    'If Order found then get lines for Order
    Set mcolLines = New Collection
    Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_RETURNLINE)
    Call oLine.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNLINE_ReturnID, mReturnID)
    Call oLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_RETURNLINE_LineNo, 0)
    Set mcolLines = oLine.LoadLines

End Sub

'##ModelId=3D5B6BD4029E
Public Property Get ReturnValue() As Currency
   Let ReturnValue = mReturnValue
End Property

'##ModelId=3D5B6BD40190
Public Property Let ReturnValue(ByVal Value As Currency)
    Let mReturnValue = Value
End Property

'##ModelId=3D5B6BD400E6
Public Property Get DRLNo() As String
   Let DRLNo = mDRLNo
End Property

'##ModelId=3D5B6BD303C0
Public Property Let DRLNo(ByVal Value As String)
    Let mDRLNo = Value
End Property

'##ModelId=3D5B6BD30316
Public Property Get PurchaseOrderNo() As String
   Let PurchaseOrderNo = mPurchaseOrderNo
End Property

'##ModelId=3D5B6BD30208
Public Property Let PurchaseOrderNo(ByVal Value As String)
    Let mPurchaseOrderNo = Value
End Property

'##ModelId=3D5B6BD3015E
Public Property Get CollectionDate() As Date
   Let CollectionDate = mCollectionDate
End Property

'##ModelId=3D5B6BD30082
Public Property Let CollectionDate(ByVal Value As Date)
    Let mCollectionDate = Value
End Property

'##ModelId=3D5B6BD203CA
Public Property Get DateCreated() As Date
   Let DateCreated = mDateCreated
End Property

'##ModelId=3D5B6BD202EE
Public Property Let DateCreated(ByVal Value As Date)
    Let mDateCreated = Value
End Property

'##ModelId=3D5B6BD20280
Public Property Get SupplierNo() As String
   Let SupplierNo = mSupplierNo
End Property

'##ModelId=3D5B6BD201A4
Public Property Let SupplierNo(ByVal Value As String)
    Let mSupplierNo = Value
End Property

'##ModelId=3D5B6BD20104
Public Property Get SupplierReturnNo() As String
   Let SupplierReturnNo = mSupplierReturnNo
End Property

'##ModelId=3D5B6BD2005A
Public Property Let SupplierReturnNo(ByVal Value As String)
    Let mSupplierReturnNo = Value
End Property

'##ModelId=3D5B6BD103A2
Public Property Get ReturnID() As Long
   Let ReturnID = mReturnID
End Property


'##ModelId=3D5B6BD102F8
Public Property Let ReturnID(ByVal Value As Long)
    Let mReturnID = Value
End Property
'##ModelId=3D5B6BD4037A
Public Property Let Printed(ByVal Value As Boolean)
    Let mPrinted = Value
End Property

'##ModelId=3D5B6BD500AA
Public Property Get Printed() As Boolean
   Let Printed = mPrinted
End Property
'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_RETURNHDR_ReturnID): mReturnID = oField.ValueAsVariant
            Case (FID_RETURNHDR_SupplierReturnNo): mSupplierReturnNo = oField.ValueAsVariant
            Case (FID_RETURNHDR_SupplierNo): mSupplierNo = oField.ValueAsVariant
            Case (FID_RETURNHDR_DateCreated): mDateCreated = oField.ValueAsVariant
            Case (FID_RETURNHDR_CollectionDate): mCollectionDate = oField.ValueAsVariant
            Case (FID_RETURNHDR_PurchaseOrderNo): mPurchaseOrderNo = oField.ValueAsVariant
            Case (FID_RETURNHDR_DRLNo): mDRLNo = oField.ValueAsVariant
            Case (FID_RETURNHDR_ReturnValue): mReturnValue = oField.ValueAsVariant
            Case (FID_RETURNHDR_Printed): mPrinted = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Sub Class_Initialize()
    
    ReDim mastrUpdatedLines(0)

End Sub

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Returns No - " & mReturnID

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cReturnHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_RETURNHDR

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_RETURNHDR, FID_RETURNHDR_END_OF_STATIC, m_oSession)

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function
Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView As IView
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function
Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set IBo_LoadMatches = colBO

End Function



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter
Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cReturnHeader

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D3B9501D6
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oSysNum    As cSystemNumbers
Dim lngLineNo  As Long
Dim oRetLine   As cReturnLine
    
    Save = False
    
    If IsValid() Then
        If eSave = SaveTypeIfNew Then
            'Get Next Supplier Return Number
            Set oSysNum = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMNO)
            mSupplierReturnNo = oSysNum.GetNewSupplierReturnNumber
            Set oSysNum = Nothing
        End If
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        If eSave = SaveTypeIfExists Then
            'First save all edited line to the database
            For lngLineNo = 1 To UBound(mastrUpdatedLines) Step 1
                Set oRetLine = mcolLines(CLng(mastrUpdatedLines(lngLineNo)))
                Call oRetLine.IBo_SaveIfExists
            Next lngLineNo
        End If
    End If

End Function
Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_RETURNHDR * &H10000) + 1 To FID_RETURNHDR_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function



Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_RETURNHDR_ReturnID):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReturnID
        Case (FID_RETURNHDR_SupplierReturnNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSupplierReturnNo
        Case (FID_RETURNHDR_SupplierNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSupplierNo
        Case (FID_RETURNHDR_DateCreated):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateCreated
        Case (FID_RETURNHDR_CollectionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mCollectionDate
        Case (FID_RETURNHDR_PurchaseOrderNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPurchaseOrderNo
        Case (FID_RETURNHDR_DRLNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDRLNo
        Case (FID_RETURNHDR_ReturnValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mReturnValue
        Case (FID_RETURNHDR_Printed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPrinted
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cReturnHeader
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_RETURNHDR_END_OF_STATIC

End Function

Public Function UpdateItemQuantity(lngLineNo As Long, strPartCode As String, dblQuantity As Double, strReasonCode As String, strComment As String) As Boolean

Dim lngItemNo As Long
Dim oLine     As cReturnLine

    UpdateItemQuantity = False
    If mcolLines Is Nothing Then Exit Function
    'Find selected line in collection of lines
    For lngItemNo = 1 To mcolLines.Count Step 1
        If (mcolLines(lngItemNo).LineNo = lngLineNo) And (mcolLines(lngItemNo).PartCode = strPartCode) Then
            Set oLine = mcolLines(lngItemNo)
            Exit For
        End If
    Next lngItemNo
    'check that line was found
    If oLine Is Nothing Then Exit Function
    'add updated line to list of lines to be updated to database
    ReDim Preserve mastrUpdatedLines(UBound(mastrUpdatedLines) + 1)
    lngItemNo = UBound(mastrUpdatedLines)
    mastrUpdatedLines(lngItemNo) = lngLineNo
    'set quantity received to 0 to reverse out delivery
    oLine.QuantityReturned = dblQuantity
    oLine.ReturnCode = Val(strReasonCode)
    oLine.ReturnReason = strComment
    UpdateItemQuantity = True
    
End Function


