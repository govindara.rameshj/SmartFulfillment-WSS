VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cReturnNoteLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3F0E8B5C01DD"
'<CAMH>****************************************************************************************
'* Module: cReturnNoteLine
'* Date  : 15/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/ReturnsBO/ReturnNoteLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Delivery
'Note Line
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 20/10/03 9:19 $ $Revision: 5 $
'* Versions:
'* 15/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

'##ModelId=3F0E8B5C0238
Const MODULE_NAME As String = "cReturnNoteLine"

Implements IBo
Implements ISysBo

'##ModelId=3F0E8B5C0269
Private m_oSession As Session
'##ModelId=3F0E8B5C0291
Private mRetNoteKey As String

'##ModelId=3F0E8B5C02C3
Private mLineNo As String

'##ModelId=3F0E8B5C02F5
Private mPartCode As String

'##ModelId=3F0E8B5C0328
Private mPartCodeAlphaCode As String

'##ModelId=3F0E8B5C035A
Private mOrderQty As Long

'##ModelId=3F0E8B5C038C
Private mReturnedQty As Long

'##ModelId=3F0E8B5C03BE
Private mOrderPrice As Currency

'##ModelId=3F0E8B5D0008
Private mBlanketDiscDesc As String

'##ModelId=3F0E8B5D0044
Private mBlanketPartCode As String

'##ModelId=3F0E8B5D0076
Private mCurrentPrice As Currency

'##ModelId=3F0E8B5D00B2
Private mCurrentCost As Double

'##ModelId=3F0E8B5D00EE
Private mIBTQuantity As Long

'##ModelId=3F0E8B5D012A
Private mReturnQty As Long

'##ModelId=3F0E8B5D0166
Private mToFollowQty As Long

'##ModelId=3F0E8B5D01A2
Private mReturnReasonCode As String

'##ModelId=3F0E8B5D01DE
Private mPOLineNumber As Long

'##ModelId=3F0E8B5D021B
Private mChecked As Boolean

'##ModelId=3F0E8B5D0257
Private mSinglesSellingUnit As Currency

'##ModelId=3F0E8B5D029D
Private mNarrative As String

'##ModelId=3F0E8B5D02E3
Private mPreviousQty As Double 'used to hold del qty if updating entry

'##ModelId=3F0E8B5D0329
Private moRowSel As IRowSelector

'##ModelId=3F0E8B5D036F
Public ReturnNote As cReturnNote

'##ModelId=3F0E8B5D0383
Private moLoadRow As IRowSelector

'##ModelId=3F0E8B5D03C9
Private mintNarrStatus As Integer 'flag if narrative edited - -1 (Not Loaded), 0=Okay and 1 = Edited

Public DelNote As cDeliveryNote 'provide reference to Delivery Note for updating Shortages/Returns if set

'##ModelId=3F0E8B5E0027
Const NOT_LOADED As Long = -1
'##ModelId=3F0E8B5E0077
Const LOADED As Long = 0
'##ModelId=3F0E8B5E00C8
Const EDITED As Long = 1

'##ModelId=3F0E8B5E0118
Public Sub ReceiveQuantity()
End Sub

'##ModelId=3F0E8B5E01CC
Public Property Get Narrative() As String

Dim oDRLNarr As Object

    If mintNarrStatus = NOT_LOADED Then
        Set oDRLNarr = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_StoreNumber, m_oSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_DocumentNumber, mRetNoteKey)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_PartCode, mPartCode)
        Call oDRLNarr.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_SequenceNumber, mLineNo)
        Call oDRLNarr.IBo_LoadMatches
        mNarrative = oDRLNarr.NarrativeText1
        If LenB(oDRLNarr.NarrativeText2) <> 0 Then mNarrative = mNarrative & vbCrLf & oDRLNarr.NarrativeText2
        mintNarrStatus = LOADED
        Set oDRLNarr = Nothing
    End If
    Narrative = mNarrative

End Property

'##ModelId=3F0E8B5E0136
Public Property Let Narrative(ByVal Value As String)
    
    If mNarrative <> Value Then
        mNarrative = Value
        mintNarrStatus = EDITED
    End If

End Property

'##ModelId=3F0E8B5E02B2
Public Property Get SinglesSellingUnit() As Currency
   Let SinglesSellingUnit = mSinglesSellingUnit
End Property

'##ModelId=3F0E8B5E021C
Public Property Let SinglesSellingUnit(ByVal Value As Currency)
    Let mSinglesSellingUnit = Value
End Property

'##ModelId=3F0E8B5E03A3
Public Property Get Checked() As Boolean
   Let Checked = mChecked
End Property

'##ModelId=3F0E8B5E030C
Public Property Let Checked(ByVal Value As Boolean)
    Let mChecked = Value
End Property

'##ModelId=3F0E8B5F00AB
Public Property Get POLineNumber() As Long
   Let POLineNumber = mPOLineNumber
End Property

'##ModelId=3F0E8B5F000B
Public Property Let POLineNumber(ByVal Value As Long)
    Let mPOLineNumber = Value
End Property

'##ModelId=3F0E8B5F019B
Public Property Get ReturnReasonCode() As String
   Let ReturnReasonCode = mReturnReasonCode
End Property

'##ModelId=3F0E8B5F00FB
Public Property Let ReturnReasonCode(ByVal Value As String)
    Let mReturnReasonCode = Value
End Property

'##ModelId=3F0E8B5F0296
Public Property Get ToFollowQty() As Long
   Let ToFollowQty = mToFollowQty
End Property

'##ModelId=3F0E8B5F01F5
Public Property Let ToFollowQty(ByVal Value As Long)
    Let mToFollowQty = Value
End Property

'##ModelId=3F0E8B5F0390
Public Property Get ReturnQty() As Long
   Let ReturnQty = mReturnQty
End Property

'##ModelId=3F0E8B5F02F0
Public Property Let ReturnQty(ByVal Value As Long)
    mPreviousQty = mReturnQty
    Let mReturnQty = Value
End Property

'##ModelId=3F0E8B6000B6
Public Property Get IBTQuantity() As Long
   Let IBTQuantity = mIBTQuantity
End Property

'##ModelId=3F0E8B600002
Public Property Let IBTQuantity(ByVal Value As Long)
    Let mIBTQuantity = Value
End Property

'##ModelId=3F0E8B6001BB
Public Property Get CurrentCost() As Double
   Let CurrentCost = mCurrentCost
End Property

'##ModelId=3F0E8B600110
Public Property Let CurrentCost(ByVal Value As Double)
    Let mCurrentCost = Value
End Property

'##ModelId=3F0E8B6002C9
Public Property Get CurrentPrice() As Currency
   Let CurrentPrice = mCurrentPrice
End Property

'##ModelId=3F0E8B600215
Public Property Let CurrentPrice(ByVal Value As Currency)
    Let mCurrentPrice = Value
End Property

'##ModelId=3F0E8B6003E2
Public Property Get BlanketPartCode() As String
   Let BlanketPartCode = mBlanketPartCode
End Property

'##ModelId=3F0E8B600323
Public Property Let BlanketPartCode(ByVal Value As String)
    Let mBlanketPartCode = Value
End Property

'##ModelId=3F0E8B610112
Public Property Get BlanketDiscDesc() As String
   Let BlanketDiscDesc = mBlanketDiscDesc
End Property

'##ModelId=3F0E8B610054
Public Property Let BlanketDiscDesc(ByVal Value As String)
    Let mBlanketDiscDesc = Value
End Property

'##ModelId=3F0E8B61023E
Public Property Get OrderPrice() As Currency
   Let OrderPrice = mOrderPrice
End Property

'##ModelId=3F0E8B610180
Public Property Let OrderPrice(ByVal Value As Currency)
    Let mOrderPrice = Value
End Property

'##ModelId=3F0E8B610361
Public Property Get ReturnedQty() As Long
   Let ReturnedQty = mReturnedQty
End Property

'##ModelId=3F0E8B6102A3
Public Property Let ReturnedQty(ByVal Value As Long)
    Let mReturnedQty = Value
End Property

'##ModelId=3F0E8B6200A5
Public Property Get OrderQty() As Long
   Let OrderQty = mOrderQty
End Property

'##ModelId=3F0E8B6103C5
Public Property Let OrderQty(ByVal Value As Long)
    Let mOrderQty = Value
End Property

'##ModelId=3F0E8B6201D2
Public Property Get PartCodeAlphaCode() As String
   Let PartCodeAlphaCode = mPartCodeAlphaCode
End Property

'##ModelId=3F0E8B620109
Public Property Let PartCodeAlphaCode(ByVal Value As String)
    Let mPartCodeAlphaCode = Value
End Property

'##ModelId=3F0E8B620308
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3F0E8B620240
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3F0E8B630057
Public Property Get LineNo() As String
   Let LineNo = mLineNo
End Property

'##ModelId=3F0E8B620376
Public Property Let LineNo(ByVal Value As String)
    Let mLineNo = Format$(Val(Value), "0000")
End Property

'##ModelId=3F0E8B630197
Public Property Get RetNoteKey() As String
   Let RetNoteKey = mRetNoteKey
End Property


'##ModelId=3F0E8B6300C5
Public Property Let RetNoteKey(ByVal Value As String)
    Let mRetNoteKey = Value
End Property

'<CACH>*************************************************************************-
'***************
'* Function:  Boolean LoadFromRow()
'******************************************************************************-
'****************
'* Description: Part of the IBo interface called by the DB object to pass in a
'row of field and
'*              for this object to extract the data and populate the
'properties of the object.
'******************************************************************************-
'****************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'******************************************************************************-
'****************
'* Returns:  Boolean
'******************************************************************************-
'****************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***********************************************************************-
'****************
'##ModelId=3F0E8B630205
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_RETURNNOTELINE_RetNoteKey):      mRetNoteKey = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_LineNo):          mLineNo = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_PartCode):        mPartCode = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_PartCodeAlphaCode): mPartCodeAlphaCode = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_OrderQty):        mOrderQty = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_ReturnedQty):     mReturnedQty = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_OrderPrice):      mOrderPrice = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_BlanketDiscDesc): mBlanketDiscDesc = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_BlanketPartCode): mBlanketPartCode = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_CurrentPrice):    mCurrentPrice = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_CurrentCost):     mCurrentCost = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_IBTQuantity):     mIBTQuantity = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_ReturnQty):       mReturnQty = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_ToFollowQty):     mToFollowQty = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_ReturnReasonCode): mReturnReasonCode = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_POLineNumber):    mPOLineNumber = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_Checked):         mChecked = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_SinglesSellingUnit): mSinglesSellingUnit = oField.ValueAsVariant
            Case (FID_RETURNNOTELINE_Narrative):       mNarrative = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



'##ModelId=3F0E8B6302A5
Private Sub Class_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)
    mintNarrStatus = NOT_LOADED

End Sub

'##ModelId=3F0E8B6302F6
Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cReturnNoteLine

End Function

'##ModelId=3F0E8B630346
Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Return Note Line " & mRetNoteKey & " Line " & mLineNo

End Property

'##ModelId=3F0E8B6303DC
Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

'##ModelId=3F0E8B6400B2
Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

'##ModelId=3F0E8B64010C
Private Function Initialise(oSession As ISession) As cReturnNoteLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function
'##ModelId=3F0E8B6401CB
Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

'##ModelId=3F0E8B640289
Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_RETURNNOTELINE

End Property

'##ModelId=3F0E8B64030B
Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

'##ModelId=3F0E8B640379
Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function
'##ModelId=3F0E8B6403B5
Private Function Load() As Boolean
    
Dim oView As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
        
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count >= 1 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

'##ModelId=3F0E8B650027
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

'##ModelId=3F0E8B6500F0
Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

'##ModelId=3F0E8B650186
Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    Select Case (lFieldID)
        Case (FID_RETURNNOTELINE_RetNoteKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRetNoteKey
        Case (FID_RETURNNOTELINE_LineNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLineNo
        Case (FID_RETURNNOTELINE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_RETURNNOTELINE_PartCodeAlphaCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCodeAlphaCode
        Case (FID_RETURNNOTELINE_OrderQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrderQty
        Case (FID_RETURNNOTELINE_ReturnedQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReturnedQty
        Case (FID_RETURNNOTELINE_OrderPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrderPrice
        Case (FID_RETURNNOTELINE_BlanketDiscDesc):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBlanketDiscDesc
        Case (FID_RETURNNOTELINE_BlanketPartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mBlanketPartCode
        Case (FID_RETURNNOTELINE_CurrentPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCurrentPrice
        Case (FID_RETURNNOTELINE_CurrentCost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCurrentCost
        Case (FID_RETURNNOTELINE_IBTQuantity):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mIBTQuantity
        Case (FID_RETURNNOTELINE_ReturnQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReturnQty
        Case (FID_RETURNNOTELINE_ToFollowQty):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mToFollowQty
        Case (FID_RETURNNOTELINE_ReturnReasonCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReturnReasonCode
        Case (FID_RETURNNOTELINE_POLineNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mPOLineNumber
        Case (FID_RETURNNOTELINE_Checked):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mChecked
        Case (FID_RETURNNOTELINE_SinglesSellingUnit):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mSinglesSellingUnit
        Case (FID_RETURNNOTELINE_Narrative):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mNarrative
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID

End Function

'##ModelId=3F0E8B650258
Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_RETURNNOTELINE, FID_RETURNNOTELINE_END_OF_STATIC, m_oSession)

End Function

'##ModelId=3F0E8B6502B2
Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

'##ModelId=3F0E8B6502EE
Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

'##ModelId=3F0E8B650334
Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

'##ModelId=3F0E8B650371
Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

'##ModelId=3F0E8B6503AD
Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3F0E8B66000B
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oStockLog    As cStockLog
Dim oStockMove   As cStockMovement
Dim oNarrLine    As Object
Dim dblSellUnits As Double
Dim dblAdjustQty As Double
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
        If eSave = SaveTypeIfExists Then 'reflect just the adjustment quantity
            dblAdjustQty = mReturnQty - mPreviousQty
        Else
            dblAdjustQty = mReturnQty
        End If
        'Update delivery note with Shortage or Return, if Del Note set
        If LenB(ReturnNote.OriginalDelNoteDRL) <> 0 Then
            If (DelNote Is Nothing) = True Then
                Set DelNote = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTEHEADER)
                Call DelNote.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTEHEADER_DocNo, ReturnNote.OriginalDelNoteDRL)
                DelNote.GetLines = True
                Call DelNote.IBo_LoadMatches
            End If
            If (DelNote Is Nothing) = False Then
                If ReturnNote.ShortageNote = True Then
                    Call DelNote.RecordShortage(mPartCode, mReturnedQty, mPOLineNumber)
                Else
                    Call DelNote.RecordReturn(mPartCode, mReturnedQty, mPOLineNumber)
                End If 'Update delivery note
            End If 'Del Note already retrieved
        End If 'Delivery note identified for Return
        'Record Stock movement log entry if Quantity was updated
        If dblAdjustQty <> 0 Then
            MsgBox "Stock Log code removed"
'            Set oStockLog = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKLOG)
'            oStockLog.LogDate = Date
'            oStockLog.LogTime = Format$(Now, "HHMMSS")
'            oStockLog.PartCode = mPartCode
'            oStockLog.TransactionKey = RetNoteKey
'            oStockLog.TransactionNo = ReturnNote.DocNo
'            If eSave = SaveTypeIfExists Then 'reflect just the adjustment quantity
'                oStockLog.QuantityAdjusted = mReturnQty - mPreviousQty
'            Else
'                oStockLog.QuantityAdjusted = mReturnQty
'            End If
'            oStockLog.QuantityAdjusted = oStockLog.QuantityAdjusted * -1
'            oStockLog.ExtCost = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentCost
'            oStockLog.ExtRetailValue = (oStockLog.QuantityAdjusted / mSinglesSellingUnit) * mCurrentPrice
'            oStockLog.QuantityPerSellUnit = mSinglesSellingUnit
''            oStockLog.ExtCost = (mReturnQty / mSinglesSellingUnit) * mCurrentCost * -1
''            oStockLog.ExtRetailValue = (mReturnQty / mSinglesSellingUnit) * mCurrentPrice * -1
'            If oStockLog.QuantityAdjusted <> 0 Then Call oStockLog.SaveSupplierReturn
'            Set oStockLog = Nothing
            'Update Stock Movement History
            Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
            Call oStockMove.SaveStockReturned(mPartCode, mReturnQty, mCurrentPrice)
            Set oStockMove = Nothing
        End If
        If eSave = SaveTypeIfNew Then
            If LenB(mNarrative) <> 0 Then 'if narrative captured then call sub BO to save
                Set oNarrLine = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
                oNarrLine.DocumentNumber = mRetNoteKey
                If InStr(mNarrative, vbCrLf) > 0 Then
                    If InStr(mNarrative, vbCrLf) > 1 Then
                        oNarrLine.NarrativeText1 = Left$(mNarrative, InStr(mNarrative, vbCrLf) - 1)
                    End If
                    oNarrLine.NarrativeText2 = Mid$(mNarrative, InStr(mNarrative, vbCrLf) + 2)
                Else
                    oNarrLine.NarrativeText1 = Left$(mNarrative, 40)
                    oNarrLine.NarrativeText2 = Mid$(mNarrative, 41)
                End If
                oNarrLine.StoreNumber = m_oSession.CurrentEnterprise.IEnterprise_StoreNumber
                oNarrLine.PartCode = mPartCode
                oNarrLine.SequenceNumber = mLineNo
                Call oNarrLine.IBo_SaveIfNew
                Set oNarrLine = Nothing
            End If
        End If 'new entry
        If eSave = SaveTypeIfExists Then 'roll back previous stock movements
            'Update Stock Movement History if any stock levels were updated
            If dblAdjustQty <> 0 Then
                Set oStockMove = m_oSession.Database.CreateBusinessObject(CLASSID_STOCKMOVE)
                dblSellUnits = mSinglesSellingUnit
                If dblSellUnits = 0 Then dblSellUnits = 1
                Call oStockMove.SaveStockReturned(mPartCode, mPreviousQty * -1, (mReturnQty / dblSellUnits) * mOrderPrice * -1)
                Set oStockMove = Nothing
            End If
            'check if narrative edited and if so save changes
            If mintNarrStatus = EDITED Then
                Set oNarrLine = m_oSession.Database.CreateBusinessObject(CLASSID_DELNOTENARRATIVE)
                Call oNarrLine.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_StoreNumber, m_oSession.CurrentEnterprise.IEnterprise_StoreNumber)
                Call oNarrLine.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_DocumentNumber, mRetNoteKey)
                Call oNarrLine.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_PartCode, mPartCode)
                Call oNarrLine.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTENARRATIVE_SequenceNumber, mLineNo)
                Call oNarrLine.IBo_LoadMatches
                If InStr(mNarrative, vbCrLf) > 0 Then
                    If InStr(mNarrative, vbCrLf) > 1 Then
                        oNarrLine.NarrativeText1 = Left$(mNarrative, InStr(mNarrative, vbCrLf) - 1)
                    End If
                    oNarrLine.NarrativeText2 = Mid$(mNarrative, InStr(mNarrative, vbCrLf) + 2)
                Else
                    oNarrLine.NarrativeText1 = Left$(mNarrative, 40)
                    oNarrLine.NarrativeText2 = Mid$(mNarrative, 41)
                End If
                Call oNarrLine.IBo_SaveIfExists
                mintNarrStatus = LOADED
                Set oNarrLine = Nothing
            End If
        End If
    End If

End Function

'##ModelId=3F0E8B6600D3
Private Function Delete() As Boolean
    Debug.Assert False
End Function

'##ModelId=3F0E8B660137
Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


'##ModelId=3F0E8B660191
Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_RETURNNOTELINE * &H10000) + 1 To FID_RETURNNOTELINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>*************************************************************************-
'***************
'* Function:  Boolean IBo_AddLoadFilter()
'******************************************************************************-
'****************
'* Description: Part of the IBo Interface.  Used to set-up any selection
'criteria before
'*              calling the IBo_Load method.  This appends the selection
'criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'******************************************************************************-
'****************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare
'against
'*In/Out:Value  Variant. - Value that Field will be compared to
'******************************************************************************-
'****************
'* Returns:  Boolean - True if Valid FieldID and successful
'******************************************************************************-
'****************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***********************************************************************-
'****************
'##ModelId=3F0E8B6601EB
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter


'<CACH>*************************************************************************-
'***************
'* Function:  Boolean IBo_AddLoadField()
'******************************************************************************-
'****************
'* Description: Part of the IBo Interface.  Used to set-up all fields that
'must be returned
'*              when using the LoadMatches method.  This appends the field to
'the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is
'populated.
'******************************************************************************-
'****************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare
'against
'******************************************************************************-
'****************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***********************************************************************-
'****************
'##ModelId=3F0E8B66037C
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

'##ModelId=3F0E8B670048
Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

'##ModelId=3F0E8B670084
Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

'##ModelId=3F0E8B6700CA
Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set IBo_LoadMatches = colBO

End Function

'##ModelId=3F0E8B670189
Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

'##ModelId=3F0E8B67025B
Public Function Interface(Optional eInterfaceType As Long) As cReturnNoteLine
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function


'##ModelId=3F0E8B670319
Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

'##ModelId=3F0E8B67035F
Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_RETURNNOTELINE_END_OF_STATIC

End Function

