VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDespatchMethod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E4E73330230"
'<CAMH>****************************************************************************************
'* Module : cDespatchMethod
'* Date   : 15/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/ReturnsBO/cDespatchMethod.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 25/07/03 12:26 $  $Revision: 3 $
'* Versions:
'* 15/02/03    Unknown
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cDespatchMethod"

'##ModelId=3E4E7363026C
Private mID As Long

'##ModelId=3E4E736F01AE
Private mDescription As String

'##ModelId=3E4E738701E0
Private mActive As Boolean

Private mReferenceRequired As Long

Private moRowSel As Object

Private moLoadRow As Object

Private m_oSession As Object

'##ModelId=3E4E73F601EA
Public Property Get Active() As Boolean
   Let Active = mActive
End Property

'##ModelId=3E4E73F6017C
Public Property Let Active(ByVal Value As Boolean)
    Let mActive = Value
End Property

Public Property Get ReferenceRequired() As Long
   Let ReferenceRequired = mReferenceRequired
End Property

Public Property Let ReferenceRequired(ByVal Value As Long)
    Let mReferenceRequired = Value
End Property

'##ModelId=3E4E73F60140
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3E4E73F600D2
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3E4E73F600A0
Public Property Get ID() As Long
   Let ID = mID
End Property


'##ModelId=3E4E73F60032
Public Property Let ID(ByVal Value As Long)
    Let mID = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As Object
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As Object
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As Object
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_DESPATCHMETHOD * &H10000) + 1 To FID_DESPATCHMETHOD_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.IField_ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As Object = Nothing) As Collection

' Load up all properties from the database
Dim oView       As Object
Dim colBO       As Collection
Dim oBO         As Object
Dim lBONo       As Long
Dim oLoadFields As Object
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.createBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set IBo_LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As Object
    
    Set IBo_CreateNew = New cDespatchMethod

End Function


Private Function IBo_GetField(nFid As Long) As Object
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As Object
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As Object
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_DESPATCHMETHOD, FID_DESPATCHMETHOD_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As Object

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_DESPATCHMETHOD_ID):
                Set GetField = New CFieldLong
                GetField.LongValue = mID
        Case (FID_DESPATCHMETHOD_Description):
                Set GetField = New CFieldString
                GetField.StringValue = mDescription
        Case (FID_DESPATCHMETHOD_Active):
                Set GetField = New CFieldBool
                GetField.BoolValue = mActive
        Case (FID_DESPATCHMETHOD_ReferenceRequired):
                Set GetField = New CFieldLong
                GetField.LongValue = mReferenceRequired
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.IField_Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As Object) As cDespatchMethod
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_DESPATCHMETHOD

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Despatch Method " & mID

End Property

Private Function IBo_Initialise(oSession As Object) As Object
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As Object) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As Object) As Boolean

Dim lFieldNo As Integer
Dim oField   As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.IField_Id)
            Case (FID_DESPATCHMETHOD_ID):                mID = oField.LongValue
            Case (FID_DESPATCHMETHOD_Description):       mDescription = oField.StringValue
            Case (FID_DESPATCHMETHOD_Active):            mActive = oField.BoolValue
            Case (FID_DESPATCHMETHOD_ReferenceRequired): mReferenceRequired = oField.LongValue
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As Object
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As Object
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_DESPATCHMETHOD_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As Object

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


