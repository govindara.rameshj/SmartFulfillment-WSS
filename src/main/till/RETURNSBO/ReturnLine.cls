VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cReturnLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D538C5C03C0"
'<CAMH>****************************************************************************************
'* Module : cReturnLine
'* Date   : 09/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/ReturnsBO/ReturnLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Item Return Line
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 2/11/03 4:11p $ $Revision: 4 $
'* Versions:
'* 09/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cReturnLine"

Implements IBo
Implements ISysBo


'##ModelId=3D538C720258
Private mReturnID As Long

'##ModelId=3D538C7E0384
Private mLineNo As Long

'##ModelId=3D538C83023A
Private mPartCode As String

'##ModelId=3D538C8900E6
Private mPartAlphaKey As String

'##ModelId=3D538C8F028A
Private mQuantityReturned As Long

'##ModelId=3D538C980262
Private mPrice As Currency

'##ModelId=3D538C9C037A
Private mCost As Double

'##ModelId=3D538CA00050
Private mReturnCode As String

'##ModelId=3D538CA50190
Private mReturnReason As String

Private m_oSession As Session

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

'##ModelId=3D5B6BD90154
Public Property Get ReturnReason() As String
   Let ReturnReason = mReturnReason
End Property

'##ModelId=3D5B6BD90046
Public Property Let ReturnReason(ByVal Value As String)
    Let mReturnReason = Value
End Property

'##ModelId=3D5B6BD80384
Public Property Get ReturnCode() As String
   Let ReturnCode = mReturnCode
End Property

'##ModelId=3D5B6BD80276
Public Property Let ReturnCode(ByVal Value As String)
    Let mReturnCode = Value
End Property

'##ModelId=3D5B6BD801CC
Public Property Get Cost() As Double
   Let Cost = mCost
End Property

'##ModelId=3D5B6BD800BE
Public Property Let Cost(ByVal Value As Double)
    Let mCost = Value
End Property

'##ModelId=3D5B6BD80014
Public Property Get Price() As Currency
   Let Price = mPrice
End Property

'##ModelId=3D5B6BD70320
Public Property Let Price(ByVal Value As Currency)
    Let mPrice = Value
End Property

'##ModelId=3D5B6BD70280
Public Property Get QuantityReturned() As Long
   Let QuantityReturned = mQuantityReturned
End Property

'##ModelId=3D5B6BD701A4
Public Property Let QuantityReturned(ByVal Value As Long)
    Let mQuantityReturned = Value
End Property

'##ModelId=3D5B6BD70136
Public Property Get PartAlphaKey() As String
   Let PartAlphaKey = mPartAlphaKey
End Property

'##ModelId=3D5B6BD7005A
Public Property Let PartAlphaKey(ByVal Value As String)
    Let mPartAlphaKey = Value
End Property

'##ModelId=3D5B6BD60398
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property

'##ModelId=3D5B6BD602BC
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

'##ModelId=3D5B6BD6024E
Public Property Get LineNo() As Long
   Let LineNo = mLineNo
End Property

'##ModelId=3D5B6BD601AE
Public Property Let LineNo(ByVal Value As Long)
    Let mLineNo = Value
End Property

'##ModelId=3D5B6BD60104
Public Property Get ReturnID() As Long
   Let ReturnID = mReturnID
End Property


'##ModelId=3D5B6BD60064
Public Property Let ReturnID(ByVal Value As Long)
    Let mReturnID = Value
End Property
Friend Function LoadLines() As Collection

' Load up all properties from the database
Dim oView       As IView
Dim lLineNo     As Long
Dim colLines    As Collection
Dim oLine       As cReturnLine
Dim oRow        As IRow
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    'Merge selection criteria together
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        'Build up a collection of Purchase Order Lines that will be passed out
        Set colLines = New Collection
        For lLineNo = 1 To oView.Count
            Set oLine = m_oSession.Database.CreateBusinessObject(CLASSID_RETURNLINE)
            Set oRow = oView.Row(CLng(lLineNo))
            Call oLine.LoadFromRow(oRow)
            Call colLines.Add(oLine)
        Next lLineNo
    End If
    Set LoadLines = colLines

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_RETURNLINE_ReturnID):     mReturnID = oField.ValueAsVariant
            Case (FID_RETURNLINE_LineNo):       mLineNo = oField.ValueAsVariant
            Case (FID_RETURNLINE_PartCode):     mPartCode = oField.ValueAsVariant
            Case (FID_RETURNLINE_PartAlphaKey): mPartAlphaKey = oField.ValueAsVariant
            Case (FID_RETURNLINE_QuantityReturned): mQuantityReturned = oField.ValueAsVariant
            Case (FID_RETURNLINE_Price):        mPrice = oField.ValueAsVariant
            Case (FID_RETURNLINE_Cost):         mCost = oField.ValueAsVariant
            Case (FID_RETURNLINE_ReturnCode):   mReturnCode = oField.ValueAsVariant
            Case (FID_RETURNLINE_ReturnReason): mReturnReason = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Return Line - " & mReturnID & " Line " & mLineNo

End Property
Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cReturnLine
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_RETURNLINE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField
Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_RETURNLINE * &H10000) + 1 To FID_RETURNLINE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function



Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IBo_GetField(nFid As Long) As IField

    Set IBo_GetField = GetField(nFid)

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_RETURNLINE, FID_RETURNLINE_END_OF_STATIC, m_oSession)

End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function



Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function
Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function
Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo

    Set IBo_CreateNew = New cReturnLine

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_RETURNLINE_ReturnID):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mReturnID
        Case (FID_RETURNLINE_LineNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mLineNo
        Case (FID_RETURNLINE_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_RETURNLINE_PartAlphaKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartAlphaKey
        Case (FID_RETURNLINE_QuantityReturned):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQuantityReturned
        Case (FID_RETURNLINE_Price):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mPrice
        Case (FID_RETURNLINE_Cost):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCost
        Case (FID_RETURNLINE_ReturnCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReturnCode
        Case (FID_RETURNLINE_ReturnReason):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mReturnReason
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cReturnLine
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function


Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_RETURNLINE_END_OF_STATIC

End Function

