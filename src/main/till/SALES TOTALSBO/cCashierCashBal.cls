VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCashierCashBal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"409618B100FF"
'<CAMH>****************************************************************************************
'* Module : cCashierCashBal
'* Date   : 06/05/04
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Sales TotalsBO/cCashierCashBal.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 11/05/04 18:57 $ $Revision: 2 $
'* Versions:
'* 11/05/04    mauricem
'*             Initail Version created.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cCashierCashBal"

Implements IBo
Implements ISysBo

'##ModelId=4096193F0095
Private mTotalsDate As Date

'##ModelId=4096194E0227
Private mCashierNumber As String

'##ModelId=4096199100D9
Private mGrossSales As Double

'##ModelId=409619990120
Private mDiscountAmount As Double

'##ModelId=409619BE03CC
Private mNoOfTranTypes(8) As Long

'##ModelId=409619D9000A
Private mTranTypeTotals(8) As Double

'##ModelId=409619EC027E
Private mTranDiscountTotals(8) As Double

'##ModelId=409619F300A7
Private mFloatAmount As Double

'##ModelId=409619FC003C
Private mPickUpTotal As Double

'##ModelId=40961A020302
Private mTenderTypeOccurence(40) As Long

'##ModelId=40961A0F0080
Private mTenderTypeAmount(40) As Double

'##ModelId=40961A33024E
Private mTenderTypePickUps(40) As Double

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=40961B63001A
Public Property Let TotalsDate(ByVal Value As Date)
    Let mTotalsDate = Value
End Property

'##ModelId=40961B63007F
Public Property Get TotalsDate() As Date
   Let TotalsDate = mTotalsDate
End Property

'##ModelId=40961B6503D5
Public Property Get TenderTypePickUps(Index As Variant) As Double
    Let TenderTypePickUps = mTenderTypePickUps(Index)
End Property

'##ModelId=40961B650302
Public Property Let TenderTypePickUps(Index As Variant, ByVal Value As Double)
    Let mTenderTypePickUps(Index) = Value
End Property

'##ModelId=40961B65026C
Public Property Get TenderTypeAmount(Index As Variant) As Double
    Let TenderTypeAmount = mTenderTypeAmount(Index)
End Property

'##ModelId=40961B6501A4
Public Property Let TenderTypeAmount(Index As Variant, ByVal Value As Double)
    Let mTenderTypeAmount(Index) = Value
End Property

'##ModelId=40961B65010E
Public Property Get TenderTypeOccurence(Index As Variant) As Long
    Let TenderTypeOccurence = mTenderTypeOccurence(Index)
End Property

'##ModelId=40961B650045
Public Property Let TenderTypeOccurence(Index As Variant, ByVal Value As Long)
    Let mTenderTypeOccurence(Index) = Value
End Property

'##ModelId=40961B6403E7
Public Property Get PickUpTotal() As Double
   Let PickUpTotal = mPickUpTotal
End Property

'##ModelId=40961B640365
Public Property Let PickUpTotal(ByVal Value As Double)
    Let mPickUpTotal = Value
End Property

'##ModelId=40961B640315
Public Property Get FloatAmount() As Double
   Let FloatAmount = mFloatAmount
End Property

'##ModelId=40961B640293
Public Property Let FloatAmount(ByVal Value As Double)
    Let mFloatAmount = Value
End Property

'##ModelId=40961B640211
Public Property Get TranDiscountTotals(Index As Variant) As Double
    Let TranDiscountTotals = mTranDiscountTotals(Index)
End Property

'##ModelId=40961B640152
Public Property Let TranDiscountTotals(Index As Variant, ByVal Value As Double)
    Let mTranDiscountTotals(Index) = Value
End Property

'##ModelId=40961B6400D0
Public Property Get TranTypeTotals(Index As Variant) As Double
    Let TranTypeTotals = mTranTypeTotals(Index)
End Property

'##ModelId=40961B64001C
Public Property Let TranTypeTotals(Index As Variant, ByVal Value As Double)
    Let mTranTypeTotals(Index) = Value
End Property

'##ModelId=40961B630382
Public Property Get NoOfTranTypes(Index As Variant) As Long
    Let NoOfTranTypes = mNoOfTranTypes(Index)
End Property

'##ModelId=40961B6302CD
Public Property Let NoOfTranTypes(Index As Variant, ByVal Value As Long)
    Let mNoOfTranTypes(Index) = Value
End Property

'##ModelId=40961B630291
Public Property Get DiscountAmount() As Double
   Let DiscountAmount = mDiscountAmount
End Property

'##ModelId=40961B630219
Public Property Let DiscountAmount(ByVal Value As Double)
    Let mDiscountAmount = Value
End Property

'##ModelId=40961B6301D3
Public Property Get GrossSales() As Double
   Let GrossSales = mGrossSales
End Property

'##ModelId=40961B630165
Public Property Let GrossSales(ByVal Value As Double)
    Let mGrossSales = Value
End Property

'##ModelId=40961B630129
Public Property Get CashierNumber() As String
   Let CashierNumber = mCashierNumber
End Property

'##ModelId=40961B6300BB
Public Property Let CashierNumber(ByVal Value As String)
    Let mCashierNumber = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    SaveIfNew = Save(SaveTypeIfNew)
End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean
    SaveIfExists = Save(SaveTypeIfExists)
End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean
    Delete = Save(SaveTypeDelete)
End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CASHIERCASHBAL * &H10000) + 1 To FID_CASHIERCASHBAL_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCashierCashBal

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CASHIERCASHBAL, FID_CASHIERCASHBAL_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CASHIERCASHBAL_TotalsDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTotalsDate
        Case (FID_CASHIERCASHBAL_CashierNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCashierNumber
        Case (FID_CASHIERCASHBAL_GrossSales):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mGrossSales
        Case (FID_CASHIERCASHBAL_DiscountAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mDiscountAmount
        Case (FID_CASHIERCASHBAL_NoOfTranTypes):       Set GetField = SaveLongArray(mNoOfTranTypes, m_oSession)
        Case (FID_CASHIERCASHBAL_TranTypeTotals):      Set GetField = SaveDoubleArray(mTranTypeTotals, m_oSession)
        Case (FID_CASHIERCASHBAL_TranDiscountTotals):  Set GetField = SaveDoubleArray(mTranDiscountTotals, m_oSession)
        Case (FID_CASHIERCASHBAL_FloatAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mFloatAmount
        Case (FID_CASHIERCASHBAL_PickUpTotal):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mPickUpTotal
        Case (FID_CASHIERCASHBAL_TenderTypeOccurence): Set GetField = SaveLongArray(mTenderTypeOccurence, m_oSession)
        Case (FID_CASHIERCASHBAL_TenderTypeAmount):    Set GetField = SaveDoubleArray(mTenderTypeAmount, m_oSession)
        Case (FID_CASHIERCASHBAL_TenderTypePickUps):   Set GetField = SaveDoubleArray(mTenderTypePickUps, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cCashierCashBal
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CASHIERCASHBAL

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Cashier Cash Bal " & mTotalsDate & " " & mCashierNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CASHIERCASHBAL_TotalsDate):          mTotalsDate = oField.ValueAsVariant
            Case (FID_CASHIERCASHBAL_CashierNumber):       mCashierNumber = oField.ValueAsVariant
            Case (FID_CASHIERCASHBAL_GrossSales):          mGrossSales = oField.ValueAsVariant
            Case (FID_CASHIERCASHBAL_DiscountAmount):      mDiscountAmount = oField.ValueAsVariant
            Case (FID_CASHIERCASHBAL_NoOfTranTypes):       Call LoadLongArray(mNoOfTranTypes, oField, m_oSession)
            Case (FID_CASHIERCASHBAL_TranTypeTotals):      Call LoadDoubleArray(mTranTypeTotals, oField, m_oSession)
            Case (FID_CASHIERCASHBAL_TranDiscountTotals):  Call LoadDoubleArray(mTranDiscountTotals, oField, m_oSession)
            Case (FID_CASHIERCASHBAL_FloatAmount):         mFloatAmount = oField.ValueAsVariant
            Case (FID_CASHIERCASHBAL_PickUpTotal):         mPickUpTotal = oField.ValueAsVariant
            Case (FID_CASHIERCASHBAL_TenderTypeOccurence): Call LoadLongArray(mTenderTypeOccurence, oField, m_oSession)
            Case (FID_CASHIERCASHBAL_TenderTypeAmount):    Call LoadDoubleArray(mTenderTypeAmount, oField, m_oSession)
            Case (FID_CASHIERCASHBAL_TenderTypePickUps):   Call LoadDoubleArray(mTenderTypePickUps, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CASHIERCASHBAL_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cCashierCashBal

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


