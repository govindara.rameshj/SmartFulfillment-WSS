VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSaleTotals"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DF741970140"
'<CAMH>****************************************************************************************
'* Module : cSaleTotals
'* Date   : 11/12/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Sales TotalsBO/cSaleTotals.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 11/05/04 18:57 $ $Revision: 4 $
'* Versions:
'* 11/12/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************

Option Base 0
Option Explicit

Const MODULE_NAME As String = "cSaleTotals"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3DF745960244
Private mSaleTotalKey As Long

'##ModelId=3DF745B80320
Private mFloatAmount As Double

'##ModelId=3DF745BD00C8
Private mPickUpAmount As Double

'##ModelId=3DF745D800AA
Private mTransactionCount(13) As Long

'##ModelId=3DF745EA02A8
Private mTranLastTime(13) As String

'##ModelId=3DF745F30050
Private mTransactionValue(13) As Double

'##ModelId=3DF745FB0140
Private mTenderTypesCount(20) As Long

'##ModelId=3DF7460303AC
Private mTenderTypesValue(20) As Double

'##ModelId=3DF7476002D0
Public Property Get TenderTypesValue(Index As Variant) As Double
    Let TenderTypesValue = mTenderTypesValue(Index)
End Property

'##ModelId=3DF747600118
Public Property Let TenderTypesValue(Index As Variant, ByVal Value As Double)
    Let mTenderTypesValue(Index) = Value
End Property

'##ModelId=3DF7475F03B6
Public Property Get TenderTypesCount(Index As Variant) As Long
    Let TenderTypesCount = mTenderTypesCount(Index)
End Property

'##ModelId=3DF7475F0082
Public Property Let TenderTypesCount(Index As Variant, ByVal Value As Long)
    Let mTenderTypesCount(Index) = Value
End Property

'##ModelId=3DF7475E0320
Public Property Get TransactionValue(Index As Variant) As Double
    Let TransactionValue = mTransactionValue(Index)
End Property

'##ModelId=3DF7475E0168
Public Property Let TransactionValue(Index As Variant, ByVal Value As Double)
    Let mTransactionValue(Index) = Value
End Property

'##ModelId=3DF7475E0050
Public Property Get TranLastTime(Index As Variant) As String
    Let TranLastTime = mTranLastTime(Index)
End Property

'##ModelId=3DF7475D02BC
Public Property Let TranLastTime(Index As Variant, ByVal Value As String)
    Let mTranLastTime(Index) = Value
End Property

'##ModelId=3DF7475D0136
Public Property Get TransactionCount(Index As Variant) As Long
    Let TransactionCount = mTransactionCount(Index)
End Property

'##ModelId=3DF7475C0366
Public Property Let TransactionCount(Index As Variant, ByVal Value As Long)
    Let mTransactionCount(Index) = Value
End Property

'##ModelId=3DF7475C02C6
Public Property Get PickUpAmount() As Double
   Let PickUpAmount = mPickUpAmount
End Property

'##ModelId=3DF7475C01AE
Public Property Let PickUpAmount(ByVal Value As Double)
    Let mPickUpAmount = Value
End Property

'##ModelId=3DF7475C010E
Public Property Get FloatAmount() As Double
   Let FloatAmount = mFloatAmount
End Property

'##ModelId=3DF7475B03DE
Public Property Let FloatAmount(ByVal Value As Double)
    Let mFloatAmount = Value
End Property

'##ModelId=3DF7475B0370
Public Property Get SaleTotalKey() As Long
   Let SaleTotalKey = mSaleTotalKey
End Property


'##ModelId=3DF7475B0294
Public Property Let SaleTotalKey(ByVal Value As Long)
    Let mSaleTotalKey = Value
End Property



Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    
    IBo_SaveIfNew = SaveIfNew

End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = SaveIfExists

End Function

Public Function SaveIfExists() As Boolean
    
    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Delete

End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_SALETOTALS * &H10000) + 1 To FID_SALETOTALS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cSaleTotals

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SALETOTALS, FID_SALETOTALS_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_SALETOTALS_SaleTotalKey):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = SaleTotalKey
        Case (FID_SALETOTALS_FloatAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = FloatAmount
        Case (FID_SALETOTALS_PickUpAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = PickUpAmount
        Case (FID_SALETOTALS_TransactionCount): Set GetField = SaveLongArray(mTransactionCount, m_oSession)
        Case (FID_SALETOTALS_TranLastTime):     Set GetField = SaveStringArray(mTranLastTime, m_oSession)
        Case (FID_SALETOTALS_TransactionValue): Set GetField = SaveDoubleArray(mTransactionValue, m_oSession)
        Case (FID_SALETOTALS_TenderTypesCount): Set GetField = SaveLongArray(mTenderTypesCount, m_oSession)
        Case (FID_SALETOTALS_TenderTypesValue): Set GetField = SaveDoubleArray(mTenderTypesValue, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cSaleTotals
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SALETOTALS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = " Sales Total " & mSaleTotalKey

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_SALETOTALS_SaleTotalKey):     mSaleTotalKey = oField.ValueAsVariant
            Case (FID_SALETOTALS_FloatAmount):      mFloatAmount = oField.ValueAsVariant
            Case (FID_SALETOTALS_PickUpAmount):     mPickUpAmount = oField.ValueAsVariant
            Case (FID_SALETOTALS_TransactionCount): Call LoadLongArray(mTransactionCount, oField, m_oSession)
            Case (FID_SALETOTALS_TranLastTime):     Call LoadStringArray(mTranLastTime, oField, m_oSession)
            Case (FID_SALETOTALS_TransactionValue): Call LoadDoubleArray(mTransactionValue, oField, m_oSession)
            Case (FID_SALETOTALS_TenderTypesCount): Call LoadLongArray(mTenderTypesCount, oField, m_oSession)
            Case (FID_SALETOTALS_TenderTypesValue): Call LoadDoubleArray(mTenderTypesValue, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SALETOTALS_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cSaleTotals

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


