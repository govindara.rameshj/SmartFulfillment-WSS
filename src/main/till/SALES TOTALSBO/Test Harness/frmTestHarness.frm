VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   5715
   ClientLeft      =   1590
   ClientTop       =   1545
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   ScaleHeight     =   5715
   ScaleWidth      =   6585
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   1111
      ButtonWidth     =   609
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   393216
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

Dim oRoot As Object
Dim oBO   As Object

    
    Set oRoot = GetRoot
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_COLLEAGUECARD)
    Call oBO.Load
    Set oBO = Nothing

    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_SALESCUSTOMER)
'    Call oBO.IBO_AddLoadFilter(CMP_EQUAL, FID_SOLINE_OrderNo, "000002")
    Call oBO.IBo_LoadMatches
    Set oBO = Nothing

    Set oBO = goDatabase.CreateBusinessObject(CLASSID_POSPAYMENT)
    Call oBO.IBO_Load
    Set oBO = Nothing
    
    Set oBO = goDatabase.CreateBusinessObject(CLASSID_TRANSACTION_DISCOUNT)
    Call oBO.IBo_LoadMatches
    Set oBO = Nothing

End Sub
