VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCashierTotals"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DF74238008C"
'<CAMH>****************************************************************************************
'* Module : cCashierTotals
'* Date   : 11/12/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Sales TotalsBO/cCashierTotals.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 11/05/04 18:57 $ $Revision: 4 $
'* Versions:
'* 11/12/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cCashierTotals"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3DF744A502A8
Private mCashierNumber As String

'##ModelId=3DF744AB0154
Private mSaleTotalsKey As Long

'##ModelId=3DF744B50226
Private mCurrentTillNumber As String

'##ModelId=3DF744C00032
Private mCashierName As String

'##ModelId=3DF744C701CC
Private mSecurityCode As String

'##ModelId=3DF744D00050
Private mTransactionCount As Long

'##ModelId=3DF744DA0316
Private mTransactionValue As Double

'##ModelId=3DF7476A0302
Public Property Get TransactionValue() As Double
   Let TransactionValue = mTransactionValue
End Property

'##ModelId=3DF7476A017C
Public Property Let TransactionValue(ByVal Value As Double)
    Let mTransactionValue = Value
End Property

'##ModelId=3DF7476A00DC
Public Property Get TransactionCount() As Long
   Let TransactionCount = mTransactionCount
End Property

'##ModelId=3DF74769037A
Public Property Let TransactionCount(ByVal Value As Long)
    Let mTransactionCount = Value
End Property

'##ModelId=3DF74769029E
Public Property Get SecurityCode() As String
   Let SecurityCode = mSecurityCode
End Property

'##ModelId=3DF747690154
Public Property Let SecurityCode(ByVal Value As String)
    Let mSecurityCode = Value
End Property

'##ModelId=3DF7476900B4
Public Property Get CashierName() As String
   Let CashierName = mCashierName
End Property

'##ModelId=3DF7476802A8
Public Property Let CashierName(ByVal Value As String)
    Let mCashierName = Value
End Property

'##ModelId=3DF747680208
Public Property Get CurrentTillNumber() As String
   Let CurrentTillNumber = mCurrentTillNumber
End Property

'##ModelId=3DF7476800F0
Public Property Let CurrentTillNumber(ByVal Value As String)
    Let mCurrentTillNumber = Value
End Property

'##ModelId=3DF747680014
Public Property Get SaleTotalsKey() As Long
   Let SaleTotalsKey = mSaleTotalsKey
End Property

'##ModelId=3DF7476702EE
Public Property Let SaleTotalsKey(ByVal Value As Long)
    Let mSaleTotalsKey = Value
End Property

'##ModelId=3DF747670244
Public Property Get CashierNumber() As String
   Let CashierNumber = mCashierNumber
End Property


'##ModelId=3DF747670136
Public Property Let CashierNumber(ByVal Value As String)
    Let mCashierNumber = Value
End Property



Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    SaveIfNew = Save(SaveTypeIfNew)
End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean
    SaveIfExists = Save(SaveTypeIfExists)
End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean
    Delete = Save(SaveTypeDelete)
End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CASHIERTOTALS * &H10000) + 1 To FID_CASHIERTOTALS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCashierTotals

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CASHIERTOTALS, FID_CASHIERTOTALS_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CASHIERTOTALS_CashierNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCashierNumber
        Case (FID_CASHIERTOTALS_SaleTotalsKey):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSaleTotalsKey
        Case (FID_CASHIERTOTALS_CurrentTillNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrentTillNumber
        Case (FID_CASHIERTOTALS_CashierName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCashierName
        Case (FID_CASHIERTOTALS_SecurityCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSecurityCode
        Case (FID_CASHIERTOTALS_TransactionCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mTransactionCount
        Case (FID_CASHIERTOTALS_TransactionValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTransactionValue
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cCashierTotals
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CASHIERTOTALS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Cashier totals " & mCashierNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CASHIERTOTALS_CashierNumber):     mCashierNumber = oField.ValueAsVariant
            Case (FID_CASHIERTOTALS_SaleTotalsKey):     mSaleTotalsKey = oField.ValueAsVariant
            Case (FID_CASHIERTOTALS_CurrentTillNumber): mCurrentTillNumber = oField.ValueAsVariant
            Case (FID_CASHIERTOTALS_CashierName):       mCashierName = oField.ValueAsVariant
            Case (FID_CASHIERTOTALS_SecurityCode):      mSecurityCode = oField.ValueAsVariant
            Case (FID_CASHIERTOTALS_TransactionCount):  mTransactionCount = oField.ValueAsVariant
            Case (FID_CASHIERTOTALS_TransactionValue):  mTransactionValue = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CASHIERTOTALS_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cCashierTotals

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


