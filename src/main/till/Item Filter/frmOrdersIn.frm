VERSION 5.00
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Begin VB.Form frmOrdersIn 
   BorderStyle     =   0  'None
   Caption         =   "Orders Due In"
   ClientHeight    =   3405
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7215
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   7215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin LpADOLib.fpListAdo lstOrdersIn 
      Height          =   2160
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   6975
      _Version        =   196608
      _ExtentX        =   12303
      _ExtentY        =   3810
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Columns         =   5
      Sorted          =   0
      LineWidth       =   1
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   -1
      ColumnWidthScale=   2
      RowHeight       =   -1
      MultiSelect     =   0
      WrapList        =   0   'False
      WrapWidth       =   0
      SelMax          =   -1
      AutoSearch      =   1
      SearchMethod    =   0
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   0
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   0
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   1
      BorderColor     =   -2147483642
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   1
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   0
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   0   'False
      VScrollSpecialType=   0
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   0
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   1
      VirtualPageSize =   150
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   -1  'True
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      DataField       =   ""
      DataMember      =   ""
      OLEDragMode     =   0
      OLEDropMode     =   0
      EnableClickEvent=   -1  'True
      Redraw          =   -1  'True
      ResizeRowToFont =   0   'False
      TextTipMultiLine=   0
      ColDesigner     =   "frmOrdersIn.frx":0000
   End
   Begin VB.PictureBox picBorder 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3400
      Left            =   0
      ScaleHeight     =   3375
      ScaleWidth      =   7185
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   7215
      Begin VB.CommandButton cmdClose 
         Caption         =   "F12-Close"
         Default         =   -1  'True
         Height          =   375
         Left            =   6000
         TabIndex        =   3
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label lblTitle 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Orders Due In for Item "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   1
         Top             =   180
         Width           =   7215
      End
   End
End
Attribute VB_Name = "frmOrdersIn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const SINGLE_SPACE As String = " "
Private mstrSkuNumber As String

Public Property Let SkuNumber(ByVal strSkun As String)
    mstrSkuNumber = strSkun
    
End Property

Private Sub cmdClose_Click()
    Hide

End Sub

Private Sub Form_Activate()

    If (lstOrdersIn.ListCount > 0) Then
        lstOrdersIn.SetFocus
    Else
        cmdClose.Value = True
    End If
    
End Sub

Private Sub Form_Initialize()
    mstrSkuNumber = vbNullString
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (KeyCode)
        Case vbKeyF12
            KeyCode = 0
            cmdClose.Value = True
                
    End Select

End Sub

Private Sub Form_Load()

    picBorder.BackColor = RGBQuery_BackColour
    BackColor = RGBQuery_BackColour
    lblTitle.Caption = lblTitle.Caption & mstrSkuNumber
    
    lstOrdersIn.BorderColor = RGBQuery_BackColour
    lstOrdersIn.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstOrdersIn.ListApplyTo = ListApplyToEvenRows
    lstOrdersIn.BackColor = lstOrdersIn.BackColor
    lstOrdersIn.ListApplyTo = ListApplyToOddRows
    lstOrdersIn.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstOrdersIn.Row = -1
    lstOrdersIn.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)
    
    If GetOrders Then
        Call FormatColumnData
        lstOrdersIn.Selected(0) = True
        lstOrdersIn.ReDraw = True
        DoEvents
        
    Else
        Call MsgBoxEx("This item is not on order.", vbExclamation, "Orders Due In", , , , , RGBMSGBox_PromptColour)

    End If
    
End Sub

Private Sub FormatColumnData()
    
    lstOrdersIn.Row = -1
    lstOrdersIn.ListApplyTo = ListApplyToIndividual
    lstOrdersIn.Col = 0
    lstOrdersIn.ColHeaderText = "P/O Number"
    lstOrdersIn.AlignH = AlignHCenter
    lstOrdersIn.Col = 1
    lstOrdersIn.ColHeaderText = "Qty. Ordered"
    lstOrdersIn.AlignH = AlignHRight
    lstOrdersIn.Col = 2
    lstOrdersIn.ColHeaderText = "Qty. Received"
    lstOrdersIn.AlignH = AlignHRight
    lstOrdersIn.Col = 3
    lstOrdersIn.ColHeaderText = "Due/Received Date"
    lstOrdersIn.AlignH = AlignHCenter
    lstOrdersIn.Col = 4
    lstOrdersIn.ColHeaderText = "Status"
    
End Sub

Private Function GetOrders() As Boolean
Dim strQuery As String
Dim strRow As String
Dim rsData As Recordset
Dim oConn As Connection

    On Error GoTo ErrorHandler
    
    GetOrders = False
    lstOrdersIn.Clear
    MousePointer = vbHourglass
    
    strQuery = "SELECT purhdr.numb, purlin.qtyo, purlin.rqty, purhdr.ddat, " & _
               "IF(purhdr.rcom = 0 AND purhdr.rpar = 0, 'Outstanding', 'Received') ""Status"" " & _
               "FROM purlin INNER JOIN purhdr ON purlin.hkey = purhdr.tkey " & _
               "WHERE purlin.skun = '" & mstrSkuNumber & "' " & _
               "ORDER BY purhdr.ddat DESC"
               
    Set oConn = New Connection
    oConn.ConnectionString = goDatabase.ConnectionString
    oConn.CursorLocation = adUseClient
    oConn.Open
    
    Set rsData = oConn.Execute(strQuery)
    
    If (rsData.RecordCount > 0) Then
        While (Not rsData.EOF)
            strRow = rsData!numb & vbTab & _
                     Format(rsData!qtyo, " 0 ") & vbTab & _
                     Format(rsData!rqty, " 0 ") & vbTab & _
                     Format(rsData!ddat, "DD/MM/YYYY") & vbTab & _
                     SINGLE_SPACE & rsData!Status
            lstOrdersIn.AddItem strRow
            rsData.MoveNext
        
        Wend
        GetOrders = True
    
    End If
    
    oConn.Close
    Set rsData = Nothing
    Set oConn = Nothing
    MousePointer = vbDefault
    
    Exit Function
    
ErrorHandler:

    If (oConn.State = adStateOpen) Then oConn.Close
    If (Not rsData Is Nothing) Then Set rsData = Nothing
    If (Not oConn Is Nothing) Then Set oConn = Nothing
    
    MousePointer = vbDefault
    Call MsgBoxEx("An error occured while searching purchase orders.", vbCritical + vbOKOnly, "Error", , , , , RGBMsgBox_WarnColour)

End Function

