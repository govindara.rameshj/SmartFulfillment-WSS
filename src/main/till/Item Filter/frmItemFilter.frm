VERSION 5.00
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmItemFilter 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Item Filter"
   ClientHeight    =   11145
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   15345
   Icon            =   "frmItemFilter.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11145
   ScaleWidth      =   15345
   Visible         =   0   'False
   Begin prjKeyPadUC.ucKeyPad ucKeyPad 
      Height          =   4110
      Left            =   120
      Top             =   2040
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Frame fraHierarchyLoading 
      BorderStyle     =   0  'None
      Height          =   4575
      Left            =   5520
      TabIndex        =   25
      Top             =   480
      Visible         =   0   'False
      Width           =   7095
      Begin VB.Label lblLoadingHierarchyMessage 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1695
         Left            =   1680
         TabIndex        =   26
         Top             =   1440
         Width           =   4335
      End
      Begin VB.Shape shpMessage 
         BackColor       =   &H8000000F&
         BackStyle       =   1  'Opaque
         Height          =   3375
         Left            =   1080
         Top             =   600
         Width           =   5175
      End
   End
   Begin MSWinsockLib.Winsock wsckComms 
      Index           =   0
      Left            =   0
      Top             =   1200
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Frame fraItemFilter 
      BorderStyle     =   0  'None
      Height          =   10480
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   15200
      Begin LpADOLib.fpListAdo lstMatches 
         Height          =   9510
         Left            =   5145
         TabIndex        =   12
         Top             =   90
         Width           =   9855
         _Version        =   196608
         _ExtentX        =   17383
         _ExtentY        =   16775
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Columns         =   14
         Sorted          =   0
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   0
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   0
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   1
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   0
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   150
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         DataMember      =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "frmItemFilter.frx":058A
      End
      Begin LpLib.fpCombo cboSupplier 
         Height          =   315
         Left            =   1095
         TabIndex        =   6
         Top             =   1230
         Width           =   3945
         _Version        =   196608
         _ExtentX        =   6959
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   2
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483633
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmItemFilter.frx":0B8C
      End
      Begin VB.CommandButton cmdReloadData 
         Caption         =   "Relo&ad Data"
         Height          =   255
         Left            =   5542
         TabIndex        =   24
         Top             =   9700
         Width           =   1480
      End
      Begin VB.Timer tmrRefresh 
         Interval        =   1000
         Left            =   0
         Top             =   240
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3 - &Reset"
         Height          =   375
         Left            =   2240
         TabIndex        =   7
         Top             =   1590
         Width           =   1095
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "F7 - &Search"
         Height          =   375
         Left            =   3410
         TabIndex        =   8
         Top             =   1590
         Width           =   1635
      End
      Begin VB.TextBox txtSKU 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1100
         MaxLength       =   6
         TabIndex        =   5
         Top             =   855
         Width           =   3945
      End
      Begin VB.TextBox txtAltDescription 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1100
         TabIndex        =   3
         Top             =   465
         Width           =   3945
      End
      Begin VB.TextBox txtDescription 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1100
         TabIndex        =   1
         Top             =   90
         Width           =   3945
      End
      Begin VB.CommandButton cmdEnlarge 
         Caption         =   "F4 - &Enlarge"
         Height          =   375
         Left            =   1708
         TabIndex        =   15
         Top             =   10020
         Visible         =   0   'False
         Width           =   1200
      End
      Begin VB.CommandButton cmdViewType 
         Caption         =   "F2 - &View Images"
         Height          =   375
         Left            =   80
         TabIndex        =   14
         Top             =   10020
         Width           =   1620
      End
      Begin VB.CommandButton cmdSelect 
         Caption         =   "F5 - Se&lect"
         Height          =   375
         Left            =   2916
         TabIndex        =   16
         Top             =   10020
         Width           =   1100
      End
      Begin VB.CommandButton cmdRelatedItem 
         Caption         =   "F6 - Rel. Ite&m"
         Enabled         =   0   'False
         Height          =   375
         Left            =   5542
         TabIndex        =   18
         Top             =   10020
         Visible         =   0   'False
         Width           =   1480
      End
      Begin VB.CommandButton cmdOrdersIn 
         Caption         =   "F8 - Orders Due &In"
         Height          =   375
         Left            =   7030
         TabIndex        =   19
         Top             =   10020
         Visible         =   0   'False
         Width           =   1740
      End
      Begin VB.CommandButton cmdDetails 
         Caption         =   "F11 - De&tails"
         Height          =   375
         Left            =   12544
         TabIndex        =   22
         Top             =   10020
         Visible         =   0   'False
         Width           =   1240
      End
      Begin VB.CommandButton cmdUseList 
         Caption         =   "Ctrl F5 - &Use List"
         Height          =   375
         Left            =   4024
         TabIndex        =   17
         Top             =   10020
         Visible         =   0   'False
         Width           =   1590
      End
      Begin VB.CheckBox chkActiveOnly 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         Caption         =   "F9 - Inc. &Obs/Del"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   8778
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   10020
         Width           =   1815
      End
      Begin VB.CheckBox chkIncludeNonStock 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         Caption         =   "F10 - Inc. &Non-Stock"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   10601
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   10020
         Width           =   1935
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "F12 - &Close"
         Height          =   375
         Left            =   13800
         TabIndex        =   23
         Top             =   10020
         Width           =   1200
      End
      Begin VB.Timer tmrLoadImages 
         Enabled         =   0   'False
         Interval        =   10
         Left            =   720
         Top             =   1320
      End
      Begin VB.Timer tmrLoadHierarchy 
         Enabled         =   0   'False
         Interval        =   1
         Left            =   720
         Top             =   1800
      End
      Begin VB.Timer tmrBuildTreeData 
         Enabled         =   0   'False
         Interval        =   10
         Left            =   720
         Top             =   360
      End
      Begin VB.Frame fraImagesLoading 
         BorderStyle     =   0  'None
         Height          =   3800
         Left            =   6000
         TabIndex        =   28
         Top             =   5600
         Visible         =   0   'False
         Width           =   5600
         Begin VB.Label lblLoadingImagesMessage 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1695
            Left            =   625
            TabIndex        =   29
            Top             =   1080
            Width           =   4350
         End
         Begin VB.Shape shpMessage2 
            BackColor       =   &H8000000F&
            BackStyle       =   1  'Opaque
            Height          =   3400
            Left            =   200
            Top             =   200
            Width           =   5200
         End
      End
      Begin MSAdodcLib.Adodc adodcSearch 
         Height          =   495
         Left            =   1080
         Top             =   1680
         Visible         =   0   'False
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   873
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   "OASYS"
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "Adodc1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin MSComctlLib.TreeView tvwHierarchyPIM 
         Height          =   7875
         Left            =   75
         TabIndex        =   10
         Top             =   2040
         Width           =   4980
         _ExtentX        =   8784
         _ExtentY        =   13891
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList ilsThumbnailImages 
         Left            =   720
         Top             =   720
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   150
         ImageHeight     =   150
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemFilter.frx":0EA3
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin FPSpreadADO.fpSpread sprdThumbnails 
         Height          =   9560
         Left            =   5140
         TabIndex        =   13
         Top             =   90
         Width           =   9855
         _Version        =   458752
         _ExtentX        =   17383
         _ExtentY        =   16863
         _StockProps     =   64
         DisplayColHeaders=   0   'False
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GridShowHoriz   =   0   'False
         GridShowVert    =   0   'False
         MaxCols         =   4
         MaxRows         =   2
         ScrollBars      =   2
         SelectBlockOptions=   1
         SpreadDesigner  =   "frmItemFilter.frx":1CF2
      End
      Begin MSComctlLib.TreeView tvwHierarchyFull 
         Height          =   7880
         Left            =   80
         TabIndex        =   11
         Top             =   2030
         Width           =   4980
         _ExtentX        =   8784
         _ExtentY        =   13891
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   6
         Appearance      =   1
      End
      Begin VB.Label lblfraItemFilter 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Select Item Filter Criteria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5040
         TabIndex        =   35
         Top             =   10320
         Visible         =   0   'False
         Width           =   2355
      End
      Begin VB.Label lblHierarchy 
         BackStyle       =   0  'Transparent
         Caption         =   "&Hierarchy"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   90
         TabIndex        =   9
         Top             =   1730
         Width           =   1095
      End
      Begin VB.Label lblSupplier 
         BackStyle       =   0  'Transparent
         Caption         =   "Su&pplier"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   90
         TabIndex        =   34
         Top             =   1230
         Width           =   1095
      End
      Begin VB.Label lblSKU 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "S&KU"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   90
         TabIndex        =   4
         Top             =   855
         Width           =   975
      End
      Begin VB.Label lblAltDescription 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "&Alt. Descr."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   90
         TabIndex        =   2
         Top             =   465
         Width           =   975
      End
      Begin VB.Label lblDescription 
         BackStyle       =   0  'Transparent
         Caption         =   "&Description"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   90
         TabIndex        =   0
         Top             =   90
         Width           =   1035
      End
      Begin VB.Label lblNumberOfMatches 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   10601
         TabIndex        =   33
         Top             =   9720
         Width           =   1935
      End
      Begin VB.Label lblNumberOfMatchesTitle 
         BackStyle       =   0  'Transparent
         Caption         =   "Number of Matches"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8778
         TabIndex        =   32
         Top             =   9740
         Width           =   1815
      End
      Begin VB.Label lblImagePagesTitle 
         BackStyle       =   0  'Transparent
         Caption         =   "Image Page"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   12580
         TabIndex        =   31
         Top             =   9740
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblImagePages 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   13605
         TabIndex        =   30
         Top             =   9700
         Visible         =   0   'False
         Width           =   1395
      End
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   36
      Top             =   10770
      Width           =   15345
      _ExtentX        =   27067
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmItemFilter.frx":20AD
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18706
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   970
            MinWidth        =   970
            Picture         =   "frmItemFilter.frx":3167
            Key             =   "KeyBoard"
            Object.ToolTipText     =   "Show/Hide Keyboard"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-05"
            TextSave        =   "01-Sept-05"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "17:03"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmItemFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*  Module : frmItemFilter
'*  Date   : 17/11/06
'*  Author : DaveF
' $Archive: /Projects/OasysHW/VB/Item Filter/frmItemFilter.frm $
'**********************************************************************************************
'*  Application Summary
'*  ===================
'*  Provides the item fuzzy matching including PIM viewing in an exe called by other
'*      applications using winsock commmunication. Resides in memory after first call and
'*      displays when called.
'*
'**********************************************************************************************
'* $ Author: $ $ Date: $ $ Revision: $
'*
'*  Date        Author      Vers.   Comments
'*  ========    ========    =====   =================================
'*  17/11/06    DaveF       1.0.0   Initial version.
'*
'</CAMH>***************************************************************************************

Option Explicit
                                                
Private Const MODULE_NAME As String = "frmItemFilter"

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Const PRM_KEY_DURESS       As Long = 868
Private Const PRM_LOCAL_DATA_PATH  As Long = 911
Private Const SINGLE_SPACE         As String = " "

' Control Postioning and sizing constants.
Private Const SMALL_FONT_SIZE As Integer = 7
Private Const LARGE_FONT_SIZE As Integer = 8
Private Const SMALL_MESSAGE_FONT_SIZE As Integer = 12
Private Const LARGE_MESSAGE_FONT_SIZE As Integer = 16
Private Const SMALL_FONT_TYPE As String = "Arial"
Private Const LARGE_FONT_TYPE As String = "MS Sans Serif"

Private Const TEXT_BOX_NORMAL_COLOUR As Long = -2147483643
Private Const TEXT_BOX_DIMMED_COLOUR As Long = 12632256

' Match filenames.
Private Const MATCH_FILE As String = "MatchC.exe"
Private Const MATCH_INPUT_FILE As String = "MIN"
Private Const MATCH_OUTPUT_FILE As String = "MOUT"

' Match input and output file paths.
Private Const MATCH_FILES_PATH As String = "C:\"

' MatchC.exe index name constants.
Private Const MATCH_INDEX_DESCRIPTION_ONLY As String = "STKMAS.NDX"
Private Const MATCH_INDEX_DESCRIPTION_CATEGORY As String = "STCTGY.NDX"
Private Const MATCH_INDEX_DESCRIPTION_GROUP As String = "STGRUP.NDX"
Private Const MATCH_INDEX_DESCRIPTION_SUBGROUP As String = "STSGRP.NDX"
Private Const MATCH_INDEX_DESCRIPTION_STYLE As String = "STSTYL.NDX"
Private Const MATCH_INDEX_ALT_DESCRIPTION_ONLY As String = "STKMASEQ.NDX"
Private Const MATCH_INDEX_ALT_DESCRIPTION_CATEGORY As String = "STCTGYEQ.NDX"
Private Const MATCH_INDEX_ALT_DESCRIPTION_GROUP As String = "STGRUPEQ.NDX"
Private Const MATCH_INDEX_ALT_DESCRIPTION_SUBGROUP As String = "STSGRPEQ.NDX"
Private Const MATCH_INDEX_ALT_DESCRIPTION_STYLE As String = "STSTYLEQ.NDX"

' Skus to return from MatchC.exe
Private Const MATCH_SKUS_TO_RETURN As Integer = 255

Private Const WINSOCK_MESSAGE_TERMINATOR As String = "~"

' Milliseconds to pause the Winsock calls.
Private Const SLEEP_TIME As Long = 1

Private mblnInit            As Boolean
Private mblnReload          As Boolean
Private mblnIncObsDel       As Boolean
Private mblnIncNonStk       As Boolean
Private mblnShowUseList     As Boolean
Private mblnShowObsoleteButton As Boolean
Private mblnShowNonStockButton As Boolean
Private mstrOldSupp         As String
Private mlngOldColour       As Long
Private moParent            As Form
Private cItems              As cInventory   'Used to hold items matching criteria.
Private mintDuressKeyCode   As Integer

Dim m_ShowSupplier          As Boolean

Dim mblnShowKeyPad  As Boolean

' Grid constants.
Private Const COL_SKU As Integer = 1
Private Const COL_DESCRIPTION As Integer = 2
Private Const COL_PRICE As Integer = 3
Private Const COL_CATEGORY As Integer = 9
Private Const COL_GROUP As Integer = 10
Private Const COL_SUBGROUP As Integer = 11
Private Const COL_STYLE As Integer = 12
Private Const COL_DISPLAY_ORDER As Integer = 13
Private Const EMPTY_CELL_TAG_TEXT As String = "Empty"

' Hierarchy tree constants and variables.
Private Const CATEGORY_LEVEL As Integer = 1
Private Const GROUP_LEVEL As Integer = 2
Private Const SUBGROUP_LEVEL As Integer = 3
Private Const STYLE_LEVEL As Integer = 4
Private Const CATEGORY_CODE As String = "CA"
Private Const GROUP_CODE As String = "GR"
Private Const SUBGROUP_CODE As String = "SG"
Private Const STYLE_CODE As String = "ST"
Private Const HIERARCHY_LEVEL_CATEGORY As String = "CATEGORY"
Private Const HIERARCHY_LEVEL_GROUP As String = "GROUP"
Private Const HIERARCHY_LEVEL_SUBGROUP As String = "SUBGROUP"
Private Const HIERARCHY_LEVEL_STYLE As String = "STYLE"
Private Const HIERARCHY_NODE_SELECTED As String = "1"
Private Const HIERARCHY_NODE_NOT_SELECTED As String = "0"

' Image constants.
Private Const SMALL_IMAGE_FILE_EXTENSION As String = ".jpg"
Private Const GRID_IMAGE_ROW_HEIGHT As Long = 114
Private Const GRID_TEXT_ROW_HEIGHT As Long = 30
Private Const GRID_COLUMNS_LOW_RES As Long = 3
Private Const GRID_COLUMNS_HIGH_RES As Long = 4

' Status text constants.
Private Const STATUS_TEXT_SEARCH_OPTIONS As String = "Use the cursor or tab keys to move or press enter to search"
Private Const STATUS_TEXT_SEARCH_OPTIONS_SUPPLIER As String = "Use the cursor keys to select Supplier or tab keys to move"
Private Const STATUS_TEXT_HIERARCHY As String = "Use the cursor keys to move or press enter to start the search"
Private Const STATUS_TEXT_LIST As String = "Use the cursor keys to move or press enter to select the item. Press Escape to return to search options"
Private Const STATUS_TEXT_IMAGES As String = "Use the cursor keys to move or press enter to select the item. Press Escape to return to search options"
Private Const STATUS_TEXT_BUTTON_VIEW_LARGE_IMAGE As String = "Press enter to load the large image for the selected product"
Private Const STATUS_TEXT_BUTTON As String = "Use the cursor keys to move or press enter to activate the button"
Private Const STATUS_TEXT_BUTTON_RESET As String = "Use the cursor keys to move or press enter to reset the search options"
Private Const STATUS_TEXT_BUTTON_CLOSE As String = "Use the cursor keys to move or press enter to close the item enquiry"
Private Const STATUS_TEXT_BUTTON_VIEW_IMAGES As String = "Use the cursor keys to move or press enter to view the item images"
Private Const STATUS_TEXT_BUTTON_VIEW_LIST As String = "Use the cursor keys to move or press enter to view the item list"
Private Const STATUS_TEXT_BUTTON_SEARCH As String = "Use the cursor keys to move or press enter to start the search"
Private Const STATUS_TEXT_BUTTON_SELECT As String = "Use the cursor keys to move or press enter select the current item"
Private Const STATUS_TEXT_BUTTON_RELOAD_DATA As String = "Press enter to reload the product hierarchy data"

Private mlngCatCounter As Long
Private mlngGrpCounter As Long
Private mlngSGrpCounter As Long
Private mlngStyleCounter As Long
Private mstrCatNo As String
Private mstrGrpNo As String
Private mstrSGrpNo As String
Private mstrStyleNo As String

Private colSearchCriteria As Collection

Private Enum FieldType
    ftCategory = 1
    ftGroup = 2
    ftSubGroup = 3
    ftStyle = 4
    ftDescription = 5
End Enum

' Heirarchy level at which to view images.
Private Enum ViewImagesLevel
    vilCategory = 1
    vilGroup = 2
    vilSubGroup = 3
    vilStyle = 4
    vilNotKnown = 5
End Enum
Private mvilViewImagesLevel As ViewImagesLevel

' Screen resolution.
Private Enum ScreenResolution
    srLow = 1
    srHigh = 2
End Enum
Private msrScreenResolution As ScreenResolution

' SKU enquiry type, PIM items only or full product listing.
Public Enum EnquiryType
    etPIMOnly = 1
    etFullItems = 2
    etNotSet = 3
End Enum
Private metEnquiryType         As EnquiryType
Private metLoadTreeEnquiryType As EnquiryType

' Image grid load type used when scrolling to a new set of images when the number of search results exceeds the number
'   of images allowed to be loaded at a time, or if finding an item' simage that has not yet been loaded.
Private Enum ImageGridLoadType
    gisScrollDown = 1
    gisScrollUp = 2
    gisImageFind = 3
    gisNone = 4
End Enum
Private mlngLoadedImageRow As Long
Private mintImagePage As Integer
Private mblnLoadingImages As Boolean

' Path to images.
Private mstrPIMImagePath As String
Private mstrFullImagePath As String

' The number of images to load in to the grid page at a time.
Private mlngImagesPerPage As Long

' Whether to stop the load of images when a new search is invoked.
Private mblnStopLoad As Boolean

Private mblnHierarchyChanged      As Boolean
Private mblnGridMouseClick        As Boolean
Private mblnGridMouseDblClick     As Boolean
Private mblnQueryingHierarchyData As Boolean
Private mblnIsControlLoading      As Boolean

Private mcnnDBConnection As ADODB.Connection
Private WithEvents mrecHierarchyData As ADODB.Recordset
Attribute mrecHierarchyData.VB_VarHelpID = -1

Private mlngWinsockItemFilterPort As Long
Private mblnTCPIPOpen             As Boolean
Private mintWinsockControls       As Integer
Private mstrWinsockMessage        As String
Private mblnIsFirstWinsockData    As Boolean
Private mblnShowItemFilter        As Boolean

Private mblnDataRefreshed         As Boolean
Private mstrRefreshTime           As String

Public mblnViewOnly As Boolean

Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

Dim etEnquiryType As EnquiryType
Dim ScreenControl As Control
Dim oWorkStationBo As cWorkStation
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
   
    etEnquiryType = metEnquiryType
    lstMatches.Visible = False
    sprdThumbnails.Visible = False
    lblImagePages.Caption = ""
    mblnIsFirstWinsockData = True
    mblnDataRefreshed = True
    mblnShowObsoleteButton = True
    mblnShowNonStockButton = True
    
    ' Initialise the setup.
    Call Initialise

    'Retrieve Workstation control and check if Touch Screen
    Set oWorkStationBo = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWorkStationBo.AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, goSession.CurrentEnterprise.IEnterprise_WorkstationID)
    Call oWorkStationBo.AddLoadField(FID_WORKSTATIONCONFIG_UseTouchScreen)
    Call oWorkStationBo.LoadMatches
    If (oWorkStationBo.UseTouchScreen = False) Then sbStatus.Panels("KeyBoard").Picture = Nothing
    mblnShowKeyPad = False
    Set oWorkStationBo = Nothing
    
    If (msrScreenResolution = srLow) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Low Res")
        Me.Left = -30
        Me.Top = -130
        Me.Height = 9174
        Me.Width = 12090
        fraItemFilter.Left = 110
        fraItemFilter.Top = 106
        fraItemFilter.Height = 8125
        fraItemFilter.Width = 11760
        lblLoadingHierarchyMessage.FontName = LARGE_FONT_TYPE
        lblLoadingHierarchyMessage.FontSize = SMALL_MESSAGE_FONT_SIZE
        lblLoadingHierarchyMessage.FontBold = True
        lblLoadingImagesMessage.FontName = LARGE_FONT_TYPE
        lblLoadingImagesMessage.FontSize = SMALL_MESSAGE_FONT_SIZE
        lblLoadingImagesMessage.FontBold = True
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Hi Res")
        Me.Left = -30
        Me.Top = -80
        Me.Height = 11625
        Me.Width = 15435
        fraItemFilter.Left = 116
        fraItemFilter.Top = 128
        fraItemFilter.Height = 10480
        fraItemFilter.Width = 15090
        lblLoadingHierarchyMessage.FontName = LARGE_FONT_TYPE
        lblLoadingHierarchyMessage.FontSize = LARGE_MESSAGE_FONT_SIZE
        lblLoadingImagesMessage.FontName = LARGE_FONT_TYPE
        lblLoadingImagesMessage.FontSize = LARGE_MESSAGE_FONT_SIZE
    End If

    ' Center the control.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Centre Controls")
    fraHierarchyLoading.Left = fraItemFilter.Left
    fraHierarchyLoading.Top = fraItemFilter.Top
    fraHierarchyLoading.Height = fraItemFilter.Height
    fraHierarchyLoading.Width = fraItemFilter.Width
    lblLoadingHierarchyMessage.Left = (fraHierarchyLoading.Width - lblLoadingHierarchyMessage.Width) / 2
    lblLoadingHierarchyMessage.Top = (fraHierarchyLoading.Height - lblLoadingHierarchyMessage.Height) / 2
    shpMessage.Left = (fraHierarchyLoading.Width - shpMessage.Width) / 2
    shpMessage.Top = (fraHierarchyLoading.Height - shpMessage.Height) / 2
    fraImagesLoading.Left = (fraItemFilter.Width - fraImagesLoading.Width) / 2
    fraImagesLoading.Top = (fraItemFilter.Height - fraImagesLoading.Height) / 2
    
    ' Test if the hierarchy is still loading.
    If (mblnIsControlLoading = True) Then
        fraHierarchyLoading.Visible = True
    End If
    
    ' Test the screen resolution.

    ' If the screen resolution is below 1024x768 then use the small screen and small font, else use the opposite.
    If (msrScreenResolution = srLow) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Low Res-Set Positions")
        ' Small screen.
        For Each ScreenControl In Me.Controls
            DoEvents
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setting Controls:" & ScreenControl.Name)
            If (ScreenControl.Name = "tvwHierarchyPIM") Or (ScreenControl.Name = "tvwHierarchyFull") Then
                ScreenControl.Font.Name = SMALL_FONT_TYPE
                ScreenControl.Font.Size = SMALL_FONT_SIZE
            ElseIf (ScreenControl.Name = "lstMatches") Then
                ScreenControl.ListApplyTo = ListApplyToDefault
                ScreenControl.FontName = SMALL_FONT_TYPE
                ScreenControl.FontSize = SMALL_FONT_SIZE
            ElseIf (ScreenControl.Name <> "adodcSearch") And (ScreenControl.Name <> "sbStatus") And _
                    (ScreenControl.Name <> "ilsThumbnailImages") And (ScreenControl.Name <> "tmrLoadImages") And _
                    (ScreenControl.Name <> "tmrLoadHierarchy") And (ScreenControl.Name <> "tmrBuildTreeData") And _
                    (ScreenControl.Name <> "shpMessage") And (ScreenControl.Name <> "shpMessage2") And _
                    (ScreenControl.Name <> "lblLoadingHierarchyMessage") And _
                    (ScreenControl.Name <> "lblLoadingImagesMessage") And (ScreenControl.Name <> "wsckComms") And _
                    (ScreenControl.Name <> "tmrRefresh") And (ScreenControl.Name <> "ucKeyPad") Then
                ScreenControl.FontName = SMALL_FONT_TYPE
                ScreenControl.FontSize = SMALL_FONT_SIZE
            End If
        Next ScreenControl
        lblDescription.Left = 40
        lblDescription.Top = 90
        lblDescription.Height = 240
        lblDescription.Width = 1035
        lblAltDescription.Left = 40
        lblAltDescription.Top = 360
        lblAltDescription.Height = 240
        lblAltDescription.Width = 1095
        lblSKU.Left = 40
        lblSKU.Top = 636
        lblSKU.Height = 240
        lblSKU.Width = 1095
        lblSupplier.Left = 40
        lblSupplier.Top = 917
        lblSupplier.Height = 240
        lblSupplier.Width = 1095
        lblHierarchy.Left = 40
        lblHierarchy.Top = 1380
        lblHierarchy.Height = 240
        lblHierarchy.Width = 1095
        txtDescription.Left = 940
        txtDescription.Top = 60
        txtDescription.Height = 240
        txtDescription.Width = 2600
        txtAltDescription.Left = 940
        txtAltDescription.Top = 333
        txtAltDescription.Height = 240
        txtAltDescription.Width = 2600
        txtSKU.Left = 940
        txtSKU.Top = 606
        txtSKU.Height = 240
        txtSKU.Width = 2600
        cboSupplier.Left = 940
        cboSupplier.Top = 870
        cboSupplier.Height = 255
        cboSupplier.Width = 2600
        tvwHierarchyPIM.Left = 40
        tvwHierarchyPIM.Top = 1580
        tvwHierarchyPIM.Height = 6120
        tvwHierarchyPIM.Width = 3500
        tvwHierarchyFull.Left = 40
        tvwHierarchyFull.Top = 1580
        tvwHierarchyFull.Height = 6120
        tvwHierarchyFull.Width = 3500
        lstMatches.Left = 3600
        lstMatches.Top = 45
        lstMatches.Height = 7550
        lstMatches.Width = 8100
        lstMatches.ColumnHeaderHeight = 360
        lstMatches.ListApplyTo = ListApplyToColHeaders
        lstMatches.MultiLine = MultiLineMultipleLine
        lstMatches.Col = 0
        lstMatches.ColWidth = 7
        lstMatches.Col = 1
        lstMatches.ColWidth = 7.4
        lstMatches.Col = 2
        lstMatches.ColWidth = 45
        lstMatches.Col = 3
        lstMatches.ColWidth = 9
        lstMatches.Col = 4
        lstMatches.ColWidth = 8.6
        lstMatches.Col = 5
        lstMatches.ColWidth = 8.6
        lstMatches.Col = 6
        lstMatches.ColWidth = 8.8
        lstMatches.Col = 7
        lstMatches.ColWidth = 10
        sprdThumbnails.Left = 3600
        sprdThumbnails.Top = 45
        sprdThumbnails.Height = 7290
        sprdThumbnails.Width = 8100
        sprdThumbnails.MaxCols = 3
        lblNumberOfMatchesTitle.Caption = "No. of Matches"
        lblNumberOfMatchesTitle.Left = 7540
        lblNumberOfMatchesTitle.Top = 7450
        lblNumberOfMatchesTitle.Height = 255
        lblNumberOfMatchesTitle.Width = 1290
        lblNumberOfMatches.Left = 8760
        lblNumberOfMatches.Top = 7410
        lblNumberOfMatches.Height = 255
        lblNumberOfMatches.Width = 1200
        lblImagePagesTitle.Left = 9990
        lblImagePagesTitle.Top = 7450
        lblImagePagesTitle.Height = 255
        lblImagePagesTitle.Width = 1000
        lblImagePages.Left = 10890
        lblImagePages.Top = 7410
        lblImagePages.Height = 255
        lblImagePages.Width = 800
        cmdViewType.Left = 50
        cmdViewType.Top = 7770
        cmdViewType.Height = 300
        cmdViewType.Width = 1440
        cmdEnlarge.Left = 1570
        cmdEnlarge.Top = 7770
        cmdEnlarge.Height = 300
        cmdEnlarge.Width = 1180
        cmdSelect.Left = 4860
        cmdSelect.Top = 7410
        cmdSelect.Height = 300
        cmdSelect.Width = 1040
        cmdUseList.Left = 5956
        cmdUseList.Top = 7410
        cmdUseList.Height = 300
        cmdUseList.Width = 1450
        cmdReloadData.Left = 3620
        cmdReloadData.Top = 7410
        cmdReloadData.Height = 300
        cmdReloadData.Width = 1160
        cmdRelatedItem.Left = 2810
        cmdRelatedItem.Top = 7770
        cmdRelatedItem.Height = 300
        cmdRelatedItem.Width = 1350
        cmdOrdersIn.Left = 4270
        cmdOrdersIn.Top = 7770
        cmdOrdersIn.Height = 300
        cmdOrdersIn.Width = 1540
        cmdReset.Left = 1440
        cmdReset.Top = 1200
        cmdReset.Height = 280
        cmdReset.Width = 1000
        cmdSearch.Left = 2510
        cmdSearch.Top = 1200
        cmdSearch.Height = 280
        cmdSearch.Width = 1040
        chkActiveOnly.Left = 5956
        chkActiveOnly.Top = 7770
        chkActiveOnly.Height = 300
        chkActiveOnly.Width = 1450
        chkIncludeNonStock.Left = 7620
        chkIncludeNonStock.Top = 7770
        chkIncludeNonStock.Height = 300
        chkIncludeNonStock.Width = 1680
        cmdDetails.Left = 9460
        cmdDetails.Top = 7770
        cmdDetails.Height = 300
        cmdDetails.Width = 1100
        cmdClose.Left = 10675
        cmdClose.Top = 7770
        cmdClose.Height = 300
        cmdClose.Width = 1020
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Hi Res-Set Positions")
        ' Large screen.
        For Each ScreenControl In Me.Controls
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setting Controls:" & ScreenControl.Name)
            DoEvents
            If (ScreenControl.Name <> "tvwHierarchyPIM") And (ScreenControl.Name <> "tvwHierarchyFull") And _
                    (ScreenControl.Name <> "adodcSearch") And (ScreenControl.Name <> "sbStatus") And _
                    (ScreenControl.Name <> "ilsThumbnailImages") And (ScreenControl.Name <> "tmrLoadImages") And _
                    (ScreenControl.Name <> "tmrLoadHierarchy") And (ScreenControl.Name <> "tmrBuildTreeData") And _
                    (ScreenControl.Name <> "lblLoadingHierarchyMessage") And (ScreenControl.Name <> "shpMessage") And _
                    (ScreenControl.Name <> "shpMessage2") And (ScreenControl.Name <> "lblLoadingImagesMessage") And _
                    (ScreenControl.Name <> "wsckComms") And (ScreenControl.Name <> "tmrRefresh") And (ScreenControl.Name <> "ucKeyPad") Then
                ScreenControl.FontSize = LARGE_FONT_SIZE
                ScreenControl.FontName = LARGE_FONT_TYPE
            ElseIf (ScreenControl.Name = "tvwHierarchyPIM") Or (ScreenControl.Name = "tvwHierarchyFull") Then
                ScreenControl.Font.Size = LARGE_FONT_SIZE
                ScreenControl.Font.Name = LARGE_FONT_TYPE
            End If
        Next ScreenControl
        lblDescription.Left = 90
        lblDescription.Top = 90
        lblAltDescription.Left = 90
        lblAltDescription.Top = 465
        lblSKU.Left = 90
        lblSKU.Top = 855
        lblSupplier.Left = 90
        lblSupplier.Top = 1230
        lblHierarchy.Left = 90
        lblHierarchy.Top = 1730
        txtDescription.Left = 1100
        txtDescription.Top = 90
        txtAltDescription.Left = 1100
        txtAltDescription.Top = 465
        txtSKU.Left = 1100
        txtSKU.Top = 855
        cboSupplier.Left = 1100
        cboSupplier.Top = 1230
        tvwHierarchyPIM.Left = 80
        tvwHierarchyPIM.Top = 2030
        tvwHierarchyFull.Left = 80
        tvwHierarchyFull.Top = 2030
        lstMatches.Left = 5140
        lstMatches.Top = 90
        lstMatches.Height = 9570
        sprdThumbnails.Left = 5140
        sprdThumbnails.Top = 90
        sprdThumbnails.Height = 9570
        sprdThumbnails.Width = 9855
        sprdThumbnails.MaxCols = 4
        sprdThumbnails.Col = 4
        sprdThumbnails.CellType = CellTypePicture
        sprdThumbnails.TypeHAlign = TypeHAlignCenter
        sprdThumbnails.TypeVAlign = TypeVAlignCenter
        lblNumberOfMatchesTitle.Left = 8729
        lblNumberOfMatchesTitle.Top = 9740
        lblNumberOfMatches.Left = 10553
        lblNumberOfMatches.Top = 9700
        lblImagePagesTitle.Left = 12580
        lblImagePagesTitle.Top = 9740
        lblImagePages.Left = 13605
        lblImagePages.Top = 9700
        cmdViewType.Left = 80
        cmdViewType.Top = 10020
        cmdEnlarge.Left = 1708
        cmdEnlarge.Top = 10020
        cmdSelect.Left = 2916
        cmdSelect.Top = 10020
        cmdUseList.Left = 4024
        cmdUseList.Top = 10020
        cmdReloadData.Left = 5542
        cmdReloadData.Top = 9700
        cmdRelatedItem.Left = 5542
        cmdRelatedItem.Top = 10020
        cmdOrdersIn.Left = 7030
        cmdOrdersIn.Top = 10020
        cmdReset.Left = 2240
        cmdReset.Top = 1590
        cmdSearch.Left = 3410
        cmdSearch.Top = 1590
        chkActiveOnly.Left = 8778
        chkActiveOnly.Top = 10020
        chkIncludeNonStock.Left = 10601
        chkIncludeNonStock.Top = 10020
        cmdDetails.Left = 12544
        cmdDetails.Top = 10020
        cmdClose.Left = 13800
        cmdClose.Top = 10020
    End If

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Translate Status")
    Call TranslateStatus

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End-Form_Load")

End Sub

Private Sub Form_Activate()

Const PROCEDURE_NAME As String = "Form_Activate"
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Activate Item Filter")

    If (msrScreenResolution = srLow) Then
        Me.Left = -30
        Me.Top = -130
        Me.Height = 9174
        Me.Width = 12090
        fraItemFilter.Left = 110
        fraItemFilter.Top = 106
        fraItemFilter.Height = 8125
        fraItemFilter.Width = 11760
        lblLoadingHierarchyMessage.FontName = LARGE_FONT_TYPE
        lblLoadingHierarchyMessage.FontSize = SMALL_MESSAGE_FONT_SIZE
        lblLoadingHierarchyMessage.FontBold = True
        lblLoadingImagesMessage.FontName = LARGE_FONT_TYPE
        lblLoadingImagesMessage.FontSize = SMALL_MESSAGE_FONT_SIZE
        lblLoadingImagesMessage.FontBold = True
    Else
        Me.Left = -30
        Me.Top = -80
        Me.Height = 11625
        Me.Width = 15435
        fraItemFilter.Left = 116
        fraItemFilter.Top = 128
        fraItemFilter.Height = 10480
        fraItemFilter.Width = 15090
        lblLoadingHierarchyMessage.FontName = LARGE_FONT_TYPE
        lblLoadingHierarchyMessage.FontSize = LARGE_MESSAGE_FONT_SIZE
        lblLoadingImagesMessage.FontName = LARGE_FONT_TYPE
        lblLoadingImagesMessage.FontSize = LARGE_MESSAGE_FONT_SIZE
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

Const PROCEDURE_NAME As String = "Form_KeyDown"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, KeyCode)
    
    If (mintDuressKeyCode <> 0) And (KeyCode = mintDuressKeyCode) Then
        Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_EVENT_DURESS)
        Exit Sub
    End If

    If (Shift = 0) Then
        Select Case KeyCode
            ' View Type, images or grid.
            Case vbKeyF2
                KeyCode = 0
                If cmdViewType.Visible Then
                    cmdViewType.Value = True
                End If

            ' Reset.
            Case vbKeyF3
                KeyCode = 0
                cmdReset.Value = True

            ' Enlarged image view.
            Case vbKeyF4
                KeyCode = 0
                If cmdEnlarge.Visible Then
                    cmdEnlarge.Value = True
                End If

            ' Select SKU.
            Case vbKeyF5
                KeyCode = 0
                If cmdSelect.Visible Then
                    cmdSelect.Value = True
                End If

            ' View related item.
            Case vbKeyF6
                KeyCode = 0
                If (cmdRelatedItem.Visible And cmdRelatedItem.Enabled) Then
                    cmdRelatedItem.Value = True
                End If

            ' Search.
            Case vbKeyF7
                KeyCode = 0
                cmdSearch.Value = True

            ' Orders due in.
            Case vbKeyF8
                KeyCode = 0
                If cmdOrdersIn.Visible Then
                    cmdOrdersIn.Value = True
                End If

            ' Include obsolete and deleted items.
            Case vbKeyF9
                KeyCode = 0
                If (chkActiveOnly.Value = vbChecked) Then
                    chkActiveOnly.Value = vbUnchecked
                Else
                    chkActiveOnly.Value = vbChecked
                End If

            ' Include Non-Stock items.
            Case vbKeyF10
                KeyCode = 0
                If (chkIncludeNonStock.Value = vbChecked) Then
                    chkIncludeNonStock.Value = vbUnchecked
                Else
                    chkIncludeNonStock.Value = vbChecked
                End If

            ' Item details.
            Case vbKeyF11
                KeyCode = 0
                If cmdDetails.Visible Then
                    cmdDetails.Value = True
                End If

            ' Cancel.
            Case vbKeyF12
                KeyCode = 0
                If cmdClose.Visible Then
                    cmdClose.Value = True
                End If

            Case vbKeyTab
                FirstActiveControl.SetFocus

        End Select
    ElseIf (Shift = 1) Then
        If (KeyCode = vbKeyTab) Then
            LastActiveControl.SetFocus
        End If
    ElseIf (Shift = 2) Then
        Select Case (KeyCode)
            ' Use list.
            Case vbKeyF5
                KeyCode = 0
                If cmdUseList.Visible Then
                    cmdUseList.Value = True
                End If
        End Select
    End If

End Sub

Private Sub LookupSKU(ByVal strEAN As String)

Dim oEANItem As cEAN
Dim colItems As Collection
Dim strEnquiryEXE As String
    
    Set oEANItem = goSession.Database.CreateBusinessObject(CLASSID_EAN)
    Call oEANItem.AddLoadFilter(CMP_EQUAL, FID_EAN_EANNumber, Right(String$(16, "0") & strEAN, 16))
    Set colItems = oEANItem.LoadMatches
    
    If (colItems.Count < 1) Then
        Call MsgBoxEx("Item Not Found:" & vbNewLine & vbNewLine & strEAN, vbInformation, "Item Lookup", , , , , RGBMsgBox_WarnColour)
    Else
        Set oEANItem = colItems(1)
        txtDescription.Text = vbNullString
        txtSKU.Text = oEANItem.PartCode
        cmdSearch.Value = True
    End If
    
    Set colItems = Nothing
    Set oEANItem = Nothing
    
End Sub

' Initialise the setup.
Private Sub Initialise()
                      
Const PROCEDURE_NAME As String = "Initialise"

Dim oSupp   As cSuppliers
Dim oBOCol  As Collection
Dim lItemNo As Long
Dim strViewImagesLevel As String
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceIn)
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    mblnViewOnly = False
    mblnIsControlLoading = True
            
    If (mblnInit) Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    Set goRoot = GetRoot
    Set goDatabase = goSession.Database
    
    Call CreateErrorObject(SYSTEM_NAME & "{" & App.Title & "}", VBA.Err, Err)
    Call InitialiseStatusBar(sbStatus)
    
    ' Test the screen resolution.
    If (Screen.Height / Screen.TwipsPerPixelY < 768) Or (Screen.Width / Screen.TwipsPerPixelX < 1024) Then
        msrScreenResolution = srLow
    Else
        msrScreenResolution = srHigh
    End If
    
    ' Get the number of images to load in to the grid at a time.
    mlngImagesPerPage = goSession.GetParameter(PRM_PIM_IMAGES_TO_LOAD)
    ' Convert the value to be divisible by the number of columns in the grid so all cells in last row will be filled
    '   if more items are available than are allowed to be loaded at a time.
    If (msrScreenResolution = srLow) Then
        mlngImagesPerPage = (CLng(mlngImagesPerPage / GRID_COLUMNS_LOW_RES)) * GRID_COLUMNS_LOW_RES
    Else
        mlngImagesPerPage = (CLng(mlngImagesPerPage / GRID_COLUMNS_HIGH_RES)) * GRID_COLUMNS_HIGH_RES
    End If
    
    ' Get the ports to use for the winsock communication and setup up the winsock control.
    mlngWinsockItemFilterPort = goSession.GetParameter(PRM_PIM_ITEM_FILTER_WINSOCK_PORT)
    Call SetupWinsock
    
    mstrRefreshTime = Format(goSession.GetParameter(PRM_PIM_REFRESH_TIME), "00")
    If (IsNumeric(mstrRefreshTime) = True) And (mstrRefreshTime <> "") Then
        mstrRefreshTime = mstrRefreshTime & ":00:00"
    End If
    
    ' Get the paths to the image folders.
    mstrPIMImagePath = goSession.GetParameter(PRM_PIM_ONLY_IMAGE_PATH)
    If (Right(mstrPIMImagePath, 1) <> "\") Then
        mstrPIMImagePath = mstrPIMImagePath & "\"
    End If
    mstrFullImagePath = goSession.GetParameter(PRM_PIM_FULL_IMAGE_PATH)
    If (Right(mstrFullImagePath, 1) <> "\") Then
        mstrFullImagePath = mstrFullImagePath & "\"
    End If
    
    ' Get the required level at which to view the images in the hierarchy.
    strViewImagesLevel = goSession.GetParameter(PRM_PIM_VIEW_IMAGES_LEVEL)
    Select Case UCase(strViewImagesLevel)
        Case HIERARCHY_LEVEL_CATEGORY
            mvilViewImagesLevel = vilCategory
        Case HIERARCHY_LEVEL_GROUP
            mvilViewImagesLevel = vilGroup
        Case HIERARCHY_LEVEL_SUBGROUP
            mvilViewImagesLevel = vilSubGroup
        Case HIERARCHY_LEVEL_STYLE
            mvilViewImagesLevel = vilStyle
        Case Else
            mvilViewImagesLevel = vilNotKnown
    End Select
'    metEnquiryType = etNotSet
    
    ' Build the Hierarchy trees for the PIM and Full product hierarchies.
    metLoadTreeEnquiryType = etPIMOnly
    tmrLoadHierarchy.Enabled = True
    
    Me.BackColor = goSession.GetParameter(PRM_QUERY_BORDERCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    
    fraItemFilter.BackColor = RGBQuery_BackColour
    fraHierarchyLoading.BackColor = RGBQuery_BackColour
    fraImagesLoading.BackColor = RGBQuery_BackColour
    lblLoadingHierarchyMessage.Caption = "The Item Enquiry is loading" & vbCrLf & vbCrLf & _
                            "Please wait for the load to complete"
    lblLoadingImagesMessage.Caption = "The Item Images are loading" & vbCrLf & vbCrLf & _
                            "Please wait for the load to complete"
    cmdReset.BackColor = RGBQuery_BackColour
    cmdClose.BackColor = RGBQuery_BackColour
    cmdDetails.BackColor = RGBQuery_BackColour
    cmdOrdersIn.BackColor = RGBQuery_BackColour
    cmdSearch.BackColor = RGBQuery_BackColour
    cmdSelect.BackColor = RGBQuery_BackColour
    cmdUseList.BackColor = RGBQuery_BackColour
    cmdReloadData.BackColor = RGBQuery_BackColour
    cmdRelatedItem.BackColor = RGBQuery_BackColour
    cmdViewType.BackColor = RGBQuery_BackColour
    cmdEnlarge.BackColor = RGBQuery_BackColour
    
    chkActiveOnly.BackColor = vbRed
    chkIncludeNonStock.BackColor = vbRed
    mblnIncObsDel = False
    mblnIncNonStk = False
    
    lstMatches.BorderColor = RGBQuery_BackColour
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstMatches.ListApplyTo = ListApplyToEvenRows
    lstMatches.BackColor = lstMatches.BackColor
    lstMatches.ListApplyTo = ListApplyToOddRows
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstMatches.Row = -1
    lstMatches.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)
    sprdThumbnails.BackColor = RGB_WHITE
    sprdThumbnails.GrayAreaBackColor = RGB_WHITE
    sprdThumbnails.GridColor = RGB_WHITE
    
    mintDuressKeyCode = goSession.GetParameter(PRM_KEY_DURESS)
    
    If (cboSupplier.Enabled) Then
        cboSupplier.AddItem "*ALL*" & vbTab & "*ALL*"
        cboSupplier.Row = cboSupplier.NewIndex
        cboSupplier.Text = cboSupplier.List(cboSupplier.Row)
        
        Set oSupp = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERS)
        Call oSupp.GetList
        
        While Not oSupp.EndOfList
            cboSupplier.AddItem oSupp.EntryString
            oSupp.MoveNext
        Wend
        
        Set oSupp = Nothing
    End If

    mblnInit = True
    Screen.MousePointer = vbDefault
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlTraceOut)

End Sub

Public Sub InitialiseStatusBar(ByRef sbInfo As StatusBar)
    
    sbInfo.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbInfo.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbInfo.Panels(PANEL_WSID + 1).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbInfo.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbInfo.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")

End Sub

Private Sub SetupSupplier(ByVal strSupplierCode As String)
    
    If (strSupplierCode <> mstrOldSupp) Then
        mblnReload = True 'force apply of search even if keyword/SKU not entered
        Call DebugMsg(MODULE_NAME, "Let_SupplierNo", endlDebug, "Switching supplier on Item Filter - '" & strSupplierCode & "'")
        
        If (LenB(strSupplierCode) <> 0) Then 'select supplier from list
            cboSupplier.SearchText = strSupplierCode
            cboSupplier.ColumnSearch = 0
            cboSupplier.SearchMethod = SearchMethodExactMatch
            cboSupplier.Action = ActionSearch
            cboSupplier.ListIndex = cboSupplier.SearchIndex
            cboSupplier.Enabled = False
            lstMatches.Clear
            lblNumberOfMatches.Caption = "N/A"
        Else
            cboSupplier.Text = "*ALL*" & vbTab & "*ALL*"
            cboSupplier.Enabled = True
            lstMatches.Clear
            lblNumberOfMatches.Caption = "N/A"
        End If
        
        mstrOldSupp = strSupplierCode
        
    End If
        
End Sub

Private Sub ShowSupplier(ByVal ShowSupplier As Boolean)

    cboSupplier.Visible = ShowSupplier
    lblSupplier.Visible = ShowSupplier
    m_ShowSupplier = ShowSupplier

End Sub

' Populate the passed in variables with the hierarchy information based upon the selected node in the hierarchy tree.
Private Sub GetHierarchyValues(ByRef strCategoryCode As String, ByRef strGroupCode As String, _
                                    ByRef strSubGroupCode As String, strStyleCode As String)

Dim tvwHierarchy As TreeView
Dim nNode        As Node
Dim arrTag()     As String

    On Error GoTo GetHierarchyValues_Error
    
    strCategoryCode = ""
    strGroupCode = ""
    strSubGroupCode = ""
    strStyleCode = ""
    
    If (metEnquiryType = etPIMOnly) Then
        Set tvwHierarchy = tvwHierarchyPIM
    Else
        Set tvwHierarchy = tvwHierarchyFull
    End If
    
    If Not (tvwHierarchy.SelectedItem Is Nothing) Then
        ' Get the selected hierarchy tree node.
        Set nNode = tvwHierarchy.SelectedItem
    
        ' Get the hierarchy level of the selected node.
        Select Case Left(nNode.Tag, 1)
            Case CATEGORY_LEVEL
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
            Case GROUP_LEVEL
                arrTag = Split(nNode.Tag, ",")
                strGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
            Case SUBGROUP_LEVEL
                arrTag = Split(nNode.Tag, ",")
                strSubGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
            Case STYLE_LEVEL
                arrTag = Split(nNode.Tag, ",")
                strStyleCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strSubGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
        End Select
    End If
    
    Set tvwHierarchy = Nothing
    
    Exit Sub
    
GetHierarchyValues_Error:

    Set tvwHierarchy = Nothing
    
End Sub
                                    
Private Sub ResizeColumns()

Dim lngRowNo As Long
Dim strFlag5 As String
Dim vntcoldata As Variant
        
    Call DebugMsg(MODULE_NAME, "Resize Columns", endlDebug)
    DoEvents
        
    lstMatches.Redraw = False
    lstMatches.Row = -1
    lstMatches.ListApplyTo = ListApplyToIndividual
    lstMatches.Col = 0
    lstMatches.AlignH = AlignHCenter
    'lstMatches.ColWidth = 10
    lstMatches.Col = 1
    lstMatches.AlignH = AlignHCenter
    'lstMatches.ColWidth = 50
    lstMatches.Col = 2
    lstMatches.AlignH = AlignHLeft
    'lstMatches.ColWidth = 13
    lstMatches.Col = 3
    lstMatches.AlignH = AlignHRight
    'lstMatches.ColWidth = 14
    lstMatches.Col = 4
    lstMatches.AlignH = AlignHRight
    'lstMatches.ColWidth = 10
    lstMatches.Col = 5
    lstMatches.AlignH = AlignHRight
    'lstMatches.ColWidth = 10
    lstMatches.Col = 6
    lstMatches.AlignH = AlignHCenter
    'lstMatches.ColWidth = 14
    lstMatches.Col = 7
    lstMatches.AlignH = AlignHLeft
    'lstMatches.ColWidth = 14
    lstMatches.Col = 8
    lstMatches.AlignH = AlignHCenter
    lstMatches.ColWidth = 0
    lstMatches.ColHide = True
    lstMatches.Col = 9
    lstMatches.ColWidth = 0
    lstMatches.ColHide = True
    lstMatches.Col = 10
    lstMatches.ColWidth = 0
    lstMatches.ColHide = True
    lstMatches.Col = 11
    lstMatches.ColWidth = 0
    lstMatches.ColHide = True
    lstMatches.Col = 12
    lstMatches.ColWidth = 0
    lstMatches.ColHide = True
    lstMatches.Col = 13
    lstMatches.ColWidth = 0
    lstMatches.ColHide = True
    
    lstMatches.Redraw = True

End Sub

Private Sub TranslateStatus()

Dim lngRowNo As Long
Dim vntcoldata As Variant

    lstMatches.Col = 0
    lstMatches.ColHeaderText = "Status"
    lstMatches.Col = 1
    lstMatches.ColHeaderText = "SKU"
    lstMatches.Col = 2
    lstMatches.ColHeaderText = "Description"
    lstMatches.Col = 3
    lstMatches.ColHeaderText = "Price"
    lstMatches.Col = 4
    lstMatches.ColHeaderText = "On Hand"
    lstMatches.Col = 5
    lstMatches.ColHeaderText = "On Order"
    lstMatches.Col = 6
    lstMatches.ColHeaderText = "Supplier"
    lstMatches.Col = 7
    lstMatches.ColHeaderText = "Supplier Ref No"
    lstMatches.Col = 8
    lstMatches.ColHeaderText = "Type"
    lstMatches.Redraw = False
    lstMatches.Redraw = True 'does not really work here
    DoEvents
    
End Sub

Private Function FirstActiveControl() As Control

Dim lowTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    lowTab = -1
    
    On Error Resume Next
    
    For Each Ctrl In Controls
        If (Ctrl.Visible And Ctrl.Enabled And Ctrl.TabStop) Then
            If (Err.Number = 0) Then
                If (Ctrl.TabIndex < lowTab) Or (lowTab = -1) Then
                    lowTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                    
                End If
            End If
            
            Err.Clear
            
        End If
        
    Next Ctrl
    
    On Error GoTo 0
    Set FirstActiveControl = retCtrl
    
End Function

Private Function LastActiveControl() As Control

Dim highTab  As Long
Dim Ctrl    As Control
Dim retCtrl As Control

    highTab = -1
    
    On Error Resume Next
    
    For Each Ctrl In Controls
        If (Ctrl.Visible And Ctrl.Enabled And Ctrl.TabStop) Then
            If (Err.Number = 0) Then
                If (Ctrl.TabIndex > highTab) Or (highTab = -1) Then
                    highTab = Ctrl.TabIndex
                    Set retCtrl = Ctrl
                    
                End If
            End If
            
            Err.Clear
            
        End If
        
    Next Ctrl
    
    On Error GoTo 0
    Set LastActiveControl = retCtrl
    
End Function

Private Sub chkActiveOnly_Click()
    
    mblnReload = True
    mblnIncObsDel = chkActiveOnly.Value
    If mblnIncObsDel Then
        chkActiveOnly.BackColor = vbGreen
    Else
        chkActiveOnly.BackColor = vbRed
    End If
    
End Sub

Private Sub chkActiveOnly_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON)
    
End Sub

Private Sub chkActiveOnly_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
        
    End If
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
        
    End If

End Sub

Private Sub chkIncludeNonStock_click()

    mblnReload = True
    mblnIncNonStk = chkIncludeNonStock.Value
    If mblnIncNonStk Then
        chkIncludeNonStock.BackColor = vbGreen
    Else
        chkIncludeNonStock.BackColor = vbRed
    End If
    
End Sub

Private Sub chkIncludeNonStock_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON)
    
End Sub

Private Sub chkIncludeNonStock_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cboSupplier_Change()

    mblnReload = True
    
End Sub

Private Sub cboSupplier_GotFocus()
    
    Call DebugMsg(MODULE_NAME, "cboSupplier_GotFocus", endlDebug, "Start")

    UpdateStatus (STATUS_TEXT_SEARCH_OPTIONS_SUPPLIER)
    cboSupplier.ListDown = True
    
    ' If in PIM enquiry type mode then change to Full products mode.
    If (metEnquiryType = etPIMOnly) Then
        metEnquiryType = etFullItems
        Call SwitchHierarchyTree
    End If

End Sub

Private Sub cboSupplier_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmdDetails_Click()

Dim strPartCode   As String
Dim strEnquiryEXE As String
        
    On Error GoTo Bad_Enquiry
    
    cmdDetails.FontBold = False
    lstMatches.Row = lstMatches.ListIndex
    lstMatches.Col = 0
    strPartCode = lstMatches.ColText
    strEnquiryEXE = goSession.GetParameter(PRM_ENQUIRY_EXE)
    strEnquiryEXE = strEnquiryEXE & " " & goSession.CreateCommandLine("SKU=" & strPartCode)
    Call DebugMsg(MODULE_NAME, "callMenuOption", endlDebug, strEnquiryEXE)
    Call ShellWait(strEnquiryEXE, SW_MAX)
    If (lstMatches.Visible) Then lstMatches.SetFocus
    
    Exit Sub
    
Bad_Enquiry:

    Call MsgBox("Invalid set-up for item enquiry" & vbCrLf & "  " & strEnquiryEXE & vbCrLf & Err.Number & "-" & Err.Description, vbExclamation, "Select Option")
    Call Err.Clear
    Resume Next
    
End Sub

Private Sub cmdDetails_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON)
    cmdDetails.FontBold = True
    
End Sub

Private Sub cmdDetails_LostFocus()

    cmdDetails.FontBold = False
    
End Sub

Private Sub cmdEnlarge_Click()

    ' Show the enlarged image for the current SKU.
    Call LoadLargeImage
        
End Sub

Private Sub cmdEnlarge_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON_VIEW_LARGE_IMAGE)
    cmdEnlarge.FontBold = True

End Sub

Private Sub cmdEnlarge_LostFocus()

    cmdEnlarge.FontBold = False

End Sub

Private Sub cmdRelatedItem_Click()

Dim strSkun As String

    If (sprdThumbnails.Visible = True) Then Call HighlightListItem
    cmdRelatedItem.FontBold = False
    lstMatches.Row = lstMatches.ListIndex
    lstMatches.Col = 1                              'Item Sku number
    frmRelatedItems.SkuNumber = lstMatches.ColText
    lstMatches.Col = 8                              'Item type, S/B/N, set in cmdSearch.Click()
    frmRelatedItems.ItemType = lstMatches.ColText
    frmRelatedItems.ViewOnly = Not cmdSelect.Visible
        
    frmRelatedItems.Show vbModal
    strSkun = frmRelatedItems.SkuNumber
    Unload frmRelatedItems
    
    If (LenB(strSkun) <> 0) Then
        If (lstMatches.Visible) Then lstMatches.SetFocus
        If (Not moParent Is Nothing) Then moParent.KeyPreview = True
        Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_EVENT_APPLY & strSkun)
    Else
        If (lstMatches.Visible) Then
            lstMatches.SetFocus
        End If
    End If

End Sub

Private Sub cmdOrdersIn_Click()

    If (sprdThumbnails.Visible = True) Then Call HighlightListItem
    cmdOrdersIn.FontBold = False
    lstMatches.Row = lstMatches.ListIndex
    lstMatches.Col = 1                              'Item Sku number.
    frmOrdersIn.SkuNumber = lstMatches.ColText
    frmOrdersIn.Show vbModal
    Unload frmOrdersIn
    If (lstMatches.Visible) Then lstMatches.SetFocus

End Sub

Private Sub cmdReloadData_Click()

    If (MsgBox("Refreshing the data will update the product hierarchy tree with" & vbNewLine & _
                "latest hierarchy data, this may take more that 30 seconds." & vbNewLine & vbNewLine & _
                "Do you wish to continue?", vbQuestion + vbYesNo, _
                "Refresh Product Hierarchy Data") = vbYes) Then
        ' Refresh the data.
        Call RefreshTheData
    End If
    
End Sub

Private Sub cmdReloadData_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON_RELOAD_DATA)
    cmdReloadData.FontBold = True
    
End Sub

Private Sub cmdReloadData_LostFocus()

    cmdReloadData.FontBold = False
    
End Sub

Private Sub cmdSearch_Click()

Const PROCEDURE_NAME As String = "cmdSearch_Click"

Dim lngCharPos      As Long
Dim lngWherePos     As Long
Dim lngFlagNo       As Long
Dim vntFlags        As Variant
Dim strLookUp       As String
Dim strValue        As String
Dim strWhereHierarchy As String
Dim strWhereObsDel  As String
Dim strWhereNon     As String
Dim strWhereAlt     As String
Dim strWhere        As String
Dim strWhereDesc    As String
Dim strDescClause   As String
Dim strRow          As String
Dim rsData          As Recordset
Dim fsFileWorker    As FileSystemObject
Dim strFuzzyWords   As String
Dim strFilePath     As String
Dim strIndexFile    As String
Dim strSkuNumber    As String
Dim I               As Integer
Dim lngCurrentByte  As Long
Dim lngFileSize     As Long

Dim aFuzzyWordsBytes(1 To 80)    As Byte
Dim aFilePathBytes(1 To 80)      As Byte
Dim bytWork                      As Byte
Dim abytHierarchyCode(1 To 6)    As Byte
Dim abytHierarchySubCode(1 To 6) As Byte
Dim aInByte(1 To 3)              As Byte
Dim aOutByte(1 To 6)             As Byte
Dim cSkuList                     As Collection
Dim blnFilterData                As Boolean

Dim tvwHierarchy      As TreeView
Dim arrTag()          As String
Dim strHierarchyCode  As String
Dim strCategoryCode   As String
Dim strGroupCode      As String
Dim strSubGroupCode   As String
Dim strStyleCode      As String
Dim strSQL            As String
Dim strSQLWhere       As String
Dim intCounter        As Integer
Dim intSubCodeCounter As Integer
Dim blnUseMatch       As Boolean
Dim lngDisplayOrder   As Long
Dim astrSubCodes()    As String
Dim lngBytePlace      As Long

    On Error GoTo cmdSearch_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    Screen.MousePointer = vbHourglass
    cmdSearch.FontBold = False
    lstMatches.SortState = SortStateSuspend
    lstMatches.Redraw = False
    mblnStopLoad = True
    sprdThumbnails.MaxRows = 0
    
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Enquiry Type - PIM")
        Set tvwHierarchy = tvwHierarchyPIM
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Enquiry Type - Full")
        Set tvwHierarchy = tvwHierarchyFull
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Tree View - " & tvwHierarchy.Name)
    If Not (tvwHierarchy.SelectedItem Is Nothing) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Tree View Node - " & tvwHierarchy.SelectedItem)
    End If
    
    ' Test if the search criteria has changed, if so untag all the nodes on the hierarchy tree.
    If (mblnReload = True) Then
        ' Un select all the hierarchy tree nodes.
        Call UnTagTreeNodes
    End If
    
    ' If the hierarchy selection has changed then test if the selected node has previously been
    '   selected, i.e. data exists in grid. If not then load the data.
    If (mblnHierarchyChanged = True) And (mblnReload = False) Then
        If Not (tvwHierarchy.SelectedItem Is Nothing) Then
            If (Right(tvwHierarchy.SelectedItem.Tag, 1) = HIERARCHY_NODE_SELECTED) Then
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Filter Data")
                blnFilterData = True
            End If
        End If
    End If
    
     ' Reload data as criteria has changed or hierarchy node not previously selected.
    If (mblnReload = True) Or (blnFilterData = False) Then
        
        ' Tag the selected nodes.
        Call TagTreeNodes(tvwHierarchy.SelectedItem)
        
        lstMatches.Clear
        lblNumberOfMatches.BackColor = RGB_WHITE
        lblNumberOfMatches.ForeColor = RGB_BLACK
        lblNumberOfMatches.FontBold = False
        lblNumberOfMatches.Caption = "Searching..."
        cmdDetails.Visible = False
'        ucpbProgress.Visible = True
'        ucpbProgress.Caption1 = "Loading criteria"
        DoEvents
        
        ' If the enquiry type is PIM only then do a normal SELECT WHERE SQL query.
        If (metEnquiryType = etPIMOnly) Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "GET PIM Only data")
        
            ' Build the SQL.
            strSQL = "SELECT a.SKUN, a.CategoryCode, a.GroupCode, a.SubGroupCode, a.StyleCode," & _
                        "if(b.IOBS=1, 'Obs', if (b.IDEL=1, 'Del', if(INON=1, 'Non', if(IRIS=1, 'Sng', '')))) ""Status"", " & _
                        "b.IRIB, b.DESCR, b.PRIC, b.ONHA, b.ONOR, b.SUPP, b.PROD " & _
                        "FROM PIMMAS a INNER JOIN STKMAS b on a.SKUN = b.SKUN "
            ' Build the SQL WHERE clause.
            strSQLWhere = "WHERE "
            Call GetHierarchyValues(strCategoryCode, strGroupCode, strSubGroupCode, strStyleCode)
            If (strCategoryCode <> "") Then
                strSQLWhere = strSQLWhere & "a.CategoryCode = '" & strCategoryCode & "' AND "
            End If
            If (strGroupCode <> "") Then
                strSQLWhere = strSQLWhere & "a.GroupCode = '" & strGroupCode & "' AND "
            End If
            If (strSubGroupCode <> "") Then
                strSQLWhere = strSQLWhere & "a.SubGroupCode = '" & strSubGroupCode & "' AND "
            End If
            If (strStyleCode <> "") Then
                strSQLWhere = strSQLWhere & "a.StyleCode = '" & strStyleCode & "' AND "
            End If
            
            If (mblnIncObsDel = False) Then
                strSQLWhere = strSQLWhere & "(b.IOBS = 0 AND b.IDEL = 0) AND "
            End If
            If (mblnIncNonStk = False) Then
                strSQLWhere = strSQLWhere & "b.INON = 0 AND "
            End If
            If (Right(strSQLWhere, 5) = " AND ") Then
                strSQLWhere = Left(strSQLWhere, Len(strSQLWhere) - 5)
            End If
            If (strSQLWhere = "WHERE ") Then
                strSQLWhere = ""
            End If
            ' Join all the SQL parts.
            strSQL = strSQL & strSQLWhere
            strSQL = strSQL & " ORDER BY a.CategoryCode, a.GroupCode, a.SubGroupCode, a.StyleCode"
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQL)
            
            ' Open the data.
            Set rsData = New Recordset
            rsData.Open strSQL, goDatabase.ConnectionString, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            ' Loop over the data adding it to the list.
            While (Not rsData.EOF)
                strRow = rsData!Status & vbTab & _
                         rsData!SKUN & vbTab & _
                         SINGLE_SPACE & Trim(rsData!descr) & vbTab & _
                         Format(rsData!pric, " 0.00 ") & vbTab & _
                         Format(rsData!onha, " 0 ") & vbTab & _
                         Format(rsData!ONOR, " 0 ") & vbTab & _
                         Format(rsData!SUPP, " 00000 ") & vbTab & _
                         SINGLE_SPACE & rsData!PROD & vbTab

                'Figure out if it's a 's'ingle item, 'b'ulk item or 'n'one.
                If (rsData!Status = "Sng") Then
                    strRow = strRow & "S"
                ElseIf (Not IsNull(rsData!irib)) Then
                    If (rsData!irib > 0) Then
                        strRow = strRow & "B"
                    Else
                        strRow = strRow & "N"
                    End If
                Else
                    strRow = strRow & "N"
                End If
                
                ' Add the hierarchy information.
                strRow = strRow & vbTab & rsData!CategoryCode & vbTab & _
                         rsData!GroupCode & vbTab & _
                         rsData!SubGroupCode & vbTab & _
                         rsData!StyleCode

                lstMatches.AddItem strRow
                rsData.MoveNext
            Wend
            
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "GET Full product data")
            
            strHierarchyCode = ""
            ' If the enquiry type is on full products then use MatchC.EXE to return a weighted list of SKUs if the Description,
            '   Alternative Description or Hierarchy values have been set, else use sql to select from the SKU or Supplier values.
            blnUseMatch = False
            If (Trim(txtDescription.Text) <> "") Or (Trim(txtAltDescription.Text) <> "") Then
                
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Description or Alt-Description set")

                blnUseMatch = True
                
                strFilePath = goSession.GetParameter(PRM_LOCAL_DATA_PATH)
                If (Mid$(strFilePath, Len(strFilePath), 1) <> "\") Then
                    strFilePath = strFilePath & "\"
                End If
                
                Set fsFileWorker = New FileSystemObject
                If fsFileWorker.FileExists(MATCH_FILES_PATH & MATCH_INPUT_FILE) Then
                    Call fsFileWorker.DeleteFile(MATCH_FILES_PATH & MATCH_INPUT_FILE)
                End If
                
                If fsFileWorker.FileExists(MATCH_FILES_PATH & MATCH_OUTPUT_FILE) Then
                    Call fsFileWorker.DeleteFile(MATCH_FILES_PATH & MATCH_OUTPUT_FILE)
                End If
                
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Match files deleted in " & MATCH_FILES_PATH)
                
                ' Test which index file to use.
                If (tvwHierarchy.SelectedItem Is Nothing) Then
                    ' If not using the hierarchy then use the standard index.
                    If (txtAltDescription <> "") Then
                        strIndexFile = strFilePath & MATCH_INDEX_ALT_DESCRIPTION_ONLY
                    Else
                        strIndexFile = strFilePath & MATCH_INDEX_DESCRIPTION_ONLY
                    End If
                Else
                    ' Get the hierarchy code.
                    arrTag = Split(tvwHierarchy.SelectedItem.Tag, ",")
                    strHierarchyCode = arrTag(1)
                    ' Select which hierarchy index to use based upon the branch level of node selected.
                    Select Case Left(tvwHierarchy.SelectedItem.Tag, 1)
                        Case CATEGORY_LEVEL
                            If (txtAltDescription <> "") Then
                                strIndexFile = strFilePath & MATCH_INDEX_ALT_DESCRIPTION_CATEGORY
                            Else
                                strIndexFile = strFilePath & MATCH_INDEX_DESCRIPTION_CATEGORY
                            End If
                        Case GROUP_LEVEL
                            If (txtAltDescription <> "") Then
                                strIndexFile = strFilePath & MATCH_INDEX_ALT_DESCRIPTION_GROUP
                            Else
                                strIndexFile = strFilePath & MATCH_INDEX_DESCRIPTION_GROUP
                            End If
                        Case SUBGROUP_LEVEL
                            If (txtAltDescription <> "") Then
                                strIndexFile = strFilePath & MATCH_INDEX_ALT_DESCRIPTION_SUBGROUP
                            Else
                                strIndexFile = strFilePath & MATCH_INDEX_DESCRIPTION_SUBGROUP
                            End If
                        Case STYLE_LEVEL
                            If (txtAltDescription <> "") Then
                                strIndexFile = strFilePath & MATCH_INDEX_ALT_DESCRIPTION_STYLE
                            Else
                                strIndexFile = strFilePath & MATCH_INDEX_DESCRIPTION_STYLE
                            End If
                    End Select
                End If
                
                For I = 1 To 80
                    If I > Len(strIndexFile) Then
                        aFilePathBytes(I) = &H0
                    Else
                        aFilePathBytes(I) = Asc(Mid$(strIndexFile, I, 1))
                    End If
                Next
                
                ' Setup MATCH_INPUT_FILE.
                If (txtAltDescription <> "") Then
                    strFuzzyWords = UCase(txtAltDescription.Text) & Space(60 - Len(txtAltDescription.Text))
                Else
                    strFuzzyWords = UCase(txtDescription.Text) & Space(60 - Len(txtDescription.Text))
                End If
                For I = 1 To 80
                    If I > Len(strFuzzyWords) Then
                        aFuzzyWordsBytes(I) = &H0
                    Else
                        aFuzzyWordsBytes(I) = Asc(Mid$(strFuzzyWords, I, 1))
                    End If
                Next
            
                Dim MatchInFileNum As Integer
                MatchInFileNum = FreeFile()
                Open MATCH_FILES_PATH & MATCH_INPUT_FILE For Binary As MatchInFileNum
                bytWork = 1
                Put MatchInFileNum, 1, bytWork
                Put MatchInFileNum, 2, aFilePathBytes
                Put MatchInFileNum, 82, aFuzzyWordsBytes
                bytWork = MATCH_SKUS_TO_RETURN
                Put MatchInFileNum, 162, bytWork
                bytWork = 0
                Put MatchInFileNum, 163, bytWork
                'Add in number of Hierachicy filters to use, x6.  Hard coded to 1 Category
                If (strHierarchyCode <> "") Then
                    bytWork = 6
                Else
                    bytWork = 0
                End If
                Put MatchInFileNum, 164, bytWork
                bytWork = 0
                Put MatchInFileNum, 165, bytWork
                
                ' Add the hierarchy criteria filter information.
                If (strHierarchyCode <> "") Then
                    lngBytePlace = 165
                    For intCounter = 1 To 6
                        abytHierarchyCode(intCounter) = 0
                        If (IsNumeric(Mid(strHierarchyCode, intCounter, 1))) Then
                            abytHierarchyCode(intCounter) = CInt(Mid(strHierarchyCode, intCounter, 1))
                        End If
                    Next intCounter
                    bytWork = (abytHierarchyCode(1) * 16) + abytHierarchyCode(2)
                    lngBytePlace = lngBytePlace + 1
                    Put MatchInFileNum, lngBytePlace, bytWork
                    bytWork = (abytHierarchyCode(3) * 16) + abytHierarchyCode(4)
                    lngBytePlace = lngBytePlace + 1
                    Put MatchInFileNum, lngBytePlace, bytWork
                    bytWork = (abytHierarchyCode(5) * 16) + abytHierarchyCode(6)
                    lngBytePlace = lngBytePlace + 1
                    Put MatchInFileNum, lngBytePlace, bytWork
                    bytWork = 0
                    lngBytePlace = lngBytePlace + 1
                    Put MatchInFileNum, lngBytePlace, bytWork
                    lngBytePlace = lngBytePlace + 1
                    Put MatchInFileNum, lngBytePlace, bytWork
                    lngBytePlace = lngBytePlace + 1
                    Put MatchInFileNum, lngBytePlace, bytWork
                End If
                
                Close MatchInFileNum
            
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Match In completed for index-> " & strIndexFile)
                
                ' Debug the MIN file text.
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Match In File - " & _
                        MATCH_FILES_PATH & MATCH_INPUT_FILE)
                Dim Stream As TextStream
                Set Stream = fsFileWorker.OpenTextFile(MATCH_FILES_PATH & MATCH_INPUT_FILE, ForReading, False)
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Match In Text - " & _
                        Stream.ReadAll)
                
                ' Run MatchC.exe
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Process Match file - " & strFilePath & MATCH_FILE)
                ChDir strFilePath
                Call ShellAndWait.ShellWait(strFilePath & MATCH_FILE, SW_HIDE)
            
                DoEvents
            
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Out Match file found->" & fsFileWorker.FileExists(MATCH_FILES_PATH & MATCH_OUTPUT_FILE))
                ' Get results from Match.Out
                Dim MatchOutFileNum As Integer
                MatchOutFileNum = FreeFile()
                Open MATCH_FILES_PATH & MATCH_OUTPUT_FILE For Binary As MatchOutFileNum
                
                lngFileSize = LOF(MatchOutFileNum)
            
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "getting Results : " & LOF(MatchOutFileNum))
                lngCurrentByte = 3
                      
                Call DebugMsg(MODULE_NAME, "cmdSearch_click", endlDebug, "Process Match Out file")
                Set cSkuList = New Collection
                Do
                    If (lngCurrentByte + 2) > lngFileSize Then
                        Exit Do
                    End If
                    
                    For I = 1 To 3
                        Get MatchOutFileNum, lngCurrentByte, aInByte(I)
                        lngCurrentByte = lngCurrentByte + 1
                    Next
                    
                    aOutByte(1) = (aInByte(1) \ 16) + 48
                    aOutByte(2) = (aInByte(1) Mod 16) + 48
                    aOutByte(3) = (aInByte(2) \ 16) + 48
                    aOutByte(4) = (aInByte(2) Mod 16) + 48
                    aOutByte(5) = (aInByte(3) \ 16) + 48
                    aOutByte(6) = (aInByte(3) Mod 16) + 48
                    
                    strSkuNumber = Chr$(aOutByte(1)) & Chr$(aOutByte(2)) & Chr$(aOutByte(3)) _
                        & Chr$(aOutByte(4)) & Chr$(aOutByte(5)) & Chr$(aOutByte(6))
                    Call cSkuList.Add(strSkuNumber)
                Loop
        
                Close MatchOutFileNum
                    
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Items found - " & cSkuList.Count)
                
                Set fsFileWorker = Nothing
                If cSkuList.Count = 0 Then
                    Call DisplayCount
                    Call MsgBox("No Matches - Search again", vbInformation, App.Title)
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
        
                Set cItems = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)

                For I = 1 To cSkuList.Count
                    Call cItems.AddLoadFilter(CMP_SELECTLIST, FID_INVENTORY_PartCode, cSkuList.Item(I))
                Next
                
                strDescClause = cItems.GetSelectSQL
                cItems.GetList
            Else
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Description or Alt-Description not set")
                Set cItems = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
                If Not (tvwHierarchy.SelectedItem Is Nothing) Then
                    ' Get the hierarchy code.
                    arrTag = Split(tvwHierarchy.SelectedItem.Tag, ",")
                    strHierarchyCode = arrTag(1)
                End If
            End If 'Keyword was entered
                        
            '*** Force filter to use Supplier ***
            cboSupplier.Col = 0
            If (cboSupplier.ColText <> "*ALL*") Then
                Call cItems.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_SupplierNo, cboSupplier.ColText)
            End If
       
            '*** Force filter to use SKU ***
            If (Trim(txtSKU.Text) <> "") Then
                If (InStr(txtSKU.Text, "%") > 0) Then
                    Call cItems.AddLoadFilter(CMP_LIKE, FID_INVENTORY_PartCode, txtSKU.Text)
                Else
                    If (Len(txtSKU.Text) < PARTCODE_LEN) Then
                        txtSKU.Text = Left$(PARTCODE_PAD, PARTCODE_LEN - Len(txtSKU.Text)) & txtSKU.Text
                    End If
                    Call cItems.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, txtSKU.Text)
                End If
            End If
                
            'Added 1.3.0
            '*** Force filter to find deleted and obsolete items
            If (Not mblnIncObsDel) Then
                strWhereObsDel = "((stkmas.iobs = 0) AND (stkmas.idel = 0)) "
            Else
                strWhereObsDel = vbNullString
            End If
            
            'Added 1.3.0
            '*** Force filter to find non-stocked items
            If (Not mblnIncNonStk) Then
                strWhereNon = "(stkmas.inon = 0) "
            Else
                strWhereNon = vbNullString
            End If
            
            If Not (mblnIncObsDel Or mblnIncNonStk) Then
                strWhere = strWhereObsDel & "AND " & strWhereNon
            Else
                strWhere = strWhereObsDel & strWhereNon
            End If
            
            If (blnUseMatch = False) And Not (tvwHierarchy.SelectedItem Is Nothing) Then
                ' Add the hierarchy clause.
                Select Case Left(tvwHierarchy.SelectedItem.Tag, 1)
                    Case CATEGORY_LEVEL
                        strWhereHierarchy = "(CTGY = '" & strHierarchyCode & "') "
                    Case GROUP_LEVEL
                        strWhereHierarchy = "(GRUP = '" & strHierarchyCode & "') "
                    Case SUBGROUP_LEVEL
                        strWhereHierarchy = "(SGRP = '" & strHierarchyCode & "') "
                    Case STYLE_LEVEL
                        strWhereHierarchy = "(STYL = '" & strHierarchyCode & "') "
                End Select
                ' Add to the WHERE clause.
                If (strWhere = "") Then
                    strWhere = strWhereHierarchy
                Else
                    strWhere = strWhere & " AND " & strWhereHierarchy
                End If
            End If
                       
            strWhereAlt = Trim(UCase(txtAltDescription.Text))
            If (LenB(strWhereAlt) <> 0) Then
                strWhereAlt = Replace(strWhereAlt, SINGLE_SPACE, "%' AND UPPER(stkmas.equv) LIKE '%", , , vbTextCompare)
                If (LenB(strWhere) <> 0) Then
                    strWhere = strWhere & "AND (UPPER(stkmas.equv) LIKE '%" & strWhereAlt & "%') "
                Else
                    strWhere = "(UPPER(stkmas.equv) LIKE '%" & strWhereAlt & "%') "
                End If
            End If
            
            Call cItems.AddLoadField(FID_INVENTORY_PartCode)
            Call cItems.AddLoadField(FID_INVENTORY_Description)
            Call cItems.AddLoadField(FID_INVENTORY_NormalSellPrice)
            Call cItems.AddLoadField(FID_INVENTORY_QuantityAtHand)
            Call cItems.AddLoadField(FID_INVENTORY_QuantityOnOrder)
            Call cItems.AddLoadField(FID_INVENTORY_SupplierNo)
            Call cItems.AddLoadField(FID_INVENTORY_SupplierPartCode)
                   
            strValue = cItems.GetSelectSQL
            
            'Added 1.3.0
            If (LenB(strWhere) <> 0) Then
                lngWherePos = InStr(1, strValue, "WHERE", vbTextCompare)
                If ((lngWherePos > 1) And (Not IsNull(lngWherePos))) Then
                    strValue = Left$(strValue, lngWherePos + 5) & strWhere & "AND " & Mid$(strValue, lngWherePos + 6)
                Else
                    strValue = strValue & " WHERE " & strWhere
                End If
            End If
            
            strValue = Left$(strValue, 7) & "if(iobs=1, 'Obs', if (idel=1, 'Del', if(inon=1, 'Non', if(iris=1, 'Sng', '')))) ""Status"", stkmas.irib, " & Mid$(strValue, 8)
            
            'Append any extra keywords
            If (strWhereDesc <> "") Then
                'group where Description to allow multiples
                 strValue = Replace(strValue, strDescClause, "((" & strDescClause & ") " & strWhereDesc & ")")
            End If
    
            ' Add an ORDER BY clause if not using the MatchC.exe program.
            If (blnUseMatch = False) Then
                strValue = strValue & " ORDER BY SKUN"
            End If
            
    '        ucpbProgress.Caption1 = "Loading data"
    '        Call ucpbProgress.Refresh
    '
    '        Set lstMatches.DataSource = Nothing
    '        If Not adodcSearch.Recordset Is Nothing Then
    '            If adodcSearch.Recordset.State = adStateOpen Then Call adodcSearch.Recordset.Close
    '        End If
    '        Set adodcSearch.Recordset = Nothing
                
            'Added 1.3.0
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strValue)
            Set rsData = New Recordset
            rsData.CursorLocation = adUseClient
            rsData.Open strValue, goDatabase.ConnectionString, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            'Left incase of compatibility issues, 1.3.0
            adodcSearch.ConnectionString = goDatabase.ConnectionString
    '        adodcSearch.RecordSource = strValue
    '        adodcSearch.Refresh
            Set adodcSearch.Recordset = rsData
            
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Add items to grid")
            
            lngDisplayOrder = 1
            If Not (cSkuList Is Nothing) Then
                ' Add the data based upon the results from the call to MatchC.exe
                For I = 1 To cSkuList.Count
                    ' Enforces the retention of the SKU order recieved from MatchC.EXE.
                    rsData.Filter = "SKUN='" & cSkuList.Item(I) & "'"
                
                    If (rsData.RecordCount > 0) Then
                        While (Not rsData.EOF)
                            strRow = rsData!Status & vbTab & _
                                     rsData!SKUN & vbTab & _
                                     SINGLE_SPACE & Trim(rsData!descr) & vbTab & _
                                     Format(rsData!pric, " 0.00 ") & vbTab & _
                                     Format(rsData!onha, " 0 ") & vbTab & _
                                     Format(rsData!ONOR, " 0 ") & vbTab & _
                                     Format(rsData!SUPP, " 00000 ") & vbTab & _
                                     SINGLE_SPACE & rsData!PROD & vbTab
                            
                            'Figure out if it's a 's'ingle item, 'b'ulk item or 'n'one.
                            If (rsData!Status = "Sng") Then
                                strRow = strRow & "S"
                            ElseIf (Not IsNull(rsData!irib)) Then
                                If (rsData!irib > 0) Then
                                    strRow = strRow & "B"
                                Else
                                    strRow = strRow & "N"
                                End If
                            Else
                                strRow = strRow & "N"
                            End If
                            
                            ' Add the hierarchy information.
                            If Not (tvwHierarchy.SelectedItem Is Nothing) Then
                                ' Add the hierarchy clause.
                                Select Case Left(tvwHierarchy.SelectedItem.Tag, 1)
                                    Case CATEGORY_LEVEL
                                        strRow = strRow & vbTab & strHierarchyCode & vbTab & vbTab & vbTab & vbTab & lngDisplayOrder
                                    Case GROUP_LEVEL
                                        strRow = strRow & vbTab & vbTab & strHierarchyCode & vbTab & vbTab & vbTab & lngDisplayOrder
                                    Case SUBGROUP_LEVEL
                                        strRow = strRow & vbTab & vbTab & vbTab & strHierarchyCode & vbTab & vbTab & lngDisplayOrder
                                    Case STYLE_LEVEL
                                        strRow = strRow & vbTab & vbTab & vbTab & vbTab & strHierarchyCode & vbTab & lngDisplayOrder
                                End Select
                            End If
                            
                            lstMatches.AddItem strRow
                            lngDisplayOrder = lngDisplayOrder + 1
                            rsData.MoveNext
            
                        Wend
                    End If
                Next I
            Else
                ' Add the data returned from the basic query.
                While (Not rsData.EOF)
                    strRow = rsData!Status & vbTab & _
                             rsData!SKUN & vbTab & _
                             SINGLE_SPACE & Trim(rsData!descr) & vbTab & _
                             Format(rsData!pric, " 0.00 ") & vbTab & _
                             Format(rsData!onha, " 0 ") & vbTab & _
                             Format(rsData!ONOR, " 0 ") & vbTab & _
                             Format(rsData!SUPP, " 00000 ") & vbTab & _
                             SINGLE_SPACE & rsData!PROD & vbTab
                    
                    'Figure out if it's a 's'ingle item, 'b'ulk item or 'n'one.
                    If (rsData!Status = "Sng") Then
                        strRow = strRow & "S"
                    ElseIf (Not IsNull(rsData!irib)) Then
                        If (rsData!irib > 0) Then
                            strRow = strRow & "B"
                        Else
                            strRow = strRow & "N"
                        End If
                    Else
                        strRow = strRow & "N"
                    End If
                    
                    ' Add the hierarchy information.
                    If Not (tvwHierarchy.SelectedItem Is Nothing) Then
                        ' Add the hierarchy clause.
                        Select Case Left(tvwHierarchy.SelectedItem.Tag, 1)
                            Case CATEGORY_LEVEL
                                strRow = strRow & vbTab & strHierarchyCode & vbTab & vbTab & vbTab & vbTab & lngDisplayOrder
                            Case GROUP_LEVEL
                                strRow = strRow & vbTab & vbTab & strHierarchyCode & vbTab & vbTab & vbTab & lngDisplayOrder
                            Case SUBGROUP_LEVEL
                                strRow = strRow & vbTab & vbTab & vbTab & strHierarchyCode & vbTab & vbTab & lngDisplayOrder
                            Case STYLE_LEVEL
                                strRow = strRow & vbTab & vbTab & vbTab & vbTab & strHierarchyCode & vbTab & lngDisplayOrder
                        End Select
                    End If
                    
                    lstMatches.AddItem strRow
                    lngDisplayOrder = lngDisplayOrder + 1
                    rsData.MoveNext
                Wend
            End If
    '       Removed in 1.3.0
    '        Set lstMatches.DataSource = adodcSearch
            
    '        ucpbProgress.Caption1 = "Formatting data"
    '        Call ucpbProgress.Refresh
            
    '        lblNumberOfMatches.Caption = cItems.Count
    '        If cItems.Count > 0 Then
    '            lstMatches.AddItem (cItems.PartCode & vbTab & cItems.PartDescription & vbTab & cItems.StatusFlag)
    '            While Not cItems.EndOfList
    '                Call cItems.MoveNext
    '                lstMatches.AddItem (cItems.PartCode & vbTab & cItems.PartDescription & vbTab & cItems.StatusFlag)
    '            Wend
    '        End If
        End If
    ElseIf (blnFilterData = True) Then
        ' Filter the data as the required data already exists in the grid.
        Call FilterGrid
    End If 'must reload data
    
    ' Show the number of matches found.
    Call DisplayCount

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Matching items found - " & lblNumberOfMatches.Caption)

    If (lblNumberOfMatches.Caption <> "NO MATCHES") Then
        ' Start the process to load the thumbnail images.
        tmrLoadImages.Enabled = True
        
        If (lstMatches.Visible = True) Then
            lstMatches.SetFocus
        Else
            sprdThumbnails.SetFocus
        End If
        
        cmdRelatedItem.Visible = True
        lstMatches.Selected(0) = True
        'cmdDetails.Visible = True
        If (mblnViewOnly = False) Then
            cmdSelect.Visible = True
        Else
            cmdSelect.Visible = False
        End If
        cmdOrdersIn.Visible = True
        
        If mblnShowUseList Then
            cmdUseList.Visible = True
        End If
    Else
        If (metEnquiryType = etPIMOnly) Then
            tvwHierarchy.SetFocus
        Else
            txtDescription.SetFocus
        End If
        cmdDetails.Visible = False
        cmdSelect.Visible = False
        cmdOrdersIn.Visible = False
        cmdRelatedItem.Visible = False
        cmdUseList.Visible = False
    End If
    
    mblnReload = False
    
    lstMatches.SortState = SortStateActive
    lstMatches.Redraw = True
    lstMatches.Refresh
    
    Screen.MousePointer = vbDefault
    
    Set tvwHierarchy = Nothing
    Set rsData = Nothing
    Set fsFileWorker = Nothing
    Set cSkuList = Nothing
    Set cItems = Nothing
    
    Call DebugMsg(MODULE_NAME, "cmdSearch", endlDebug, "Ended-" & Now() & "->" & strValue)
    
    Exit Sub

cmdSearch_Click_Error:

    Close MatchInFileNum
    Close MatchOutFileNum
    
    lstMatches.SortState = SortStateActive
    lstMatches.Redraw = True
    lstMatches.Refresh
    
    Screen.MousePointer = vbDefault
    
    Set tvwHierarchy = Nothing
    Set rsData = Nothing
    Set fsFileWorker = Nothing
    Set cSkuList = Nothing
    Set cItems = Nothing
    
    Call DisplayCount

    Call MsgBoxEx("Search Error - " & Err.Number & " " & Err.Description, vbCritical + vbOKOnly, _
                        "Search Error", , , , , RGBMsgBox_WarnColour)

End Sub

Private Sub cmdSearch_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON_SEARCH)
    cmdSearch.FontBold = True

End Sub

Private Sub cmdSearch_LostFocus()
    cmdSearch.FontBold = False

End Sub

Private Sub cmdClose_Click()

    cmdClose.FontBold = False
    Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_EVENT_CANCEL)
    Call HideForm

End Sub

Private Sub cmdClose_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON_CLOSE)
    cmdClose.FontBold = True

End Sub

Private Sub cmdClose_LostFocus()
    
    cmdClose.FontBold = False

End Sub

Private Sub cmdReset_Click()
    
Const PROCEDURE_NAME As String = "cmdReset_Click"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    On Error GoTo cmdReset_Click_Error
    
    Screen.MousePointer = vbHourglass
    
    'Select all departments
    txtDescription.Text = vbNullString
    txtAltDescription.Text = vbNullString
    txtSKU.Text = vbNullString
    'only reset supplier if not preselected
    If (LenB(mstrOldSupp) = 0) Then cboSupplier.Text = "*ALL*" & vbTab & "*ALL*"
    
    Call lstMatches.Clear
    sprdThumbnails.MaxRows = 0
    lblNumberOfMatches.BackColor = RGB_WHITE
    lblNumberOfMatches.ForeColor = RGB_BLACK
    lblNumberOfMatches.Caption = "0"
    chkActiveOnly.Value = vbUnchecked         'Added 1.3.0
    chkIncludeNonStock.Value = vbUnchecked    'Added 1.3.0
    cmdDetails.Visible = False
    cmdOrdersIn.Visible = False             'Added 1.3.0
    cmdRelatedItem.Visible = False          'Added 1.3.0
    cmdRelatedItem.Enabled = False          'Added 1.3.0
    cmdSelect.Visible = False
    cmdUseList.Visible = False
    cmdEnlarge.Visible = False
    ' Contract the hierarchy tree nodes.
    Call ContractTree
    ' Untag all the hierarchy tree nodes.
    Call UnTagTreeNodes
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Set Start-up Controls")
    If Not (tvwHierarchyPIM.SelectedItem Is Nothing) Then
        tvwHierarchyPIM.SelectedItem = Nothing
    End If
    If Not (tvwHierarchyFull.SelectedItem Is Nothing) Then
        tvwHierarchyFull.SelectedItem = Nothing
    End If
    
    If (metEnquiryType = etPIMOnly) Then
        If (tvwHierarchyPIM.Visible = True) Then
            tvwHierarchyPIM.SetFocus
        End If
    Else
        If (txtDescription.Visible = True) Then
            txtDescription.SetFocus
        End If
    End If
    Screen.MousePointer = vbDefault
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub
    
cmdReset_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

Private Sub cmdReset_GotFocus()
    
    UpdateStatus (STATUS_TEXT_BUTTON_RESET)
    cmdReset.FontBold = True

End Sub

Private Sub cmdReset_LostFocus()
    
    cmdReset.FontBold = False

End Sub

Private Sub cmdSelect_Click()

Dim sPartCode As String

    cmdSelect.FontBold = False
    
    If (lstMatches.Visible = True) Then
        lstMatches.Row = lstMatches.ListIndex
        lstMatches.Col = COL_SKU
        sPartCode = lstMatches.ColText
        Call DebugMsg(MODULE_NAME, "cmdSelect_click", endlDebug, "From List:" & sPartCode & " at " & lstMatches.ListIndex)
    Else
        sPartCode = sprdThumbnails.CellTag
        If (sPartCode = "") Then 'check if focus on Item Description so move up one and try again
            sprdThumbnails.Row = sprdThumbnails.Row - 1
            sPartCode = sprdThumbnails.CellTag
        End If
        Call DebugMsg(MODULE_NAME, "cmdSelect_click", endlDebug, "From Image:" & sPartCode & " at (" & sprdThumbnails.Row & "," & sprdThumbnails.Col & ")")
    End If
    If (lstMatches.Visible) Then lstMatches.SetFocus
    If (Not moParent Is Nothing) Then moParent.KeyPreview = True
    Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_EVENT_APPLY & sPartCode)
    Call HideForm
    
End Sub

Private Sub cmdSelect_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON_SELECT)
    cmdSelect.FontBold = True

End Sub

Private Sub cmdOrdersIn_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON)
    cmdOrdersIn.FontBold = True

End Sub

Private Sub cmdSelect_LostFocus()
    
    cmdSelect.FontBold = False
    
End Sub

Private Sub cmdOrdersIn_LostFocus()
    
    cmdOrdersIn.FontBold = False
    
End Sub

Private Sub cmdUseList_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdUseList_Click"

'Create field for storing Key into
Dim lCharPos    As Long
Dim tSearchItem As clsSearchCriteria
Dim sLookUp     As String
Dim sValue      As String
Dim lItemNo     As Long

    Screen.MousePointer = vbHourglass
    
    Set colSearchCriteria = New Collection
    DoEvents
    
    '*** Force filter to use Keyword ***
    If (LenB(txtDescription.Text) <> 0) Then
        If (InStr(txtDescription.Text, " ") > 0) Then
            sLookUp = txtDescription.Text
            
            While (Len(sLookUp) > 0)
                lCharPos = InStr(sLookUp, " ")
                If (lCharPos > 0) Then
                    sValue = "%" & Left$(sLookUp, lCharPos - 1) & "%"
                Else
                    sValue = "%" & sLookUp & "%"
                End If
                
                If (lCharPos > 0) Then
                    sLookUp = Mid$(sLookUp, lCharPos + 1)
                Else
                    sLookUp = vbNullString
                End If
                Set tSearchItem = New clsSearchCriteria
                tSearchItem.Comparator = CMP_LIKE
                tSearchItem.FieldID = FID_INVENTORY_Description
                tSearchItem.Value = sValue
                Call colSearchCriteria.Add(tSearchItem)
            Wend
            
        Else
            Set tSearchItem = New clsSearchCriteria
            tSearchItem.Comparator = CMP_LIKE
            tSearchItem.FieldID = FID_INVENTORY_Description
            tSearchItem.Value = "%" & txtDescription.Text & "%"
            Call colSearchCriteria.Add(tSearchItem)
            
        End If
        
    End If 'keyword was entered
    
    '*** Force filter to use Supplier ***
    cboSupplier.Col = 0
    If (cboSupplier.ColText <> "*ALL*") Then
        Set tSearchItem = New clsSearchCriteria
        tSearchItem.Comparator = CMP_EQUAL
        tSearchItem.FieldID = FID_INVENTORY_SupplierNo
        tSearchItem.Value = cboSupplier.ColText
        Call colSearchCriteria.Add(tSearchItem)
    End If
    
    '*** Force filter to use SKU ***
    If (LenB(txtSKU.Text) <> 0) Then
        Set tSearchItem = New clsSearchCriteria
        tSearchItem.Comparator = CMP_LIKE
        tSearchItem.FieldID = FID_INVENTORY_PartCode
        tSearchItem.Value = "%" & txtSKU.Text & "%"
        Call colSearchCriteria.Add(tSearchItem)
    End If
    
    '*** Force filter to find deleted and obsolete items
    If (Not mblnIncObsDel) Then
        Set tSearchItem = New clsSearchCriteria
        tSearchItem.Comparator = CMP_EQUAL
        tSearchItem.FieldID = FID_INVENTORY_ItemObsolete
        tSearchItem.Value = 0
        Call colSearchCriteria.Add(tSearchItem)
        
        Set tSearchItem = New clsSearchCriteria
        tSearchItem.Comparator = CMP_EQUAL
        tSearchItem.FieldID = FID_INVENTORY_ItemDeleted
        tSearchItem.Value = 0
        Call colSearchCriteria.Add(tSearchItem)
    End If
    
    'Added 1.3.0
    '*** Force filter to find non-stocked items
    If (Not mblnIncNonStk) Then
        Set tSearchItem = New clsSearchCriteria
        tSearchItem.Comparator = CMP_EQUAL
        tSearchItem.FieldID = FID_INVENTORY_NonStockItem
        tSearchItem.Value = 0
        Call colSearchCriteria.Add(tSearchItem)
    End If
    
    Screen.MousePointer = vbNormal
    If (lstMatches.Visible) Then lstMatches.SetFocus
    Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_EVENT_USELIST)
    Call HideForm
    'RaiseEvent UseList(colSearchCriteria, adodcSearch)

End Sub

Private Sub cmdUseList_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON)
    cmdUseList.FontBold = True
    
End Sub

Private Sub cmdUseList_LostFocus()
    
    cmdUseList.FontBold = False

End Sub

Private Sub cmdRelatedItem_GotFocus()

    UpdateStatus (STATUS_TEXT_BUTTON)
    cmdRelatedItem.FontBold = True
    
End Sub

Private Sub cmdRelatedItem_LostFocus()
    
    cmdRelatedItem.FontBold = False

End Sub

Private Sub cmdViewType_Click()

    If (cmdViewType.Caption = "F2 - &View Images") Then
        UpdateStatus (STATUS_TEXT_BUTTON_VIEW_LIST)
        cmdViewType.Caption = "F2 - &View Text"
        ' Highlight the current SKU from the list in the image grid.
        If (lstMatches.ListCount > 0) Then Call HighlightImageItem
        sprdThumbnails.Visible = True
        lblImagePagesTitle.Visible = True
        lblImagePages.Visible = True
        lstMatches.Visible = False
        If (mblnLoadingImages = True) Then
            fraImagesLoading.Visible = True
        End If
        sprdThumbnails.SetFocus
        Call DoesLargeImageExist(sprdThumbnails.CellTag)
    Else
        UpdateStatus (STATUS_TEXT_BUTTON_VIEW_IMAGES)
        fraImagesLoading.Visible = False
        cmdViewType.Caption = "F2 - &View Images"
        ' Highlight the current SKU from the image grid in the list.
        Call HighlightListItem
        lstMatches.Visible = True
        sprdThumbnails.Visible = False
        lblImagePagesTitle.Visible = False
        lblImagePages.Visible = False
        lstMatches.SetFocus
        lstMatches.Col = COL_SKU
        Call DoesLargeImageExist(lstMatches.ColText)
    End If
    
End Sub

Private Sub cmdViewType_GotFocus()

    If (lstMatches.Visible = True) Then
        UpdateStatus (STATUS_TEXT_BUTTON_VIEW_IMAGES)
    Else
        UpdateStatus (STATUS_TEXT_BUTTON_VIEW_LIST)
    End If
    cmdViewType.FontBold = True
    
End Sub

Private Sub cmdViewType_LostFocus()

    cmdViewType.FontBold = False

End Sub


Private Sub Form_Unload(Cancel As Integer)

    ' Flag to calling apps unloading.
    Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_EVENT_CANCEL)
    
    ' Close the Winsock connection.
    CloseConnection
    
End Sub

Private Sub lblNumberOfMatchesTitle_Click()
    lblNumberOfMatchesTitle.ToolTipText = "Ver." & App.Major & "." & App.Minor & "." & App.Revision

End Sub

Private Sub lstMatches_GotFocus()

    UpdateStatus (STATUS_TEXT_LIST)
    lstMatches.BorderColor = vbBlack

End Sub

Private Sub lstMatches_LostFocus()

    lstMatches.BorderColor = fraItemFilter.BackColor
    
End Sub

Private Sub lstMatches_KeyPress(KeyAscii As Integer)

    Call DebugMsg(MODULE_NAME, "lstMatches_KeyPress", endlDebug, "Start")

    If (KeyAscii = vbKeyEscape) Then
        If (metEnquiryType = etPIMOnly) Then
            tvwHierarchyPIM.SetFocus
        Else
            Call txtDescription.SetFocus
        End If
    End If
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        cmdSelect.Value = True
    End If

End Sub

Private Sub lstMatches_SelChange(ItemIndex As Long)
    
    If (cmdRelatedItem.Visible) Then
        lstMatches.Col = 8
        If (lstMatches.ColText = "B") Then
            cmdRelatedItem.Caption = "F6-SNG Details"
            cmdRelatedItem.Enabled = True
        ElseIf (lstMatches.ColText = "S") Then
            cmdRelatedItem.Caption = "F6-BLK Details"
            cmdRelatedItem.Enabled = True
        Else
            cmdRelatedItem.Caption = "F6-Rel. Item"
            cmdRelatedItem.Enabled = False
        End If
    End If
    lstMatches.Col = COL_SKU
    Call DoesLargeImageExist(lstMatches.ColText)

End Sub

Private Sub mrecHierarchyData_FetchComplete(ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

    mblnQueryingHierarchyData = False
    Call DebugMsg(MODULE_NAME, "FetchComplete", endlDebug, mrecHierarchyData.Source)
    
End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)


    mblnShowKeyPad = Not mblnShowKeyPad
    Select Case (Panel.Key)
        Case ("KeyBoard"):
                If ((Panel.Picture Is Nothing) = False) Then
                    ucKeyPad.Visible = mblnShowKeyPad
                    If (txtDescription.Visible = True) Then txtDescription.SetFocus
                End If
    End Select
    
End Sub

Private Sub sprdThumbnails_Click(ByVal Col As Long, ByVal Row As Long)

    mblnGridMouseClick = True
    
End Sub

Private Sub sprdThumbnails_DblClick(ByVal Col As Long, ByVal Row As Long)

    mblnGridMouseDblClick = True
    
    ' Show the enlarged image for the current SKU.
    If (cmdEnlarge.Visible = True) Then
        Call LoadLargeImage
    End If
    
End Sub

Private Sub sprdThumbnails_GotFocus()

    UpdateStatus (STATUS_TEXT_IMAGES)

End Sub

Private Sub sprdThumbnails_KeyDown(KeyCode As Integer, Shift As Integer)
    
    ' If scrolling past the end of the images then load the next set.
    If (fraImagesLoading.Visible = False) Then
        If (sprdThumbnails.Row = 1) And ((KeyCode = vbKeyUp) Or (KeyCode = vbKeyPageUp)) Then
            KeyCode = 0
            Call LoadImages(gisScrollUp)
        ElseIf (sprdThumbnails.Row = sprdThumbnails.MaxRows - 1) And ((KeyCode = vbKeyDown) Or (KeyCode = vbKeyPageDown)) Then
            KeyCode = 0
            Call LoadImages(gisScrollDown)
        End If
    End If
       
End Sub

Private Sub sprdThumbnails_KeyPress(KeyAscii As Integer)

    Call DebugMsg(MODULE_NAME, "sprdThumbnails_KeyPress", endlDebug, "Start")

    If (KeyAscii = vbKeyEscape) Then
        If (metEnquiryType = etPIMOnly) Then
            tvwHierarchyPIM.SetFocus
        Else
            Call txtDescription.SetFocus
        End If
    End If

End Sub

Private Sub sprdThumbnails_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)

Dim NextRow As Integer

    ' If the mouse was double clicked then don't change grid cell just show the enlarged image if possible.
    If (mblnGridMouseDblClick = True) Or (fraImagesLoading.Visible = True) Then
        mblnGridMouseDblClick = False
        Cancel = True
        Exit Sub
    End If
        
    sprdThumbnails.Row = Row + 1
    sprdThumbnails.Col = Col
    sprdThumbnails.BackColor = RGB_WHITE

    ' Move to the next image on the grid skipping over the text row.
    If (NewRow <> -1) And (NewCol <> -1) Then
        If (NewRow < Row) Then
            sprdThumbnails.Col = NewCol
            ' Get the next selected image row.
            If (NewRow Mod 2 = 0) Then
                ' Clicked on a text row so use the image row.
                NextRow = NewRow - 1
            Else
                NextRow = NewRow
            End If
            sprdThumbnails.Row = NextRow
            If (sprdThumbnails.CellTag = EMPTY_CELL_TAG_TEXT) Then
                Cancel = True
            Else
                ' Set the new active cell on an image cell.
                Call sprdThumbnails.SetActiveCell(NewCol, NextRow)
            End If
        ElseIf (NewRow > Row) Then
            If (NewRow <> sprdThumbnails.MaxRows) Then
                sprdThumbnails.Col = NewCol
                ' Get the next selected image row.
                If (NewRow Mod 2 = 0) Then
                    ' Clicked on a text row so use the image row.
                    If (NewRow = Row + 1) Then
                        If (mblnGridMouseClick = True) Then
                            NextRow = NewRow - 1
                        Else
                            NextRow = Row + 2
                        End If
                    Else
                        NextRow = NewRow - 1
                    End If
                Else
                    NextRow = NewRow
                End If
                sprdThumbnails.Row = NextRow
                If (sprdThumbnails.CellTag = EMPTY_CELL_TAG_TEXT) Then
                    ' Move the cursor to the first cell of the row with empty cells.
                    ' Set the new active cell on an image cell.
                    Call sprdThumbnails.SetActiveCell(1, NextRow)
                    ' Make the text viewable on the screen if dropped off the bottom of the grid.
                    If (sprdThumbnails.IsVisible(1, NextRow + 1, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(1, NextRow + 1)
                        Call sprdThumbnails.SetActiveCell(1, NextRow)
                    End If
                    NewCol = 1
                Else
                    ' Set the new active cell on an image cell.
                    Call sprdThumbnails.SetActiveCell(NewCol, NextRow)
                    ' Make the text viewable on the screen if dropped off the bottom of the grid.
                    If (sprdThumbnails.IsVisible(NewCol, NextRow + 1, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(NewCol, NextRow + 1)
                        Call sprdThumbnails.SetActiveCell(NewCol, NextRow)
                    End If
                End If
            Else
                sprdThumbnails.Row = sprdThumbnails.MaxRows - 1
                sprdThumbnails.Col = NewCol
                If (sprdThumbnails.CellTag = EMPTY_CELL_TAG_TEXT) Then
                    ' Move the cursor to the first cell of the row with empty cells.
                    ' Set the new active cell on an image cell.
                    NextRow = sprdThumbnails.MaxRows - 1
                    Call sprdThumbnails.SetActiveCell(1, NextRow)
                    ' Make the text viewable on the screen if dropped off the bottom of the grid.
                    If (sprdThumbnails.IsVisible(1, NextRow + 1, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(1, NextRow + 1)
                        Call sprdThumbnails.SetActiveCell(1, NextRow)
                    End If
                    NewCol = 1
                Else
                    ' Set the new active cell on an image cell.
                    Call sprdThumbnails.SetActiveCell(NewCol, sprdThumbnails.MaxRows - 1)
                    ' Make the text viewable on the screen if dropped off the bottom of the grid.
                    If (sprdThumbnails.IsVisible(NewCol, sprdThumbnails.MaxRows, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(NewCol, sprdThumbnails.MaxRows)
                        Call sprdThumbnails.SetActiveCell(NewCol, sprdThumbnails.MaxRows - 1)
                    End If
                End If
            End If
        ElseIf (NewCol <> Col) Then
                sprdThumbnails.Col = NewCol
                sprdThumbnails.Row = Row
            If (sprdThumbnails.CellTag = EMPTY_CELL_TAG_TEXT) Then
                Cancel = True
                NewCol = Col
            End If
        End If
        ' Get the next selected image row.
        If (sprdThumbnails.ActiveRow Mod 2 = 0) Then
            ' Clicked on a text row so use the image row.
            NextRow = NewRow
        Else
            NextRow = sprdThumbnails.ActiveRow + 1
        End If
        sprdThumbnails.Row = NextRow
        sprdThumbnails.Col = NewCol
        sprdThumbnails.BackColor = RGB_LTRED
        sprdThumbnails.Row = sprdThumbnails.ActiveRow
        sprdThumbnails.Col = NewCol
        ' Test if an enlarged image for the current SKU can be shown.
        Call DoesLargeImageExist(sprdThumbnails.CellTag)
        
        mblnGridMouseClick = False
    End If
    
End Sub

Private Sub sprdThumbnails_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)

    mblnGridMouseClick = True
    
End Sub

Private Sub tmrLoadHierarchy_Timer()
   
    tmrLoadHierarchy.Enabled = False
    ' Build the Hierarchy trees for the PIM and Full product hierarchies.
    Call LoadTreeData

End Sub

Private Sub tmrLoadImages_Timer()

    tmrLoadImages.Enabled = False
    ' Start the load of the images asynchronously.
    Call LoadImages(gisNone)
    
End Sub

Private Sub tvwHierarchy_NodeClick(ByVal Node As MSComctlLib.Node)

    mblnHierarchyChanged = True
    
End Sub

Private Sub tmrBuildTreeData_Timer()

    ' Start the build of the hierarchy tree nodes.
    If (mblnQueryingHierarchyData = False) Then
        tmrBuildTreeData.Enabled = False
        DoEvents
        Call BuildTreeData
    End If
    
End Sub

' At the requested time refresh the data.
Private Sub tmrRefresh_Timer()

    If (mblnDataRefreshed = False) And (Time > mstrRefreshTime) Then
        Call RefreshTheData
    ElseIf (mblnDataRefreshed = True) Then
        If (Time > "00:00:00") And (Time < mstrRefreshTime) Then
            mblnDataRefreshed = False
        End If
    End If

End Sub

Private Sub tvwHierarchyFull_Expand(ByVal Node As MSComctlLib.Node)

    If (Left$(Node.Child.Tag, 2) = "X2") Then Call BuildGroupBranch(Node, tvwHierarchyFull)

End Sub

Private Sub tvwHierarchyFull_GotFocus()

    UpdateStatus (STATUS_TEXT_HIERARCHY)
    
End Sub

Private Sub tvwHierarchyFull_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        cmdSearch.Value = True
    End If

End Sub

Private Sub tvwHierarchyPIM_Expand(ByVal Node As MSComctlLib.Node)
    
    If (Left$(Node.Child.Tag, 2) = "X2") Then Call BuildGroupBranch(Node, tvwHierarchyPIM)

End Sub

Private Sub tvwHierarchyPIM_GotFocus()

    UpdateStatus (STATUS_TEXT_HIERARCHY)

End Sub

Private Sub tvwHierarchyPIM_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        cmdSearch.Value = True
    End If

End Sub

Private Sub txtAltDescription_Change()
    mblnReload = True
    
End Sub

Private Sub txtAltDescription_KeyDown(KeyCode As Integer, Shift As Integer)
      
    Call DebugMsg(MODULE_NAME, "txtAltDescription_KeyDown", endlDebug, "Start")
      
    If (KeyCode = vbKeyDown) Then
        txtSKU.SetFocus
    ElseIf (KeyCode = vbKeyUp) Then
        txtDescription.SetFocus
    End If
    
End Sub

Private Sub txtDescription_Change()

    mblnReload = True

End Sub

Private Sub DisplayCount()

Dim lngRowCounter  As Long
Dim lngVisibleRows As Long

    lblNumberOfMatches.BackColor = RGB_WHITE
    lblNumberOfMatches.ForeColor = RGB_BLACK
    lblNumberOfMatches.FontBold = False
    ' Loop over the grid to return the number of visible rows.
    lngVisibleRows = 0
    For lngRowCounter = 0 To lstMatches.ListCount - 1
        lstMatches.Row = lngRowCounter
        If (lstMatches.RowHide = False) Then
            lngVisibleRows = lngVisibleRows + 1
        End If
    Next lngRowCounter
    lblNumberOfMatches.Caption = lngVisibleRows
    lstMatches.Refresh
    
    If (lngVisibleRows = 0) Then
        Call Display_NoMatches(lblNumberOfMatches)
    End If

End Sub

Private Sub txtDescription_GotFocus()

    Call DebugMsg(MODULE_NAME, "txtDescription_GotFocus", endlDebug, "Start")

    UpdateStatus (STATUS_TEXT_SEARCH_OPTIONS)
    txtDescription.BackColor = TEXT_BOX_NORMAL_COLOUR
    mlngOldColour = txtDescription.BackColor
    txtAltDescription.BackColor = TEXT_BOX_DIMMED_COLOUR
    txtDescription.SelStart = 0
    txtDescription.SelLength = Len(txtDescription.Text)
    txtDescription.BackColor = RGBEdit_Colour
    
    If (mblnShowKeyPad = True) Then ucKeyPad.Visible = True
    
    ' If in PIM enquiry type mode then change to Full products mode.
    If (metEnquiryType = etPIMOnly) Then
        metEnquiryType = etFullItems
        Call SwitchHierarchyTree
    End If
    
End Sub

Private Sub txtAltDescription_GotFocus()

    Call DebugMsg(MODULE_NAME, "txtDescription_GotFocus", endlDebug, "Start")

    UpdateStatus (STATUS_TEXT_SEARCH_OPTIONS)
    txtAltDescription.BackColor = TEXT_BOX_NORMAL_COLOUR
    mlngOldColour = txtAltDescription.BackColor
    txtDescription.BackColor = TEXT_BOX_DIMMED_COLOUR
    txtAltDescription.SelStart = 0
    txtAltDescription.SelLength = Len(txtAltDescription.Text)
    txtAltDescription.BackColor = RGBEdit_Colour
    
    If (mblnShowKeyPad = True) Then ucKeyPad.Visible = True
    
    ' If in PIM enquiry type mode then change to Full products mode.
    If (metEnquiryType = etPIMOnly) Then
        metEnquiryType = etFullItems
        Call SwitchHierarchyTree
    End If
    
End Sub

Private Sub txtDescription_KeyDown(KeyCode As Integer, Shift As Integer)

    If (KeyCode = vbKeyDown) Then
        txtAltDescription.SetFocus
    End If
    
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If (LenB(txtDescription.Text) <> 0) Or (LenB(txtSKU.Text) <> 0) Or (LenB(txtAltDescription.Text) <> 0) Or _
                (cboSupplier.ListIndex <> -1) Then
            mblnReload = True
            cmdSearch.Value = True
        Else
            mblnReload = False
            Call SendKeys(vbTab)
        End If
    End If

End Sub

Private Sub txtAltDescription_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If (LenB(txtDescription.Text) <> 0) Or (LenB(txtSKU.Text) <> 0) Or (LenB(txtAltDescription.Text) <> 0) Or _
                (cboSupplier.ListIndex <> -1) Then
            mblnReload = True
            cmdSearch.Value = True
        Else
            Call SendKeys(vbTab)
        End If
    
    ElseIf (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtDescription_LostFocus()
    
    ucKeyPad.Visible = False
    txtDescription.BackColor = mlngOldColour

End Sub

Private Sub txtAltDescription_LostFocus()
    
    ucKeyPad.Visible = False
    txtAltDescription.BackColor = mlngOldColour

End Sub

Private Sub txtSKU_Change()
    
    mblnReload = True

End Sub

Private Sub txtSKU_GotFocus()
    
    Call DebugMsg(MODULE_NAME, "txtSKU_GotFocus", endlDebug, "Start")

    UpdateStatus (STATUS_TEXT_SEARCH_OPTIONS)
    mlngOldColour = txtSKU.BackColor
    txtSKU.BackColor = RGBEdit_Colour
    txtSKU.SelStart = 0
    txtSKU.SelLength = Len(txtSKU.Text)

    If (mblnShowKeyPad = True) Then ucKeyPad.Visible = True
    
    ' If in PIM enquiry type mode then change to Full products mode.
    If (metEnquiryType = etPIMOnly) Then
        metEnquiryType = etFullItems
        Call SwitchHierarchyTree
    End If

End Sub

Private Sub txtSKU_KeyDown(KeyCode As Integer, Shift As Integer)

    If (KeyCode = vbKeyDown) Then
        If (cboSupplier.Visible = True) Then
            cboSupplier.SetFocus
        End If
    ElseIf (KeyCode = vbKeyUp) Then
        txtAltDescription.SetFocus
    End If

End Sub

Private Sub txtSKU_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If (LenB(txtDescription.Text) <> 0) Or (LenB(txtSKU.Text) <> 0) Or (LenB(txtAltDescription.Text) <> 0) Or _
                (cboSupplier.ListIndex <> -1) Then
            mblnReload = True
            cmdSearch.Value = True
        Else
            Call SendKeys(vbTab)
        End If
    ElseIf (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub txtSKU_LostFocus()
    
    ucKeyPad.Visible = False
    txtSKU.BackColor = mlngOldColour

End Sub

' Load the data for the hierarchy trees.
Private Sub LoadTreeData()

Const PROCEDURE_NAME As String = "LoadTreeData"

'Dim objPIMItem As Object

Dim strSQL As String

    On Error GoTo LoadTreeData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlTraceIn)

    ' Open the database connection.
    Set mcnnDBConnection = New ADODB.Connection
    mcnnDBConnection.ConnectionString = goDatabase.ConnectionString
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
       
    DoEvents
    
    ' Get the data for the hierarchy tree.
    If (metLoadTreeEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get PIM Data")
    
        ' SQL to return the tree.
        strSQL = "SELECT CategoryCode as CatNo, CategoryDescr as CatDesc, GroupCode as GrpNo, GroupDescr AS GrpDesc, " & _
                    "SubGroupCode AS SGrpNo, SubGroupDescr AS SGRPDesc, StyleCode AS StylNo, StyleDescr AS StylDesc " & _
                    "FROM PIMMAS " & _
                    "ORDER BY CatDesc, GrpDesc, SGRPDesc, StylDesc"
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Full Data")
        ' SQL to return the tree.
        strSQL = "SELECT d.NUMB AS CatNo, d.DESCR AS CatDesc, c.GROU AS GrpNo, c.DESCR AS GrpDesc," & _
                " b.SGRP AS SGrpNo, b.DESCR AS SGRPDesc, a.STYL AS StylNo, a.DESCR AS StylDesc" & _
                " FROM HIESTY a INNER JOIN HIESGP b on a.SGRP = b.SGRP INNER JOIN" & _
                " HIEGRP c on a.GROU = c.GROU INNER JOIN HIECAT d ON a.NUMB = d.NUMB" & _
                " ORDER BY CatDesc, GrpDesc, SGRPDesc, StylDesc"
'        strSQL = "SELECT a.NUMB AS CatNo, a.DESCR AS CatDesc, b.GROU AS GrpNo, b.DESCR AS GrpDesc, " & _
'                    "c.SGRP AS SGrpNo, c.DESCR AS SGRPDesc, d.STYL AS StylNo, d.DESCR AS StylDesc " & _
'                    "FROM HIECAT a INNER JOIN HIEGRP b on a.NUMB = b.NUMB " & _
'                    "INNER JOIN HIESGP c on b.GROU = c.GROU " & _
'                    "INNER JOIN HIESTY d ON c.SGRP = d.SGRP " & _
'                    "ORDER BY CatDesc, GrpDesc, SGRPDesc, StylDesc"
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQL)
    ' Open the recordset for adding the data to the tree.
    Set mrecHierarchyData = New ADODB.Recordset
    mrecHierarchyData.CursorLocation = adUseClient
    mrecHierarchyData.Properties("Initial Fetch Size") = 1
    mblnQueryingHierarchyData = True
    ' Read in the data asynchronously to allow the main thred to keep processing.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open Recordset")
    Call mrecHierarchyData.Open(strSQL, mcnnDBConnection, adOpenForwardOnly, adLockReadOnly, adCmdText + adAsyncExecute + adAsyncFetch)
    DoEvents
    
    ' Start the timer which will trigger the build of the tree data when the data load has completed.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start BuildTreeData Timer")
    tmrBuildTreeData.Enabled = True
        
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlTraceOut)
        
    Exit Sub

LoadTreeData_Error:
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Build the hierarchy trees with the data.
'   Each node is tagged with the Tree Level, Item Code , If previously selected (data exists in grid) 1 = Selected  0 = Not Selected.
Private Sub BuildTreeData()

Const PROCEDURE_NAME As String = "BuildTreeData"
    
Dim tvwHierarchy As TreeView
Dim nNode        As Node

Dim strCatNode   As String
Dim strGrpNode   As String
Dim strSGrpNode  As String
Dim strStyleNode As String
Dim strFieldCode As String

Dim blnBuildCategory As Boolean
Dim blnBuildGroup    As Boolean
Dim blnBuildSubGroup As Boolean
Dim blnBuildStyle    As Boolean

Dim intCounter       As Integer
    
    On Error GoTo BuildTreeData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    Select Case metLoadTreeEnquiryType
        Case etPIMOnly
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM Data")
            ' Clear the tree.
            tvwHierarchyPIM.Nodes.Clear
            Set tvwHierarchy = tvwHierarchyPIM
        Case etFullItems
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Full Data")
            ' Clear the tree.
            tvwHierarchyFull.Nodes.Clear
            Set tvwHierarchy = tvwHierarchyFull
    End Select
    
    mlngCatCounter = 0
    mlngGrpCounter = 0
    mlngSGrpCounter = 0
    mlngStyleCounter = 0

    DoEvents
    tvwHierarchy.Visible = False
    ' Build the initial tree values.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "First Loop Through ")
    If (mrecHierarchyData.BOF = False) And (mrecHierarchyData.EOF = False) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Build Hierarchy Tree From Recordset Data")
    
        ' Create Category node.
        strFieldCode = GetDBField(mrecHierarchyData.Fields("CatNo"), False, , ftCategory)
        strCatNode = CATEGORY_CODE & CStr(mlngCatCounter)
        Set nNode = tvwHierarchy.Nodes.Add(, , strCatNode, _
                    Trim(GetDBField(mrecHierarchyData.Fields("CatDesc"), False, CATEGORY_CODE & CStr(mlngCatCounter), ftDescription)))
        nNode.Tag = CATEGORY_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
        ' Create Group node.
        strFieldCode = GetDBField(mrecHierarchyData.Fields("GrpNo"), False, , ftGroup)
        strGrpNode = GROUP_CODE & CStr(mlngGrpCounter)
        Set nNode = tvwHierarchy.Nodes.Add(strCatNode, tvwChild, strGrpNode, _
                    Trim(GetDBField(mrecHierarchyData.Fields("GrpDesc"), False, GROUP_CODE & CStr(mlngGrpCounter), ftDescription)))
        nNode.Tag = "X" & GROUP_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
        ' Create Sub Group node.
'        strFieldCode = GetDBField(mrecHierarchyData.Fields("SGrpNo"), False, , ftSubGroup)
'        strSGrpNode = SUBGROUP_CODE & CStr(mlngSGrpCounter)
'        Set nNode = tvwHierarchy.Nodes.Add(strGrpNode, tvwChild, strSGrpNode, _
'                    Trim(GetDBField(mrecHierarchyData.Fields("SGrpDesc"), False, SUBGROUP_CODE & CStr(mlngSGrpCounter), ftDescription)))
'        nNode.Tag = SUBGROUP_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
'        ' Create Style leaf node.
'        strFieldCode = GetDBField(mrecHierarchyData.Fields("StylNo"), False, , ftStyle)
'        strStyleNode = STYLE_CODE & CStr(mlngStyleCounter)
'        Set nNode = tvwHierarchy.Nodes.Add(strSGrpNode, tvwChild, strStyleNode, _
'                    Trim(GetDBField(mrecHierarchyData.Fields("StylDesc"), False, STYLE_CODE & CStr(mlngStyleCounter), ftDescription)))
'        nNode.Tag = STYLE_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
'
        ' Read the next record.
        mrecHierarchyData.MoveNext
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Second Pass Started:" & tvwHierarchy.Nodes.Count)
    ' Loop over the records and build the tree.
    Do Until (mrecHierarchyData.BOF) Or (mrecHierarchyData.EOF)
        ' Add the Category, Group, Sub Group and Style and loop over styles till any of the parent node values change, when
        '   this happens build a new branch and continue with the styles.
        blnBuildCategory = False
        blnBuildGroup = False
        blnBuildSubGroup = False
        blnBuildStyle = False
        ' If a change of category.
        If (mstrCatNo <> GetDBField(mrecHierarchyData.Fields("CatNo"), True, , ftCategory)) Then
            ' Build new Category branch.
            blnBuildCategory = True
            blnBuildGroup = True
            blnBuildSubGroup = True
            blnBuildStyle = True
        ' If change of group.
        ElseIf (mstrGrpNo <> GetDBField(mrecHierarchyData.Fields("GrpNo"), True, , ftGroup)) Then
            ' Build new Group branch.
            blnBuildGroup = True
            blnBuildSubGroup = True
            blnBuildStyle = True
        ' If change of sub group.
        ElseIf (mstrSGrpNo <> GetDBField(mrecHierarchyData.Fields("SGrpNo"), True, , ftSubGroup)) Then
            ' Build new Sub Group branch.
            blnBuildSubGroup = True
            blnBuildStyle = True
        ElseIf (mstrStyleNo <> GetDBField(mrecHierarchyData.Fields("StylNo"), True, , ftStyle)) Then
            ' Build new Style leaf.
            blnBuildStyle = True
        End If
        
        DoEvents
        ' If required build the relevant branch.
        ' Add category branch.
        If (blnBuildCategory = True) Then
            strFieldCode = GetDBField(mrecHierarchyData.Fields("CatNo"), False, , ftCategory)
            strCatNode = CATEGORY_CODE & CStr(mlngCatCounter)
            Set nNode = tvwHierarchy.Nodes.Add(, , strCatNode, _
                        Trim(GetDBField(mrecHierarchyData.Fields("CatDesc"), False, CATEGORY_CODE & CStr(mlngCatCounter), ftDescription)))
            nNode.Tag = CATEGORY_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
        End If
        
        ' Add Group branch.
        If (blnBuildGroup = True) Then
            strFieldCode = GetDBField(mrecHierarchyData.Fields("GrpNo"), False, , ftGroup)
            strGrpNode = GROUP_CODE & CStr(mlngGrpCounter)
            Set nNode = tvwHierarchy.Nodes.Add(strCatNode, tvwChild, strGrpNode, _
                        Trim(GetDBField(mrecHierarchyData.Fields("GrpDesc"), False, GROUP_CODE & CStr(mlngGrpCounter), ftDescription)))
            nNode.Tag = "X" & GROUP_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
        End If
        
        DoEvents
        ' Add Sub Group branch.
'        If (blnBuildSubGroup = True) Then
'            strFieldCode = GetDBField(mrecHierarchyData.Fields("SGrpNo"), False, , ftSubGroup)
'            strSGrpNode = SUBGROUP_CODE & CStr(mlngSGrpCounter)
'            Set nNode = tvwHierarchy.Nodes.Add(strGrpNode, tvwChild, strSGrpNode, _
'                        Trim(GetDBField(mrecHierarchyData.Fields("SGrpDesc"), False, SUBGROUP_CODE & CStr(mlngSGrpCounter), ftDescription)))
'            nNode.Tag = SUBGROUP_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
'        End If
        
        ' Add the new style leaf.
'        If (blnBuildStyle = True) Then
'            strFieldCode = GetDBField(mrecHierarchyData.Fields("StylNo"), False, , ftStyle)
'            strStyleNode = STYLE_CODE & CStr(mlngStyleCounter)
'            Set nNode = tvwHierarchy.Nodes.Add(strSGrpNode, tvwChild, strStyleNode, _
'                        Trim(GetDBField(mrecHierarchyData.Fields("StylDesc"), False, STYLE_CODE & CStr(mlngStyleCounter), ftDescription)))
'            nNode.Tag = STYLE_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
'        End If
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Second Pass Added:" & tvwHierarchy.Nodes.Count)
        
        ' Read the next record.
        mrecHierarchyData.MoveNext
    Loop
    tvwHierarchy.Visible = True
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Second Pass Completed:" & tvwHierarchy.Nodes.Count)

    If (mcnnDBConnection.State = adStateOpen) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Close Recordset")
'        mcnnDBConnection.Close
    End If
    Set nNode = Nothing
    Set tvwHierarchy = Nothing
 '   Set mrecHierarchyData = Nothing
    Set mcnnDBConnection = Nothing

    ' If processed both trees then flag as ended the load.
    If (metLoadTreeEnquiryType = etFullItems) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Processed Both Trees")
        mblnIsControlLoading = False
        fraHierarchyLoading.Visible = False
        If (mblnShowItemFilter = True) Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Focus Item Filter")
            Sleep (50)
            Me.Hide
            Me.Show
            Me.SetFocus
            Call ShowForm
            mblnShowItemFilter = False
        End If
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Load Full Data")
        ' Load process the Full products tree.
        metLoadTreeEnquiryType = etFullItems
        Call LoadTreeData
    End If
    
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Enquiry Type - PIM")
        tvwHierarchyPIM.Visible = True
        tvwHierarchyFull.Visible = False
        tvwHierarchyPIM.SetFocus
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Enquiry Type - Full")
        tvwHierarchyPIM.Visible = False
        tvwHierarchyFull.Visible = True
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Tree View PIM Visible - " & tvwHierarchyPIM.Visible)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Tree View Full Visible - " & tvwHierarchyFull.Visible)

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub

BuildTreeData_Error:

    If Not (mcnnDBConnection Is Nothing) Then
        If (mcnnDBConnection.State = adStateOpen) Then
            mcnnDBConnection.Close
        End If
    End If
    Set nNode = Nothing
    Set tvwHierarchy = Nothing
'    Set mrecHierarchyData = Nothing
    Set mcnnDBConnection = Nothing

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

    mblnIsControlLoading = False
    fraHierarchyLoading.Visible = False

End Sub

Private Sub BuildGroupBranch(CategoryNode As MSComctlLib.Node, tvwHierarchy As TreeView)

Const PROCEDURE_NAME As String = "BuildGroupData"
    
Dim nNode        As Node

Dim strCatNode   As String
Dim strGrpNode   As String
Dim strSGrpNode  As String
Dim strStyleNode As String
Dim strFieldCode As String

Dim strCatCode   As String

Dim blnBuildSubGroup As Boolean
Dim blnBuildStyle    As Boolean

Dim intCounter       As Integer

Dim colGroupNodes   As New Collection
Dim GroupNode       As MSComctlLib.Node

    On Error GoTo BuildTreeData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    Select Case metLoadTreeEnquiryType
        Case etPIMOnly
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM Data")
        Case etFullItems
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Full Data")
    End Select
    
    tvwHierarchy.Visible = False
    
    'Extract List of Groups to locate sub Groups/Style for
    Set GroupNode = CategoryNode.Child.FirstSibling
    While (Not GroupNode Is Nothing)
        Call colGroupNodes.Add(GroupNode)
        'Flag Group as processed by removing Leading X from Tag
        GroupNode.Tag = Mid$(GroupNode.Tag, 2)
        Set GroupNode = GroupNode.Next
    Wend
    
    'Reset group markers to force adding of Nodes
    mstrSGrpNo = ""
    mstrStyleNo = ""
    
    For Each GroupNode In colGroupNodes
        strCatCode = Mid$(GroupNode.Tag, InStr(GroupNode.Tag, ",") + 1, 6)
        mrecHierarchyData.Filter = "GrpNo='" & strCatCode & "'"
    
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Step through Sub Groups and Add" & tvwHierarchy.Nodes.Count)
        ' Loop over the records and build the tree.
        Do Until (mrecHierarchyData.BOF) Or (mrecHierarchyData.EOF)
            ' Add the Category, Group, Sub Group and Style and loop over styles till any of the parent node values change, when
            '   this happens build a new branch and continue with the styles.
            blnBuildSubGroup = False
            blnBuildStyle = False
            ' If change of sub group.
            If (mstrSGrpNo <> GetDBField(mrecHierarchyData.Fields("SGrpNo"), True, , ftSubGroup)) Then
                ' Build new Sub Group branch.
                blnBuildSubGroup = True
                blnBuildStyle = True
            ElseIf (mstrStyleNo <> GetDBField(mrecHierarchyData.Fields("StylNo"), True, , ftStyle)) Then
                ' Build new Style leaf.
                blnBuildStyle = True
            End If
            
            DoEvents
            ' If required build the relevant branch.
            ' Add Sub Group branch.
            If (blnBuildSubGroup = True) Then
                strFieldCode = GetDBField(mrecHierarchyData.Fields("SGrpNo"), False, , ftSubGroup)
                strSGrpNode = SUBGROUP_CODE & CStr(mlngSGrpCounter)
                Set nNode = tvwHierarchy.Nodes.Add(GroupNode.Key, tvwChild, strSGrpNode, _
                            Trim(GetDBField(mrecHierarchyData.Fields("SGrpDesc"), False, SUBGROUP_CODE & CStr(mlngSGrpCounter), ftDescription)))
                nNode.Tag = SUBGROUP_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
            End If
            
            ' Add the new style leaf.
            If (blnBuildStyle = True) Then
                strFieldCode = GetDBField(mrecHierarchyData.Fields("StylNo"), False, , ftStyle)
                strStyleNode = STYLE_CODE & CStr(mlngStyleCounter)
                Set nNode = tvwHierarchy.Nodes.Add(strSGrpNode, tvwChild, strStyleNode, _
                            Trim(GetDBField(mrecHierarchyData.Fields("StylDesc"), False, STYLE_CODE & CStr(mlngStyleCounter), ftDescription)))
                nNode.Tag = STYLE_LEVEL & "," & strFieldCode & "," & HIERARCHY_NODE_NOT_SELECTED
            End If
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Second Pass Added:" & tvwHierarchy.Nodes.Count)
            
            ' Read the next record.
            mrecHierarchyData.MoveNext
        Loop
    Next
    
    tvwHierarchy.Visible = True
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Second Pass Completed:" & tvwHierarchy.Nodes.Count)

    Set nNode = Nothing
    Set tvwHierarchy = Nothing

    ' If processed both trees then flag as ended the load.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub

BuildTreeData_Error:

    Set nNode = Nothing
    Set tvwHierarchy = Nothing

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Test if the database value is null, if so return the default value, If blnTest = TRUE then just test the value code number.
Private Function GetDBField(ByVal field As ADODB.field, blnTest As Boolean, _
                    Optional strDefaultValue As String, Optional ftFieldType As FieldType) As String

Const PROCEDURE_NAME As String = "GetDBField"

Dim blnNoValue As Boolean
Dim strValue As String
    
    On Error GoTo GetDBField_Error
    
    ' Increment the relevant counter.
    If (blnTest = False) Then
        Select Case ftFieldType
            Case ftCategory
                mlngCatCounter = mlngCatCounter + 1
            Case ftGroup
                mlngGrpCounter = mlngGrpCounter + 1
            Case ftSubGroup
                mlngSGrpCounter = mlngSGrpCounter + 1
            Case ftStyle
                mlngStyleCounter = mlngStyleCounter + 1
        End Select
    End If
    
    ' Test if the field has a valid value.
    blnNoValue = False
    If IsNull(field) Then
        blnNoValue = True
    ElseIf (Trim(field) = "") Then
        blnNoValue = True
    Else
        If (blnTest = False) Then
            Select Case ftFieldType
                Case ftCategory
                    mstrCatNo = Trim(field)
                Case ftGroup
                    mstrGrpNo = Trim(field)
                Case ftSubGroup
                    mstrSGrpNo = Trim(field)
                Case ftStyle
                    mstrStyleNo = Trim(field)
            End Select
        End If
        GetDBField = Trim(field)
    End If
    
    ' Add a default value if required, if testing the Category, Group or Sub Group code then return a blank value if not found.
    If (blnNoValue = True) Then
        Select Case ftFieldType
            Case ftCategory
                If (blnTest = True) Then
                    mstrCatNo = ""
                    GetDBField = ""
                Else
                    GetDBField = CStr(mlngCatCounter)
                End If
            Case ftGroup
                If (blnTest = True) Then
                    mstrGrpNo = ""
                    GetDBField = ""
                Else
                    GetDBField = CStr(mlngGrpCounter)
                End If
            Case ftSubGroup
                If (blnTest = True) Then
                    mstrSGrpNo = ""
                    GetDBField = ""
                Else
                    GetDBField = CStr(mlngSGrpCounter)
                End If
            Case ftStyle
                If (blnTest = True) Then
                    mstrSGrpNo = ""
                    GetDBField = ""
                Else
                    GetDBField = CStr(mlngStyleCounter)
                End If
            Case ftDescription
                GetDBField = strDefaultValue
        End Select
    End If
    
    Exit Function

GetDBField_Error:
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Function

' Untags all the nodes in the hierarchy tree to flag as not having been selected previously, thus no data already
'   exists in the grid.
Private Sub UnTagTreeNodes()

Const PROCEDURE_NAME As String = "UnTagTreeNodes"

Dim nNode As Node
    
    On Error GoTo UnTagTreeNodes_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    ' Loop over the nodes in the hierarchy trees and set all the nodes tags to show that the node has not
    '   been previously selected.
    For Each nNode In tvwHierarchyPIM.Nodes
        nNode.Tag = Left(nNode.Tag, Len(nNode.Tag) - 1) & HIERARCHY_NODE_NOT_SELECTED
    Next nNode
    For Each nNode In tvwHierarchyFull.Nodes
        nNode.Tag = Left(nNode.Tag, Len(nNode.Tag) - 1) & HIERARCHY_NODE_NOT_SELECTED
    Next nNode
        
    Set nNode = Nothing

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub
    
UnTagTreeNodes_Error:

    Set nNode = Nothing

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Tag the required nodes as have been selected.
Private Sub TagTreeNodes(nPassedNode As Node)

Const PROCEDURE_NAME As String = "TagTreeNodes"

Dim nNode      As Node
Dim intCounter As Integer
  
    On Error GoTo TagTreeNodes_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    ' If using PIM products then tag the selected node and any child nodes.
    If Not (nPassedNode Is Nothing) Then
        If (metEnquiryType = etPIMOnly) Then
            nPassedNode.Tag = Left(nPassedNode.Tag, Len(nPassedNode.Tag) - 1) & HIERARCHY_NODE_SELECTED
            ' Loop over the children of the node.
            For intCounter = 1 To nPassedNode.Children
                If (intCounter = 1) Then
                    Set nNode = nPassedNode.Child
                Else
              '      Set nNode = nPassedNode.Child
              '      For intChildCounter = 2 To intCounter
                    Set nNode = nNode.Next
               '     Next intChildCounter
                End If
                ' If the node is at the Style level then just set the node as selected.
                If (Left(nNode.Tag, 1) = STYLE_LEVEL) Then
                    nNode.Tag = Left(nNode.Tag, Len(nNode.Tag) - 1) & HIERARCHY_NODE_SELECTED
                Else
                    ' Otherwise call this method again passing in the current node to loop over its children.
                    Call TagTreeNodes(nNode)
                End If
            Next intCounter
        Else
            ' If using the full product listing then only then tag the selected node.
            nPassedNode.Tag = Left(nPassedNode.Tag, Len(nPassedNode.Tag) - 1) & HIERARCHY_NODE_SELECTED
        End If
    
        Set nNode = Nothing
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub
    
TagTreeNodes_Error:

    Set nNode = Nothing

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Contracts the nodes on the hierarchy trees.
Private Sub ContractTree()

Const PROCEDURE_NAME As String = "ContractTree"

Dim nNode As Node
    
    On Error GoTo ContractTree_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    ' Loop over the nodes in the hierarchy tree and set all the nodes as not expanded.
    ' Stop the tree from scrolling.
    tvwHierarchyPIM.Scroll = False
    For Each nNode In tvwHierarchyPIM.Nodes
        nNode.Expanded = False
    Next nNode
    tvwHierarchyPIM.Scroll = True
    tvwHierarchyFull.Scroll = False
    For Each nNode In tvwHierarchyFull.Nodes
        nNode.Expanded = False
    Next nNode
    tvwHierarchyFull.Scroll = True
    
    Set nNode = Nothing

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub
    
ContractTree_Error:

    Set nNode = Nothing
    tvwHierarchyPIM.Scroll = True
    tvwHierarchyFull.Scroll = True

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Loads the thumbnail images for the SKUs in the grid.
Private Sub LoadImages(gisImageGridLoadType As ImageGridLoadType)

Const PROCEDURE_NAME As String = "LoadImages"

Dim fsoFileSystem As Scripting.FileSystemObject
Dim strImagePath     As String
Dim tvwHierarchy     As TreeView
Dim nNode            As Node
Dim lngCounter       As Long
Dim intCounter       As Integer
Dim intColumnCounter As Integer
Dim strImageFile     As String
Dim strSKU           As String
Dim strDescription   As String
Dim strPrice         As String
Dim blnLoadImages    As Boolean
Dim ftHierarchyLevel As FieldType
Dim strEndImageSKU   As String
Dim lngImagesToLoad  As Long
Dim strPages         As String

    On Error GoTo LoadImages_Error
             
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    mblnStopLoad = False
    
    If (sprdThumbnails.Visible = True) Then
        fraImagesLoading.Visible = True
    End If
    
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM Images")
        Set tvwHierarchy = tvwHierarchyPIM
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Full Images")
        Set tvwHierarchy = tvwHierarchyFull
    End If
    
    ' Test if the Hierarchy node selected is at a level which allows the images to be displayed.
    If Not (tvwHierarchy.SelectedItem Is Nothing) Then
        ' Get the selected hierarchy tree node.
        Set nNode = tvwHierarchy.SelectedItem
    
        ' Get the hierarchy level of the selected node.
        ftHierarchyLevel = Left(nNode.Tag, 1)
        If (mvilViewImagesLevel <= ftHierarchyLevel) Then
            blnLoadImages = True
        Else
            blnLoadImages = False
        End If
    Else
        ' If no hierarchy level selected then if set to view images at the Category level then show images else don't.
        If (mvilViewImagesLevel = vilCategory) Then
            blnLoadImages = True
        Else
            blnLoadImages = False
        End If
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Show Images - " & blnLoadImages)
    
    ' If images already exist on the grid and are scrolling to the next set of images get the row on the list
    '   as the first SKU to load images for.
    If (blnLoadImages = True) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Load Images")
    
        If (gisImageGridLoadType = gisScrollDown) Then
            ' Make sure not already loaded the last set of images.
            If (mlngLoadedImageRow + mlngImagesPerPage < lstMatches.ListCount) Then
                mlngLoadedImageRow = mlngLoadedImageRow + mlngImagesPerPage
                mintImagePage = mintImagePage + 1
            Else
                blnLoadImages = False
            End If
        ElseIf (gisImageGridLoadType = gisScrollUp) Then
            ' Make sure not already loaded the first set of images.
            If (mlngLoadedImageRow - mlngImagesPerPage >= 0) Then
                mlngLoadedImageRow = mlngLoadedImageRow - mlngImagesPerPage
                mintImagePage = mintImagePage - 1
            Else
                blnLoadImages = False
            End If
        ElseIf (gisImageGridLoadType = gisImageFind) Then
            ' Load image page containing the SKU selected on the list.
            mlngLoadedImageRow = (mintImagePage - 1) * mlngImagesPerPage
        ElseIf (gisImageGridLoadType = gisNone) Then
            ' Load first page of images.
            mlngLoadedImageRow = 0
            mintImagePage = 1
        End If
        lngImagesToLoad = mintImagePage * mlngImagesPerPage
        ' Set the label to show the current and total number of image pages.
        If (lstMatches.ListCount Mod mlngImagesPerPage = 0) Then
            lblImagePages.Caption = mintImagePage & " of " & CLng(lstMatches.ListCount / mlngImagesPerPage)
        Else
            strPages = CStr(Round(lstMatches.ListCount / mlngImagesPerPage, 2))
            If (InStr(strPages, ".") > 0) Then
                strPages = Left(strPages, InStr(strPages, ".") - 1)
            End If
            lblImagePages.Caption = mintImagePage & " of " & (CLng(strPages) + 1)
        End If
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image Page - " & lblImagePages.Caption)
    End If
    
    If (blnLoadImages = True) Then
                
        mblnLoadingImages = True
        
        sprdThumbnails.Redraw = False
        sprdThumbnails.MaxRows = 0

        Set fsoFileSystem = New Scripting.FileSystemObject
        
        ' If the enquiry type mode is PIM only then load the thumbnails from the PIM image directory.
        If (metEnquiryType = etPIMOnly) Then
            strImagePath = mstrPIMImagePath
        Else
            ' If the enquiry type mode is Full products then load the thumbnails from the full image directory.
            strImagePath = mstrFullImagePath
        End If
        
        ' Test if the path exists.
        If (fsoFileSystem.FolderExists(strImagePath) = True) Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image Folder - " & strImagePath & " exists")
            intColumnCounter = 1
            sprdThumbnails.MaxRows = 2
            sprdThumbnails.Col = 1
            sprdThumbnails.Row = 1
            If (msrScreenResolution = srLow) Then
                sprdThumbnails.Col2 = GRID_COLUMNS_LOW_RES
            Else
                sprdThumbnails.Col2 = GRID_COLUMNS_HIGH_RES
            End If
            sprdThumbnails.RowHeight(sprdThumbnails.MaxRows - 1) = GRID_IMAGE_ROW_HEIGHT
            sprdThumbnails.RowHeight(sprdThumbnails.MaxRows) = GRID_TEXT_ROW_HEIGHT
            sprdThumbnails.Row = sprdThumbnails.MaxRows
            sprdThumbnails.Row2 = sprdThumbnails.MaxRows
            sprdThumbnails.BlockMode = True
            sprdThumbnails.CellType = CellTypeStaticText
            sprdThumbnails.TypeTextWordWrap = True
            sprdThumbnails.TypeHAlign = TypeHAlignCenter
            sprdThumbnails.TypeVAlign = TypeVAlignTop
            sprdThumbnails.BlockMode = False
            sprdThumbnails.Row = 1
            Call sprdThumbnails.SetActiveCell(1, 1)
            ' Loop over the SKUs in the grid and load the thumbnail images.
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Load Images - " & mlngLoadedImageRow & " To " & (lngImagesToLoad - 1))
            For lngCounter = mlngLoadedImageRow To (lngImagesToLoad - 1)
                DoEvents
                If (mblnStopLoad = True) Then
                    Exit For
                Else
                    lstMatches.Row = lngCounter
                    If (lstMatches.RowHide = False) Then
                        sprdThumbnails.Col = intColumnCounter
                        strSKU = Trim(lstMatches.colList(COL_SKU, lngCounter))
                        strDescription = Trim(lstMatches.colList(COL_DESCRIPTION, lngCounter))
                        strPrice = Trim(lstMatches.colList(COL_PRICE, lngCounter))
                        strImageFile = strImagePath & strSKU & SMALL_IMAGE_FILE_EXTENSION
                        sprdThumbnails.Row = sprdThumbnails.MaxRows - 1
                        sprdThumbnails.CellType = CellTypePicture
                        ' Test if the image file exists.
                        If (fsoFileSystem.FileExists(strImageFile) = True) Then
                            ' Load the image.
                            sprdThumbnails.TypePictPicture = sprdThumbnails.LoadPicture(strImageFile, PictureTypeJPEG)
                        Else
                            ' Load the 'No Image Available' image from the resource file.
                            sprdThumbnails.TypePictPicture = sprdThumbnails.LoadResPicture(App.hInstance, "#101", "Bitmap", PictureTypeBMP)
                        End If
                        sprdThumbnails.TypePictCenter = True
                        sprdThumbnails.CellTag = strSKU
                        
                        ' Add the image description.
                        sprdThumbnails.Row = sprdThumbnails.MaxRows
                        sprdThumbnails.Text = strDescription & vbLf & "SKU - " & strSKU & " " & Format(strPrice, ChrW(163) & "0.00")
                    
                        ' The High resolution screen has 4 images per row the low resolution screen has 3 images per row.
                        If (msrScreenResolution = srLow) Then
                            ' If placed images in all columns for the current row move to the next row.
                            If (intColumnCounter >= GRID_COLUMNS_LOW_RES) And (lngCounter <> lstMatches.ListCount - 1) Then
                                If (lngCounter <> lngImagesToLoad - 1) Then
                                    sprdThumbnails.MaxRows = sprdThumbnails.MaxRows + 2
                                    sprdThumbnails.Col = 1
                                    sprdThumbnails.Col2 = GRID_COLUMNS_LOW_RES
                                    sprdThumbnails.RowHeight(sprdThumbnails.MaxRows - 1) = GRID_IMAGE_ROW_HEIGHT
                                    sprdThumbnails.RowHeight(sprdThumbnails.MaxRows) = GRID_TEXT_ROW_HEIGHT
                                    sprdThumbnails.Row = sprdThumbnails.MaxRows
                                    sprdThumbnails.Row2 = sprdThumbnails.MaxRows
                                    sprdThumbnails.BlockMode = True
                                    sprdThumbnails.CellType = CellTypeStaticText
                                    sprdThumbnails.TypeTextWordWrap = True
                                    sprdThumbnails.TypeHAlign = TypeHAlignCenter
                                    sprdThumbnails.TypeVAlign = TypeVAlignTop
                                    sprdThumbnails.BlockMode = False
                                    sprdThumbnails.Row = sprdThumbnails.MaxRows - 1
                                    intColumnCounter = 1
                                End If
                            Else
                                intColumnCounter = intColumnCounter + 1
                            End If
                        Else
                            ' If placed images in all columns for the current row move to the next row.
                            If (intColumnCounter >= GRID_COLUMNS_HIGH_RES) And (lngCounter <> lstMatches.ListCount - 1) Then
                                If (lngCounter <> lngImagesToLoad - 1) Then
                                    sprdThumbnails.MaxRows = sprdThumbnails.MaxRows + 2
                                    sprdThumbnails.Col = 1
                                    sprdThumbnails.Col2 = GRID_COLUMNS_HIGH_RES
                                    sprdThumbnails.RowHeight(sprdThumbnails.MaxRows - 1) = GRID_IMAGE_ROW_HEIGHT
                                    sprdThumbnails.RowHeight(sprdThumbnails.MaxRows) = GRID_TEXT_ROW_HEIGHT
                                    sprdThumbnails.Row = sprdThumbnails.MaxRows
                                    sprdThumbnails.Row2 = sprdThumbnails.MaxRows
                                    sprdThumbnails.BlockMode = True
                                    sprdThumbnails.CellType = CellTypeStaticText
                                    sprdThumbnails.TypeTextWordWrap = True
                                    sprdThumbnails.TypeHAlign = TypeHAlignCenter
                                    sprdThumbnails.TypeVAlign = TypeVAlignTop
                                    sprdThumbnails.BlockMode = False
                                    sprdThumbnails.Row = sprdThumbnails.MaxRows - 1
                                    intColumnCounter = 1
                                End If
                            Else
                                intColumnCounter = intColumnCounter + 1
                            End If
                        End If
                    End If
                End If
                If (lngCounter = lstMatches.ListCount - 1) Then
                    ' Loaded last sku image.
                    Exit For
                End If
            Next lngCounter
            
            ' If there are empty cells on the last row then tag them as being empty so they can't be selected.
            sprdThumbnails.Row = sprdThumbnails.MaxRows - 1
            If (msrScreenResolution = srLow) Then
                For lngCounter = intColumnCounter To GRID_COLUMNS_LOW_RES
                    sprdThumbnails.Col = lngCounter
                    If (sprdThumbnails.CellTag = "") Then
                        sprdThumbnails.CellTag = EMPTY_CELL_TAG_TEXT
                    End If
                Next lngCounter
            Else
                For lngCounter = intColumnCounter To GRID_COLUMNS_HIGH_RES
                    sprdThumbnails.Col = lngCounter
                    If (sprdThumbnails.CellTag = "") Then
                        sprdThumbnails.CellTag = EMPTY_CELL_TAG_TEXT
                    End If
                Next lngCounter
            End If
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Images Loaded")

            If (sprdThumbnails.MaxRows <> 0) Then
                If (gisImageGridLoadType = gisScrollUp) Then
                    ' Select the last cell in the grid.
                    If (msrScreenResolution = srLow) Then
                        sprdThumbnails.Col = GRID_COLUMNS_LOW_RES
                    Else
                        sprdThumbnails.Col = GRID_COLUMNS_HIGH_RES
                    End If
                    sprdThumbnails.Row = sprdThumbnails.MaxRows
                    sprdThumbnails.BackColor = RGB_LTRED
                    sprdThumbnails.Row = sprdThumbnails.MaxRows - 1
                    Call sprdThumbnails.SetActiveCell(sprdThumbnails.Col, sprdThumbnails.Row)
                    If (sprdThumbnails.IsVisible(sprdThumbnails.Col, sprdThumbnails.Row + 1, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(sprdThumbnails.Col, sprdThumbnails.Row + 1)
                        Call sprdThumbnails.SetActiveCell(sprdThumbnails.Col, sprdThumbnails.Row)
                    End If
                Else
                    ' Select the first cell in the grid.
                    sprdThumbnails.Col = 1
                    sprdThumbnails.Row = 2
                    sprdThumbnails.BackColor = RGB_LTRED
                    sprdThumbnails.Row = 1
                End If
                If (sprdThumbnails.Visible = True) Then
                    Call DoesLargeImageExist(sprdThumbnails.CellTag)
                End If
            End If
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image Path does not exist - " & strImagePath)
        End If
    End If
    
    fraImagesLoading.Visible = False
    
    sprdThumbnails.Redraw = True
    
    mblnLoadingImages = False
    
    Set fsoFileSystem = Nothing
    Set nNode = Nothing
    Set tvwHierarchy = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")
    
    Exit Sub
    
LoadImages_Error:
    
    fraImagesLoading.Visible = False
    
    sprdThumbnails.Redraw = True
    
    mblnLoadingImages = False
    
    Set fsoFileSystem = Nothing
    Set nNode = Nothing
    Set tvwHierarchy = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Filters the grid showing only the required rows.
Private Sub FilterGrid()

Const PROCEDURE_NAME As String = "FilterGrid"

Dim tvwHierarchy         As TreeView
Dim nNode                As Node
Dim ftNodeHierarchyLevel As FieldType
Dim strCategoryCode      As String
Dim strGroupCode         As String
Dim strSubGroupCode      As String
Dim strStyleCode         As String
Dim arrTag()             As String
Dim lngRowCounter        As Long
Dim blnFound             As Boolean
Dim lngFirstRow          As Long
Dim lngLastRow           As Long

    On Error GoTo FilterGrid_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM Data")
        Set tvwHierarchy = tvwHierarchyPIM
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Full Data")
        Set tvwHierarchy = tvwHierarchyFull
    End If
    
    If (lstMatches.ListCount <> 0) And Not (tvwHierarchy.SelectedItem Is Nothing) Then
        ' Get the selected hierarchy tree node.
        Set nNode = tvwHierarchy.SelectedItem
        
        ' Get the hierarchy level of the selected node.
        Select Case Left(nNode.Tag, 1)
            Case CATEGORY_LEVEL
                ftNodeHierarchyLevel = ftCategory
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
            Case GROUP_LEVEL
                ftNodeHierarchyLevel = ftGroup
                arrTag = Split(nNode.Tag, ",")
                strGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
            Case SUBGROUP_LEVEL
                ftNodeHierarchyLevel = ftSubGroup
                arrTag = Split(nNode.Tag, ",")
                strSubGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
            Case STYLE_LEVEL
                ftNodeHierarchyLevel = ftStyle
                arrTag = Split(nNode.Tag, ",")
                strStyleCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strSubGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strGroupCode = arrTag(1)
                Set nNode = nNode.Parent
                arrTag = Split(nNode.Tag, ",")
                strCategoryCode = arrTag(1)
        End Select
        
        ' Freeze the grid screen updating.
        lstMatches.Redraw = False
        
        ' Sort the grid.
        lstMatches.SortState = SortStateSuspend
        lstMatches.Col = COL_CATEGORY
        ' Set which grid columns are part of the sort.
        If (ftNodeHierarchyLevel >= ftCategory) Then
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortSeq = 0
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
        Else
            lstMatches.ColSorted = SortedNone
        End If
        lstMatches.Col = COL_GROUP
        If (ftNodeHierarchyLevel >= ftGroup) Then
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortSeq = 1
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
        Else
            lstMatches.ColSorted = SortedNone
        End If
        lstMatches.Col = COL_SUBGROUP
        If (ftNodeHierarchyLevel >= ftSubGroup) Then
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortSeq = 2
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
        Else
            lstMatches.ColSorted = SortedNone
        End If
        lstMatches.Col = COL_STYLE
        If (ftNodeHierarchyLevel >= ftStyle) Then
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortSeq = 3
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
        Else
            lstMatches.ColSorted = SortedNone
        End If
        ' Perform the sort.
        lstMatches.SortState = SortStateActiveReSort
        
        ' If the enquiry type is PIM only then filter the grid based upon the hierarchy branch selected.
        blnFound = False
        If (metEnquiryType = etPIMOnly) Then
            ' Loop over the grid to find the first full matching row.
            Select Case ftNodeHierarchyLevel
                Case ftCategory
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_CATEGORY, lngRowCounter) = strCategoryCode) Then
                            blnFound = True
                            lngFirstRow = lngRowCounter
                            Exit For
                        End If
                    Next lngRowCounter
                Case ftGroup
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_GROUP, lngRowCounter) = strGroupCode) Then
                            If (lstMatches.colList(COL_CATEGORY, lngRowCounter) = strCategoryCode) Then
                                blnFound = True
                                lngFirstRow = lngRowCounter
                                Exit For
                            End If
                        End If
                    Next lngRowCounter
                Case ftSubGroup
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_SUBGROUP, lngRowCounter) = strSubGroupCode) Then
                            If (lstMatches.colList(COL_GROUP, lngRowCounter) = strGroupCode) Then
                                If (lstMatches.colList(COL_CATEGORY, lngRowCounter) = strCategoryCode) Then
                                    blnFound = True
                                    lngFirstRow = lngRowCounter
                                    Exit For
                                End If
                            End If
                        End If
                    Next lngRowCounter
                Case ftStyle
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_STYLE, lngRowCounter) = strStyleCode) Then
                            If (lstMatches.colList(COL_SUBGROUP, lngRowCounter) = strSubGroupCode) Then
                                If (lstMatches.colList(COL_GROUP, lngRowCounter) = strGroupCode) Then
                                    If (lstMatches.colList(COL_CATEGORY, lngRowCounter) = strCategoryCode) Then
                                        blnFound = True
                                        lngFirstRow = lngRowCounter
                                        Exit For
                                    End If
                                End If
                            End If
                        End If
                    Next lngRowCounter
            End Select
            
            ' Find the last row of matching data.
            If (blnFound = True) Then
                Select Case ftNodeHierarchyLevel
                    Case ftCategory
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_CATEGORY, lngRowCounter) <> strCategoryCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                    Case ftGroup
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_CATEGORY, lngRowCounter) <> strCategoryCode) Or _
                                    (lstMatches.colList(COL_GROUP, lngRowCounter) <> strGroupCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                    Case ftSubGroup
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_CATEGORY, lngRowCounter) <> strCategoryCode) Or _
                                    (lstMatches.colList(COL_GROUP, lngRowCounter) <> strGroupCode) Or _
                                    (lstMatches.colList(COL_SUBGROUP, lngRowCounter) <> strSubGroupCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                    Case ftStyle
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_CATEGORY, lngRowCounter) <> strCategoryCode) Or _
                                    (lstMatches.colList(COL_GROUP, lngRowCounter) <> strGroupCode) Or _
                                    (lstMatches.colList(COL_SUBGROUP, lngRowCounter) <> strSubGroupCode) Or _
                                    (lstMatches.colList(COL_STYLE, lngRowCounter) <> strStyleCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                End Select
                ' If not found end of section then set the end to be the end of the list.
                If (lngLastRow = 0) Then
                    lngLastRow = lstMatches.ListCount - 1
                End If
            
                ' Hide the preceding rows to the matching data.
                For lngRowCounter = 0 To lngFirstRow - 1
                    lstMatches.Row = lngRowCounter
                    lstMatches.RowHide = True
                Next lngRowCounter
                
                ' Show the matching rows.
                For lngRowCounter = lngFirstRow To lngLastRow
                    lstMatches.Row = lngRowCounter
                    lstMatches.RowHide = False
                Next lngRowCounter
                
                ' Hide the rows succeeding the matching data.
                For lngRowCounter = lngLastRow + 1 To lstMatches.ListCount - 1
                    lstMatches.Row = lngRowCounter
                    lstMatches.RowHide = True
                Next lngRowCounter
                
            End If
            
            ' Sort the grid on the Display Order column.
            lstMatches.SortState = SortStateSuspend
            lstMatches.Col = COL_CATEGORY
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
            lstMatches.Col = COL_GROUP
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
            lstMatches.Col = COL_SUBGROUP
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
            lstMatches.Col = COL_STYLE
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
            lstMatches.Col = COL_DISPLAY_ORDER
            lstMatches.ColSorted = SortedNone
            ' Perform the sort.
            lstMatches.SortState = SortStateActiveReSort
            
        Else
            ' If the enquiry type is full products then filter the grid based upon only the hierarchy node selected.
            ' Do a search to find the first row matching for the hierarchy node selected.
            
            ' Loop over the grid to find the first full matching row.
            Select Case ftNodeHierarchyLevel
                Case ftCategory
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_CATEGORY, lngRowCounter) = strCategoryCode) Then
                            blnFound = True
                            lngFirstRow = lngRowCounter
                            Exit For
                        End If
                    Next lngRowCounter
                Case ftGroup
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_GROUP, lngRowCounter) = strGroupCode) Then
                            blnFound = True
                            lngFirstRow = lngRowCounter
                            Exit For
                        End If
                    Next lngRowCounter
                Case ftSubGroup
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_SUBGROUP, lngRowCounter) = strSubGroupCode) Then
                            blnFound = True
                            lngFirstRow = lngRowCounter
                            Exit For
                        End If
                    Next lngRowCounter
                Case ftStyle
                    For lngRowCounter = 0 To lstMatches.ListCount - 1
                        If (lstMatches.colList(COL_STYLE, lngRowCounter) = strStyleCode) Then
                            blnFound = True
                            lngFirstRow = lngRowCounter
                            Exit For
                        End If
                    Next lngRowCounter
            End Select
            
            ' Find the last row of matching data.
            If (blnFound = True) Then
                Select Case ftNodeHierarchyLevel
                    Case ftCategory
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_CATEGORY, lngRowCounter) <> strCategoryCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                    Case ftGroup
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_GROUP, lngRowCounter) <> strGroupCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                    Case ftSubGroup
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_SUBGROUP, lngRowCounter) <> strSubGroupCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                    Case ftStyle
                        For lngRowCounter = lngFirstRow + 1 To lstMatches.ListCount - 1
                            If (lstMatches.colList(COL_STYLE, lngRowCounter) <> strStyleCode) Then
                                lngLastRow = lngRowCounter - 1
                                Exit For
                            End If
                        Next lngRowCounter
                End Select
                ' If not found end of section then set the end to be the end of the list.
                If (lngLastRow = 0) Then
                    lngLastRow = lstMatches.ListCount - 1
                End If
                
                ' Hide the preceding rows to the matching data.
                For lngRowCounter = 0 To lngFirstRow - 1
                    lstMatches.Row = lngRowCounter
                    lstMatches.RowHide = True
                Next lngRowCounter
                
                ' Show the matching rows.
                For lngRowCounter = lngFirstRow To lngLastRow
                    lstMatches.Row = lngRowCounter
                    lstMatches.RowHide = False
                Next lngRowCounter
                
                ' Hide the rows succeeding the matching data.
                For lngRowCounter = lngLastRow + 1 To lstMatches.ListCount - 1
                    lstMatches.Row = lngRowCounter
                    lstMatches.RowHide = True
                Next lngRowCounter
            End If
            
            ' Sort the grid on the Display Order column.
            lstMatches.SortState = SortStateSuspend
            lstMatches.Col = COL_CATEGORY
            lstMatches.ColSorted = SortedNone
            lstMatches.Col = COL_GROUP
            lstMatches.ColSorted = SortedNone
            lstMatches.Col = COL_SUBGROUP
            lstMatches.ColSorted = SortedNone
            lstMatches.Col = COL_STYLE
            lstMatches.ColSorted = SortedNone
            lstMatches.Col = COL_DISPLAY_ORDER
            lstMatches.ColSorted = SortedAscending
            lstMatches.ColSortDataType = ColSortDataTypeTextCase
            ' Perform the sort.
            lstMatches.SortState = SortStateActiveReSort
            
        End If
        
        ' Un-Freeze the grid screen updating.
        lstMatches.Redraw = True
    End If
    
    Set nNode = Nothing
    Set tvwHierarchy = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")
    
    Exit Sub
    
FilterGrid_Error:

    Set nNode = Nothing
    Set tvwHierarchy = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)
    
End Sub

' Test if the enlarged image exists for the given SKU.
Private Function DoesLargeImageExist(strSKU As String) As Boolean

Const PROCEDURE_NAME As String = "DoesLargeImageExist"

Dim fsoFileSystem As Scripting.FileSystemObject

Dim strImagePath  As String
Dim strImageFile  As String
      
    On Error GoTo DoesLargeImageExist_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    ' If the enquiry type mode is PIM only then load the thumbnails from the PIM image directory.
    If (metEnquiryType = etPIMOnly) Then
        strImagePath = mstrPIMImagePath
    Else
        ' If the enquiry type mode is Full products then load the thumbnails from the full image directory.
        strImagePath = mstrFullImagePath
    End If
    
    Set fsoFileSystem = New Scripting.FileSystemObject
    
    ' Test if the path exists.
    If (fsoFileSystem.FolderExists(strImagePath) = True) Then
        ' Test if the image file exists.
        strImageFile = strImagePath & strSKU & LARGE_IMAGE_FILE_EXTENSION
        If (fsoFileSystem.FileExists(strImageFile)) Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image File - " & strImageFile & " Exists")
            DoesLargeImageExist = True
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image File - " & strImageFile & " Does Not Exist")
            DoesLargeImageExist = False
        End If
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image Folder - " & strImagePath & " Does Not Exist")
        DoesLargeImageExist = False
    End If
    
    ' Show or hide the button to allow showing the enlarged image.
    cmdEnlarge.Visible = DoesLargeImageExist
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Function
    
DoesLargeImageExist_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Function

' Load the large image for the selected SKU.
Private Sub LoadLargeImage()

Const PROCEDURE_NAME As String = "LoadLargeImage"

Dim strSKU         As String
Dim strDescription As String
Dim strPrice       As String
Dim strImagePath   As String

    On Error GoTo LoadLargeImage_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (lstMatches.Visible = True) Then
        lstMatches.Col = COL_SKU
        strSKU = lstMatches.ColText
        lstMatches.Col = COL_PRICE
        strPrice = lstMatches.ColText
        lstMatches.Col = COL_DESCRIPTION
        strDescription = lstMatches.ColText & vbCrLf & "SKU - " & strSKU & " " & Format(strPrice, ChrW(163) & ".00")
    Else
        sprdThumbnails.Row = sprdThumbnails.ActiveRow
        strSKU = sprdThumbnails.CellTag
        sprdThumbnails.Row = sprdThumbnails.ActiveRow + 1
        strDescription = Replace(sprdThumbnails.Text, vbLf, vbCrLf)
        sprdThumbnails.Row = sprdThumbnails.ActiveRow
    End If
    
    ' If the enquiry type mode is PIM only then load the thumbnails from the PIM image directory.
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM Image")
        strImagePath = mstrPIMImagePath
    Else
        ' If the enquiry type mode is Full products then load the thumbnails from the full image directory.
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Full Image")
        strImagePath = mstrFullImagePath
    End If
    
    Load frmItemViewer
    With frmItemViewer
        .ImagePath = strImagePath
        If (.LoadImage(strSKU) = True) Then
            .Description = strDescription
            .ShowSelectButton = cmdSelect.Visible
            .Show vbModal, Me
            If (frmItemViewer.ItemSelected = True) Then
                Unload frmItemViewer
                Call cmdSelect_Click
            End If
        Else
            Unload frmItemViewer
        End If
    End With

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")
    
    Exit Sub
    
LoadLargeImage_Error:
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Highlight the current SKU from the list in the image grid.
Private Sub HighlightImageItem()

Const PROCEDURE_NAME As String = "HighlightImageItem"

Dim strSKU        As String
Dim lngRowCounter As Long
Dim intColCounter As Integer
Dim blnFound      As Boolean

    On Error GoTo HighlightImageItem_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    If (lstMatches.ListCount = 0) Then Exit Sub
    ' Get the SKU of the selected item in the list.
    lstMatches.Col = COL_SKU
    strSKU = lstMatches.ColText
    
    ' Test if the list SKU is on the loaded image page, is not then load the correct page and find the image.
    If (lstMatches.ListIndex < ((mintImagePage - 1) * mlngImagesPerPage)) Or _
        (lstMatches.ListIndex > ((mintImagePage * mlngImagesPerPage) - 1)) Then
        If (((lstMatches.ListIndex + 1) Mod mlngImagesPerPage) = 0) Then
            mintImagePage = Int((lstMatches.ListIndex + 1) / mlngImagesPerPage)
        Else
            mintImagePage = Int(((lstMatches.ListIndex + 1) / mlngImagesPerPage)) + 1
        End If
        ' Load the correct page of images.
        Call LoadImages(gisImageFind)
    End If
    
    ' Clear the current item.
    sprdThumbnails.Row = sprdThumbnails.ActiveRow + 1
    sprdThumbnails.Col = sprdThumbnails.ActiveCol
    sprdThumbnails.BackColor = RGB_WHITE

    ' Loop over the grid to search for the image with a matching SKU.
    blnFound = False
    If (msrScreenResolution = srLow) Then
        For lngRowCounter = 1 To sprdThumbnails.MaxRows
            For intColCounter = 1 To GRID_COLUMNS_LOW_RES
                sprdThumbnails.Row = lngRowCounter
                sprdThumbnails.Col = intColCounter
                If (sprdThumbnails.CellTag = strSKU) Then
                    Call sprdThumbnails.SetActiveCell(intColCounter, lngRowCounter)
                    ' Make the text viewable on the screen if dropped off the bottom of the grid.
                    If (sprdThumbnails.IsVisible(intColCounter, lngRowCounter + 1, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(intColCounter, lngRowCounter + 1)
                        Call sprdThumbnails.SetActiveCell(intColCounter, lngRowCounter)
                    End If
                    sprdThumbnails.Col = intColCounter
                    sprdThumbnails.Row = lngRowCounter + 1
                    sprdThumbnails.BackColor = RGB_LTRED
                    blnFound = True
                    Exit Sub
                End If
            Next intColCounter
        Next lngRowCounter
    Else
        For lngRowCounter = 1 To sprdThumbnails.MaxRows
            For intColCounter = 1 To GRID_COLUMNS_HIGH_RES
                sprdThumbnails.Row = lngRowCounter
                sprdThumbnails.Col = intColCounter
                If (sprdThumbnails.CellTag = strSKU) Then
                    Call sprdThumbnails.SetActiveCell(intColCounter, lngRowCounter)
                    ' Make the text viewable on the screen if dropped off the bottom of the grid.
                    If (sprdThumbnails.IsVisible(intColCounter, lngRowCounter + 1, False) = False) Then
                        Call sprdThumbnails.SetActiveCell(intColCounter, lngRowCounter + 1)
                        Call sprdThumbnails.SetActiveCell(intColCounter, lngRowCounter)
                    End If
                    sprdThumbnails.Col = intColCounter
                    sprdThumbnails.Row = lngRowCounter + 1
                    sprdThumbnails.BackColor = RGB_LTRED
                    blnFound = True
                    Exit Sub
                End If
            Next intColCounter
        Next lngRowCounter
    End If
    ' If the SKU was not found then select the first image on the grid.
    If (blnFound = False) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Image Not Found")
        Call sprdThumbnails.SetActiveCell(1, 1)
        sprdThumbnails.Row = 2
        sprdThumbnails.Col = 1
        sprdThumbnails.BackColor = RGB_LTRED
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")

    Exit Sub
    
HighlightImageItem_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Highlight the current SKU from the image grid in the list.
Private Sub HighlightListItem()

Const PROCEDURE_NAME As String = "HighlightListItem"

Dim strSKU        As String
Dim lngRowCounter As Long

    On Error GoTo HighlightListItem_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' Get the SKU of the selected item in the grid.
    sprdThumbnails.Col = sprdThumbnails.ActiveCol
    sprdThumbnails.Row = sprdThumbnails.ActiveRow
    strSKU = sprdThumbnails.CellTag
    If (strSKU <> "") Then
        ' Loop over the list to search for the row with a matching SKU.
        For lngRowCounter = 0 To lstMatches.ListCount - 1
            lstMatches.Row = lngRowCounter
            If (lstMatches.colList(COL_SKU, lngRowCounter) = strSKU) Then
                lstMatches.Selected(lngRowCounter) = True
                Exit Sub
            End If
        Next lngRowCounter
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "End")
    
    Exit Sub
    
HighlightListItem_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error Number - " & Err.Number & " Description - " & Err.Description)

End Sub

' Switches which hierarchy tree is shown based upon the Enquiry type.
Private Sub SwitchHierarchyTree()

Const PROCEDURE_NAME As String = "SwitchHierarchyTree"

    If (fraHierarchyLoading.Visible = True) Then
        If (metEnquiryType = etFullItems) Then
            metEnquiryType = etPIMOnly
        ElseIf (metEnquiryType = etPIMOnly) Then
            metEnquiryType = etFullItems
        End If
    End If
        
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIM")
        tvwHierarchyPIM.Visible = True
        tvwHierarchyFull.Visible = False
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "FULL")
        tvwHierarchyFull.Visible = True
        tvwHierarchyPIM.Visible = False
    End If
    
End Sub

' Update the status bar with information on what keys can be pressed relevant to to which control has the focus.
Private Sub UpdateStatus(strStatusText As String)

    sbStatus.Panels(PANEL_INFO).Text = strStatusText

End Sub

'   Connect to the remote Winsock server.
Private Sub SetupWinsock()

Const PROCEDURE_NAME As String = "SetupWinsock"
    
    On Error GoTo SetupWinsock_Error

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Connect To TCP/IP")
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Local Port - " & mlngWinsockItemFilterPort)
    
    With wsckComms(0)
        If (.State <> 0) Then
            .Close
        End If
        .LocalPort = mlngWinsockItemFilterPort
        .Listen
        Call Sleep(SLEEP_TIME)
        DoEvents
    End With
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "TCP/IP Connection Status - " & wsckComms(0).State)
    
    Exit Sub
    
SetupWinsock_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Connect To TCP/IP failed, " & _
            "Error - " & Err.Number & " " & Err.Description)
    Call MsgBoxEx("Connect To TCP/IP failed, " & _
            "Error - " & Err.Number & " " & Err.Description, vbCritical, "Connect To TCP/IP Error", _
            , , , , RGBMsgBox_WarnColour)
    
End Sub

'   Close the Winsock connection.
Private Sub CloseConnection()

Dim intCounter As Integer

    '   Close the winsock connection.
    If (wsckComms.Count <> 1) Then
        For intCounter = 1 To wsckComms.Count - 1
            If wsckComms(intCounter).State <> 0 Then
                Call wsckComms(intCounter).Close
            End If
        Next intCounter
    End If
    
End Sub

'   The Winsock close event.
Private Sub wsckComms_Close(Index As Integer)

    Call DebugMsg(MODULE_NAME, "wsckComms_Close", endlDebug, _
            "wsckComms Close. Connection Status - " & Me.wsckComms(Index).State)
    Unload wsckComms(Index)
    mintWinsockControls = mintWinsockControls - 1
    mblnTCPIPOpen = False

End Sub

'   The Winsock connect event, used to flag that the connection is open and ready to use.
Private Sub wsckComms_Connect(Index As Integer)

    Call DebugMsg(MODULE_NAME, "wsckComms_Connect", endlDebug, _
            "wsckComms Connect. Connection Status - " & Me.wsckComms(Index).State)
    If (wsckComms(Index).State = sckConnected) Then
        mblnTCPIPOpen = True
    Else
        mblnTCPIPOpen = False
    End If

End Sub

Private Sub wsckComms_ConnectionRequest(Index As Integer, ByVal requestID As Long)

    Call DebugMsg(MODULE_NAME, "wsckComms_ConnectionRequest", endlDebug, _
            "Request ID - " & requestID)
    mintWinsockControls = mintWinsockControls + 1
    Load wsckComms(mintWinsockControls)
    wsckComms(mintWinsockControls).LocalPort = mlngWinsockItemFilterPort
    wsckComms(mintWinsockControls).Accept requestID
    Call DebugMsg(MODULE_NAME, "wsckComms_ConnectionRequest", endlDebug, _
            "wsckComms_ConnectionRequest. Connection Status - " & Me.wsckComms(Index).State)
    If (wsckComms(Index).State = sckConnected) Then
        mblnTCPIPOpen = True
    Else
        mblnTCPIPOpen = False
    End If
    
End Sub

Private Sub wsckComms_DataArrival(Index As Integer, ByVal bytesTotal As Long)

    Dim strReceivedData As String
    
    Call DebugMsg(MODULE_NAME, "wsckComms_DataArrival", endlDebug, _
            "Bytes Total - " & bytesTotal)
            
    If (mblnIsFirstWinsockData = True) Then
        Call DebugMsg(MODULE_NAME, "wsckComms_DataArrival", endlDebug, "New Message")
        mstrWinsockMessage = ""
        mblnIsFirstWinsockData = False
    End If
    
    wsckComms(Index).GetData strReceivedData, vbString
    Call DebugMsg(MODULE_NAME, "wsckComms_DataArrival", endlDebug, _
            "Message Received - " & strReceivedData)
    
    If (Right(strReceivedData, 1) = WINSOCK_MESSAGE_TERMINATOR) Then
        mblnIsFirstWinsockData = True
        mstrWinsockMessage = mstrWinsockMessage & Left(strReceivedData, Len(strReceivedData) - 1)
        Call ProcessWinsockMessage(mstrWinsockMessage)
    Else
        mstrWinsockMessage = mstrWinsockMessage & strReceivedData
    End If
               
End Sub

'   The Winsock error event.
Private Sub wsckComms_Error(Index As Integer, ByVal Number As Integer, Description As String, _
        ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, _
        CancelDisplay As Boolean)
    
    Call DebugMsg(MODULE_NAME, "wsckComms_Error", endlDebug, _
            "Error No - " & Number & " - Desc - " & Description)
    mblnTCPIPOpen = False
    Call HideForm

End Sub

' Processes the received winsock message.
Private Sub ProcessWinsockMessage(strMessage As String)

    Dim strMessageType As String
    
    Call DebugMsg(MODULE_NAME, "ProcessWinsockMessage", endlDebug, "Message - " & strMessage)
    
    If (InStr(strMessage, "|") > 0) Then
        strMessageType = Left(strMessage, InStr(strMessage, "|"))
    Else
        strMessageType = strMessage
    End If
    
    ' Process the message.
    Select Case strMessageType
        Case PIM_ITEM_FILTER_IS_RECEIVING
            Call SendWinsockMessage(mintWinsockControls, PIM_ITEM_FILTER_RECEIVING)
        Case PIM_ITEM_FILTER_HIDE
            Call HideForm
        Case PIM_ITEM_FILTER_SHOW
            Call ShowForm
        Case PIM_ITEM_FILTER_RESET
            Call cmdReset_Click
        Case PIM_ITEM_FILTER_LOOKUP_SKU
            ' Retrieve the passed EAN value from the message.
            Call LookupSKU(Right(strMessage, Len(strMessage) - InStr(strMessage, "|")))
        Case PIM_ITEM_FILTER_SHOW_SUPPLIER_FALSE
            Call ShowSupplier(False)
        Case PIM_ITEM_FILTER_SHOW_SUPPLIER_TRUE
            Call ShowSupplier(True)
        Case PIM_ITEM_FILTER_START_MODE_PIM
            metEnquiryType = etPIMOnly
        Case PIM_ITEM_FILTER_START_MODE_FULL
            metEnquiryType = etFullItems
        Case PIM_ITEM_FILTER_START_MODE_VIEW_ONLY
            mblnViewOnly = True
        Case PIM_ITEM_FILTER_USE_LIST_FALSE
            mblnShowUseList = False
        Case PIM_ITEM_FILTER_USE_LIST_TRUE
            'mblnShowUseList = True
        Case PIM_ITEM_FILTER_SHOW_OBSOLETE_TRUE
            mblnShowObsoleteButton = True
        Case PIM_ITEM_FILTER_SHOW_OBSOLETE_FALSE
            mblnShowObsoleteButton = False
        Case PIM_ITEM_FILTER_SHOW_NON_STOCK_TRUE
            mblnShowNonStockButton = True
        Case PIM_ITEM_FILTER_SHOW_NON_STOCK_FALSE
            mblnShowNonStockButton = False
        Case PIM_ITEM_FILTER_SUPPLIER_CODE
            ' Retrieve the passed Supplier Code value from the message.
            Call SetupSupplier(Right(strMessage, Len(strMessage) - InStr(strMessage, "|")))
    End Select

End Sub

'   Send the message through the Winsock control.
Private Function SendWinsockMessage(Index As Integer, strMessage) As Boolean

    Call DebugMsg(MODULE_NAME, "SendWinsockMessage", endlDebug, _
            "Winsock Message - " & strMessage)
    
    If (wsckComms(Index).State = sckConnected) Then
        Call wsckComms(Index).SendData(strMessage & WINSOCK_MESSAGE_TERMINATOR)
        Call Sleep(SLEEP_TIME)
        DoEvents
    Else
        Call HideForm
    End If
    
End Function

' Show the form.
Private Sub ShowForm()
 
Const PROCEDURE_NAME As String = "ShowForm"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    mblnShowItemFilter = True
    
    ' Set-up the form.
    Call ResizeColumns
    lstMatches.Clear
    Call cmdReset_Click
    sprdThumbnails.MaxRows = 0
    
    If (metEnquiryType = etPIMOnly) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Enquiry Type - PIM")
        lstMatches.Visible = False
        sprdThumbnails.Visible = True
        lblImagePagesTitle.Visible = True
        lblImagePages.Visible = True
        cmdViewType.Caption = "F2 - &View Text"
        tvwHierarchyPIM.Visible = True
        tvwHierarchyFull.Visible = False
    Else
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Enquiry Type - Full")
        lstMatches.Visible = True
        sprdThumbnails.Visible = False
        lblImagePagesTitle.Visible = False
        lblImagePages.Visible = False
        cmdViewType.Caption = "F2 - &View Images"
        tvwHierarchyPIM.Visible = False
        tvwHierarchyFull.Visible = True
    End If
    chkActiveOnly.Visible = mblnShowObsoleteButton
    chkIncludeNonStock.Visible = mblnShowNonStockButton

    ' Make the Item Filter visible.
    Me.Visible = False
    Me.WindowState = vbNormal
    Me.Visible = True
    Me.Hide
    Me.Show
    SetWindowPos Me.hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOACTIVATE Or _
                    SWP_SHOWWINDOW Or SWP_NOMOVE Or SWP_NOSIZE
    Me.SetFocus
    
''    If (metEnquiryType = etFullItems) And (txtDescription.Visible = True) Then
''        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Focus Description")
''        txtDescription.SetFocus
''    ElseIf (metEnquiryType = etPIMOnly) And (tvwHierarchyPIM.Visible = True) Then
''        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Focus PIM Hierarchy")
''        tvwHierarchyPIM.SetFocus
''    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Tree View PIM Visible - " & tvwHierarchyPIM.Visible)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Tree View Full Visible - " & tvwHierarchyFull.Visible)

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, Me.WindowState)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, Me.Visible)
    
End Sub

' Hides and resets the form.
Private Sub HideForm()
 
Const PROCEDURE_NAME As String = "HideForm"

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")

    Call cmdReset_Click
    
    mblnShowItemFilter = False
    mblnViewOnly = False
    cboSupplier.Visible = True
    lblSupplier.Visible = True
    m_ShowSupplier = True
    mblnShowObsoleteButton = True
    mblnShowNonStockButton = True
    
    ' Close the Winsock connectiion.
    Call CloseConnection
    
    Me.Visible = False
    
End Sub

' Refresh the data.
Private Sub RefreshTheData()

Const PROCEDURE_NAME As String = "RefreshTheData"

    On Error GoTo RefreshTheData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' Build the Hierarchy trees for the PIM and Full product hierarchies.
    fraHierarchyLoading.Visible = True
    metLoadTreeEnquiryType = etPIMOnly
    tmrLoadHierarchy.Enabled = True
    
    mblnDataRefreshed = True
    
    Exit Sub
    
RefreshTheData_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Refreshing The Data failed, " & _
            "Error - " & Err.Number & " " & Err.Description)
    Call MsgBoxEx("Refreshing The Data failed, " & _
            "Error - " & Err.Number & " " & Err.Description, vbCritical, "Refresh The Data Error", _
            , , , , RGBMsgBox_WarnColour)

End Sub
