Attribute VB_Name = "modItemFilter"
'<CAMH>****************************************************************************************
'*  Module : modItemFilter
'*  Date   : 30/06/06
'*  Author : DaveF
' $Archive: /Projects/OasysHW/VB/Item Filter UC/modItemFilter.bas $
'*
'**********************************************************************************************
'*  Application Summary
'*  ===================
'*  Provides global variables.
'*
'**********************************************************************************************
'* $ Author: $ $ Date: $ $ Revision: $
'*
'*  Date        Author      Vers.   Comments
'*  ========    ========    =====   =================================
'*  30/06/06    DaveF       1.0.0   Initial version.
'*
'</CAMH>***************************************************************************************

Option Explicit

Public Const SWP_NOMOVE = 2
Public Const SWP_NOSIZE = 1
Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const HWND_TOP = 0

Public Declare Sub SetWindowPos Lib "user32" _
                        (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, _
                            ByVal X As Long, ByVal Y As Long, _
                            ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
                            
Public Const LARGE_IMAGE_FILE_EXTENSION As String = "L.jpg"



