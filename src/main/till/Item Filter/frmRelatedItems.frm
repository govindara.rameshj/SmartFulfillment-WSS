VERSION 5.00
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Begin VB.Form frmRelatedItems 
   BorderStyle     =   0  'None
   Caption         =   "Related Items"
   ClientHeight    =   3405
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7215
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   7215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin LpADOLib.fpListAdo lstRelatedItems 
      Height          =   2160
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   6975
      _Version        =   196608
      _ExtentX        =   12303
      _ExtentY        =   3810
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Columns         =   5
      Sorted          =   0
      LineWidth       =   1
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   -1
      ColumnWidthScale=   2
      RowHeight       =   -1
      MultiSelect     =   0
      WrapList        =   0   'False
      WrapWidth       =   0
      SelMax          =   -1
      AutoSearch      =   1
      SearchMethod    =   0
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   0
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   0
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   1
      BorderColor     =   -2147483642
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   1
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   0
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   0   'False
      VScrollSpecialType=   0
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   0
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   1
      VirtualPageSize =   150
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   -1  'True
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      DataField       =   ""
      DataMember      =   ""
      OLEDragMode     =   0
      OLEDropMode     =   0
      EnableClickEvent=   -1  'True
      Redraw          =   -1  'True
      ResizeRowToFont =   0   'False
      TextTipMultiLine=   0
      ColDesigner     =   "frmRelatedItems.frx":0000
   End
   Begin VB.PictureBox picBorder 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3400
      Left            =   0
      ScaleHeight     =   3375
      ScaleWidth      =   7185
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      Begin VB.CommandButton cmdClose 
         Caption         =   "F12-Close"
         Height          =   375
         Left            =   6000
         TabIndex        =   2
         Top             =   2880
         Width           =   1095
      End
      Begin VB.CommandButton cmdUse 
         Caption         =   "F5-Use"
         Height          =   375
         Left            =   4800
         TabIndex        =   1
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label lblTitle 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Related Single/Bulk Item Detail"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   0
         TabIndex        =   3
         Top             =   180
         Width           =   7215
      End
   End
End
Attribute VB_Name = "frmRelatedItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const SINGLE_SPACE As String = " "
Private mstrSkuNumber As String
Private mstrItemType As String

Public Property Let ItemType(ByVal strType As String)
    mstrItemType = strType

End Property

Public Property Let SkuNumber(ByVal strSkun As String)
    mstrSkuNumber = strSkun
    
End Property

Public Property Get SkuNumber() As String
    SkuNumber = mstrSkuNumber

End Property

Public Property Let ViewOnly(ByVal Value As Boolean)

    cmdUse.Visible = Not Value

End Property


Private Sub cmdClose_Click()

    mstrSkuNumber = vbNullString
    mstrItemType = vbNullString
    Hide

End Sub

Private Sub cmdUse_Click()

    lstRelatedItems.Row = lstRelatedItems.ListIndex
    lstRelatedItems.Col = 0
    mstrSkuNumber = lstRelatedItems.ColText
    Hide

End Sub

Private Sub Form_Activate()

    If (lstRelatedItems.ListCount > 0) Then
        lstRelatedItems.SetFocus
    Else
        cmdClose.Value = True
    End If
    
End Sub

Private Sub Form_Initialize()

    mstrSkuNumber = vbNullString
    mstrItemType = vbNullString
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (KeyCode)
        Case vbKeyF12
            KeyCode = 0
            cmdClose.Value = True
            
        Case vbKeyF5
            KeyCode = 0
            If (cmdUse.Visible = True) Then cmdUse.Value = True
                
    End Select

End Sub

Private Sub Form_Load()

    picBorder.BackColor = RGBQuery_BackColour
    BackColor = RGBQuery_BackColour
    
    If (mstrItemType = "S") Then
        lblTitle.Caption = "Related Bulk Item Data for " & mstrSkuNumber
    ElseIf (mstrItemType = "B") Then
        lblTitle.Caption = "Related Singles Item Data for " & mstrSkuNumber
    Else
        lblTitle.Caption = "Related Item Data for " & mstrSkuNumber
    End If
    
    lstRelatedItems.BorderColor = RGBQuery_BackColour
    lstRelatedItems.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstRelatedItems.ListApplyTo = ListApplyToEvenRows
    lstRelatedItems.BackColor = lstRelatedItems.BackColor
    lstRelatedItems.ListApplyTo = ListApplyToOddRows
    lstRelatedItems.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstRelatedItems.Row = -1
    lstRelatedItems.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)
    
    If GetRelatedData Then
        Call FormatColumnData
        lstRelatedItems.Selected(0) = True
        lstRelatedItems.ReDraw = True
        DoEvents
        
    Else
        Call MsgBoxEx("This item has no related information.", vbExclamation, "Related Item Data", , , , , RGBMSGBox_PromptColour)
        
    End If
    
End Sub

Private Sub FormatColumnData()
    
    lstRelatedItems.Row = -1
    lstRelatedItems.ListApplyTo = ListApplyToIndividual
    lstRelatedItems.Col = 0
    lstRelatedItems.ColHeaderText = "SKU"
    lstRelatedItems.AlignH = AlignHCenter
    lstRelatedItems.Col = 1
    lstRelatedItems.ColHeaderText = "Description"
    lstRelatedItems.AlignH = AlignHLeft
    
    If (mstrItemType = "B") Then
        lstRelatedItems.ColWidth = 48
    End If
    
    lstRelatedItems.Col = 2
    lstRelatedItems.ColHeaderText = "Price"
    lstRelatedItems.AlignH = AlignHRight
    lstRelatedItems.Col = 3
    lstRelatedItems.ColHeaderText = "Qty. On Hand"
    lstRelatedItems.AlignH = AlignHRight
    lstRelatedItems.Col = 4
    lstRelatedItems.ColHeaderText = "No. Per Pack"
    lstRelatedItems.AlignH = AlignHRight
    
    If (mstrItemType = "B") Then
        lstRelatedItems.ColWidth = 0
        lstRelatedItems.ColHide = True
        
    End If
    
End Sub

Private Function GetRelatedData() As Boolean

Dim strQuery As String
Dim strRow As String
Dim rsData As Recordset
Dim oConn As Connection

    On Error GoTo ErrorHandler
    
    GetRelatedData = False
    lstRelatedItems.Clear
    Screen.MousePointer = vbHourglass
    
    If (mstrItemType = "S") Then
        strQuery = "SELECT relitm.ppos, relitm.nspp, stkmas.pric, stkmas.descr, stkmas.onha FROM relitm INNER JOIN stkmas ON relitm.ppos = stkmas.skun WHERE relitm.spos = '" & mstrSkuNumber & "'"
    ElseIf (mstrItemType = "B") Then
        strQuery = "SELECT relitm.spos, stkmas.descr, stkmas.pric, stkmas.onha FROM stkmas INNER JOIN relitm ON relitm.spos = stkmas.skun WHERE relitm.ppos = '" & mstrSkuNumber & "'"
    Else
        Exit Function
    End If
               
    Set oConn = New Connection
    oConn.ConnectionString = goDatabase.ConnectionString
    oConn.CursorLocation = adUseClient
    oConn.Open
    
    Set rsData = oConn.Execute(strQuery)
    
    If (rsData.RecordCount > 0) Then
        While (Not rsData.EOF)
            If (mstrItemType = "S") Then
                strRow = rsData!ppos & vbTab
            Else
                strRow = rsData!spos & vbTab
            End If
            
            strRow = strRow & SINGLE_SPACE & Trim(rsData!descr) & vbTab & Format(rsData!pric, " 0.00 ") & vbTab & Format(rsData!onha, " 0 ")
            
            If (mstrItemType = "S") Then
                strRow = strRow & vbTab & Format(rsData!nspp, " 0 ")
            End If
                
            lstRelatedItems.AddItem strRow
            rsData.MoveNext
        
        Wend
        GetRelatedData = True
    
    End If
    
    oConn.Close
    Set rsData = Nothing
    Set oConn = Nothing
    Screen.MousePointer = vbDefault
    
    Exit Function
    
ErrorHandler:

    If (oConn.State = adStateOpen) Then oConn.Close
    If (Not rsData Is Nothing) Then Set rsData = Nothing
    If (Not oConn Is Nothing) Then Set oConn = Nothing
    
    Screen.MousePointer = vbDefault
    Call MsgBoxEx("An error occured while searching related items.", vbCritical + vbOKOnly, "Error", , , , , RGBMsgBox_WarnColour)

End Function

