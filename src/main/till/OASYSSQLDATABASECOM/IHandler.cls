VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysSqlDatabaseCom/IHandler.cls $
' $Revision: 16 $
' $Date: 24/03/03 9:55 $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------

Public Function Initialise(oTable As CTable, oConnection As Connection, _
                                    Optional SortKeys As CBoSortKeys) As Boolean
End Function
Public Function CreatePrimaryView() As CView
End Function
Public Function CreateSelectSQL(Optional RowSelector As CRowSelector) As String
End Function
Public Function CreateLiteralView(DataFormat As Long, _
                                  sLineSeparator As String, _
                                  sDataSeparator As String, _
                                  sTextDelim As String) As String
End Function
Public Function AddSelector(oFSelector As CFieldSelector) As Boolean
End Function
Public Function BuildRow(oRow As CRow) As Boolean
' BuildRow() for secondary properties after we have used CreatePrimaryView() on oRecordset
End Function
Public Property Get TableId() As Integer
End Property
Public Function SaveFromRow(eSave As enSaveType, oRow As CRow, ByRef RowOfChanges As CRow) As enDbErrorType
End Function
Public Function GetAggregateValue(ByVal nFunctionId As Integer, oSourceField As IField) As Variant
End Function
Public Function DoBulkUpdate(oTarget As Object, oSource As Object) As Boolean
End Function
Public Function DeleteBoCollection() As enDbErrorType
End Function
