VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCidLookup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysSqlDatabaseCom/CCidLookup.cls $
' $Revision: 5 $
' $Date: 4/12/03 12:37 $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Const MODULE_NAME As String = "CCidLookUp"

Private m_arrFidToCid()     As Long
Private m_nClassId          As Integer
Private m_nPrimaryTableId   As Integer

Public Function Initialise(nMaxFid As Long, nPrimaryTableId As Integer) As Boolean
    
Dim intFieldNo As Integer
    
    m_nClassId = nMaxFid \ &H10000
    ReDim m_arrFidToCid(nMaxFid And &H7FFF)
    m_nPrimaryTableId = nPrimaryTableId
    ' By default, the short FIDs are the same as the short CIDs on the primary table
    For intFieldNo = 0 To UBound(m_arrFidToCid)
        m_arrFidToCid(intFieldNo) = (m_nPrimaryTableId * &H10000) + intFieldNo
    Next intFieldNo
End Function
Public Property Get Cid(nFid As Long) As Long
    On Error GoTo Cid_Error
    ' Simply use the short (low integer) FID as the subscript
    If (nFid And &H7FFF) > UBound(m_arrFidToCid) Then
        Call DebugMsg(MODULE_NAME, "CID", endlDebug, "ERROR-Field ID exceeds end of static FID=" & (nFid And &H7FFF) & ",EOS=" & UBound(m_arrFidToCid))
        Debug.Assert False 'Check that field ID passed in is below the END_OF_STATIC
    Else
        Cid = m_arrFidToCid(nFid And &H7FFF)
    End If
    Exit Property
Cid_Error:
    Debug.Assert False
    Cid = 0
End Property
