VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTableHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : CTableHandler
'* Date   : 27/09/02
'* Author : martynw
'*$Archive: /Projects/OasysV2/VB/OasysSqlDatabaseCom/CTableHandler.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 23/05/03 16:33 $
'* $Revision: 52 $
'* Versions:
'* 27/09/02    martynw
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit
#Const ccValidateSqlResults = 0

Const MODULE_NAME As String = "CTableHandler"
Implements IHandler

Private m_oTable        As CTable
Private m_collFSelector As Collection
Private m_oSortKeys     As IBoSortKeys
Private m_oDatabase     As IBoDatabase

Private Function GetSqlAggregateString(ByVal lAggregateFunction As Integer, oSourceField As IField) As String
    ' Convert the collection of Field Selectors into a SQL string
Dim nColNo          As Integer
Dim oField          As IField
Dim strSql          As String
Dim strWhere        As String
Dim nComparator     As Long
    
    ' first build up the select statement with aggregate function
    strSql = "SELECT "
    Select Case (lAggregateFunction)
        Case (AGG_MAX):   strSql = strSql & "MAX("
        Case (AGG_MIN):   strSql = strSql & "MIN("
        Case (AGG_COUNT): strSql = strSql & "COUNT("
        Case (AGG_AVG):   strSql = strSql & "AVG("
        Case (AGG_SUM):   strSql = strSql & "SUM("
        Case (AGG_DIST):  strSql = strSql & "DISTINCT("
        Case Else:        GetSqlAggregateString = vbNullString
                          Exit Function
    End Select
    strSql = strSql & m_oTable.SqlColumnName(oSourceField) & ")"
    
    'add the FROM and any WHERE clause
    strSql = strSql & " FROM " & m_oTable.TableName & GetSqlWhereString
    
    Call DebugMsg(MODULE_NAME, "GETSQLAggrString", endlDebug, strSql)
    
    GetSqlAggregateString = strSql
    
End Function

Private Function GetSqlSelectString(Optional oRow As IRowSelector) As String
    ' Convert the collection of Field Selectors into a SQL string
    
Dim i           As Integer
Dim oField      As IField
Dim strSql      As String
Dim strGroupBy  As String
    
    If oRow Is Nothing Then
        ' first build up the select statement
        For i = 1 To m_collFSelector.Count
            Set oField = m_collFSelector.Item(i).Field
            If i = 1 Then
                strSql = "SELECT " & m_oTable.SqlColumnName(oField)
            Else
                If InStr(strSql, m_oTable.SqlColumnName(oField)) = 0 Then strSql = strSql & "," & m_oTable.SqlColumnName(oField)
            End If
            If m_oTable.IsDistinct(oField.Id) Then
                strGroupBy = " GROUP BY """ & m_oTable.SimpleColumnName(oField) & """"
            End If
        Next i
    Else
        ' first build up the select statement
        For i = 1 To oRow.Count
            Set oField = oRow.FieldSelector(i).Field
            If i = 1 Then
                strSql = "SELECT " & m_oTable.SqlColumnName(oField)
            Else
                If InStr(strSql, m_oTable.SqlColumnName(oField)) = 0 Then strSql = strSql & "," & m_oTable.SqlColumnName(oField)
            End If
            If m_oTable.IsDistinct(oField.Id) Then
                strGroupBy = " GROUP BY """ & m_oTable.SimpleColumnName(oField) & """"
            End If
        Next i
    End If
    If LenB(strSql) = 0 Then
        Debug.Assert False
        Exit Function
    End If
    
    If Not (m_oSortKeys Is Nothing) Then
        ' Create our own Field object
        Set oField = oField.Duplicate
        For i = 1 To m_oSortKeys.Count
            If i = 1 Then
                strGroupBy = " ORDER BY "
            Else
                strGroupBy = strGroupBy & ", "
            End If
            oField.Id = m_oSortKeys.FieldID(i)
            strGroupBy = strGroupBy & """" & m_oTable.SimpleColumnName(oField) & """"
            If m_oSortKeys.IsDescending(i) Then strGroupBy = strGroupBy & " DESC"
        Next i
    End If
    
    ' second add the FROM and WHERE and GROUP BY clauses
    GetSqlSelectString = strSql & " FROM " & m_oTable.TableName & _
                    GetSqlWhereString & strGroupBy
    Call DebugMsg(MODULE_NAME, "GETSQLSelect", endlDebug, GetSqlSelectString)
    
End Function

Private Function GetSqlSelectAllString() As String
' Convert the collection of Field Selectors into a SQL string
    
Dim strSql      As String
    
    ' first build up the select statement
    strSql = "SELECT * FROM " & m_oTable.TableName
    
    If LenB(strSql) = 0 Then
        Debug.Assert False
        Exit Function
    End If
    
    ' second add the WHERE clauses
    GetSqlSelectAllString = strSql & GetSqlWhereString
    Call DebugMsg(MODULE_NAME, "GETSQLSelectAll", endlDebug, GetSqlSelectAllString)
    
End Function

Private Function GetSqlInsertString(oRow As IRow, ByRef strSelectStr As String) As String
    ' Convert the collection of Fields into a SQL string
Dim i           As Integer
Dim strSqlCols  As String
Dim strSqlVals  As String
Dim oField      As IField
Dim sColName    As String
Dim bIsStarted  As Boolean
    
    strSelectStr = vbNullString 'reset any SQL return string that may be required
    ' first, build up the columns and values clauses
    strSqlVals = ") VALUES ("
    For i = 1 To oRow.Count
        Set oField = oRow.Field(i)
        sColName = m_oTable.SqlColumnName(oField)
        If m_oTable.IsAutoIncrement(oField.Id) Then
            ' Ensure we return the AutoIncrement value.  Note: this cannot be an occurs
            ' The name encodes the field index within the row
            strSelectStr = strSelectStr & " MAX(" & sColName & ") AS Field" & i & ","
        Else
            If bIsStarted Then
                strSqlCols = strSqlCols & ", "
                strSqlVals = strSqlVals & ", "
            Else
                bIsStarted = True
            End If
            strSqlCols = strSqlCols & sColName
            strSqlVals = strSqlVals & oField.SqlLiteral
        End If
    Next i
    If LenB(strSqlCols) = 0 Then
        Debug.Assert False      ' Wheres our SQL?
        Exit Function
    End If
    
    ' second, join all the clauses to give the returned SQL string
    GetSqlInsertString = "INSERT INTO " & m_oTable.TableName & " (" & strSqlCols & strSqlVals & ")"
    
    ' third, set up the o/p paramter
    If LenB(strSelectStr) <> 0 Then
        'chop off last ","
        strSelectStr = Left$(strSelectStr, Len(strSelectStr) - 1)
        strSelectStr = "SELECT " & strSelectStr & " FROM " & m_oTable.TableName
    End If
    
    Call DebugMsg(MODULE_NAME, "GETSQLInsert", endlDebug, GetSqlInsertString)
    If LenB(strSelectStr) <> 0 Then Call DebugMsg(MODULE_NAME, "GETSQLInsert-GetID", endlDebug, strSelectStr)
    
End Function

Private Function GetSqlUpdateString(oRow As IRow) As String
    ' Convert the collection of Fields into a SQL string
Dim i          As Integer
Dim j          As Integer
Dim nKeyCount  As Integer
Dim oField     As IField
Dim oSubRow    As IRow
Dim strSqlCols As String
Dim oTempGroup As CFieldGroup
    
    ' First check we have all the keys at the front of the row
    nKeyCount = m_oTable.KeyCount           ' How many keys have we got for this table?
    If nKeyCount = 0 Then nKeyCount = 1     ' None set up but we assume there must be one
    
    For i = 1 To nKeyCount
        Set oField = oRow.Field(i)
        ' First fields must be the key fields, all of them!
        If (oField.Id And &H7FFF) > nKeyCount Then
            ' We don't have all the necessary key values
            Debug.Assert False
        End If
    Next i
    
    ' The second onwards are the data values
    For i = (nKeyCount + 1) To oRow.Count
        Set oField = oRow.Field(i)
        If LenB(strSqlCols) <> 0 Then
            strSqlCols = strSqlCols & ", "
        End If
        If m_oTable.IsSimpleField(oField.Id) Then
            strSqlCols = strSqlCols & m_oTable.SqlColumnName(oField) & _
                         " = " & m_oTable.SqlLiteral(oField)
'                         " = " & oField.SqlLiteral
        Else
            ' Special processing needed for Group field, ie. occurs.
            Set oTempGroup = oField.Interface
            Set oSubRow = oTempGroup.GroupRow
            j = oTempGroup.Subscript
            If j > 0 Then
                ' We pick just one item from the array
                strSqlCols = strSqlCols & m_oTable.SqlColumnName(oField) & _
                             " = " & oField.SqlLiteral
            Else
                ' We handle the whole array
                Dim strNames() As String
                Dim strValues() As String
                strNames = Split(m_oTable.SqlColumnName(oField), ", ")
                strValues = Split(oField.SqlLiteral, ", ")
                For j = 0 To UBound(strNames)
                    If j > 0 Then
                        ' Don't need comma before first (already done), only subsequently.
                        strSqlCols = strSqlCols & ", "
                    End If
                    strSqlCols = strSqlCols & strNames(j) & _
                                 " = " & strValues(j)
                Next j
            End If
        End If
    Next i
    If LenB(strSqlCols) = 0 Then
        Debug.Assert False
        Exit Function
    End If
    
    ' second join all the clauses
    GetSqlUpdateString = "UPDATE " & m_oTable.TableName & " SET " & strSqlCols & GetSqlWhereString
    Call DebugMsg(MODULE_NAME, "GETSQLUpdate", endlDebug, GetSqlUpdateString)
    
End Function

Private Function GetSqlDeleteString() As String
    ' Convert the collection of Fields into a SQL 'WHERE' string
Dim strWhere    As String
    
    ' first get the WHERE string, it must exist!
    strWhere = GetSqlWhereString
    If LenB(strWhere) = 0 Then
        ' We must have a 'where' when we are deleting!
        Debug.Assert False
        Exit Function
    End If
    
    ' second join all the clauses
    GetSqlDeleteString = "DELETE FROM " & m_oTable.TableName & GetSqlWhereString
    Call DebugMsg(MODULE_NAME, "GETSQLDelete", endlDebug, GetSqlDeleteString)
    
End Function

Private Function GetSqlBulkUpdateString(oTarget As Object, oSource As Object) As String
Dim strSqlTargetCol As String
Dim strSqlSource    As String
Dim strSqlSet       As String
Dim oTargetField    As IField
Dim oSourceField    As IField
Dim oTargetRow      As IRow
Dim oSourceRow      As IRow
Dim i               As Integer
Dim arrHandlers()   As IHandler
    
    ReDim arrHandlers(0)
    
    On Error Resume Next
    Set oTargetField = oTarget
    Set oSourceField = oSource
    On Error GoTo 0
    
    If Not (oTargetField Is Nothing) Then
        ' We have been passed IFields
        ' first build up the columns and values clauses
        Debug.Assert Not (oSourceField Is Nothing)
        strSqlSet = GetSqlBulkUpdateSetString(oTargetField, oSourceField)
    Else
        Set oTargetRow = oTarget
        Set oSourceRow = oSource
        ' We have been passed CRows
        ' Number of source and targets should match
        Debug.Assert (oTargetRow.Count = oSourceRow.Count)
        For i = 1 To oTargetRow.Count
            If i > 1 Then strSqlSet = strSqlSet & ", "
            strSqlSet = strSqlSet & GetSqlBulkUpdateSetString(oTargetRow.Field(i), oSourceRow.Field(i))
        Next i
    End If
    
    ' second join all the clauses
    GetSqlBulkUpdateString = "UPDATE " & m_oTable.TableName & " SET " & _
                strSqlSet & GetSqlWhereString
    Call DebugMsg(MODULE_NAME, "GETSQLBulkUpdateString", endlDebug, GetSqlBulkUpdateString)

End Function
Private Function GetSqlBulkUpdateSetString(oTargetField As IField, oSourceField As IField) As String
    ' This method is a helper for GetSqlBulkUpdateString() and sets the SQL for
    ' the clause that sets one column to one value
    GetSqlBulkUpdateSetString = m_oTable.SqlColumnName(oTargetField) & " = "
    If oTargetField.Id = oSourceField.Id Then
        ' If the FIDs are the same then we use the source value
        GetSqlBulkUpdateSetString = GetSqlBulkUpdateSetString & oSourceField.SqlLiteral
    Else
        ' If the FIDs are different then we move column to column
        GetSqlBulkUpdateSetString = GetSqlBulkUpdateSetString & m_oTable.SqlColumnName(oSourceField)
    End If
End Function

Private Function GetSqlWhereString() As String
    ' Convert the collection of Field selectors into a SQL string
    Dim i, j            As Integer
    Dim iWhere          As Integer
    Dim oField          As IField
    Dim nComparator     As Long
    Dim strComparator   As String
    Dim oFSelector      As IFieldSelector
    Dim sColName        As String
    Dim strWhere        As String
    Dim strInList       As String
    Dim strInColName    As String
    
    iWhere = 6
    strInList = vbNullString
    For i = 1 To m_collFSelector.Count
        ' for each selector
        Set oFSelector = m_collFSelector.Item(i)
        If oFSelector.Comparator <> CMP_SELECTALL Then
            ' SelectAll is implicit, so we only need handle these other comparators
            Set oField = oFSelector.Field
            sColName = m_oTable.SqlColumnName(oField)
            nComparator = oFSelector.Comparator
            If oField.SqlLiteral <> "NULL" Then
                Select Case nComparator
                    Case (CMP_EQUAL):            strComparator = " = "
                    Case (CMP_NOTEQUAL):         strComparator = " <> "
                    Case (CMP_LESSTHAN):         strComparator = " < "
                    Case (CMP_GREATERTHAN):      strComparator = " > "
                    Case (CMP_LESSEQUALTHAN):    strComparator = " <= "
                    Case (CMP_GREATEREQUALTHAN): strComparator = " >= "
                    Case (CMP_SELECTLIST):       strComparator = " IN ("
                    Case (CMP_LIKE):             strComparator = " like "
                    Case (CMP_INRANGE):          strComparator = " "
                    Case Else:                   strComparator = vbNullString
                End Select
            Else
                ' NULL value has special SQL syntax
                Select Case nComparator
                    Case (CMP_EQUAL):       strComparator = " IS "
                    Case (CMP_NOTEQUAL):    strComparator = " IS NOT "
                    Case Else:              strComparator = vbNullString
                End Select
            End If
            If LenB(strComparator) <> 0 Then
                If nComparator <> CMP_SELECTLIST Then
                    If LenB(strWhere) = 0 Then
                        strWhere = " WHERE "
                    Else
                        strWhere = strWhere & " AND "
                    End If
                End If
                'put it all together
                Select Case nComparator
                    Case (CMP_SELECTLIST):
                        For j = 1 To oFSelector.FieldCount
                            If LenB(strInList) <> 0 Then strInList = strInList & ", "
                            Set oField = oFSelector.Field(j)
                            strInList = strInList & oField.SqlLiteral
                        Next j
                        strInColName = sColName
                        'strWhere = strWhere & sColName & strComparator & strInList & ")"
                    Case (CMP_LIKE):
                        strWhere = strWhere & "UPPER(" & sColName & ")" & strComparator & UCase$(oField.SqlLiteral)
                    Case (CMP_INRANGE):
                        Debug.Assert (oFSelector.FieldCount = 2)
                        strWhere = strWhere & "UPPER(" & sColName & ") >= " & UCase$(oField.SqlLiteral) & _
                                    " AND " & "UPPER(" & sColName & ") <= "
                        Set oField = oFSelector.Field(2)
                        strWhere = strWhere & UCase$(oField.SqlLiteral)
                    Case Else:
                        strWhere = strWhere & sColName & strComparator & oField.SqlLiteral
                End Select
            End If
        End If
    Next i
    If LenB(strInList) <> 0 Then
        If LenB(strWhere) = 0 Then
            strWhere = " WHERE "
        Else
            strWhere = strWhere & " AND "
        End If
        strWhere = strWhere & strInColName & " IN (" & strInList & ")"
    End If
    GetSqlWhereString = strWhere
    
End Function

Private Function SaveFromRow(eSave As enSaveType, oRow As IRow, _
            ByRef RowOfChanges As IRow) As enDbErrorType
    ' We update the database according to eSave, with the data to be saved
    ' given by oRow.  If there is any data created by this action,
    ' for example and autoincrement field, then it is returned in RowOfChanges.
    ' If no returned data, then RowOfChanges is Nothing.
    
Dim oRecordset  As Recordset
Dim strSql      As String
Dim strSelectID As String
Dim lColID      As Long
Dim iFieldID    As Integer
Dim oField      As IField
    
    SaveFromRow = DbErrorTypeUnknown
    Set RowOfChanges = Nothing
    
    Select Case (eSave)
    Case SaveTypeAllCases
        MsgBox "CTableHandler::SaveFromRow (all) Not implemented yet"
    Case SaveTypeIfNew
        strSql = GetSqlInsertString(oRow, strSelectID)
    Case SaveTypeIfExists
        strSql = GetSqlUpdateString(oRow)
    Case SaveTypeDelete
        strSql = GetSqlDeleteString()
    Case Else
        Debug.Assert False
    End Select
    
    On Error GoTo SaveFromRow_Err

    'perform the SQL command
    Call m_oDatabase.ExecuteCommand(strSql)
    
    'if there is an need to retrieve data then execute the SQL to get values
    If LenB(strSelectID) <> 0 Then
        ' We have some data to return, like AutoIncrement values.
        ' So create a Row to hold the new values
        Set RowOfChanges = New CRow
        ' Run SQL to get those values
        Set oRecordset = m_oDatabase.ExecuteCommand(strSelectID)
        For lColID = 0 To oRecordset.Fields.Count - 1 Step 1
            ' The returned column name encodes the index of the related field within oRow.
            iFieldID = Val(Mid$(oRecordset.Fields(lColID).Name, 6))
            Debug.Assert (iFieldID > 0)
            ' Using this we can point to the field within the row ...
            Set oField = oRow.Field(iFieldID)
            ' ... then update field value with returned value ...
            oField.ValueAsVariant = oRecordset.Fields(lColID).Value
            ' ... and finally we add the field to the row to return
            RowOfChanges.Add oField
        Next lColID
        Call oRecordset.Close
    End If
    
    SaveFromRow = DbErrorTypeNoError
    
    Exit Function
    
    
SaveFromRow_Err:
    If m_oDatabase.Connection.Errors.Count > 0 Then
       Dim oErr As Error
'       Debug.Assert False
       For Each oErr In m_oDatabase.Connection.Errors
            Select Case (oErr.SQLState)
            Case ODBC_ERR_Syntax_error_or_access_violation
                Debug.Assert False
            Case ODBC_ERR_DataType_conversion_error
                Debug.Assert False
            Case ODBC_ERR_Integrity_constraint_violation
                If oErr.NativeError = PERVASIVE_errKeyDuplicate Then
                    SaveFromRow = DbErrorTypeDuplicateKey
                    Exit For
                End If
                Debug.Assert False
            Case ODBC_ERR_General_error
                If oErr.NativeError = PERVASIVE_errKeyDuplicate2 Then
                    SaveFromRow = DbErrorTypeDuplicateKey
                    Exit For
                End If
                Debug.Assert False
            End Select
        Next oErr
    End If
    If SaveFromRow = DbErrorTypeUnknown Then
        Err.Raise Err.Number, "CTableHandler::SaveFromRow()", _
            Err.Description & vbCrLf & "CTableHandler::SaveFromRow(): " & _
            vbCrLf & strSql
        Debug.Assert False
    End If
End Function

Private Sub Class_Terminate()
    
    Set m_oTable = Nothing
    Set m_oDatabase = Nothing
    Set m_collFSelector = Nothing
    Set m_oSortKeys = Nothing

End Sub

'<CACH>****************************************************************************************
'* Function:  String IHandler_CreateLiteralView()
'**********************************************************************************************
'* Description:
    ' The Primary table is the one which determines whether a record is
    ' to be selected or not.  The Primary recordset is the recordset from
    ' this table that only includes records that match the selection criteria.
    ' In this method we process the Primary recordset, extracting data from it
    ' and placing it within rows within a new CView.
    ' Later, secondary handlers can fill out the data for each row, based on
    ' the primary data that is already there.
'**********************************************************************************************
'* Parameters:
'*In/Out:DataFormat Long.
'*In/Out:sLineSeparator String.
'*In/Out:sDataSeparator String.
'*In/Out:sTextDelim String.
'**********************************************************************************************
'* Returns:  String
'**********************************************************************************************
'* History:
'* 06/12/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function IHandler_CreateLiteralView(DataFormat As Long, sLineSeparator As String, sDataSeparator As String, sTextDelim As String) As String
    
Dim vtColValue  As Variant
Dim oRecordset  As Recordset
Dim iColNo      As Integer
Dim oField      As ADODB.Field
Dim strTempStr  As String
Dim strTempLine As String
Dim strSql      As String
Dim strNumber   As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".IHandler_CreateLiteralView"
    
    ' first build up the SQL statement
    strSql = GetSqlSelectAllString
    
    On Error GoTo CreateLiteralView_Err

    ' second execute the SQL
    
    Set oRecordset = m_oDatabase.ExecuteCommand(strSql)
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Starting - " & Now() & "(" & m_oTable.TableName & ")")
    While Not oRecordset.EOF
        ' Process each Row in the recordset
        strTempLine = vbNullString
        For iColNo = 0 To oRecordset.Fields.Count - 1 Step 1
            Set oField = oRecordset.Fields(iColNo)
            Debug.Print oField.Name, oField.Type, oField.NumericScale, oField.Precision, oField.DefinedSize
            Select Case (oField.Type)
                Case (adNumeric):   strNumber = Left$("00000000000", 5)
                                    strTempLine = strTempLine & oField.Value
                Case (adBoolean):
                                    If oField.Value = True Then
                                        strTempLine = strTempLine & "1"
                                    Else
                                        strTempLine = strTempLine & "0"
                                    End If
                Case (adDBDate):
                                    If oField.Value = Null Then
                                        strTempLine = strTempLine & "--/--/--"
                                    Else
                                        strTempLine = strTempLine & oField.Value
                                    End If
                Case Else:          strTempLine = strTempLine & oField.Value
            End Select
            If DataFormat = 2 Then strTempLine = strTempLine & sDataSeparator
        Next iColNo
        strTempStr = strTempStr & strTempLine
        If DataFormat = 2 Then strTempStr = strTempStr & sLineSeparator
        oRecordset.MoveNext
    Wend
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Ended Transferring Data to oView - " & Now() & "(" & m_oTable.TableName & ")")
    
    IHandler_CreateLiteralView = strTempStr
    
    Exit Function
    
CreateLiteralView_Err:
    Err.Raise Err.Number, PROCEDURE_NAME, _
            Err.Description & vbCrLf & PROCEDURE_NAME & _
            vbCrLf & strSql
            Resume Next



End Function 'IHandler_CreateLiteralView

Private Function IHandler_GetAggregateValue(ByVal nFunctionId As Integer, oSourceField As IField) As Variant
    ' The Primary table is the one which determines whether a record is
    ' to be selected or not.  The Primary recordset is the recordset from
    ' this table that only includes records that match the selection criteria.
    ' In this method we process the Primary recordset, extracting data from it
    ' and placing it within rows within a new CView.
    ' Later, secondary handlers can fill out the data for each row, based on
    ' the primary data that is already there.
Dim strSql      As String
Dim oRecordset  As Recordset
Dim vtColValue  As Variant
Dim arValues()  As Variant

Const PROCEDURE_NAME As String = MODULE_NAME & ".IHandler_GetAggregateValue"
    
    ' first build up the SQL statement
    Call DebugMsg(MODULE_NAME, "Aggregate - Starting", Now(), endlDebug)
    
    strSql = GetSqlAggregateString(nFunctionId, oSourceField)
    
    On Error GoTo GetAggregateValue_Err

    ' second execute the SQL
    Set oRecordset = m_oDatabase.ExecuteCommand(strSql)
    
    ' Process returned Row in recordset
    vtColValue = Null
    If Not oRecordset.EOF Then
        vtColValue = oRecordset.Fields(0)
        'if selecting distinct value then return results as an array
        If nFunctionId = AGG_DIST Then
            ReDim arValues(0)
            arValues(0) = oRecordset.Fields(0)
            Call oRecordset.MoveNext
            While Not oRecordset.EOF
                ReDim Preserve arValues(UBound(arValues) + 1)
                arValues(UBound(arValues)) = oRecordset.Fields(0)
                Call oRecordset.MoveNext
            Wend
            vtColValue = arValues
            Call DebugMsg("MODULE_NAME", "Ended Aggregate-" & Now() & "No. Values=" & UBound(vtColValue) + 1, endlDebug)
        Else
            Call DebugMsg("MODULE_NAME", "Ended Aggregate-" & Now() & "Value=" & vtColValue, endlDebug)
        End If
    End If
    
    IHandler_GetAggregateValue = vtColValue
    
    Exit Function
    
GetAggregateValue_Err:
    
    Err.Raise Err.Number, PROCEDURE_NAME, _
            Err.Description & vbCrLf & PROCEDURE_NAME & _
            vbCrLf & strSql
    Resume Next

End Function 'IHandler_GetAggregateValue

Public Function IHandler_DoBulkUpdate(oTarget As Object, oSource As Object) As Boolean
    Dim strSql      As String
    Dim oRecordset  As Recordset
    
    strSql = GetSqlBulkUpdateString(oTarget, oSource)
    
    On Error GoTo IHandler_DoBulkUpdate_Err

    ' second execute the SQL
    Set oRecordset = m_oDatabase.ExecuteCommand(strSql)
    
    Set oRecordset = Nothing
    
    IHandler_DoBulkUpdate = True
    
    Exit Function
    
IHandler_DoBulkUpdate_Err:
    
    Call DebugMsg(MODULE_NAME, "DoBulkUpdate", endlTraceOut, "Error-" & Err.Number & "-" & Err.Description & " " & strSql)
    Err.Raise Err.Number, "CTableHandler::IHandler_DoBulkUpdate()", _
            Err.Description & vbCrLf & "CTableHandler::IHandler_DoBulkUpdate(): " & _
            vbCrLf & strSql
    Resume Next

End Function

Public Function IHandler_Initialise(oTable As OasysInterfaces_Wickes.ITable, oDatabase As OasysInterfaces_Wickes.IBoDatabase, _
                                    Optional SortKeys As OasysInterfaces_Wickes.IBoSortKeys) As Boolean
    Debug.Assert Not (oTable Is Nothing)
    Debug.Assert Not (oDatabase Is Nothing)
    Set m_oTable = oTable
    Set m_oDatabase = oDatabase
    Set m_collFSelector = New Collection
    Set m_oSortKeys = SortKeys
    IHandler_Initialise = True
End Function


Public Property Get IHandler_TableId() As Integer
    IHandler_TableId = m_oTable.TableId
End Property

Public Function IHandler_AddSelector(oFSelector As IFieldSelector) As Boolean
    ' Note we only use this reference to the selector and don't own it
    m_collFSelector.Add oFSelector
    IHandler_AddSelector = True
End Function
Private Function IHandler_CreatePrimaryView() As IView
    ' The Primary table is the one which determines whether a record is
    ' to be selected or not.  The Primary recordset is the recordset from
    ' this table that only includes records that match the selection criteria.
    ' In this method we process the Primary recordset, extracting data from it
    ' and placing it within rows within a new CView.
    ' Later, secondary handlers can fill out the data for each row, based on
    ' the primary data that is already there.
    Dim i           As Integer
    Dim j           As Integer
    Dim oField      As IField
    Dim vtColValue  As Variant
    Dim oNewField   As IField
    Dim oRecordset  As Recordset
    Dim strSql      As String
    Dim strWhere    As String
    Dim bIsSelected As Boolean
    Dim oRow        As IRow
    Dim oView       As IView
    Dim nComparator As Long
    Dim sColName    As String
    Dim nFieldCount As Integer
    Dim oSubRow     As IRow
    Dim oSubField   As IField
    Dim oFSelector  As IFieldSelector
    Dim oFieldGroup As CFieldGroup
    
    ' first build up the SQL statement
    strSql = GetSqlSelectString
    
    On Error GoTo CreatePrimaryView_Err

    ' second execute the SQL
    
    Set oRecordset = m_oDatabase.ExecuteCommand(strSql)
    
    ' Create a working row object which we constantly re-use, then delete
    Set oRow = New CRow
    Set oView = New CView
    
    Call DebugMsg(MODULE_NAME, "CreatePrimaryView", endlDebug, "Starting - " & Now() & "(" & m_oTable.TableName & ")")
    Do Until oRecordset.EOF
        ' Process each Row in the recordset
        bIsSelected = True
        oRow.Clear
        For i = 1 To m_collFSelector.Count
            Set oFSelector = m_collFSelector.Item(i)
            Set oField = oFSelector.Field
            
#If ccValidateSqlResults = 1 Then
            ' If we built the SQL command correctly, we only get the rows
            ' we selected in the selector.  The following code validates it in
            ' case we ever want to check.
            If m_oTable.IsSimpleField(oField.Id) Then
                ' we can select on simple fields but not group fields
                If m_oTable.IsSum(oField.Id) Then
                    vtColValue = oRecordset("SUM" & m_oTable.SimpleColumnName(oField))
                Else
                    vtColValue = oRecordset(m_oTable.SimpleColumnName(oField))
                End If
                ' map column to property
                If Not (vtColValue = Empty) Then
'                    If Not oField.Matches(oFSelector.Comparator, vtColValue) Then
                    If Not oFSelector.Matches(vtColValue) Then
                        bIsSelected = False
                        Exit For
                    End If
                End If
            End If
#End If
            
            ' We have now checked the current field and it is selected, but we
            ' don't know yet if a later field deselects the entire row.
            ' So we build the row in anticipation that it is selected.
            
            If Not oFSelector.IsSelectOnly Then
                'Extract Column Name as we may manipulate the value
                sColName = m_oTable.SimpleColumnName(oField)
                'Check if this is an occurrence Column
                If m_oTable.IsSimpleField(oField.Id) Then
                    ' We want to return data for this guy, so create field of correct type
                    Set oNewField = oField.Duplicate
                    If m_oTable.IsSum(oField.Id) Then
                        sColName = "SUM" & sColName
                    End If
                    'No occurrence so just move
                    If IsNull(oRecordset(sColName)) = True Then
                        oNewField.ValueAsVariant = Null
                    Else
                        oNewField.ValueAsVariant = RTrim(oRecordset(sColName))
                    End If
                    ' and pop it into our working row
                    oRow.Add oNewField
                    Set oNewField = Nothing
                Else
                    ' Group field, ie. occurs.
                    ' Create new FieldGroup
                    Set oFieldGroup = oField.Duplicate
                    ' Get new subrow
                    
                    Set oSubRow = oFieldGroup.GroupRow
                    j = oFieldGroup.Subscript
                    If j > 0 Then
                        ' We pick just one item from the array
                        Set oSubField = oSubRow.Field(1)
                        If IsNull(oRecordset(sColName & j)) = True Then
                            oSubField.ValueAsVariant = Null
                        Else
                            oSubField.ValueAsVariant = RTrim(oRecordset(sColName & j))
                        End If
                    Else
                        ' We handle the whole array
                        'Step through each occurrence and move to Row collections
                        nFieldCount = m_oTable.FieldCount(oField.Id)
                        Debug.Assert nFieldCount = oSubRow.Count
                        For j = 1 To nFieldCount
                            Set oSubField = oSubRow.Field(j)
                            If IsNull(oRecordset(sColName & j)) = True Then
                                oSubField.ValueAsVariant = Null
                            Else
                                oSubField.ValueAsVariant = RTrim(oRecordset(sColName & j))
                            End If
                        Next j
                    End If
                    ' and pop the group field into our working row
                    oRow.Add oFieldGroup
                    Set oFieldGroup = Nothing
                    Set oSubRow = Nothing
                End If
            End If  ' Not oFSelector.IsSelectOnly
        Next i
        If bIsSelected Then
            ' Row is set up so add to the view
            oView.Add oRow
        Else
            ' Row is set up but not selected, so discard
            oRow.Clear
            ' Shouldn't happen if we have used the most efficient SQL
            Debug.Assert False
        End If
        
        oRecordset.MoveNext
        
    Loop
    Call DebugMsg(MODULE_NAME, "CreatePrimaryView", endlDebug, "Ended Transferring Data to oView - " & Now() & "(" & m_oTable.TableName & ")")
    
    Set IHandler_CreatePrimaryView = oView
    Set oRow = Nothing
    Set oView = Nothing
    
    Exit Function
    
CreatePrimaryView_Err:
    Err.Raise Err.Number, "CTableHandler::IHandler_CreatePrimaryView()", _
            Err.Description & vbCrLf & "CTableHandler::IHandler_CreatePrimaryView(): " & CStr(Erl) & _
            vbCrLf & strSql
            Resume Next
End Function

Private Function IHandler_CreateSelectSQL(Optional RowSelector As IRowSelector) As String
    
    'get the SQL statement
    IHandler_CreateSelectSQL = GetSqlSelectString(RowSelector)
        
End Function

Public Function IHandler_BuildRow(oRow As IRow) As Boolean
    MsgBox "IHandler_BuildRow() - Not implemented yet"
End Function

Private Function IHandler_SaveFromRow(eSave As enSaveType, oRow As IRow, _
            ByRef RowOfChanges As IRow) As enDbErrorType
    
Dim oRecordset  As Recordset
Dim strSql As String
    
    Select Case (eSave)
    Case SaveTypeAllCases
        ' ***** This needs optimising *****
        IHandler_SaveFromRow = SaveFromRow(SaveTypeIfNew, oRow, RowOfChanges)
        If IHandler_SaveFromRow = DbErrorTypeDuplicateKey Then
            ' Insert failed, so we need an update
            IHandler_SaveFromRow = SaveFromRow(SaveTypeIfExists, oRow, RowOfChanges)
        End If
    Case Else
        IHandler_SaveFromRow = SaveFromRow(eSave, oRow, RowOfChanges)
    End Select
End Function

Private Function IHandler_DeleteBoCollection() As enDbErrorType
    ' Delete all Bos that match the selection criteria
    Dim strSql      As String

    IHandler_DeleteBoCollection = DbErrorTypeUnknown
    strSql = "DELETE FROM " & m_oTable.TableName & GetSqlWhereString()
    
    On Error GoTo DeleteBoCollection_Err

    'perform the SQL command
    Call m_oDatabase.ExecuteCommand(strSql)
    
    IHandler_DeleteBoCollection = DbErrorTypeNoError
    Exit Function
    
    
DeleteBoCollection_Err:
    If m_oDatabase.Connection.Errors.Count > 0 Then
       Dim oErr As Error
       Debug.Assert False
       For Each oErr In m_oDatabase.Connection.Errors
            Select Case (oErr.SQLState)
            Case ODBC_ERR_Syntax_error_or_access_violation
                Debug.Assert False
            Case ODBC_ERR_Integrity_constraint_violation
                If oErr.NativeError = PERVASIVE_errKeyDuplicate Then
                    IHandler_DeleteBoCollection = DbErrorTypeDuplicateKey
                Else
                    Debug.Assert False
                End If
            End Select
        Next oErr
    End If
    If IHandler_DeleteBoCollection = DbErrorTypeUnknown Then
        Err.Raise Err.Number, "CTableHandler::IHandler_DeleteBoCollection()", _
            Err.Description & vbCrLf & "CTableHandler::IHandler_DeleteBoCollection(): " & _
            vbCrLf & strSql
        Debug.Assert False
    End If
End Function

