VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : CTable
'* Date   : 27/09/02
'* Author : martynw
'*$Archive: /Projects/OasysV2/VB/OasysSqlDatabaseCom/CTable.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 4/12/03 12:37 $
'* $Revision: 17 $
'* Versions:
'* 27/09/02    martynw
'*             Header added.
'</CAMH>***************************************************************************************

' This class represents a database table and holds all Db information regarding
' the table.  Typically, each Bo will be mapped to a table object to handle persistence.
' This class provides methods to map IFields to column names, expand occurring
' IFields into a list of expanded column names and provide the field value(s)
' in a SQL-friendly format.
' The table object is built Just In Time when it is required and will normally
' hang around until End Of Run, but may be deleted anytime.  The class is primarily
' used by the CTableHandler class.

Option Explicit
Implements ITable

Const COLUMN_FLAG_AUTOINCREMENT = "A"
Const COLUMN_FLAG_DISTINCT = "D"
Const COLUMN_FLAG_SUM = "S"
Const COLUMN_FLAG_TIME_AS_CHAR6 = "T"

Const SQL_LITERAL_TYPE_STANDARD = 0
Const SQL_LITERAL_TYPE_TIME_AS_CHAR6 = 1

Const MODULE_NAME As String = "CTable"
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Private m_nTableId          As Integer
Private m_strTablename      As String
Private m_colColumnNames    As Collection
Private m_nKeyColumnCount   As Integer
Public Type tColumnNameItem
    nOccurs         As Integer  ' Expand name to give multiple Db column names
    bIsAutoIncr     As Boolean  ' This is a database AutoIncrement data type
    bIsDistinct     As Boolean  ' This is a database DISTINCT qualifier
    bIsSum          As Boolean  ' This is a database SUM function
    strName         As String   ' Column name (subject to nOccurs > 0)
    nSqlLiteralType As Byte     ' Non-standard SQL type.
End Type

Private Sub Class_Initialize()
    Set m_colColumnNames = New Collection
    m_nKeyColumnCount = 0
End Sub
Public Function Initialise(nTableId As Integer, strNameList As String) As Boolean
    ' We add a list of column names into our collection.
    ' They must be in CID sequence.
    ' The first name in the list is the table name which may optionally have
    ' parameters within parentheses.  The parameters are a semi-colon-delimited list
    ' of primary key column names, and each may have parameters within parentheses.
    ' The second and all subsequent names are data column names, and each may have
    ' parameters within parentheses.
    
Dim strNames()      As String
Dim strKeys()       As String
Dim strCurrent      As String
Dim i               As Integer 'I the universal loop counter (to C programmers)
Dim ColumnItem      As tColumnNameItem
Dim EmptyColumnItem As tColumnNameItem
Dim posn            As Integer
    
    On Error GoTo Initialise_Error
    
    EmptyColumnItem.nOccurs = 0
    EmptyColumnItem.bIsAutoIncr = False
    EmptyColumnItem.bIsDistinct = False
    EmptyColumnItem.bIsSum = False
    EmptyColumnItem.nSqlLiteralType = SQL_LITERAL_TYPE_STANDARD
    
    m_nTableId = nTableId
    strNames = Split(strNameList, ",")
    '------------- Handle Table name --------------
    m_strTablename = strNames(0)
    ' May be table options within parentheses.
    If Right$(m_strTablename, 1) = ")" Then
        ' Yes we should have Table options to process (eg. Key)
        strCurrent = Left$(m_strTablename, Len(m_strTablename) - 1)
        posn = InStr(strCurrent, "(")
        If posn > 0 Then
            ' Yes we DO have Table options to process (ie. Keys)
            m_strTablename = Left$(strCurrent, posn - 1)     ' Extract table name
            strCurrent = Right$(strCurrent, Len(strCurrent) - posn)
            strKeys = Split(strCurrent, ";")
            '------------- Handle Key Column names --------------
            For i = 0 To UBound(strKeys)
                'Reset column properties
                ColumnItem = EmptyColumnItem
                ColumnItem.strName = strKeys(i)
                ' May be options within braces
                If Right$(ColumnItem.strName, 1) = ")" Then
                    SetColumnFlags ColumnItem.strName, ColumnItem
                End If
                m_colColumnNames.Add ColumnItem
            Next i
            ' We record the last key entry number in m_colColumnNames
            m_nKeyColumnCount = i
        Else
            Debug.Assert False  ' No opening bracket!
        End If
    End If
    
    '------------- Handle Data Column names --------------
    ' Note that strNames(0) contains the table name AND the keys, so strNames(1)
    ' represents the first column name.
    For i = 1 To UBound(strNames)
        'Reset column properties
        ColumnItem = EmptyColumnItem
        ColumnItem.strName = strNames(i)
        ' May be options within braces
        If Right$(ColumnItem.strName, 1) = ")" Then
            SetColumnFlags ColumnItem.strName, ColumnItem
        End If
        m_colColumnNames.Add ColumnItem
    Next i
    Initialise = True
    
Exit Function

Initialise_Error:
    Dim strMess As String
    On Error Resume Next
    strMess = " Column #" & i & " : "
'    If Not (strNames(0) Is Nothing) And (i >= UBound(strNames)) Then
    If i > UBound(strNames) Then
        strMess = strMess & "Unknown"
    Else
        strMess = strMess & strNames(i)
    End If
    Err.Raise OASYS_ERR_INCONSISTENT_OASYS_SETUP, "CTable::Initialise()", _
                "CTable::Initialise()" & vbCrLf & "Error initialising table Id: " & _
                nTableId & " Name: " & m_strTablename & strMess
End Function

Private Function SetColumnFlags(strParams As String, ColumnItem As tColumnNameItem)
    ' This function simply interprests any flags found in the column specifier
    ' and sets the appropriate flag in the ColumnItem structure.  It assumes that
    ' the column specifier string is enclosed in parentheses.
    Dim posn        As Integer
    Dim strCurrent  As String
    Dim sOptions    As String  'used to extract any column options i.e. Occurrences / Key
    
    ' remove right-hand parenthesis
    strCurrent = Left$(strParams, Len(strParams) - 1)
    posn = InStr(strCurrent, "(")
    If posn > 0 Then
        sOptions = Mid$(strCurrent, posn + 1)
        If InStr(sOptions, COLUMN_FLAG_AUTOINCREMENT) > 0 Then
            ColumnItem.bIsAutoIncr = True
            sOptions = Replace(sOptions, COLUMN_FLAG_AUTOINCREMENT, vbNullString)
        End If
        If InStr(sOptions, COLUMN_FLAG_DISTINCT) > 0 Then
            ColumnItem.bIsDistinct = True
            sOptions = Replace(sOptions, COLUMN_FLAG_DISTINCT, vbNullString)
        End If
        If InStr(sOptions, COLUMN_FLAG_SUM) > 0 Then
            ColumnItem.bIsSum = True
            sOptions = Replace(sOptions, COLUMN_FLAG_SUM, vbNullString)
        End If
        If InStr(sOptions, COLUMN_FLAG_TIME_AS_CHAR6) > 0 Then
            ColumnItem.nSqlLiteralType = SQL_LITERAL_TYPE_TIME_AS_CHAR6
            sOptions = Replace(sOptions, COLUMN_FLAG_TIME_AS_CHAR6, vbNullString)
        End If
        If IsNumeric(sOptions) Then
            ColumnItem.nOccurs = CInt(sOptions)
        End If
        ColumnItem.strName = Left$(strCurrent, posn - 1)
        Call DebugMsg(MODULE_NAME, "Initialise", endlDebug, "Parameter processed for " & m_strTablename & " column " & ColumnItem.strName & "(" & ColumnItem.nOccurs & "), Key=" & ColumnItem.bIsAutoIncr)
    Else
        Debug.Assert False  ' No opening bracket!
    End If
End Function

Public Function InitialiseForeignKey(strName As String, nCid As Long)
    MsgBox "Not supported yet"
End Function
Public Property Get TableId() As Integer
    TableId = m_nTableId
End Property

Public Property Get SimpleColumnName(oField As IField) As String
    ' This method returns the base column name, disregarding any occurrences.
    On Error GoTo ColumnName_Error
    Dim ColumnItem  As tColumnNameItem
    Dim nCid        As Long
    nCid = oField.Id
    Debug.Assert m_colColumnNames.Count >= (nCid And &H7FFF)
    ColumnItem = m_colColumnNames.Item(nCid And &H7FFF)
    SimpleColumnName = ColumnItem.strName
    Exit Property
    
ColumnName_Error:
    Err.Raise OASYS_ERR_BO_CLASS_INCOMPLETE, "CTable::SimpleColumnName()", _
                "CTable::SimpleColumnName()" & vbCrLf & "No Column name found for Table/Column Id: " & (nCid \ &H10000) & "/" & (nCid And &H7FFF) & _
                vbCrLf & "See CBoDatabase::CreateTable()"
End Property

Public Property Get TableName() As String
    TableName = m_strTablename
End Property

Public Property Get SqlColumnName(oField As IField) As String
    ' This method returns a 'cooked' column name.  The cooking may consist of expanding
    ' the one name to a comma-delimited list of names that have an occurrence number
    ' as a suffix to match the ocurrences that are used in the old UpTrack database.
    ' The names are also cooked by enclosing them in SQL (double) quotes.
    Dim ColumnItem  As tColumnNameItem
    Dim i           As Integer
    Dim nCid        As Long
    Dim oFieldGroup As CFieldGroup
    nCid = oField.Id
    
    Debug.Assert m_colColumnNames.Count >= (nCid And &H7FFF)
    ColumnItem = m_colColumnNames.Item(nCid And &H7FFF)
    
    If ColumnItem.nOccurs = 0 Then
        ' Simple field
        If ColumnItem.bIsSum Then
            ' We have an agregate function SUM
            SqlColumnName = "SUM(""" & ColumnItem.strName & """) AS ""SUM" & ColumnItem.strName & """"
        Else
            SqlColumnName = """" & ColumnItem.strName & """"
            If ColumnItem.bIsDistinct Then SqlColumnName = "DISTINCT " & SqlColumnName
        End If
    Else
        'if error 438, check that Table columns match FID Order
        ' We have an occurs to handle, so get all the names into a list.
        Set oFieldGroup = oField
        i = oFieldGroup.Subscript
        If i > 0 Then
            ' We pick just one item from the array
            SqlColumnName = """" & ColumnItem.strName & i & """"
        Else
            ' We handle the whole array
            For i = 1 To ColumnItem.nOccurs
                SqlColumnName = SqlColumnName & """" & ColumnItem.strName & i & """, "
            Next i
            'chop off last ", "
            SqlColumnName = Left$(SqlColumnName, Len(SqlColumnName) - 2)
        End If
    End If

End Property

Public Property Get SqlLiteral(oField As IField) As String
    ' This method returns a potentially 'cooked' field value.  The cooking will consist of
    ' any special conversions where the logical field type needs mapping to
    ' a non-standard format (eg. Pervasive time before version 8 held as char(6)).
    If m_colColumnNames.Item(oField.Id And &H7FFF).nSqlLiteralType = SQL_LITERAL_TYPE_STANDARD Then
        ' Default is standard, ie. no conversion.
        SqlLiteral = oField.SqlLiteral
    Else
        If m_colColumnNames.Item(oField.Id And &H7FFF).nSqlLiteralType = SQL_LITERAL_TYPE_TIME_AS_CHAR6 Then
            Debug.Assert oField.FieldType = FieldTypeDate
            Dim oTempDate As CFieldDate
            Set oTempDate = oField
            SqlLiteral = oTempDate.SqlLiteralTimeAsChar6
        End If
    End If
End Property

'<CACH>****************************************************************************************
'* Property Get:  Boolean ReturnID()
'**********************************************************************************************
'* Description: Used as part of the Inserting of value to return if the column must be SELECTed
'*              once the value has been added to the table.  This is used where the system
'*              allocates an ID to a row which is then used as a Foreign key for data stored in
'*              other tables.
'**********************************************************************************************
'* Parameters:
'*In/Out:nCid   Long. - Field ID as defined in modBO.bas to uniquely identify each field
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 07/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Property Get IsAutoIncrement(nCid As Long) As Boolean
    
Const PROCEDURE_NAME As String = MODULE_NAME & ".ReturnID_Get"
    
    IsAutoIncrement = m_colColumnNames.Item(nCid And &H7FFF).bIsAutoIncr
    
End Property

Public Property Get IsDistinct(nCid As Long) As Boolean
    IsDistinct = m_colColumnNames.Item(nCid And &H7FFF).bIsDistinct
End Property

Public Property Get IsSum(nCid As Long) As Boolean
    IsSum = m_colColumnNames.Item(nCid And &H7FFF).bIsSum
End Property

Public Property Get IsSimpleField(nCid As Long) As Boolean
    ' Check if the colum name is used for a simple field or a group field.
    Dim ColumnItem  As tColumnNameItem
    
    Debug.Assert m_colColumnNames.Count >= (nCid And &H7FFF)
    ColumnItem = m_colColumnNames.Item(nCid And &H7FFF)
    
    ' If no occurs, then it must be simple
    IsSimpleField = (ColumnItem.nOccurs = 0)
End Property

Public Property Get FieldCount(nCid As Long) As Integer
    Dim ColumnItem  As tColumnNameItem
    
    Debug.Assert m_colColumnNames.Count >= (nCid And &H7FFF)
    ColumnItem = m_colColumnNames.Item(nCid And &H7FFF)
    
    ' If no occurs, then it must be simple
    FieldCount = ColumnItem.nOccurs
End Property

Public Property Get KeyCount() As Integer
    ' There may be one primary key or none or list of columns that
    ' comprise a composite key.  This property tells you which.
    ' The m_colColumnNames collection starts off with the key
    ' names, in sequence, followed by the non-key names.
    KeyCount = m_nKeyColumnCount
End Property

'----------------------------------------------------------------------------
' ITable Interface specifics
'----------------------------------------------------------------------------
Public Function ITable_Initialise(nTableId As Integer, strNameList As String) As Boolean
    ITable_Initialise = Initialise(nTableId, strNameList)
End Function

Public Function ITable_InitialiseForeignKey(strName As String, nCid As Long) As Boolean
    ITable_InitialiseForeignKey = InitialiseForeignKey(strName, nCid)
End Function

Public Property Get ITable_TableId() As Integer
    ITable_TableId = TableId
End Property

Public Property Get ITable_SimpleColumnName(oField As IField) As String
    ITable_SimpleColumnName = SimpleColumnName(oField)
End Property

Public Property Get ITable_TableName() As String
    ITable_TableName = TableName
End Property

Public Property Get ITable_SqlColumnName(oField As IField) As String
    ITable_SqlColumnName = SqlColumnName(oField)
End Property

Public Property Get ITable_SqlLiteral(oField As IField) As String
    ITable_SqlLiteral = SqlLiteral(oField)
End Property

Public Property Get ITable_IsAutoIncrement(nCid As Long) As Boolean
     ITable_IsAutoIncrement = IsAutoIncrement(nCid)
End Property

Public Property Get ITable_IsDistinct(nCid As Long) As Boolean
    ITable_IsDistinct = IsDistinct(nCid)
End Property

Public Property Get ITable_IsSum(nCid As Long) As Boolean
    ITable_IsSum = IsSum(nCid)
End Property

Public Property Get ITable_IsSimpleField(nCid As Long) As Boolean
    ITable_IsSimpleField = IsSimpleField(nCid)
End Property

Public Property Get ITable_FieldCount(nCid As Long) As Integer
    ITable_FieldCount = FieldCount(nCid)
End Property

Public Property Get ITable_KeyCount() As Integer
    ITable_KeyCount = KeyCount
End Property

