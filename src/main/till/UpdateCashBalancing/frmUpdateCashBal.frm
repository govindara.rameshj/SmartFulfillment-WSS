VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmUpdateCashBal 
   Caption         =   "Update Cash Balancing"
   ClientHeight    =   2175
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7995
   Icon            =   "frmUpdateCashBal.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2175
   ScaleWidth      =   7995
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   1800
      Width           =   7995
      _ExtentX        =   14102
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmUpdateCashBal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Refunds Report/frmRefundReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'* 26/03/08    mauricem
'*             WIX1312 - Added WEEE Rate for each item
'* 10/09/08    darrylh - WIX1332 added fields for cashier performance
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmUpdateCashBal"
Const PRM_USE_SQL As Integer = 140

Dim mdteSystemDate As Date
Dim mlngPeriodID   As Long
Dim mlngTranPeriodID As Long
Dim mblnFromNightlyClose  As Boolean
Dim mblnPervasive As Boolean

Private Sub Form_Load()

Dim lngOptNo As Long

    Call DebugMsg(MODULE_NAME, "Program Loaded", endlDebug, "Running: " & Command)

    Call GetRoot
        
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)

On Error Resume Next
    mblnPervasive = goSession.GetParameter(PRM_USE_SQL) <> 1
On Error GoTo 0

'    Call InitialiseStatusBar(sbStatus)
    If Left(Command, 1) = "V" Then
        If mblnPervasive Then
            Call DecodeVisionSalesPervasive(Command)
        Else
            Call DecodeVisionSales(Command)
        End If
    Else
        Call DecodeParameters(Command)
    End If
    Call DebugMsg(MODULE_NAME, "Program Ending ...", endlDebug, "Stopped.")
    End
        
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 4500) Then
        Me.Width = 4500
        Exit Sub
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF3): 'if F3 then exit
'                    Call cmdReset_Click
                Case (vbKeyF7): 'if F7 then check if any process buttons active
 '                   If (cmdPGProcess.Visible = True) Then cmdPGProcess.Value = True
 '                   If (cmdGetPriceChanges.Visible = True) Then cmdGetPriceChanges.Value = True
 '                   If (cmdGetSuppItems.Visible = True) Then cmdGetSuppItems.Value = True
                    KeyCode = 0
                Case (vbKeyF9): 'if F10 then exit
  '                  If (cmdPrint.Visible = True) Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
   '                 Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

    
Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strTillID    As String
Dim strTranNo    As String
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

Dim strSQL              As String
Dim strDefaultCurr      As String
Dim strUser             As String

Dim strCurr As String
Dim dblTender As Double
        
Dim LinesReversed   As Integer
Dim LinesScanned    As Integer
Dim LinesSold       As Integer
Dim TotalDiscount   As Currency

Dim oSaleBO     As cSale_Wickes.cPOSHeader
Dim oPaymentBO  As cSale_Wickes.cPOSPayment
Dim oLineBo     As cSale_Wickes.cPOSLine

Dim rsCashBal As New ADODB.Recordset

Dim strCBBUDate As String

    vntSection = Split(strCommand, ",")
    
    If (Left(strCommand, 1) <> "T") Then End
    
    strTempDate = Mid$(strCommand, 2, 8)
    dteTempDate = GetParamDate(strTempDate)
    strTillID = Mid$(strCommand, 10, 2)
    strTranNo = Mid$(strCommand, 12, 4)
    
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    Set oSaleBO = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    Call oSaleBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, dteTempDate)
    Call oSaleBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TillID, strTillID)
    Call oSaleBO.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionNo, strTranNo)
    If oSaleBO.LoadMatches.Count = 0 Then
        Call MsgBox("Unable to retrieve Transaction " & dteTempDate & " - " & strTillID & ":" & strTranNo, vbCritical, "Unable to Process Transaction")
        End
    End If
    
    If (oSaleBO.TrainingMode = True) Then
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "Exiting Updating as Training Transaction")
        End
    End If
    
    strUser = oSaleBO.CashierID
    strTillID = oSaleBO.TillID
    
    'Force loading of payments
    If (oSaleBO.Payments.Count > 0) Then
    End If
    
    
    'Get Period to record sale against
    mlngTranPeriodID = GetPeriodID(dteTempDate, strCBBUDate, True)
    mlngPeriodID = GetPeriodID(Now(), strCBBUDate, False)
    
    Call rsCashBal.Open("SELECT ID FROM SystemCurrency WHERE IsDefault=1", goDatabase.Connection)
    If (rsCashBal.EOF = False) Then
        strDefaultCurr = Trim$(rsCashBal.Fields("ID"))
    End If
    Call rsCashBal.Close
       
    'Retrieve existing Cashiers and Till Control records exist and create if missing
    Call CheckCashBalCashier(mlngPeriodID, strDefaultCurr, strUser, strDefaultCurr)
    Call CheckCashBalTill(mlngPeriodID, strDefaultCurr, strTillID, strDefaultCurr)
    
    'loop through each line and count lines sold, scanned, reversed
    LinesReversed = 0
    LinesScanned = 0
    LinesSold = 0
    TotalDiscount = 0 'Added WIX2009-077 1/10/09 M.Milne
    For Each oLineBo In oSaleBO.Lines
        If oLineBo.SaleType <> "W" Then 'PRF SKU
            TotalDiscount = TotalDiscount + (oLineBo.EmpSalePrimaryMarginAmount * oLineBo.QuantitySold)
            If oLineBo.LineReversed = True Then
                If (oLineBo.QuantitySold > 0) Then
                    LinesReversed = LinesReversed + 1
                End If
            Else
                If (oLineBo.QuantitySold > 0) Then '
                    If (oLineBo.MarkedDownStock = False) And (oLineBo.SaleType <> "D") And (oLineBo.SaleType <> "V") Then LinesSold = LinesSold + 1
                    If (oLineBo.BarcodeEntry = True) Then LinesScanned = LinesScanned + 1
                End If
            End If
        End If
    Next
    TotalDiscount = TotalDiscount * -1 'Swap Negative flag
    strSQL = "UPDATE CashBalTill SET " & UpdateValues(oSaleBO.TransactionCode, oSaleBO.TotalSaleAmount, TotalDiscount, oSaleBO.ReasonCode, oSaleBO.Voided, LinesSold, LinesReversed, LinesScanned)
    strSQL = strSQL & " WHERE TillID='" & strTillID & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strDefaultCurr & "'"
    Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateTill:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)
    

    strSQL = "UPDATE CashBalCashier SET " & UpdateValues(oSaleBO.TransactionCode, oSaleBO.TotalSaleAmount, TotalDiscount, oSaleBO.ReasonCode, oSaleBO.Voided, LinesSold, LinesReversed, LinesScanned)
    strSQL = strSQL & " WHERE CashierID='" & strUser & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strDefaultCurr & "'"
    Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashier:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)

    'Step through each payment and save details
    For Each oPaymentBO In oSaleBO.Payments
        'Check whether payments is in default currency
        If oPaymentBO.ForeignTender <> 0 Then
            strCurr = oPaymentBO.CurrencyCode
            dblTender = oPaymentBO.ForeignTender

            'Check and create headers if not exists for this currency
            Call CheckCashBalTill(mlngPeriodID, strCurr, strTillID, strDefaultCurr)
            Call CheckCashBalCashier(mlngPeriodID, strCurr, strUser, strDefaultCurr)

        Else
            strCurr = strDefaultCurr
            dblTender = oPaymentBO.TenderAmount * -1
        End If
        
        'Retrieve existing Cashiers tender records
        Call rsCashBal.Open("SELECT * FROM CashBalCashierTen WHERE CashierID='" & strUser & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType, goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalCashierTen (PeriodID, CashierID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngPeriodID & ",'" & strUser & "','" & strCurr & "'," & oPaymentBO.TenderType & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "InsertCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalCashierTen SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngPeriodID & " AND " & _
             "CashierID='" & strUser & "' AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        
        'Retrieve existing Till tender records
        Call rsCashBal.Open("SELECT * FROM CashBalTillTen WHERE TillID='" & strTillID & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType, goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalTillTen (PeriodID, TillID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngPeriodID & ",'" & strTillID & "','" & strCurr & "'," & oPaymentBO.TenderType & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalTillTen SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngPeriodID & " AND " & _
             "TillID='" & strTillID & "' AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
    
        'Added 26/9/09 WIX1377 Update Tenders into Variance Records
        'Retrieve existing Cashiers Variance tender records
        Call rsCashBal.Open("SELECT * FROM CashBalCashierTenVar WHERE CashierID='" & strUser & "' AND TradingPeriodID=" & mlngPeriodID & " AND PeriodID=" & mlngTranPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType, goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalCashierTenVar (PeriodID, TradingPeriodID, CashierID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngTranPeriodID & "," & mlngPeriodID & ",'" & strUser & "','" & strCurr & "'," & oPaymentBO.TenderType & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "InsertCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalCashierTenVar SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngTranPeriodID & " AND TradingPeriodID=" & mlngPeriodID & _
             " AND CashierID='" & strUser & "' AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        
        'Retrieve existing Till tender records
        Call rsCashBal.Open("SELECT * FROM CashBalTillTenVar WHERE TillID='" & strTillID & "' AND TradingPeriodID=" & mlngPeriodID & " AND PeriodID=" & mlngTranPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType, goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalTillTenVar (PeriodID, TradingPeriodID, TillID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngTranPeriodID & "," & mlngPeriodID & ",'" & strTillID & "','" & strCurr & "'," & oPaymentBO.TenderType & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalTillTenVar SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngTranPeriodID & " AND TradingPeriodID=" & mlngPeriodID & _
             "AND TillID='" & strTillID & "' AND CurrencyID='" & strCurr & "' AND ID=" & oPaymentBO.TenderType
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
    Next
    
    'Phil Referral
    If Format(Now, "DD/MM/YYYY") <> dteTempDate Then
        strSQL = "UPDATE DLTOTS SET CBBU='" & strCBBUDate & "' WHERE DATE1='" & Format(dteTempDate, "YYYY-MM-DD") & _
            "' AND TILL='" & strTillID & "' AND ""TRAN""='" & strTranNo & "'"
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
    End If
End Sub

Private Function UpdateValues(strTranCode As String, TranAmount As Currency, DiscAmount As Currency, MiscCode As Long, Voided As Boolean, LinesSold As Integer, LinesReversed As Integer, LinesScanned As Integer) As String

Dim strSQL As String
Dim strMiscCode As String

    If Voided = True Then
        UpdateValues = "NumVoids=NumVoids+1"
    Else
        strMiscCode = Format(MiscCode, "00")
        strSQL = "GrossSalesAmount=GrossSalesAmount+" & TranAmount & ","
        strSQL = strSQL & "DiscountAmount=DiscountAmount+" & DiscAmount & ","
        strSQL = strSQL & "NumLinesSold=NumLinesSold+" & LinesSold & ","
        strSQL = strSQL & "NumLinesReversed=NumLinesReversed+" & LinesReversed & ","
        strSQL = strSQL & "NumLinesScanned=NumLinesScanned+" & LinesScanned & ","
        
        Select Case (strTranCode)
            Case "SA", "RF", "M+", "M-": strSQL = strSQL & "NumTransactions=NumTransactions+1,"
            Case "SC", "RC", "C+", "C-": strSQL = strSQL & "NumCorrections=NumCorrections+1,"
        End Select
        
        Select Case (strTranCode)
            Case ("SA"): strSQL = strSQL & "SalesCount=SalesCount+1, SalesAmount=SalesAmount+" & TranAmount & ","
            Case ("SC"): strSQL = strSQL & "SalesCorrectCount=SalesCorrectCount+1, SalesCorrectAmount=SalesCorrectAmount+" & TranAmount & ","
            Case ("RF"): strSQL = strSQL & "RefundCount=RefundCount+1, RefundAmount=RefundAmount+" & TranAmount & ","
            Case ("RC"): strSQL = strSQL & "RefundCorrectCount=RefundCorrectCount+1, RefundCorrectAmount=RefundCorrectAmount+" & TranAmount & ","
            Case ("M+"): strSQL = strSQL & "MiscIncomeCount" & strMiscCode & "=MiscIncomeCount" & strMiscCode & "+1, MiscIncomeValue" & strMiscCode & "=MiscIncomeValue" & strMiscCode & "+" & TranAmount & ","
            Case ("C+"): strSQL = strSQL & "MiscIncomeCount" & strMiscCode & "=MiscIncomeCount" & strMiscCode & "-1, MiscIncomeValue" & strMiscCode & "=MiscIncomeValue" & strMiscCode & "-" & Abs(TranAmount) & ","
            Case ("M-"): strSQL = strSQL & "MisOutCount" & strMiscCode & "=MisOutCount" & strMiscCode & "+1, MisOutValue" & strMiscCode & "=MisOutValue" & strMiscCode & "+" & TranAmount & ","
            Case ("C-"): strSQL = strSQL & "MisOutCount" & strMiscCode & "=MisOutCount" & strMiscCode & "-1, MisOutValue" & strMiscCode & "=MisOutValue" & strMiscCode & "+" & Abs(TranAmount) & ","
            Case ("OD"): strSQL = strSQL & "NumOpenDrawer=NumOpenDrawer+1,"
        End Select
        UpdateValues = Left(strSQL, Len(strSQL) - 1)
    End If

End Function

Private Sub CheckCashBalCashier(PeriodID As Long, CurrencyCode As String, CashierID As String, DefaultCurrency As String)

Dim rsCashBal As New ADODB.Recordset
   
    Call rsCashBal.Open("SELECT * FROM CashBalCashier WHERE CashierID='" & CashierID & "' AND PeriodID=" & PeriodID & " AND CurrencyID='" & CurrencyCode & "'", goDatabase.Connection)
    If (rsCashBal.EOF = True) Then
        rsCashBal.Close
        Call CreateCashBalCashier(PeriodID, CurrencyCode, CashierID, DefaultCurrency)
    Else
        rsCashBal.Close
    End If

End Sub

Private Sub CreateCashBalCashier(PeriodID As Long, CurrencyCode As String, CashierID As String, DefaultCurrency As String)

Dim strSQL As String
Dim dblRate As Double
Dim dblPower As Double
Dim rsExRate As New ADODB.Recordset

    If CurrencyCode = DefaultCurrency Then
        dblRate = 0
        dblPower = 0
    Else
        Call rsExRate.Open("SELECT * FROM EXRATE WHERE CURR='" & CurrencyCode & "'", goDatabase.Connection)
        If (rsExRate.EOF = False) Then
            dblRate = rsExRate.Fields("RATE")
            dblPower = rsExRate.Fields("POWR")
        End If
        rsExRate.Close
    End If

    strSQL = "INSERT INTO CashBalCashier (PeriodID," & _
        "CurrencyID, CashierID, GrossSalesAmount, DiscountAmount, " & _
        "SalesCount, SalesAmount, SalesCorrectCount, SalesCorrectAmount, " & _
        "RefundCount, RefundAmount, RefundCorrectCount, RefundCorrectAmount, " & _
        "MiscIncomeCount01, MiscIncomeCount02, MiscIncomeCount03, MiscIncomeCount04, " & _
        "MiscIncomeCount05, MiscIncomeCount06, MiscIncomeCount07, MiscIncomeCount08, " & _
        "MiscIncomeCount09, MiscIncomeCount10, MiscIncomeCount11, MiscIncomeCount12, " & _
        "MiscIncomeCount13, MiscIncomeCount14, MiscIncomeCount15, MiscIncomeCount16, " & _
        "MiscIncomeCount17, MiscIncomeCount18, MiscIncomeCount19, MiscIncomeCount20, " & _
        "MiscIncomeValue01, MiscIncomeValue02, MiscIncomeValue03, MiscIncomeValue04, " & _
        "MiscIncomeValue05, MiscIncomeValue06, MiscIncomeValue07, MiscIncomeValue08, " & _
        "MiscIncomeValue09, MiscIncomeValue10, MiscIncomeValue11, MiscIncomeValue12, " & _
        "MiscIncomeValue13, MiscIncomeValue14, MiscIncomeValue15, MiscIncomeValue16, " & _
        "MiscIncomeValue17, MiscIncomeValue18, MiscIncomeValue19, MiscIncomeValue20, " & _
        "MisOutCount01, MisOutCount02, MisOutCount03, MisOutCount04, MisOutCount05, " & _
        "MisOutCount06, MisOutCount07, MisOutCount08, MisOutCount09, MisOutCount10, " & _
        "MisOutCount11, MisOutCount12, MisOutCount13, MisOutCount14, MisOutCount15, " & _
        "MisOutCount16, MisOutCount17, MisOutCount18, MisOutCount19, MisOutCount20, " & _
        "MisOutValue01, MisOutValue02, MisOutValue03,MisOutValue04, MisOutValue05, " & _
        "MisOutValue06, MisOutValue07, MisOutValue08, MisOutValue09, MisOutValue10, " & _
        "MisOutValue11, MisOutValue12, MisOutValue13, MisOutValue14, MisOutValue15, " & _
        "MisOutValue16, MisOutValue17, MisOutValue18, MisOutValue19, MisOutValue20, " & _
        "Comment, " & _
        "NumTransactions, NumCorrections, NumVoids, NumOpenDrawer, " & _
        "NumLinesReversed, NumLinesSold, NumLinesScanned, ExchangeRate, ExchangePower) VALUES ("
        
    strSQL = strSQL & PeriodID & ",'" & CurrencyCode & "','" & CashierID & "'," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "'', 0 ,0 ,0 ,0 ,0 ,0, 0, " & dblRate & ", " & dblPower & ")"
        
    Call DebugMsg(MODULE_NAME, "CheckCashBalCashier", endlDebug, "InsertUpdateCashier:" & strSQL)

    Call goDatabase.ExecuteCommand(strSQL)

End Sub

Private Sub CheckCashBalTill(PeriodID As Long, CurrencyCode As String, TillID As String, DefaultCurrency As String)

Dim rsCashBal As New ADODB.Recordset
    
    Call rsCashBal.Open("SELECT * FROM CashBalTill WHERE TillID='" & TillID & "' AND PeriodID=" & PeriodID & " AND CurrencyID='" & CurrencyCode & "'", goDatabase.Connection)
    If (rsCashBal.EOF = True) Then
        rsCashBal.Close
        Call CreateCashBalTill(PeriodID, CurrencyCode, TillID, DefaultCurrency)
    Else
        rsCashBal.Close
    End If

End Sub

Private Sub CreateCashBalTill(PeriodID As Long, CurrencyCode As String, TillID As String, DefaultCurrency As String)

Dim strSQL As String
Dim dblRate As Double
Dim dblPower As Double
Dim rsExRate As New ADODB.Recordset

    If CurrencyCode = DefaultCurrency Then
        dblRate = 0
        dblPower = 0
    Else
        Call rsExRate.Open("SELECT * FROM EXRATE WHERE CURR='" & CurrencyCode & "'", goDatabase.Connection)
        If (rsExRate.EOF = False) Then
            dblRate = rsExRate.Fields("RATE")
            dblPower = rsExRate.Fields("POWR")
        End If
        rsExRate.Close
    End If
    
    strSQL = "INSERT INTO CashBalTill (PeriodID," & _
        "CurrencyID, TillID, GrossSalesAmount, DiscountAmount, " & _
        "SalesCount, SalesAmount, SalesCorrectCount, SalesCorrectAmount, " & _
        "RefundCount, RefundAmount, RefundCorrectCount, RefundCorrectAmount, " & _
        "MiscIncomeCount01, MiscIncomeCount02, MiscIncomeCount03, MiscIncomeCount04, " & _
        "MiscIncomeCount05, MiscIncomeCount06, MiscIncomeCount07, MiscIncomeCount08, " & _
        "MiscIncomeCount09, MiscIncomeCount10, MiscIncomeCount11, MiscIncomeCount12, " & _
        "MiscIncomeCount13, MiscIncomeCount14, MiscIncomeCount15, MiscIncomeCount16, " & _
        "MiscIncomeCount17, MiscIncomeCount18, MiscIncomeCount19, MiscIncomeCount20, " & _
        "MiscIncomeValue01, MiscIncomeValue02, MiscIncomeValue03, MiscIncomeValue04, " & _
        "MiscIncomeValue05, MiscIncomeValue06, MiscIncomeValue07, MiscIncomeValue08, " & _
        "MiscIncomeValue09, MiscIncomeValue10, MiscIncomeValue11, MiscIncomeValue12, " & _
        "MiscIncomeValue13, MiscIncomeValue14, MiscIncomeValue15, MiscIncomeValue16, " & _
        "MiscIncomeValue17, MiscIncomeValue18, MiscIncomeValue19, MiscIncomeValue20, " & _
        "MisOutCount01, MisOutCount02, MisOutCount03, MisOutCount04, MisOutCount05, " & _
        "MisOutCount06, MisOutCount07, MisOutCount08, MisOutCount09, MisOutCount10, " & _
        "MisOutCount11, MisOutCount12, MisOutCount13, MisOutCount14, MisOutCount15, " & _
        "MisOutCount16, MisOutCount17, MisOutCount18, MisOutCount19, MisOutCount20, " & _
        "MisOutValue01, MisOutValue02, MisOutValue03,MisOutValue04, MisOutValue05, " & _
        "MisOutValue06, MisOutValue07, MisOutValue08, MisOutValue09, MisOutValue10, " & _
        "MisOutValue11, MisOutValue12, MisOutValue13, MisOutValue14, MisOutValue15, " & _
        "MisOutValue16, MisOutValue17, MisOutValue18, MisOutValue19, MisOutValue20, " & _
        "NumTransactions, NumCorrections, NumVoids, NumOpenDrawer, " & _
        "NumLinesReversed, NumLinesSold, NumLinesScanned, ExchangeRate, ExchangePower) VALUES ("
        
    strSQL = strSQL & PeriodID & ",'" & CurrencyCode & "','" & TillID & "'," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0," & _
        "0, 0, 0, 0, 0, 0, 0, " & dblRate & ", " & dblPower & ")"
        
    Call DebugMsg(MODULE_NAME, "CreateCashBalTill", endlDebug, "InsertUpdateTill:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)

End Sub

Private Sub DecodeVisionSales(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strTillID    As String
Dim strTranNo    As String
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

Dim strSQL              As String
Dim strDefaultCurr      As String
Dim strUser             As String

Dim strCurr     As String
Dim dblTender   As Double
Dim curDisc     As Currency

Dim LinesReversed   As Integer
Dim LinesScanned    As Integer
Dim LinesSold       As Integer

Dim rsSale      As New ADODB.Recordset
Dim rsPayment   As New ADODB.Recordset
Dim rsLine      As New ADODB.Recordset
Dim rsCashBal   As New ADODB.Recordset

Dim strCBBUDate As String

    vntSection = Split(strCommand, ",")
    
    If (Left(strCommand, 1) <> "V") Then End
    
    strTempDate = Mid$(strCommand, 2, 8)
    dteTempDate = GetParamDate(strTempDate)
    strTillID = Mid$(strCommand, 10, 2)
    strTranNo = Mid$(strCommand, 12, 4)
    
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
   
    strSQL = "SELECT * FROM VisionTotal WHERE TRANDATE='" & Format(dteTempDate, "YYYY-MM-DD") & _
                        "' AND TILLID='" & strTillID & "' AND " & Chr(34) & "TRANNUMBER" & Chr(34) & "='" & strTranNo & "'"
    Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "Read VisionTotal:" & strSQL)
    Call rsSale.Open(strSQL, goDatabase.Connection)
    
 
    If rsSale.EOF Then
        MsgBox ("Vision Sale not found")
        Call MsgBox("Unable to retrieve Vision Transaction " & dteTempDate & " - " & strTillID & ":" & strTranNo, vbCritical, "Unable to Process Transaction")
        End
    End If
    
    strUser = rsSale.Fields("CashierId")
    strTillID = rsSale.Fields("TillId")
    
    'Force loading of Lines & payments
    Call rsLine.Open("SELECT * FROM VISIONLINE WHERE TRANDATE='" & Format(dteTempDate, "YYYY-MM-DD") & _
                        "' AND TILLID='" & strTillID & "' AND " & Chr(34) & "TRANNUMBER" & Chr(34) & "='" & strTranNo & "'", goDatabase.Connection)
    
    Call rsPayment.Open("SELECT * FROM VISIONPAYMENT WHERE TRANDATE='" & Format(dteTempDate, "YYYY-MM-DD") & _
                        "' AND TILLID='" & strTillID & "' AND " & Chr(34) & "TRANNUMBER" & Chr(34) & "='" & strTranNo & "'", goDatabase.Connection)
    
    'Get Period to record sale against
    'dteTempDate = Now
    'Get Period to record sale against
    mlngTranPeriodID = GetPeriodID(dteTempDate, strCBBUDate, True)
    mlngPeriodID = GetPeriodID(Now(), strCBBUDate, False)
    
    Call rsCashBal.Open("SELECT ID FROM SystemCurrency WHERE IsDefault=1", goDatabase.Connection)
    If (rsCashBal.EOF = False) Then
        strDefaultCurr = Trim$(rsCashBal.Fields("ID"))
    End If
    Call rsCashBal.Close
       
    'Retrieve existing Cashiers and Till Control records exist and create if missing
    Call CheckCashBalCashier(mlngPeriodID, strDefaultCurr, strUser, strDefaultCurr)
    Call CheckCashBalTill(mlngPeriodID, strDefaultCurr, strTillID, strDefaultCurr)
    
    'loop through each line and count lines sold, scanned, reversed
    LinesReversed = 0
    LinesScanned = 0
    LinesSold = 0
    While rsLine.EOF = False
        If rsLine.Fields("SaleType") <> "W" Then
            LinesSold = LinesSold + 1
        End If
        rsLine.MoveNext
    Wend
    
    curDisc = 0
    
    strSQL = "UPDATE CashBalTill SET " & UpdateValues(rsSale.Fields("Type"), Val(rsSale.Fields("Value")), curDisc, Val(rsSale.Fields("ReasonCode")), False, LinesSold, LinesReversed, LinesScanned)
    strSQL = strSQL & " WHERE TillID='" & strTillID & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strDefaultCurr & "'"
    Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalTill:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)

    strSQL = "UPDATE CashBalCashier SET " & UpdateValues(rsSale.Fields("Type"), Val(rsSale.Fields("Value")), curDisc, Val(rsSale.Fields("ReasonCode")), False, LinesSold, LinesReversed, LinesScanned)
    strSQL = strSQL & " WHERE CashierID='" & strUser & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strDefaultCurr & "'"
    Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalCashier:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)

    'Step through each payment and save details
    While rsPayment.EOF = False
        'Check whether payments is in default currency
        strCurr = strDefaultCurr
        ' 19/11/2010
        ' Alan Lewis
        ' Live Woking and Salisbury issue - ValueTender in VisionPayment is -ve for Sale, +ve for refund, so reverse for
        ' adding to cashier and till amounts (i.e. * -1)
        dblTender = Val(rsPayment.Fields("ValueTender")) * -1
        
        'Retrieve existing Cashiers header record
        Call rsCashBal.Open("SELECT * FROM CashBalCashierTen WHERE CashierID='" & strUser & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalCashierTen (PeriodID, CashierID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngPeriodID & ",'" & strUser & "','" & strCurr & "'," & rsPayment.Fields("TenderTypeId") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalCashierTen SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngPeriodID & " AND " & _
             "CashierID='" & strUser & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId")
        Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        
        'Retrieve existing Till header record
        Call rsCashBal.Open("SELECT * FROM CashBalTillTen WHERE TillID='" & strTillID & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalTillTen (PeriodID, TillID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngPeriodID & ",'" & strTillID & "','" & strCurr & "'," & rsPayment.Fields("TenderTypeId") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalTillTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalTillTen SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngPeriodID & " AND " & _
             "TillID='" & strTillID & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId")
        Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalTillTen: " & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        'Added 26/9/09 WIX1377 Update Tenders into Variance Records
        'Retrieve existing Cashiers Variance tender records
        Call rsCashBal.Open("SELECT * FROM CashBalCashierTenVar WHERE CashierID='" & strUser & "' AND TradingPeriodID=" & mlngPeriodID & " AND PeriodID=" & mlngTranPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalCashierTenVar (PeriodID, TradingPeriodID, CashierID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngTranPeriodID & "," & mlngPeriodID & ",'" & strUser & "','" & strCurr & "'," & rsPayment.Fields("TenderTypeId") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "InsertCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalCashierTenVar SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngTranPeriodID & " AND TradingPeriodID=" & mlngPeriodID & _
             " AND CashierID='" & strUser & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId")
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        
        'Retrieve existing Till tender records
        Call rsCashBal.Open("SELECT * FROM CashBalTillTenVar WHERE TillID='" & strTillID & "' AND TradingPeriodID=" & mlngPeriodID & " AND PeriodID=" & mlngTranPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalTillTenVar (PeriodID, TradingPeriodID, TillID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngTranPeriodID & "," & mlngPeriodID & ",'" & strTillID & "','" & strCurr & "'," & rsPayment.Fields("TenderTypeId") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalTillTenVar SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngTranPeriodID & " AND TradingPeriodID=" & mlngPeriodID & _
             "AND TillID='" & strTillID & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TenderTypeId")
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        rsPayment.MoveNext
    
    Wend

End Sub

Private Sub DecodeVisionSalesPervasive(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strTillID    As String
Dim strTranNo    As String
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

Dim strSQL              As String
Dim strDefaultCurr      As String
Dim strUser             As String

Dim strCurr     As String
Dim dblTender   As Double
Dim curDisc     As Currency

Dim LinesReversed   As Integer
Dim LinesScanned    As Integer
Dim LinesSold       As Integer

Dim rsSale      As New ADODB.Recordset
Dim rsPayment   As New ADODB.Recordset
Dim rsLine      As New ADODB.Recordset
Dim rsCashBal   As New ADODB.Recordset

Dim strCBBUDate As String

    vntSection = Split(strCommand, ",")
    
    If (Left(strCommand, 1) <> "V") Then End
    
    strTempDate = Mid$(strCommand, 2, 8)
    dteTempDate = GetParamDate(strTempDate)
    strTillID = Mid$(strCommand, 10, 2)
    strTranNo = Mid$(strCommand, 12, 4)
    
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
   
    strSQL = "SELECT * FROM PVTOTS WHERE DATE1='" & Format(dteTempDate, "DD/MM/YY") & _
                        "' AND TILL='" & strTillID & "' AND " & Chr(34) & "TRAN" & Chr(34) & "='" & strTranNo & "'"
    Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "Read PVTOTS:" & strSQL)
    Call rsSale.Open(strSQL, goDatabase.Connection)
    
 
    If rsSale.EOF Then
        MsgBox ("Vision Sale not found")
        Call MsgBox("Unable to retrieve Vision Transaction " & dteTempDate & " - " & strTillID & ":" & strTranNo, vbCritical, "Unable to Process Transaction")
        End
    End If
    
    strUser = rsSale.Fields("Cash")
    strTillID = rsSale.Fields("Till")
    
    'Force loading of Lines & payments
    Call rsLine.Open("SELECT * FROM PVLINE WHERE DATE1='" & Format(dteTempDate, "DD/MM/YY") & _
                        "' AND TILL='" & strTillID & "' AND " & Chr(34) & "TRAN" & Chr(34) & "='" & strTranNo & "'", goDatabase.Connection)
    
    Call rsPayment.Open("SELECT * FROM PVPAID WHERE DATE1='" & Format(dteTempDate, "DD/MM/YY") & _
                        "' AND TILL='" & strTillID & "' AND " & Chr(34) & "TRAN" & Chr(34) & "='" & strTranNo & "'", goDatabase.Connection)
    
    'Get Period to record sale against
    'dteTempDate = Now
    'Get Period to record sale against
    mlngTranPeriodID = GetPeriodID(dteTempDate, strCBBUDate, True)
    mlngPeriodID = GetPeriodID(Now(), strCBBUDate, False)
    
    Call rsCashBal.Open("SELECT ID FROM SystemCurrency WHERE IsDefault=1", goDatabase.Connection)
    If (rsCashBal.EOF = False) Then
        strDefaultCurr = Trim$(rsCashBal.Fields("ID"))
    End If
    Call rsCashBal.Close
       
    'Retrieve existing Cashiers and Till Control records exist and create if missing
    Call CheckCashBalCashier(mlngPeriodID, strDefaultCurr, strUser, strDefaultCurr)
    Call CheckCashBalTill(mlngPeriodID, strDefaultCurr, strTillID, strDefaultCurr)
    
    'loop through each line and count lines sold, scanned, reversed
    LinesReversed = 0
    LinesScanned = 0
    LinesSold = 0
    While rsLine.EOF = False
        If rsLine.Fields("SALT") <> "W" Then
            LinesSold = LinesSold + 1
        End If
        rsLine.MoveNext
    Wend
    
    curDisc = 0
    
    strSQL = "UPDATE CashBalTill SET " & UpdateValues(rsSale.Fields("TCOD"), Val(rsSale.Fields("TOTL")) * IIf(rsSale.Fields("TCOD") = "RF", -1, 1), curDisc, Val(rsSale.Fields("MISC")), False, LinesSold, LinesReversed, LinesScanned)
    strSQL = strSQL & " WHERE TillID='" & strTillID & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strDefaultCurr & "'"
    Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalTill:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)

    strSQL = "UPDATE CashBalCashier SET " & UpdateValues(rsSale.Fields("TCOD"), Val(rsSale.Fields("TOTL")) * IIf(rsSale.Fields("TCOD") = "RF", -1, 1), curDisc, Val(rsSale.Fields("MISC")), False, LinesSold, LinesReversed, LinesScanned)
    strSQL = strSQL & " WHERE CashierID='" & strUser & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strDefaultCurr & "'"
    Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalCashier:" & strSQL)
    Call goDatabase.ExecuteCommand(strSQL)

    'Step through each payment and save details
    While rsPayment.EOF = False
        'Check whether payments is in default currency
        strCurr = strDefaultCurr
        If InStr(rsPayment.Fields("AMNT"), "-") > 0 Then
           dblTender = Val(rsPayment.Fields("AMNT"))
        Else
           dblTender = Val(rsPayment.Fields("AMNT")) * -1
        End If
        
        'Retrieve existing Cashiers header record
        Call rsCashBal.Open("SELECT * FROM CashBalCashierTen WHERE CashierID='" & strUser & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & Val(rsPayment.Fields("TYPE")), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalCashierTen (PeriodID, CashierID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngPeriodID & ",'" & strUser & "','" & strCurr & "'," & rsPayment.Fields("TYPE") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalCashierTen SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngPeriodID & " AND " & _
             "CashierID='" & strUser & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE")
        Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        
        'Retrieve existing Till header record
        Call rsCashBal.Open("SELECT * FROM CashBalTillTen WHERE TillID='" & strTillID & "' AND PeriodID=" & mlngPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalTillTen (PeriodID, TillID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngPeriodID & ",'" & strTillID & "','" & strCurr & "'," & rsPayment.Fields("TYPE") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalTillTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalTillTen SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngPeriodID & " AND " & _
             "TillID='" & strTillID & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE")
        Call DebugMsg(MODULE_NAME, "DecodeVisionSales", endlDebug, "CashBalTillTen: " & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        'Added 26/9/09 WIX1377 Update Tenders into Variance Records
        'Retrieve existing Cashiers Variance tender records
        Call rsCashBal.Open("SELECT * FROM CashBalCashierTenVar WHERE CashierID='" & strUser & "' AND TradingPeriodID=" & mlngPeriodID & " AND PeriodID=" & mlngTranPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalCashierTenVar (PeriodID, TradingPeriodID, CashierID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngTranPeriodID & "," & mlngPeriodID & ",'" & strUser & "','" & strCurr & "'," & rsPayment.Fields("TYPE") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "InsertCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalCashierTenVar SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngTranPeriodID & " AND TradingPeriodID=" & mlngPeriodID & _
             " AND CashierID='" & strUser & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE")
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        
        
        'Retrieve existing Till tender records
        Call rsCashBal.Open("SELECT * FROM CashBalTillTenVar WHERE TillID='" & strTillID & "' AND TradingPeriodID=" & mlngPeriodID & " AND PeriodID=" & mlngTranPeriodID & " AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE"), goDatabase.Connection)
        If (rsCashBal.EOF = True) Then 'create as does not exist
            rsCashBal.Close
            strSQL = "INSERT INTO CashBalTillTenVar (PeriodID, TradingPeriodID, TillID, CurrencyID, ID, Quantity, Amount, PickUp) VALUES ("
            strSQL = strSQL & mlngTranPeriodID & "," & mlngPeriodID & ",'" & strTillID & "','" & strCurr & "'," & rsPayment.Fields("TYPE") & ",0,0,0)"
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
            Call goDatabase.ExecuteCommand(strSQL)
        Else
            rsCashBal.Close
        End If
        
        strSQL = "UPDATE CashBalTillTenVar SET Quantity=Quantity+1, Amount=Amount+"
        strSQL = strSQL & dblTender & " WHERE PeriodID=" & mlngTranPeriodID & " AND TradingPeriodID=" & mlngPeriodID & _
             "AND TillID='" & strTillID & "' AND CurrencyID='" & strCurr & "' AND ID=" & rsPayment.Fields("TYPE")
        Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "UpdateCashierTen:" & strSQL)
        Call goDatabase.ExecuteCommand(strSQL)
        rsPayment.MoveNext
    
    Wend

End Sub

Private Function GetPeriodID(ByVal FindDate As Date, ByRef strCBBUDate As String, ByVal IgnoreClosed As Boolean) As Long

Dim rsCashBal As New ADODB.Recordset
Dim rsPeriods As New ADODB.Recordset
Dim strSQL    As String
    
    'Get Safe for Transaction date
    Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Getting Safe for PeriodDate '" & Format(FindDate, "YYYY-MM-DD") & "'")
    Call rsCashBal.Open("SELECT * FROM Safe WHERE PeriodDate = '" & Format(FindDate, "YYYY-MM-DD") & "'", goDatabase.Connection)
    If rsCashBal.EOF Then 'not found so find Period ID and create Safe Entry
        rsCashBal.Close
        Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Safe Not Found so get SystemPeriod '" & Format(FindDate, "YYYY-MM-DD") & "'")
        Call rsPeriods.Open("SELECT ID,StartDate FROM SystemPeriods WHERE StartDate >= '" & Format(FindDate, "YYYY-MM-DD") & " 00:00:00' AND EndDate <= '" & Format(FindDate, "YYYY-MM-DD") & " 23:59:59'", goDatabase.Connection)
        If (rsPeriods.EOF = True) Then 'No Period Record so Raise Error
            rsPeriods.Close
            MsgBox ("no System Period(s) found - unable to proceed.")
            End
        End If
        'Insert Safe Entry for Transaction Date and re-select
        Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Inserting Safe for PeriodDate '" & rsPeriods("ID") & "," & Format(Now, "YYYY-MM-DD") & "'")
        Call goDatabase.ExecuteCommand("exec dbo.CreateSafeForNonTradingDay @PeriodDate = '" & Format(rsPeriods("StartDate"), "YYYY-MM-DD") & "'")
        Call goDatabase.ExecuteCommand("INSERT INTO Safe (PeriodID, PeriodDate,UserID1, UserID2, LastAmended, IsClosed) VALUES (" & rsPeriods("ID") & ",'" & Format(rsPeriods("StartDate"), "YYYY-MM-DD") & "',0,0,'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "',0)")
        Call rsCashBal.Open("SELECT * FROM Safe WHERE PeriodDate = '" & Format(FindDate, "YYYY-MM-DD") & "'", goDatabase.Connection)
    End If 'no Safe Entry Found
    
    'added 28/7/09 - ensure that SafeDenom record exists for Safe entry, and create if missing
    Call CheckSafeDenoms(rsCashBal("PeriodID"))
    
    Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "SELECT * FROM Safe WHERE IsClosed=0 AND PeriodDate = '" & Format(Now, "YYYY-MM-DD") & "'")
    If (rsCashBal.Fields("IsClosed").Value = True) And (IgnoreClosed = False) Then
        'Added 27/06/09 - If System Period Closed then put into today
        rsCashBal.Close
        Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Period Closed so get Next Open Safe from Today")
        Call rsCashBal.Open("SELECT TOP 1 * FROM Safe WHERE IsClosed=0 AND PeriodDate > '" & Format(Now, "YYYY-MM-DD") & "' ORDER BY PeriodID", goDatabase.Connection)
        If rsCashBal.EOF Then 'No Open Safe entries found so get next one and create next available
            rsCashBal.Close
            'Get Period for Today
            Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Period Closed so get Next System Periods from Today")
            Call rsPeriods.Open("SELECT TOP 1 ID,StartDate FROM SystemPeriods WHERE StartDate > '" & Format(Now, "YYYY-MM-DD") & "' ORDER BY ID", goDatabase.Connection)
            If (rsPeriods.EOF = True) Then 'No Period Record so Raise Error
                rsPeriods.Close
                MsgBox ("no System Period(s) found past today - unable to proceed.")
                End
            End If
            Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Get Safe for Period:" & rsPeriods("ID"))
            Call rsCashBal.Open("SELECT * FROM Safe WHERE PeriodID = " & rsPeriods("ID"), goDatabase.Connection)
            If rsCashBal.EOF Then
            'Insert Safe Entry for Transaction Date and re-select
                rsCashBal.Close
                Call goDatabase.ExecuteCommand("INSERT INTO Safe (PeriodID, PeriodDate,UserID1, UserID2, LastAmended, IsClosed) VALUES (" & rsPeriods("ID") & ",'" & Format(rsPeriods("StartDate"), "YYYY-MM-DD") & "',0,0,'" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "',0)")
                Call rsCashBal.Open("SELECT * FROM Safe WHERE PeriodID = " & rsPeriods("ID"), goDatabase.Connection)
            End If
            Call DebugMsg(MODULE_NAME, "DecodeParameters", endlDebug, "SELECT TOP 1 * FROM Safe WHERE IsClosed=0 AND PeriodDate >= '" & Format(Now, "YYYY-MM-DD") & "' ORDER BY PeriodID")
            'added 28/7/09 - ensure that SafeDenom record exists for Safe entry, and create if missing
            Call CheckSafeDenoms(rsPeriods("ID"))
        End If 'no forward periods can be found
    End If
    GetPeriodID = rsCashBal.Fields("PeriodID")
    strCBBUDate = Format(rsCashBal.Fields("PeriodDate"), "DD/MM/YY")
    Call rsCashBal.Close
    Call DebugMsg(MODULE_NAME, "GetPeriodID", endlDebug, "Period:" & GetPeriodID & " for date " & strCBBUDate)

End Function

Public Sub CheckSafeDenoms(PeriodID As Long)

Dim rsSafeDenom As New ADODB.Recordset
Dim lngCopyPeriodID As Long

    Call DebugMsg(MODULE_NAME, "CheckSafeDenoms", endlDebug, "Checking SafeDenoms for PeriodID:" & PeriodID)
    'Added 28/7/09 - Create SafeDenoms if not exists
    Call rsSafeDenom.Open("SELECT TOP 1 PeriodID FROM SafeDenoms WHERE PeriodID <= " & PeriodID & " ORDER BY PeriodID DESC", goDatabase.Connection)
    If rsSafeDenom.EOF = False Then 'not found so create starting Safe Denom entry
        lngCopyPeriodID = rsSafeDenom("PeriodID")
        If (PeriodID <> lngCopyPeriodID) Then 'Denoms already exist for period so do not need to create
            rsSafeDenom.Close
            Call DebugMsg(MODULE_NAME, "CheckSafeDenoms", endlDebug, "Checking SafeDenoms from Source PeriodID:" & lngCopyPeriodID)
            Call rsSafeDenom.Open("SELECT * FROM SafeDenoms WHERE PeriodID = " & lngCopyPeriodID, goDatabase.Connection)
            While rsSafeDenom.EOF = False
                Call goDatabase.ExecuteCommand("INSERT INTO SafeDenoms (PeriodID, CurrencyID, TenderID, ID, SafeValue, ChangeValue, SystemValue, SuggestedValue) VALUES (" _
                    & PeriodID & ",'" & rsSafeDenom("CurrencyID") & "', " & rsSafeDenom("TenderID") & "," & rsSafeDenom("ID") & "," & rsSafeDenom("SafeValue") & "," _
                    & rsSafeDenom("ChangeValue") & "," & rsSafeDenom("SystemValue") & ",0)")
                rsSafeDenom.MoveNext
            Wend
        End If
    End If 'no Safe Entry Found
    rsSafeDenom.Close
    
End Sub

