VERSION 5.00
Begin VB.UserControl ucDateText 
   ClientHeight    =   285
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2730
   ScaleHeight     =   285
   ScaleWidth      =   2730
   ToolboxBitmap   =   "EditDate.ctx":0000
   Begin VB.TextBox txtEditDate 
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Text            =   "dText1"
      Top             =   0
      Width           =   2415
   End
End
Attribute VB_Name = "ucDateText"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' 24/04/04 KVB  Version 1.0.11 Fix delete key problems.

Option Explicit

'Default Property Values:
Const m_def_DateFormat = "dd/mm/yy"
'Property Variables:
Dim m_DateFormat As String
'Event Declarations:
Event Change() 'MappingInfo=txtEditDate,txtEditDate,-1,Change
Event Click() 'MappingInfo=txtEditDate,txtEditDate,-1,Click
Event DblClick() 'MappingInfo=txtEditDate,txtEditDate,-1,DblClick
Event KeyDown(KeyCode As Integer, Shift As Integer) 'MappingInfo=txtEditDate,txtEditDate,-1,KeyDown
Event KeyPress(KeyAscii As Integer) 'MappingInfo=txtEditDate,txtEditDate,-1,KeyPress
Event KeyUp(KeyCode As Integer, Shift As Integer) 'MappingInfo=txtEditDate,txtEditDate,-1,KeyUp
Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=txtEditDate,txtEditDate,-1,MouseDown
Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=txtEditDate,txtEditDate,-1,MouseMove
Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=txtEditDate,txtEditDate,-1,MouseUp

Private mlngMonSeparator  As Long
Private mlngYearSeparator As Long
Private mlngMonthPos1     As Long
Private mlngMonthPos2     As Long
Private mlngDayPos1       As Long
Private mlngDayPos2       As Long
Private mstrBlankDate     As String
    


'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditDate,txtEditDate,-1,BackColor
Public Property Get BackColor() As OLE_COLOR
Attribute BackColor.VB_Description = "Returns/sets the background color used to display text and graphics in an object."
    BackColor = txtEditDate.BackColor
End Property

Public Property Let BackColor(ByVal New_BackColor As OLE_COLOR)
    txtEditDate.BackColor() = New_BackColor
    PropertyChanged "BackColor"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditDate,txtEditDate,-1,ForeColor
Public Property Get ForeColor() As OLE_COLOR
Attribute ForeColor.VB_Description = "Returns/sets the foreground color used to display text and graphics in an object."
    ForeColor = txtEditDate.ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As OLE_COLOR)
    txtEditDate.ForeColor() = New_ForeColor
    PropertyChanged "ForeColor"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditDate,txtEditDate,-1,Enabled
Public Property Get Enabled() As Boolean
Attribute Enabled.VB_Description = "Returns/sets a value that determines whether an object can respond to user-generated events."
    Enabled = txtEditDate.Enabled
End Property

Public Property Let Enabled(ByVal New_Enabled As Boolean)
    txtEditDate.Enabled() = New_Enabled
    UserControl.Enabled = New_Enabled
    PropertyChanged "Enabled"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditDate,txtEditDate,-1,Font
Public Property Get Font() As Font
Attribute Font.VB_Description = "Returns a Font object."
Attribute Font.VB_UserMemId = -512
    Set Font = txtEditDate.Font
End Property

Public Property Set Font(ByVal New_Font As Font)
    Set txtEditDate.Font = New_Font
    PropertyChanged "Font"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=UserControl,UserControl,-1,Refresh
Public Sub Refresh()
Attribute Refresh.VB_Description = "Forces a complete repaint of a object."
    UserControl.Refresh
End Sub

Private Sub txtEditDate_Change()
    RaiseEvent Change
End Sub

Private Sub txtEditDate_Click()
    RaiseEvent Click
End Sub

Private Sub txtEditDate_DblClick()
    RaiseEvent DblClick
End Sub

Private Sub txtEditDate_GotFocus()

    txtEditDate.SelStart = 0

End Sub

Private Sub txtEditDate_KeyDown(KeyCode As Integer, Shift As Integer)

    ' 24/04/03 kvb Prevent use of delete key (vbKeyDelete - KeyCode 46)
    If KeyCode = vbKeyDelete Then
        KeyCode = 0
    End If

    RaiseEvent KeyDown(KeyCode, Shift)
    If Shift = 0 Then
        If KeyCode = 39 Then 'Right Arrow
            If txtEditDate.SelStart + 2 = mlngMonSeparator Then txtEditDate.SelStart = txtEditDate.SelStart + 1
            If txtEditDate.SelStart + 2 = mlngYearSeparator Then txtEditDate.SelStart = txtEditDate.SelStart + 1
        End If
        If KeyCode = 37 Then 'Left Arrow
            If txtEditDate.SelStart = mlngMonSeparator Then txtEditDate.SelStart = txtEditDate.SelStart - 1
            If txtEditDate.SelStart = mlngYearSeparator Then txtEditDate.SelStart = txtEditDate.SelStart - 1
        End If
        If KeyCode = vbKeyF8 Then
            txtEditDate.Text = mstrBlankDate
        End If
    End If
    
End Sub

Private Sub txtEditDate_KeyPress(KeyAscii As Integer)

Dim strLeft    As String
Dim strRight   As String
Dim lngEditPos As Long

    If KeyAscii <> 13 And KeyAscii <> 9 And KeyAscii <> 27 And KeyAscii <> 8 And (KeyAscii < 48 Or KeyAscii > 57) Then
        KeyAscii = 0
    Else
        
        ' 24/04/03 kvb Prevent use of delete back key (vbKeyBack - KeyAscii 8)
        '              when at very start of input.
        If KeyAscii = vbKeyBack And txtEditDate.SelStart = 0 Then
            KeyAscii = 0
        End If
        
        If KeyAscii = vbKeyBack And txtEditDate.SelStart = mlngMonSeparator Then
            txtEditDate.SelStart = mlngMonSeparator - 1
        End If
        If KeyAscii = vbKeyBack Then
            If txtEditDate.SelStart = mlngMonSeparator Then txtEditDate.SelStart = txtEditDate.SelStart - 1
            If txtEditDate.SelStart = mlngYearSeparator Then txtEditDate.SelStart = txtEditDate.SelStart - 1
            If txtEditDate.SelStart >= 1 Then strLeft = Left$(txtEditDate.Text, txtEditDate.SelStart - 1)
            txtEditDate.Text = strLeft & "0" & Mid$(txtEditDate.Text, txtEditDate.SelStart + 1)
            txtEditDate.SelStart = Len(strLeft)
            KeyAscii = 0
        End If
        If KeyAscii >= 48 And KeyAscii <= 57 Then 'process keys 0-9
            'check bounds of entered field
            'Force part1 of day to be a 0,1,2 or a 3
            If (txtEditDate.SelStart = mlngDayPos1) And (KeyAscii > 51) Then KeyAscii = 48
            
            'Force part1 of month to be a 0 if greater than 1 entered
            If (txtEditDate.SelStart = mlngMonthPos1) And (KeyAscii > 49) Then KeyAscii = 48
            'if part1 of month entered as a 1, then force part2 = 0 if part2 > 2
            If (txtEditDate.SelStart = mlngMonthPos1) And (KeyAscii = 49) And Val(Mid$(txtEditDate.Text, mlngMonthPos1 + 2, 1)) > 2 Then
                txtEditDate.Text = Left$(txtEditDate.Text, mlngMonthPos1 + 1) & "0" & Mid$(txtEditDate.Text, mlngMonthPos1 + 3)
                txtEditDate.SelStart = mlngMonthPos1
            End If
            
            'Check part2 of month does not accept > 2 if part1 = 1 i.e. month is 10+
            If (txtEditDate.SelStart = mlngMonthPos2) And (KeyAscii > 50) Then
                If Val(Mid$(txtEditDate.Text, mlngMonthPos1 + 1, 1)) = 1 Then
                    KeyAscii = 0
                    Exit Sub
                End If
            End If
            
            'if part1 of day entered as a 3, then force part2 = 0 if part2 > 1
            If (txtEditDate.SelStart = mlngDayPos1) And (KeyAscii = 51) And Val(Mid$(txtEditDate.Text, mlngDayPos1 + 2, 1)) > 1 Then
                txtEditDate.Text = Left$(txtEditDate.Text, mlngDayPos1 + 1) & "0" & Mid$(txtEditDate.Text, mlngDayPos1 + 3)
                txtEditDate.SelStart = mlngDayPos1
            End If
            
            'Check part2 of day does not accept > 1 if part1 = 3 i.e. day is 30+
            If (txtEditDate.SelStart = mlngDayPos2) And (KeyAscii > 49) Then
                If Val(Mid$(txtEditDate.Text, mlngDayPos1 + 1, 1)) = 3 Then
                    KeyAscii = 0
                    Exit Sub
                End If
            End If
            
            'And txtEditDate.SelStart = mlngMonSeparator
            If txtEditDate.SelStart >= 1 Then strLeft = Left$(txtEditDate.Text, txtEditDate.SelStart)
            txtEditDate.Text = strLeft & Chr(KeyAscii) & Mid$(txtEditDate.Text, txtEditDate.SelStart + 2)
            txtEditDate.SelStart = Len(strLeft) + 1
            If txtEditDate.SelStart + 1 = mlngMonSeparator Then txtEditDate.SelStart = txtEditDate.SelStart + 1
            If txtEditDate.SelStart + 1 = mlngYearSeparator Then txtEditDate.SelStart = txtEditDate.SelStart + 1
            KeyAscii = 0
            
            'Catch if month = 00 then set to 01
            If (Mid$(txtEditDate.Text, mlngMonthPos1 + 1, 2) = "00") Then
                lngEditPos = txtEditDate.SelStart
                txtEditDate.Text = Left$(txtEditDate.Text, mlngMonthPos1 + 1) & "1" & Mid$(txtEditDate.Text, mlngMonthPos1 + 3)
                txtEditDate.SelStart = lngEditPos
            End If
            
            'Catch if day = 00 then set to 01
            If (Mid$(txtEditDate.Text, mlngDayPos1 + 1, 2) = "00") Then
                lngEditPos = txtEditDate.SelStart
                txtEditDate.Text = Left$(txtEditDate.Text, mlngDayPos1 + 1) & "1" & Mid$(txtEditDate.Text, mlngDayPos1 + 3)
                txtEditDate.SelStart = lngEditPos
            End If
            'txtEditDate.SelStart = mlngMonSeparator + 1
        End If
        RaiseEvent KeyPress(KeyAscii) 'pass key back to container form
    End If 'valid key pressed
    
End Sub

Private Sub txtEditDate_KeyUp(KeyCode As Integer, Shift As Integer)
    
    ' 24/04/03 kvb Prevent use of delete key (vbKeyDelete - KeyCode 46)
    If KeyCode = vbKeyDelete Then
        KeyCode = 0
    End If

    RaiseEvent KeyUp(KeyCode, Shift)
End Sub

Private Sub txtEditDate_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub txtEditDate_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub txtEditDate_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,dd/mm/yy
Public Property Get DateFormat() As String
Attribute DateFormat.VB_Description = "Supplied date format used to drive control to enforce that only valid dates are captured"
    DateFormat = m_DateFormat
End Property

Public Property Let DateFormat(ByVal New_DateFormat As String)
    
    m_DateFormat = UCase$(New_DateFormat)
    
    If InStr(m_DateFormat, "DD") = 0 And InStr(m_DateFormat, "D") > 0 Then m_DateFormat = Left$(m_DateFormat, InStr(m_DateFormat, "D")) & Mid$(m_DateFormat, InStr(m_DateFormat, "D"))
    If InStr(m_DateFormat, "DD") = 0 Then m_DateFormat = "DD/MM/YY"
    
    If InStr(m_DateFormat, "MM") = 0 And InStr(m_DateFormat, "M") > 0 Then m_DateFormat = Left$(m_DateFormat, InStr(m_DateFormat, "M")) & Mid$(m_DateFormat, InStr(m_DateFormat, "M"))
    If InStr(m_DateFormat, "MM") = 0 Then m_DateFormat = "DD/MM/YY"
    
    If InStr(m_DateFormat, "YY") = 0 And InStr(m_DateFormat, "Y") > 0 Then m_DateFormat = Left$(m_DateFormat, InStr(m_DateFormat, "Y")) & Mid$(m_DateFormat, InStr(m_DateFormat, "Y"))
    If InStr(m_DateFormat, "YY") = 0 Then m_DateFormat = "DD/MM/YY"
    
    mlngDayPos1 = InStr(m_DateFormat, "D") - 1
    mlngDayPos2 = mlngDayPos1 + 1
    
    mlngMonthPos1 = InStr(m_DateFormat, "M") - 1
    mlngMonthPos2 = mlngMonthPos1 + 1
    
    mlngMonSeparator = InStr(m_DateFormat, "/")
    mlngYearSeparator = InStr(mlngMonSeparator + 1, m_DateFormat, "/")
    txtEditDate.Text = Format(txtEditDate.Text, m_DateFormat)
    'Set-Up the default blank date as --/--/-- to display when F8 pressed
    mstrBlankDate = Replace(m_DateFormat, "Y", "-")
    mstrBlankDate = Replace(mstrBlankDate, "M", "-")
    mstrBlankDate = Replace(mstrBlankDate, "D", "-")
    PropertyChanged "DateFormat"
    
End Property
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MemberInfo=3,0,2,0
'Public Property Get DateValue() As Date
'    DateValue = m_DateValue
'End Property
'

'Initialize Properties for User Control
Private Sub UserControl_InitProperties()
    DateFormat = m_def_DateFormat 'force local vars setting
'    m_DateValue = m_def_DateValue
'    txtEditDate.Text = Format(m_DateValue, m_DateFormat)
End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    txtEditDate.BackColor = PropBag.ReadProperty("BackColor", &H80000005)
    txtEditDate.ForeColor = PropBag.ReadProperty("ForeColor", &H80000008)
    txtEditDate.Enabled = PropBag.ReadProperty("Enabled", True)
    Set txtEditDate.Font = PropBag.ReadProperty("Font", Ambient.Font)
    DateFormat = PropBag.ReadProperty("DateFormat", m_def_DateFormat) 'force local settings
    txtEditDate.Text = PropBag.ReadProperty("Text", Format(Date, m_def_DateFormat))
    txtEditDate.MaxLength = Len(m_DateFormat)
    txtEditDate.Locked = PropBag.ReadProperty("Locked", False)
End Sub

Private Sub UserControl_Resize()

    txtEditDate.Width = UserControl.Width
    If UserControl.Height < 285 Then UserControl.Height = 285
    txtEditDate.Height = UserControl.Height
    UserControl.Height = txtEditDate.Height
    
End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

    Call PropBag.WriteProperty("BackColor", txtEditDate.BackColor, &H80000005)
    Call PropBag.WriteProperty("ForeColor", txtEditDate.ForeColor, &H80000008)
    Call PropBag.WriteProperty("Enabled", txtEditDate.Enabled, True)
    Call PropBag.WriteProperty("Font", txtEditDate.Font, Ambient.Font)
    Call PropBag.WriteProperty("DateFormat", m_DateFormat, m_def_DateFormat)
    Call PropBag.WriteProperty("Text", txtEditDate.Text, "dText1")
    Call PropBag.WriteProperty("Locked", txtEditDate.Locked, False)
End Sub
'
''WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
''MemberInfo=3,0,0,0
'Public Property Get DateValue() As Date
'    DateValue = m_DateValue
'End Property
'
'Public Property Let DateValue(ByVal New_DateValue As Date)
'    m_DateValue = New_DateValue
'    PropertyChanged "DateValue"
'End Property
'
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditDate,txtEditDate,-1,Text
Public Property Get Text() As String
Attribute Text.VB_Description = "Returns/sets the text contained in the control."
    Text = txtEditDate.Text
End Property

Public Property Let Text(ByVal New_Text As String)
    txtEditDate.Text() = New_Text
    PropertyChanged "Text"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditDate,txtEditDate,-1,Locked
Public Property Get Locked() As Boolean
Attribute Locked.VB_Description = "Determines whether a control can be edited."
    Locked = txtEditDate.Locked
End Property

Public Property Let Locked(ByVal New_Locked As Boolean)
    txtEditDate.Locked() = New_Locked
    PropertyChanged "Locked"
End Property

