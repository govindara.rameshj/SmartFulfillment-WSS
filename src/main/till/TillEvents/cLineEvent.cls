VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cLineEvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public strSKU         As String
Public lngLineNo      As Long
Public dblQuantity    As Double
Public dblDiscRate    As Double
Public strDesc        As String
Public strType        As String
Public strEventNo     As String
Public strDealGroupNo As String
Public dblDiscTotal   As Double
Public dteEndDate     As Date

