VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDotNetEventLibrary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function TemporaryPriceChangeSKU(ByVal SKU As String, ByVal SelectedDate As Date, ByRef OverrideEvent As Boolean, ByRef ExistingEventPrice As Double, ByRef ExistingEventNumber As String) As Double

   Dim V As COMTPWickes_InterOp_Interface.ITemporaryPriceChangeEvent
   Dim Factory As New COMTPWickes_InterOp_Wrapper.TemporaryPriceChangeEvent

   Set V = Factory.FactoryGet

   TemporaryPriceChangeSKU = V.GetCurrentPrice(SKU, SelectedDate, OverrideEvent, ExistingEventPrice, ExistingEventNumber)

End Function

Public Function MultiplePriceOverrideGetDealGroup(ByVal SKU As String, ByVal SelectedDate As Date) As Recordset

   Dim V As COMTPWickes_InterOp_Interface.IMultiplePriceOverride
   Dim Factory As New COMTPWickes_InterOp_Wrapper.MultiplePriceOverride

   Set V = Factory.FactoryGet
   Set MultiplePriceOverrideGetDealGroup = V.GetDealGroup(SelectedDate, SKU)

End Function

Public Function MultiplePriceOverrideGetEventMaster(ByVal SKU As String, ByVal SelectedDate As Date) As Recordset

   Dim V As COMTPWickes_InterOp_Interface.IMultiplePriceOverride
   Dim Factory As New COMTPWickes_InterOp_Wrapper.MultiplePriceOverride

   Set V = Factory.FactoryGet
   Set MultiplePriceOverrideGetEventMaster = V.GetEventMaster(SelectedDate, SKU)

End Function

Public Function MultiplePriceOverrideGetValidEvent(ByVal EventNumber As String, ByVal Priority As String) As Recordset

   Dim V As COMTPWickes_InterOp_Interface.IMultiplePriceOverride
   Dim Factory As New COMTPWickes_InterOp_Wrapper.MultiplePriceOverride

   Set V = Factory.FactoryGet
   Set MultiplePriceOverrideGetValidEvent = V.GetValidEvent(EventNumber, Priority)

End Function



