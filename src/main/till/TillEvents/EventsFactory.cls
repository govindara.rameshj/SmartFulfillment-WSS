VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EventsFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const m_GlobalEventsParameterID As Long = 983375
Private m_InitialisedUseGlobalEventsImplementation As Boolean
Private m_UseGlobalEventsImplementation As Boolean

Private Const m_GetCouponsParameterID As Long = 983352
Private m_InitialisedUseGetCouponsImplementation As Boolean
Private m_UseGetCouponsImplementation As Boolean

Private Const m_BuyCouponsParameterID As Long = 983351
Private m_InitialisedUseBuyCouponsImplementation As Boolean
Private m_UseBuyCouponsImplementation As Boolean

Private Const m_MultilinePriceOverrideParameterID As Long = 981404
Private m_InitialisedUseMultilinePriceOverrideImplementation As Boolean
Private m_UseMultilinePriceOverrideImplementation As Boolean

Private goSession     As Session

Friend Property Get UseGlobalEventImplementation() As Boolean

        UseGlobalEventImplementation = m_UseGlobalEventsImplementation
End Property

Friend Function FactoryGetGlobalEvent() As IGlobalEventValDisc
    
    If UseGlobalEventImplementation Then
        'using implementation with Global events on
        Set FactoryGetGlobalEvent = New GlobalEventValDisc
    Else
        'using live implementation
        Set FactoryGetGlobalEvent = New LiveEvents
    End If
End Function

Friend Property Get UseGetCouponImplementation() As Boolean

        UseGetCouponImplementation = m_UseGetCouponsImplementation
End Property

Friend Function FactoryGetGetCoupon() As IGetCoupon
    
    If UseGetCouponImplementation Then
        'using implementation with Get Coupon events on
        Set FactoryGetGetCoupon = New GetCoupon
    Else
        'using live implementation
        Set FactoryGetGetCoupon = New LiveEvents
    End If
End Function

Friend Property Get UseBuyCouponImplementation() As Boolean

        UseBuyCouponImplementation = m_UseBuyCouponsImplementation
End Property

Friend Function FactoryGetBuyCoupon() As IBuyCoupon
    
    If UseBuyCouponImplementation Then
        'using implementation with Buy Coupon events on
        Set FactoryGetBuyCoupon = New BuyCoupon
    Else
        'using live implementation
        Set FactoryGetBuyCoupon = New LiveEvents
    End If
End Function

Friend Property Get UseMultilinePriceOverrideImplementation() As Boolean

        UseMultilinePriceOverrideImplementation = m_UseMultilinePriceOverrideImplementation
End Property

Friend Function FactoryGetMultilinePriceOverride() As IMultilinePO
    
    If UseMultilinePriceOverrideImplementation Then
        'using implementation with Multiline price override on
        Set FactoryGetMultilinePriceOverride = New MultilinePO
    Else
        'using live implementation
        Set FactoryGetMultilinePriceOverride = New LiveEvents
    End If
End Function

Private Sub Class_Initialize()

    GetUseGlobalEventsValue
    GetUseGetCouponsParameterValue
    GetUseBuyCouponsParameterValue
    GetUseMultilinePriceOverrideParameterValue
End Sub

Friend Sub GetUseGlobalEventsValue()
    Dim strQuery    As String
    Dim oConnection As Connection
    Dim lngSaveCL   As Long
    Dim Recordvalue As Recordset

    If Not m_InitialisedUseGlobalEventsImplementation Then
        Set oConnection = m_oSession.Database.Connection
        strQuery = "select BooleanValue from PARAMETERS where PARAMETERID= " & m_GlobalEventsParameterID
        
        lngSaveCL = oConnection.CursorLocation
        oConnection.CursorLocation = adUseClient
On Error Resume Next
        Set Recordvalue = m_oSession.Database.ExecuteCommand(strQuery)
        oConnection.CursorLocation = lngSaveCL
        m_InitialisedUseGlobalEventsImplementation = True
On Error GoTo 0
        If Not Recordvalue Is Nothing Then
            With Recordvalue
                If Not .BOF And Not .EOF Then
                    If .Fields.Count > 0 Then
                        If (Recordvalue.Fields.Item(0).value = True) Then
                            m_UseGlobalEventsImplementation = True
                        End If
                    End If
                End If
            End With
        End If
    End If
End Sub

Friend Sub GetUseGetCouponsParameterValue()

    If Not m_InitialisedUseGetCouponsImplementation Then
On Error Resume Next
        m_UseGetCouponsImplementation = m_oSession.GetParameter(m_GetCouponsParameterID)
        m_InitialisedUseGetCouponsImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseBuyCouponsParameterValue()

    If Not m_InitialisedUseBuyCouponsImplementation Then
On Error Resume Next
        m_UseBuyCouponsImplementation = m_oSession.GetParameter(m_BuyCouponsParameterID)
        m_InitialisedUseBuyCouponsImplementation = True
On Error GoTo 0
    End If
End Sub

Friend Sub GetUseMultilinePriceOverrideParameterValue()

    If Not m_InitialisedUseMultilinePriceOverrideImplementation Then
On Error Resume Next
        m_UseMultilinePriceOverrideImplementation = m_oSession.GetParameter(m_MultilinePriceOverrideParameterID)
        m_InitialisedUseMultilinePriceOverrideImplementation = True
On Error GoTo 0
    End If
End Sub

