VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmHSEvents 
   Caption         =   "HS Spend Calculations"
   ClientHeight    =   8040
   ClientLeft      =   75
   ClientTop       =   450
   ClientWidth     =   16695
   LinkTopic       =   "Form1"
   ScaleHeight     =   8040
   ScaleWidth      =   16695
   Begin FPSpreadADO.fpSpread sprdItems 
      Height          =   2595
      Left            =   60
      TabIndex        =   0
      Top             =   2760
      Width           =   13935
      _Version        =   458752
      _ExtentX        =   24580
      _ExtentY        =   4577
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   19
      MaxRows         =   0
      SpreadDesigner  =   "frmHSEvents.frx":0000
   End
   Begin FPSpreadADO.fpSpread sprdHSTotal 
      Height          =   2415
      Left            =   60
      TabIndex        =   1
      Top             =   5460
      Width           =   11175
      _Version        =   458752
      _ExtentX        =   19711
      _ExtentY        =   4260
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   7
      MaxRows         =   0
      SpreadDesigner  =   "frmHSEvents.frx":07E3
   End
   Begin FPSpreadADO.fpSpread sprdMaster 
      Height          =   2595
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   13935
      _Version        =   458752
      _ExtentX        =   24580
      _ExtentY        =   4577
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   18
      MaxRows         =   0
      SpreadDesigner  =   "frmHSEvents.frx":0B66
   End
End
Attribute VB_Name = "frmHSEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmHSEvents"

Const HIERACHAL_SPEND_TYPE As String = "HS"
Const HS_DESCRIPTION As String = "Spend Level Saving"

'Constants to extract the values from the frmTill spread sheet
Const COL_OP_ITEMNO As Long = 1
Const COL_OP_PARTCODE As Long = 2
Const COL_OP_QTY As Long = 5
Const COL_OP_SELLPRICE As Long = 9
Const COL_OP_INCTOTAL As Long = 14
Const COL_OP_LINETYPE As Long = 24
Const COL_OP_VOIDED As Long = 25
Const COL_OP_DEPTCODE As Long = 30
Const COL_OP_DEPTGRPCODE As Long = 31

'Constants where the valid items that can be discounted
Const COL_ITEM_NO As Long = 1
Const COL_ITEM_PARTCODE As Long = 2
Const COL_ITEM_QTY As Long = 3
Const COL_ITEM_SELLPRICE As Long = 4
Const COL_ITEM_INCTOTAL As Long = 5
Const COL_ITEM_HIER_CAT As Long = 6
Const COL_ITEM_HIER_GROUP As Long = 7
Const COL_ITEM_HIER_SUBGROUP As Long = 8
Const COL_ITEM_HIER_STYLE As Long = 9
Const COL_ITEM_TOTALDEPTNO As Long = 10
Const COL_ITEM_TOTALGRP As Long = 11
Const COL_ITEM_DEPT_LEVEL As Long = 12
Const COL_ITEM_GROUP_LEVEL As Long = 13
Const COL_ITEM_DEPT_DISC As Long = 14
Const COL_ITEM_GROUP_DISC As Long = 15
Const COL_ITEM_EVENT_NO As Long = 16
Const COL_ITEM_PRINT_LINENO As Long = 17
Const COL_ITEM_PRICE_USED As Long = 18
Const COL_ITEM_EVENT_DATE As Long = 19

'Constants where the summary of the events is calculated
Const COL_TOT_DESC As Long = 1
Const COL_TOT_EVENTNO As Long = 2
Const COL_TOT_SUBNO As Long = 3
Const COL_TOT_DEPTNO As Long = 4
Const COL_TOT_GROUPNO As Long = 5
Const COL_TOT_DISC As Long = 6
Const COL_TOT_DISCRATE As Long = 7 'G

Private mcolLineEvents   As Collection
Dim mcolPrintEvents As Collection
Dim mcolPrintCoupons As Collection 'Coupons rewards given to customer

Private mlngHSEventSeqNo As Long

Private mcolTotalEvents  As Collection

Dim mcolAddedCoupons As Collection 'Coupons captured by customer
Dim mcolUnUsedCoupons  As Collection 'Working coupons in case Tender reset
Dim mUsedCoupons As Collection

Public Sub AddSKU(ByVal strSKUN As String, _
                ByVal lngQuantity As Long, _
                ByVal dblPrice As Double, _
                ByVal lngLineNo As Long, _
                ByVal strHierCategory As String, _
                ByVal strHierGroup As String, _
                ByVal strHierSubGroup As String, _
                ByVal strHierStyle As String)

    With sprdMaster
        .MaxRows = .MaxRows + 1
        .Row = .MaxRows
        .Col = COL_ITEM_NO
        .Text = lngLineNo
        .Col = COL_ITEM_PARTCODE
        .Text = strSKUN
        .Col = COL_ITEM_QTY
        .Text = lngQuantity
        .Col = COL_ITEM_SELLPRICE
        .Text = dblPrice
        .Col = COL_ITEM_INCTOTAL
        .Text = dblPrice * lngQuantity
        .Col = COL_ITEM_HIER_CAT
        .Text = strHierCategory
        .Col = COL_ITEM_HIER_GROUP
        .Text = strHierGroup
        .Col = COL_ITEM_HIER_SUBGROUP
        .Text = strHierSubGroup
        .Col = COL_ITEM_HIER_STYLE
        .Text = strHierStyle
    End With

End Sub


Private Sub ConsolidateDeptGroups(ByVal lngColLevel As Long)

Dim lngRowNo        As Long
Dim strOldGroup     As String
Dim dblGroupValue   As Double

    'Sort items on purchase order so that they are grouped by Part Code
    sprdItems.SortKey(1) = lngColLevel
    sprdItems.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdItems.Sort(COL_ITEM_NO, 1, sprdItems.MaxCols, sprdItems.MaxRows, SortByRow)
    'Step through list and add all items with the same department
    sprdItems.Row = -1
    sprdItems.Col = COL_ITEM_TOTALDEPTNO
    sprdItems.Text = ""
    sprdItems.Col = COL_ITEM_TOTALGRP
    sprdItems.value = 0
    
    'move line total to Department total, where calculations will be performed
    For lngRowNo = 1 To sprdItems.MaxRows Step 1
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_ITEM_INCTOTAL
        dblGroupValue = Val(sprdItems.Text)
        sprdItems.Col = lngColLevel
        strOldGroup = sprdItems.Text
        'move to current line and addin quantity
        sprdItems.Col = COL_ITEM_TOTALGRP
        sprdItems.Text = dblGroupValue
        sprdItems.Col = COL_ITEM_TOTALDEPTNO
        sprdItems.Text = strOldGroup
    Next lngRowNo
    
    'Next add up totals based on the selected category
    strOldGroup = ""
    For lngRowNo = sprdItems.MaxRows To 1 Step -1
        sprdItems.Row = lngRowNo
        sprdItems.Col = lngColLevel
        If strOldGroup = sprdItems.Text Then 'Item matches so add lines together
            sprdItems.Row = sprdItems.Row + 1 'move to next line (should be same Department code)
            sprdItems.Col = COL_ITEM_TOTALGRP
            dblGroupValue = Val(sprdItems.Text)
            sprdItems.Text = 0
            sprdItems.Col = COL_ITEM_TOTALDEPTNO
            sprdItems.Text = ""
            'move to current line and addin quantity
            sprdItems.Row = lngRowNo
            sprdItems.Col = COL_ITEM_TOTALGRP
            sprdItems.Text = Val(sprdItems.Text) + dblGroupValue
        End If
        sprdItems.Row = lngRowNo
        sprdItems.Col = lngColLevel
        strOldGroup = sprdItems.Text
    Next lngRowNo
    
End Sub

Public Sub DeleteSKU(ByVal strSKUN As String, _
                     ByVal lngLineNo As Long)
                     
Dim lngRowNo As Long
                
    For lngRowNo = 1 To sprdMaster.MaxRows Step 1
        sprdMaster.Row = lngRowNo
        sprdMaster.Col = COL_ITEM_NO
        If (Val(sprdMaster.value) = lngLineNo) Then
            sprdMaster.Col = COL_ITEM_PARTCODE
            If (sprdMaster.Text = strSKUN) Then
                Call sprdMaster.DeleteRows(lngRowNo, 1)
                sprdMaster.MaxRows = sprdMaster.MaxRows - 1
                Exit For
            End If 'sku matched
        End If 'Line number matched
    Next lngRowNo

End Sub

Public Function CheckEvents(ByRef colOPEvents As Collection, ByRef colHSSummary As Collection, ByRef colPrintCoupons As Collection, ByRef colUnusedCoupons As Collection) As Collection
    
    Set mcolLineEvents = New Collection
    Set mcolTotalEvents = New Collection
    Set mcolPrintCoupons = New Collection
    Set mcolUnUsedCoupons = colUnusedCoupons

    CheckForDiscounts colOPEvents, colHSSummary, colPrintCoupons
    If EnableGetCoupons Then
        CheckForGetCoupons colOPEvents, colHSSummary, colPrintCoupons
    End If
    
    'Now pass HS Events back out for display and saving
    Set colHSSummary = mcolTotalEvents
    Set colPrintCoupons = mcolPrintCoupons
    Set CheckEvents = mcolLineEvents
End Function

Public Sub CheckForDiscounts(ByRef colOPEvents As Collection, ByRef colHSSummary As Collection, ByRef colPrintCoupons As Collection)
    Dim arlngCols(4) As Long
    Dim lngHSSeqNo   As Long

    'Create list of Groups and the order will be processed in
    arlngCols(4) = COL_ITEM_HIER_CAT
    arlngCols(3) = COL_ITEM_HIER_GROUP
    arlngCols(2) = COL_ITEM_HIER_SUBGROUP
    arlngCols(1) = COL_ITEM_HIER_STYLE
    
    mlngHSEventSeqNo = 0
    sprdHSTotal.MaxRows = 0
    
    Call CopyItemsFromMaster
    Call ApplyOPEvents(colOPEvents)
    For lngHSSeqNo = 1 To 4 Step 1
        Call ConsolidateDeptGroups(arlngCols(lngHSSeqNo))
        Call ProcessDiscounts(arlngCols(lngHSSeqNo), False)
    Next lngHSSeqNo
    
    'After doing normal HS Events - check for Global HS Event
    Call CopyItemsFromMaster
    Call ApplyOPEvents(colOPEvents)
    Call ApplyOPEvents(mcolLineEvents)
    'Force all items to Group 2, this will force the Global event to be fired
    Call ForceDeptToAllGroups(arlngCols(1))
    Call ConsolidateDeptGroups(arlngCols(1))
    Call ProcessDiscounts(arlngCols(1), False)
    
    'Check for store opening global event if applied
     Call CopyItemsFromMaster
    Call ApplyOPEvents(colOPEvents)
    Call ApplyOPEvents(mcolLineEvents)
    'Force all items to Group 2, this will force the Global event to be fired
    Call ForceDeptToAllGroups(arlngCols(1))
    Call ConsolidateDeptGroups(arlngCols(1))
    Call ProcessDiscounts(arlngCols(1), True)
        
End Sub

Public Sub CheckForGetCoupons(ByRef colOPEvents As Collection, ByRef colHSSummary As Collection, ByRef colPrintCoupons As Collection)
    Dim arlngCols(4) As Long
    Dim lngHSSeqNo   As Long

    'Create list of Groups and the order will be processed in
    arlngCols(4) = COL_ITEM_HIER_CAT
    arlngCols(3) = COL_ITEM_HIER_GROUP
    arlngCols(2) = COL_ITEM_HIER_SUBGROUP
    arlngCols(1) = COL_ITEM_HIER_STYLE
    
    sprdHSTotal.MaxRows = 0
    
    Call CopyItemsFromMaster
    Call ApplyOPEvents(colOPEvents)
    For lngHSSeqNo = 1 To 4 Step 1
        Call ConsolidateDeptGroups(arlngCols(lngHSSeqNo))
        Call ProcessGetCoupons(arlngCols(lngHSSeqNo), False)
    Next lngHSSeqNo
    
    'After doing normal HS Events - check for Global HS Event
    Call CopyItemsFromMaster
    Call ApplyOPEvents(colOPEvents)
    Call ApplyOPEvents(mcolLineEvents)
    'Force all Style to '000000', this will force the Global event to be fired
    Call ForceDeptToAllGroups(arlngCols(1))
    Call ConsolidateDeptGroups(arlngCols(1))
    Call ProcessGetCoupons(arlngCols(1), False)
    'Check for store opening global event if applied
     Call CopyItemsFromMaster
    Call ApplyOPEvents(colOPEvents)
    Call ApplyOPEvents(mcolLineEvents)
    'Force all items to Group 2, this will force the Global event to be fired
    Call ForceDeptToAllGroups(arlngCols(1))
    Call ConsolidateDeptGroups(arlngCols(1))
    Call ProcessGetCoupons(arlngCols(1), True)
End Sub

Public Sub ForceDeptToAllGroups(ByVal lngColNo As Long)

    sprdItems.Col = lngColNo
    sprdItems.Row = -1
    sprdItems.Text = "000000"

End Sub

Public Sub UpdateSKUPrice(ByVal strSKUN As String, _
                          ByVal lngLineNo As Long, _
                          ByVal curPrice As Currency)

Dim lngRowNo    As Long
Dim lngQuantity As Long
                
    'step through lines and locate specified line and sku number
    For lngRowNo = 1 To sprdMaster.MaxRows Step 1
        sprdMaster.Row = lngRowNo
        sprdMaster.Col = COL_ITEM_NO
        If (Val(sprdMaster.value) = lngLineNo) Then
            sprdMaster.Col = COL_ITEM_PARTCODE
            If (sprdMaster.Text = strSKUN) Then
                'update item price and recalc line total
                sprdMaster.Col = COL_ITEM_QTY
                lngQuantity = Val(sprdMaster.value)
                sprdMaster.Col = COL_ITEM_SELLPRICE
                sprdMaster.value = curPrice
                sprdMaster.Col = COL_ITEM_INCTOTAL
                sprdMaster.Text = curPrice * lngQuantity
            End If 'sku matched
        End If 'Line number matched
    Next lngRowNo
    
End Sub

Public Property Get UsedCoupons() As Collection

    Set UsedCoupons = mUsedCoupons
End Property

Public Property Set UsedCoupons(ByRef value As Collection)

    Set mUsedCoupons = value
End Property


Private Sub ApplyOPEvents(ByRef colOPEvents As Collection)

Dim lngEventNo   As Long
Dim lngLineNo    As Long
Dim oEvent       As cTillEvents_Wickes.cLineEvent

    'step through events and locate line to place event details against
    For lngEventNo = 1 To colOPEvents.Count Step 1
        Set oEvent = colOPEvents(lngEventNo)
        For lngLineNo = 1 To sprdItems.MaxRows Step 1
            sprdItems.Row = lngLineNo
            sprdItems.Col = COL_ITEM_NO
            If Val(sprdItems.Text) = oEvent.lngLineNo Then
                sprdItems.Col = COL_ITEM_INCTOTAL
                sprdItems.value = sprdItems.value - oEvent.dblDiscTotal
                Exit For
            End If
        Next lngLineNo
    Next lngEventNo

End Sub

Private Sub ProcessDiscounts(ByVal lngDeptColNo As Long, ByVal globalEvent As Boolean)
    Dim rsOPMaster      As Recordset
    Dim rsSKUExclude    As Recordset
    Dim strFilter       As String
    Dim lngMasterNo     As Long
    Dim lngLineNo       As Long
    Dim lngPrintLineNo  As Long
    Dim lngDealNo       As Long
    Dim lngExcNo        As Long
    Dim lngExcSKUPos    As Long
    Dim lngDeptNo       As String
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strGroupCode    As String
    Dim strDeptCode     As String
    Dim strEventDesc    As String
    
    Dim dblGetQty       As Double
    Dim dblDiscQty      As Double
    Dim curDptTotal     As Currency
    Dim blnItemUsed     As Boolean
    Dim blnDiscountMatchFound   As Boolean
    
    Dim valueDiscountCheck As Boolean

    strNow = Format$(Now, "YYYYMMDDHHNN")
    sprdHSTotal.MaxRows = 0
    
    Set rsOPMaster = HSEventsRecordset(sprdItems.Text, dblDiscQty)

    '*****************************************
    '** Process Department and Group Offers **
    '*****************************************
    lngLineNo = 0
    Do While (lngLineNo < sprdItems.MaxRows)
        lngLineNo = lngLineNo + 1
        sprdItems.Row = lngLineNo
        sprdItems.Col = COL_ITEM_TOTALGRP
        curDptTotal = Val(sprdItems.value)
        sprdItems.Col = COL_ITEM_TOTALDEPTNO
        If (sprdItems.Text <> "") Then 'search if Group Summary line

            strFilter = "EventKey='" & sprdItems.Text & "' " _
                      & " And SpecialPrice <= " & curDptTotal
            
            ' Make sure only use events that have a discount (Get Coupon event might not have a discount)
            If EnableGetCoupons Then
                strFilter = strFilter & " And GETCPN = '0000000'"
            End If
            
            If globalEvent Then
                strFilter = strFilter & " And EventNumber = '999999'"
            Else
                strFilter = strFilter & " And EventNumber <> '999999'"
            End If
            
            rsOPMaster.Filter = strFilter
            
            If (rsOPMaster.RecordCount > 0) Then  'HS Event found for Dept and Value
                Do While (rsOPMaster.EOF = False)
                    dblDiscQty = curDptTotal
                    'Check if matching Header for Master Event - stored by Deal groups
                    If (modEvents.CheckHeaderValid(rsOPMaster!EventNumber, rsOPMaster!Priority, rsOPMaster!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True) Then
                        If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True) Then
                            sprdItems.Col = COL_ITEM_PRICE_USED
                            sprdItems.Row = -1
                            sprdItems.value = 0
                            'get list of SKU's within the HS Group
                            Set rsSKUExclude = SKUExcludeRecordSet(rsOPMaster!EventNumber, rsOPMaster!EventKey)
                            Call DebugMsg(MODULE_NAME, "ProcessDiscounts", endlDebug, "HS Group " & rsOPMaster!EventKey & "-Excludes " & rsSKUExclude.RecordCount & " SKUs")
                            If (rsSKUExclude.RecordCount > 0) Then
                                'Step though each sku within group and check if excluded from Event
                                For lngExcSKUPos = lngLineNo To sprdItems.MaxRows Step 1
                                    sprdItems.Row = lngExcSKUPos
                                    sprdItems.Col = lngDeptColNo
                                    'Check if item is part of the the Event Group
                                    If (sprdItems.Text = rsOPMaster!EventKey) Then
                                        'Check if SKU is on the exclusion list
                                        If (rsSKUExclude.BOF = False) Then Call rsSKUExclude.MoveFirst
                                        Do While Not rsSKUExclude.EOF
                                            sprdItems.Col = COL_ITEM_PARTCODE
                                            If (sprdItems.Text = rsSKUExclude!PartCode) Then
                                                sprdItems.Col = COL_ITEM_PRICE_USED
                                                sprdItems.value = 1
                                                sprdItems.Col = COL_ITEM_INCTOTAL
                                                'Reduce Grpiup total and check it still meets spend level
                                                dblDiscQty = dblDiscQty - Val(sprdItems.value)
                                                Call DebugMsg(MODULE_NAME, "ProcessDiscounts", endlDebug, "Proc-Exc(" & rsSKUExclude!PartCode & ")" & "New=" & ChrW(163) & dblDiscQty)
                                                If (dblDiscQty < Val(rsOPMaster!SpecialPrice)) Then
                                                    Exit For
                                                End If
                                            End If 'part code is excluded
                                            rsSKUExclude.MoveNext
                                        Loop
                                        If (dblDiscQty < Val(rsOPMaster!SpecialPrice)) Then Exit For
                                        sprdItems.Col = lngDeptColNo
                                    Else
                                        Exit For
                                    End If
                                Next lngExcSKUPos 'check next line
                            End If 'any Excluded SKU's to check
                            
                            blnDiscountMatchFound = False
                            lngPrintLineNo = 0
                            sprdItems.Row = -1
                            sprdItems.Col = COL_ITEM_PRINT_LINENO
                            sprdItems.Text = ""
                            Call DebugMsg(MODULE_NAME, "ProcessDiscounts", endlDebug, "Processing (" & rsOPMaster.RecordCount & ")" & rsOPMaster!EventNumber & "/" & rsOPMaster!Priority & "->" & rsOPMaster!EventKey & "(" & dblDiscQty & " Level=" & rsOPMaster!SpecialPrice)
                            
                            'Perform check to ensure that Excluded items did not take total below event limit
                            If dblDiscQty >= Val(rsOPMaster!SpecialPrice) Then
                                'Step through list of items and see if Department can be found
                                Do While (lngLineNo <= sprdItems.MaxRows)
                                    sprdItems.Row = lngLineNo
                                    sprdItems.Col = lngDeptColNo
                                    If (sprdItems.Text = rsOPMaster!EventKey) Then
                                        sprdItems.Col = COL_ITEM_PRICE_USED
                                        If (Val(sprdItems.value) = 0) Then
                                            sprdItems.Col = COL_ITEM_INCTOTAL
                                            dblDiscQty = Val(sprdItems.value)
                                            Call DebugMsg(MODULE_NAME, "ProcessDiscounts", endlDebug, "Item " & lngLineNo & "-(MM" & rsOPMaster!EventKey & ") to bought " & sprdItems.Text)
                                            sprdItems.Col = COL_ITEM_DEPT_DISC
                                            If EnableGlobalEvents = True Then
                                                If Val(rsOPMaster!DiscountPercent) = 0 Then
                                                    If valueDiscountCheck = False Then
                                                        sprdItems.value = Val(rsOPMaster!ValueDiscount)
                                                        valueDiscountCheck = True
                                                    End If
                                                Else
                                                    sprdItems.value = dblDiscQty * Val(rsOPMaster!DiscountPercent) / 100
                                                End If
                                            Else
                                                sprdItems.value = dblDiscQty * Val(rsOPMaster!DiscountPercent) / 100
                                            End If
                                            sprdItems.Col = COL_ITEM_EVENT_NO
                                            sprdItems.Text = rsOPMaster!EventNumber
                                            sprdItems.Col = COL_ITEM_EVENT_DATE
                                            sprdItems.Text = rsOPMaster!EDAT
                                            blnDiscountMatchFound = True
                                            'check if line found appears after this line then set print line to this line
                                            sprdItems.Col = COL_ITEM_NO
                                            If (Val(sprdItems.value) > lngPrintLineNo) Then
                                                lngPrintLineNo = Val(sprdItems.value)
                                                sprdItems.Row = -1
                                                sprdItems.Col = COL_ITEM_PRINT_LINENO
                                                sprdItems.value = lngPrintLineNo
                                                sprdItems.Row = lngLineNo
                                            End If
                                        End If 'SKU was not excluded from HS Event
                                    Else
                                        lngLineNo = lngLineNo - 1
                                        Exit Do
                                    End If
                                    lngLineNo = lngLineNo + 1
                                Loop
                                If (blnDiscountMatchFound = True) Then
                                    Call ExtractTotals
                                    'Adjust lineNo to enumerate all sprdItems rows
                                    lngLineNo = lngLineNo - 1
                                    If (Val(rsOPMaster!BUYCPN) > 0) Then
                                        Call RemoveMatchedCoupon(rsOPMaster!BUYCPN)
                                        mcolCouponsUsedInTransaction.Add (rsOPMaster!BUYCPN)
                                    End If
                                    If Not EnableGetCoupons Then
                                        If (Val(rsOPMaster!GETCPN) > 0) Then
                                            mcolPrintCoupons.Add (rsOPMaster!GETCPN & ":" & lngPrintLineNo)
                                        End If
                                    End If
                                    Exit Do
                                Else
                                    Call rsOPMaster.MoveNext
                                End If
                            Else
                                Call rsOPMaster.MoveNext
                            End If 'excluded SKU has dropped value to below event
                        Else
                            Call rsOPMaster.MoveNext
                        End If 'if event header date/time valid for Event Master
                    Else
                        Call rsOPMaster.MoveNext
                    End If 'Header was found and not deleted
                Loop 'if further discounts found for Dept Code
            End If 'No event master found for Department / amount spent
        End If 'Dept Code to search against
    Loop
End Sub

Private Sub ProcessGetCoupons(ByVal lngDeptColNo As Long, ByVal globalEvent As Boolean)
    Dim rsOPMaster      As Recordset
    Dim rsSKUExclude    As Recordset
    Dim strFilter       As String
    Dim lngLineNo       As Long
    Dim lngPrintLineNo  As Long
    Dim lngExcSKUPos    As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strEventDesc    As String
    Dim dblDiscQty      As Double
    Dim curDptTotal     As Currency
    Dim blnGetCouponMatchFound As Boolean

    strNow = Format$(Now, "YYYYMMDDHHNN")
    sprdHSTotal.MaxRows = 0
    
    Set rsOPMaster = HSEventsRecordset(sprdItems.Text, dblDiscQty)

    '*****************************************
    '** Process Department and Group Offers **
    '*****************************************
    lngLineNo = 0
    Do While (lngLineNo < sprdItems.MaxRows)
        lngLineNo = lngLineNo + 1
        sprdItems.Row = lngLineNo
        sprdItems.Col = COL_ITEM_TOTALGRP
        curDptTotal = Val(sprdItems.value)
        sprdItems.Col = COL_ITEM_TOTALDEPTNO
        If (sprdItems.Text <> "") Then 'search if Group Summary line

            ' Filter for the GETCPN events
            strFilter = "EventKey='" & sprdItems.Text & "'" _
                      & " And SpecialPrice <= " & curDptTotal _
                      & " And GETCPN <> '0000000'"
            
            If globalEvent Then
                strFilter = strFilter & " And EventNumber = '999999'"
            End If
            
            rsOPMaster.Filter = strFilter
                              
            If (rsOPMaster.RecordCount > 0) Then 'Get Coupon Event found for Dept and Value
                Do While Not rsOPMaster.EOF
                    dblDiscQty = curDptTotal
                    'Check if matching Header for Master Event - stored by Deal groups
                    If (modEvents.CheckHeaderValid(rsOPMaster!EventNumber, rsOPMaster!Priority, rsOPMaster!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True) Then
                        If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True) Then
                            sprdItems.Col = COL_ITEM_PRICE_USED
                            sprdItems.Row = -1
                            sprdItems.value = 0
                            'get list of SKU's within the HS Group
                            Set rsSKUExclude = SKUExcludeRecordSet(rsOPMaster!EventNumber, rsOPMaster!EventKey)
                            Call DebugMsg(MODULE_NAME, "ProcessGetCoupons", endlDebug, "HS Group " & rsOPMaster!EventKey & "-Excludes " & rsSKUExclude.RecordCount & " SKUs")
                            If (rsSKUExclude.RecordCount > 0) Then
                                'Step though each sku within group and check if excluded from Event
                                For lngExcSKUPos = lngLineNo To sprdItems.MaxRows Step 1
                                    sprdItems.Row = lngExcSKUPos
                                    sprdItems.Col = lngDeptColNo
                                    'Check if item is part of the the Event Group
                                    If (sprdItems.Text = rsOPMaster!EventKey) Then
                                        'Check if SKU is on the exclusion list
                                        If (rsSKUExclude.BOF = False) Then Call rsSKUExclude.MoveFirst
                                        Do While Not rsSKUExclude.EOF
                                            sprdItems.Col = COL_ITEM_PARTCODE
                                            If (sprdItems.Text = rsSKUExclude!PartCode) Then
                                                sprdItems.Col = COL_ITEM_PRICE_USED
                                                sprdItems.value = 1
                                                sprdItems.Col = COL_ITEM_INCTOTAL
                                                'Reduce Group total and check it still meets spend level
                                                dblDiscQty = dblDiscQty - Val(sprdItems.value)
                                                Call DebugMsg(MODULE_NAME, "ProcessGetCoupons", endlDebug, "Proc-Exc(" & rsSKUExclude!PartCode & ")" & "New=" & ChrW(163) & dblDiscQty)
                                                If (dblDiscQty < Val(rsOPMaster!SpecialPrice)) Then
                                                    Exit For
                                                End If
                                            End If 'part code is excluded
                                            rsSKUExclude.MoveNext
                                        Loop
                                        If (dblDiscQty < Val(rsOPMaster!SpecialPrice)) Then Exit For
                                        sprdItems.Col = lngDeptColNo
                                    Else
                                        Exit For
                                    End If
                                Next lngExcSKUPos 'check next line
                            End If 'any Excluded SKU's to check
                            
                            blnGetCouponMatchFound = False
                            lngPrintLineNo = 0
                            sprdItems.Row = -1
                            sprdItems.Col = COL_ITEM_PRINT_LINENO
                            sprdItems.Text = ""
                            Call DebugMsg(MODULE_NAME, "ProcessGetCoupons", endlDebug, "Processing (" & rsOPMaster.RecordCount & ")" & rsOPMaster!EventNumber & "/" & rsOPMaster!Priority & "->" & rsOPMaster!EventKey & "(" & dblDiscQty & " Level=" & rsOPMaster!SpecialPrice)
                            
                            'Perform check to ensure that rsOPMaster Excluded items did not take total below event limit
                            If dblDiscQty >= Val(rsOPMaster!SpecialPrice) Then
                                'Step through list of items and see if Department can be found
                                Do While (lngLineNo <= sprdItems.MaxRows)
                                    sprdItems.Row = lngLineNo
                                    sprdItems.Col = lngDeptColNo
                                    If (sprdItems.Text = rsOPMaster!EventKey And dblDiscQty >= Val(rsOPMaster!SpecialPrice)) Then
                                        sprdItems.Col = COL_ITEM_PRICE_USED
                                        If (Val(sprdItems.value) = 0) Then
                                            sprdItems.Col = COL_ITEM_INCTOTAL
                                            dblDiscQty = Val(sprdItems.value)
                                            Call DebugMsg(MODULE_NAME, "ProcessGetCoupons", endlDebug, "Item " & lngLineNo & "-(MM" & rsOPMaster!EventKey & ") to bought " & sprdItems.Text)
                                            sprdItems.Col = COL_ITEM_EVENT_NO
                                            sprdItems.Text = rsOPMaster!EventNumber
                                            sprdItems.Col = COL_ITEM_EVENT_DATE
                                            sprdItems.Text = rsOPMaster!EDAT
                                            blnGetCouponMatchFound = True
                                            'check if line found appears after this line then set print line to this line
                                            sprdItems.Col = COL_ITEM_NO
                                            If (Val(sprdItems.value) > lngPrintLineNo) Then
                                                lngPrintLineNo = Val(sprdItems.value)
                                                sprdItems.Row = -1
                                                sprdItems.Col = COL_ITEM_PRINT_LINENO
                                                sprdItems.value = lngPrintLineNo
                                                sprdItems.Row = lngLineNo
                                            End If
                                        End If 'SKU was not excluded from HS Event
                                    Else
                                        lngLineNo = lngLineNo - 1
                                        Exit Do
                                    End If
                                    lngLineNo = lngLineNo + 1
                                Loop
                                If blnGetCouponMatchFound Then
                                    Call mcolPrintCoupons.Add(rsOPMaster!GETCPN & ":" & lngPrintLineNo)
                                    Exit Do
                                Else
                                    Call rsOPMaster.MoveNext
                                End If
                            Else
                                Call rsOPMaster.MoveNext
                            End If 'excluded SKU has dropped value to below event
                        Else
                            Call rsOPMaster.MoveNext
                        End If 'if event header date/time valid for Event Master
                    Else
                        Call rsOPMaster.MoveNext
                    End If 'Header was found and not deleted
                Loop 'if further discounts found for Dept Code
            End If 'No event master found for Department / amount spent
        End If 'Dept Code to search against
    Loop
End Sub

Private Sub ExtractTotals()

Dim lngRowNo    As Long
Dim oTotal      As cEventTotal
Dim oLine       As cLineEvent

    'Create a new Event Header
    Set oTotal = New cEventTotal
    sprdItems.Row = lngRowNo
    oTotal.strType = "HS"
    mlngHSEventSeqNo = mlngHSEventSeqNo + 1
    oTotal.strEventNo = Format$(mlngHSEventSeqNo, "000000")
    oTotal.strDesc = HS_DESCRIPTION
    Call mcolTotalEvents.Add(oTotal)
    
    'Now save discounted lines
    For lngRowNo = sprdItems.MaxRows To 1 Step -1
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_ITEM_DEPT_DISC
        If (Val(sprdItems.value) > 0) Then 'if discount given, then save
            Set oLine = New cLineEvent
            oLine.strType = "HS"
            sprdItems.Col = COL_ITEM_EVENT_NO
            oLine.strEventNo = oTotal.strEventNo
            sprdItems.Col = COL_ITEM_NO
            oLine.lngLineNo = Val(sprdItems.value)
            oLine.strDesc = "HS Desc"
            sprdItems.Col = COL_ITEM_DEPT_DISC
            oLine.dblDiscTotal = Val(sprdItems.Text)
            oTotal.dblDiscTotal = oTotal.dblDiscTotal + Val(sprdItems.Text)
            sprdItems.Col = COL_ITEM_EVENT_NO
            oLine.strDealGroupNo = sprdItems.Text
            Call mcolLineEvents.Add(oLine)
            'Update Event Total header with line details
            sprdItems.Col = COL_ITEM_PRINT_LINENO
            oTotal.lngLineNo = Val(sprdItems.value)
            sprdItems.Col = COL_ITEM_EVENT_NO
            oTotal.strDealGroupNo = sprdItems.Text
            sprdItems.Col = COL_ITEM_EVENT_DATE
            oTotal.dteEndDate = sprdItems.value
            'Once discount applied - remove from list
            Call sprdItems.DeleteRows(lngRowNo, 1)
            sprdItems.MaxRows = sprdItems.MaxRows - 1
        End If
    Next lngRowNo
    
End Sub

Public Sub ResetEvents()

    sprdHSTotal.MaxRows = 0
    sprdItems.MaxRows = 0
    sprdMaster.MaxRows = 0
    Set mcolAddedCoupons = New Collection
End Sub

Private Sub CopyItemsFromMaster()

Dim lngColNo As Long
Dim lngRowNo As Long

    sprdItems.MaxRows = sprdMaster.MaxRows
    For lngColNo = 1 To sprdMaster.MaxCols Step 1
        For lngRowNo = 1 To sprdMaster.MaxRows Step 1
            sprdItems.Col = lngColNo
            sprdItems.Row = lngRowNo
            sprdMaster.Col = lngColNo
            sprdMaster.Row = lngRowNo
            sprdItems.Text = sprdMaster.Text
        Next lngRowNo
    Next lngColNo

End Sub


Private Function HSEventsRecordset(strItemKey As String, dblDiscQty As Double) As Recordset

Dim strQuery       As String
Dim oConnection As Connection
Dim lngRow         As Long

Dim lngSaveCL As Long
    
    Set oConnection = m_oSession.Database.Connection
  
'    strQuery = "select NUMB EventNumber, PRIO Priority, IDOW TimeOrDayRelated, KEY1 EventKey, " & "PRIC SpecialPrice, PDIS DiscountPercent from EVTMAS "
'    strQuery = strQuery & "where SDAT <= '" & Format$(Date, "yyyy-mm-dd") & "' AND EDAT >= '" & Format$(Date, "yyyy-mm-dd")
'    strQuery = strQuery & "' and TYPE = 'HS' and IDEL=0 and KEY1 = '" & strItemKey
'    strQuery = strQuery & "' and PRIC <= " & dblDiscQty & " order by TYPE desc, KEY1 desc, KEY2 desc, NUMB desc"
    
    strQuery = "select NUMB EventNumber, PRIO Priority, IDOW TimeOrDayRelated, KEY1 EventKey, " & "PRIC SpecialPrice, PDIS DiscountPercent,VDIS ValueDiscount, EDAT, BUYCPN, GETCPN from EVTMAS "
    
    
    strQuery = strQuery & "where SDAT <= '" & Format$(Date, "yyyy-mm-dd") & "' AND EDAT >= '" & Format$(Date, "yyyy-mm-dd")
    strQuery = strQuery & "' and TYPE = 'HS' and IDEL=0"
    
    strQuery = strQuery & " and key1 in ('"

    sprdItems.Col = COL_ITEM_TOTALDEPTNO
    For lngRow = 1 To sprdItems.MaxRows
        sprdItems.Row = lngRow
        If LenB(Trim$(sprdItems.value)) <> 0 Then
            If Right$(strQuery, 2) <> "('" Then
                strQuery = strQuery & "', '"
            End If
            strQuery = strQuery & sprdItems.Text
        End If
    Next

    strQuery = strQuery & "')"
    
    strQuery = strQuery & " order by TYPE desc, KEY1 desc, KEY2 desc, NUMB desc"
    
    lngSaveCL = oConnection.CursorLocation
    oConnection.CursorLocation = adUseClient
    Set HSEventsRecordset = m_oSession.Database.ExecuteCommand(strQuery)
    oConnection.CursorLocation = lngSaveCL
    
End Function

Private Function SKUExcludeRecordSet(ByVal EventNumber As String, ByVal EventKey As String)

Dim strQuery    As String
Dim oConnection As Connection
Dim lngSaveCL   As Long
    
    Set oConnection = m_oSession.Database.Connection
    
    strQuery = "select ITEM PartCode from EVTHEX where NUMB='" & EventNumber & "' and HIER='" & EventKey & "' and IDEL=0"
    
    lngSaveCL = oConnection.CursorLocation
    oConnection.CursorLocation = adUseClient
    Set SKUExcludeRecordSet = m_oSession.Database.ExecuteCommand(strQuery)
    oConnection.CursorLocation = lngSaveCL
    
End Function


Private Function CheckCouponsMatch(strBuyCouponID As String) As Boolean

Dim lngCouponNo As Long

    'if not coupon required then just accept matched
    If (strBuyCouponID = "") Or (strBuyCouponID = "0000000") Then
        CheckCouponsMatch = True
        Exit Function
    End If
    For lngCouponNo = 1 To mcolUnUsedCoupons.Count Step 1
        If strBuyCouponID = Left$(mcolUnUsedCoupons(lngCouponNo), 7) Then
            CheckCouponsMatch = True
            Exit Function
        End If
    Next lngCouponNo

End Function

Private Sub RemoveMatchedCoupon(strBuyCouponID As String)

Dim lngCouponNo As Long

    For lngCouponNo = 1 To mcolUnUsedCoupons.Count Step 1
        If strBuyCouponID = Left$(mcolUnUsedCoupons(lngCouponNo), 7) Then
            Call mcolUnUsedCoupons.Remove(lngCouponNo)
            Call AddUsedCoupon(strBuyCouponID, lngCouponNo)
            Exit Sub
        End If
    Next lngCouponNo

End Sub

Private Sub AddUsedCoupon(ByVal CouponId As String, lngCouponNo As Long)
    Dim ExistingEntry As String
    Dim OpeningBracketPos As Integer
    Dim NumberOfCoupons As Integer
    
    NumberOfCoupons = 1

On Error GoTo AddEntryForCoupon
    ExistingEntry = mUsedCoupons(CouponId)
On Error GoTo 0
    
    If ExistingEntry <> "" Then
        OpeningBracketPos = InStr(1, ExistingEntry, "(", vbTextCompare)
        If OpeningBracketPos > 0 Then
            NumberOfCoupons = Val(Mid$(ExistingEntry, OpeningBracketPos + 1)) + 1
        End If
        mUsedCoupons.Remove CouponId
    End If
    
AddEntryForCoupon:

On Error GoTo NoDescriptionSoDoNotAdd
    If Not NonReusableCouponDescriptions Is Nothing Then
        If lngCouponNo <= NonReusableCouponDescriptions.Count Then
            If NonReusableCouponDescriptions(lngCouponNo) <> "" Then
On Error GoTo 0
                mUsedCoupons.Add Mid(NonReusableCouponDescriptions(lngCouponNo), Len(CouponId & ":") + 1) & "   (" & CStr(NumberOfCoupons) & ")", CouponId
            End If
        End If
    End If

NoDescriptionSoDoNotAdd:
End Sub
