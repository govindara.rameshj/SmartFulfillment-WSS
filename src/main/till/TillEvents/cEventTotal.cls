VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cEventTotal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public lngLineNo      As Long
Public strDesc        As String
Public dteEndDate     As Date
Public dblDiscTotal   As Double
'Fields added below to cater for Hierachy Discounts
Public dblDiscRate    As Double
Public strType        As String
Public strEventNo     As String
Public strDealGroupNo As String

