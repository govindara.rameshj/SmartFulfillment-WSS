VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmOPEvents 
   Caption         =   "OP Event Calculations"
   ClientHeight    =   8040
   ClientLeft      =   75
   ClientTop       =   450
   ClientWidth     =   14310
   LinkTopic       =   "Form1"
   ScaleHeight     =   8040
   ScaleWidth      =   14310
   Begin FPSpreadADO.fpSpread sprdItems 
      Height          =   2775
      Left            =   60
      TabIndex        =   0
      Top             =   2400
      Width           =   14235
      _Version        =   458752
      _ExtentX        =   25109
      _ExtentY        =   4895
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   17
      MaxRows         =   0
      SpreadDesigner  =   "frmOPEvents.frx":0000
   End
   Begin FPSpreadADO.fpSpread sprdEvents 
      Height          =   2715
      Left            =   60
      TabIndex        =   1
      Top             =   5220
      Width           =   12075
      _Version        =   458752
      _ExtentX        =   21299
      _ExtentY        =   4789
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   11
      MaxRows         =   0
      SpreadDesigner  =   "frmOPEvents.frx":07F0
   End
   Begin FPSpreadADO.fpSpread sprdMaster 
      Height          =   2295
      Left            =   0
      TabIndex        =   2
      Top             =   60
      Width           =   13935
      _Version        =   458752
      _ExtentX        =   24580
      _ExtentY        =   4048
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   17
      MaxRows         =   0
      SpreadDesigner  =   "frmOPEvents.frx":09D2
   End
End
Attribute VB_Name = "frmOPEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>************************************************************************************
'* Module : frmOPEvents
'* Date   : 17/02/05
'* Author : Unknown
'*$Archive: /Projects/Wickes/VB/Till/frmTill.frm $
'******************************************************************************************
'* Summary:
'******************************************************************************************
'* $Author: mauricem $ $Date: 17/02/05 9:20 $ $Revision: 1 $
'* Versions:
'* 17/02/05    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmOPEvents"

Const QUANTITY_BREAK_TYPE As String = "QS"
Const MULTI_SKU_MATCH_TYPE As String = "MS"
Const DEALGROUP_TYPE As String = "DG"

Const QS_DESCRIPTION As String = "Bulk Saving"
Const MS_DESCRIPTION As String = "Multibuy Saving"
Const DG_DESCRIPTION As String = "Project Saving"

'Constants to extract the values from the frmTill spread sheet
Const COL_OP_ITEMNO As Long = 1
Const COL_OP_PARTCODE As Long = 2
Const COL_OP_QTY As Long = 5
Const COL_OP_SELLPRICE As Long = 9
Const COL_OP_INCTOTAL As Long = 14
Const COL_OP_LINETYPE As Long = 24
Const COL_OP_VOIDED As Long = 25

'Constants where the valid items that can be discounted
Const COL_ITEM_NO As Long = 1
Const COL_ITEM_PARTCODE As Long = 2
Const COL_ITEM_QTY As Long = 3
Const COL_ITEM_CONS_QTY As Long = 4
Const COL_ITEM_SELLPRICE As Long = 5
Const COL_ITEM_INCTOTAL As Long = 6
Const COL_ITEM_BUYQTY As Long = 7
Const COL_ITEM_SELLQTY As Long = 8
Const COL_ITEM_SELLDISC As Long = 9
Const COL_ITEM_EVENTNO As Long = 10
Const COL_ITEM_SUBEVENTNO As Long = 11
Const COL_ITEM_EVENT_QTY As Long = 12
Const COL_ITEM_EVENT_DISCQTY As Long = 13
Const COL_ITEM_FINAL_LINE As Long = 14
Const COL_ITEM_PRE_QTY As Long = 15
Const COL_ITEM_PRECONS_QTY As Long = 16
Const COL_ITEM_PRICE_USED As Long = 17

'Constants where the Events are held as they are being created
Const COL_DG_ITEMNO As Long = 1
Const COL_DG_DESC As Long = 2
Const COL_DG_EVENTNO As Long = 3
Const COL_DG_SUBNO As Long = 4
Const COL_DG_BUY_SKU As Long = 5
Const COL_DG_BUY_QTY As Long = 6
Const COL_DG_SELL_QTY As Long = 7
Const COL_DG_SELL_DISC As Long = 8
Const COL_DG_PRINT_LINE As Long = 9
Const COL_DG_DISC_TOTAL As Long = 10
Const COL_DG_EVENTDATE As Long = 11

'Constants where the summary of the events is calculated
Const COL_DG_TOT_DESC As Long = 1
Const COL_DG_TOT_EVENTNO As Long = 2
Const COL_DG_TOT_SUBNO As Long = 3
Const COL_DG_TOT_DISC As Long = 4


Dim colItems As Collection

Dim mcolLineEvents  As Collection
Dim mcolPrintEvents As Collection
Dim mcolPrintCoupons As Collection 'Coupons rewards given to customer

Dim mlngQSEventSeqNo As Long
Dim mlngMSEventSeqNo As Long
Dim mlngDGEventSeqNo As Long

Dim mcolAddedCoupons As Collection 'Coupons captured by customer
Dim mcolUnUsedCoupons  As Collection 'Working coupons in case Tender reset
Dim mUsedCoupons As Collection

Private TotalTimeInPreserveRestore As Double

Public Sub AddSKU(ByVal strSKUN As String, _
                ByVal lngQuantity As Long, _
                ByVal dblPrice As Double, _
                ByVal lngLineNo As Long)
                
    With sprdMaster
        .MaxRows = sprdMaster.MaxRows + 1
        .Row = sprdMaster.MaxRows
        .Col = COL_ITEM_NO
        .Text = lngLineNo
        .Col = COL_ITEM_PARTCODE
        .Text = strSKUN
        .Col = COL_ITEM_QTY
        .Text = lngQuantity
        .Col = COL_ITEM_CONS_QTY
        .Text = lngQuantity 'this is used to total up when applying events
        .Col = COL_ITEM_SELLPRICE
        .Text = dblPrice
        .Col = COL_ITEM_INCTOTAL
        .Text = dblPrice * lngQuantity
    End With
    
End Sub

Public Sub AddCoupon(ByVal strCouponID As String, _
                ByVal lngLineNo As Long)

    If (mcolAddedCoupons Is Nothing) Then Set mcolAddedCoupons = New Collection
    Call mcolAddedCoupons.Add(strCouponID & ":" & lngLineNo, strCouponID & ":" & lngLineNo)

End Sub

Public Sub AddCouponWithDescription(ByVal CouponId As String, ByVal LineNo As Long, Optional ByVal NonReusableCouponDescription As String = "")
    Dim CouponCount As Long

    If mcolAddedCoupons Is Nothing Then
        Set mcolAddedCoupons = New Collection
        CouponCount = 1
        Do While CouponCount <= NonReusableCouponDescriptions.Count
            NonReusableCouponDescriptions.Remove CouponCount
        Loop
    End If
    Call mcolAddedCoupons.Add(CouponId & ":" & LineNo, CouponId & ":" & LineNo)
    
    If NonReusableCouponDescription <> "" Then
On Error GoTo AddDescription
        If NonReusableCouponDescriptions(CouponId) = "" Then
On Error GoTo 0
AddDescription:
            Call NonReusableCouponDescriptions.Add(CouponId & ":" & NonReusableCouponDescription, CouponId)
        End If
    End If
End Sub

Public Sub DeleteSKU(ByVal strSKUN As String, _
                     ByVal lngLineNo As Long)
                     
Dim lngRowNo As Long
                
    For lngRowNo = 1 To sprdMaster.MaxRows Step 1
        sprdMaster.Row = lngRowNo
        sprdMaster.Col = COL_ITEM_NO
        If (Val(sprdMaster.value) = lngLineNo) Then
            sprdMaster.Col = COL_ITEM_PARTCODE
            If (sprdMaster.Text = strSKUN) Then
                Call sprdMaster.DeleteRows(lngRowNo, 1)
                sprdMaster.MaxRows = sprdMaster.MaxRows - 1
                Exit For
            End If 'sku matched
        End If 'Line number matched
    Next lngRowNo

End Sub

Public Sub DeleteCoupon(ByVal strCouponID As String, _
                     ByVal lngLineNo As Long)
                     
Dim lngRowNo As Long
                
    For lngRowNo = 1 To mcolAddedCoupons.Count Step 1
        If (mcolAddedCoupons(lngRowNo) = strCouponID & ":" & lngLineNo) Then
            Call mcolAddedCoupons.Remove(lngRowNo)
            Exit For
        End If 'Line number matched
    Next lngRowNo
End Sub

Public Sub UpdateSKUPrice(ByVal strSKUN As String, _
                          ByVal lngLineNo As Long, _
                          ByVal curPrice As Currency)

Dim lngRowNo    As Long
Dim lngQuantity As Long
                
    'step through lines and locate specified line and sku number
    For lngRowNo = 1 To sprdMaster.MaxRows Step 1
        sprdMaster.Row = lngRowNo
        sprdMaster.Col = COL_ITEM_NO
        If (Val(sprdMaster.value) = lngLineNo) Then
            sprdMaster.Col = COL_ITEM_PARTCODE
            If (sprdMaster.Text = strSKUN) Then
                'update item price and recalc line total
                sprdMaster.Col = COL_ITEM_QTY
                lngQuantity = Val(sprdMaster.value)
                sprdMaster.Col = COL_ITEM_SELLPRICE
                sprdMaster.value = curPrice
                sprdMaster.Col = COL_ITEM_INCTOTAL
                sprdMaster.Text = curPrice * lngQuantity
            End If 'sku matched
        End If 'Line number matched
    Next lngRowNo
    
End Sub

Public Property Get UsedCoupons() As Collection

    Set UsedCoupons = mUsedCoupons
End Property

Private Sub ConsolidateItems()

Dim lngRowNo     As Long
Dim strOldPart   As String
Dim dblQuantity  As Double

    'Sort items on purchase order so that they are grouped by Part Code
    sprdItems.SortKey(1) = COL_ITEM_PARTCODE
    sprdItems.SortKeyOrder(1) = SortKeyOrderAscending
    sprdItems.SortKey(2) = COL_ITEM_SELLPRICE
    sprdItems.SortKeyOrder(2) = SortKeyOrderDescending
    sprdItems.SortKey(3) = COL_ITEM_NO
    sprdItems.SortKeyOrder(3) = SortKeyOrderAscending
    Call sprdItems.Sort(COL_ITEM_NO, 1, sprdItems.MaxCols, sprdItems.MaxRows, SortByRow)
    'Step through list and add all items with the same partcode
    strOldPart = ""
    For lngRowNo = sprdItems.MaxRows To 1 Step -1
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_ITEM_PARTCODE
        If strOldPart = sprdItems.Text Then 'Item matches so add lines together
            sprdItems.Row = sprdItems.Row + 1 'move to next line (should be same Part code)
            sprdItems.Col = COL_ITEM_CONS_QTY
            dblQuantity = Val(sprdItems.Text)
            sprdItems.Text = 0
            'move to current line and addin quantity
            sprdItems.Row = lngRowNo
            sprdItems.Col = COL_ITEM_CONS_QTY
            sprdItems.Text = Val(sprdItems.Text) + dblQuantity
        End If
        sprdItems.Row = lngRowNo
        sprdItems.Col = COL_ITEM_PARTCODE
        strOldPart = sprdItems.Text
    Next lngRowNo
    
End Sub

Private Sub ExtractEvents(ByVal strEventType As String)

Dim lngRowNo   As Long
Dim oLine      As cLineEvent

    For lngRowNo = 1 To sprdEvents.MaxRows Step 1
        Set oLine = New cLineEvent
        sprdEvents.Row = lngRowNo
        sprdEvents.Col = COL_DG_ITEMNO
        oLine.lngLineNo = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_BUY_SKU
        oLine.strSKU = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_DESC
        oLine.strDesc = sprdEvents.Text
        
        oLine.strType = strEventType
        sprdEvents.Col = COL_DG_SELL_DISC
        oLine.dblDiscRate = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_SELL_QTY
        oLine.dblQuantity = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_DISC_TOTAL
        oLine.dblDiscTotal = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_EVENTNO
        oLine.strDealGroupNo = sprdEvents.Text
        sprdEvents.Col = COL_DG_EVENTDATE
        oLine.dteEndDate = sprdEvents.Text
        oLine.strEventNo = mcolPrintEvents(mcolPrintEvents.Count).strEventNo
        Call mcolLineEvents.Add(oLine)
    Next lngRowNo
    
End Sub

Private Sub ExtractTotalEvents(ByVal strEventType As String)

Dim lngRowNo     As Long
Dim oTotal       As cEventTotal
Dim lngEventNo   As Long
Dim strFixedDesc As String

    Select Case (strEventType)
        Case (QUANTITY_BREAK_TYPE):  lngEventNo = mlngQSEventSeqNo
                                     If (mblnUseDesc = False) Then strFixedDesc = QS_DESCRIPTION
        Case (MULTI_SKU_MATCH_TYPE): lngEventNo = mlngMSEventSeqNo
                                     If (mblnUseDesc = False) Then strFixedDesc = MS_DESCRIPTION
        Case (DEALGROUP_TYPE):       lngEventNo = mlngDGEventSeqNo
                                     If (mblnUseDesc = False) Then strFixedDesc = DG_DESCRIPTION
    End Select
    
    For lngRowNo = 1 To sprdEvents.MaxRows Step 1
        lngEventNo = lngEventNo + 1
        Set oTotal = New cEventTotal
        sprdEvents.Row = lngRowNo
        sprdEvents.Col = COL_DG_ITEMNO
        oTotal.lngLineNo = Val(sprdEvents.Text)
        oTotal.strEventNo = Format$(lngEventNo, "000000")
        sprdEvents.Col = COL_DG_DESC
        If (strFixedDesc = "") Then
            oTotal.strDesc = sprdEvents.Text
        Else
            oTotal.strDesc = strFixedDesc
        End If

        oTotal.strType = strEventType
        sprdEvents.Col = COL_DG_SELL_DISC
        oTotal.dblDiscRate = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_DISC_TOTAL
        oTotal.dblDiscTotal = Val(sprdEvents.Text)
        sprdEvents.Col = COL_DG_EVENTNO
        oTotal.strDealGroupNo = sprdEvents.Text
        sprdEvents.Col = COL_DG_EVENTDATE
        oTotal.dteEndDate = sprdEvents.Text
        Call mcolPrintEvents.Add(oTotal)
    Next lngRowNo
    
    Select Case (strEventType)
        Case (QUANTITY_BREAK_TYPE):  mlngQSEventSeqNo = lngEventNo
        Case (MULTI_SKU_MATCH_TYPE): mlngMSEventSeqNo = lngEventNo
        Case (DEALGROUP_TYPE):       mlngDGEventSeqNo = lngEventNo
    End Select
    
End Sub

Private Sub ExtractDealGroupTotalEvents()

Dim lngRowNo   As Long
Dim oTotal     As cEventTotal
    
    mlngDGEventSeqNo = mlngDGEventSeqNo + 1
    Set oTotal = New cEventTotal
    oTotal.strEventNo = Format$(mlngDGEventSeqNo, "000000")
    oTotal.strType = DEALGROUP_TYPE
    sprdEvents.Row = 1
    sprdEvents.Col = COL_DG_PRINT_LINE
    oTotal.lngLineNo = Val(sprdEvents.Text)
    sprdEvents.Col = COL_DG_DESC
    If (mblnUseDesc = False) Then
        oTotal.strDesc = DG_DESCRIPTION
    Else
        oTotal.strDesc = sprdEvents.Text
    End If
    sprdEvents.Col = COL_DG_EVENTNO
    oTotal.strDealGroupNo = sprdEvents.Text
    For lngRowNo = 1 To sprdEvents.MaxRows Step 1
        sprdEvents.Row = lngRowNo
        sprdEvents.Col = COL_DG_DISC_TOTAL
        oTotal.dblDiscTotal = oTotal.dblDiscTotal + Val(sprdEvents.Text)
    Next lngRowNo
    sprdEvents.Col = COL_DG_EVENTDATE
    oTotal.dteEndDate = sprdEvents.Text
    Call mcolPrintEvents.Add(oTotal)
    
End Sub

Public Function CheckEvents(ByRef colSummary As Collection, ByRef colPrintCoupons As Collection, ByRef colUnusedCoupons As Collection) As Collection
    Dim lngRowNo As Long
    Dim strCoupon As String
                
    Set mcolLineEvents = New Collection
    Set mcolPrintEvents = New Collection
    Set mcolPrintCoupons = New Collection
    Set mcolUnUsedCoupons = New Collection
    Set mcolCouponsUsedInTransaction = New Collection
    Set mUsedCoupons = New Collection
    
    sprdEvents.MaxRows = 0
    
    mlngQSEventSeqNo = 0
    mlngMSEventSeqNo = 0
    mlngDGEventSeqNo = 0
    
    If mcolAddedCoupons Is Nothing Then
        Set mcolAddedCoupons = New Collection
    End If
    'Copy Added coupons into Used coupons
    For lngRowNo = 1 To mcolAddedCoupons.Count Step 1
        strCoupon = mcolAddedCoupons(lngRowNo)
        Call mcolUnUsedCoupons.Add(mcolAddedCoupons(lngRowNo), strCoupon)
    Next lngRowNo
    
    'Firstly apply the Quantity Price Breaks
    Call CopyItemsFromMaster
    Call ConsolidateItems
    Call CheckForQuantityBreaks
    
    'Followed by the SKU Multibuys
    Call CheckForSKUMultiBuys
    
    ' now check for any get coupons for either of these event types
    If EnableGetCoupons Then
        Call CopyItemsFromMaster
        Call ConsolidateItems
        CheckForQuantityBreakGetCoupons
        CheckForSKUMultiBuyGetCoupons '- will it be logical to add Get Coupon to a Multibuy event?
    End If
    
    'Copy back in the items removed by the Quantity and SKU Multibuys
    Call CopyItemsFromMaster
    Call ConsolidateItems

    'Apply Deal group discounts
    If EnableGetCoupons Then
        Call CheckForDealGroupDiscounts
        ' now check for any get coupons for Deal Groups
        Call CopyItemsFromMaster
        Call ConsolidateItems
        CheckForDealGroupGetCoupons
    Else
        ' existing functionality prior to Get Coupons enabled
        CheckForDealGroupEvents
    End If
    
    Set colSummary = mcolPrintEvents
    Set CheckEvents = mcolLineEvents
    Set colPrintCoupons = mcolPrintCoupons
    Set colUnusedCoupons = mcolUnUsedCoupons
End Function

Private Sub CopyItemsFromMaster()

Dim lngColNo As Long
Dim lngRowNo As Long

    sprdItems.MaxRows = sprdMaster.MaxRows
    For lngColNo = 1 To sprdMaster.MaxCols Step 1
        sprdItems.Col = lngColNo
        sprdMaster.Col = lngColNo
        For lngRowNo = 1 To sprdMaster.MaxRows Step 1
            sprdItems.Row = lngRowNo
            sprdMaster.Row = lngRowNo
            sprdItems.Text = sprdMaster.Text
        Next lngRowNo
    Next lngColNo

End Sub


Public Sub CheckForDealGroupEvents()

Const MIX_MATCH_TYPE As String = "M"

Dim oEventMaster    As cOPEvents_Wickes.cEventMaster
'Dim oEventDealGroup As cOPEvents_Wickes.cEventDealGroup
Dim oEventMixMatch  As cOPEvents_Wickes.cEventMixMatch
Dim colOPMaster     As Collection
Dim colOPDealGroup  As Collection
Dim colOPMixMatch   As Collection
Dim lngMasterNo     As Long
Dim lngLineNo       As Long
Dim lngDealNo       As Long
Dim lngMixMatchNo   As Long
Dim lngSKUNo        As Long
Dim strNow          As String
Dim strStartAt      As String
Dim strEndAt        As String
Dim strSKU          As String
Dim strDealGroupSKU As String
Dim blnItemMatched  As Boolean
Dim blnGroupMatched As Boolean
Dim blnAnyItemMatch As Boolean
Dim lngQuantity     As Double
Dim lngTotalQty     As Double
Dim lngLineQty      As Long
Dim dblDiscQty      As Double
Dim dblDiscLeft     As Double
Dim blnItemUsed     As Boolean
Dim lngMatchBalance As Long
Dim lngQtyUsed      As Long
Dim dblOrigTotal    As Double
Dim dblDealTotal    As Double
Dim lngEventLineNo  As Long
Dim lngConsLineNo   As Long
Dim lngItemNo       As Long
Dim dblLineValue    As Double
Dim strEventDesc    As String

'Dim oRSelector      As CRowSelector
'Dim oBOSortKeys     As CBoSortKeys
    
Dim rsDealGroups    As Recordset
Dim rsEventMasters  As Recordset

Dim strLookupSku    As String

On Error GoTo Error_DealGroups

    strNow = Format$(Now, "YYYYMMDDHHNN")
        
    'Set Up sorting keys to use index and froce higher quantities first
'    Set oBOSortKeys = m_oSession.Root.CreateUtilityObject("CBoSortKeys")
'    Call oBOSortKeys.Add(FID_OPMASTER_EventType, True)
'    Call oBOSortKeys.Add(FID_OPMASTER_EventKey, True)
'    Call oBOSortKeys.Add(FID_OPMASTER_EventKey2, True)
'    Call oBOSortKeys.Add(FID_OPMASTER_EventNumber, True)
'
'    Set oRSelector = m_oSession.Database.CreateBoSelector(CLASSID_OPMASTER)
'
'    Call oRSelector.AddSelectValue(CMP_LESSEQUALTHAN, FID_OPMASTER_StartDate, Date)
'    Call oRSelector.AddSelectValue(CMP_GREATEREQUALTHAN, FID_OPMASTER_EndDate, Date)
'    Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_EventType, DEALGROUP_TYPE)
'    Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_Deleted, False)
'    Set colOPMaster = modEvents.m_oSession.Database.GetSortedBoCollection(oRSelector, oBOSortKeys)

    Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Getting DealGroups")
    Set rsDealGroups = DealGroupRecordset() 'oEventMaster.EventNumber, Right$(oEventMaster.EventKey2, 6))
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Got DealGroups: " & rsDealGroups.RecordCount)

    Set rsEventMasters = EventMasterRecordset("DG", rsDealGroups)

    sprdEvents.MaxRows = 0
    
    'Set oEventDealGroup = m_oSession.Database.CreateBusinessObject(CLASSID_OPDEALGROUP)
    Set oEventMixMatch = m_oSession.Database.CreateBusinessObject(CLASSID_OPMIXMATCH)
    
    If Not rsEventMasters Is Nothing Then
        '**********************************
        '** Process Mix and Match Offers **
        '**********************************
        Do While rsEventMasters.EOF = False
            'Check if matching Header for Master Event - stored by Deal groups
            If CheckHeaderValid(rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc, True) = True Then
                If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsEventMasters("BUYCPN")) = True) Then
                    'get list of all Deal Groups within the specific event and sub event
                    rsDealGroups.Filter = "EventNumber='" & rsEventMasters!EventNumber & "' and DealGroupNumber='" & Right$(rsEventMasters!EventKey2, 6) & "'"
                'Call DebugMsg("asdf", "asdf1", 0, "Applied Filter")
                    
                    
    ''Call DebugMsg("asdf", "asdf1", 0, "Start Building String")
                    dblDealTotal = rsEventMasters!SpecialPrice
                    If rsDealGroups.RecordCount <> 0 Then
                        blnGroupMatched = True
                    Else
                        blnGroupMatched = False
                    End If
                    'Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Processing (" & colOPDealGroup.Count & ")" & oEventMaster.EventNumber & "-" & oEventMaster.EventKey2 & "-" & oEventMaster.Priority)
                    While (blnGroupMatched = True) And (CheckCouponsMatch(rsEventMasters("BUYCPN")) = True)
                        dblOrigTotal = 0
                        sprdEvents.MaxRows = 0
                        lngEventLineNo = 0
                        Call PreserveQuantities
                        'Step through each Event and process (check for any matches)
                        'Debug.Print "Deal Group Count: " & colOPDealGroup.Count
                        'Debug.Print "Deal Group Count: " & rsDealGroups.RecordCount
                        rsDealGroups.MoveFirst
                        Do While rsDealGroups.EOF = False
                            strLookupSku = rsDealGroups!ItemKey
                            'Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Processing " & rsDealGroups!ItemKey & "-" & rsEventMasters!EventNumber & "-" & rsEventMasters!Priority)
                            blnGroupMatched = True
                            'Determine whether Deal Group is based on single SKU's or Mix&Match Groups
                            lngMatchBalance = rsDealGroups!Quantity
                            If rsDealGroups!DealType = MIX_MATCH_TYPE Then
                                'For each Mix Match - get list of Active SKUS to attempt to match
                                Call oEventMixMatch.IBo_ClearLoadFilter
                                Call oEventMixMatch.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_MixMatchGroupID, strLookupSku)
                                Call oEventMixMatch.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_Deleted, False)
                                Set colOPMixMatch = oEventMixMatch.IBo_LoadMatches
                                strDealGroupSKU = ""
                                For lngMixMatchNo = 1 To colOPMixMatch.Count Step 1
                                    Set oEventMixMatch = colOPMixMatch(CLng(lngMixMatchNo))
                                    strDealGroupSKU = strDealGroupSKU & oEventMixMatch.PartCode & "|"
                                Next lngMixMatchNo
                                Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Deal Items " & strDealGroupSKU)
                                'Step through list of items within Mix Match Group and see if SKU can be found
                                For lngLineNo = 1 To sprdItems.MaxRows Step 1
                                    sprdItems.Row = lngLineNo
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngQuantity = Val(sprdItems.Text)
                                    sprdItems.Col = COL_ITEM_PARTCODE
                                    'Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Item " & lngMixMatchNo & "-(SKU" & oEventMixMatch.PartCode & ") to bought " & sprdItems.Text)
                                    If (InStr(strDealGroupSKU, sprdItems.Text) > 0) And (lngQuantity > 0) Then
                                        'Update entry with Event details
                                        sprdItems.Col = COL_ITEM_QTY
                                        'Check if line will fulfil quantity required
                                        If Val(sprdItems.value) >= lngMatchBalance Then
                                            sprdItems.Col = COL_ITEM_QTY
                                            lngQtyUsed = lngMatchBalance
                                            sprdItems.value = sprdItems.value - lngMatchBalance
                                            lngMatchBalance = 0
                                        Else
                                            lngQtyUsed = Val(sprdItems.value)
                                            lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                            sprdItems.value = 0
                                        End If
                                        'when item found, copy line to Event History and deplete qty available
                                        Call CopyItemToEvent(strSKU, lngQtyUsed, strEventDesc, _
                                            rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!EDAT, lngEventLineNo, _
                                            lngConsLineNo, (rsDealGroups!ErosionPercentage / rsDealGroups!Quantity) * lngQtyUsed, dblOrigTotal)
                                        'If No of items has been found then exit to next part of deal to match
                                        If (lngMatchBalance = 0) Then Exit For
                                    End If
                                Next lngLineNo
                                'After checking all items in Group - flag if there was a match
                                'in order to proceed to next group or skip to next Header
                                If (lngMatchBalance > 0) Then
                                    blnGroupMatched = False
                                    Exit Do 'do not check any other items in Deal Group
                                End If
                            Else
                                'Step through list of items and see if SKU can be found
                                lngLineNo = -1
                                Do
                                    lngLineNo = sprdItems.SearchCol(COL_ITEM_PARTCODE, lngLineNo, sprdItems.MaxRows, strLookupSku, SearchFlagsSortedAscending)
                                    If lngLineNo = -1 Then Exit Do
    
                                    sprdItems.Row = lngLineNo
                                    'Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Item " & lngMixMatchNo & "-(MM" & rsDealGroups!ItemKey & ") to bought " & sprdItems.Text)
                                    sprdItems.Col = COL_ITEM_QTY
                                    'Check if line will fulfil quantity required
                                    If Val(sprdItems.value) >= lngMatchBalance Then
                                        lngQtyUsed = lngMatchBalance
                                        sprdItems.value = sprdItems.value - lngMatchBalance
                                        lngMatchBalance = 0
                                        'If force loop to use same line again?
                                    Else
                                        lngQtyUsed = Val(sprdItems.value)
                                        lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                        sprdItems.value = 0
                                    End If
                                    'when item found, copy line to Event History and deplete qty available
                                    Call CopyItemToEvent(strSKU, lngQtyUsed, strEventDesc, rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!EDAT, lngEventLineNo, lngConsLineNo, (rsDealGroups!ErosionPercentage / rsDealGroups!Quantity) * lngQtyUsed, dblOrigTotal)

                                    If lngMatchBalance = 0 Then Exit Do ' lngLineNo = sprdItems.MaxRows 'force loop to exit

                                Loop
                                
                                If (lngMatchBalance > 0) Then
                                    'Event was incomplete so restore deducted values
                                    blnGroupMatched = False
                                    Exit Do 'skip to next Deal Group
                                End If
                            End If
                            rsDealGroups.MoveNext
                        Loop 'Next oEventDealGroup
                        
                        'After processing all the items within the Deal Group - Check if
                        'group was match else reset to starting values
                        If (blnGroupMatched = True) Then
                            'calculate how much discount must be given
                            dblOrigTotal = Round(dblOrigTotal - dblDealTotal, 2)
                            If (dblOrigTotal >= 0) Then 'ensure deal total is more than goods total
                                dblDiscLeft = dblOrigTotal
                                For lngLineNo = 1 To sprdEvents.MaxRows - 1 Step 1
                                    sprdEvents.Row = lngLineNo
                                    sprdEvents.Col = COL_DG_SELL_DISC
                                    dblLineValue = Round(Val(sprdEvents.value) / 100 * dblOrigTotal, 2)
                                    sprdEvents.Col = COL_DG_DISC_TOTAL
                                    sprdEvents.value = dblLineValue
                                    dblDiscLeft = dblDiscLeft - dblLineValue
                                Next lngLineNo
                                'If over allocated discount then put back onto first item to balance out
                                If (dblDiscLeft < 0) Then
                                    sprdEvents.Row = 1
                                    sprdEvents.Col = COL_DG_SELL_DISC
                                    dblLineValue = Round(Val(sprdEvents.value) / 100 * dblOrigTotal, 2)
                                    dblLineValue = Round(dblLineValue + dblDiscLeft, 2)
                                    sprdEvents.Col = COL_DG_DISC_TOTAL
                                    sprdEvents.value = dblLineValue
                                    dblDiscLeft = 0
                                End If
                                sprdEvents.Row = sprdEvents.MaxRows
                                sprdEvents.Col = COL_DG_DISC_TOTAL
                                sprdEvents.value = dblDiscLeft
                            Else
                                dblDiscLeft = dblOrigTotal
                            End If 'discount price is less than goods totals
                            
                            'Remove items that have events and recalc the consolidated quantities
                            For lngLineNo = sprdItems.MaxRows To 1 Step -1
                                sprdItems.Row = lngLineNo
                                sprdItems.Col = COL_ITEM_QTY
                                If (Val(sprdItems.value) = 0) Then
                                    Call sprdItems.DeleteRows(lngLineNo, 1)
                                    sprdItems.MaxRows = sprdItems.MaxRows - 1
                                End If
                            Next
                            If (Val(rsEventMasters!BUYCPN) > 0) Then
                                RemoveMatchedCoupon (rsEventMasters!BUYCPN)
                                mcolCouponsUsedInTransaction.Add (rsEventMasters!BUYCPN)
                            End If
                            If (Val(rsEventMasters!GETCPN) > 0) Then mcolPrintCoupons.Add (rsEventMasters!GETCPN & ":" & lngEventLineNo)
                            Call ConsolidateItems
                            
                            If (Round(dblDiscLeft, 2) >= 0) Then
                                Call ExtractDealGroupTotalEvents
                                Call ExtractEvents(DEALGROUP_TYPE)
                            End If
                           'After checking all items in Group - flag if there was a match
                        'in order to proceed to next group or skip to next Header
                        Else
                            Call RestoreQuantities
                        End If
                        'dblDiscQty = rsDealGroups!Quantity
                    Wend
                End If 'date and time valid for header record
            End If 'Header was found and not deleted
            rsEventMasters.MoveNext
        Loop
    End If
    
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Leaving DealGroups")
    Exit Sub
    
Error_DealGroups:
    Call Err.Raise(Err.Number, "CheckForDealGroupEvents", Err.Description)
    Resume Next
    
End Sub

Public Sub CheckForDealGroupDiscounts()
    Const MIX_MATCH_TYPE As String = "M"
    Dim oEventMixMatch  As cOPEvents_Wickes.cEventMixMatch
    Dim colOPMixMatch   As Collection
    Dim lngLineNo       As Long
    Dim lngMixMatchNo   As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strSKU          As String
    Dim strDealGroupSKU As String
    Dim blnGroupMatched As Boolean
    Dim lngQuantity     As Double
    Dim dblDiscLeft     As Double
    Dim lngMatchBalance As Long
    Dim lngQtyUsed      As Long
    Dim dblOrigTotal    As Double
    Dim dblDealTotal    As Double
    Dim lngEventLineNo  As Long
    Dim lngConsLineNo   As Long
    Dim dblLineValue    As Double
    Dim strEventDesc    As String
    Dim rsDealGroups    As Recordset
    Dim rsEventMasters  As Recordset
    Dim strLookupSku    As String

On Error GoTo Error_DealGroups

    strNow = Format$(Now, "YYYYMMDDHHNN")
        
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupDiscounts", endlDebug, "Getting DealGroups")
    Set rsDealGroups = DealGroupRecordset()
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupDiscounts", endlDebug, "Got DealGroups: " & rsDealGroups.RecordCount)
    
    Set rsEventMasters = EventMasterRecordset("DG", rsDealGroups)
    
    sprdEvents.MaxRows = 0
    
    Set oEventMixMatch = m_oSession.Database.CreateBusinessObject(CLASSID_OPMIXMATCH)
    
    If Not rsEventMasters Is Nothing Then
        ' Do not include any Get Coupon events
        rsEventMasters.Filter = "GETCPN = '0000000'"
        
        '**********************************
        '** Process Mix and Match Offers **
        '**********************************
        Do While rsEventMasters.EOF = False
            'Check if matching Header for Master Event - stored by Deal groups
            If CheckHeaderValid(rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc, True) = True Then
                If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsEventMasters("BUYCPN")) = True) Then
                    'get list of all Deal Groups within the specific event and sub event
                    rsDealGroups.Filter = "EventNumber='" & rsEventMasters!EventNumber & "' " _
                                        & "And DealGroupNumber='" & Right$(rsEventMasters!EventKey2, 6) & "' "
                    dblDealTotal = rsEventMasters!SpecialPrice
                    If rsDealGroups.RecordCount <> 0 Then
                        blnGroupMatched = True
                    Else
                        blnGroupMatched = False
                    End If
                    
                    While (blnGroupMatched = True) And (CheckCouponsMatch(rsEventMasters("BUYCPN")) = True)
                        dblOrigTotal = 0
                        sprdEvents.MaxRows = 0
                        lngEventLineNo = 0
                        Call PreserveQuantities
                        'Step through each Event and process (check for any matches)
                        rsDealGroups.MoveFirst
                        Do While rsDealGroups.EOF = False
                            strLookupSku = rsDealGroups!ItemKey
                            blnGroupMatched = True
                            'Determine whether Deal Group is based on single SKU's or Mix&Match Groups
                            lngMatchBalance = rsDealGroups!Quantity
                            If rsDealGroups!DealType = MIX_MATCH_TYPE Then
                                'For each Mix Match - get list of Active SKUS to attempt to match
                                Call oEventMixMatch.IBo_ClearLoadFilter
                                Call oEventMixMatch.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_MixMatchGroupID, strLookupSku)
                                Call oEventMixMatch.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_Deleted, False)
                                Set colOPMixMatch = oEventMixMatch.IBo_LoadMatches
                                strDealGroupSKU = ""
                                For lngMixMatchNo = 1 To colOPMixMatch.Count Step 1
                                    Set oEventMixMatch = colOPMixMatch(CLng(lngMixMatchNo))
                                    strDealGroupSKU = strDealGroupSKU & oEventMixMatch.PartCode & "|"
                                Next lngMixMatchNo
                                Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Deal Items " & strDealGroupSKU)
                                'Step through list of items within Mix Match Group and see if SKU can be found
                                For lngLineNo = 1 To sprdItems.MaxRows Step 1
                                    sprdItems.Row = lngLineNo
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngQuantity = Val(sprdItems.Text)
                                    sprdItems.Col = COL_ITEM_PARTCODE
                                    'Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Item " & lngMixMatchNo & "-(SKU" & oEventMixMatch.PartCode & ") to bought " & sprdItems.Text)
                                    If (InStr(strDealGroupSKU, sprdItems.Text) > 0) And (lngQuantity > 0) Then
                                        'Update entry with Event details
                                        sprdItems.Col = COL_ITEM_QTY
                                        'Check if line will fulfil quantity required
                                        If Val(sprdItems.value) >= lngMatchBalance Then
                                            sprdItems.Col = COL_ITEM_QTY
                                            lngQtyUsed = lngMatchBalance
                                            sprdItems.value = sprdItems.value - lngMatchBalance
                                            lngMatchBalance = 0
                                        Else
                                            lngQtyUsed = Val(sprdItems.value)
                                            lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                            sprdItems.value = 0
                                        End If
                                        'when item found, copy line to Event History and deplete qty available
                                        Call CopyItemToEvent(strSKU, lngQtyUsed, strEventDesc, _
                                            rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!EDAT, lngEventLineNo, _
                                            lngConsLineNo, (rsDealGroups!ErosionPercentage / rsDealGroups!Quantity) * lngQtyUsed, dblOrigTotal)
                                        'If No of items has been found then exit to next part of deal to match
                                        If (lngMatchBalance = 0) Then
                                            Exit For
                                        End If
                                    End If
                                Next lngLineNo
                                'After checking all items in Group - flag if there was a match
                                'in order to proceed to next group or skip to next Header
                                If (lngMatchBalance > 0) Then
                                    blnGroupMatched = False
                                    Exit Do 'do not check any other items in Deal Group
                                End If
                            Else
                                'Step through list of items and see if SKU can be found
                                lngLineNo = -1
                                Do
                                    lngLineNo = sprdItems.SearchCol(COL_ITEM_PARTCODE, lngLineNo, sprdItems.MaxRows, strLookupSku, SearchFlagsSortedAscending)
                                    If lngLineNo = -1 Then
                                        Exit Do
                                    End If
                                    sprdItems.Row = lngLineNo
                                    'Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Item " & lngMixMatchNo & "-(MM" & rsDealGroups!ItemKey & ") to bought " & sprdItems.Text)
                                    sprdItems.Col = COL_ITEM_QTY
                                    'Check if line will fulfil quantity required
                                    If Val(sprdItems.value) >= lngMatchBalance Then
                                        lngQtyUsed = lngMatchBalance
                                        sprdItems.value = sprdItems.value - lngMatchBalance
                                        lngMatchBalance = 0
                                        'If force loop to use same line again?
                                    Else
                                        lngQtyUsed = Val(sprdItems.value)
                                        lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                        sprdItems.value = 0
                                    End If
                                    'when item found, copy line to Event History and deplete qty available
                                    Call CopyItemToEvent(strSKU, lngQtyUsed, strEventDesc, rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!EDAT, lngEventLineNo, lngConsLineNo, (rsDealGroups!ErosionPercentage / rsDealGroups!Quantity) * lngQtyUsed, dblOrigTotal)
                                    If lngMatchBalance = 0 Then
                                        Exit Do ' lngLineNo = sprdItems.MaxRows 'force loop to exit
                                    End If
                                Loop
                                If (lngMatchBalance > 0) Then
                                    'Event was incomplete so restore deducted values
                                    blnGroupMatched = False
                                    Exit Do 'skip to next Deal Group
                                End If
                            End If
                            rsDealGroups.MoveNext
                        Loop 'Next oEventDealGroup
                        
                        'After processing all the items within the Deal Group - Check if
                        'group was match else reset to starting values
                        If (blnGroupMatched = True) Then
                            'calculate how much discount must be given
                            dblOrigTotal = Round(dblOrigTotal - dblDealTotal, 2)
                            If (dblOrigTotal >= 0) Then 'ensure deal total is more than goods total
                                dblDiscLeft = dblOrigTotal
                                For lngLineNo = 1 To sprdEvents.MaxRows - 1 Step 1
                                    sprdEvents.Row = lngLineNo
                                    sprdEvents.Col = COL_DG_SELL_DISC
                                    dblLineValue = Round(Val(sprdEvents.value) / 100 * dblOrigTotal, 2)
                                    sprdEvents.Col = COL_DG_DISC_TOTAL
                                    sprdEvents.value = dblLineValue
                                    dblDiscLeft = dblDiscLeft - dblLineValue
                                Next lngLineNo
                                'If over allocated discount then put back onto first item to balance out
                                If (dblDiscLeft < 0) Then
                                    sprdEvents.Row = 1
                                    sprdEvents.Col = COL_DG_SELL_DISC
                                    dblLineValue = Round(Val(sprdEvents.value) / 100 * dblOrigTotal, 2)
                                    dblLineValue = Round(dblLineValue + dblDiscLeft, 2)
                                    sprdEvents.Col = COL_DG_DISC_TOTAL
                                    sprdEvents.value = dblLineValue
                                    dblDiscLeft = 0
                                End If
                                sprdEvents.Row = sprdEvents.MaxRows
                                sprdEvents.Col = COL_DG_DISC_TOTAL
                                sprdEvents.value = dblDiscLeft
                            Else
                                dblDiscLeft = dblOrigTotal
                            End If 'discount price is less than goods totals
                            
                            'Remove items that have events and recalc the consolidated quantities
                            For lngLineNo = sprdItems.MaxRows To 1 Step -1
                                sprdItems.Row = lngLineNo
                                sprdItems.Col = COL_ITEM_QTY
                                If (Val(sprdItems.value) = 0) Then
                                    Call sprdItems.DeleteRows(lngLineNo, 1)
                                    sprdItems.MaxRows = sprdItems.MaxRows - 1
                                End If
                            Next
                            If (Val(rsEventMasters!BUYCPN) > 0) Then
                                RemoveMatchedCoupon (rsEventMasters!BUYCPN)
                                mcolCouponsUsedInTransaction.Add (rsEventMasters!BUYCPN)
                            End If
                            Call ConsolidateItems
                            
                            If (Round(dblDiscLeft, 2) >= 0) Then
                                Call ExtractDealGroupTotalEvents
                                Call ExtractEvents(DEALGROUP_TYPE)
                            End If
                           'After checking all items in Group - flag if there was a match
                        'in order to proceed to next group or skip to next Header
                        Else
                            Call RestoreQuantities
                        End If
                    Wend
                End If 'date and time valid for header record
            End If 'Header was found and not deleted
            rsEventMasters.MoveNext
        Loop
    End If
    
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupDiscounts", endlDebug, "Leaving DealGroups")
    Exit Sub
    
Error_DealGroups:
    Call Err.Raise(Err.Number, "CheckForDealGroupDiscounts", Err.Description)
    Resume Next
End Sub

Public Sub CheckForDealGroupGetCoupons()
    Const MIX_MATCH_TYPE As String = "M"
    Dim oEventMixMatch  As cOPEvents_Wickes.cEventMixMatch
    Dim colOPMixMatch   As Collection
    Dim lngLineNo       As Long
    Dim lngMixMatchNo   As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strSKU          As String
    Dim strDealGroupSKU As String
    Dim blnGroupMatched As Boolean
    Dim lngQuantity     As Double
    Dim dblDiscLeft     As Double
    Dim lngMatchBalance As Long
    Dim lngQtyUsed      As Long
    Dim dblOrigTotal    As Double
    Dim dblDealTotal    As Double
    Dim lngEventLineNo  As Long
    Dim lngConsLineNo   As Long
    Dim dblLineValue    As Double
    Dim strEventDesc    As String
    Dim rsDealGroups    As Recordset
    Dim rsEventMasters  As Recordset
    Dim strLookupSku    As String

On Error GoTo Error_DealGroups

    strNow = Format$(Now, "YYYYMMDDHHNN")
        
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupGetCoupons", endlDebug, "Getting DealGroups")
    Set rsDealGroups = DealGroupRecordset() 'oEventMaster.EventNumber, Right$(oEventMaster.EventKey2, 6))
    Call DebugMsg(MODULE_NAME, "CheckForDealGroupGetCoupons", endlDebug, "Got DealGroups: " & rsDealGroups.RecordCount)
    
    Set rsEventMasters = EventMasterRecordset("DG", rsDealGroups)
    
    Set oEventMixMatch = m_oSession.Database.CreateBusinessObject(CLASSID_OPMIXMATCH)
    
    If Not rsEventMasters Is Nothing Then
        ' Only include Get Coupon events
        rsEventMasters.Filter = "GETCPN <> '0000000'"
        '**********************************
        '** Process Mix and Match Offers **
        '**********************************
        Do While rsEventMasters.EOF = False
            'Check if matching Header for Master Event - stored by Deal groups
            If CheckHeaderValid(rsEventMasters!EventNumber, rsEventMasters!Priority, rsEventMasters!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True Then
                If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsEventMasters("BUYCPN")) = True) Then
                    'get list of all Deal Groups within the specific event and sub event
                    rsDealGroups.Filter = "EventNumber='" & rsEventMasters!EventNumber & "' " _
                                        & "And DealGroupNumber='" & Right$(rsEventMasters!EventKey2, 6) & "' "
                    dblDealTotal = rsEventMasters!SpecialPrice
                    If rsDealGroups.RecordCount <> 0 Then
                        blnGroupMatched = True
                    Else
                        blnGroupMatched = False
                    End If
                    
                    While (blnGroupMatched = True) And (CheckCouponsMatch(rsEventMasters("BUYCPN")) = True)
                        dblOrigTotal = 0
                        lngEventLineNo = 0
                        Call PreserveQuantities
                        'Step through each Event and process (check for any matches)
                        
                        rsDealGroups.MoveFirst
                        Do While rsDealGroups.EOF = False
                            strLookupSku = rsDealGroups!ItemKey
                            
                            blnGroupMatched = True
                            'Determine whether Deal Group is based on single SKU's or Mix&Match Groups
                            lngMatchBalance = rsDealGroups!Quantity
                            If rsDealGroups!DealType = MIX_MATCH_TYPE Then
                                'For each Mix Match - get list of Active SKUS to attempt to match
                                Call oEventMixMatch.IBo_ClearLoadFilter
                                Call oEventMixMatch.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_MixMatchGroupID, strLookupSku)
                                Call oEventMixMatch.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_Deleted, False)
                                Set colOPMixMatch = oEventMixMatch.IBo_LoadMatches
                                strDealGroupSKU = ""
                                For lngMixMatchNo = 1 To colOPMixMatch.Count Step 1
                                    Set oEventMixMatch = colOPMixMatch(CLng(lngMixMatchNo))
                                    strDealGroupSKU = strDealGroupSKU & oEventMixMatch.PartCode & "|"
                                Next lngMixMatchNo
                                Call DebugMsg(MODULE_NAME, "CheckForDealGroupEvents", endlDebug, "Deal Items " & strDealGroupSKU)
                                'Step through list of items within Mix Match Group and see if SKU can be found
                                For lngLineNo = 1 To sprdItems.MaxRows Step 1
                                    sprdItems.Row = lngLineNo
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngQuantity = Val(sprdItems.Text)
                                    sprdItems.Col = COL_ITEM_PARTCODE
                                    
                                    If (InStr(strDealGroupSKU, sprdItems.Text) > 0) And (lngQuantity > 0) Then
                                        'Update entry with Event details
                                        sprdItems.Col = COL_ITEM_QTY
                                        'Check if line will fulfil quantity required
                                        If Val(sprdItems.value) >= lngMatchBalance Then
                                            sprdItems.Col = COL_ITEM_QTY
                                            lngQtyUsed = lngMatchBalance
                                            sprdItems.value = sprdItems.value - lngMatchBalance
                                            lngMatchBalance = 0
                                        Else
                                            lngQtyUsed = Val(sprdItems.value)
                                            lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                            sprdItems.value = 0
                                        End If
                                        'when item found, check print line and add to total
                                        sprdItems.Col = COL_ITEM_NO
                                        'If row is after previous rows then update all events to print here
                                        If (lngEventLineNo < Val(sprdItems.Text)) Then
                                            lngEventLineNo = Val(sprdItems.Text)
                                        End If
                                        sprdItems.Col = COL_ITEM_SELLPRICE
                                        'Add up original total of items purchased
                                        dblOrigTotal = dblOrigTotal + (sprdItems.value * lngQtyUsed)
                                            
                                        'If No of items has been found then exit to next part of deal to match
                                        If (lngMatchBalance = 0) Then
                                            Exit For
                                        End If
                                    End If
                                Next lngLineNo
                                'After checking all items in Group - flag if there was a match
                                'in order to proceed to next group or skip to next Header
                                If (lngMatchBalance > 0) Then
                                    blnGroupMatched = False
                                    Exit Do 'do not check any other items in Deal Group
                                End If
                            Else
                                'Step through list of items and see if SKU can be found
                                lngLineNo = -1
                                Do
                                    lngLineNo = sprdItems.SearchCol(COL_ITEM_PARTCODE, lngLineNo, sprdItems.MaxRows, strLookupSku, SearchFlagsSortedAscending)
                                    If lngLineNo = -1 Then
                                        Exit Do
                                    End If
                                    
                                    sprdItems.Row = lngLineNo
                                    
                                    sprdItems.Col = COL_ITEM_QTY
                                    'Check if line will fulfil quantity required
                                    If Val(sprdItems.value) >= lngMatchBalance Then
                                        lngQtyUsed = lngMatchBalance
                                        sprdItems.value = sprdItems.value - lngMatchBalance
                                        lngMatchBalance = 0
                                        'If force loop to use same line again?
                                    Else
                                        lngQtyUsed = Val(sprdItems.value)
                                        lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                        sprdItems.value = 0
                                    End If
                                    'when item found, check print line and add to total
                                    sprdItems.Col = COL_ITEM_NO
                                    'If row is after previous rows then update all events to print here
                                    If (lngEventLineNo < Val(sprdItems.Text)) Then
                                        lngEventLineNo = Val(sprdItems.Text)
                                    End If
                                    sprdItems.Col = COL_ITEM_SELLPRICE
                                    'Add up original total of items purchased
                                    dblOrigTotal = dblOrigTotal + (sprdItems.value * lngQtyUsed)

                                    If lngMatchBalance = 0 Then
                                        Exit Do ' lngLineNo = sprdItems.MaxRows 'force loop to exit
                                    End If
                                Loop
                                
                                If (lngMatchBalance > 0) Then
                                    'Event was incomplete so restore deducted values
                                    blnGroupMatched = False
                                    Exit Do 'skip to next Deal Group
                                End If
                            End If
                            rsDealGroups.MoveNext
                        Loop 'Next oEventDealGroup
                        'After processing all the items within the Deal Group - Check if
                        'group was match else reset to starting values
                        If (blnGroupMatched = True) Then
                            If (Val(rsEventMasters!GETCPN) > 0) Then
                                mcolPrintCoupons.Add (rsEventMasters!GETCPN & ":" & lngEventLineNo)
                            End If
                            Call ConsolidateItems
                           'After checking all items in Group - flag if there was a match
                        'in order to proceed to next group or skip to next Header
                        Else
                            Call RestoreQuantities
                        End If
                    Wend
                End If 'date and time valid for header record
            End If 'Header was found and not deleted
            rsEventMasters.MoveNext
        Loop
    End If

    Call DebugMsg(MODULE_NAME, "CheckForDealGroupGetCoupons", endlDebug, "Leaving DealGroups")

    Exit Sub

Error_DealGroups:
    Call Err.Raise(Err.Number, "CheckForDealGroupGetCoupons", Err.Description)
    Resume Next
End Sub

Private Sub CopyItemToEvent(ByVal strSKU As String, _
                            ByVal lngQtyUsed As Long, _
                            ByVal strDesc As String, _
                            ByVal strEventNo As String, _
                            ByVal strPriority As String, _
                            ByVal dteExpiryDate As Date, _
                            ByRef lngEventLineNo As Long, _
                            ByVal lngConsLineNo As Long, _
                            ByVal dblErosionRate As Double, _
                            ByRef dblOrigTotal As Double)

    sprdEvents.MaxRows = sprdEvents.MaxRows + 1
    sprdEvents.Row = sprdEvents.MaxRows
    sprdItems.Col = COL_ITEM_NO
    sprdEvents.Col = COL_DG_ITEMNO
    sprdEvents.Text = sprdItems.Text
    sprdEvents.Col = COL_DG_DESC
    sprdEvents.Text = strDesc
    sprdEvents.Col = COL_DG_EVENTNO
    sprdEvents.Text = strEventNo
    sprdEvents.Col = COL_DG_SUBNO
    sprdEvents.Text = strPriority
    sprdEvents.Col = COL_DG_BUY_SKU
    sprdEvents.Text = strSKU
    sprdEvents.Col = COL_DG_BUY_QTY
    sprdEvents.value = lngQtyUsed
    sprdItems.Col = COL_ITEM_NO
    sprdEvents.Col = COL_DG_ITEMNO
    sprdEvents.Text = sprdItems.Text
    sprdEvents.Col = COL_DG_PRINT_LINE
    'If row is after previous rows then update all events to print here
    If (lngEventLineNo < Val(sprdItems.Text)) Then
        sprdEvents.Row = -1
        sprdEvents.Text = sprdItems.Text
        lngEventLineNo = Val(sprdItems.Text)
    Else
        sprdEvents.Text = lngEventLineNo
    End If
    sprdEvents.Row = sprdEvents.MaxRows
    sprdItems.Col = COL_ITEM_SELLPRICE
    sprdEvents.Col = COL_DG_SELL_QTY
    sprdEvents.value = Val(sprdItems.value)
    sprdEvents.Col = COL_DG_EVENTDATE
    sprdEvents.value = dteExpiryDate
    'Add up original total of items purchased
    dblOrigTotal = dblOrigTotal + (sprdItems.value * lngQtyUsed)
    sprdEvents.Col = COL_DG_SELL_DISC
    sprdEvents.Text = dblErosionRate
End Sub

Public Function ExistsInCollection(ByVal oCol As Collection, ByVal vKey As Variant) As Boolean
    On Error Resume Next
    oCol.Item vKey
    ExistsInCollection = (Err.Number = 0)
    Err.Clear
End Function


Public Sub CheckForQuantityBreaks()
    Dim rsOPMaster      As Recordset
    Dim lngLineNo       As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strSKU          As String
    Dim strEventDesc    As String
    Dim lngTotalQty     As Long
    Dim lngItemNo       As Long
    Dim dblOrigTotal    As Double
    Dim lngEventLineNo  As Long
    
    Dim itemIndexBackup     As Long
    Dim itemInnerIndex      As Long
    Dim calculatedDiscount  As Double
    
    Dim currentLineNo       As String
    Dim currentItemPrice    As Double
    Dim itemPrices          As New Collection
    
    lngItemNo = 0
    Set rsOPMaster = QuantityBreaksRecordset(strSKU, lngTotalQty)
    While lngItemNo < sprdItems.MaxRows
        'Step through lines and find the Total Qty Line
        lngItemNo = lngItemNo + 1
        strSKU = ""
        sprdItems.Row = lngItemNo
        sprdItems.Col = COL_ITEM_CONS_QTY
        If Val(sprdItems.value) > 0 Then
            lngTotalQty = Val(sprdItems.value)
            sprdItems.Col = COL_ITEM_PARTCODE
            strSKU = sprdItems.Text
            'Check if discount Event exists for SKU and with current quantity for SKU
            strNow = Format$(Now, "YYYYMMDDHHNN")
            If EnableGetCoupons Then
                rsOPMaster.Filter = "EventKey = '" & strSKU & "' " _
                                  & "And ReqQty <= '" & Format$(lngTotalQty, "00000000") & "' " _
                                  & "And GETCPN = '0000000'"
            Else
                rsOPMaster.Filter = "EventKey = '" & strSKU & "' " _
                                  & "And ReqQty <= '" & Format$(lngTotalQty, "00000000") & "'"
            End If

            ' Clear events table
            sprdEvents.MaxRows = 0
            '************************************
            '** Process Quantity Breaks Offers **
            '************************************
            Do While Not rsOPMaster.EOF
                'Check if matching Header for Master Event - stored by Deal groups
                If CheckHeaderValid(rsOPMaster!EventNumber, rsOPMaster!Priority, rsOPMaster!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True Then
                    If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True) Then
                        ' Go through all lines having current SKU and add event entries
                        itemIndexBackup = sprdItems.Row
                        For itemInnerIndex = lngItemNo To sprdItems.MaxRows Step 1
                            sprdItems.Row = itemInnerIndex
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                ' Get current price per item for the current line
                                sprdItems.Col = COL_ITEM_NO
                                currentLineNo = sprdItems.Text
                                If ExistsInCollection(itemPrices, currentLineNo) Then
                                    currentItemPrice = itemPrices.Item(currentLineNo)
                                Else
                                    sprdItems.Col = COL_ITEM_SELLPRICE
                                    currentItemPrice = Val(sprdItems.value)
                                End If
                                
                                'check Event Price is less than Item Unit Price, else skip
                                If (Val(rsOPMaster!SpecialPrice) < currentItemPrice) Then
                                    ' Save our new lower price
                                    If ExistsInCollection(itemPrices, currentLineNo) Then
                                        itemPrices.Remove currentLineNo
                                        
                                        sprdEvents.DeleteRows 1, 1
                                        sprdEvents.MaxRows = sprdEvents.MaxRows - 1
                                    End If
                                    itemPrices.Add rsOPMaster!SpecialPrice.value, currentLineNo
                                    
                                    ' Add new event entry to spread
                                    sprdEvents.MaxRows = sprdEvents.MaxRows + 1
                                    lngEventLineNo = sprdEvents.MaxRows
                                    sprdEvents.Row = lngEventLineNo
                                    
                                    sprdEvents.Col = COL_DG_ITEMNO
                                    sprdEvents.Text = currentLineNo
                                    
                                    sprdEvents.Col = COL_DG_DESC
                                    sprdEvents.Text = strEventDesc
                                    
                                    sprdEvents.Col = COL_DG_EVENTNO
                                    sprdEvents.Text = rsOPMaster!EventNumber
                                    
                                    sprdEvents.Col = COL_DG_SUBNO
                                    sprdEvents.Text = rsOPMaster!Priority
                                    
                                    sprdEvents.Col = COL_DG_BUY_SKU
                                    sprdEvents.Text = strSKU
                                    
                                    sprdEvents.Col = COL_DG_EVENTDATE
                                    sprdEvents.Text = rsOPMaster!EDAT
                                    
                                    ' Set buy quantity from items to events
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngTotalQty = sprdItems.value
                                    
                                    sprdEvents.Col = COL_DG_BUY_QTY
                                    sprdEvents.Text = lngTotalQty
                                        
                                    ' get total
                                    sprdItems.Col = COL_ITEM_INCTOTAL
                                    dblOrigTotal = sprdItems.value
                                    
                                    ' calculate discount for the line
                                    calculatedDiscount = dblOrigTotal - (lngTotalQty * Val(rsOPMaster!SpecialPrice))
                                    
                                    ' set discount total
                                    sprdEvents.Col = COL_DG_DISC_TOTAL
                                    sprdEvents.value = calculatedDiscount
                                End If
                            End If
                        Next
                        sprdItems.Row = itemIndexBackup
                        
                        If (Val(rsOPMaster!BUYCPN) > 0) Then
                            Dim tmp As Long
                            tmp = mcolUnUsedCoupons.Count
                            Call RemoveMatchedCoupon(rsOPMaster!BUYCPN)
                            If (tmp <> mcolUnUsedCoupons.Count) Then
                                mcolCouponsUsedInTransaction.Add (rsOPMaster!BUYCPN)
                            End If
                        End If
                        
                        ' PROBABLY GetCoupons function can be REMOVED and that functionality could possibly replace by the following code
                        ' Add check for uniqueness
                        'If Not EnableGetCoupons Then
                        '    If (Val(rsOPMaster!GETCPN) > 0) And (sprdEvents.MaxRows > 0) Then
                        '        mcolPrintCoupons.Add (rsOPMaster!GETCPN & ":" & lngEventLineNo)
                        '    End If
                        'End If

                    End If 'Header had valid date and Time range
                End If 'Header was not deleted and retrieved

                ' go to the next event for current SKU and its quantity
                rsOPMaster.MoveNext
            Loop

            ' Save information from spreads for the line
            Call ExtractTotalEvents(QUANTITY_BREAK_TYPE)
            Call ExtractEvents(QUANTITY_BREAK_TYPE)
            sprdEvents.MaxRows = 0
        End If
    Wend
End Sub

Private Sub CheckForQuantityBreakGetCoupons()
    Dim rsOPMaster      As Recordset
    Dim lngLineNo       As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strSKU          As String
    Dim strEventDesc    As String
    Dim lngTotalQty     As Long
    Dim lngItemNo       As Long
    Dim Found As Boolean
    Dim EventTriggered As Boolean
    Dim PrintCouponCount As Integer
    
    lngItemNo = 0
    Set rsOPMaster = QuantityBreaksRecordset(strSKU, lngTotalQty)
    While lngItemNo < sprdItems.MaxRows
        'Step through lines and find the Total Qty Line
        lngItemNo = lngItemNo + 1
        strSKU = ""
        sprdItems.Row = lngItemNo
        sprdItems.Col = COL_ITEM_CONS_QTY
        If Val(sprdItems.value) > 0 Then
            lngTotalQty = Val(sprdItems.value)
            sprdItems.Col = COL_ITEM_PARTCODE
            strSKU = sprdItems.Text
            'Check if Event exists for SKU and with current quantity for SKU
            strNow = Format$(Now, "YYYYMMDDHHNN")
            rsOPMaster.Filter = "EventKey = '" & strSKU & "'" _
                              & "And ReqQty <= '" & Format$(lngTotalQty, "00000000") & "' " _
                              & "And GETCPN <> '0000000'"
            
            sprdEvents.MaxRows = 0
            '************************************
            '** Process Quantity Breaks Offers **
            '************************************
            Do While Not rsOPMaster.EOF
                'Check if matching Header for Master Event - stored by Deal groups
                If CheckHeaderValid(rsOPMaster!EventNumber, rsOPMaster!Priority, rsOPMaster!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True Then
                    If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True) Then
                        EventTriggered = False
                        'If matched, then reduce quantity in item list so not counted for next event check
                        For lngLineNo = sprdItems.Row To sprdItems.MaxRows Step 1
                            sprdItems.Row = lngLineNo
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                'check Event Price is less than Item Unit Price, else skip
                                sprdItems.Col = COL_ITEM_SELLPRICE
                                If (Val(sprdItems.value) > Val(rsOPMaster!SpecialPrice)) Then
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngTotalQty = sprdItems.value
                                    sprdItems.value = sprdItems.value - lngTotalQty
                                    sprdItems.Col = COL_ITEM_INCTOTAL
                                    EventTriggered = True
                                Else
                                    Call DebugMsg(MODULE_NAME, "CheckForQuantityBreaks", endlDebug, "Item Below Special Price-" & sprdItems.value & " SP=" & rsOPMaster!SpecialPrice)
                                End If 'Event Price is less then Unit Price
                            Else
                                Exit For
                            End If
                        Next
                        'Remove items for out-standing items for discounting with matching SKU and Items used
                        For lngLineNo = sprdItems.MaxRows To lngItemNo Step -1
                            sprdItems.Row = lngLineNo
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                sprdItems.Col = COL_ITEM_QTY
                                If (Val(sprdItems.value) = 0) Then
                                    Call sprdItems.DeleteRows(lngLineNo, 1)
                                    sprdItems.MaxRows = sprdItems.MaxRows - 1
                                End If
                            End If
                        Next
                        If (Val(rsOPMaster!GETCPN) > 0) Then
                            Found = False
                            ' Only add Coupon if not already in the list - only 1 coupon for 1 qs event,
                            ' i.e. do not get multiple coupons for same event no matter how many of the
                            ' items are in the basket
                            With mcolPrintCoupons
                                For PrintCouponCount = 1 To .Count
                                    If StrComp(Left$(.Item(PrintCouponCount), 7), rsOPMaster!GETCPN, vbTextCompare) = 0 Then
                                        Found = True
                                        Exit For
                                    End If
                                Next PrintCouponCount
                                If Not Found Then
                                    Call .Add(rsOPMaster!GETCPN & ":0")
                                End If
                            End With
                        End If
                        'Determine if need to step back a line, if Header replaced
                        sprdItems.Col = COL_ITEM_PARTCODE
                        sprdItems.Row = lngItemNo
                        If (sprdItems.Text <> strSKU) Then
                            lngItemNo = lngItemNo - 1 'step back one line, as it has been replaced
                        End If
                    End If 'Header had valid date and Time range
                End If 'Header was not deleted and retrieved
                If sprdItems.MaxRows = 0 Then
                    Exit Do
                End If
                rsOPMaster.MoveNext
            Loop
        End If
    Wend
End Sub

Public Sub CheckForSKUMultiBuys()
    Dim rsOPMaster      As Recordset
    Dim lngLineNo       As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strSKU          As String
    Dim lngQuantity     As Long
    Dim lngTotalQty     As Long
    Dim lngUsedQty      As Long
    Dim lngMatchBalance As Long
    Dim lngBoughtQty    As Long
    Dim lngItemNo       As Long
    Dim lngConsLineNo   As Long
    Dim dblOrigTotal    As Double
    Dim lngEventLineNo  As Long
    Dim strEventDesc    As String
    Dim blnGetCoupon    As Boolean

    lngItemNo = 0
    Set rsOPMaster = SKUMultiBuysRecordSet()
    While lngItemNo < sprdItems.MaxRows
        'Step through lines and find the Total Qty Line
        lngItemNo = lngItemNo + 1
        strSKU = ""
        sprdItems.Row = lngItemNo
        sprdItems.Col = COL_ITEM_CONS_QTY
        If Val(sprdItems.value) > 0 Then
            lngTotalQty = Val(sprdItems.value)
            sprdItems.Col = COL_ITEM_PARTCODE
            strSKU = sprdItems.Text
            lngConsLineNo = sprdItems.Row
        
            'Check if Event exists for SKU and with current quantity for SKU
            strNow = Format$(Now, "YYYYMMDDHHNN")
            If EnableGetCoupons Then
                rsOPMaster.Filter = "EventKey = '" & strSKU & "' " _
                                  & "And EventKey2 <= '" & Format$(lngTotalQty, "00000000") & "' " _
                                  & "And GETCPN = '0000000'"
            Else
                rsOPMaster.Filter = "EventKey = '" & strSKU & "' " _
                                  & "And EventKey2 <= '" & Format$(lngTotalQty, "00000000") & "'"
            End If
            sprdEvents.MaxRows = 0
        
            '************************************
            '** Process SKU Multibuy Offers **
            '************************************
            Do While Not rsOPMaster.EOF
                'Check if matching Header for Master Event
                lngEventLineNo = 0
                If CheckHeaderValid(rsOPMaster!EventNumber, rsOPMaster!Priority, rsOPMaster!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True Then
                    If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True) Then
                        lngMatchBalance = Val(rsOPMaster!EventKey2)
                        blnGetCoupon = False
                        dblOrigTotal = 0
                        lngBoughtQty = rsOPMaster!GetQuantity
                        lngLineNo = sprdItems.Row 'set row before saving values
                        Call PreserveQuantities
                        'If matched, then copy to event with new price for each line
                        While (lngLineNo <= sprdItems.MaxRows) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True)
                            sprdItems.Row = lngLineNo
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                sprdItems.Col = COL_ITEM_QTY
                                'Check if line will fulfil quantity required
                                If Val(sprdItems.value) >= lngMatchBalance Then
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngUsedQty = lngMatchBalance
                                    sprdItems.value = sprdItems.value - lngMatchBalance
                                    lngMatchBalance = 0
                                    'If force loop to use same line again
                                    If (sprdItems.value > 0) Then lngLineNo = lngLineNo - 1
                                Else
                                    lngUsedQty = Val(sprdItems.value)
                                    lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                    sprdItems.value = 0
                                End If
                                'Add up total discount - using most expensive first up till no of Get Qty
                                sprdItems.Col = COL_ITEM_SELLPRICE
                                If (lngBoughtQty > 0) Then
                                    If (lngUsedQty > lngBoughtQty) Then
                                        dblOrigTotal = dblOrigTotal + (lngBoughtQty * Val(sprdItems.value))
                                        lngBoughtQty = 0
                                    Else
                                        dblOrigTotal = dblOrigTotal + (lngUsedQty * Val(sprdItems.value))
                                        lngBoughtQty = lngBoughtQty - lngUsedQty
                                    End If
                                End If
                                If (lngMatchBalance = 0) Then
                                    'when No of items found, copy final line to Event History
                                    'check if event should be on new line or appending to last line
                                    sprdEvents.Row = sprdEvents.MaxRows
                                    sprdItems.Col = COL_ITEM_NO
                                    sprdEvents.Col = COL_DG_ITEMNO
                                    If (sprdItems.Text <> sprdEvents.Text) Then
                                        sprdEvents.MaxRows = sprdEvents.MaxRows + 1
                                        sprdEvents.Row = sprdEvents.MaxRows
                                        sprdItems.Col = COL_ITEM_NO
                                        sprdEvents.Col = COL_DG_ITEMNO
                                        sprdEvents.Text = sprdItems.Text
                                        sprdEvents.Col = COL_DG_DESC
                                        sprdEvents.Text = strEventDesc
                                        sprdEvents.Col = COL_DG_EVENTNO
                                        sprdEvents.Text = rsOPMaster!EventNumber
                                        sprdEvents.Col = COL_DG_SUBNO
                                        sprdEvents.Text = rsOPMaster!Priority
                                        sprdEvents.Col = COL_DG_BUY_SKU
                                        sprdEvents.Text = strSKU
                                        sprdEvents.Col = COL_DG_EVENTDATE
                                        sprdEvents.Text = rsOPMaster!EDAT
                                    End If
                                    sprdItems.Col = COL_ITEM_NO
                                    sprdEvents.Col = COL_DG_ITEMNO
                                    'If row is after previous rows then update all events to print here
                                    If (lngEventLineNo < Val(sprdItems.Text)) Then
                                        sprdEvents.Row = -1
                                        sprdEvents.Text = sprdItems.Text
                                        lngEventLineNo = Val(sprdItems.Text)
                                    Else
                                        sprdEvents.Text = lngEventLineNo
                                    End If
                                    sprdEvents.Row = sprdEvents.MaxRows
                                    sprdEvents.Col = COL_DG_BUY_QTY
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngTotalQty = Val(rsOPMaster!EventKey2)
                                    sprdEvents.Text = Val(sprdEvents.Text) + lngTotalQty
                                    sprdEvents.Col = COL_DG_DISC_TOTAL
                                    If EnableGlobalEvents Then
                                        If Val(rsOPMaster!DiscountPercent) = 0 Then
                                            sprdEvents.value = Val(sprdEvents.value) + Val(rsOPMaster!ValueDiscount)
                                        Else
                                            sprdEvents.value = Val(sprdEvents.value) + (dblOrigTotal * Val(rsOPMaster!DiscountPercent) / 100)
                                        End If
                                    Else
                                        sprdEvents.value = Val(sprdEvents.value) + (dblOrigTotal * Val(rsOPMaster!DiscountPercent) / 100)
                                    End If
                                    sprdItems.Row = lngConsLineNo
                                    sprdItems.Col = COL_ITEM_CONS_QTY
                                    sprdItems.value = (Val(sprdItems.value)) - Val(rsOPMaster!EventKey2)
                                    lngMatchBalance = Val(rsOPMaster!EventKey2)
                                    Call PreserveQuantities
                                    If (Val(rsOPMaster!BUYCPN) > 0) Then
                                        Call RemoveMatchedCoupon(rsOPMaster!BUYCPN)
                                        mcolCouponsUsedInTransaction.Add (rsOPMaster!BUYCPN)
                                    End If
                                    If (Val(rsOPMaster!GETCPN) > 0) Then
                                        blnGetCoupon = True
                                    End If
                                End If
                            Else
                                lngLineNo = sprdItems.MaxRows + 1 'force loop to exit
                            End If
                            sprdItems.Col = COL_ITEM_QTY
                            lngLineNo = lngLineNo + 1
                        Wend
                        If (lngMatchBalance > 0) And (lngMatchBalance <> Val(rsOPMaster!EventKey2)) Then
                            'Event was incomplete so restore deducted values
                            Call RestoreQuantities
                        End If
                        If blnGetCoupon And Not EnableGetCoupons Then
                            Call mcolPrintCoupons.Add(rsOPMaster!GETCPN & ":" & lngEventLineNo)
                            blnGetCoupon = False
                        End If
                        'Remove items that have events and recalc the consolidated quantities
                        For lngLineNo = sprdItems.MaxRows To lngItemNo Step -1
                            sprdItems.Row = lngLineNo
                            sprdItems.Col = COL_ITEM_QTY
                            lngQuantity = Val(sprdItems.value)
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                If (lngQuantity = 0) Then
                                    Call sprdItems.DeleteRows(lngLineNo, 1)
                                    sprdItems.MaxRows = sprdItems.MaxRows - 1
                                Else
                                    sprdItems.Col = COL_ITEM_CONS_QTY
                                    sprdItems.value = lngQuantity
                                End If
                            End If
                        Next
                        Call ConsolidateItems
                        Call ExtractTotalEvents(MULTI_SKU_MATCH_TYPE)
                        Call ExtractEvents(MULTI_SKU_MATCH_TYPE)
                        lngItemNo = lngItemNo - 1 'step back one line, as it has been replaced
                    End If 'date and time matched for header
                End If 'header was found and not marked as deleted
                rsOPMaster.MoveNext
            Loop
        End If
    Wend
End Sub

Public Sub CheckForSKUMultiBuyGetCoupons()
    Dim rsOPMaster      As Recordset
    Dim lngLineNo       As Long
    Dim strNow          As String
    Dim strStartAt      As String
    Dim strEndAt        As String
    Dim strSKU          As String
    Dim lngQuantity     As Long
    Dim lngTotalQty     As Long
    Dim lngUsedQty      As Long
    Dim lngMatchBalance As Long
    Dim lngBoughtQty    As Long
    Dim lngItemNo       As Long
    Dim lngConsLineNo   As Long
    Dim dblOrigTotal    As Double
    Dim lngEventLineNo  As Long
    Dim strEventDesc    As String

    lngItemNo = 0
    Set rsOPMaster = SKUMultiBuysRecordSet()
    While lngItemNo < sprdItems.MaxRows
        'Step through lines and find the Total Qty Line
        lngItemNo = lngItemNo + 1
        strSKU = ""
        sprdItems.Row = lngItemNo
        sprdItems.Col = COL_ITEM_CONS_QTY
        If Val(sprdItems.value) > 0 Then
            lngTotalQty = Val(sprdItems.value)
            sprdItems.Col = COL_ITEM_PARTCODE
            strSKU = sprdItems.Text
            lngConsLineNo = sprdItems.Row
        
            'Check if Event exists for SKU and with current quantity for SKU
            strNow = Format$(Now, "YYYYMMDDHHNN")
            rsOPMaster.Filter = "EventKey = '" & strSKU & "' " _
                              & "And EventKey2 <= '" & Format$(lngTotalQty, "00000000") & "' " _
                              & "And GETCPN <> '0000000'"
        
            '************************************
            '** Process SKU Multibuy Offers **
            '************************************
            Do While Not rsOPMaster.EOF
                'Check if matching Header for Master Event
                lngEventLineNo = 0
                If CheckHeaderValid(rsOPMaster!EventNumber, rsOPMaster!Priority, rsOPMaster!TimeOrDayRelated, strStartAt, strEndAt, strEventDesc) = True Then
                    If (strNow >= strStartAt) And (strNow < strEndAt) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True) Then
                        lngMatchBalance = Val(rsOPMaster!EventKey2)
                        dblOrigTotal = 0
                        lngBoughtQty = rsOPMaster!GetQuantity
                        lngLineNo = sprdItems.Row 'set row before saving values
                        Call PreserveQuantities
                        'If matched, then copy to event with new price for each line
                        While (lngLineNo <= sprdItems.MaxRows) And (CheckCouponsMatch(rsOPMaster("BUYCPN")) = True)
                            sprdItems.Row = lngLineNo
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                sprdItems.Col = COL_ITEM_QTY
                                'Check if line will fulfil quantity required
                                If Val(sprdItems.value) >= lngMatchBalance Then
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngUsedQty = lngMatchBalance
                                    sprdItems.value = sprdItems.value - lngMatchBalance
                                    lngMatchBalance = 0
                                    'If force loop to use same line again
                                    If (sprdItems.value > 0) Then lngLineNo = lngLineNo - 1
                                Else
                                    lngUsedQty = Val(sprdItems.value)
                                    lngMatchBalance = lngMatchBalance - Val(sprdItems.value)
                                    sprdItems.value = 0
                                End If
                                'Add up total discount - using most expensive first up till no of Get Qty
                                sprdItems.Col = COL_ITEM_SELLPRICE
                                If (lngBoughtQty > 0) Then
                                    If (lngUsedQty > lngBoughtQty) Then
                                        dblOrigTotal = dblOrigTotal + (lngBoughtQty * Val(sprdItems.value))
                                        lngBoughtQty = 0
                                    Else
                                        dblOrigTotal = dblOrigTotal + (lngUsedQty * Val(sprdItems.value))
                                        lngBoughtQty = lngBoughtQty - lngUsedQty
                                    End If
                                End If
                                If (lngMatchBalance = 0) Then
                                    'when No of items found, copy final line to Event History
                                    'check if event should be on new line or appending to last line
                                    sprdItems.Col = COL_ITEM_QTY
                                    lngTotalQty = Val(rsOPMaster!EventKey2)
                                    sprdItems.Row = lngConsLineNo
                                    sprdItems.Col = COL_ITEM_CONS_QTY
                                    sprdItems.value = (Val(sprdItems.value)) - Val(rsOPMaster!EventKey2)
                                    lngMatchBalance = Val(rsOPMaster!EventKey2)
                                    Call PreserveQuantities
                                    If (Val(rsOPMaster!GETCPN) > 0) Then
                                        ' Add a coupon every time repeat of criteria is met
                                        Call mcolPrintCoupons.Add(rsOPMaster!GETCPN & ":" & lngEventLineNo)
                                    End If
                                End If
                            Else
                                lngLineNo = sprdItems.MaxRows + 1 'force loop to exit
                            End If
                            sprdItems.Col = COL_ITEM_QTY
                            lngLineNo = lngLineNo + 1
                        Wend
                        If (lngMatchBalance > 0) And (lngMatchBalance <> Val(rsOPMaster!EventKey2)) Then
                            'Event was incomplete so restore deducted values
                            Call RestoreQuantities
                        End If
                        'Remove items that have events and recalc the consolidated quantities
                        For lngLineNo = sprdItems.MaxRows To lngItemNo Step -1
                            sprdItems.Row = lngLineNo
                            sprdItems.Col = COL_ITEM_QTY
                            lngQuantity = Val(sprdItems.value)
                            sprdItems.Col = COL_ITEM_PARTCODE
                            If (sprdItems.Text = strSKU) Then
                                If (lngQuantity = 0) Then
                                    Call sprdItems.DeleteRows(lngLineNo, 1)
                                    sprdItems.MaxRows = sprdItems.MaxRows - 1
                                Else
                                    sprdItems.Col = COL_ITEM_CONS_QTY
                                    sprdItems.value = lngQuantity
                                End If
                            End If
                        Next
                        Call ConsolidateItems
                        lngItemNo = lngItemNo - 1 'step back one line, as it has been replaced
                    End If 'date and time matched for header
                End If 'header was found and not marked as deleted
                rsOPMaster.MoveNext
            Loop
        End If
    Wend
End Sub

Private Sub PreserveQuantities()

    Call sprdItems.CopyColRange(COL_ITEM_QTY, COL_ITEM_CONS_QTY, COL_ITEM_PRE_QTY)
End Sub

Private Sub RestoreQuantities()

    Call sprdItems.CopyColRange(COL_ITEM_PRE_QTY, COL_ITEM_PRECONS_QTY, COL_ITEM_QTY)
End Sub

Public Sub ResetEvents()

    sprdMaster.MaxRows = 0
    sprdEvents.MaxRows = 0
    sprdItems.MaxRows = 0
    Set mcolPrintCoupons = New Collection
    Set mcolAddedCoupons = New Collection
    Set mcolUnUsedCoupons = New Collection
    Set mUsedCoupons = New Collection
    ResetNonReusableCouponDescriptions
End Sub

Private Function DealGroupRecordset() As Recordset
    Dim strQuery    As String
    Dim oConnection As Connection
    Dim lngRow      As Long
    Dim rsSifter    As Recordset
    Dim lngSaveCL   As Long
    
    If EnableMultilinePriceOverride Then
        Dim X As New cDotNetEventLibrary
      
        strQuery = BuildSkuList
        Set DealGroupRecordset = X.MultiplePriceOverrideGetDealGroup(strQuery, Date)
    Else
        Set oConnection = m_oSession.Database.Connection
        'Build up list of SKU that have been sold, to check against all valid Deal Groups and Mix and Match Deal Groups
        strQuery = "('"
    
        sprdItems.Col = COL_ITEM_PARTCODE
        For lngRow = 1 To sprdItems.MaxRows
            sprdItems.Row = lngRow
            If LenB(Trim$(sprdItems.value)) <> 0 Then
                If Right$(strQuery, 2) <> "('" Then
                    strQuery = strQuery & "', '"
                End If
                strQuery = strQuery & sprdItems.Text
            End If
        Next
        strQuery = strQuery & "')"
        'Insert SKU's into select list to attempt to retrieve
        strQuery = "Select Distinct numb,dlgn from evtdlg where (key1 in " & strQuery & " and Type='S') union " & _
                   "select distinct numb,dlgn from evtmmg inner join evtdlg on evtdlg.key1=evtmmg.mmgn and " & _
                   "evtdlg.type='M' where skun in " & strQuery
        
    '    Call DebugMsg(MODULE_NAME, "DealGroupRecordset", endlDebug, "DealGroupRecordset In")
        lngSaveCL = oConnection.CursorLocation
        oConnection.CursorLocation = adUseClient
        Call DebugMsg(MODULE_NAME, "DealGroupRecordset", endlDebug, "DealGroupRS(SKUs)-" & strQuery)
        Set rsSifter = m_oSession.Database.ExecuteCommand(strQuery)
    '    Call DebugMsg(MODULE_NAME, "DealGroupRecordset", endlDebug, "DealGroupRecordset Out")

        'Using list of valid Events, retrieve full detauls for each Deal Group event for further checking
        strQuery = "SELECT NUMB EventNumber,DLGN DealGroupNumber, TYPE DealType, KEY1 ItemKey, IDEL Deleted, " & _
            "QUAN Quantity, VERO ErosionValue,PERO ErosionPercentage FROM EVTDLG"
        strQuery = strQuery & " where NUMB+DLGN in ('"

        'Append list of Events that must be retrieved
        sprdItems.Col = COL_ITEM_PARTCODE
        Do While rsSifter.EOF = False
            If Right$(strQuery, 2) <> "('" Then
                strQuery = strQuery & "', '"
            End If
            strQuery = strQuery & rsSifter!Numb & rsSifter!DLGN
            rsSifter.MoveNext
        Loop
        strQuery = strQuery & "')"

        Call DebugMsg(MODULE_NAME, "DealGroupRecordset", endlDebug, "DealGroupRS-" & strQuery)
        Set DealGroupRecordset = m_oSession.Database.ExecuteCommand(strQuery)
        oConnection.CursorLocation = lngSaveCL
    End If
End Function

Private Function EventMasterRecordset(ByVal EventType As String, rsSifter As Recordset) As Recordset
    Dim strQuery     As String
    Dim oConnection  As Connection
    Dim strLastEvtNo As String
    Dim lngSaveCL    As Long
    
    If EnableMultilinePriceOverride And StrComp(UCase(EventType), "DG", vbTextCompare) = 0 Then
        Dim X As New cDotNetEventLibrary

        If rsSifter.RecordCount > 0 Then
            strQuery = BuildSkuList
            Set EventMasterRecordset = X.MultiplePriceOverrideGetEventMaster(strQuery, Date)
            If Not EventMasterRecordset Is Nothing Then
                If EventMasterRecordset.BOF = False Then
                    EventMasterRecordset.MoveFirst
                Else
                    Set EventMasterRecordset = Nothing
                End If
            End If
            rsSifter.MoveFirst
        Else
            Set EventMasterRecordset = Nothing
        End If
    Else
        If rsSifter.RecordCount = 0 Then
            Set EventMasterRecordset = Nothing
        Else
            rsSifter.MoveFirst
            Set oConnection = m_oSession.Database.Connection
            strLastEvtNo = ""
            strQuery = "SELECT " _
                        & "NUMB EventNumber, " _
                        & "PRIO Priority, " _
                        & "KEY2 EventKey2, " _
                        & "IDOW TimeOrDayRelated, " _
                        & "PRIC SpecialPrice, " _
                        & "EDAT, " _
                        & "BUYCPN, " _
                        & "GETCPN " _
                     & "FROM EVTMAS " _
                     & "WHERE SDAT <= '" & Format$(Date, "yyyy-mm-dd") & "' " _
                     & "AND EDAT >= '" & Format$(Date, "yyyy-mm-dd") & "' " _
                     & "AND TYPE = '" & EventType & "' " _
                     & "AND IDEL = 0 " _
                     & "AND NUMB in ('"
        
            Do While rsSifter.EOF = False
                If strLastEvtNo <> rsSifter!EventNumber Then
                    If Right$(strQuery, 2) <> "('" Then
                        strQuery = strQuery & "', '"
                    End If
                    strQuery = strQuery & rsSifter!EventNumber
                End If
                strLastEvtNo = rsSifter!EventNumber
                rsSifter.MoveNext
            Loop
            strQuery = strQuery & "')"
                            
            strQuery = strQuery & " ORDER BY NUMB desc, KEY2 desc, KEY1 desc, TYPE desc" 'this should mimic the UpTrac system
        '    Call DebugMsg(MODULE_NAME, "DealGroupRecordset", endlDebug, "DealGroupRecordset In")
            lngSaveCL = oConnection.CursorLocation
            oConnection.CursorLocation = adUseClient
            Set EventMasterRecordset = m_oSession.Database.ExecuteCommand(strQuery)
        '    Call DebugMsg(MODULE_NAME, "DealGroupRecordset", endlDebug, "DealGroupRecordset Out")
            oConnection.CursorLocation = lngSaveCL
            rsSifter.MoveFirst
        End If
    End If
End Function

Private Function QuantityBreaksRecordset(strItemKey As String, lngTotalQuan As Long) As Recordset
    Dim strQuery    As String
    Dim oConnection As Connection
    Dim lngRow      As Long
    Dim lngSaveCL   As Long
    
    Set oConnection = m_oSession.Database.Connection
    
    strQuery = "select NUMB EventNumber, PRIO Priority, IDOW TimeOrDayRelated, KEY1 EventKey, PRIC SpecialPrice, PDIS DiscountPercent, KEY2 ReqQty ,EDAT, BUYCPN, GETCPN from EVTMAS m1 "
    strQuery = strQuery & "where SDAT <= '" & Format$(Date, "yyyy-mm-dd") & "' AND EDAT >= '" & Format$(Date, "yyyy-mm-dd")
    strQuery = strQuery & "' and TYPE = 'QS' and IDEL=0"
    
    strQuery = strQuery & " and key1 in ('"

    sprdItems.Col = COL_ITEM_PARTCODE
    For lngRow = 1 To sprdItems.MaxRows
        sprdItems.Row = lngRow
        If LenB(Trim$(sprdItems.value)) <> 0 Then
            If Right$(strQuery, 2) <> "('" Then
                strQuery = strQuery & "', '"
            End If
            strQuery = strQuery & sprdItems.Text
        End If
    Next

    strQuery = strQuery & "')"
    strQuery = strQuery & " and NUMB in (select MAX(m2.numb) from EVTMAS m2 Where m2.KEY1 = m1.KEY1 " & _
            "and SDAT <= '" & Format$(Date, "yyyy-mm-dd") & "' AND EDAT >= '" & Format$(Date, "yyyy-mm-dd") & "' and IDEL = 0 group by m2.KEY1, m2.key2)"
    
    strQuery = strQuery & " order by TYPE desc, KEY1 desc, NUMB desc, KEY2 desc" 'MO'C 28/08/2007 - Changed order for NUMB field must come before Key2 events
    
    lngSaveCL = oConnection.CursorLocation
    oConnection.CursorLocation = adUseClient
    Set QuantityBreaksRecordset = m_oSession.Database.ExecuteCommand(strQuery)
    oConnection.CursorLocation = lngSaveCL
End Function

Private Function SKUMultiBuysRecordSet()
    Dim strQuery    As String
    Dim oConnection As Connection
    Dim lngRow      As Long
    Dim lngSaveCL   As Long
    
    Set oConnection = m_oSession.Database.Connection
    
    strQuery = "select NUMB EventNumber, PRIO Priority, IDOW TimeOrDayRelated, KEY1 EventKey, " & _
            "PRIC SpecialPrice, PDIS DiscountPercent, VDIS ValueDiscount, KEY2 EventKey2, GQTY GetQuantity, EDAT, BUYCPN, GETCPN from EVTMAS "
    strQuery = strQuery & "where SDAT <= '" & Format$(Date, "yyyy-mm-dd") & "' AND EDAT >= '" & Format$(Date, "yyyy-mm-dd")
    strQuery = strQuery & "' and TYPE = 'MS' and IDEL=0"
    
    strQuery = strQuery & " and key1 in ('"

    sprdItems.Col = COL_ITEM_PARTCODE
    For lngRow = 1 To sprdItems.MaxRows
        sprdItems.Row = lngRow
        If LenB(Trim$(sprdItems.value)) <> 0 Then
            If Right$(strQuery, 2) <> "('" Then
                strQuery = strQuery & "', '"
            End If
            strQuery = strQuery & sprdItems.Text
        End If
    Next

    strQuery = strQuery & "')"
    
    strQuery = strQuery & " order by TYPE desc, KEY1 desc, KEY2 desc, NUMB desc"
    
    lngSaveCL = oConnection.CursorLocation
    oConnection.CursorLocation = adUseClient
    Set SKUMultiBuysRecordSet = m_oSession.Database.ExecuteCommand(strQuery)
    oConnection.CursorLocation = lngSaveCL
End Function

Private Function CheckCouponsMatch(strBuyCouponID As String) As Boolean
    Dim lngCouponNo As Long

    'if not coupon required then just accept matched
    If (strBuyCouponID = "") Or (strBuyCouponID = "0000000") Then
        CheckCouponsMatch = True
        Exit Function
    End If
    For lngCouponNo = 1 To mcolUnUsedCoupons.Count Step 1
        If strBuyCouponID = Left$(mcolUnUsedCoupons(lngCouponNo), 7) Then
            CheckCouponsMatch = True
            Exit Function
        End If
    Next lngCouponNo
End Function

Private Sub RemoveMatchedCoupon(strBuyCouponID As String)
    Dim lngCouponNo As Long

    For lngCouponNo = 1 To mcolUnUsedCoupons.Count Step 1
        If strBuyCouponID = Left$(mcolUnUsedCoupons(lngCouponNo), 7) Then
            Call mcolUnUsedCoupons.Remove(lngCouponNo)
            Call AddUsedCoupon(strBuyCouponID, lngCouponNo)
            Exit Sub
        End If
    Next lngCouponNo
End Sub

Private Sub AddUsedCoupon(ByVal CouponId As String, ByVal lngCouponNo As Long)
    Dim ExistingEntry As String
    Dim OpeningBracketPos As Integer
    Dim NumberOfCoupons As Integer
    
    NumberOfCoupons = 1

On Error GoTo AddEntryForCoupon
    ExistingEntry = mUsedCoupons(CouponId)
On Error GoTo 0
    
    If ExistingEntry <> "" Then
        OpeningBracketPos = InStr(1, ExistingEntry, "(", vbTextCompare)
        If OpeningBracketPos > 0 Then
            NumberOfCoupons = Val(Mid$(ExistingEntry, OpeningBracketPos + 1)) + 1
        End If
        mUsedCoupons.Remove CouponId
    End If
    
AddEntryForCoupon:
    
On Error GoTo NoDescriptionSoDoNotAdd
    If Not NonReusableCouponDescriptions Is Nothing Then
        If lngCouponNo <= NonReusableCouponDescriptions.Count Then
            If NonReusableCouponDescriptions(lngCouponNo) <> "" Then
On Error GoTo 0
                mUsedCoupons.Add Mid(NonReusableCouponDescriptions(lngCouponNo), Len(CouponId & ":") + 1) & "   (" & CStr(NumberOfCoupons) & ")", CouponId
            End If
        End If
    End If

NoDescriptionSoDoNotAdd:
End Sub

Friend Function BuildSkuList() As String
    Dim SkuList As String
    Dim RowIndex As Long

    'build sku list
    SkuList = ""
    sprdItems.Col = COL_ITEM_PARTCODE
    For RowIndex = 1 To sprdItems.MaxRows
        sprdItems.Row = RowIndex
        If Len(SkuList) <> 0 Then
            SkuList = SkuList & ","
        End If
        If LenB(Trim$(sprdItems.value)) <> 0 Then
            SkuList = SkuList & sprdItems.Text
        End If
    Next
    BuildSkuList = SkuList
End Function

