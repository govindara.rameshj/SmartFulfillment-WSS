VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LiveEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Implements IGlobalEventValDisc
Implements IGetCoupon
Implements IBuyCoupon
Implements IMultilinePO

Private Function IBuyCoupon_EnableBuyCoupon() As Boolean

    IBuyCoupon_EnableBuyCoupon = False
End Function

Private Function IGetCoupon_EnableGetCoupon() As Boolean

    IGetCoupon_EnableGetCoupon = False
End Function

Public Function IGlobalEventValDisc_EnableGlobalEvent() As Boolean

    IGlobalEventValDisc_EnableGlobalEvent = False
End Function

Private Function IMultilinePO_EnableMultilinePO() As Boolean

    IMultilinePO_EnableMultilinePO = False
End Function
