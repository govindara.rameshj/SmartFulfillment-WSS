Attribute VB_Name = "modEvents"
Option Explicit

Public m_oSession As Session

Const QS_DESC As String = "Bulk Saving"
Const MS_DESC As String = "Multibuy Saving"
Const DG_DESC As String = "Project Saving"
Const HS_DESC As String = "Spend Level Saving"

Public mblnUseDesc As Boolean 'debug flag whether to use fixed desc or from EVTHDR
Public mlngDayNo   As Long
Public mNonReusableCouponDescriptions As Collection
Public mcolCouponsUsedInTransaction As Collection

Public Function CheckHeaderValid(ByVal strEventNo As String, _
                                 ByVal strPriority As String, _
                                 ByVal blnCheckDays As Boolean, _
                                 ByRef strStartAt As String, _
                                 ByRef strEndAt As String, _
                                 ByRef strDesc As String, _
                                 Optional ByVal IncludeMultilinePriceOverrides As Boolean = False) As Boolean
    Static lstrEventNo      As String
    Static lstrPriority     As String
    Static lblnCheckDays    As Boolean
    Static lstrStartAt      As String
    Static lstrEndAt        As String
    Static lstrDesc         As String
    Static lblnLastResponse As Boolean

    Dim oEventHeader    As cOPEvents_Wickes.cEventHeader
    Dim strQuery    As String
    Dim rsData      As Recordset
    Dim oConnection As Connection
    Dim lngSaveCL As Long

    Set oConnection = m_oSession.Database.Connection

    If strEventNo = lstrEventNo And strPriority = lstrPriority And blnCheckDays = lblnCheckDays Then
        strStartAt = lstrStartAt
        strEndAt = lstrEndAt
        strDesc = lstrDesc
        CheckHeaderValid = lblnLastResponse
        Exit Function
    End If
    
    lstrEventNo = strEventNo
    lstrPriority = strPriority
    lblnCheckDays = blnCheckDays
    
    strStartAt = ""
    strEndAt = ""
    strDesc = ""

    If EnableMultilinePriceOverride And IncludeMultilinePriceOverrides Then
        Dim X As New cDotNetEventLibrary
        
        Set rsData = X.MultiplePriceOverrideGetValidEvent(strEventNo, strPriority)
    Else
        strQuery = "SELECT SDAT ""StartDate"", STIM ""StartTime"", EDAT ""EndDate"", DESCR ""Description""," & _
                                " DACT1 ""ActiveDay1"", DACT2 ""ActiveDay2"", DACT3 ""ActiveDay3""," & _
                                " DACT4 ""ActiveDay4"", DACT5 ""ActiveDay5"", DACT6 ""ActiveDay6""," & _
                                " DACT7 ""ActiveDay7"" " & _
                                "from EVTHDR where NUMB='" & strEventNo & "' and PRIO='" & strPriority & "' and IDEL=0"
        lngSaveCL = oConnection.CursorLocation
        oConnection.CursorLocation = adUseClient
        Set rsData = m_oSession.Database.ExecuteCommand(strQuery)
        oConnection.CursorLocation = lngSaveCL
    End If

    If (rsData.RecordCount <> 0) Then
        strStartAt = Format$(rsData!StartDate, "YYYYMMDD") & rsData!StartTime
        strEndAt = Format$(rsData!EndDate, "YYYYMMDD")
        strEndAt = strEndAt & "2359"
        strDesc = rsData!Description
        CheckHeaderValid = True
        'If based on Time/Day then check if valid for current day
        If (blnCheckDays = True) And (rsData.Fields("ActiveDay" & CStr(mlngDayNo)) = False) Then
            CheckHeaderValid = False
        End If
    Else
        CheckHeaderValid = False
    End If

    lstrStartAt = strStartAt
    lstrEndAt = strEndAt
    lstrDesc = strDesc
    lblnLastResponse = CheckHeaderValid
End Function

Public Property Get NonReusableCouponDescriptions() As Collection

    If mNonReusableCouponDescriptions Is Nothing Then
        ResetNonReusableCouponDescriptions
    End If
    Set NonReusableCouponDescriptions = mNonReusableCouponDescriptions
End Property

Public Sub ResetNonReusableCouponDescriptions()

    Set mNonReusableCouponDescriptions = New Collection
End Sub

' Get Coupons P014-01
Public Function EnableBuyCoupons() As Boolean
    Dim EventsImplementationFactory As New EventsFactory
    Dim BuyCouponsImplementation As IBuyCoupon

    Set BuyCouponsImplementation = EventsImplementationFactory.FactoryGetBuyCoupon
    EnableBuyCoupons = BuyCouponsImplementation.EnableBuyCoupon
End Function

' Get Coupons P014-02
Public Function EnableGetCoupons() As Boolean
    Dim EventsImplementationFactory As New EventsFactory
    Dim GetCouponsImplementation As IGetCoupon

    Set GetCouponsImplementation = EventsImplementationFactory.FactoryGetGetCoupon
    EnableGetCoupons = GetCouponsImplementation.EnableGetCoupon
End Function

' Global Events
 Public Function EnableGlobalEvents() As Boolean
    Dim EventsImplementationFactory As New EventsFactory
    Dim GlobalEventsImplementation As IGlobalEventValDisc

    Set GlobalEventsImplementation = EventsImplementationFactory.FactoryGetGlobalEvent
    EnableGlobalEvents = GlobalEventsImplementation.EnableGlobalEvent
 End Function
' End of Global Events

' Multiline Price Override
 Public Function EnableMultilinePriceOverride() As Boolean
    Dim MultilinePOImplementationFactory As New EventsFactory
    Dim MultilinePOImplementation As IMultilinePO

    Set MultilinePOImplementation = MultilinePOImplementationFactory.FactoryGetMultilinePriceOverride
    EnableMultilinePriceOverride = MultilinePOImplementation.EnableMultilinePO
 End Function
' End of Global Events

