VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cOPEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const MODULE_NAME As String = "cOPEvents"

Type tItem
    ItemNo       As Long
    PartCode     As String
    Quantity     As Long
    SellPrice    As Currency
    IncTotal     As Currency
    ConsQty      As Long
    BuyQty       As Long
    SellQty      As Long
    SellDisc     As Currency
    EventNo      As String
    SubEventNo   As String
    EventQty     As Long
    EventDiscQty As Long
    FinalLine    As String
    PreQty       As Long
    PreConsQty   As Long
    PriceUsed    As Boolean
End Type

Dim mcolOPEvents As Collection
Dim mUsedCoupons As Collection

Public Sub Initialise(ByRef oSession As Session)

'Dim oSysDates As cEnterprise_Wickes.cSystemDates

    Set modEvents.m_oSession = oSession

'    Set oSysDates = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMDATES)
'    Call oSysDates.AddLoadField(FID_SYSTEMDATES_TodaysDayNo)
'    Call oSysDates.IBo_Load
    
    mlngDayNo = Weekday(Date, vbMonday)
    If mlngDayNo = 0 Then mlngDayNo = 1
    
'    Set oSysDates = Nothing
    
    Call DebugMsg(MODULE_NAME, "Initialise", endlDebug, "Current Day is " & mlngDayNo)

End Sub

'Called by the Till Each Time a new item is entered to see if the price has
'a temporary price override
Public Function CheckSKUPrice(ByVal strSKUN As String, ByVal lngQuantity As Long, ByRef strEventNo As String, ByRef OverrideEvent As Boolean, ByRef ExistingEventPrice As Double) As Double

Const TEMP_SKU_PRICE_TYPE As String = "TS"
Const TEMP_MIX_PRICE_TYPE As String = "TM"

Dim strNow       As String
Dim oEventMaster As cOPEvents_Wickes.cEventMaster
Dim oEventMix    As cOPEvents_Wickes.cEventMixMatch
Dim colOPMaster  As Collection
Dim colOPHeader  As Collection
Dim colOPMix     As Collection
Dim lngMasterNo  As Long
Dim lngEventNo   As Long
Dim strStartAt   As String
Dim strEndAt     As String

Dim oRSelector   As CRowSelector
Dim oBOSortKeys  As CBoSortKeys

    strNow = Format$(Now, "YYYYMMDDHHNN")

   'requirement switch
   If m_oSession.GetParameter(9801403) = True Then
    
      Dim X As New cDotNetEventLibrary
      Dim OverridePrice As Double

      OverridePrice = X.TemporaryPriceChangeSKU(strSKUN, Date, OverrideEvent, ExistingEventPrice, strEventNo)
      CheckSKUPrice = OverridePrice

      If OverridePrice <> 0 Then Exit Function

   Else
   
      'Set Up sorting keys to use index and froce higher quantities first
      Set oBOSortKeys = m_oSession.Root.CreateUtilityObject("CBoSortKeys")
       Call oBOSortKeys.Add(FID_OPMASTER_EventType, True)
       Call oBOSortKeys.Add(FID_OPMASTER_EventKey, True)
       Call oBOSortKeys.Add(FID_OPMASTER_EventKey2, True)
       Call oBOSortKeys.Add(FID_OPMASTER_EventNumber, True)
    
       'First check if Temporary Price Change Exists for SKU
       Set oRSelector = m_oSession.Database.CreateBoSelector(CLASSID_OPMASTER)

       Call oRSelector.AddSelectValue(CMP_LESSEQUALTHAN, FID_OPMASTER_StartDate, Date)
       Call oRSelector.AddSelectValue(CMP_GREATEREQUALTHAN, FID_OPMASTER_EndDate, Date)
       Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_EventType, TEMP_SKU_PRICE_TYPE)
       Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_Deleted, False)
       Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_EventKey, strSKUN)
       Set colOPMaster = modEvents.m_oSession.Database.GetSortedBoCollection(oRSelector, oBOSortKeys)

       For lngMasterNo = 1 To colOPMaster.Count Step 1
          'Check if matching Header for Master Event -
          Set oEventMaster = colOPMaster(lngMasterNo)
          If modEvents.CheckHeaderValid(oEventMaster.EventNumber, oEventMaster.Priority, oEventMaster.TimeOrDayRelated, strStartAt, strEndAt, "") Then
             If (strNow >= strStartAt) And (strNow < strEndAt) Then
                'get list of all Deal Groups within the specific event and sub event
                CheckSKUPrice = oEventMaster.SpecialPrice
                strEventNo = oEventMaster.EventNumber
                Set colOPMaster = Nothing
                Set oEventMaster = Nothing
                Exit Function
             End If
          End If
       Next lngMasterNo

   End If

   ''''''''''''''''''''''''''''''''''''''''''' End : PO14-03 ''''''''''''''''''''''''''''''''''''''''''

    'If no SKU Price then check for Mix and Match Temporary Price Change Exists for SKU
    Set oRSelector = m_oSession.Database.CreateBoSelector(CLASSID_OPMASTER)

    Call oRSelector.AddSelectValue(CMP_LESSEQUALTHAN, FID_OPMASTER_StartDate, Date)
    Call oRSelector.AddSelectValue(CMP_GREATEREQUALTHAN, FID_OPMASTER_EndDate, Date)
    Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_EventType, TEMP_MIX_PRICE_TYPE)
    Call oRSelector.AddSelectValue(CMP_EQUAL, FID_OPMASTER_Deleted, False)
    Set colOPMaster = modEvents.m_oSession.Database.GetSortedBoCollection(oRSelector, oBOSortKeys)
    
    For lngMasterNo = 1 To colOPMaster.Count Step 1
        'Check if matching Header for Master Event -
        Set oEventMaster = colOPMaster(lngMasterNo)
        If modEvents.CheckHeaderValid(oEventMaster.EventNumber, oEventMaster.Priority, oEventMaster.TimeOrDayRelated, strStartAt, strEndAt, "") Then
            If (strNow >= strStartAt) And (strNow < strEndAt) Then
                'get list of all Deal Groups within the specific event and sub event
                'If no SKU Price then check for Mix and Match Temporary Price Change Exists for SKU
                Set oEventMix = modEvents.m_oSession.Database.CreateBusinessObject(CLASSID_OPMIXMATCH)
                Call oEventMix.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_PartCode, strSKUN)
                Call oEventMix.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_MixMatchGroupID, oEventMaster.EventKey)
                Call oEventMix.AddLoadFilter(CMP_EQUAL, FID_OPMIXMATCH_Deleted, False)
                Set colOPMix = oEventMix.IBo_LoadMatches
                If (colOPMix.Count > 0) Then
                    'Check if matching Mix Match entry for SKU
                    CheckSKUPrice = oEventMaster.SpecialPrice
                    strEventNo = oEventMaster.EventNumber
                    Set colOPMix = Nothing
                    Set colOPMaster = Nothing
                    Set oEventMaster = Nothing
                    Set oEventMix = Nothing
                    Exit Function
                End If
            End If
        End If
    Next lngMasterNo
    
    Set colOPMaster = Nothing
    Set colOPMix = Nothing
    Set oEventMaster = Nothing
    Set oEventMix = Nothing

End Function

'Called by the Till Each Time a new item is entered - need to ensure qty has been entered
'and any operator price override applied.  System will check if SKU/LINE NO exists and will
'update the price and quantity.
Public Sub AddSKU(ByVal strSKUN As String, _
                ByVal lngQuantity As Long, _
                ByVal dblPrice As Double, _
                ByVal lngLineNo As Long, _
                ByVal strHierCategory As String, _
                ByVal strHierGroup As String, _
                ByVal strHierSubGroup As String, _
                ByVal strHierStyle As String)
                
    Call frmOPEvents.AddSKU(strSKUN, lngQuantity, dblPrice, lngLineNo)
    Call frmHSEvents.AddSKU(strSKUN, lngQuantity, dblPrice, lngLineNo, _
                        strHierCategory, strHierGroup, strHierSubGroup, strHierStyle)

End Sub

'Called by the Till Each Time an item is reversed
Public Sub DeleteSKU(strSKUN As String, lngLineNo As Long)

    Call frmOPEvents.DeleteSKU(strSKUN, lngLineNo)
    Call frmHSEvents.DeleteSKU(strSKUN, lngLineNo)

End Sub

'Called by the Till Each Time a new item is entered - need to ensure qty has been entered
'and any operator price override applied.  System will check if SKU/LINE NO exists and will
'update the price and quantity.
Public Sub AddCoupon(ByVal strCouponID As String, lngLineNo As Long)
                
    Call frmOPEvents.AddCoupon(strCouponID, lngLineNo)

End Sub

'Called by the Till Each Time a new item is entered - need to ensure qty has been entered
'and any operator price override applied.  System will check if SKU/LINE NO exists and will
'update the price and quantity.
Public Sub AddCouponWithDescription(ByVal strCouponID As String, ByVal lngLineNo As Long, ByVal Description As String)
                
    Call frmOPEvents.AddCouponWithDescription(strCouponID, lngLineNo, Description)
End Sub

'Called by the Till Each Time an item is reversed
Public Sub DeleteCoupon(strCouponID As String, lngLineNo As Long)

    Call frmOPEvents.DeleteCoupon(strCouponID, lngLineNo)

End Sub

'Called by the Till Each Time an price is override is performed
Public Sub UpdateSKUPrice(strSKUN As String, lngLineNo As Long, curPrice As Currency)

    Call frmOPEvents.UpdateSKUPrice(strSKUN, lngLineNo, curPrice)
    Call frmHSEvents.UpdateSKUPrice(strSKUN, lngLineNo, curPrice)

End Sub

'Called at the start of each Transaction to clear down the previous items
Public Sub ClearSKUS()

    Call frmHSEvents.ResetEvents
    Call frmOPEvents.ResetEvents

End Sub

'Called at the end of the transaction to get the Events against each line
'as well a list of Event Totals
Public Function GetOPEvents(ByRef colOPSummary As Collection, ByRef colGetCoupons As Collection, ByRef colReturnCoupons As Collection) As Collection

    Set mcolOPEvents = frmOPEvents.CheckEvents(colOPSummary, colGetCoupons, colReturnCoupons)
    Set mUsedCoupons = frmOPEvents.UsedCoupons
    Set GetOPEvents = mcolOPEvents

End Function
    
'Called at the end of the transaction to get the HS Discounts against each department
'as well a list of the HS Totals
Public Function CheckForHSDiscounts(ByRef colHSSummary As Collection, ByRef colGetCoupons As Collection, ByRef colReturnCoupons As Collection) As Collection

    Set frmHSEvents.UsedCoupons = mUsedCoupons
    Set CheckForHSDiscounts = frmHSEvents.CheckEvents(mcolOPEvents, colHSSummary, colGetCoupons, colReturnCoupons) ', colReturnCoupons)
    Set mUsedCoupons = frmHSEvents.UsedCoupons
End Function

Public Property Get UsedCoupons() As Collection

    Set UsedCoupons = mUsedCoupons
End Property

Public Property Get CouponsUsedInTransaction() As Collection
    Set CouponsUsedInTransaction = mcolCouponsUsedInTransaction
End Property
