VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPOSAction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB73E0370"
'<CAMH>****************************************************************************************
'* Module : cPOSAction
'* Date   : 21/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/cPOSAction.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 11/18/02 11:58a $
'* $Revision: 2 $
'* Versions:
'* 21/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cPOSAction"

'##ModelId=3D5CDA4300A0
Private mPartCode As String

'##ModelId=3D5CDA470032
Private mLocation As String

'##ModelId=3D5CDA4C0370
Private mSelMissing As Boolean

'##ModelId=3D5CDA5C032A
Private mRetailPrice As Currency

'##ModelId=3D5CDA610212
Private mSpecialPrice As Currency

'##ModelId=3D5CDA6603CA
Private mEANWrong As Boolean

'##ModelId=3D5CDA6C0384
Private mOverShelfEANWrong As Boolean

'##ModelId=3D5CDA7602A8
Private mPOSMissing As Boolean

'##ModelId=3D5CDA7B0078
Private mPOSInfoIncorrect As Boolean

'##ModelId=3D5CDA820104
Private mItemNeedsTagging As Boolean

'##ModelId=3D5CDA8A0154
Private mWrongLocation As Boolean

'##ModelId=3D5CDA9400E6
Private mOverStockWrongLocation As Boolean

'##ModelId=3D5CDA9E0226
Private mShelfStockCount As Double

'##ModelId=3D5CDAA5008C
Private mOverStockCount As Double

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


'##ModelId=3D7738BB03AC
Public Property Get OverStockCount() As Double
   Let OverStockCount = mOverStockCount
End Property

'##ModelId=3D7738BB0294
Public Property Let OverStockCount(ByVal Value As Double)
    Let mOverStockCount = Value
End Property

'##ModelId=3D7738BB01B8
Public Property Get ShelfStockCount() As Double
   Let ShelfStockCount = mShelfStockCount
End Property

'##ModelId=3D7738BB00DC
Public Property Let ShelfStockCount(ByVal Value As Double)
    Let mShelfStockCount = Value
End Property

'##ModelId=3D7738BB0000
Public Property Get OverStockWrongLocation() As Boolean
   Let OverStockWrongLocation = mOverStockWrongLocation
End Property

'##ModelId=3D7738BA030C
Public Property Let OverStockWrongLocation(ByVal Value As Boolean)
    Let mOverStockWrongLocation = Value
End Property

'##ModelId=3D7738BA026C
Public Property Get WrongLocation() As Boolean
   Let WrongLocation = mWrongLocation
End Property

'##ModelId=3D7738BA0154
Public Property Let WrongLocation(ByVal Value As Boolean)
    Let mWrongLocation = Value
End Property

'##ModelId=3D7738BA00E6
Public Property Get ItemNeedsTagging() As Boolean
   Let ItemNeedsTagging = mItemNeedsTagging
End Property

'##ModelId=3D7738BA000A
Public Property Let ItemNeedsTagging(ByVal Value As Boolean)
    Let mItemNeedsTagging = Value
End Property

'##ModelId=3D7738B90352
Public Property Get POSInfoIncorrect() As Boolean
   Let POSInfoIncorrect = mPOSInfoIncorrect
End Property

'##ModelId=3D7738B90276
Public Property Let POSInfoIncorrect(ByVal Value As Boolean)
    Let mPOSInfoIncorrect = Value
End Property

'##ModelId=3D7738B901CC
Public Property Get POSMissing() As Boolean
   Let POSMissing = mPOSMissing
End Property

'##ModelId=3D7738B900FA
Public Property Let POSMissing(ByVal Value As Boolean)
    Let mPOSMissing = Value
End Property

'##ModelId=3D7738B9008C
Public Property Get OverShelfEANWrong() As Boolean
   Let OverShelfEANWrong = mOverShelfEANWrong
End Property

'##ModelId=3D7738B80398
Public Property Let OverShelfEANWrong(ByVal Value As Boolean)
    Let mOverShelfEANWrong = Value
End Property

'##ModelId=3D7738B8032A
Public Property Get EANWrong() As Boolean
   Let EANWrong = mEANWrong
End Property

'##ModelId=3D7738B80280
Public Property Let EANWrong(ByVal Value As Boolean)
    Let mEANWrong = Value
End Property

'##ModelId=3D7738B801E0
Public Property Get SpecialPrice() As Currency
   Let SpecialPrice = mSpecialPrice
End Property

'##ModelId=3D7738B80136
Public Property Let SpecialPrice(ByVal Value As Currency)
    Let mSpecialPrice = Value
End Property

'##ModelId=3D7738B800C8
Public Property Get RetailPrice() As Currency
   Let RetailPrice = mRetailPrice
End Property

'##ModelId=3D7738B80028
Public Property Let RetailPrice(ByVal Value As Currency)
    Let mRetailPrice = Value
End Property

'##ModelId=3D7738B703A2
Public Property Get SelMissing() As Boolean
   Let SelMissing = mSelMissing
End Property

'##ModelId=3D7738B702F8
Public Property Let SelMissing(ByVal Value As Boolean)
    Let mSelMissing = Value
End Property

'##ModelId=3D7738B702C6
Public Property Get Location() As String
   Let Location = mLocation
End Property

'##ModelId=3D7738B7021C
Public Property Let Location(ByVal Value As String)
    Let mLocation = Value
End Property

'##ModelId=3D7738B701AE
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property


'##ModelId=3D7738B70140
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_POSACTION * &H10000) + 1 To FID_POSACTION_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_POSACTION

End Property
Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "POSAction " & mPartCode

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPOSAction

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_POSACTION, FID_POSACTION_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_POSACTION_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_POSACTION_Location):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLocation
        Case (FID_POSACTION_SelMissing):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSelMissing
        Case (FID_POSACTION_RetailPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mRetailPrice
        Case (FID_POSACTION_SpecialPrice):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mSpecialPrice
        Case (FID_POSACTION_EANWrong):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mEANWrong
        Case (FID_POSACTION_OverShelfEANWrong):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOverShelfEANWrong
        Case (FID_POSACTION_POSMissing):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPOSMissing
        Case (FID_POSACTION_POSInfoIncorrect):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPOSInfoIncorrect
        Case (FID_POSACTION_ItemNeedsTagging):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mItemNeedsTagging
        Case (FID_POSACTION_WrongLocation):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mWrongLocation
        Case (FID_POSACTION_OverStockWrongLocation):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOverStockWrongLocation
        Case (FID_POSACTION_ShelfStockCount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mShelfStockCount
        Case (FID_POSACTION_OverStockCount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOverStockCount
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPOSAction
    Set m_oSession = oSession
    Set Initialise = Me
End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_POSACTION_PartCode):               mPartCode = oField.ValueAsVariant
            Case (FID_POSACTION_Location):               mLocation = oField.ValueAsVariant
            Case (FID_POSACTION_SelMissing):             mSelMissing = oField.ValueAsVariant
            Case (FID_POSACTION_RetailPrice):            mRetailPrice = oField.ValueAsVariant
            Case (FID_POSACTION_SpecialPrice):           mSpecialPrice = oField.ValueAsVariant
            Case (FID_POSACTION_EANWrong):               mEANWrong = oField.ValueAsVariant
            Case (FID_POSACTION_OverShelfEANWrong):      mOverShelfEANWrong = oField.ValueAsVariant
            Case (FID_POSACTION_POSMissing):             mPOSMissing = oField.ValueAsVariant
            Case (FID_POSACTION_POSInfoIncorrect):       mPOSInfoIncorrect = oField.ValueAsVariant
            Case (FID_POSACTION_ItemNeedsTagging):       mItemNeedsTagging = oField.ValueAsVariant
            Case (FID_POSACTION_WrongLocation):          mWrongLocation = oField.ValueAsVariant
            Case (FID_POSACTION_OverStockWrongLocation): mOverStockWrongLocation = oField.ValueAsVariant
            Case (FID_POSACTION_ShelfStockCount):        mShelfStockCount = oField.ValueAsVariant
            Case (FID_POSACTION_OverStockCount):         mOverStockCount = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow


Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

    

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_POSACTION_END_OF_STATIC

End Function

