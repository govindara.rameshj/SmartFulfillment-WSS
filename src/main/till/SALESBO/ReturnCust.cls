VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cReturnCust"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D3EBFC10262"
'<CAMH>****************************************************************************************
'* Module: cReturnCust
'* Date  : 09/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/ReturnCust.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of the Till Sales Slip Return
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 4/02/04 9:28 $
'* $Revision: 3 $
'* Versions:
'* 09/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cReturnCust"

Implements IBo
Implements ISysBo

Private mTransactionDate   As Date
Private mPCTillID          As String
Private mTransactionNumber As String
Private mLineNumber        As Long
Private mCustomerName      As String
Private mAddressLine1      As String
Private mAddressLine2      As String
Private mAddressLine3      As String
Private mPostCode          As String
Private mPhoneNo           As String
Private mRefundReason      As String
Private mOrigTranValidated As Boolean
Private mOrigTenderType    As Long
Private mLabelsRequired    As Boolean
Private mHouseName         As String
Private mOrigTranStore     As String
Private mOrigTranDate      As Date
Private mOrigTranTill      As String
Private mOrigTranNumb      As String
Private mOrigLineNumber    As Long
Private mMobileNo          As String
Private mEmail             As String
Private mWorkTelNumber     As String
Private mWebOrderNumber    As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get TransactionDate() As Date
    TransactionDate = mTransactionDate
End Property

Public Property Let TransactionDate(ByVal Value As Date)
    mTransactionDate = Value
End Property

Public Property Get PCTillID() As String
    PCTillID = mPCTillID
End Property

Public Property Let PCTillID(ByVal Value As String)
    mPCTillID = Value
End Property

Public Property Get TransactionNumber() As String
    TransactionNumber = mTransactionNumber
End Property

Public Property Let TransactionNumber(ByVal Value As String)
    mTransactionNumber = Value
End Property

Public Property Get LineNumber() As Long
    LineNumber = mLineNumber
End Property

Public Property Let LineNumber(ByVal Value As Long)
    mLineNumber = Value
End Property

Public Property Get CustomerName() As String
    CustomerName = mCustomerName
End Property

Public Property Let CustomerName(ByVal Value As String)
    mCustomerName = Value
End Property

Public Property Get AddressLine1() As String
    AddressLine1 = mAddressLine1
End Property

Public Property Let AddressLine1(ByVal Value As String)
    mAddressLine1 = Value
End Property

Public Property Get AddressLine2() As String
    AddressLine2 = mAddressLine2
End Property

Public Property Let AddressLine2(ByVal Value As String)
    mAddressLine2 = Value
End Property

Public Property Get AddressLine3() As String
    AddressLine3 = mAddressLine3
End Property

Public Property Let AddressLine3(ByVal Value As String)
    mAddressLine3 = Value
End Property

Public Property Get PostCode() As String
    PostCode = mPostCode
End Property

Public Property Let PostCode(ByVal Value As String)
    mPostCode = Value
End Property

Public Property Get PhoneNo() As String
    PhoneNo = mPhoneNo
End Property

Public Property Let PhoneNo(ByVal Value As String)
    mPhoneNo = Value
End Property

Public Property Get RefundReason() As String
    RefundReason = mRefundReason
End Property

Public Property Let RefundReason(ByVal Value As String)
    mRefundReason = Value
End Property

Public Property Get OrigTranValidated() As Boolean
    OrigTranValidated = mOrigTranValidated
End Property

Public Property Let OrigTranValidated(ByVal Value As Boolean)
    mOrigTranValidated = Value
End Property

Public Property Get OrigTenderType() As Long
    OrigTenderType = mOrigTenderType
End Property

Public Property Let OrigTenderType(ByVal Value As Long)
    mOrigTenderType = Value
End Property

Public Property Get LabelsRequired() As Boolean
    LabelsRequired = mLabelsRequired
End Property

Public Property Let LabelsRequired(ByVal Value As Boolean)
    mLabelsRequired = Value
End Property

Public Property Get HouseName() As String
    HouseName = mHouseName
End Property

Public Property Let HouseName(ByVal Value As String)
    mHouseName = Value
End Property

Public Property Get OrigTranStore() As String
    OrigTranStore = mOrigTranStore
End Property

Public Property Let OrigTranStore(ByVal Value As String)
    mOrigTranStore = Value
End Property

Public Property Get OrigTranDate() As Date
    OrigTranDate = mOrigTranDate
End Property

Public Property Let OrigTranDate(ByVal Value As Date)
    mOrigTranDate = Value
End Property

Public Property Get OrigTranTill() As String
    OrigTranTill = mOrigTranTill
End Property

Public Property Let OrigTranTill(ByVal Value As String)
    mOrigTranTill = Value
End Property

Public Property Get OrigTranNumb() As String
    OrigTranNumb = mOrigTranNumb
End Property

Public Property Let OrigTranNumb(ByVal Value As String)
    mOrigTranNumb = Value
End Property

Public Property Get OrigLineNumber() As Long
    OrigLineNumber = mOrigLineNumber
End Property

Public Property Let OrigLineNumber(ByVal Value As Long)
    mOrigLineNumber = Value
End Property

Public Property Get MobileNo() As String
    MobileNo = mMobileNo
End Property

Public Property Let MobileNo(ByVal Value As String)
    mMobileNo = Value
End Property

Public Property Get Email() As String
    Email = mEmail
End Property

Public Property Let Email(ByVal Value As String)
    mEmail = Value
End Property

Public Property Get WorkTelNumber() As String
    WorkTelNumber = mWorkTelNumber
End Property

Public Property Let WorkTelNumber(ByVal Value As String)
    mWorkTelNumber = Value
End Property

Public Property Get WebOrderNumber() As String
    mWebOrderNumber = mWebOrderNumber
End Property

Public Property Let WebOrderNumber(ByVal Value As String)
    mWebOrderNumber = Value
End Property


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_RETURNCUST_TransactionDate):   mTransactionDate = oField.ValueAsVariant
            Case (FID_RETURNCUST_PCTillID):          mPCTillID = oField.ValueAsVariant
            Case (FID_RETURNCUST_TransactionNumber): mTransactionNumber = oField.ValueAsVariant
            Case (FID_RETURNCUST_LineNumber):        mLineNumber = oField.ValueAsVariant
            Case (FID_RETURNCUST_CustomerName):      mCustomerName = oField.ValueAsVariant
            Case (FID_RETURNCUST_AddressLine1):      mAddressLine1 = oField.ValueAsVariant
            Case (FID_RETURNCUST_AddressLine2):      mAddressLine2 = oField.ValueAsVariant
            Case (FID_RETURNCUST_AddressLine3):      mAddressLine3 = oField.ValueAsVariant
            Case (FID_RETURNCUST_PostCode):          mPostCode = oField.ValueAsVariant
            Case (FID_RETURNCUST_PhoneNo):           mPhoneNo = oField.ValueAsVariant
            Case (FID_RETURNCUST_RefundReason):      mRefundReason = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigTranValidated): mOrigTranValidated = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigTenderType):    mOrigTenderType = oField.ValueAsVariant
            Case (FID_RETURNCUST_LabelsRequired):    mLabelsRequired = oField.ValueAsVariant
            Case (FID_RETURNCUST_HouseName):         mHouseName = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigTranStore):     mOrigTranStore = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigTranDate):      mOrigTranDate = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigTranTill):      mOrigTranTill = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigTranNumb):      mOrigTranNumb = oField.ValueAsVariant
            Case (FID_RETURNCUST_OrigLineNumber):    mOrigLineNumber = oField.ValueAsVariant
            Case (FID_RETURNCUST_MobileNo):          mMobileNo = oField.ValueAsVariant
            Case (FID_RETURNCUST_Email):             mEmail = oField.ValueAsVariant
            Case (FID_RETURNCUST_WorkTelNumber):     mWorkTelNumber = oField.ValueAsVariant
            Case (FID_RETURNCUST_WebOrderNo):        mWebOrderNumber = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_RETURNCUST

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String

    IBo_DebugString = "Return Cust " & mTransactionNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If LenB(mOrigTranStore) = 0 Then mOrigTranStore = "000"
        If LenB(mOrigTranTill) = 0 Then mOrigTranTill = "00"
        If LenB(mOrigTranNumb) = 0 Then mOrigTranNumb = "0000"
        
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_RETURNCUST * &H10000) + 1 To FID_RETURNCUST_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cReturnCust

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_RETURNCUST, FID_RETURNCUST_END_OF_STATIC, m_oSession)

End Function

Public Function GetSelectSQL() As String

Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim oView       As IView
Dim lRowNo      As Long
Dim intFieldNo  As Integer
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lenb(lId) <> 0 Then mPartCode = sId
    
    Set oRSelector = New CRowSelector
    'Add in selector first to overide the ALL's
    
    Call oRSelector.AddSelectAll(FID_RETURNCUST_CustomerName)
    Call oRSelector.AddSelectAll(FID_RETURNCUST_TransactionDate)
    Call oRSelector.AddSelectAll(FID_RETURNCUST_PCTillID)
    Call oRSelector.AddSelectAll(FID_RETURNCUST_TransactionNumber)
    
    If Not moRowSel Is Nothing Then
        For intFieldNo = 1 To moRowSel.Count Step 1
            Call oRSelector.AddSelection(moRowSel.FieldSelector(intFieldNo).Comparator, moRowSel.FieldSelector(intFieldNo).Field.Interface)
        Next intFieldNo
    End If
    
    'Set mcolPartCode = New Collection
    ' Pass the selector to the database to get the data view
    GetSelectSQL = m_oSession.Database.GetSelectView(moLoadRow, oRSelector)

End Function


Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_RETURNCUST_TransactionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTransactionDate
        Case (FID_RETURNCUST_PCTillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPCTillID
        Case (FID_RETURNCUST_TransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNumber
        Case (FID_RETURNCUST_LineNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mLineNumber
        Case (FID_RETURNCUST_CustomerName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerName
        Case (FID_RETURNCUST_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_RETURNCUST_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_RETURNCUST_AddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine3
        Case (FID_RETURNCUST_PostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPostCode
        Case (FID_RETURNCUST_PhoneNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPhoneNo
        Case (FID_RETURNCUST_RefundReason):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRefundReason
        Case (FID_RETURNCUST_OrigTranValidated):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOrigTranValidated
        Case (FID_RETURNCUST_OrigTenderType):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrigTenderType
        Case (FID_RETURNCUST_LabelsRequired):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mLabelsRequired
        Case (FID_RETURNCUST_HouseName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHouseName
        Case (FID_RETURNCUST_OrigTranStore):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrigTranStore
        Case (FID_RETURNCUST_OrigTranDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mOrigTranDate
        Case (FID_RETURNCUST_OrigTranTill):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrigTranTill
        Case (FID_RETURNCUST_OrigTranNumb):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrigTranNumb
        Case (FID_RETURNCUST_OrigLineNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOrigLineNumber
        Case (FID_RETURNCUST_MobileNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMobileNo
        Case (FID_RETURNCUST_Email):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmail
        Case (FID_RETURNCUST_WorkTelNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWorkTelNumber
        Case (FID_RETURNCUST_WebOrderNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWebOrderNumber
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cReturnCust
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

    

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_RETURNCUST_END_OF_STATIC

End Function


Friend Function GetFileLine() As String

Dim strTemp As String

    strTemp = ""
    strTemp = strTemp & FmtVal(mTransactionDate, enftDate)
    strTemp = strTemp & FmtVal(mPCTillID, enftCode, 2)
    strTemp = strTemp & FmtVal(mTransactionNumber, enftCode, 4)
    strTemp = strTemp & FmtVal(mCustomerName, enftString, 30)
    strTemp = strTemp & "0000" ' HouseNumber
    strTemp = strTemp & FmtVal(mPostCode, enftString, 8)
    strTemp = strTemp & FmtVal(mOrigTranDate, enftDate)
    strTemp = strTemp & FmtVal(mOrigTranTill, enftCode, 2)
    strTemp = strTemp & FmtVal(mOrigTranNumb, enftCode, 4)
    strTemp = strTemp & FmtVal(mHouseName, enftString, 15)
    strTemp = strTemp & FmtVal(mAddressLine1, enftString, 30)
    strTemp = strTemp & FmtVal(mAddressLine2, enftString, 30)
    strTemp = strTemp & FmtVal(mAddressLine3, enftString, 30)
    strTemp = strTemp & FmtVal(mPhoneNo, enftString, 15)
    strTemp = strTemp & FmtVal(mLineNumber, enftCode, 5)
    strTemp = strTemp & FmtVal(mOrigTranValidated, enftYesNo)
    strTemp = strTemp & FmtVal(mOrigTenderType, enftNumber, 2)
    strTemp = strTemp & FmtVal(mRefundReason, enftCode, 2)
    strTemp = strTemp & FmtVal(mLabelsRequired, enftYesNo)
    strTemp = strTemp & FmtVal(mWorkTelNumber, enftString, 1)
    strTemp = strTemp & FmtVal(mEmail, enftString, 1)
    strTemp = strTemp & FmtVal(mWebOrderNumber, enftString, 20)
    GetFileLine = strTemp

End Function
