VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D3EB7EB026C"
'<CAMH>****************************************************************************************
'* Module : cCustomer
'* Date  : 09/08/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/Customer.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of a single Customer
'**********************************************************************************************
'* $Author: Richardc $
'* $Date: 18/05/04 15:32 $
'* $Revision: 15 $
'* Versions:
'* 09/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cCustomer"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D3FC5EC029E
Private mCardNumber As String

'##ModelId=3D3FC5F203A2
Private mActive As Boolean

'##ModelId=3D3FC60000A0
Private mCardExpiryDate As Date

'##ModelId=3D3FC61300AA
Private mCustomerName As String

'##ModelId=3D3FC61F01A4
Private mAddressLine1 As String

'##ModelId=3D3FC62C028A
Private mAddressLine2 As String

'##ModelId=3D3FC630006E
Private mAddressLine3 As String

'##ModelId=3D3FC63403B6
Private mAddressLine4 As String

'##ModelId=3D3FC63F02C6
Private mPostCode As String

'##ModelId=3D3FC64403A2
Private mWorkTelNumber As String

'##ModelId=3D3FC64E010E
Private mHomeTelNumber As String

'##ModelId=3D3FC656029E
Private mDeliveryName As String

'##ModelId=3D3FC6600154
Private mDeliveryAddressLine1 As String

'##ModelId=3D3FC6680320
Private mDeliveryAddressLine2 As String

'##ModelId=3D3FC66E0190
Private mDeliveryAddressLine3 As String

'##ModelId=3D3FC6830370
Private mDeliveryPostCode As String

'##ModelId=3D3FC69A033E
Private mDeliveryWorkTelNumber As String

'##ModelId=3D3FC6A701D6
Private mDeliveryHomeTelNumber As String

'##ModelId=3D3FC6B401E0
Private mDateAccountOpened As Date

Private mStopped As Boolean

Private mCreditLimit As Double

Private mAccountBalance As Double

Private mSoldToday As Double

Private mTend As Boolean

Public Property Get Tend() As Boolean
   Let Tend = mTend
End Property

Public Property Let Tend(ByVal Value As Boolean)
    Let mTend = Value
End Property

Public Property Get SoldToday() As Double
   Let SoldToday = mSoldToday
End Property

Public Property Let SoldToday(ByVal Value As Double)
    Let mSoldToday = Value
End Property

Public Property Get AccountBalance() As Double
   Let AccountBalance = mAccountBalance
End Property

Public Property Let AccountBalance(ByVal Value As Double)
    Let mAccountBalance = Value
End Property


Public Property Get CreditLimit() As Double
   Let CreditLimit = mCreditLimit
End Property


Public Property Let CreditLimit(ByVal Value As Double)
    Let mCreditLimit = Value
End Property

Public Property Get Stopped() As Boolean
   Let Stopped = mStopped
End Property

Public Property Let Stopped(ByVal Value As Boolean)
    Let mStopped = Value
End Property

'##ModelId=3D3FC4BC02F8
Public Sub Create()
End Sub

'##ModelId=3D3FC4D20046
Public Sub Retrieve()
End Sub

'##ModelId=3D3FC4D702A8
Public Sub List()
End Sub

'##ModelId=3D53CE230050
Public Property Get DateAccountOpened() As Date
   Let DateAccountOpened = mDateAccountOpened
End Property

'##ModelId=3D53CE220280
Public Property Let DateAccountOpened(ByVal Value As Date)
    Let mDateAccountOpened = Value
End Property

'##ModelId=3D53CE220172
Public Property Get DeliveryHomeTelNumber() As String
   Let DeliveryHomeTelNumber = mDeliveryHomeTelNumber
End Property

'##ModelId=3D53CE2103A2
Public Property Let DeliveryHomeTelNumber(ByVal Value As String)
    Let mDeliveryHomeTelNumber = Value
End Property


'##ModelId=3D53CE2003AC
Public Property Get DeliveryPostCode() As String
   Let DeliveryPostCode = mDeliveryPostCode
End Property

'##ModelId=3D53CE2001F4
Public Property Let DeliveryPostCode(ByVal Value As String)
    Let mDeliveryPostCode = Value
End Property

'##ModelId=3D53CE2000DC
Public Property Get DeliveryAddressLine3() As String
   Let DeliveryAddressLine3 = mDeliveryAddressLine3
End Property

'##ModelId=3D53CE1F0348
Public Property Let DeliveryAddressLine3(ByVal Value As String)
    Let mDeliveryAddressLine3 = Value
End Property

'##ModelId=3D53CE1F026C
Public Property Get DeliveryAddressLine2() As String
   Let DeliveryAddressLine2 = mDeliveryAddressLine2
End Property

'##ModelId=3D53CE1F00B4
Public Property Let DeliveryAddressLine2(ByVal Value As String)
    Let mDeliveryAddressLine2 = Value
End Property

'##ModelId=3D53CE1E03C0
Public Property Get DeliveryAddressLine1() As String
   Let DeliveryAddressLine1 = mDeliveryAddressLine1
End Property

'##ModelId=3D53CE1E0244
Public Property Let DeliveryAddressLine1(ByVal Value As String)
    Let mDeliveryAddressLine1 = Value
End Property

'##ModelId=3D53CE1E0168
Public Property Get DeliveryName() As String
   Let DeliveryName = mDeliveryName
End Property

'##ModelId=3D53CE1D03CA
Public Property Let DeliveryName(ByVal Value As String)
    Let mDeliveryName = Value
End Property

'##ModelId=3D53CE1D02EE
Public Property Get HomeTelNumber() As String
   Let HomeTelNumber = mHomeTelNumber
End Property

'##ModelId=3D53CE1D01A4
Public Property Let HomeTelNumber(ByVal Value As String)
    Let mHomeTelNumber = Value
End Property

'##ModelId=3D53CE1D00C8
Public Property Get WorkTelNumber() As String
   Let WorkTelNumber = mWorkTelNumber
End Property

'##ModelId=3D53CE1C0366
Public Property Let WorkTelNumber(ByVal Value As String)
    Let mWorkTelNumber = Value
End Property

'##ModelId=3D53CE1C028A
Public Property Get PostCode() As String
   Let PostCode = mPostCode
End Property

'##ModelId=3D53CE1C014A
Public Property Let PostCode(ByVal Value As String)
    Let mPostCode = Value
End Property



'##ModelId=3D53CE1B0230
Public Property Get AddressLine3() As String
   Let AddressLine3 = mAddressLine3
End Property

'##ModelId=3D53CE1B0118
Public Property Let AddressLine3(ByVal Value As String)
    Let mAddressLine3 = Value
End Property

'##ModelId=3D53CE1B0078
Public Property Get AddressLine2() As String
   Let AddressLine2 = mAddressLine2
End Property

'##ModelId=3D53CE1A0348
Public Property Let AddressLine2(ByVal Value As String)
    Let mAddressLine2 = Value
End Property

'##ModelId=3D53CE1A026C
Public Property Get AddressLine1() As String
   Let AddressLine1 = mAddressLine1
End Property

'##ModelId=3D53CE1A015E
Public Property Let AddressLine1(ByVal Value As String)
    Let mAddressLine1 = Value
End Property

'##ModelId=3D53CE1A00B4
Public Property Get CustomerName() As String
   Let CustomerName = mCustomerName
End Property

'##ModelId=3D53CE19038E
Public Property Let CustomerName(ByVal Value As String)
    Let mCustomerName = Value
End Property

'##ModelId=3D53CE1902E4
Public Property Get CardExpiryDate() As Date
   Let CardExpiryDate = mCardExpiryDate
End Property

'##ModelId=3D53CE1901D6
Public Property Let CardExpiryDate(ByVal Value As Date)
    Let mCardExpiryDate = Value
End Property

'##ModelId=3D53CE1900FA
Public Property Get Active() As Boolean
   Let Active = mActive
End Property

'##ModelId=3D53CE1803CA
Public Property Let Active(ByVal Value As Boolean)
    Let mActive = Value
End Property

'##ModelId=3D53CE18035C
Public Property Get CardNumber() As String
   Let CardNumber = mCardNumber
End Property

'##ModelId=3D53CE18028A
Public Property Let CardNumber(ByVal Value As String)
    Let mCardNumber = Value
End Property
'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_CUSTOMER_CardNumber):            mCardNumber = oField.ValueAsVariant
            Case (FID_CUSTOMER_Active):                mActive = oField.ValueAsVariant
            Case (FID_CUSTOMER_CardExpiry):            mCardExpiryDate = oField.ValueAsVariant
            Case (FID_CUSTOMER_CustomerName):          mCustomerName = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine1):          mAddressLine1 = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine2):          mAddressLine2 = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine3):          mAddressLine3 = oField.ValueAsVariant
            Case (FID_CUSTOMER_PostCode):              mPostCode = oField.ValueAsVariant
            Case (FID_CUSTOMER_WorkTelNumber):         mWorkTelNumber = oField.ValueAsVariant
            Case (FID_CUSTOMER_HomeTelNumber):         mHomeTelNumber = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryName):          mDeliveryName = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryAddressLine1):  mDeliveryAddressLine1 = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryAddressLine2):  mDeliveryAddressLine2 = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryAddressLine3):  mDeliveryAddressLine3 = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryPostCode):      mDeliveryPostCode = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryHomeTelNumber): mDeliveryHomeTelNumber = oField.ValueAsVariant
            Case (FID_CUSTOMER_DateAccountOpened):     mDateAccountOpened = oField.ValueAsVariant
            Case (FID_CUSTOMER_Stopped):               mStopped = oField.ValueAsVariant
            Case (FID_CUSTOMER_CreditLimit):           mCreditLimit = oField.ValueAsVariant
            Case (FID_CUSTOMER_AccountBalance):        mAccountBalance = oField.ValueAsVariant
            Case (FID_CUSTOMER_SoldToday):             mSoldToday = oField.ValueAsVariant
            Case (FID_CUSTOMER_TEND):                  mTend = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Customer " & mCardNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CUSTOMER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3FC4C403B6
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_CUSTOMER * &H10000) + 1 To FID_CUSTOMER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCustomer

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CUSTOMER, FID_CUSTOMER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        
        Case (FID_CUSTOMER_CardNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCardNumber
        Case (FID_CUSTOMER_Active):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mActive
        Case (FID_CUSTOMER_CardExpiry):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mCardExpiryDate
        Case (FID_CUSTOMER_CustomerName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerName
        Case (FID_CUSTOMER_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_CUSTOMER_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_CUSTOMER_AddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine3
        Case (FID_CUSTOMER_PostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPostCode
        Case (FID_CUSTOMER_WorkTelNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWorkTelNumber
        Case (FID_CUSTOMER_HomeTelNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mHomeTelNumber
        Case (FID_CUSTOMER_DeliveryName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryName
        Case (FID_CUSTOMER_DeliveryAddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddressLine1
        Case (FID_CUSTOMER_DeliveryAddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddressLine2
        Case (FID_CUSTOMER_DeliveryAddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddressLine3
        Case (FID_CUSTOMER_DeliveryPostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryPostCode
        Case (FID_CUSTOMER_DeliveryHomeTelNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryHomeTelNumber
        Case (FID_CUSTOMER_DateAccountOpened):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateAccountOpened
        Case (FID_CUSTOMER_Stopped):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mStopped
        Case (FID_CUSTOMER_CreditLimit):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCreditLimit
        Case (FID_CUSTOMER_AccountBalance):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mAccountBalance
        Case (FID_CUSTOMER_SoldToday):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mSoldToday
        Case (FID_CUSTOMER_TEND):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mTend

        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cCustomer
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

    

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CUSTOMER_END_OF_STATIC

End Function

Public Function GetSelectSQL() As String

Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim intFieldNo  As Integer
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lId <> "" Then mPartCode = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    'Add in selector first to overide the ALL's
    
    Call oRSelector.AddSelectAll(FID_CUSTOMER_CardNumber)
    Call oRSelector.AddSelectAll(FID_CUSTOMER_CustomerName)
    
    If Not moRowSel Is Nothing Then
        For intFieldNo = 1 To moRowSel.Count Step 1
            Call oRSelector.AddSelection(moRowSel.FieldSelector(intFieldNo).Comparator, moRowSel.FieldSelector(intFieldNo).Field.Interface)
        Next intFieldNo
    End If
    
    'Set mcolPartCode = New Collection
    ' Pass the selector to the database to get the data view
    GetSelectSQL = m_oSession.Database.GetSelectView(moLoadRow, oRSelector)

End Function

Public Sub RecordAccountSale(strAccountNum As String, dblTransValue As Double)

Dim oRow As IRow
            
    Call AddLoadFilter(CMP_EQUAL, FID_CUSTOMER_CardNumber, strAccountNum)
    Call AddLoadField(FID_CUSTOMER_SoldToday)
    Call AddLoadField(FID_CUSTOMER_AccountBalance)
    Call LoadMatches
    
    mSoldToday = mSoldToday + dblTransValue
    mAccountBalance = mAccountBalance + dblTransValue
    

    'Save updated fields back to Database
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    Call oRow.Add(GetField(FID_CUSTOMER_CardNumber))
    Call oRow.Add(GetField(FID_CUSTOMER_SoldToday))
    Call oRow.Add(GetField(FID_CUSTOMER_AccountBalance))

    Call m_oSession.Database.SavePartialBo(Me, oRow)
    Set oRow = Nothing

End Sub 'RecordISTItemReceived

