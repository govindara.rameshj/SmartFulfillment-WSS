VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPriceOverrideCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3DA3E310032A"
'<CAMH>****************************************************************************************
'* Module : cPriceOverrideCode
'* Date   : 09/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/cPriceOverrideCode.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 1/09/03 12:51p $
'* $Revision: 3 $
'* Versions:
'* 09/10/02    mauricem
'*             Header added.
'* 30/03/03    mauricem
'*             WIX1195 - to determine if Authorisation required for selected reason code
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cPriceOverrideCode"
Const PRICE_OVERRIDE_CODE As String = "OV"

Implements IBo
Implements ISysBo

'##ModelId=3DA3E371005A
Private mCodeType As String

'##ModelId=3DA3E37502C6
Private mCode As String

'##ModelId=3DA3E37C0014
Private mDescription As String

Private mIsRefundPriceOverride As Boolean

'Added WIX1195 - to determine if Authorisation required for selected reason code
Private mSupervisorAuthorisation As Boolean
Private mManagerAuthorisation As Boolean

'Added WIX1196 - Price Match capture competitor
Private mPercBasis As Boolean
Private mActive As Boolean
Private mPriceMatch As Boolean
Private mPriceMultiplier As Double
Private mPriceMaximum As Double
Private mManagerFlexibility As Boolean
Private mIsActive As Boolean

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get PercBasis() As Boolean
   Let PercBasis = mPercBasis
End Property

Public Property Let PercBasis(ByVal Value As Boolean)
    Let mPercBasis = Value
End Property

Public Property Get Active() As Boolean
   Let Active = mActive
End Property

Public Property Let Active(ByVal Value As Boolean)
    Let mActive = Value
End Property

Public Property Get PriceMatch() As Boolean
   Let PriceMatch = mPriceMatch
End Property

Public Property Let PriceMatch(ByVal Value As Boolean)
    Let mPriceMatch = Value
End Property

Public Property Get PriceMultiplier() As Double
   Let PriceMultiplier = mPriceMultiplier
End Property

Public Property Let PriceMultiplier(ByVal Value As Double)
    Let mPriceMultiplier = Value
End Property

Public Property Get PriceMaximum() As Double
   Let PriceMaximum = mPriceMaximum
End Property

Public Property Let PriceMaximum(ByVal Value As Double)
    Let mPriceMaximum = Value
End Property

Public Property Get ManagerFlexibility() As Boolean
   Let ManagerFlexibility = mManagerFlexibility
End Property

Public Property Let ManagerFlexibility(ByVal Value As Boolean)
    Let mManagerFlexibility = Value
End Property

Public Property Get IsActive() As Boolean
   Let IsActive = mIsActive
End Property

Public Property Let IsActive(ByVal Value As Boolean)
    Let mIsActive = Value
End Property

Public Property Get IsRefundPriceOverride() As Boolean
   Let IsRefundPriceOverride = mIsRefundPriceOverride
End Property

Public Property Let IsRefundPriceOverride(ByVal Value As Boolean)
    Let mIsRefundPriceOverride = Value
End Property

'##ModelId=3DA3E48D00AA
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3DA3E48D003C
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3DA3E48C03B6
Public Property Get Code() As String
   Let Code = mCode
End Property

'##ModelId=3DA3E48C0348
Public Property Let Code(ByVal Value As String)
    Let mCode = Value
End Property

'##ModelId=3DA3E48C030C
Public Property Get CodeType() As String
   mCodeType = PRICE_OVERRIDE_CODE
   Let CodeType = mCodeType
End Property


'##ModelId=3DA3E48C029E
Public Property Let CodeType(ByVal Value As String)
    Let mCodeType = Value
    mCodeType = PRICE_OVERRIDE_CODE
End Property


Public Property Get SupervisorAuthorisation() As Boolean
   Let SupervisorAuthorisation = mSupervisorAuthorisation
End Property

Public Property Let SupervisorAuthorisation(ByVal Value As Boolean)
    Let mSupervisorAuthorisation = Value
End Property

Public Property Get ManagerAuthorisation() As Boolean
   Let ManagerAuthorisation = mManagerAuthorisation
End Property

Public Property Let ManagerAuthorisation(ByVal Value As Boolean)
    Let mManagerAuthorisation = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Call AddLoadFilter(CMP_EQUAL, FID_PRICEOVERRIDE_CodeType, PRICE_OVERRIDE_CODE)
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function
'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_PRICEOVERRIDE_CodeType):                  mCodeType = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_Code):                      mCode = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_Description):               mDescription = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_IsRefundPriceOverride):     mIsRefundPriceOverride = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_SupervisorAuthorisation):   mSupervisorAuthorisation = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_ManagerAuthorisation):      mManagerAuthorisation = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_PercBasis):                 mPercBasis = (oField.ValueAsVariant = "Y")
            Case (FID_PRICEOVERRIDE_Active):                    mActive = (oField.ValueAsVariant = "Y")
            Case (FID_PRICEOVERRIDE_PriceMatch):                mPriceMatch = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_PriceMultiplier):           mPriceMultiplier = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_PriceMaximum):              mPriceMaximum = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_ManagerFlexibility):        mManagerFlexibility = oField.ValueAsVariant
            Case (FID_PRICEOVERRIDE_IsActive):                  mIsActive = oField.ValueAsVariant

            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_PRICEOVERRIDE

End Property
Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "PriceOverrideCode " & mCode

End Property
Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me
    
    Call ClearLoadFilter

End Function


Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_PRICEOVERRIDE * &H10000) + 1 To FID_PRICEOVERRIDE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

Dim oField As IField

    Set moRowSel = New CRowSelector
    Set oField = GetField(FID_PRICEOVERRIDE_CodeType)
    If oField Is Nothing Then Exit Sub
    oField.ValueAsVariant = PRICE_OVERRIDE_CODE
    
    'Add field to Row selector
    Call moRowSel.AddSelection(CMP_EQUAL, oField)

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function



Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPriceOverrideCode

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_PRICEOVERRIDE, FID_PRICEOVERRIDE_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_PRICEOVERRIDE_CodeType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCodeType
        Case (FID_PRICEOVERRIDE_Code):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCode
        Case (FID_PRICEOVERRIDE_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_PRICEOVERRIDE_IsRefundPriceOverride):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mIsRefundPriceOverride
        Case (FID_PRICEOVERRIDE_SupervisorAuthorisation):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSupervisorAuthorisation
        Case (FID_PRICEOVERRIDE_ManagerAuthorisation):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mManagerAuthorisation
        Case (FID_PRICEOVERRIDE_PercBasis):
                Set GetField = New CFieldString
                If (mPercBasis = True) Then
                    GetField.ValueAsVariant = "Y"
                Else
                    GetField.ValueAsVariant = "N"
                End If
        Case (FID_PRICEOVERRIDE_Active):
                Set GetField = New CFieldString
                If (mActive = True) Then
                    GetField.ValueAsVariant = "Y"
                Else
                    GetField.ValueAsVariant = "N"
                End If
        Case (FID_PRICEOVERRIDE_PriceMatch):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPriceMatch
        Case (FID_PRICEOVERRIDE_PriceMultiplier):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mPriceMultiplier
        Case (FID_PRICEOVERRIDE_PriceMaximum):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mPriceMaximum
        Case (FID_PRICEOVERRIDE_ManagerFlexibility):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mManagerFlexibility
        Case (FID_PRICEOVERRIDE_IsActive):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mIsActive
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPriceOverrideCode
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

    

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_PRICEOVERRIDE_END_OF_STATIC

End Function

