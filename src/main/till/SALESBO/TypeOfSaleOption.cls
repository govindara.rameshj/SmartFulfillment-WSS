VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTypesOfSaleOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E52BD8B0352"
'<CAMH>****************************************************************************************
'* Module : TypeOfSaleOption
'* Date   : 18/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/SalesBO/TypeOfSaleOption.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 2/20/03 9:10a $ $Revision: 1 $
'* Versions:
'* 18/02/03    Unknown
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cTypesOfSaleOptions"

Private mDisplaySeq            As String
Private mCode                  As String
Private mDescription           As String
Private mRequestDocRef         As Boolean
Private mSpecialPrint          As Boolean
Private mOpenOnZeroTran        As Boolean
Private mAllowSpecialAccTender As Boolean
Private mSupervisorRequired    As Boolean

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get DisplaySeq() As String
    DisplaySeq = mDisplaySeq
End Property

Public Property Let DisplaySeq(ByVal Value As String)
    mDisplaySeq = Value
End Property

Public Property Get Code() As String
    Code = mCode
End Property

Public Property Let Code(ByVal Value As String)
    mCode = Value
End Property

Public Property Get Description() As String
    Description = mDescription
End Property

Public Property Let Description(ByVal Value As String)
    mDescription = Value
End Property

Public Property Get RequestDocRef() As Boolean
    RequestDocRef = mRequestDocRef
End Property

Public Property Let RequestDocRef(ByVal Value As Boolean)
    mRequestDocRef = Value
End Property

Public Property Get SpecialPrint() As Boolean
    SpecialPrint = mSpecialPrint
End Property

Public Property Let SpecialPrint(ByVal Value As Boolean)
    mSpecialPrint = Value
End Property

Public Property Get OpenOnZeroTran() As Boolean
    OpenOnZeroTran = mOpenOnZeroTran
End Property

Public Property Let OpenOnZeroTran(ByVal Value As Boolean)
    mOpenOnZeroTran = Value
End Property

Public Property Get AllowSpecialAccTender() As Boolean
    AllowSpecialAccTender = mAllowSpecialAccTender
End Property

Public Property Let AllowSpecialAccTender(ByVal Value As Boolean)
    mAllowSpecialAccTender = Value
End Property

Public Property Get SupervisorRequired() As Boolean
    SupervisorRequired = mSupervisorRequired
End Property

Public Property Let SupervisorRequired(ByVal Value As Boolean)
    mSupervisorRequired = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 1 To FID_TYPESOFSALEOPTIONS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cTypesOfSaleOptions

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_TYPESOFSALEOPTIONS, FID_TYPESOFSALEOPTIONS_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_TYPESOFSALEOPTIONS_DisplaySeq):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDisplaySeq
        Case (FID_TYPESOFSALEOPTIONS_Code):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCode
        Case (FID_TYPESOFSALEOPTIONS_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_TYPESOFSALEOPTIONS_RequestDocRef):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRequestDocRef
        Case (FID_TYPESOFSALEOPTIONS_SpecialPrint):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSpecialPrint
        Case (FID_TYPESOFSALEOPTIONS_OpenOnZeroTran):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOpenOnZeroTran
        Case (FID_TYPESOFSALEOPTIONS_AllowSpecialAccTender):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAllowSpecialAccTender
        Case (FID_TYPESOFSALEOPTIONS_SupervisorRequired):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSupervisorRequired
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cTypesOfSaleOptions
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_TYPESOFSALEOPTIONS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Type Of Sale Code " & mCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_TYPESOFSALEOPTIONS_DisplaySeq):            mDisplaySeq = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_Code):                  mCode = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_Description):           mDescription = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_RequestDocRef):         mRequestDocRef = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_SpecialPrint):          mSpecialPrint = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_OpenOnZeroTran):        mOpenOnZeroTran = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_AllowSpecialAccTender): mAllowSpecialAccTender = oField.ValueAsVariant
            Case (FID_TYPESOFSALEOPTIONS_SupervisorRequired):    mSupervisorRequired = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_TYPESOFSALEOPTIONS_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cTypesOfSaleOptions

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


