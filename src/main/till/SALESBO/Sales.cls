VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPOSPayment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D45497100B4"
'<CAMH>****************************************************************************************
'* Module: cPOSPayment
'* Date  : 09/08/02
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/Sales.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of the Till Sales Slip Payment
'**********************************************************************************************
'* $Author: Richardc $ $Date: 5/05/04 17:37 $ $Revision: 6 $
'* Versions:
'* 09/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cPOSPayment"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


Private mTransactionDate      As Date
Private mTillID               As String
Private mTransactionNumber    As String
Private mSequenceNumber       As Long
Private mTenderType           As String
Private mTenderAmount         As Currency
Private mCreditCardNumber     As String
Private mCreditCardStartDate  As String
Private mCreditCardExpiryDate As String
Private mIssuerNumber         As String
Private mAuthorisationCode    As String
Private mCCNumberKeyed        As Boolean
Private mChequeAccountNumber  As String
Private mChequeSortCode       As String
Private mChequeNumber         As String
Private mSupervisorCode       As String
Private mEFTPOSVoucherNo      As String
Private mEFTPOSCommsDone      As Boolean
Private mEFTPOSMerchantNo     As String
Private mDigitCount           As Long
Private mCouponNumber         As String
Private mCouponPostCode       As String
Private mCashBalanceProcessed As Boolean
Private mAuthorisationType    As String
Private mCashBackAmount       As Currency
Private mCreditCardName       As String
Private mCouponClass          As String
Private mEFTConfirmID         As String
Private mEFTConfirmed         As String
Private mAuthorisationMethod  As String
Private mCardSecurityCode     As String
Private mCurrencyCode         As String
Private mExchangeRate         As Double
Private mExchangeFactor       As Long
Private mForeignTender        As Currency
Private mTerminalID           As String
Private mPANSeq               As String
Private mCardAppID            As String
Private mTrasactionID         As Double
Private mMessageNumber        As String
Private mGCTenderAmount       As Currency

Public POSHeader As cPOSHeader

Const NOT_LOADED As Long = -1
Const LOADED As Long = 0
Const EDITED As Long = 1

Public Property Let EFTConfirmID(ByVal Value As String)
    mEFTConfirmID = Value
End Property

Public Property Get EFTConfirmID() As String
    EFTConfirmID = mEFTConfirmID
End Property


Public Property Get EFTConfirmed() As String
    EFTConfirmed = mEFTConfirmed
End Property

Public Property Let AuthorisationMethod(Value As String)
    mAuthorisationMethod = Value
End Property

Public Property Get AuthorisationMethod() As String
    AuthorisationMethod = mAuthorisationMethod
End Property

Public Property Get TransactionDate() As Date
    TransactionDate = mTransactionDate
End Property

Public Property Let TransactionDate(ByVal Value As Date)
    mTransactionDate = Value
End Property

Public Property Get TillID() As String
    TillID = mTillID
End Property

Public Property Let TillID(ByVal Value As String)
    mTillID = Value
End Property

Public Property Get TransactionNumber() As String
    TransactionNumber = mTransactionNumber
End Property

Public Property Let TransactionNumber(ByVal Value As String)
    mTransactionNumber = Value
End Property

Public Property Get SequenceNumber() As Long
    SequenceNumber = mSequenceNumber
End Property

Public Property Let SequenceNumber(ByVal Value As Long)
    mSequenceNumber = Value
End Property

Public Property Get TenderType() As String
    TenderType = mTenderType
End Property

Public Property Let TenderType(ByVal Value As String)
    mTenderType = Value
End Property

Public Property Get TenderAmount() As Currency
    TenderAmount = mTenderAmount
End Property

Public Property Let TenderAmount(ByVal Value As Currency)
    mTenderAmount = Value
End Property

Public Property Get CreditCardNumber() As String
    CreditCardNumber = mCreditCardNumber
End Property

Public Property Let CreditCardNumber(ByVal Value As String)
    mCreditCardNumber = Value
End Property

Public Property Get CreditCardStartDate() As String
    CreditCardStartDate = mCreditCardStartDate
End Property

Public Property Let CreditCardStartDate(ByVal Value As String)
    mCreditCardStartDate = Value
End Property

Public Property Get CreditCardExpiryDate() As String
    CreditCardExpiryDate = mCreditCardExpiryDate
End Property

Public Property Let CreditCardExpiryDate(ByVal Value As String)
    mCreditCardExpiryDate = Value
End Property

Public Property Get IssuerNumber() As String
    IssuerNumber = mIssuerNumber
End Property

Public Property Let IssuerNumber(ByVal Value As String)
    mIssuerNumber = Value
End Property

Public Property Get AuthorisationCode() As String
    AuthorisationCode = mAuthorisationCode
End Property

Public Property Let AuthorisationCode(ByVal Value As String)
    mAuthorisationCode = Value
End Property

Public Property Get CardSecurityCode() As String
    CardSecurityCode = mCardSecurityCode
End Property

Public Property Let CardSecurityCode(ByVal Value As String)
    mCardSecurityCode = Value
End Property

Public Property Get CCNumberKeyed() As Boolean
    CCNumberKeyed = mCCNumberKeyed
End Property

Public Property Let CCNumberKeyed(ByVal Value As Boolean)
    mCCNumberKeyed = Value
End Property

Public Property Get ChequeAccountNumber() As String
    ChequeAccountNumber = mChequeAccountNumber
End Property

Public Property Let ChequeAccountNumber(ByVal Value As String)
    mChequeAccountNumber = Value
End Property

Public Property Get ChequeSortCode() As String
    ChequeSortCode = mChequeSortCode
End Property

Public Property Let ChequeSortCode(ByVal Value As String)
    mChequeSortCode = Value
End Property

Public Property Get ChequeNumber() As String
    ChequeNumber = mChequeNumber
End Property

Public Property Let ChequeNumber(ByVal Value As String)
    mChequeNumber = Value
End Property

Public Property Get SupervisorCode() As String
    SupervisorCode = mSupervisorCode
End Property

Public Property Let SupervisorCode(ByVal Value As String)
    mSupervisorCode = Value
End Property

Public Property Get EFTPOSVoucherNo() As String
    EFTPOSVoucherNo = mEFTPOSVoucherNo
End Property

Public Property Let EFTPOSVoucherNo(ByVal Value As String)
    mEFTPOSVoucherNo = Value
End Property

Public Property Get AuthorisationType() As String
    AuthorisationType = mAuthorisationType
End Property

Public Property Let AuthorisationType(ByVal Value As String)
    mAuthorisationType = Value
End Property

Public Property Get CashBackAmount() As Currency
    CashBackAmount = mCashBackAmount
End Property

Public Property Let CashBackAmount(ByVal Value As Currency)
    mCashBackAmount = Value
End Property

Public Property Get CouponPostCode() As String
    CouponPostCode = mCouponPostCode
End Property

Public Property Let CouponPostCode(ByVal Value As String)
    mCouponPostCode = Value
End Property

Public Property Get CashBalanceProcessed() As Boolean
    CashBalanceProcessed = mCashBalanceProcessed
End Property

Public Property Let CashBalanceProcessed(ByVal Value As Boolean)
    mCashBalanceProcessed = Value
End Property

Public Property Get EFTPOSMerchantNo() As String
    EFTPOSMerchantNo = mEFTPOSMerchantNo
End Property

Public Property Let EFTPOSMerchantNo(ByVal Value As String)
    mEFTPOSMerchantNo = Value
End Property

Public Property Get DigitCount() As Long
    DigitCount = mDigitCount
End Property

Public Property Let DigitCount(ByVal Value As Long)
    mDigitCount = Value
End Property

Public Property Get EFTPOSCommsDone() As Boolean
    EFTPOSCommsDone = mEFTPOSCommsDone
End Property

Public Property Let EFTPOSCommsDone(ByVal Value As Boolean)
    mEFTPOSCommsDone = Value
End Property

Public Property Get CouponNumber() As String
    CouponNumber = mCouponNumber
End Property

Public Property Let CouponNumber(ByVal Value As String)
    mCouponNumber = Value
End Property

Public Property Get CreditCardName() As String
    CreditCardName = mCreditCardName
End Property

Public Property Let CreditCardName(ByVal Value As String)
    mCreditCardName = Value
End Property

Public Property Get CouponClass() As String
    CouponClass = mCouponClass
End Property

Public Property Let CouponClass(ByVal Value As String)
    mCouponClass = Value
End Property


Public Property Get CurrencyCode() As String
    CurrencyCode = mCurrencyCode
End Property

Public Property Let CurrencyCode(ByVal Value As String)
    mCurrencyCode = Value
End Property

Public Property Get ExchangeRate() As Double
    ExchangeRate = mExchangeRate
End Property

Public Property Let ExchangeRate(ByVal Value As Double)
    mExchangeRate = Value
End Property

Public Property Get ExchangeFactor() As Long
    ExchangeFactor = mExchangeFactor
End Property

Public Property Let ExchangeFactor(ByVal Value As Long)
    mExchangeFactor = Value
End Property

Public Property Get ForeignTender() As Currency
    ForeignTender = mForeignTender
End Property

Public Property Let ForeignTender(ByVal Value As Currency)
    mForeignTender = Value
End Property

Public Property Get TerminalID() As String
    TerminalID = mTerminalID
    TerminalID = mChequeAccountNumber & mChequeSortCode
End Property

Public Property Let TerminalID(ByVal Value As String)
    mTerminalID = Value
    mChequeAccountNumber = Left$(mTerminalID, 8)
    mChequeSortCode = Mid$(mTerminalID, 9)
End Property

Public Property Get PANSeq() As String
    PANSeq = mPANSeq
    PANSeq = mCardSecurityCode
End Property

Public Property Let PANSeq(ByVal Value As String)
    mPANSeq = Value
    mCardSecurityCode = Value
End Property

Public Property Get CardAppID() As String
    CardAppID = mCardAppID
End Property

Public Property Let CardAppID(ByVal Value As String)
    mCardAppID = Value
End Property

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_POSPAYMENT

End Property

Public Property Get TransactionId() As Double
    TransactionId = mTrasactionID
End Property

Public Property Let TransactionId(ByVal Value As Double)
    mTrasactionID = Value
End Property

Public Property Get MessageNumber() As String
    MessageNumber = mMessageNumber
End Property

Public Property Let MessageNumber(ByVal Value As String)
    mMessageNumber = Value
End Property

Public Property Get GCTenderAmount() As Currency
    GCTenderAmount = mGCTenderAmount
End Property

Public Property Let GCTenderAmount(ByVal Value As Currency)
    mGCTenderAmount = Value
End Property

Public Function SetEFTAsSubmitted() As Boolean

Dim oRow     As Object 'list of fields to save
Dim blnSaved As Boolean
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_POSPAYMENT_TransactionDate))
    Call oRow.Add(GetField(FID_POSPAYMENT_TillID))
    Call oRow.Add(GetField(FID_POSPAYMENT_TransactionNumber))
    Call oRow.Add(GetField(FID_POSPAYMENT_SequenceNumber))
    
    'Add the status field that need updating
    mEFTConfirmed = "S"
    Call oRow.Add(GetField(FID_POSPAYMENT_EFTConfirmed))
        
    blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert blnSaved
    Set oRow = Nothing
    
    SetEFTAsSubmitted = blnSaved

End Function 'SetEFTAsSubmitted


Public Function SetEFTAsConfirmed() As Boolean

Dim oRow     As Object 'list of fields to save
Dim blnSaved As Boolean
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    ' Identify row by the key
    Call oRow.Add(GetField(FID_POSPAYMENT_TransactionDate))
    Call oRow.Add(GetField(FID_POSPAYMENT_TillID))
    Call oRow.Add(GetField(FID_POSPAYMENT_TransactionNumber))
    Call oRow.Add(GetField(FID_POSPAYMENT_SequenceNumber))
    
    'Add the status field that need updating
    mEFTConfirmed = "C"
    Call oRow.Add(GetField(FID_POSPAYMENT_EFTConfirmed))
        
    blnSaved = m_oSession.Database.SavePartialBo(Me, oRow)
    Debug.Assert blnSaved
    Set oRow = Nothing
    
    SetEFTAsConfirmed = blnSaved

End Function 'SetEFTAsConfirmed


Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String

    IBo_DebugString = "POSPayment " & mTransactionNumber

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function
'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_POSPAYMENT_TransactionDate):      mTransactionDate = oField.ValueAsVariant
            Case (FID_POSPAYMENT_TillID):               mTillID = oField.ValueAsVariant
            Case (FID_POSPAYMENT_TransactionNumber):    mTransactionNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENT_SequenceNumber):       mSequenceNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENT_TenderType):           mTenderType = oField.ValueAsVariant
            Case (FID_POSPAYMENT_TenderAmount):         mTenderAmount = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CreditCardNumber):     mCreditCardNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CreditCardStartDate):  mCreditCardStartDate = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CreditCardExpiryDate): mCreditCardExpiryDate = oField.ValueAsVariant
            Case (FID_POSPAYMENT_IssuerNumber):         mIssuerNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENT_AuthorisationCode):    mAuthorisationCode = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CCNumberKeyed):        mCCNumberKeyed = oField.ValueAsVariant
            Case (FID_POSPAYMENT_ChequeAccountNumber):  mChequeAccountNumber = oField.ValueAsVariant
                                                        mTerminalID = mChequeAccountNumber & mChequeSortCode
            Case (FID_POSPAYMENT_ChequeSortCode):       mChequeSortCode = oField.ValueAsVariant
                                                        mTerminalID = mChequeAccountNumber & mChequeSortCode
            Case (FID_POSPAYMENT_ChequeNumber):         mChequeNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENT_SupervisorCode):       mSupervisorCode = oField.ValueAsVariant
            Case (FID_POSPAYMENT_EFTPOSVoucherNo):      mEFTPOSVoucherNo = oField.ValueAsVariant
            Case (FID_POSPAYMENT_EFTPOSCommsDone):      mEFTPOSCommsDone = oField.ValueAsVariant
            Case (FID_POSPAYMENT_EFTPOSMerchantNo):     mEFTPOSMerchantNo = oField.ValueAsVariant
            Case (FID_POSPAYMENT_DigitCount):           mDigitCount = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CouponNumber):         mCouponNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CouponPostCode):       mCouponPostCode = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CashBalanceProcessed): mCashBalanceProcessed = oField.ValueAsVariant
            Case (FID_POSPAYMENT_AuthorisationType):    mAuthorisationType = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CashBackAmount):       mCashBackAmount = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CreditCardName):       mCreditCardName = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CouponClass):          mCouponClass = oField.ValueAsVariant
            Case (FID_POSPAYMENT_EFTConfirmID):         mEFTConfirmID = oField.ValueAsVariant
            Case (FID_POSPAYMENT_EFTConfirmed):         mEFTConfirmed = oField.ValueAsVariant
            Case (FID_POSPAYMENT_AuthorisationMethod):  mAuthorisationMethod = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CardSecurityCode):     mCardSecurityCode = oField.ValueAsVariant
                                                        mPANSeq = mCardSecurityCode
            Case (FID_POSPAYMENT_CurrencyCode):         mCurrencyCode = oField.ValueAsVariant
            Case (FID_POSPAYMENT_ExchangeRate):         mExchangeRate = oField.ValueAsVariant
            Case (FID_POSPAYMENT_ExchangeFactor):       mExchangeFactor = oField.ValueAsVariant
            Case (FID_POSPAYMENT_ForeignTender):        mForeignTender = oField.ValueAsVariant
            Case (FID_POSPAYMENT_TerminalID):           mTerminalID = oField.ValueAsVariant
            Case (FID_POSPAYMENT_PANSeq):               mPANSeq = oField.ValueAsVariant
            Case (FID_POSPAYMENT_CardAppID):            mCardAppID = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow



Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean

    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database

Dim oCardInfo As cCardInfo
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If LenB(mCreditCardNumber) = 0 Then mCreditCardNumber = "0000000000000000000"
        If LenB(mCreditCardStartDate) = 0 Then mCreditCardStartDate = "0000"
        If LenB(mCreditCardExpiryDate) = 0 Then mCreditCardExpiryDate = "0000"
        If LenB(mCouponNumber) = 0 Then mCouponNumber = "000000"
        If LenB(mChequeAccountNumber) = 0 Then mChequeAccountNumber = "0000000000"
        If LenB(mChequeSortCode) = 0 Then mChequeSortCode = "000000"
        If LenB(mChequeNumber) = 0 Then mChequeNumber = "000000"
        If LenB(mSupervisorCode) = 0 Then mSupervisorCode = "000"
        If LenB(mEFTPOSVoucherNo) = 0 Then mEFTPOSVoucherNo = "0000"
        mCouponClass = "00"
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Public Sub SaveGiftCard(Optional ByVal doTopupGiftCard = False)
    Dim strSql As String
    Dim strTranCode As String
    
    If (doTopupGiftCard And POSHeader.TransactionCode <> "RF") Then
        strTranCode = POSHeader.TransactionCode
    Else
        Select Case POSHeader.TransactionCode
            Case "SA"
                strTranCode = "TS"
            Case "RF"
                strTranCode = "TR"
        End Select
    End If
    
    strSql = "EXEC [dbo].[usp_SaveDataInDLGiftCard] " & _
    "@DATE1 = '" & Format(Me.TransactionDate, "YYYY-MM-DD") & _
    "', @TILL = '" & Me.TillID & _
    "', @TRAN = '" & Me.TransactionNumber & _
    "', @CARDNUM = '" & Me.CreditCardNumber & _
    "', @EEID = '" & POSHeader.CashierID & _
    "', @TYPE = '" & strTranCode & _
    "', @AMNT = " & -Me.GCTenderAmount & _
    ", @AUTH = '" & Me.AuthorisationCode & _
    "', @MSGNUM = '" & Me.MessageNumber & _
    "', @TRANID = " & Me.TransactionId

    m_oSession.Database.ExecuteCommand (strSql)
    
    If Err.Number <> 0 Then Call DebugMsg(MODULE_NAME, "SaveGiftCard", endlDebug, "Error when saving data:" & Err.Description)
    Call Err.Clear
End Sub


Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_POSPAYMENT * &H10000) + 1 To FID_POSPAYMENT_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPOSPayment

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_POSPAYMENT, FID_POSPAYMENT_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_POSPAYMENT_TransactionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTransactionDate
        Case (FID_POSPAYMENT_TillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillID
        Case (FID_POSPAYMENT_TransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNumber
        Case (FID_POSPAYMENT_SequenceNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSequenceNumber
        Case (FID_POSPAYMENT_TenderType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTenderType
        Case (FID_POSPAYMENT_TenderAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTenderAmount
        Case (FID_POSPAYMENT_CreditCardNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCreditCardNumber
        Case (FID_POSPAYMENT_CreditCardStartDate):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCreditCardStartDate
        Case (FID_POSPAYMENT_CreditCardExpiryDate):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCreditCardExpiryDate
        Case (FID_POSPAYMENT_IssuerNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mIssuerNumber
        Case (FID_POSPAYMENT_AuthorisationCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAuthorisationCode
        Case (FID_POSPAYMENT_CCNumberKeyed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCCNumberKeyed
        Case (FID_POSPAYMENT_ChequeAccountNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mChequeAccountNumber
        Case (FID_POSPAYMENT_ChequeSortCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mChequeSortCode
        Case (FID_POSPAYMENT_ChequeNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mChequeNumber
        Case (FID_POSPAYMENT_SupervisorCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSupervisorCode
        Case (FID_POSPAYMENT_EFTPOSVoucherNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEFTPOSVoucherNo
        Case (FID_POSPAYMENT_AuthorisationType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAuthorisationType
        Case (FID_POSPAYMENT_CashBackAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCashBackAmount
        Case (FID_POSPAYMENT_CouponPostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCouponPostCode
        Case (FID_POSPAYMENT_CashBalanceProcessed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCashBalanceProcessed
        Case (FID_POSPAYMENT_EFTPOSMerchantNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEFTPOSMerchantNo
        Case (FID_POSPAYMENT_DigitCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mDigitCount
        Case (FID_POSPAYMENT_EFTPOSCommsDone):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mEFTPOSCommsDone
        Case (FID_POSPAYMENT_CouponNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCouponNumber
        Case (FID_POSPAYMENT_CreditCardName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCreditCardName
        Case (FID_POSPAYMENT_CouponClass):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCouponClass
        Case (FID_POSPAYMENT_EFTConfirmID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEFTConfirmID
        Case (FID_POSPAYMENT_EFTConfirmed):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEFTConfirmed
        Case (FID_POSPAYMENT_AuthorisationMethod):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAuthorisationMethod
        Case (FID_POSPAYMENT_CardSecurityCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCardSecurityCode
        Case (FID_POSPAYMENT_CurrencyCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCurrencyCode
        Case (FID_POSPAYMENT_ExchangeRate):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mExchangeRate
        Case (FID_POSPAYMENT_ExchangeFactor):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mExchangeRate
        Case (FID_POSPAYMENT_ForeignTender):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mForeignTender
        Case (FID_POSPAYMENT_TerminalID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTerminalID
        Case (FID_POSPAYMENT_PANSeq):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPANSeq
        Case (FID_POSPAYMENT_CardAppID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCardAppID
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPOSPayment
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function
Public Function Interface(Optional eInterfaceType As Long) As cPOSPayment

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_POSPAYMENT_END_OF_STATIC

End Function

Friend Function GetFileLine() As String

Dim strTemp As String

    strTemp = ""
    strTemp = strTemp & FmtVal(mTransactionDate, enftDate)
    strTemp = strTemp & FmtVal(mTillID, enftCode, 2)
    strTemp = strTemp & FmtVal(mTransactionNumber, enftCode, 4)
    strTemp = strTemp & FmtVal(mSequenceNumber, enftCode, 5)
    strTemp = strTemp & FmtVal(mTenderType, enftNumber, 2)
    strTemp = strTemp & FmtVal(mTenderAmount, enftNumber, 8, 2)
    strTemp = strTemp & FmtVal(mCreditCardNumber, enftCode, 19)
    strTemp = strTemp & FmtVal(mCreditCardExpiryDate, enftCode, 4)
    strTemp = strTemp & FmtVal(mCouponNumber, enftCode, 6)
    strTemp = strTemp & FmtVal(mCouponClass, enftCode, 2)
    strTemp = strTemp & FmtVal(mAuthorisationCode, enftString, 9)
    strTemp = strTemp & FmtVal(mCCNumberKeyed, enftYesNo)
    strTemp = strTemp & FmtVal(mSupervisorCode, enftCode, 3)
    strTemp = strTemp & FmtVal(mCouponPostCode, enftString, 8)
    strTemp = strTemp & FmtVal(mChequeAccountNumber, enftCode, 10)
    strTemp = strTemp & FmtVal(mChequeSortCode, enftCode, 6)
    strTemp = strTemp & FmtVal(mChequeNumber, enftCode, 6)
    strTemp = strTemp & FmtVal(mCashBalanceProcessed, enftYesNo)
    strTemp = strTemp & FmtVal(mEFTPOSVoucherNo, enftCode, 4)
    strTemp = strTemp & FmtVal(mIssuerNumber, enftString, 2)
    strTemp = strTemp & FmtVal(mAuthorisationType, enftString, 1)
    strTemp = strTemp & FmtVal(mEFTPOSMerchantNo, enftString, 15)
    strTemp = strTemp & FmtVal(mCashBackAmount, enftNumber, 8, 2)
    strTemp = strTemp & FmtVal(mDigitCount, enftCode, 5)
    strTemp = strTemp & FmtVal(mEFTPOSCommsDone, enftYesNo)
    strTemp = strTemp & FmtVal(mEFTConfirmID, enftString, 6)
    strTemp = strTemp & FmtVal(mEFTConfirmed, enftString, 1)
    strTemp = strTemp & FmtVal(mCreditCardName, enftString, 30) ' Commidea Into WSS - tender type very much dependant on this so better to include it
    
    GetFileLine = strTemp
            
End Function
