VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StockAdjustmentMarkdown"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Implements IStockAdjustmentMarkdown

Private Function IStockAdjustmentMarkdown_InserrtStockAdjustmentMarkdown(SaleLine As cPOSLine, oConnection As ADODB.Connection, StartingStock As Long) As Variant
    
    Dim strSql As String
    
    strSql = "EXEC [dbo].[usp_InsertStockAdjustment] " & _
        "@Adjustmentdate = '" + Format(Now(), "yyyy-mm-dd") + "'," & _
        "@AdjustmentCode = '" + CStr(SaleLine.PriceOverrideReason) + "'," & _
        "@Skun = '" + SaleLine.PartCode + "'," & _
        "@Seqn = '" + GetNextSequenceNumber(SaleLine.PartCode, CStr(SaleLine.PriceOverrideReason), oConnection) + "'," & _
        "@AmendId = 0," & _
        "@Initials = '" + GetEmployeeInitials(CInt(SaleLine.POSHeader.CashierID), oConnection) + "'," & _
        "@Department = '" + SaleLine.DepartmentNumber + "'," & _
        "@StartingStock = " + CStr(StartingStock) + "," & _
        "@AdjustmentQty = " + CStr(SaleLine.QuantitySold) + "," & _
        "@AdjustmentPrice =" + CStr((SaleLine.ActualSellPrice - SaleLine.LookUpPrice)) + "," & _
        "@AdjustmentCost = 0.00," & _
        "@IsSentToHO = False," & _
        "@AdjustmentType = 'N'," & _
        "@Comment = 'Till Markdown Sale'," & _
        "@DRLNumber = ''," & _
        "@ReversalCode = ''," & _
        "@MarkDownWriteOff = 'M'," & _
        "@WriteOffAuthorised = ''," & _
        "@PeriodId = 0," & _
        "@IsReversed = 0"
        
    Call oConnection.Execute(strSql)

End Function

Private Function GetEmployeeInitials(EmployeeId As Integer, oConnection As Connection) As String
    Dim strSql As String
    Dim rs As ADODB.Recordset
    Dim strInitials As String
    
    strInitials = ""
    strSql = "EXEC [dbo].[usp_GetEmployeeInitials] @Id = 88"
    Set rs = oConnection.Execute(strSql)
    If rs.BOF = False And rs.EOF = False Then
        strInitials = rs.Fields(0)
    End If
    GetEmployeeInitials = Trim(strInitials)
End Function

Private Function GetNextSequenceNumber(SkuNumber As String, Code As String, oConnection As Connection) As String
    Dim strSql As String
    Dim rs As ADODB.Recordset
    Dim seqNo As Integer
    
    seqNo = 0
    strSql = "EXEC [dbo].[usp_GetStockAdjustmentForSkuToday] " & _
            "@ProductCode ='" + SkuNumber + "'," & _
            "@AdjustDate = '" + Format(Now, "yyyy-mm-dd") + "'," & _
            "@CodeNumber = '" + Code + "'"
    
    Set rs = oConnection.Execute(strSql)
    If rs.BOF = False And rs.EOF = False Then
       seqNo = CInt(rs.Fields("SEQN")) + 1
    End If
    GetNextSequenceNumber = Format(seqNo, "00")
End Function

