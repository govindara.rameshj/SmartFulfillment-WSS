VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSoldCoupon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module: cSoldCoupon
'* Date  : 09/02/09
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/POSLine.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of the Till Sales Slip Lines
'**********************************************************************************************
'* $Author: Richardc $
'* $Date: 3/06/04 10:17 $
'* $Revision: 12 $
'* Versions:
'* 09/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cSoldCoupon"

Implements IBo
Implements ISysBo

Private Const POS_LINE_REFUND_CODE As String = "R"

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Private mStoreID As String

Private mTransactionDate As Date

Private mTransactionTill As String

Private mTransactionNumber As String

Private mCouponID As String

Private mSequence As String

Private mStatus As String

Private mExclusiveCoupon As Boolean

Private mIssueStoreNo As String

Private mMarketRef As String

Private mReUsable As Boolean

Private mSerialNo As String

Private mManagerID As String

Private mRTIFlag As String

Public POSHeader As cPOSHeader

Public Property Get StoreID() As String
   Let StoreID = mStoreID
End Property

Public Property Let StoreID(ByVal Value As String)
    Let mStoreID = Value
End Property

Public Property Get TransactionTill() As String
   Let TransactionTill = mTransactionTill
End Property

'##ModelId=3D53CE2601EA
Public Property Let TransactionTill(ByVal Value As String)
    Let mTransactionTill = Value
End Property

Public Property Get TransactionDate() As Date
   Let TransactionDate = mTransactionDate
End Property

Public Property Let TransactionDate(ByVal Value As Date)
    Let mTransactionDate = Value
End Property

Public Property Get TransactionNumber() As String
   Let TransactionNumber = mTransactionNumber
End Property

Public Property Let TransactionNumber(ByVal Value As String)
    Let mTransactionNumber = Value
End Property

Public Property Get SerialNo() As String
   Let SerialNo = mSerialNo
End Property

Public Property Let SerialNo(ByVal Value As String)
    Let mSerialNo = Value
End Property

Public Property Get ExclusiveCoupon() As Boolean
   Let ExclusiveCoupon = mExclusiveCoupon
End Property

Public Property Let ExclusiveCoupon(ByVal Value As Boolean)
    Let mExclusiveCoupon = Value
End Property

Public Property Get IssueStoreNo() As String
   Let IssueStoreNo = mIssueStoreNo
End Property

Public Property Let IssueStoreNo(ByVal Value As String)
    Let mIssueStoreNo = Value
End Property

Public Property Get MarketRef() As String
   Let MarketRef = mMarketRef
End Property

Public Property Let MarketRef(ByVal Value As String)
    Let mMarketRef = Value
End Property

Public Property Get ReUsable() As Boolean
   Let ReUsable = mReUsable
End Property

Public Property Let ReUsable(ByVal Value As Boolean)
    Let mReUsable = Value
End Property

Public Property Get Status() As String
   Let Status = mStatus
End Property

Public Property Let Status(ByVal Value As String)
    Let mStatus = Value
End Property

Public Property Get CouponID() As String
   Let CouponID = mCouponID
End Property

Public Property Let CouponID(ByVal Value As String)
    Let mCouponID = Value
End Property

Public Property Get Sequence() As String
   Let Sequence = mSequence
End Property

Public Property Let Sequence(ByVal Value As String)
    Let mSequence = Value
End Property

Public Property Get ManagerID() As String
   Let ManagerID = mManagerID
End Property

Public Property Let ManagerID(ByVal Value As String)
    Let mManagerID = Value
End Property

Public Property Get RTIFlag() As String
    RTIFlag = mRTIFlag
End Property

Public Property Let RTIFlag(ByVal Value As String)
    mRTIFlag = Value
End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            'Case (FID_SOLDCOUPON_StoreId):              mStoreID = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_TransactionDate):      mTransactionDate = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_TransactionTill):      mTransactionTill = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_TransactionNumber):    mTransactionNumber = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_CouponID):             mCouponID = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_Sequence):             mSequence = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_Status):               mStatus = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_ExclusiveCoupon):      mExclusiveCoupon = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_IssueStoreNo):         mIssueStoreNo = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_MarketRef):            mMarketRef = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_ReUsable):             mReUsable = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_SerialNo):             mSerialNo = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_ManagerID):            mManagerID = oField.ValueAsVariant
            Case (FID_SOLDCOUPON_RTIFlag):              mRTIFlag = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SOLDCOUPON

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String

    IBo_DebugString = "SoldCoupon " & mTransactionNumber & " Line " & mSequence

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean

    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Call DebugMsg(MODULE_NAME, "Save", endlDebug, "Enter")
    
    Save = False
    
    If IsValid() Then
        If (mIssueStoreNo = "") Then mIssueStoreNo = "000"
        If (mMarketRef = "") Then mMarketRef = "0000"
        If (mSerialNo = "") Then mSerialNo = "000000"
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If
    Save = True

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_SOLDCOUPON * &H10000) + 1 To FID_SOLDCOUPON_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cSoldCoupon

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SOLDCOUPON, FID_SOLDCOUPON_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
'        Case (FID_SOLDCOUPON_StoreId):
'                Set GetField = New CFieldString
'                GetField.ValueAsVariant = mStoreID
        Case (FID_SOLDCOUPON_TransactionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTransactionDate
        Case (FID_SOLDCOUPON_TransactionTill):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionTill
        Case (FID_SOLDCOUPON_TransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNumber
        Case (FID_SOLDCOUPON_CouponID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCouponID
        Case (FID_SOLDCOUPON_Sequence):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSequence
        Case (FID_SOLDCOUPON_Status):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStatus
        Case (FID_SOLDCOUPON_ExclusiveCoupon):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mExclusiveCoupon
        Case (FID_SOLDCOUPON_IssueStoreNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mIssueStoreNo
        Case (FID_SOLDCOUPON_MarketRef):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMarketRef
        Case (FID_SOLDCOUPON_ReUsable):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mReUsable
        Case (FID_SOLDCOUPON_SerialNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSerialNo
        Case (FID_SOLDCOUPON_ManagerID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mManagerID
        Case (FID_SOLDCOUPON_RTIFlag):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRTIFlag
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cSoldCoupon
    Set m_oSession = oSession
    Set Initialise = Me
End Function


Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

    

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SOLDCOUPON_END_OF_STATIC

End Function

Friend Function GetFileLine() As String

Dim strOut As String
    strOut = vbNullString
    strOut = strOut & FmtVal(mStoreID, enftCode, 3)
    strOut = strOut & FmtVal(mTransactionDate, enftDate)
    strOut = strOut & FmtVal(mTransactionTill, enftCode, 2)
    strOut = strOut & FmtVal(mTransactionNumber, enftCode, 4)
    strOut = strOut & FmtVal(mCouponID, enftCode, 7)
    strOut = strOut & FmtVal(mSequence, enftCode, 4)
    strOut = strOut & FmtVal(mStatus, enftCode, 1)
    strOut = strOut & FmtVal(IIf(mExclusiveCoupon = True, "Y", "N"), enftString, 1)
    strOut = strOut & FmtVal(mMarketRef, enftString, 4)
    strOut = strOut & FmtVal(IIf(mReUsable = True, "Y", "N"), enftString, 1)
    strOut = strOut & FmtVal(mIssueStoreNo, enftCode, 3)
    strOut = strOut & FmtVal(mSerialNo, enftCode, 6)
    strOut = strOut & FmtVal(mManagerID, enftCode, 3)
    strOut = strOut & FmtVal(mRTIFlag, enftString, 1)
    GetFileLine = strOut

End Function



