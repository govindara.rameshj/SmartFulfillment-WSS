VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPriceLineInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"40B4462E038D"
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cPriceLineInfo"

Private mTranDate As Date

Private mTillID As String

Private mTransactionNo As String

Private mSequenceNo As String

Private mCompetitorName As String

Private mCompetitorAddress1 As String

Private mCompetitorAddress2 As String

Private mCompetitorAddress3 As String

Private mCompetitorAddress4 As String

Private mCompetitorPostcode As String

Private mCompetitorPhone As String

Private mCompetitorFax As String

Private mEnteredPrice As Currency

Private mIncludeVAT As Boolean

Private mConvertedPrice As Currency

Private mPrevious As Boolean

Private mOrigStoreNumber As String

Private mOrigTillNumber As String

Private mOrigTransactionNumber As String

Private mOrigTransDate As Date

Private mOrigSellingPrice As Currency

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=40B45550032A
Public Property Get OrigSellingPrice() As Currency
   Let OrigSellingPrice = mOrigSellingPrice
End Property

'##ModelId=40B455500244
Public Property Let OrigSellingPrice(ByVal Value As Currency)
    Let mOrigSellingPrice = Value
End Property

'##ModelId=40B4555001B7
Public Property Get OrigTransDate() As Date
   Let OrigTransDate = mOrigTransDate
End Property

'##ModelId=40B4555000D1
Public Property Let OrigTransDate(ByVal Value As Date)
    Let mOrigTransDate = Value
End Property

'##ModelId=40B45550004F
Public Property Get OrigTransactionNumber() As String
   Let OrigTransactionNumber = mOrigTransactionNumber
End Property

'##ModelId=40B4554F0350
Public Property Let OrigTransactionNumber(ByVal Value As String)
    Let mOrigTransactionNumber = Value
End Property

'##ModelId=40B4554F02CE
Public Property Get OrigTillNumber() As String
   Let OrigTillNumber = mOrigTillNumber
End Property

'##ModelId=40B4554F01F2
Public Property Let OrigTillNumber(ByVal Value As String)
    Let mOrigTillNumber = Value
End Property

'##ModelId=40B4554F0170
Public Property Get OrigStoreNumber() As String
   Let OrigStoreNumber = mOrigStoreNumber
End Property

'##ModelId=40B4554F0093
Public Property Let OrigStoreNumber(ByVal Value As String)
    Let mOrigStoreNumber = Value
End Property

'##ModelId=40B4554F0007
Public Property Get Previous() As Boolean
   Let Previous = mPrevious
End Property

'##ModelId=40B4554E031D
Public Property Let Previous(ByVal Value As Boolean)
    Let mPrevious = Value
End Property

'##ModelId=40B4554E0010
Public Property Get ConvertedPrice() As Currency
   Let ConvertedPrice = mConvertedPrice
End Property

'##ModelId=40B4554D0330
Public Property Let ConvertedPrice(ByVal Value As Currency)
    Let mConvertedPrice = Value
End Property

'##ModelId=40B4554D02C1
Public Property Get IncludeVAT() As Boolean
   Let IncludeVAT = mIncludeVAT
End Property

'##ModelId=40B4554D01F9
Public Property Let IncludeVAT(ByVal Value As Boolean)
    Let mIncludeVAT = Value
End Property

'##ModelId=40B4554D005F
Public Property Get EnteredPrice() As Currency
   Let EnteredPrice = mEnteredPrice
End Property

'##ModelId=40B4554C0388
Public Property Let EnteredPrice(ByVal Value As Currency)
    Let mEnteredPrice = Value
End Property

'##ModelId=40B4554C031A
Public Property Get CompetitorFax() As String
   Let CompetitorFax = mCompetitorFax
End Property

'##ModelId=40B4554C0270
Public Property Let CompetitorFax(ByVal Value As String)
    Let mCompetitorFax = Value
End Property

'##ModelId=40B4554C0202
Public Property Get CompetitorPhone() As String
   Let CompetitorPhone = mCompetitorPhone
End Property

'##ModelId=40B4554C0157
Public Property Let CompetitorPhone(ByVal Value As String)
    Let mCompetitorPhone = Value
End Property

'##ModelId=40B4554C00E9
Public Property Get CompetitorPostcode() As String
   Let CompetitorPostcode = mCompetitorPostcode
End Property

'##ModelId=40B4554C003F
Public Property Let CompetitorPostcode(ByVal Value As String)
    Let mCompetitorPostcode = Value
End Property

Public Property Get CompetitorAddress4() As String
   Let CompetitorAddress4 = mCompetitorAddress4
End Property

'##ModelId=40B4554B0319
Public Property Let CompetitorAddress4(ByVal Value As String)
    Let mCompetitorAddress4 = Value
End Property

'##ModelId=40B4554B03C3
Public Property Get CompetitorAddress3() As String
   Let CompetitorAddress3 = mCompetitorAddress3
End Property

'##ModelId=40B4554B0319
Public Property Let CompetitorAddress3(ByVal Value As String)
    Let mCompetitorAddress3 = Value
End Property

'##ModelId=40B4554B02BF
Public Property Get CompetitorAddress2() As String
   Let CompetitorAddress2 = mCompetitorAddress2
End Property

'##ModelId=40B4554B021E
Public Property Let CompetitorAddress2(ByVal Value As String)
    Let mCompetitorAddress2 = Value
End Property

'##ModelId=40B4554B01BA
Public Property Get CompetitorAddress1() As String
   Let CompetitorAddress1 = mCompetitorAddress1
End Property

'##ModelId=40B4554B011A
Public Property Let CompetitorAddress1(ByVal Value As String)
    Let mCompetitorAddress1 = Value
End Property

'##ModelId=40B4554B00C0
Public Property Get CompetitorName() As String
   Let CompetitorName = mCompetitorName
End Property

'##ModelId=40B4554B002A
Public Property Let CompetitorName(ByVal Value As String)
    Let mCompetitorName = Value
End Property

'##ModelId=40B4554A03AD
Public Property Get SequenceNo() As String
   Let SequenceNo = mSequenceNo
End Property

'##ModelId=40B4554A0321
Public Property Let SequenceNo(ByVal Value As String)
    Let mSequenceNo = Value
End Property

'##ModelId=40B4554A02C7
Public Property Get TransactionNo() As String
   Let TransactionNo = mTransactionNo
End Property

'##ModelId=40B4554A023B
Public Property Let TransactionNo(ByVal Value As String)
    Let mTransactionNo = Value
End Property

'##ModelId=40B4554A01E1
Public Property Get TillID() As String
   Let TillID = mTillID
End Property

'##ModelId=40B4554A0155
Public Property Let TillID(ByVal Value As String)
    Let mTillID = Value
End Property

'##ModelId=40B4554A010E
Public Property Get TranDate() As Date
   Let TranDate = mTranDate
End Property


'##ModelId=40B4554A008C
Public Property Let TranDate(ByVal Value As Date)
    Let mTranDate = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean

Dim dteNullDate As Date
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        
        If (Val(mOrigTillNumber) = 0) And (Val(mOrigTransactionNumber) = 0) Then mOrigTransDate = dteNullDate
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_PRICELINEINFO * &H10000) + 1 To FID_PRICELINEINFO_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPriceLineInfo

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_PRICELINEINFO, FID_PRICELINEINFO_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
         Case (FID_PRICELINEINFO_TranDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTranDate
        Case (FID_PRICELINEINFO_TillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillID
        Case (FID_PRICELINEINFO_TransactionNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNo
        Case (FID_PRICELINEINFO_SequenceNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSequenceNo
        Case (FID_PRICELINEINFO_CompetitorName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorName
        Case (FID_PRICELINEINFO_CompetitorAddress1):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorAddress1
        Case (FID_PRICELINEINFO_CompetitorAddress2):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorAddress2
        Case (FID_PRICELINEINFO_CompetitorAddress3):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorAddress3
        Case (FID_PRICELINEINFO_CompetitorAddress4):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorAddress4
        Case (FID_PRICELINEINFO_CompetitorPostcode):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorPostcode
        Case (FID_PRICELINEINFO_CompetitorPhone):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorPhone
        Case (FID_PRICELINEINFO_CompetitorFax):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mCompetitorFax
        Case (FID_PRICELINEINFO_EnteredPrice):
                 Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mEnteredPrice
        Case (FID_PRICELINEINFO_IncludeVAT):
                 Set GetField = New CFieldBool
                GetField.ValueAsVariant = mIncludeVAT
        Case (FID_PRICELINEINFO_ConvertedPrice):
                 Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mConvertedPrice
        Case (FID_PRICELINEINFO_Previous):
                 Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPrevious
        Case (FID_PRICELINEINFO_OrigStoreNumber):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrigStoreNumber
        Case (FID_PRICELINEINFO_OrigTillNumber):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrigTillNumber
        Case (FID_PRICELINEINFO_OrigTransactionNumber):
                 Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrigTransactionNumber
        Case (FID_PRICELINEINFO_OrigTransDate):
                 Set GetField = New CFieldDate
                GetField.ValueAsVariant = mOrigTransDate
        Case (FID_PRICELINEINFO_OrigSellingPrice):
                 Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrigSellingPrice
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPriceLineInfo
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_PRICELINEINFO

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = " " & mTransactionNo

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_PRICELINEINFO_TranDate):              mTranDate = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_TillID):                mTillID = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_TransactionNo):         mTransactionNo = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_SequenceNo):            mSequenceNo = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorName):        mCompetitorName = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorAddress1):    mCompetitorAddress1 = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorAddress2):    mCompetitorAddress2 = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorAddress3):    mCompetitorAddress3 = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorAddress4):    mCompetitorAddress4 = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorPostcode):    mCompetitorPostcode = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorPhone):       mCompetitorPhone = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_CompetitorFax):         mCompetitorFax = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_EnteredPrice):          mEnteredPrice = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_IncludeVAT):            mIncludeVAT = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_ConvertedPrice):        mConvertedPrice = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_Previous):              mPrevious = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_OrigStoreNumber):       mOrigStoreNumber = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_OrigTillNumber):        mOrigTillNumber = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_OrigTransactionNumber): mOrigTransactionNumber = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_OrigTransDate):         mOrigTransDate = oField.ValueAsVariant
            Case (FID_PRICELINEINFO_OrigSellingPrice):      mOrigSellingPrice = oField.ValueAsVariant
                        
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_PRICELINEINFO_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cPriceLineInfo

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function

Friend Function GetFileLine() As String

Dim strOut As String
    
    strOut = vbNullString
    strOut = strOut & FmtVal(mTranDate, enftDate)
    strOut = strOut & FmtVal(mTillID, enftCode, 2)
    strOut = strOut & FmtVal(mTransactionNo, enftCode, 4)
    strOut = strOut & FmtVal(mSequenceNo, enftCode, 5)
    strOut = strOut & FmtVal(mCompetitorName, enftString, 30)
    strOut = strOut & FmtVal(mCompetitorAddress1, enftString, 30)
    strOut = strOut & FmtVal(mCompetitorAddress2, enftString, 30)
    strOut = strOut & FmtVal(mCompetitorAddress3, enftString, 30)
    strOut = strOut & FmtVal(mCompetitorPostcode, enftString, 8)
    strOut = strOut & FmtVal(mCompetitorPhone, enftString, 15)
    strOut = strOut & FmtVal(mCompetitorFax, enftString, 15)
    strOut = strOut & FmtVal(mEnteredPrice, enftNumber, 7, 2)
    strOut = strOut & FmtVal(mIncludeVAT, enftYesNo)
    strOut = strOut & FmtVal(mConvertedPrice, enftNumber, 7, 2)
    strOut = strOut & FmtVal(mPrevious, enftYesNo)
    strOut = strOut & FmtVal(mOrigStoreNumber, enftCode, 3)
    strOut = strOut & FmtVal(mOrigTillNumber, enftCode, 2)
    strOut = strOut & FmtVal(mOrigTransactionNumber, enftCode, 4)
    If (Year(mOrigTransDate) = 1899) Then
        strOut = strOut & "--/--/--"
    Else
        strOut = strOut & FmtVal(mOrigTransDate, enftDate)
    End If
    strOut = strOut & FmtVal(mOrigSellingPrice, enftNumber, 7, 2)
    
    GetFileLine = strOut

End Function

