VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTrader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB55E0302"
'<CAMH>****************************************************************************************
'* Module : cTrader
'* Date   : 21/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/cTrader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/05/03 10:01 $ $Revision: 5 $
'* Versions:
'* 21/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cTrader"

'##ModelId=3D5CBB0601F4
Private mTraderCardNo As String

'##ModelId=3D5CBB1601AE
Private mTraderName As String

'##ModelId=3D5CBB1D0122
Private mAddressLine1 As String

'##ModelId=3D5CBB240064
Private mAddressLine2 As String

'##ModelId=3D5CBB270154
Private mAddressLine3 As String

'##ModelId=3D5CBB2C026C
Private mPostCode As String

'##ModelId=3D5CBB2F0244
Private mTelephoneNo As String

'##ModelId=3D5CBB350082
Private mFaxNo As String

'##ModelId=3D5CBB3803CA
Private mDateOpened As Date

'##ModelId=3D5CBB470280
Private mTradeDiscount As Currency

'##ModelId=3D5CBB4E030C
Private mTurnoverToDate As Currency

'##ModelId=3D5CBB69001E
Private mLastTransactionDate As Date

'##ModelId=3D5CBB7402DA
Private mDeleted As Boolean

'##ModelId=3D5CBB7C005A
Private mTraderType As String

'##ModelId=3D5CBB9600F0
Private mAccountType As String

'##ModelId=3D5CBB9C01F4
Private mCreditLimit As Long

'##ModelId=3D5CBBA402E4
Private mAccountBalance As Currency

'##ModelId=3D5CBBAF00E6
Private mOnHold As String

'##ModelId=3D5CBBB50258
Private mLastPaymentDate As Date

'##ModelId=3D5CBBBC03C0
Private mOriginatingStoreNo As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3D7738AC000A
Public Property Get OriginatingStoreNo() As String
   Let OriginatingStoreNo = mOriginatingStoreNo
End Property

'##ModelId=3D7738AB026C
Public Property Let OriginatingStoreNo(ByVal Value As String)
    Let mOriginatingStoreNo = Value
End Property

'##ModelId=3D7738AB0190
Public Property Get LastPaymentDate() As Date
   Let LastPaymentDate = mLastPaymentDate
End Property

'##ModelId=3D7738AB0046
Public Property Let LastPaymentDate(ByVal Value As Date)
    Let mLastPaymentDate = Value
End Property

'##ModelId=3D7738AA0352
Public Property Get OnHold() As String
   Let OnHold = mOnHold
End Property

'##ModelId=3D7738AA0208
Public Property Let OnHold(ByVal Value As String)
    Let mOnHold = Value
End Property

'##ModelId=3D7738AA0136
Public Property Get AccountBalance() As Currency
   Let AccountBalance = mAccountBalance
End Property

'##ModelId=3D7738AA001E
Public Property Let AccountBalance(ByVal Value As Currency)
    Let mAccountBalance = Value
End Property

'##ModelId=3D7738A9032A
Public Property Get CreditLimit() As Long
   Let CreditLimit = mCreditLimit
End Property

'##ModelId=3D7738A901E0
Public Property Let CreditLimit(ByVal Value As Long)
    Let mCreditLimit = Value
End Property

'##ModelId=3D7738A90140
Public Property Get AccountType() As String
   Let AccountType = mAccountType
End Property

'##ModelId=3D7738A803DE
Public Property Let AccountType(ByVal Value As String)
    Let mAccountType = Value
End Property

'##ModelId=3D7738A80334
Public Property Get TraderType() As String
   Let TraderType = mTraderType
End Property

'##ModelId=3D7738A801EA
Public Property Let TraderType(ByVal Value As String)
    Let mTraderType = Value
End Property

'##ModelId=3D7738A8014A
Public Property Get Deleted() As Boolean
   Let Deleted = mDeleted
End Property

'##ModelId=3D7738A80032
Public Property Let Deleted(ByVal Value As Boolean)
    Let mDeleted = Value
End Property

'##ModelId=3D7738A7037A
Public Property Get LastTransactionDate() As Date
   Let LastTransactionDate = mLastTransactionDate
End Property

'##ModelId=3D7738A70262
Public Property Let LastTransactionDate(ByVal Value As Date)
    Let mLastTransactionDate = Value
End Property

'##ModelId=3D7738A701C2
Public Property Get TurnoverToDate() As Currency
   Let TurnoverToDate = mTurnoverToDate
End Property

'##ModelId=3D7738A700AA
Public Property Let TurnoverToDate(ByVal Value As Currency)
    Let mTurnoverToDate = Value
End Property

'##ModelId=3D7738A7000A
Public Property Get TradeDiscount() As Currency
   Let TradeDiscount = mTradeDiscount
End Property

'##ModelId=3D7738A60316
Public Property Let TradeDiscount(ByVal Value As Currency)
    Let mTradeDiscount = Value
End Property

'##ModelId=3D7738A60276
Public Property Get DateOpened() As Date
   Let DateOpened = mDateOpened
End Property

'##ModelId=3D7738A6019A
Public Property Let DateOpened(ByVal Value As Date)
    Let mDateOpened = Value
End Property

'##ModelId=3D7738A600F0
Public Property Get FaxNo() As String
   Let FaxNo = mFaxNo
End Property

'##ModelId=3D7738A60014
Public Property Let FaxNo(ByVal Value As String)
    Let mFaxNo = Value
End Property

'##ModelId=3D7738A5035C
Public Property Get TelephoneNo() As String
   Let TelephoneNo = mTelephoneNo
End Property

'##ModelId=3D7738A50280
Public Property Let TelephoneNo(ByVal Value As String)
    Let mTelephoneNo = Value
End Property

'##ModelId=3D7738A50212
Public Property Get PostCode() As String
   Let PostCode = mPostCode
End Property

'##ModelId=3D7738A50136
Public Property Let PostCode(ByVal Value As String)
    Let mPostCode = Value
End Property

'##ModelId=3D7738A500C8
Public Property Get AddressLine3() As String
   Let AddressLine3 = mAddressLine3
End Property

'##ModelId=3D7738A403D4
Public Property Let AddressLine3(ByVal Value As String)
    Let mAddressLine3 = Value
End Property

'##ModelId=3D7738A40366
Public Property Get AddressLine2() As String
   Let AddressLine2 = mAddressLine2
End Property

'##ModelId=3D7738A402BC
Public Property Let AddressLine2(ByVal Value As String)
    Let mAddressLine2 = Value
End Property

'##ModelId=3D7738A4024E
Public Property Get AddressLine1() As String
   Let AddressLine1 = mAddressLine1
End Property

'##ModelId=3D7738A40172
Public Property Let AddressLine1(ByVal Value As String)
    Let mAddressLine1 = Value
End Property

'##ModelId=3D7738A40104
Public Property Get TraderName() As String
   Let TraderName = mTraderName
End Property

'##ModelId=3D7738A40064
Public Property Let TraderName(ByVal Value As String)
    Let mTraderName = Value
End Property

'##ModelId=3D7738A303DE
Public Property Get TraderCardNo() As String
   Let TraderCardNo = mTraderCardNo
End Property


'##ModelId=3D7738A30370
Public Property Let TraderCardNo(ByVal Value As String)
    Let mTraderCardNo = Value
End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim oSysNum As cSystemNumbers
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If eSave = SaveTypeIfNew Then
            Set oSysNum = m_oSession.Database.CreateBusinessObject(CLASSID_SYSTEMNO)
            mTraderCardNo = oSysNum.GetNewCustomerCardNumber
            Set oSysNum = Nothing
        End If
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_TRADER * &H10000) + 1 To FID_TRADER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Trader " & mTraderCardNo

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cTrader

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function
Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_TRADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property


Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_TRADER, FID_TRADER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_TRADER_TraderCardNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTraderCardNo
        Case (FID_TRADER_TraderName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTraderName
        Case (FID_TRADER_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_TRADER_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_TRADER_AddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine3
        Case (FID_TRADER_PostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPostCode
        Case (FID_TRADER_TelephoneNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTelephoneNo
        Case (FID_TRADER_FaxNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFaxNo
        Case (FID_TRADER_DateOpened):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateOpened
        Case (FID_TRADER_TradeDiscount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTradeDiscount
        Case (FID_TRADER_TurnoverToDate):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTurnoverToDate
        Case (FID_TRADER_LastTransactionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mLastTransactionDate
        Case (FID_TRADER_Deleted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeleted
        Case (FID_TRADER_TraderType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTraderType
        Case (FID_TRADER_AccountType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAccountType
        Case (FID_TRADER_CreditLimit):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mCreditLimit
        Case (FID_TRADER_AccountBalance):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mAccountBalance
        Case (FID_TRADER_OnHold):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOnHold
        Case (FID_TRADER_LastPaymentDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mLastPaymentDate
        Case (FID_TRADER_OriginatingStoreNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOriginatingStoreNo
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.ID = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cTrader
    Set m_oSession = oSession
    Set Initialise = Me
End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 21/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.ID)
            Case (FID_TRADER_TraderCardNo):        mTraderCardNo = oField.ValueAsVariant
            Case (FID_TRADER_TraderName):          mTraderName = oField.ValueAsVariant
            Case (FID_TRADER_AddressLine1):        mAddressLine1 = oField.ValueAsVariant
            Case (FID_TRADER_AddressLine2):        mAddressLine2 = oField.ValueAsVariant
            Case (FID_TRADER_AddressLine3):        mAddressLine3 = oField.ValueAsVariant
            Case (FID_TRADER_PostCode):            mPostCode = oField.ValueAsVariant
            Case (FID_TRADER_TelephoneNo):         mTelephoneNo = oField.ValueAsVariant
            Case (FID_TRADER_FaxNo):               mFaxNo = oField.ValueAsVariant
            Case (FID_TRADER_DateOpened):          mDateOpened = oField.ValueAsVariant
            Case (FID_TRADER_TradeDiscount):       mTradeDiscount = oField.ValueAsVariant
            Case (FID_TRADER_TurnoverToDate):      mTurnoverToDate = oField.ValueAsVariant
            Case (FID_TRADER_LastTransactionDate): mLastTransactionDate = oField.ValueAsVariant
            Case (FID_TRADER_Deleted):             mDeleted = oField.ValueAsVariant
            Case (FID_TRADER_TraderType):          mTraderType = oField.ValueAsVariant
            Case (FID_TRADER_AccountType):         mAccountType = oField.ValueAsVariant
            Case (FID_TRADER_CreditLimit):         mCreditLimit = oField.ValueAsVariant
            Case (FID_TRADER_AccountBalance):      mAccountBalance = oField.ValueAsVariant
            Case (FID_TRADER_OnHold):              mOnHold = oField.ValueAsVariant
            Case (FID_TRADER_LastPaymentDate):     mLastPaymentDate = oField.ValueAsVariant
            Case (FID_TRADER_OriginatingStoreNo):  mOriginatingStoreNo = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow


Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

    

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_TRADER_END_OF_STATIC

End Function

Public Function GetSelectSQL() As String

Dim oRSelector  As IRowSelector
Dim nFid        As Long
Dim intFieldNo  As Integer
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
'    If lId <> "" Then mPartCode = sId
    
    Set oRSelector = m_oSession.Root.CreateUtilityObject("CRowSelector")
    'Add in selector first to overide the ALL's
    
    Call oRSelector.AddSelectAll(FID_TRADER_TraderCardNo)
    Call oRSelector.AddSelectAll(FID_TRADER_TraderName)
    
    If Not moRowSel Is Nothing Then
        For intFieldNo = 1 To moRowSel.Count Step 1
            Call oRSelector.AddSelection(moRowSel.FieldSelector(intFieldNo).Comparator, moRowSel.FieldSelector(intFieldNo).Field.Interface)
        Next intFieldNo
    End If
    
    'Set mcolPartCode = New Collection
    ' Pass the selector to the database to get the data view
    GetSelectSQL = m_oSession.Database.GetSelectView(moLoadRow, oRSelector)

End Function



