VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSalesLedger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E6CF553016B"
'<CAMH>****************************************************************************************
'* Module: cSalesLedger
'* Date  : 11/03/03
'* Author: mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/cSalesLedger.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of the Till Sales Slip
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 19/03/03 13:46 $ $Revision: 1 $
'* Versions:
'* 09/08/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cSalesLedger"

'##ModelId=3E6CF5C200AC
Private mEntryID As Long

'##ModelId=3E6CF5CE0262
Private mStoreNumber As String

'##ModelId=3E6CF5D602AA
Private mCustomerNumber As String

'##ModelId=3E6CF5DC030C
Private mTransactionDate As Date

'##ModelId=3E6CF5E502F1
Private mDeliveryDate As Date

'##ModelId=3E6CF5EC0125
Private mPostingDate As Date

'##ModelId=3E6CF5F3005D
Private mPaymentDueDate As Date

'##ModelId=3E6CF5FE02B1
Private mPaidDate As Date

'##ModelId=3E6CF60402B0
Private mPeriodStart As Date

'##ModelId=3E6CF6080215
Private mDocumentNumber As String

'##ModelId=3E6CF6120238
Private mOrderNumber As String

'##ModelId=3E6CF6180291
Private mTransactionType As String

'##ModelId=3E6CF621033E
Private mTotalValue As Double

'##ModelId=3E6CF63A02AD
Private mTotalVAT As Double

'##ModelId=3E6CF6420024
Private mOutstandingValue As Double

'##ModelId=3E6CF652017C
Private mDisputed As Boolean

'##ModelId=3E6CF65E0092
Private mStatementPrinted As Boolean

'##ModelId=3E6CF66D00B2
Private mUserInitials As String

'##ModelId=3E6CF67901B4
Private mDateCommed As Date

'##ModelId=3E6CF6840200
Private mMemo1 As String

'##ModelId=3E6CF68E03DB
Private mMemo2 As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3E6DA24E0019
Public Property Get Memo2() As String
   Let Memo2 = mMemo2
End Property

'##ModelId=3E6DA24D0357
Public Property Let Memo2(ByVal Value As String)
    Let mMemo2 = Value
End Property

'##ModelId=3E6DA24D02FD
Public Property Get Memo1() As String
   Let Memo1 = mMemo1
End Property

'##ModelId=3E6DA24D0252
Public Property Let Memo1(ByVal Value As String)
    Let mMemo1 = Value
End Property

'##ModelId=3E6DA24D01F8
Public Property Get DateCommed() As Date
   Let DateCommed = mDateCommed
End Property

'##ModelId=3E6DA24D0158
Public Property Let DateCommed(ByVal Value As Date)
    Let mDateCommed = Value
End Property

'##ModelId=3E6DA24D00FE
Public Property Get UserInitials() As String
   Let UserInitials = mUserInitials
End Property

'##ModelId=3E6DA24D0068
Public Property Let UserInitials(ByVal Value As String)
    Let mUserInitials = Value
End Property

'##ModelId=3E6DA24D000D
Public Property Get StatementPrinted() As Boolean
   Let StatementPrinted = mStatementPrinted
End Property

'##ModelId=3E6DA24C035F
Public Property Let StatementPrinted(ByVal Value As Boolean)
    Let mStatementPrinted = Value
End Property

'##ModelId=3E6DA24C0305
Public Property Get Disputed() As Boolean
   Let Disputed = mDisputed
End Property

'##ModelId=3E6DA24C026F
Public Property Let Disputed(ByVal Value As Boolean)
    Let mDisputed = Value
End Property

'##ModelId=3E6DA24C021F
Public Property Get OutstandingValue() As Double
   Let OutstandingValue = mOutstandingValue
End Property

'##ModelId=3E6DA24C0189
Public Property Let OutstandingValue(ByVal Value As Double)
    Let mOutstandingValue = Value
End Property

'##ModelId=3E6DA24C0138
Public Property Get TotalVAT() As Double
   Let TotalVAT = mTotalVAT
End Property

'##ModelId=3E6DA24C00AC
Public Property Let TotalVAT(ByVal Value As Double)
    Let mTotalVAT = Value
End Property

'##ModelId=3E6DA24C005C
Public Property Get TotalValue() As Double
   Let TotalValue = mTotalValue
End Property

'##ModelId=3E6DA24B03B8
Public Property Let TotalValue(ByVal Value As Double)
    Let mTotalValue = Value
End Property

'##ModelId=3E6DA24B0368
Public Property Get TransactionType() As String
   Let TransactionType = mTransactionType
End Property

'##ModelId=3E6DA24B02E6
Public Property Let TransactionType(ByVal Value As String)
    Let mTransactionType = Value
End Property

'##ModelId=3E6DA24B0296
Public Property Get OrderNumber() As String
   Let OrderNumber = mOrderNumber
End Property

'##ModelId=3E6DA24B0213
Public Property Let OrderNumber(ByVal Value As String)
    Let mOrderNumber = Value
End Property

'##ModelId=3E6DA24B01C3
Public Property Get DocumentNumber() As String
   Let DocumentNumber = mDocumentNumber
End Property

'##ModelId=3E6DA24B014B
Public Property Let DocumentNumber(ByVal Value As String)
    Let mDocumentNumber = Value
End Property

'##ModelId=3E6DA24B00FB
Public Property Get PeriodStart() As Date
   Let PeriodStart = mPeriodStart
End Property

'##ModelId=3E6DA24B0083
Public Property Let PeriodStart(ByVal Value As Date)
    Let mPeriodStart = Value
End Property

'##ModelId=3E6DA24B003D
Public Property Get PaidDate() As Date
   Let PaidDate = mPaidDate
End Property

'##ModelId=3E6DA24A03AC
Public Property Let PaidDate(ByVal Value As Date)
    Let mPaidDate = Value
End Property

'##ModelId=3E6DA24A035C
Public Property Get PaymentDueDate() As Date
   Let PaymentDueDate = mPaymentDueDate
End Property

'##ModelId=3E6DA24A02E4
Public Property Let PaymentDueDate(ByVal Value As Date)
    Let mPaymentDueDate = Value
End Property

'##ModelId=3E6DA24A02A8
Public Property Get PostingDate() As Date
   Let PostingDate = mPostingDate
End Property

'##ModelId=3E6DA24A0230
Public Property Let PostingDate(ByVal Value As Date)
    Let mPostingDate = Value
End Property

'##ModelId=3E6DA24A01F4
Public Property Get DeliveryDate() As Date
   Let DeliveryDate = mDeliveryDate
End Property

'##ModelId=3E6DA24A0186
Public Property Let DeliveryDate(ByVal Value As Date)
    Let mDeliveryDate = Value
End Property

'##ModelId=3E6DA24A0140
Public Property Get TransactionDate() As Date
   Let TransactionDate = mTransactionDate
End Property

'##ModelId=3E6DA24A00DB
Public Property Let TransactionDate(ByVal Value As Date)
    Let mTransactionDate = Value
End Property

'##ModelId=3E6DA24A009F
Public Property Get CustomerNumber() As String
   Let CustomerNumber = mCustomerNumber
End Property

'##ModelId=3E6DA24A003B
Public Property Let CustomerNumber(ByVal Value As String)
    Let mCustomerNumber = Value
End Property

'##ModelId=3E6DA24903E7
Public Property Get StoreNumber() As String
   Let StoreNumber = mStoreNumber
End Property

'##ModelId=3E6DA249038D
Public Property Let StoreNumber(ByVal Value As String)
    Let mStoreNumber = Value
End Property

'##ModelId=3E6DA2490351
Public Property Get EntryID() As Long
   Let EntryID = mEntryID
End Property


'##ModelId=3E6DA24902E3
Public Property Let EntryID(ByVal Value As Long)
    Let mEntryID = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_SALESLEDGER * &H10000) + 1 To FID_SALESLEDGER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cSalesLedger

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_SALESLEDGER, FID_SALESLEDGER_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    Select Case (lFieldID)
        Case (FID_SALESLEDGER_EntryID):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mEntryID
        Case (FID_SALESLEDGER_StoreNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mStoreNumber
        Case (FID_SALESLEDGER_CustomerNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerNumber
        Case (FID_SALESLEDGER_TransactionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTransactionDate
        Case (FID_SALESLEDGER_DeliveryDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDeliveryDate
        Case (FID_SALESLEDGER_PostingDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mPostingDate
        Case (FID_SALESLEDGER_PaymentDueDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mPaymentDueDate
        Case (FID_SALESLEDGER_PaidDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mPaidDate
        Case (FID_SALESLEDGER_PeriodStart):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mPeriodStart
        Case (FID_SALESLEDGER_DocumentNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDocumentNumber
        Case (FID_SALESLEDGER_OrderNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrderNumber
        Case (FID_SALESLEDGER_TransactionType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionType
        Case (FID_SALESLEDGER_TotalValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalValue
        Case (FID_SALESLEDGER_TotalVAT):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mTotalVAT
        Case (FID_SALESLEDGER_OutstandingValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOutstandingValue
        Case (FID_SALESLEDGER_Disputed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDisputed
        Case (FID_SALESLEDGER_StatementPrinted):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mStatementPrinted
        Case (FID_SALESLEDGER_UserInitials):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mUserInitials
        Case (FID_SALESLEDGER_DateCommed):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateCommed
        Case (FID_SALESLEDGER_Memo1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMemo1
        Case (FID_SALESLEDGER_Memo2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mMemo2
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cSalesLedger
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_SALESLEDGER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Sales Ledger " & mEntryID

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_SALESLEDGER_EntryID):          mEntryID = oField.ValueAsVariant
            Case (FID_SALESLEDGER_StoreNumber):      mStoreNumber = oField.ValueAsVariant
            Case (FID_SALESLEDGER_CustomerNumber):   mCustomerNumber = oField.ValueAsVariant
            Case (FID_SALESLEDGER_TransactionDate):  mTransactionDate = oField.ValueAsVariant
            Case (FID_SALESLEDGER_DeliveryDate):     mDeliveryDate = oField.ValueAsVariant
            Case (FID_SALESLEDGER_PostingDate):      mPostingDate = oField.ValueAsVariant
            Case (FID_SALESLEDGER_PaymentDueDate):   mPaymentDueDate = oField.ValueAsVariant
            Case (FID_SALESLEDGER_PaidDate):         mPaidDate = oField.ValueAsVariant
            Case (FID_SALESLEDGER_PeriodStart):      mPeriodStart = oField.ValueAsVariant
            Case (FID_SALESLEDGER_DocumentNumber):   mDocumentNumber = oField.ValueAsVariant
            Case (FID_SALESLEDGER_OrderNumber):      mOrderNumber = oField.ValueAsVariant
            Case (FID_SALESLEDGER_TransactionType):  mTransactionType = oField.ValueAsVariant
            Case (FID_SALESLEDGER_TotalValue):       mTotalValue = oField.ValueAsVariant
            Case (FID_SALESLEDGER_TotalVAT):         mTotalVAT = oField.ValueAsVariant
            Case (FID_SALESLEDGER_OutstandingValue): mOutstandingValue = oField.ValueAsVariant
            Case (FID_SALESLEDGER_Disputed):         mDisputed = oField.ValueAsVariant
            Case (FID_SALESLEDGER_StatementPrinted): mStatementPrinted = oField.ValueAsVariant
            Case (FID_SALESLEDGER_UserInitials):     mUserInitials = oField.ValueAsVariant
            Case (FID_SALESLEDGER_DateCommed):       mDateCommed = oField.ValueAsVariant
            Case (FID_SALESLEDGER_Memo1):            mMemo1 = oField.ValueAsVariant
            Case (FID_SALESLEDGER_Memo2):            mMemo2 = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_SALESLEDGER_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cSalesLedger

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


