VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDeposit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D5CB5700046"
'<CAMH>****************************************************************************************
'* Module : cDeposit
'* Date   : 21/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/SalesBO/cDeposit.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 2/27/03 5:07p $
'* $Revision: 3 $
'* Versions:
'* 21/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cDeposit"

'##ModelId=3D5CBEC600D2
Private mDepositNo As String

'##ModelId=3D5CBED800E6
Private mCustomerName As String

'##ModelId=3D5CBEDF01A4
Private mDateCreated As Date

'##ModelId=3D5CBEFA02D0
Private mDepositAmount As Currency

'##ModelId=3D5CBF020276
Private mRefundAllowed As Boolean

'##ModelId=3D5CBF080230
Private mDepositCode As String

'##ModelId=3D5CBF1A0352
Private mRefunded As Boolean

'##ModelId=3D5CBF1E0172
Private mTillID As String

'##ModelId=3D5CBF230244
Private mTillTransactionNo As String

'##ModelId=3D5CBF290014
Private mCashierNo As String

'##ModelId=3D5CBF320320
Private mOrderNo As String

'##ModelId=3D5CBF36005A
Private mDepositUsedAmount As Currency

'##ModelId=3D5CBF4700E6
Private mTransactionMoveType As String

'##ModelId=3D5CBF520258
Private mTransactionMoveCode As String

'##ModelId=3D5CBF8C0258
Private mTraderNo As String

'##ModelId=3D5CBF95000A
Private mCommNo As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


'##ModelId=3D7738B5015E
Public Property Get CommNo() As String
   Let CommNo = mCommNo
End Property

'##ModelId=3D7738B50014
Public Property Let CommNo(ByVal Value As String)
    Let mCommNo = Value
End Property

'##ModelId=3D7738B40320
Public Property Get TraderNo() As String
   Let TraderNo = mTraderNo
End Property

'##ModelId=3D7738B40208
Public Property Let TraderNo(ByVal Value As String)
    Let mTraderNo = Value
End Property

'##ModelId=3D7738B40168
Public Property Get TransactionMoveCode() As String
   Let TransactionMoveCode = mTransactionMoveCode
End Property

'##ModelId=3D7738B4001E
Public Property Let TransactionMoveCode(ByVal Value As String)
    Let mTransactionMoveCode = Value
End Property

'##ModelId=3D7738B3035C
Public Property Get TransactionMoveType() As String
   Let TransactionMoveType = mTransactionMoveType
End Property

'##ModelId=3D7738B3024E
Public Property Let TransactionMoveType(ByVal Value As String)
    Let mTransactionMoveType = Value
End Property

'##ModelId=3D7738B301A4
Public Property Get DepositUsedAmount() As Currency
   Let DepositUsedAmount = mDepositUsedAmount
End Property

'##ModelId=3D7738B3005A
Public Property Let DepositUsedAmount(ByVal Value As Currency)
    Let mDepositUsedAmount = Value
End Property

'##ModelId=3D7738B20334
Public Property Get OrderNo() As String
   Let OrderNo = mOrderNo
End Property

'##ModelId=3D7738B2010E
Public Property Let OrderNo(ByVal Value As String)
    Let mOrderNo = Value
End Property

'##ModelId=3D7738B10186
Public Property Get CashierNo() As String
   Let CashierNo = mCashierNo
End Property

'##ModelId=3D7738B10078
Public Property Let CashierNo(ByVal Value As String)
    Let mCashierNo = Value
End Property

'##ModelId=3D7738B1000A
Public Property Get TillTransactionNo() As String
   Let TillTransactionNo = mTillTransactionNo
End Property

'##ModelId=3D7738B00316
Public Property Let TillTransactionNo(ByVal Value As String)
    Let mTillTransactionNo = Value
End Property

'##ModelId=3D7738B0026C
Public Property Get TillID() As String
   Let TillID = mTillID
End Property

'##ModelId=3D7738B00190
Public Property Let TillID(ByVal Value As String)
    Let mTillID = Value
End Property

'##ModelId=3D7738B00122
Public Property Get Refunded() As Boolean
   Let Refunded = mRefunded
End Property

'##ModelId=3D7738B00046
Public Property Let Refunded(ByVal Value As Boolean)
    Let mRefunded = Value
End Property

'##ModelId=3D7738AF03C0
Public Property Get DepositCode() As String
   Let DepositCode = mDepositCode
End Property

'##ModelId=3D7738AF02E4
Public Property Let DepositCode(ByVal Value As String)
    Let mDepositCode = Value
End Property

'##ModelId=3D7738AF0276
Public Property Get RefundAllowed() As Boolean
   Let RefundAllowed = mRefundAllowed
End Property

'##ModelId=3D7738AF019A
Public Property Let RefundAllowed(ByVal Value As Boolean)
    Let mRefundAllowed = Value
End Property

'##ModelId=3D7738AF012C
Public Property Get DepositAmount() As Currency
   Let DepositAmount = mDepositAmount
End Property

'##ModelId=3D7738AF008C
Public Property Let DepositAmount(ByVal Value As Currency)
    Let mDepositAmount = Value
End Property

'##ModelId=3D7738AF001E
Public Property Get DateCreated() As Date
   Let DateCreated = mDateCreated
End Property

'##ModelId=3D7738AE035C
Public Property Let DateCreated(ByVal Value As Date)
    Let mDateCreated = Value
End Property

'##ModelId=3D7738AE02EE
Public Property Get CustomerName() As String
   Let CustomerName = mCustomerName
End Property

'##ModelId=3D7738AE0280
Public Property Let CustomerName(ByVal Value As String)
    Let mCustomerName = Value
End Property

'##ModelId=3D7738AE0212
Public Property Get DepositNo() As String
   Let DepositNo = mDepositNo
End Property


'##ModelId=3D7738AE0172
Public Property Let DepositNo(ByVal Value As String)
    Let mDepositNo = Value
End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_DEPOSIT * &H10000) + 1 To FID_DEPOSIT_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub
Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
        If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_DEPOSIT

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property


Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Deposit " & mDepositNo

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me

End Function


Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cDeposit

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_DEPOSIT, FID_DEPOSIT_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_DEPOSIT_DepositNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepositNo
        Case (FID_DEPOSIT_CustomerName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerName
        Case (FID_DEPOSIT_DateCreated):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateCreated
        Case (FID_DEPOSIT_DepositAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mDepositAmount
        Case (FID_DEPOSIT_RefundAllowed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRefundAllowed
        Case (FID_DEPOSIT_DepositCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepositCode
        Case (FID_DEPOSIT_Refunded):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mRefunded
        Case (FID_DEPOSIT_TillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillID
        Case (FID_DEPOSIT_TillTransactionNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillTransactionNo
        Case (FID_DEPOSIT_CashierNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCashierNo
        Case (FID_DEPOSIT_OrderNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrderNo
        Case (FID_DEPOSIT_DepositUsedAmount):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mDepositUsedAmount
        Case (FID_DEPOSIT_TransactionMoveType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionMoveType
        Case (FID_DEPOSIT_TransactionMoveCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionMoveCode
        Case (FID_DEPOSIT_TraderNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTraderNo
        Case (FID_DEPOSIT_CommNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCommNo
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cDeposit
    Set m_oSession = oSession
    Set Initialise = Me
End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_DEPOSIT_DepositNo):           mDepositNo = oField.ValueAsVariant
            Case (FID_DEPOSIT_CustomerName):        mCustomerName = oField.ValueAsVariant
            Case (FID_DEPOSIT_DateCreated):         mDateCreated = oField.ValueAsVariant
            Case (FID_DEPOSIT_DepositAmount):       mDepositAmount = oField.ValueAsVariant
            Case (FID_DEPOSIT_RefundAllowed):       mRefundAllowed = oField.ValueAsVariant
            Case (FID_DEPOSIT_DepositCode):         mDepositCode = oField.ValueAsVariant
            Case (FID_DEPOSIT_Refunded):            mRefunded = oField.ValueAsVariant
            Case (FID_DEPOSIT_TillID):              mTillID = oField.ValueAsVariant
            Case (FID_DEPOSIT_TillTransactionNo):   mTillTransactionNo = oField.ValueAsVariant
            Case (FID_DEPOSIT_CashierNo):           mCashierNo = oField.ValueAsVariant
            Case (FID_DEPOSIT_OrderNo):             mOrderNo = oField.ValueAsVariant
            Case (FID_DEPOSIT_DepositUsedAmount):   mDepositUsedAmount = oField.ValueAsVariant
            Case (FID_DEPOSIT_TransactionMoveType): mTransactionMoveType = oField.ValueAsVariant
            Case (FID_DEPOSIT_TransactionMoveCode): mTransactionMoveCode = oField.ValueAsVariant
            Case (FID_DEPOSIT_TraderNo):            mTraderNo = oField.ValueAsVariant
            Case (FID_DEPOSIT_CommNo):              mCommNo = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cDeposit

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_DEPOSIT_END_OF_STATIC

End Function

