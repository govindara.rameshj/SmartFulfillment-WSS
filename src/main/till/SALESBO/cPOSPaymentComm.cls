VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPOSPaymentComm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module: cPOSPaymentComm
'* Date  : 30/07/10
'* Author: mauricem
'*$Archive: /Projects/OasysHB/VB-LIVE_CODE/SalesBO/cPosPaymentComm.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the functionality of the additional payment fields from Commidea PED
'**********************************************************************************************
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cPOSPaymentComm"

Implements IBo
Implements ISysBo

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


Private mTransactionDate      As Date
Private mTillID               As String
Private mTransactionNumber    As String
Private mSequenceNumber       As Long
'Private mTenderType           As String
Private mTokenID              As String
'Private mCreditCardNumber     As String
Private mCardNumberHash       As String

Private mCreditCardStartDate  As String
Private mCreditCardExpiryDate As String
Private mIssuerNumber         As String
Private mAuthorisationCode    As String
Private mCCNumberKeyed        As Boolean
Private mChequeAccountNumber  As String
Private mChequeSortCode       As String
Private mChequeNumber         As String
Private mSupervisorCode       As String
Private mEFTPOSVoucherNo      As String
Private mEFTPOSCommsDone      As Boolean
Private mEFTPOSMerchantNo     As String
Private mDigitCount           As Long
Private mCouponNumber         As String
Private mCouponPostCode       As String
Private mCashBalanceProcessed As Boolean
Private mAuthorisationType    As String
Private mCashBackAmount       As Currency
Private mCreditCardName       As String
Private mCouponClass          As String
Private mEFTConfirmID         As String
Private mEFTConfirmed         As String
Private mAuthorisationMethod  As String
Private mCardSecurityCode     As String
Private mCurrencyCode         As String
Private mExchangeRate         As Double
Private mExchangeFactor       As Long
Private mForeignTender        As Currency
Private mTerminalID           As String
Private mPANSeq               As String
Private mCardAppID            As String

Public POSHeader As cPOSHeader

Const NOT_LOADED As Long = -1
Const LOADED As Long = 0
Const EDITED As Long = 1

Public Property Get TransactionDate() As Date
    TransactionDate = mTransactionDate
End Property

Public Property Let TransactionDate(ByVal Value As Date)
    mTransactionDate = Value
End Property

Public Property Get TillID() As String
    TillID = mTillID
End Property

Public Property Let TillID(ByVal Value As String)
    mTillID = Value
End Property

Public Property Get TransactionNumber() As String
    TransactionNumber = mTransactionNumber
End Property

Public Property Let TransactionNumber(ByVal Value As String)
    mTransactionNumber = Value
End Property

Public Property Get SequenceNumber() As Long
    SequenceNumber = mSequenceNumber
End Property

Public Property Let SequenceNumber(ByVal Value As Long)
    mSequenceNumber = Value
End Property

Public Property Get TokenID() As String
    TokenID = mTokenID
End Property

Public Property Let TokenID(ByVal Value As String)
    mTokenID = Value
End Property

Public Property Get CardNumberHash() As String
    CardNumberHash = mCardNumberHash
End Property

Public Property Let CardNumberHash(ByVal Value As String)
    mCardNumberHash = Value
End Property

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_POSPAYMENTCOMM
End Property


Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME
End Property

Private Property Get IBo_DebugString() As String

    IBo_DebugString = "POSPaymentComm " & mTransactionNumber
End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set m_oSession = oSession
    Set IBo_Initialise = Me
End Function
'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 08/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean
    Dim lFieldNo As Integer
    Dim oField   As IField

    Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_POSPAYMENTCOMM_TransactionDate):      mTransactionDate = oField.ValueAsVariant
            Case (FID_POSPAYMENTCOMM_TillID):               mTillID = oField.ValueAsVariant
            Case (FID_POSPAYMENTCOMM_TransactionNumber):    mTransactionNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENTCOMM_SequenceNumber):       mSequenceNumber = oField.ValueAsVariant
            Case (FID_POSPAYMENTCOMM_TokenID):              mTokenID = oField.ValueAsVariant
            Case (FID_POSPAYMENTCOMM_CardNumberHash):       mCardNumberHash = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean

    Call LoadFromRow(oRow)
End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision
End Property

Private Function Load() As Boolean
    ' Load up all properties from the database
    Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then
        Set moRowSel = New CRowSelector
    End If
    Call moRowSel.Merge(GetSelectAllRow)
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If
End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)
End Function

Public Function IBo_SaveIfNew() As Boolean
    
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean

    SaveIfNew = Save(SaveTypeIfNew)
End Function

Public Function IBo_SaveIfExists() As Boolean
    
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)
End Function

Public Function IBo_Delete() As Boolean
    
    IBo_Delete = False
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)
End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()
End Function

'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
    ' Save all properties to the database
    Dim oCardInfo As cCardInfo
    
    Save = False
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If
End Function


Private Function IsValid() As Boolean

'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
    ' Returns a CRow object containing one field object for each property.
    Dim oRow    As IRow
    Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_POSPAYMENTCOMM * &H10000) + 1 To FID_POSPAYMENTCOMM_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow
End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    Dim oField As IField
    
    Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If Not oField Is Nothing Then
        oField.ValueAsVariant = Value
        'Add field to Row selector
        If moRowSel Is Nothing Then
            Set moRowSel = New CRowSelector
        End If
        Call moRowSel.AddSelection(Comparator, oField)
        AddLoadFilter = True
    End If
End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

    Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then
        Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    End If
    Call moLoadRow.AddSelectAll(FieldID)
End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing
End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing
End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    ' Load up all properties from the database
    Dim oView       As IView
    Dim colBO       As Collection
    Dim oBO         As ISysBo
    Dim lBONo       As Long
    Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then
        Set oLoadFields = New CRowSelector
    End If
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    Set LoadMatches = colBO
End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPOSPaymentComm
End Function

Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)
End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow
End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load
End Function

Private Function GetSelectAllRow() As IRowSelector
    ' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_POSPAYMENTCOMM, FID_POSPAYMENTCOMM_END_OF_STATIC, m_oSession)
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If

On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_POSPAYMENTCOMM_TransactionDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTransactionDate
        Case (FID_POSPAYMENTCOMM_TillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillID
        Case (FID_POSPAYMENTCOMM_TransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNumber
        Case (FID_POSPAYMENTCOMM_SequenceNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSequenceNumber
        Case (FID_POSPAYMENTCOMM_TokenID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTokenID
        Case (FID_POSPAYMENTCOMM_CardNumberHash):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCardNumberHash
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then
        GetField.Id = lFieldID
    End If
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next
End Function

Private Function Initialise(oSession As ISession) As cPOSPaymentComm
    
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As cPOSPaymentComm
    Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()
End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_POSPAYMENTCOMM_END_OF_STATIC
End Function

Friend Function GetFileLine() As String
    Dim POSPaymentCommImplementationFactory As New cPOSPaymentCommFactory
    Dim LongerTokenIDImplementation As IPOSPaymentCommTokenIDLen
    Dim TokenIDLength As Integer
    Dim strTemp As String

    TokenIDLength = 20
    If Not POSPaymentCommImplementationFactory Is Nothing Then
        Call POSPaymentCommImplementationFactory.Initialise(m_oSession)
        Set LongerTokenIDImplementation = POSPaymentCommImplementationFactory.TokenIDLengthFactory
        If Not LongerTokenIDImplementation Is Nothing Then
            TokenIDLength = LongerTokenIDImplementation.GetTokenIDLength
        End If
    End If
    
    strTemp = ""
    strTemp = strTemp & FmtVal(mTransactionDate, enftDate)
    strTemp = strTemp & FmtVal(mTillID, enftCode, 2)
    strTemp = strTemp & FmtVal(mTransactionNumber, enftCode, 4)
    strTemp = strTemp & FmtVal(mSequenceNumber, enftCode, 5)
    strTemp = strTemp & FmtVal(mTokenID, enftString, TokenIDLength)
    strTemp = strTemp & FmtVal(mCardNumberHash, enftString, 28)
    GetFileLine = strTemp
End Function
