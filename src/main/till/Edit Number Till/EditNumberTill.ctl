VERSION 5.00
Begin VB.UserControl ucTillNumberText 
   ClientHeight    =   285
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2595
   ScaleHeight     =   285
   ScaleWidth      =   2595
   ToolboxBitmap   =   "EditNumberTill.ctx":0000
   Begin VB.TextBox txtEditNumber 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Text            =   "0.00"
      Top             =   0
      Width           =   2415
   End
End
Attribute VB_Name = "ucTillNumberText"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Property Variables:
Dim mstrNumberFormat  As String
Dim mlngDecimalPlaces As Long
Dim mblnMinValue      As Double
Dim mblnMaxValue      As Double
Dim mblnAllowInvert   As Boolean
Dim mdblValue         As Double
Dim mblnNegative      As Boolean
Dim mblnFixedDecPoint As Boolean

Const MODULE_NAME As String = "ucTillNumberText"

'Event Declarations:
Event Change() 'MappingInfo=txtEditNumber,txtEditNumber,-1,Change
Event Click() 'MappingInfo=txtEditNumber,txtEditNumber,-1,Click
Event DblClick() 'MappingInfo=txtEditNumber,txtEditNumber,-1,DblClick
Event KeyDown(KeyCode As Integer, Shift As Integer) 'MappingInfo=txtEditNumber,txtEditNumber,-1,KeyDown
Event KeyPress(KeyAscii As Integer) 'MappingInfo=txtEditNumber,txtEditNumber,-1,KeyPress
Event KeyUp(KeyCode As Integer, Shift As Integer) 'MappingInfo=txtEditNumber,txtEditNumber,-1,KeyUp
Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=txtEditNumber,txtEditNumber,-1,MouseDown
Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=txtEditNumber,txtEditNumber,-1,MouseMove
Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single) 'MappingInfo=txtEditNumber,txtEditNumber,-1,MouseUp


'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditNumber,txtEditNumber,-1,BackColor
Public Property Get BackColor() As OLE_COLOR
Attribute BackColor.VB_Description = "Returns/sets the background color used to display text and graphics in an object."
    BackColor = txtEditNumber.BackColor
End Property

Public Property Let BackColor(ByVal New_BackColor As OLE_COLOR)
    txtEditNumber.BackColor() = New_BackColor
    PropertyChanged "BackColor"
End Property

Public Property Get FixedDecPoint() As Boolean
    FixedDecPoint = mblnFixedDecPoint
End Property

Public Property Let FixedDecPoint(ByVal Value As Boolean)
    mblnFixedDecPoint = Value
    PropertyChanged "FixedDecimalPoint"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditNumber,txtEditNumber,-1,ForeColor
Public Property Get ForeColor() As OLE_COLOR
Attribute ForeColor.VB_Description = "Returns/sets the foreground color used to display text and graphics in an object."
    ForeColor = txtEditNumber.ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As OLE_COLOR)
    txtEditNumber.ForeColor() = New_ForeColor
    PropertyChanged "ForeColor"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditNumber,txtEditNumber,-1,Enabled
Public Property Get Enabled() As Boolean
Attribute Enabled.VB_Description = "Returns/sets a value that determines whether an object can respond to user-generated events."
    Enabled = txtEditNumber.Enabled
End Property

Public Property Let Enabled(ByVal New_Enabled As Boolean)
    txtEditNumber.Enabled() = New_Enabled
    UserControl.Enabled = New_Enabled
    PropertyChanged "Enabled"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditNumber,txtEditNumber,-1,Font
Public Property Get Font() As Font
Attribute Font.VB_Description = "Returns a Font object."
Attribute Font.VB_UserMemId = -512
    Set Font = txtEditNumber.Font
End Property

Public Property Set Font(ByVal New_Font As Font)
    Set txtEditNumber.Font = New_Font
    PropertyChanged "Font"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=UserControl,UserControl,-1,Refresh
Public Sub Refresh()
Attribute Refresh.VB_Description = "Forces a complete repaint of a object."
    UserControl.Refresh
End Sub

Private Sub txtEditNumber_Change()
    RaiseEvent Change
End Sub

Private Sub txtEditNumber_Click()
    RaiseEvent Click
End Sub

Private Sub txtEditNumber_DblClick()
    RaiseEvent DblClick
End Sub

Private Sub txtEditNumber_GotFocus()

    'put cursor at end of number
    txtEditNumber.SelStart = Len(txtEditNumber.Text)

End Sub

Private Sub txtEditNumber_KeyDown(KeyCode As Integer, Shift As Integer)

Dim lngEditPos As Long
    
    If (txtEditNumber.Locked = True) Then
        Exit Sub
    End If
    
    RaiseEvent KeyDown(KeyCode, Shift)
    If (KeyCode = 46) And (mlngDecimalPlaces > 0) Then
        'ensure not deleting decimal point
        If InStr(txtEditNumber.Text, ".") - 1 = txtEditNumber.SelStart Then
            KeyCode = 0
            Exit Sub
        End If
        'if after decimal place then change delete to reset value to 0
        If InStr(txtEditNumber.Text, ".") - 1 < txtEditNumber.SelStart Then
            lngEditPos = txtEditNumber.SelStart
            txtEditNumber.Text = Left$(txtEditNumber.Text, txtEditNumber.SelStart) & Mid$(txtEditNumber.Text, txtEditNumber.SelStart + 2) & "0"
            txtEditNumber.SelStart = lngEditPos
            KeyCode = 0
            Exit Sub
        End If
    End If
End Sub

Private Sub txtEditNumber_KeyPress(KeyAscii As Integer)

Dim strLeft    As String
Dim strRight   As String
Dim strValue   As String
Dim lngEditPos As Long
Dim lngDecPos  As Long

    If KeyAscii <> 13 And KeyAscii <> 46 And KeyAscii <> 45 And KeyAscii <> 43 And KeyAscii <> 9 And KeyAscii <> 27 And KeyAscii <> 8 And (KeyAscii < 48 Or KeyAscii > 57) Then
        KeyAscii = 0
    ElseIf (KeyAscii <> 13) And (txtEditNumber.Locked = True) Then
        KeyAscii = 0
    Else
        If KeyAscii = 45 Then 'switch to Negative value
            If (mblnNegative = False) And (mblnAllowInvert = True) Then
                mblnNegative = True
                lngEditPos = txtEditNumber.SelStart
                Call DisplayValue
                txtEditNumber.SelStart = lngEditPos + 1
            End If
            KeyAscii = 0
            Exit Sub
        End If
        If KeyAscii = 43 Then 'switch to Positive value
            mblnNegative = False
            lngEditPos = txtEditNumber.SelStart
            Call DisplayValue
            If lngEditPos = 0 Then lngEditPos = 1
            txtEditNumber.SelStart = lngEditPos - 1
            KeyAscii = 0
            Exit Sub
        End If
        
        If (KeyAscii = 46) Then 'if pressing dec point key then check if ignore or process
            If (mblnFixedDecPoint = True) Then txtEditNumber.SelStart = Len(txtEditNumber.Text) - mlngDecimalPlaces
            KeyAscii = 0
            Exit Sub
        End If
        'convert value to string - ready to manipulate
        strValue = Format(mdblValue, mstrNumberFormat)
        
        'If enter pressed, then just pass key event out and exit
        If (KeyAscii = vbKeyReturn) Then
            RaiseEvent KeyPress(KeyAscii)
            Exit Sub
        End If
        
        If (txtEditNumber.SelLength = Len(txtEditNumber.Text)) Then
            mblnNegative = False
            strValue = Format(0, mstrNumberFormat)
            txtEditNumber.Text = strValue
            txtEditNumber.SelStart = Len(txtEditNumber.Text)
        End If
        
        'Get current cursor position, offset if negative value
        lngEditPos = txtEditNumber.SelStart
        If mblnNegative Then lngEditPos = lngEditPos - 1

        'Get current decimal position
        lngDecPos = Len(strValue) - mlngDecimalPlaces
        
        'if back spacing, process where cursor is to avoid deleting point and sign
        If KeyAscii = 8 Then
            If (lngEditPos = 0) And (mblnNegative = True) Then 'when at start - redisplay to catch deleting Sign Code
                Call DisplayValue
                KeyAscii = 0
                Exit Sub
            End If
            'if decimal reached, skip over
            If (lngEditPos = lngDecPos) And (mlngDecimalPlaces > 0) Then
                Call DisplayValue
                If mblnNegative Then lngDecPos = lngDecPos + 1 'offset by 1 to cater for sign
                txtEditNumber.SelStart = lngDecPos - 1
                KeyAscii = 0
                Exit Sub
            End If
            'if back spacing on integer part of number, then remove char and redisplay
            If (lngEditPos > 0) And ((lngEditPos < lngDecPos) Or (mlngDecimalPlaces = 0)) Then
                strValue = Left$(strValue, lngEditPos - 1) & Mid$(strValue, lngEditPos + 1)
                mdblValue = Val(strValue)
                Call DisplayValue
                If mblnNegative Then lngEditPos = lngEditPos + 1 'offset by 1 to cater for sign
                txtEditNumber.SelStart = lngEditPos - 1
                KeyAscii = 0
                Exit Sub
            End If
            'if editing after decimal then delete char and repad
            If lngEditPos > lngDecPos Then
                lngEditPos = lngEditPos - 1
                lngDecPos = lngDecPos - 1
                strValue = Left$(strValue, InStr(strValue, ".") - 1) & Mid$(strValue, InStr(strValue, ".") + 1)
                strValue = Left$(strValue, lngEditPos - 1) & Mid$(strValue, lngEditPos + 1)
                
                strValue = Left$(strValue, lngDecPos - 1) & "." & Mid$(strValue, lngDecPos)
                If Left(strValue, 1) = "." Then lngEditPos = lngEditPos + 1
                mdblValue = Val(strValue)
                Call DisplayValue
                If mblnNegative Then lngEditPos = lngEditPos + 1 'offset by 1 to cater for sign
                txtEditNumber.SelStart = lngEditPos
                KeyAscii = 0
                Exit Sub
            End If
            'if no other event then display to catch delete giving a new value to display
            mdblValue = Abs(Val(txtEditNumber.Text))
            Call DisplayValue
            KeyAscii = 0
            Exit Sub
        End If
        
        'Process keys pressed in 0-9 range
        If KeyAscii >= 48 And KeyAscii <= 57 Then 'process keys 0-9
            If (mblnNegative = True) And ((lngEditPos = 0) Or (lngEditPos = 1)) Then
                If (InStr(strValue, ".") = 2) And (Mid(strValue, 1, 1) = "0") Then lngEditPos = lngEditPos + 1
            End If
            If (mblnNegative = False) And (lngEditPos = 0) Then
                If (InStr(strValue, ".") = 2) And (Mid(strValue, 1, 1) = "0") Then lngEditPos = lngEditPos + 1
            End If
            strValue = Left$(strValue, lngEditPos) & Chr(KeyAscii) & Mid$(strValue, lngEditPos + 1)
            'if past decimal then scroll Decimal up one space to right
            If (lngEditPos >= lngDecPos) And (mlngDecimalPlaces > 0) Then 'after decimal place - insert into position and scroll all left numbers up 1 position
                'If lngEditPos = Len(strValue) Then lngEditPos = lngEditPos - 1
                strValue = Left$(strValue, InStr(strValue, ".") - 1) & Mid$(strValue, InStr(strValue, ".") + 1)
                strValue = Left$(strValue, lngDecPos) & "." & Mid$(strValue, lngDecPos + 1)
            End If
            'Move to next number to over type
            lngEditPos = lngEditPos + 1
            'check new value does not exceed min and max values
            If Val(strValue) < mblnMinValue Then
                strValue = Format(mblnMinValue, mstrNumberFormat)
            End If
            If Val(strValue) > mblnMaxValue Then strValue = Format(mblnMaxValue, mstrNumberFormat)
            KeyAscii = 0
            
            'move edited value back to value variable and display in format
            mdblValue = Val(strValue)
            Call DisplayValue
            If mblnNegative Then lngEditPos = lngEditPos + 1 'offset by 1 to cater for sign
            txtEditNumber.SelStart = lngEditPos
        End If
        RaiseEvent KeyPress(KeyAscii) 'pass key back to container form
    End If 'valid key pressed
    
End Sub

Private Sub txtEditNumber_KeyUp(KeyCode As Integer, Shift As Integer)

Dim lngEditPos As Long

    If (txtEditNumber.Locked = True) Then
        Exit Sub
    End If
    
    RaiseEvent KeyUp(KeyCode, Shift)
    If KeyCode = 46 Then
        lngEditPos = txtEditNumber.SelStart
        mdblValue = Abs(Val(txtEditNumber.Text))
        Call DisplayValue
        txtEditNumber.SelStart = lngEditPos
    End If
End Sub

Private Sub txtEditNumber_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub txtEditNumber_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseMove(Button, Shift, X, Y)
End Sub

Private Sub txtEditNumber_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,dd/mm/yy
Public Property Get DecimalPlaces() As Long
Attribute DecimalPlaces.VB_Description = "Supplied date format used to drive control to enforce that only valid dates are captured"
    DecimalPlaces = mlngDecimalPlaces
End Property

Public Property Let DecimalPlaces(ByVal New_DecimalPlaces As Long)
    
    mlngDecimalPlaces = New_DecimalPlaces
    If mlngDecimalPlaces < 0 Then mlngDecimalPlaces = 0
    If mlngDecimalPlaces = 0 Then
        mstrNumberFormat = "0"
    Else
        mstrNumberFormat = Space$(mlngDecimalPlaces)
        mstrNumberFormat = "0." & Replace(mstrNumberFormat, " ", "0")
    End If
    
    Call DisplayValue
    PropertyChanged "DecimalPlaces"
    
End Property
Private Sub DisplayValue()
    
    txtEditNumber.Text = SignCode & Format(mdblValue, mstrNumberFormat)
    If (mblnNegative = True) And (txtEditNumber.SelStart = 0) Then txtEditNumber.SelStart = 1

End Sub

Public Property Get MinimumValue() As Double
    MinimumValue = mblnMinValue
End Property

Public Property Let MinimumValue(ByVal New_MinimumValue As Double)
    
    mblnMinValue = New_MinimumValue
    If mblnMinValue > mblnMaxValue Then
        mblnMaxValue = mblnMinValue
        PropertyChanged "MaximumValue"
    End If
    PropertyChanged "MinimumValue"
    
End Property
Private Function SignCode() As String

    If mblnNegative = True Then
        SignCode = "-"
    Else
        SignCode = vbNullString
    End If

End Function

Public Property Get MaximumValue() As Double
    MaximumValue = mblnMaxValue
End Property

Public Property Let MaximumValue(ByVal New_MaximumValue As Double)
    
    mblnMaxValue = New_MaximumValue
    If mblnMaxValue < mblnMinValue Then
        mblnMinValue = mblnMaxValue
        PropertyChanged "MinimumValue"
    End If
    PropertyChanged "MaximumValue"
    
End Property

Public Property Get AllowInvert() As Boolean
    AllowInvert = mblnAllowInvert
End Property

Public Property Let AllowInvert(ByVal New_AllowInvert As Boolean)
    
    mblnAllowInvert = New_AllowInvert
    If (mblnAllowInvert = False) And (Val(txtEditNumber.Text) <= 0) Then txtEditNumber.Text = Abs(txtEditNumber.Text)
    PropertyChanged "AllowInvert"
    
End Property

Private Sub UserControl_Initialize()
    
    mstrNumberFormat = "0.00" 'force local vars setting
    mdblValue = 0
    mlngDecimalPlaces = 2
    mblnMinValue = -99999999.99
    mblnMaxValue = 99999999.99
    mblnAllowInvert = True
    mblnFixedDecPoint = False

End Sub

'Initialize Properties for User Control
Private Sub UserControl_InitProperties()
End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    txtEditNumber.BackColor = PropBag.ReadProperty("BackColor", &H80000005)
    txtEditNumber.ForeColor = PropBag.ReadProperty("ForeColor", &H80000008)
    txtEditNumber.Enabled = PropBag.ReadProperty("Enabled", True)
    Set txtEditNumber.Font = PropBag.ReadProperty("Font", Ambient.Font)
    DecimalPlaces = PropBag.ReadProperty("DecimalPlaces", mlngDecimalPlaces) 'force local settings
    Value = PropBag.ReadProperty("Value", 0)
    txtEditNumber.Locked = PropBag.ReadProperty("Locked", False)
    mblnFixedDecPoint = PropBag.ReadProperty("FixedDecPoint", False)
    mblnMinValue = PropBag.ReadProperty("MinimumValue", -99999999.99)
    mblnMaxValue = PropBag.ReadProperty("MaximumValue", 99999999.99)
End Sub

Private Sub UserControl_Resize()

    txtEditNumber.Width = UserControl.Width - 10
    If UserControl.Height < 285 Then UserControl.Height = 285
    txtEditNumber.Height = UserControl.Height - 30

End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

    Call PropBag.WriteProperty("BackColor", txtEditNumber.BackColor, &H80000005)
    Call PropBag.WriteProperty("ForeColor", txtEditNumber.ForeColor, &H80000008)
    Call PropBag.WriteProperty("Enabled", txtEditNumber.Enabled, True)
    Call PropBag.WriteProperty("Font", txtEditNumber.Font, Ambient.Font)
    Call PropBag.WriteProperty("Value", Value, 0)
    Call PropBag.WriteProperty("Locked", txtEditNumber.Locked, False)
    Call PropBag.WriteProperty("MinimumValue", mblnMinValue, -99999999.99)
    Call PropBag.WriteProperty("MaximumValue", mblnMaxValue, 99999999.99)
    Call PropBag.WriteProperty("FixedDecPoint", mblnFixedDecPoint, False)
End Sub
'
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditNumber,txtEditNumber,-1,Text
Public Property Get Value() As Double
Attribute Value.VB_Description = "Returns/sets the text contained in the control."
    Value = Val(txtEditNumber.Text)
End Property

Public Sub SelectAllValue()

    txtEditNumber.SelStart = 0
    txtEditNumber.SelLength = Len(txtEditNumber.Text)

End Sub

Public Property Let Value(ByVal New_Value As Double)

    mdblValue = Abs(New_Value)
    If (New_Value < 0) And (mblnAllowInvert = True) Then
        mblnNegative = True
    Else
        mblnNegative = False
    End If
    Call DisplayValue
    PropertyChanged "Value"

End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=txtEditNumber,txtEditNumber,-1,Locked
Public Property Get Locked() As Boolean
Attribute Locked.VB_Description = "Determines whether a control can be edited."
    Locked = txtEditNumber.Locked
End Property

Public Property Let Locked(ByVal New_Locked As Boolean)
    txtEditNumber.Locked() = New_Locked
    PropertyChanged "Locked"
End Property

