VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TailoredData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
#Const ccDebug = 0
Implements ITailoredData
'
' $Archive: /Projects/OasysV2/VB/OasysClientGeneric/TailoredData.cls $
' $Revision: 10 $
' $Date: 12/02/02 11:05a $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
' This class simply provides all the tailored data, controlled by CTS.
' This data effectively provides ALL the system configuration that
' controls the added-value and chargeable options.  This allows us to
' provide all customers with all the same system software but they can
' only use what they have paid for.
'
' Various classes can simply get their data bundled into a user data type.
' Clearly we must try to ensure that the user types don't change and the
' simplest way is to check the size.  If there is a size mismatch we raise
' an error.
'
' This implementation provides tailored data for a 'generic' customer that
' has no hard-coded tailoring.  Any tailoring will either be generic or
' come from the registry.
'----------------------------------------------------------------------------
' Following constants copied from OasysBaseCom.Globals
' but derived from OasysStartCom.ITailoredData

Private Const TAILOR_CONTEXT_NOTHING = 0
Private Const TAILOR_CONTEXT_CLIENT = 1
Private Const TAILOR_CONTEXT_ENTERPRISE = 2

Private Const TAILOR_REC_NOTHING = 0
Private Const TAILOR_CLIENT_REC_BASE = 1
Private Const TAILOR_ENTERPRISE_REC_BASE = 1

' Following constants copied from OasysBaseCom.Globals
Private Const OASYS_ERR_NULL_OBJECT_REFERENCE = vbObjectError + 3
Private Const OASYS_ERR_INVALID_ENTERPRISE_ID = vbObjectError + 5
Private Const OASYS_ERR_INCONSISTENT_OASYS_SETUP = vbObjectError + 6

Private m_oRoot As IOasysRoot     ' Useful object reference
'----------------------------------------------------------------------------
' Methods
'----------------------------------------------------------------------------

Private Sub Class_Terminate()
    Set m_oRoot = Nothing
End Sub

Public Function ITailoredData_Initialise(Root As IOasysRoot) As Boolean
    ITailoredData_Initialise = False
    Set m_oRoot = Root
    If m_oRoot Is Nothing Then
        ' Integrity check, we don't need be too specific for such a gross problem
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "ITailoredData_ClientData() ", "Root reference unset."
    Else
        ITailoredData_Initialise = True
    End If
End Function

Public Property Get ITailoredData_RecordCount(nContextType As Integer, _
            Optional nInstance As Integer) As Integer
    Select Case nContextType
    Case Is = TAILOR_CONTEXT_CLIENT
        ITailoredData_RecordCount = 1
    Case Is = TAILOR_CONTEXT_ENTERPRISE
        ITailoredData_RecordCount = EnterpriseCount
    Case Else
        ITailoredData_RecordCount = 0
    End Select
End Property

Public Property Get ITailoredData_Record(nContextType As Integer, _
                Optional nInstance As Integer, _
                Optional nRecNumber As Integer) _
                As tTailoringDataRec
    Dim Record As tTailoringDataRec
    Select Case nContextType
    Case Is = TAILOR_CONTEXT_CLIENT
        Record.nContextType = TAILOR_CONTEXT_CLIENT
        Record.nRecType = TAILOR_CLIENT_REC_BASE
        Record.nFormatVersion = 1
        Record.varBuffer = ClientData()
    Case Is = TAILOR_CONTEXT_ENTERPRISE
        Record.nContextType = TAILOR_CONTEXT_ENTERPRISE
        Record.nRecType = TAILOR_ENTERPRISE_REC_BASE
        Record.nFormatVersion = 1
        Record.varBuffer = EnterpriseData(nInstance)
    Case Else
        Record.nContextType = nContextType
        Record.nRecType = TAILOR_REC_NOTHING
        Record.nFormatVersion = 0
    End Select
    ITailoredData_Record = Record
End Property

Private Property Get ClientData() As Variant
    Dim d As tClientBaseRecV1
    Dim udtSubContext As tTailoringSubContext
    
    udtSubContext.nContextType = TAILOR_CONTEXT_ENTERPRISE
    udtSubContext.nInstanceCount = EnterpriseCount
    ReDim d.arrtSubContext(1)
    d.arrtSubContext(1) = udtSubContext
    d.nDataVersion = 1
    
    d.strShortName = "Anycompany Stores"
    d.strFullName = "Anycompany Trading Limited"
    d.strMnemonic = "Generic"
    d.strStoreNo = "000"
    d.strWorkstationID = "--"
    If m_oRoot Is Nothing Then
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "ClientData() ", "Root reference unset."
    Else
        Dim oRegistry As OASYSCBASECOMLib.Registry
        Dim nResult As Long
        Dim strKey, strValue As String
        strKey = m_oRoot.Client.RelativeRegistryKey
        Set oRegistry = CreateObject("OasysCBaseCom.Registry")
        oRegistry.GetCTSStringValue strKey, "Mnemonic", strValue, nResult
        If nResult = 0 Then
            If UCase$(d.strMnemonic) <> UCase(strValue) Then
                Err.Raise OASYS_ERR_INCONSISTENT_OASYS_SETUP, "ClientData() ", "Generic tailored data with Mnemonic=" & strValue
            End If
        End If
        oRegistry.GetCTSStringValue strKey, "ShortName", strValue, nResult
        If nResult = 0 Then
            d.strShortName = strValue
        End If
        oRegistry.GetCTSStringValue strKey, "FullName", strValue, nResult
        If nResult = 0 Then
            d.strFullName = strValue
        End If
        oRegistry.GetCTSStringValue strKey, "StoreNumber", strValue, nResult
        If nResult = 0 Then
            d.strStoreNo = strValue
        End If
        oRegistry.GetCTSStringValue strKey, "WorkstationID", strValue, nResult
        If nResult = 0 Then
            d.strWorkstationID = strValue
        End If
        Set oRegistry = Nothing
    End If
    
    ClientData = d
End Property

Private Property Get EnterpriseCount() As Integer
    EnterpriseCount = 2
End Property

Private Property Get EnterpriseData(nId As Integer) As Variant
    If nId = 0 Or nId > EnterpriseCount Then
        Err.Raise OASYS_ERR_INVALID_ENTERPRISE_ID, "EnterpriseData() ", "Invalid Enterprise Id=" & nId & ", max=" & EnterpriseCount
    Else
        Select Case nId
        Case Is = 1
            EnterpriseData = EnterpriseData1
        Case Is = 2
            EnterpriseData = EnterpriseData2
        Case Else
            Err.Raise OASYS_ERR_INVALID_ENTERPRISE_ID, "EnterpriseData() ", "Enterprise Id=" & nId & ", Has missing tailored data."
        End Select
    End If

End Property

Private Property Get EnterpriseData1() As Variant
    Dim d As tEnterpriseBaseRecV1
    ' First Enterprise data is derived from the Client data.
    Dim udtClientData As tClientBaseRecV1
    udtClientData = ClientData
    d.strShortName = ClientData.strShortName
    d.strFullName = ClientData.strFullName
    EnterpriseData1 = d
End Property

Private Property Get EnterpriseData2() As Variant
    Dim d As tEnterpriseBaseRecV1
    d.strShortName = "Enterprise #2"
    d.strFullName = "The Full Enterprise 2 name"
    EnterpriseData2 = d
End Property
