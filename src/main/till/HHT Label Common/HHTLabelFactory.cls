VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "HHTLabelFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const m_ReasonCodeDroppedParameterID As Long = 980949
Private m_InitialisedUseNewHHTLabelImplementation As Boolean
Private m_UseNewHHTLabelImplementation As Boolean
Private m_Session As Session

Public Sub Initialise(ByRef oSession As Session)

    Set m_Session = oSession
    GetReasonCodeDroppedParameterValue
End Sub

Public Function GetImplementation() As IHHTLabel
    
    If UseNewHHTLabelImplementation Then
        'using implementation with 949 fix - i.e. treat as if reason code is no longer in the FKEY field
        Set GetImplementation = New HHTLabelNew
    Else
        'using live implementation
        Set GetImplementation = New HHTLabelLive
    End If
End Function

Friend Property Get UseNewHHTLabelImplementation() As Boolean
    
    UseNewHHTLabelImplementation = m_UseNewHHTLabelImplementation
End Property

Friend Sub GetReasonCodeDroppedParameterValue()

    If Not m_InitialisedUseNewHHTLabelImplementation And Not m_Session Is Nothing Then
On Error Resume Next
        m_UseNewHHTLabelImplementation = m_Session.GetParameter(m_ReasonCodeDroppedParameterID)
        m_InitialisedUseNewHHTLabelImplementation = True
On Error GoTo 0
    End If
End Sub
