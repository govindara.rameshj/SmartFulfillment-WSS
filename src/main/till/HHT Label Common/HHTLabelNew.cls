VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "HHTLabelNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IHHTLabel

Private Function IHHTLabel_GetSkuFromKeyData(ByVal KeyData As String) As String

    If Len(KeyData & "") > 12 Then
        IHHTLabel_GetSkuFromKeyData = Mid$(KeyData, 7, 6)
    Else
        IHHTLabel_GetSkuFromKeyData = ""
    End If
End Function

Private Property Get IHHTLabel_LabelRequestSummaryRecordFKeyIdentifier() As String

    IHHTLabel_LabelRequestSummaryRecordFKeyIdentifier = "PCPLRQ00000000"
End Property

Private Function IHHTLabel_PopulateSummaryRecordAndExtractDataFieldFromTMPFILRecordSet(ByVal PrintLabelRequestSummaryTempFil As ADODB.Recordset, ByRef SessionConnection As Connection, ByRef PrintLabelRequestSummaryTempFilToPopulate As cTempFil, ByRef ExtractedDataFieldValue As String) As Boolean
    Dim FieldNo As Long

    If Not PrintLabelRequestSummaryTempFil Is Nothing And Not PrintLabelRequestSummaryTempFilToPopulate Is Nothing Then
        With PrintLabelRequestSummaryTempFil
            Call .Open("select * from TMPFIL where FKEY = '" & IHHTLabel_LabelRequestSummaryRecordFKeyIdentifier & "'", SessionConnection)
            If Not .EOF = True Then
                .MoveFirst
                For FieldNo = 0 To .Fields.Count - 1 Step 1
                    With .Fields.Item(FieldNo)
                        If Not IsNull(.Value) Then
                            Select Case .Name
                                Case "TKEY"
                                    PrintLabelRequestSummaryTempFilToPopulate.ID = .Value
                                Case "WSID"
                                    PrintLabelRequestSummaryTempFilToPopulate.WorkStationID = .Value
                                Case "FKEY"
                                    PrintLabelRequestSummaryTempFilToPopulate.KeyData = .Value
                                Case "DATE1"
                                    PrintLabelRequestSummaryTempFilToPopulate.KeyDate = .Value
                                Case "DATA"
                                    PrintLabelRequestSummaryTempFilToPopulate.Value = .Value
                                Case "DAT1"
                                    PrintLabelRequestSummaryTempFilToPopulate.Data1 = .Value
                                Case "DAT2"
                                    PrintLabelRequestSummaryTempFilToPopulate.Data2 = .Value
                                Case "DAT3"
                                    PrintLabelRequestSummaryTempFilToPopulate.Data3 = .Value
                                Case "DAT4"
                                    PrintLabelRequestSummaryTempFilToPopulate.Data4 = .Value
                            End Select
                        End If
                    End With
                Next FieldNo
                ExtractedDataFieldValue = PrintLabelRequestSummaryTempFilToPopulate.Value
                IHHTLabel_PopulateSummaryRecordAndExtractDataFieldFromTMPFILRecordSet = True
            End If
        End With
    End If
End Function
