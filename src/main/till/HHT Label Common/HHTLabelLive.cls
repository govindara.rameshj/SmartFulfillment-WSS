VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "HHTLabelLive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IHHTLabel

Private Function IHHTLabel_GetSkuFromKeyData(ByVal KeyData As String) As String

    IHHTLabel_GetSkuFromKeyData = Mid$(KeyData, 9, 6)
End Function

Private Property Get IHHTLabel_LabelRequestSummaryRecordFKeyIdentifier() As String

    IHHTLabel_LabelRequestSummaryRecordFKeyIdentifier = "PCPLRQ00"
End Property

Private Function IHHTLabel_PopulateSummaryRecordAndExtractDataFieldFromTMPFILRecordSet(ByVal PrintLabelRequestSummaryTempFil As ADODB.Recordset, ByRef SessionConnection As Connection, ByRef PrintLabelRequestSummaryTempFilToPopulate As cTempFil, ByRef ExtractedDataFieldValue As String) As Boolean

    If Not PrintLabelRequestSummaryTempFil Is Nothing Then
        With PrintLabelRequestSummaryTempFil
            Call .Open("select DATA from TMPFIL where FKEY = '" & IHHTLabel_LabelRequestSummaryRecordFKeyIdentifier & "'", SessionConnection)
            If Not .EOF = True Then
                .MoveFirst
                ExtractedDataFieldValue = .Fields.Item(0).Value
                IHHTLabel_PopulateSummaryRecordAndExtractDataFieldFromTMPFILRecordSet = True
            End If
        End With
    End If
End Function
