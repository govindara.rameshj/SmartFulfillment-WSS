VERSION 5.00
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{37AE774D-63A5-11D2-B108-E19FE47C377C}#1.0#0"; "VBUSPL~1.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmMenuEditor 
   Caption         =   "OASYS V2 - Menu Editor"
   ClientHeight    =   6105
   ClientLeft      =   90
   ClientTop       =   1515
   ClientWidth     =   11865
   Icon            =   "frmMenuEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6105
   ScaleWidth      =   11865
   Begin LpLib.fpCombo cmbIcons 
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   1575
      _Version        =   196608
      _ExtentX        =   2778
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   0   'False
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Text            =   ""
      Columns         =   3
      Sorted          =   0
      SelDrawFocusRect=   -1  'True
      ColumnSeparatorChar=   9
      ColumnSearch    =   -1
      ColumnWidthScale=   2
      RowHeight       =   -1
      WrapList        =   0   'False
      WrapWidth       =   0
      AutoSearch      =   1
      SearchMethod    =   0
      VirtualMode     =   0   'False
      VRowCount       =   0
      DataSync        =   3
      ThreeDInsideStyle=   1
      ThreeDInsideHighlightColor=   -2147483633
      ThreeDInsideShadowColor=   -2147483627
      ThreeDInsideWidth=   1
      ThreeDOutsideStyle=   1
      ThreeDOutsideHighlightColor=   -2147483628
      ThreeDOutsideShadowColor=   -2147483632
      ThreeDOutsideWidth=   1
      ThreeDFrameWidth=   0
      BorderStyle     =   0
      BorderColor     =   -2147483642
      BorderWidth     =   1
      ThreeDOnFocusInvert=   0   'False
      ThreeDFrameColor=   -2147483633
      Appearance      =   2
      BorderDropShadow=   0
      BorderDropShadowColor=   -2147483632
      BorderDropShadowWidth=   3
      ScrollHScale    =   2
      ScrollHInc      =   0
      ColsFrozen      =   0
      ScrollBarV      =   1
      NoIntegralHeight=   0   'False
      HighestPrecedence=   0
      AllowColResize  =   0
      AllowColDragDrop=   0
      ReadOnly        =   0   'False
      VScrollSpecial  =   0   'False
      VScrollSpecialType=   0
      EnableKeyEvents =   -1  'True
      EnableTopChangeEvent=   -1  'True
      DataAutoHeadings=   -1  'True
      DataAutoSizeCols=   2
      SearchIgnoreCase=   -1  'True
      ScrollBarH      =   1
      DataFieldList   =   ""
      ColumnEdit      =   1
      ColumnBound     =   -1
      Style           =   2
      MaxDrop         =   8
      ListWidth       =   -1
      EditHeight      =   -1
      GrayAreaColor   =   -2147483633
      ListLeftOffset  =   0
      ComboGap        =   -2
      MaxEditLen      =   150
      VirtualPageSize =   0
      VirtualPagesAhead=   0
      ExtendCol       =   0
      ColumnLevels    =   1
      ListGrayAreaColor=   -2147483637
      GroupHeaderHeight=   -1
      GroupHeaderShow =   0   'False
      AllowGrpResize  =   0
      AllowGrpDragDrop=   0
      MergeAdjustView =   0   'False
      ColumnHeaderShow=   -1  'True
      ColumnHeaderHeight=   -1
      GrpsFrozen      =   0
      BorderGrayAreaColor=   -2147483637
      ExtendRow       =   0
      ListPosition    =   0
      ButtonThreeDAppearance=   0
      OLEDragMode     =   0
      OLEDropMode     =   0
      Redraw          =   -1  'True
      AutoSearchFill  =   0   'False
      AutoSearchFillDelay=   500
      EditMarginLeft  =   1
      EditMarginTop   =   1
      EditMarginRight =   0
      EditMarginBottom=   3
      ResizeRowToFont =   0   'False
      TextTipMultiLine=   0
      AutoMenu        =   -1  'True
      EditAlignH      =   0
      EditAlignV      =   0
      ColDesigner     =   "frmMenuEditor.frx":058A
   End
   Begin VBUSplitterControl2.vbuSplitter2 vspSplit 
      CausesValidation=   0   'False
      Height          =   5535
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   9763
      Style           =   0
      MinHorizontalSize=   600
      SplitterTop     =   1110
      SplitterLeft    =   2170
      SplitterOrientation=   1
      Begin VB.PictureBox frGroups 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   5535
         Left            =   0
         ScaleHeight     =   5505
         ScaleWidth      =   2115
         TabIndex        =   6
         Top             =   0
         Width           =   2140
         Begin VB.CommandButton cmdNewGroup 
            Appearance      =   0  'Flat
            Caption         =   "Ne&w Group"
            Height          =   375
            Left            =   1080
            TabIndex        =   8
            Top             =   5100
            Width           =   975
         End
         Begin VB.CommandButton cmdDelGroup 
            Appearance      =   0  'Flat
            Caption         =   "De&l Group"
            Height          =   375
            Left            =   60
            TabIndex        =   7
            Top             =   5100
            Width           =   975
         End
         Begin MSComctlLib.TreeView tvwGroups 
            Height          =   5055
            Left            =   0
            TabIndex        =   9
            Top             =   0
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   8916
            _Version        =   393217
            HideSelection   =   0   'False
            LabelEdit       =   1
            LineStyle       =   1
            Style           =   6
            FullRowSelect   =   -1  'True
            SingleSel       =   -1  'True
            Appearance      =   1
         End
      End
      Begin VB.PictureBox frOptions 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   5535
         Left            =   2230
         ScaleHeight     =   5505
         ScaleWidth      =   9390
         TabIndex        =   3
         Top             =   0
         Visible         =   0   'False
         Width           =   9425
         Begin VB.CommandButton cmdSaveMenu 
            Appearance      =   0  'Flat
            Caption         =   "&Save Updates"
            Height          =   375
            Left            =   7680
            TabIndex        =   4
            Top             =   60
            Visible         =   0   'False
            Width           =   1335
         End
         Begin FPSpreadADO.fpSpread sprdGroup 
            Height          =   1095
            Left            =   0
            TabIndex        =   5
            Top             =   0
            Width           =   6495
            _Version        =   458752
            _ExtentX        =   11456
            _ExtentY        =   1931
            _StockProps     =   64
            BackColorStyle  =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GridColor       =   0
            MaxCols         =   7
            MaxRows         =   1
            RowHeaderDisplay=   0
            ScrollBars      =   0
            SelectBlockOptions=   2
            SpreadDesigner  =   "frmMenuEditor.frx":0926
         End
         Begin TabDlg.SSTab tabMenus 
            Height          =   4335
            Left            =   0
            TabIndex        =   11
            Top             =   1200
            Width           =   9375
            _ExtentX        =   16536
            _ExtentY        =   7646
            _Version        =   393216
            Tab             =   2
            TabHeight       =   520
            TabCaption(0)   =   "&1. Menu Options"
            TabPicture(0)   =   "frmMenuEditor.frx":0DD2
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "frMenuOptions"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "&2. Reports"
            TabPicture(1)   =   "frmMenuEditor.frx":0DEE
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "frReports"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "&3. Maintenance"
            TabPicture(2)   =   "frmMenuEditor.frx":0E0A
            Tab(2).ControlEnabled=   -1  'True
            Tab(2).Control(0)=   "frMaintenance"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).ControlCount=   1
            Begin VB.PictureBox frMaintenance 
               Appearance      =   0  'Flat
               ForeColor       =   &H80000008&
               Height          =   3735
               Left            =   120
               ScaleHeight     =   3705
               ScaleWidth      =   9105
               TabIndex        =   21
               Top             =   480
               Width           =   9135
               Begin VB.CommandButton cmdNewMaint 
                  Appearance      =   0  'Flat
                  Caption         =   "&New Maint"
                  Height          =   375
                  Left            =   7980
                  TabIndex        =   23
                  Top             =   3300
                  Width           =   1095
               End
               Begin VB.CommandButton cmdDelMaint 
                  Appearance      =   0  'Flat
                  Caption         =   "&Del Maint"
                  Height          =   375
                  Left            =   6780
                  TabIndex        =   22
                  Top             =   3300
                  Width           =   1095
               End
               Begin FPSpreadADO.fpSpread sprdMaint 
                  Height          =   3255
                  Left            =   0
                  TabIndex        =   24
                  Top             =   0
                  Width           =   9135
                  _Version        =   458752
                  _ExtentX        =   16113
                  _ExtentY        =   5741
                  _StockProps     =   64
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MaxCols         =   13
                  MaxRows         =   1
                  RowHeaderDisplay=   0
                  SelectBlockOptions=   2
                  SpreadDesigner  =   "frmMenuEditor.frx":0E26
                  UserResize      =   1
               End
            End
            Begin VB.PictureBox frReports 
               Appearance      =   0  'Flat
               ForeColor       =   &H80000008&
               Height          =   3735
               Left            =   -74880
               ScaleHeight     =   3705
               ScaleWidth      =   9105
               TabIndex        =   17
               Top             =   480
               Width           =   9135
               Begin VB.CommandButton cmdNewReport 
                  Appearance      =   0  'Flat
                  Caption         =   "&New Report"
                  Height          =   375
                  Left            =   7980
                  TabIndex        =   19
                  Top             =   3300
                  Width           =   1095
               End
               Begin VB.CommandButton cmdDelRep 
                  Appearance      =   0  'Flat
                  Caption         =   "&Del Report"
                  Height          =   375
                  Left            =   6780
                  TabIndex        =   18
                  Top             =   3300
                  Width           =   1095
               End
               Begin FPSpreadADO.fpSpread sprdReport 
                  Height          =   3255
                  Left            =   0
                  TabIndex        =   20
                  Top             =   0
                  Width           =   9135
                  _Version        =   458752
                  _ExtentX        =   16113
                  _ExtentY        =   5741
                  _StockProps     =   64
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MaxCols         =   13
                  MaxRows         =   1
                  RowHeaderDisplay=   0
                  SelectBlockOptions=   2
                  SpreadDesigner  =   "frmMenuEditor.frx":232C
                  UserResize      =   1
               End
            End
            Begin VB.PictureBox frMenuOptions 
               Appearance      =   0  'Flat
               ForeColor       =   &H80000008&
               Height          =   3735
               Left            =   -74880
               ScaleHeight     =   3705
               ScaleWidth      =   9105
               TabIndex        =   13
               Top             =   480
               Width           =   9135
               Begin VB.CommandButton cmdNewMenu 
                  Appearance      =   0  'Flat
                  Caption         =   "&New Option"
                  Height          =   375
                  Left            =   7980
                  TabIndex        =   15
                  Top             =   3300
                  Width           =   1095
               End
               Begin VB.CommandButton cmdDelMenu 
                  Appearance      =   0  'Flat
                  Caption         =   "&Del Option"
                  Height          =   375
                  Left            =   6780
                  TabIndex        =   14
                  Top             =   3300
                  Width           =   1095
               End
               Begin FPSpreadADO.fpSpread sprdMenu 
                  Height          =   3255
                  Left            =   0
                  TabIndex        =   16
                  Top             =   0
                  Width           =   9135
                  _Version        =   458752
                  _ExtentX        =   16113
                  _ExtentY        =   5741
                  _StockProps     =   64
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  MaxCols         =   13
                  MaxRows         =   1
                  RowHeaderDisplay=   0
                  SelectBlockOptions=   2
                  SpreadDesigner  =   "frmMenuEditor.frx":2B01
                  UserResize      =   1
               End
            End
         End
         Begin VB.Label lblParentID 
            BackStyle       =   0  'Transparent
            Caption         =   "Label1"
            Height          =   255
            Left            =   6720
            TabIndex        =   10
            Top             =   840
            Width           =   1335
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   12
      Top             =   5730
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmMenuEditor.frx":32B9
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13097
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "19:17"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList imIcons 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   128
      ImageHeight     =   96
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   30
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":5485
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":E4DD
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":17535
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":2058D
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":295E1
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":32633
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":3B685
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":446D7
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":4D729
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":5677B
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":5F7CD
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":6881F
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":71871
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":7A8C3
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":7DD15
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":81167
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":8A1B9
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":9320B
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":9665D
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":99AAF
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":A2B01
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":ABB53
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":B4BA5
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":BD5F7
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":C6049
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":CEA9B
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":D74ED
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":DFF3F
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":E8991
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMenuEditor.frx":F13E3
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblGroupID 
      BackStyle       =   0  'Transparent
      Caption         =   "lblGroupID"
      Height          =   255
      Left            =   6840
      TabIndex        =   1
      Top             =   5280
      Width           =   1695
   End
End
Attribute VB_Name = "frmMenuEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmMenuEditor
'* Date   : 23/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Menu Editor/frmMenuEditor.frm $
'**********************************************************************************************
'* Summary: Provide a complete menu editor for the creation of Menu Groups with their Menu
'*          Options and Menu Sub Options.
'**********************************************************************************************
'* $Author: Richardc $
'* $Date: 16/01/04 9:48 $
'* $Revision: 9 $
'* Versions:
'* 23/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmMenuEditor"

Const ExistEntry = 0
Const NewEntry = 1
Const DelEntry = 2
Const EditEntry = 3

Const TAB_MENU As Long = 0
Const TAB_REPORT As Long = 1
Const TAB_MAINT As Long = 2

Const COL_OPTTEXT As Long = 1
Const COL_OPTICON As Long = 2
Const COL_OPTEXE As Long = 3
Const COL_OPTPRM As Long = 4
Const COL_OPTASP As Long = 5
Const COL_OPTSHORTCUT As Long = 6
Const COL_OPTWAIT As Long = 7
Const COL_OPTSECURITY As Long = 8
Const COL_OPTADDPATH As Long = 9
Const COL_OPTSESSION As Long = 10
Const COL_OPTICONID As Long = 11
Const COL_OPTID As Long = 12
Const COL_OPTEDIT As Long = 13

Const COL_GRPTEXT As Long = 1
Const COL_GRPICON As Long = 2
Const COL_GRPSHORTCUT As Long = 3
Const COL_GRPSECURITY As Long = 4
Const COL_GRPICONID As Long = 5
Const COL_GRPID As Long = 6
Const COL_GRPEDIT As Long = 7

Dim moMenu As cMenuItem

Dim blnRszMenu   As Boolean
Dim blnRszReport As Boolean
Dim blnRszMaint  As Boolean

Private Sub cmdDelMaint_Click()

    sprdMaint.Row = sprdMaint.SelBlockRow
    sprdMaint.Col = COL_OPTTEXT
    If MsgBox("Delete maintenance option - '" & sprdMaint.Text & "'", vbYesNo, "Confirm deletion") = vbNo Then Exit Sub
    sprdMaint.Col = COL_OPTEDIT
    If Val(sprdMaint.Text) <> NewEntry Then
        sprdMaint.Text = DelEntry
        sprdMaint.RowHidden = True
        cmdSaveMenu.Visible = True
        sprdMaint.Col = COL_OPTTEXT
    Else
        Call sprdMaint.DeleteRows(sprdMaint.Row, 1)
        sprdMaint.MaxRows = sprdMaint.MaxRows - 1
    End If
    

End Sub

Private Sub cmdDelRep_Click()

    sprdReport.Row = sprdReport.SelBlockRow
    sprdReport.Col = COL_OPTTEXT
    If MsgBox("Delete report option - '" & sprdReport.Text & "'", vbYesNo, "Confirm deletion") = vbNo Then Exit Sub
    sprdReport.Col = COL_OPTEDIT
    If Val(sprdReport.Text) <> NewEntry Then
        sprdReport.Text = DelEntry
        sprdReport.RowHidden = True
        cmdSaveMenu.Visible = True
        sprdReport.Col = COL_OPTTEXT
    Else
        Call sprdReport.DeleteRows(sprdReport.Row, 1)
        sprdReport.MaxRows = sprdReport.MaxRows - 1
    End If
    


End Sub

Private Sub cmdNewGroup_Click()

    'check to save or discard changes to existing Menu Group - if any
    If cmdSaveMenu.Visible = True Then
        If MsgBox("Discard changes to Menu Group/ Options", vbYesNo, "Create New Group") = vbNo Then
            Call cmdSaveMenu.SetFocus
            Exit Sub
        End If
    End If
    
    tvwGroups.Enabled = False
    sprdMenu.MaxRows = 0
    sprdGroup.SetFocus
    sprdGroup.Row = 1
    sprdGroup.Col = COL_GRPICON
    sprdGroup.TypePictPicture = imIcons.ListImages.Item(1).Picture
    sprdGroup.Col = COL_GRPICONID
    sprdGroup.Text = 1
    sprdGroup.Col = COL_GRPID
    sprdGroup.Text = 0 'reset MenuID/Group ID to 0 - not assigned yet
    sprdGroup.Col = COL_GRPEDIT
    sprdGroup.Text = NewEntry
    sprdGroup.Col = COL_GRPTEXT
    sprdGroup.Text = vbNullString
    Call sprdGroup.SetActiveCell(COL_GRPTEXT, 1)
    sprdGroup.EditMode = True

End Sub

Private Sub cmdNewMaint_Click()
    
    Call AddNewOption("Maintenance", sprdMaint)

End Sub

Private Sub cmdNewMenu_Click()

    Call AddNewOption("Menu", sprdMenu)

End Sub

Private Sub cmdNewReport_Click()
    
    Call AddNewOption("Report", sprdReport)

End Sub

Private Sub Form_Load()

Dim lIconNo As Long

    Set goRoot = GetRoot
    
    Call InitialiseStatusBar(sbStatus)
    
    'extract all available icons into combo for assignment to Menu Options
    For lIconNo = 1 To imIcons.ListImages.Count Step 1
        Call cmbIcons.AddItem(lIconNo)
        cmbIcons.Col = 1
        cmbIcons.Row = cmbIcons.NewIndex
        cmbIcons.ListApplyTo = ListApplyToIndividual
        cmbIcons.Picture = imIcons.ListImages.Item(lIconNo).Picture
        cmbIcons.RowHeight = Screen.TwipsPerPixelY * 48
        cmbIcons.Col = 2
        cmbIcons.Row = lIconNo
    Next lIconNo
    
    sprdGroup.RowHeight(1) = 40
    
    Call DisplayMenu(1, True)
    
    Me.Show
    
    Call tvwGroups.SetFocus
    
End Sub
Private Sub cmdDelMenu_Click()
  
    sprdMenu.Row = sprdMenu.SelBlockRow
    sprdMenu.Col = COL_OPTTEXT
    If MsgBox("Delete menu option - '" & sprdMenu.Text & "'", vbYesNo, "Confirm deletion") = vbNo Then Exit Sub
    sprdMenu.Col = COL_OPTEDIT
    If Val(sprdMenu.Text) <> NewEntry Then
        sprdMenu.Text = DelEntry
        sprdMenu.RowHidden = True
        cmdSaveMenu.Visible = True
        sprdMenu.Col = COL_OPTTEXT
    Else
        Call sprdMenu.DeleteRows(sprdMenu.Row, 1)
        sprdMenu.MaxRows = sprdMenu.MaxRows - 1
    End If

End Sub
Private Sub AddNewOption(ByVal sFunction As String, ByRef oSpread As fpSpread)
    
    'if last entry used then add new line to table
    oSpread.Row = oSpread.MaxRows
    oSpread.Col = COL_OPTTEXT
    If LenB(oSpread.Text) = 0 And oSpread.MaxRows > 0 Then
        oSpread.SetFocus
        Call MsgBox("Last " & sFunction & " Option does not have a name entered" & vbCrLf & "Use existing entries before adding new", vbExclamation, "Add New " & sFunction & " Option")
        Call oSpread.SetActiveCell(COL_OPTTEXT, oSpread.Row)
        oSpread.EditMode = True
        Exit Sub
    End If
    oSpread.MaxRows = oSpread.MaxRows + 1
    oSpread.RowHeight(oSpread.MaxRows) = 40
    'Set defaults for new line
    oSpread.Col = COL_OPTEDIT
    oSpread.Row = oSpread.MaxRows
    oSpread.Text = NewEntry 'set new entry flag
    oSpread.Col = COL_OPTICON
    oSpread.TypePictPicture = imIcons.ListImages.Item(1).Picture
    oSpread.Col = COL_OPTSHORTCUT
    oSpread.Text = 1
    oSpread.Col = COL_OPTSECURITY
    oSpread.Text = 1
    oSpread.Col = COL_OPTADDPATH
    oSpread.Text = 1
    'Start Auto edit of name column
    oSpread.Col = COL_OPTTEXT
    oSpread.SetFocus
    oSpread.Action = ActionActiveCell
    oSpread.EditMode = True

End Sub
Private Sub cmdSaveMenu_Click()

Dim lRowNo   As Integer
Dim sGrpName As String
Dim lMenuID  As Long
Dim TopNode  As Node
Dim lIconID  As Long
Dim oMenu    As cMenuItem

    Screen.MousePointer = 11
    Set TopNode = tvwGroups.SelectedItem
    
    'Save group Headers first before items for group
    sprdGroup.Row = 1
    sprdGroup.Col = COL_GRPEDIT
    If Val(sprdGroup.Text) <> 0 Then 'if entry has been edited then process
        sprdGroup.Col = COL_GRPEDIT
        If Val(sprdGroup.Text) = NewEntry Then 'if new entry then add into table
            Set oMenu = goDatabase.CreateBusinessObject(CLASSID_MENU)
            sprdGroup.Col = COL_GRPTEXT
            oMenu.MenuText = sprdGroup.Text
            sGrpName = sprdGroup.Text
            sprdGroup.Col = COL_GRPICONID
            oMenu.IconID = Val(sprdGroup.Text)
            sprdGroup.Col = COL_GRPSHORTCUT
            oMenu.ShortcutKey = sprdGroup.Text
            sprdGroup.Col = COL_GRPSECURITY
            oMenu.Security = sprdGroup.Text
            Call moMenu.AddSubMenu(oMenu)
            
            sprdGroup.Col = COL_GRPID
            sprdGroup.Text = oMenu.MenuID 'given ID
            tvwGroups.Enabled = True
            Call tvwGroups.Nodes.Add(TopNode, tvwChild, "ID" & oMenu.MenuID & "|N", sGrpName)
            Call tvwGroups.Nodes("ID" & oMenu.MenuID & "|N").EnsureVisible
            Set TopNode = tvwGroups.Nodes("ID" & oMenu.MenuID & "|N")
            Set moMenu = oMenu
        End If
        sprdGroup.Col = COL_GRPEDIT
        If Val(sprdGroup.Text) = EditEntry Then 'if edited entry
            sprdGroup.Col = COL_GRPTEXT
            If moMenu.MenuText <> sprdGroup.Text Then tvwGroups.Nodes("ID" & moMenu.MenuID & "|Y").Text = sprdGroup.Text
            moMenu.MenuText = sprdGroup.Text
            sprdGroup.Col = COL_GRPICONID
            moMenu.IconID = sprdGroup.Text
            sprdGroup.Col = COL_GRPSECURITY
            moMenu.Security = Val(sprdGroup.Text)
            Call moMenu.IBo_SaveIfExists
        End If
        sprdGroup.Col = COL_GRPEDIT
        If Val(sprdGroup.Text) = DelEntry Then 'if edited entry
            sprdGroup.Col = COL_GRPID 'move to unique contact key
        End If
    End If
    sprdGroup.Col = COL_GRPEDIT
    sprdGroup.Text = 0 'reset editing flag
    
    Call SaveMenu(sprdMenu, moMenu.MenuOptions)
    Call SaveMenu(sprdReport, moMenu.Reports)
    Call SaveMenu(sprdMaint, moMenu.Maintenance)
    
    'check if new Menu Group added then select
    If Not TopNode Is Nothing Then
        If TopNode <> tvwGroups.SelectedItem Then TopNode.Selected = True
    End If
    
    cmdSaveMenu.Visible = False
    Screen.MousePointer = 0

End Sub
Private Sub SaveMenu(ByRef sprdSave As fpSpread, colOptions As Collection)

Dim lRowNo As Long
Dim oMenu  As cMenuItem

    For lRowNo = 1 To sprdSave.MaxRows Step 1 'step through each line on table
        sprdSave.Row = lRowNo
        sprdSave.Col = COL_OPTEDIT
        If Val(sprdSave.Text) <> 0 Then 'if entry has been edited then process
            sprdSave.Col = COL_OPTEDIT
            If Val(sprdSave.Text) = NewEntry Then 'if new entry then add into table
                Set oMenu = goDatabase.CreateBusinessObject(CLASSID_MENU)
                sprdSave.Col = COL_OPTTEXT
                oMenu.MenuText = sprdSave.Text  'add Menu Group Name
                sprdSave.Col = COL_OPTICONID
                oMenu.IconID = Val(sprdSave.Text)
                sprdSave.Col = COL_OPTASP
                oMenu.ASPPath = sprdSave.Text   'add Web Option
                sprdSave.Col = COL_OPTEXE
                oMenu.ExecPath = sprdSave.Text  'add EXE Option
                sprdSave.Col = COL_OPTPRM
                oMenu.Parameters = sprdSave.Text
                sprdSave.Col = COL_OPTSHORTCUT
                oMenu.ShortcutKey = sprdSave.Text  'add EXE Option
                sprdSave.Col = COL_OPTSECURITY
                oMenu.Security = sprdSave.Text   'add Web Option
                sprdSave.Col = COL_OPTADDPATH
                oMenu.AddPath = sprdSave.Value
                sprdSave.Col = COL_OPTWAIT
                oMenu.WaitEXE = sprdSave.Value
                sprdSave.Col = COL_OPTSESSION
                oMenu.SendSession = sprdSave.Value
                'oMenu.parentID = lblParentID.Caption
                sprdSave.Col = COL_OPTID
                If sprdSave.Name = sprdMenu.Name Then sprdSave.Text = moMenu.AddMenuOption(oMenu)
                If sprdSave.Name = sprdReport.Name Then sprdSave.Text = moMenu.AddReportOption(oMenu)
                If sprdSave.Name = sprdMaint.Name Then sprdSave.Text = moMenu.AddMaintOption(oMenu)
            End If
            sprdSave.Col = COL_OPTEDIT
            If Val(sprdSave.Text) = EditEntry Then 'if edited entry
                sprdSave.Col = COL_OPTID
                Set oMenu = colOptions(sprdSave.Text)
                sprdSave.Col = COL_OPTTEXT
                oMenu.MenuText = sprdSave.Text
                sprdSave.Col = COL_OPTEXE
                oMenu.ExecPath = sprdSave.Text
                sprdSave.Col = COL_OPTPRM
                oMenu.Parameters = sprdSave.Text
                sprdSave.Col = COL_OPTASP
                oMenu.ASPPath = sprdSave.Text
                sprdSave.Col = COL_OPTSHORTCUT
                oMenu.ShortcutKey = sprdSave.Text
                sprdSave.Col = COL_OPTICONID
                oMenu.IconID = sprdSave.Text
                sprdSave.Col = COL_OPTSECURITY
                oMenu.Security = sprdSave.Text   'add Web Option
                sprdSave.Col = COL_OPTADDPATH
                oMenu.AddPath = sprdSave.Value
                sprdSave.Col = COL_OPTWAIT
                oMenu.WaitEXE = sprdSave.Value
                sprdSave.Col = COL_OPTSESSION
                oMenu.SendSession = sprdSave.Value
                Call oMenu.IBo_SaveIfExists
            End If
            sprdSave.Col = COL_OPTEDIT
            If Val(sprdSave.Text) = DelEntry Then 'if new entry then add into table
                sprdSave.Col = COL_OPTID
                Set oMenu = colOptions(sprdSave.Text)
                Call oMenu.IBo_Delete
                Call sprdSave.DeleteRows(sprdSave.Row, 1)
                sprdSave.MaxRows = sprdSave.MaxRows - 1
            End If
        End If
        sprdSave.Col = COL_OPTEDIT
        sprdSave.Text = 0 'reset editing flag
    Next lRowNo

End Sub
Private Sub DisplayMenu(ByVal lGroupID As Long, ByVal blnFillSubMenu As Boolean)

Dim lOptionNo As Long
Dim oOption   As cMenuItem

    'Create holding Menu Option and retrieve selected option with all sub menus
    Set moMenu = goDatabase.CreateBusinessObject(CLASSID_MENU)
    Call moMenu.Retrieve(lGroupID, True)
    
    'Display selected option for editing
    sprdGroup.Row = 1
    sprdGroup.Col = COL_GRPTEXT
    sprdGroup.Text = moMenu.MenuText
    sprdGroup.Col = COL_GRPICON
    sprdGroup.TypePictPicture = imIcons.ListImages.Item(Val(moMenu.IconID)).Picture
    sprdGroup.Col = COL_GRPSHORTCUT
    sprdGroup.Text = moMenu.ShortcutKey
    sprdGroup.Col = COL_GRPSECURITY
    sprdGroup.Text = moMenu.Security
    sprdGroup.Col = COL_GRPSHORTCUT
    sprdGroup.Text = moMenu.ShortcutKey
    sprdGroup.Col = COL_GRPICONID
    sprdGroup.Text = moMenu.IconID
    sprdGroup.Col = COL_GRPID
    sprdGroup.Text = moMenu.MenuID
    
    'Store Parent Menu ID to new entries can share the same parent
    lblGroupID.Caption = lGroupID
        
    'Display all menu options for selected entry
    Call FillInOptions(sprdMenu, moMenu.MenuOptions)
    Call FillInOptions(sprdReport, moMenu.Reports)
    Call FillInOptions(sprdMaint, moMenu.Maintenance)
    sprdMenu.Enabled = True
    
    'check if Sub Menu Groups must be appended to the list view - only required on first click
    If blnFillSubMenu = True Then
        If tvwGroups.Nodes.Count = 0 Then 'if no Root Node then add and expand
            Call tvwGroups.Nodes.Add(, , "ID" & moMenu.MenuID & "|Y", moMenu.MenuText)
            tvwGroups.Nodes("ID" & moMenu.MenuID & "|Y").Expanded = True
        End If
        
        'step through sub groups and append under selected node
        For lOptionNo = 1 To moMenu.SubMenus.Count Step 1
            Set oOption = moMenu.SubMenus(CLng(lOptionNo))
            Call tvwGroups.Nodes.Add("ID" & lGroupID & "|Y", tvwChild, "ID" & oOption.MenuID & "|X", oOption.MenuText)
        Next lOptionNo
    End If
    
End Sub
Private Sub FillInOptions(ByRef sprdOptions As fpSpread, colOptions As Collection)

Dim lOptionNo As Long
Dim oOption   As cMenuItem
    
    sprdOptions.MaxRows = 0
    sprdOptions.LeftCol = 1
    'Display all menu optioyns for selected entry
    For lOptionNo = 1 To colOptions.Count Step 1
        Set oOption = colOptions(CLng(lOptionNo))
        
        'Add a new row and set height to cater for Icon
        sprdOptions.MaxRows = sprdOptions.MaxRows + 1
        sprdOptions.Row = sprdOptions.MaxRows
        sprdOptions.RowHeight(sprdOptions.MaxRows) = 40
        
        'Display option in columns
        sprdOptions.Col = COL_OPTTEXT
        sprdOptions.Text = oOption.MenuText
        sprdOptions.Col = COL_OPTICON
        sprdOptions.TypePictPicture = imIcons.ListImages.Item(Val(oOption.IconID)).Picture
        sprdOptions.Col = COL_OPTEXE
        sprdOptions.Text = oOption.ExecPath
        sprdOptions.Col = COL_OPTPRM
        sprdOptions.Text = oOption.Parameters
        sprdOptions.Col = COL_OPTASP
        sprdOptions.Text = oOption.ASPPath
        sprdOptions.Col = COL_OPTSHORTCUT
        sprdOptions.Text = oOption.ShortcutKey
        sprdOptions.Col = COL_OPTSECURITY
        sprdOptions.Text = oOption.Security
        sprdOptions.Col = COL_OPTWAIT
        sprdOptions.Value = oOption.WaitEXE
        sprdOptions.Col = COL_OPTADDPATH
        sprdOptions.Value = oOption.AddPath
        sprdOptions.Col = COL_OPTSESSION
        sprdOptions.Value = oOption.SendSession
        sprdOptions.Col = COL_OPTICONID
        sprdOptions.Text = oOption.IconID
        sprdOptions.Col = COL_OPTID
        sprdOptions.Text = oOption.MenuID
        
        'flag menu option as exists, so all edits will be UPDATE based
        sprdOptions.Col = COL_OPTEDIT
        sprdOptions.Text = ExistEntry
    Next lOptionNo
    

End Sub
Private Sub Frame2_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub Form_Resize()

    If Me.WindowState = vbMinimized Then Exit Sub
    
    If Me.Height < 3750 Then
        Me.Height = 3750
        Exit Sub
    End If
    
    If Me.Width < 5000 Then
        Me.Width = 5000
        Exit Sub
    End If
    
    vspSplit.Width = Me.Width - 240 - vspSplit.Left
    vspSplit.Height = Me.Height - 480 - vspSplit.Top - sbStatus.Height

End Sub

Private Sub sprdGroup_Click(ByVal Col As Long, ByVal Row As Long)
    
    If Col = COL_GRPICON Then
        Call sprdGroup.SetActiveCell(Col, Row)
        sprdGroup.Col = Col
        sprdGroup.Row = Row
        sprdGroup.CellType = CellTypeComboBox
        sprdGroup.TypeComboBoxhWnd = cmbIcons.hwnd
        'cmbIcons.Visible = True
        cmbIcons.ListDown = True
    End If

End Sub

Private Sub sprdGroup_ComboCloseUp(ByVal Col As Long, ByVal Row As Long, ByVal SelChange As Integer)
    
    If Col = COL_GRPICON Then
        cmbIcons.Col = 2
        cmbIcons.Row = cmbIcons.ListIndex
        'Move to cell and update spread to selected picture
        sprdGroup.Col = Col
        sprdGroup.Row = Row
        sprdGroup.CellType = CellTypePicture
        sprdGroup.TypePictPicture = imIcons.ListImages(Val(cmbIcons.List(cmbIcons.ListIndex))).Picture
        sprdGroup.TypePictCenter = True
        'Get selected Icon ID and store is Spread
        sprdGroup.Col = COL_GRPICONID
        sprdGroup.Text = Val(cmbIcons.List(cmbIcons.ListIndex))
        'mark lines as updated if required
        Call sprdGroup_EditMode(Col, Row, 0, True)
    End If

End Sub

Private Sub sprdGroup_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    If Mode = 0 And ChangeMade = True Then
        sprdGroup.Col = COL_GRPEDIT
        sprdGroup.Row = Row
        If Val(sprdGroup.Text) <> NewEntry Then sprdGroup.Text = EditEntry
        cmdSaveMenu.Visible = True
        sprdGroup.Col = Col
    End If

End Sub

Private Sub sprdGroup_KeyPress(KeyAscii As Integer)

    If KeyAscii = 32 And sprdGroup.ActiveCol = COL_GRPICON Then
        Call sprdGroup.SetActiveCell(COL_GRPICON, sprdGroup.ActiveRow)
        sprdGroup.Row = sprdGroup.ActiveRow
        sprdGroup.Col = COL_GRPICON
        If sprdGroup.CellType = CellTypePicture Then
            Call sprdGroup_Click(COL_GRPICON, sprdGroup.ActiveRow)
            KeyAscii = 0
        End If
    End If

End Sub

Private Sub sprdMaint_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    If Mode = 0 And ChangeMade = True Then
        sprdMaint.Col = COL_OPTEDIT
        sprdMaint.Row = Row
        If Val(sprdMaint.Text) <> NewEntry Then sprdMaint.Text = EditEntry
        cmdSaveMenu.Visible = True
        sprdMaint.Col = Col
    End If

End Sub

Private Sub sprdMenu_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)

    sprdMenu.Col = COL_OPTEDIT
    sprdMenu.Row = Row
    If Val(sprdMenu.Text) <> NewEntry Then sprdMenu.Text = EditEntry
    cmdSaveMenu.Visible = True
    sprdMenu.Col = Col

End Sub

Private Sub sprdMenu_Click(ByVal Col As Long, ByVal Row As Long)
    
    If Col = COL_OPTICON Then
        sprdMenu.Col = Col
        sprdMenu.Row = Row
        sprdMenu.CellType = CellTypeComboBox
        sprdMenu.TypeComboBoxhWnd = cmbIcons.hwnd
        'cmbIcons.Visible = True
        cmbIcons.ListDown = True
    End If

End Sub

Private Sub sprdMenu_ComboCloseUp(ByVal Col As Long, ByVal Row As Long, ByVal SelChange As Integer)
    
    If Col = COL_OPTICON Then
        cmbIcons.Col = 2
        cmbIcons.Row = cmbIcons.ListIndex
        sprdMenu.Col = Col
        sprdMenu.Row = Row
        sprdMenu.CellType = CellTypePicture
        sprdMenu.TypePictPicture = imIcons.ListImages(Val(cmbIcons.List(cmbIcons.ListIndex))).Picture
        sprdMenu.TypePictCenter = True
        sprdMenu.Col = COL_OPTICONID
        sprdMenu.Text = Val(cmbIcons.List(cmbIcons.ListIndex))
        Call sprdMenu_EditMode(Col, Row, 0, True)
    End If

End Sub

Private Sub sprdMenu_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    If Mode = 0 And ChangeMade = True Then
        sprdMenu.Col = COL_OPTEDIT
        sprdMenu.Row = Row
        If Val(sprdMenu.Text) <> NewEntry Then sprdMenu.Text = EditEntry
        cmdSaveMenu.Visible = True
        sprdMenu.Col = Col
    End If

End Sub

Private Sub sprdMenu_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 32 And sprdMenu.ActiveCol = COL_OPTICON Then
        Call sprdMenu.SetActiveCell(COL_OPTICON, sprdMenu.ActiveRow)
        sprdMenu.Row = sprdMenu.ActiveRow
        sprdMenu.Col = COL_OPTICON
        If sprdMenu.CellType = CellTypePicture Then
            Call sprdMenu_Click(COL_OPTICON, sprdMenu.ActiveRow)
            KeyAscii = 0
        End If
    End If

End Sub

Private Sub sprdReport_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    If Mode = 0 And ChangeMade = True Then
        sprdReport.Col = COL_OPTEDIT
        sprdReport.Row = Row
        If Val(sprdReport.Text) <> NewEntry Then sprdReport.Text = EditEntry
        cmdSaveMenu.Visible = True
        sprdReport.Col = Col
    End If

End Sub

Private Sub tabMenus_Click(PreviousTab As Integer)

    Call ResizeTabs
    DoEvents
    If tabMenus.Tab = TAB_MENU Then
        frMenuOptions.Enabled = True
        sprdMenu.SetFocus
        frReports.Enabled = False
        frMaintenance.Enabled = False
    End If
    If tabMenus.Tab = TAB_REPORT Then
        frReports.Enabled = True
        Call sprdReport.SetFocus
        frMenuOptions.Enabled = False
        frMaintenance.Enabled = False
    End If
    If tabMenus.Tab = TAB_MAINT Then
        frMaintenance.Enabled = True
        sprdMaint.SetFocus
        frMenuOptions.Enabled = False
        frReports.Enabled = False
    End If

End Sub

Private Sub tvwGroups_NodeClick(ByVal Node As MSComctlLib.Node)

Dim lMenuID        As Long
Dim sKey           As String
Dim blnFillSubMenu As Boolean

    'Get key from Node.Key
    sKey = Node.Key
    sKey = Left$(sKey, InStr(sKey, "|") - 1)
    'Get Menu ID from Key in first position after ID i.e. "IDxxx|"
    lMenuID = Val(Mid$(sKey, 3))
    lblParentID.Caption = lMenuID
    
    frOptions.Visible = True
    'Check if Node has had sub menu filled in by checking last char on key
    If Right$(Node.Key, 1) = "X" Then
        blnFillSubMenu = True
    Else
        blnFillSubMenu = False
    End If
    
    'Set Key to expanded, so check is not performed
    Node.Key = "ID" & lMenuID & "|Y"
    
    'Display Menu options in Spread and retrieve any sub-menus
    Call DisplayMenu(lMenuID, blnFillSubMenu)
    
    Node.Expanded = True
    
    tabMenus.Tab = TAB_MENU
    cmdNewMenu.Enabled = True
    cmdDelMenu.Enabled = True
    cmdSaveMenu.Visible = False

End Sub

Private Sub vspSplit_Resize()

    frGroups.Width = vspSplit.SplitterLeft
    tvwGroups.Width = frGroups.Width
    
    blnRszMenu = True
    blnRszReport = True
    blnRszMaint = True
    
    If vspSplit.Width - vspSplit.SplitterLeft - vspSplit.SplitterWidth > 0 Then
        frOptions.Width = vspSplit.Width - vspSplit.SplitterLeft - vspSplit.SplitterWidth
        tabMenus.Width = frOptions.Width
    End If
        
    cmdDelGroup.Top = frGroups.Height - cmdDelGroup.Height
    cmdNewGroup.Top = cmdDelGroup.Top
    tvwGroups.Height = cmdDelGroup.Top - 120
    tabMenus.Height = frOptions.Height - tabMenus.Top
    
    Call ResizeTabs
    
End Sub
Private Sub ResizeTabs()
    
    If tabMenus.Tab = TAB_MENU Then
        blnRszMenu = False
        sprdMenu.LeftCol = 1
        frMenuOptions.Width = tabMenus.Width - (frMenuOptions.Left * 2)
        frMenuOptions.Height = tabMenus.Height - frMenuOptions.Top - 120
        sprdMenu.Width = frMenuOptions.Width
        cmdNewMenu.Left = sprdMenu.Width - cmdNewMenu.Width
        cmdDelMenu.Left = cmdNewMenu.Left - cmdDelMenu.Width - 120
        sprdMenu.Height = frMenuOptions.Height - cmdDelMenu.Height - 240
        cmdDelMenu.Top = frMenuOptions.Height - cmdDelMenu.Height - 120
        cmdNewMenu.Top = cmdDelMenu.Top
    End If
    If tabMenus.Tab = TAB_REPORT Then
        blnRszReport = False
        sprdReport.LeftCol = 1
        frReports.Width = tabMenus.Width - (frReports.Left * 2)
        frReports.Height = tabMenus.Height - frReports.Top - 120
        sprdReport.Width = frReports.Width
        cmdNewReport.Left = sprdReport.Width - cmdNewReport.Width
        cmdDelRep.Left = cmdNewReport.Left - cmdDelRep.Width - 120
        
        sprdReport.Height = frReports.Height - cmdDelRep.Height - 240
        cmdDelRep.Top = frReports.Height - cmdDelRep.Height - 120
        cmdNewReport.Top = cmdDelRep.Top
    End If
    If tabMenus.Tab = TAB_MAINT Then
        blnRszMaint = False
        sprdMaint.LeftCol = 1
        frMaintenance.Width = tabMenus.Width - (frMaintenance.Left * 2)
        frMaintenance.Height = tabMenus.Height - frMaintenance.Top - 120
        sprdMaint.Width = frMaintenance.Width
        cmdNewMaint.Left = sprdMaint.Width - cmdNewMaint.Width
        cmdDelMaint.Left = cmdNewMaint.Left - cmdDelMaint.Width - 120
        
        sprdMaint.Height = frMaintenance.Height - 240 - cmdDelMaint.Height
        cmdDelMaint.Top = frMaintenance.Height - cmdDelMaint.Height - 120
        cmdNewMaint.Top = cmdDelMaint.Top
    End If

End Sub
