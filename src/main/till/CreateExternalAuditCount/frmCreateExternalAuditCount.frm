VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmCreateAuditCount 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Create External Audit Count"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6045
   Icon            =   "frmCreateExternalAuditCount.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   6045
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   1845
      Width           =   6045
      _ExtentX        =   10663
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmCreateExternalAuditCount.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   3307
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "04-May-04"
            TextSave        =   "04-May-04"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "21:31"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   360
      TabIndex        =   1
      Top             =   1080
      Width           =   5235
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Create External Audit Count Progress"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   300
      TabIndex        =   0
      Top             =   360
      Width           =   5295
   End
End
Attribute VB_Name = "frmCreateAuditCount"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MODULE_NAME As String = "CreateAuditCount"

Private Sub Form_Load()

    Me.Show
    If Me.WindowState <> vbMaximized Then
        Me.Left = (Screen.Width - Me.Width) / 2
        Me.Top = (Screen.Height - Me.Height) / 2
    End If
    GetRoot
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_WSID).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbStatus.Panels(PANEL_WSID).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE).Text = Format$(Date, "DD-MMM-YY")
    Call BuildExternalAuditCount
    If Dir(App.Path & "\TASKCOMP") <> "" Then Kill (App.Path & "\TASKCOMP")

    End

End Sub


Private Function BuildExternalAuditCount(Optional CountDate As Date = #1/1/1900#)

Dim adoConn     As New ADODB.Connection
Dim rsSysDat    As New ADODB.Recordset
Dim rsParameter As New ADODB.Recordset
Dim rsHHTDetail As New ADODB.Recordset
Dim rsStock     As New ADODB.Recordset
Dim blnInTran   As Boolean

Dim strSKU As String

Dim numWeeks As Integer
Dim numItems As Integer
    
    Call UpdateProgess(10, "Connect to DB")
    adoConn.Open (goDatabase.ConnectionString)
    'if countdate is nothing then use system tomorrow date
    Call UpdateProgess(10, "Get Date")
    If CountDate = #1/1/1900# Then
        Call DebugMsg(MODULE_NAME, "Init", endlDebug, "SELECT TMDT FROM SYSDAT")
        Call rsSysDat.Open("SELECT TMDT FROM SYSDAT", adoConn)
        CountDate = rsSysDat.Fields("TMDT")
        Call rsSysDat.Close
    End If
    Call UpdateProgess(10, "Load Parameters")

    'get parameters to use
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Load Parameter 4490")
    Call rsParameter.Open("SELECT LONGVALUE FROM PARAMETERS WHERE PARAMETERID=4490", adoConn)
    numWeeks = rsParameter.Fields(0)
    rsParameter.Close
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Load Parameter 4491")
    Call rsParameter.Open("SELECT LONGVALUE FROM PARAMETERS WHERE PARAMETERID=4491", adoConn)
    numItems = rsParameter.Fields(0)
    rsParameter.Close

    'get hht details for past numWeeks that have had an entry made
    Call UpdateProgess(10, "Get HHT Details")
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "SELECT SKUN,PRIC,ONHA,MDNQ FROM STKMAS WHERE SKUN IN (SELECT SKUN FROM HHTDET WHERE DATE1 BETWEEN '" & Format(DateAdd("ww", numWeeks * -1, CountDate), "yyyy-mm-dd") & "' AND '" & Format(CountDate, "yyyy-mm-dd") & "' AND ICNT=1 GROUP BY SKUN) AND IRIS=0 AND ONHA>0")
    Call rsStock.Open("SELECT SKUN,PRIC,ONHA,MDNQ FROM STKMAS WHERE SKUN IN (SELECT SKUN FROM HHTDET WHERE DATE1 BETWEEN '" & Format(DateAdd("ww", numWeeks * -1, CountDate), "yyyy-mm-dd") & "' AND '" & Format(CountDate, "yyyy-mm-dd") & "' AND ICNT=1 GROUP BY SKUN) AND IRIS=0 AND ONHA>0", adoConn)
    If (rsStock.EOF = True) Then
        rsStock.Close
        Exit Function
    End If

    'get stock items for these details excluding single related items and with no stock on hand
    Call UpdateProgess(40, "GetStockItems")
    
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Build Valid SKUs")
    Dim ValidSKUs As New Collection
    While Not rsStock.EOF
        strSKU = "'" & rsStock.Fields(0) & "'," & rsStock.Fields(1) & "," & rsStock.Fields(2) & "," & rsStock.Fields(3)
        Call ValidSKUs.Add(strSKU, rsStock.Fields(0) & "")
        rsStock.MoveNext
    Wend
    rsStock.Close

    'get numItems random sample from stocks
    Call UpdateProgess(70, "GetSample")
    
    Dim rowSkip As Integer
    Dim rowIndexes As New Collection
    Dim lower As Long
    Dim upper As Long
    Dim RandomPos As Long
    
    rowSkip = ValidSKUs.Count / numItems
    If rowSkip = 0 Then rowSkip = 1
    lower = 1
    upper = rowSkip

    Dim stocksToAdd As New Collection
    Dim strAddSku As Variant
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Build Valid Sample of " & numItems & "/" & ValidSKUs.Count)
    Do While upper <= ValidSKUs.Count
        RandomPos = Random(lower, upper)
        If RandomPos > ValidSKUs.Count Then RandomPos = ValidSKUs.Count
        If RandomPos = 0 Then RandomPos = 1
        strAddSku = ValidSKUs(RandomPos)
        Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Adding Sample of " & strAddSku & "(" & lower & "/" & upper & ")")
        On Error Resume Next
        Call stocksToAdd.Add(strAddSku, strAddSku)
        Err.Clear
        On Error GoTo 0
        lower = lower + rowSkip
        upper = upper + rowSkip
    Loop

    Call UpdateProgess(70, "Get Related Items")
            
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Stocks to Add:" & stocksToAdd.Count)
    If stocksToAdd.Count = 0 Then Exit Function
            
    Call GetRelatedItems(stocksToAdd, adoConn)

    'start transaction
    adoConn.BeginTrans
    blnInTran = True

    'clear any skus from PicExternalAudit for this date
    Call UpdateProgess(90, "Saving")
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Delete History:" & CountDate)
    Call adoConn.Execute("DELETE FROM PicExternalAudit WHERE COUNTDATE='" & Format(CountDate, "yyyy-mm-dd") & "'")

    'add these stock items into PicExternalAudit
    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Saving Sample:" & stocksToAdd.Count)
    For Each strAddSku In stocksToAdd
        Call adoConn.Execute("INSERT INTO PicExternalAudit (CountDate, SKUNumber, Price, StockOnHand,StockMarkDown) VALUES ('" & Format(CountDate, "yyyy-mm-dd") & "'," & strAddSku & ")")
    Next

    Call DebugMsg(MODULE_NAME, "Init", endlDebug, "Stocks to Add:COMMIT")
    adoConn.CommitTrans
    blnInTran = False

    Exit Function
    
BuildErr:
    If (blnInTran = True) Then adoConn.RollbackTrans

End Function

Private Sub GetRelatedItems(ByRef stocksToAdd As Collection, ByRef adoConn As ADODB.Connection)

Dim strSKU As Variant
    
Dim relatedSkus As New Collection
Dim rsStock As New ADODB.Recordset
Dim strRelSKUs As String
    
    strRelSKUs = ""
    'Build list of Sample SKU's and get Related Items to check as well
    For Each strSKU In stocksToAdd
        strRelSKUs = strRelSKUs & Left(strSKU, 8) & ","
    Next
    strRelSKUs = Left$(strRelSKUs, Len(strRelSKUs) - 1)
    Call UpdateProgess(70, "Get Rel Items(Pack)")
    Call DebugMsg(MODULE_NAME, "GetRelatedItems", endlDebug, "Select STKMAS Related Items:" & "SELECT SKUN,PRIC,ONHA,MDNQ FROM STKMAS WHERE SKUN IN (SELECT SPOS FROM RELITM WHERE PPOS IN (" & strRelSKUs & ")) AND " & _
        "((IOBS=0 AND INON=0) OR (ONHA <> 0) OR (MDNQ<> 0) OR (WTFQ <> 0)) AND " & _
        "((ONHA>=0) OR ((IRIS=0) AND (ICAT=0)))")

    Call rsStock.Open("SELECT SKUN,PRIC,ONHA,MDNQ FROM STKMAS WHERE SKUN IN (SELECT SPOS FROM RELITM WHERE PPOS IN (" & strRelSKUs & ")) AND " & _
        "((IOBS=0 AND INON=0) OR (ONHA <> 0) OR (MDNQ<> 0) OR (WTFQ <> 0)) AND " & _
        "((ONHA>=0) OR ((IRIS=0) AND (ICAT=0)))", adoConn)
    
    Call DebugMsg(MODULE_NAME, "GetRelatedItems", endlDebug, "Pass out values")
    While Not rsStock.EOF
        strSKU = "'" & rsStock.Fields(0) & "'," & rsStock.Fields(1) & "," & rsStock.Fields(2) & "," & rsStock.Fields(3)
        On Error Resume Next
        Call stocksToAdd.Add(strSKU, strSKU)
        On Error GoTo 0
        Err.Clear
        rsStock.MoveNext
    Wend
    rsStock.Close
        
    'now get related bulk items
    Call UpdateProgess(70, "Get Rel Items(Single)")
    Call rsStock.Open("SELECT SKUN,PRIC,ONHA,MDNQ FROM STKMAS WHERE SKUN IN (SELECT PPOS FROM RELITM WHERE SPOS IN (" & strRelSKUs & ")) AND " & _
        "((IOBS=0 AND INON=0) OR (ONHA <> 0) OR (MDNQ<> 0) OR (WTFQ <> 0)) AND " & _
        "((ONHA>=0) OR ((IRIS=0) AND (ICAT=0)))", adoConn)
    
    Call DebugMsg(MODULE_NAME, "GetRelatedItems", endlDebug, "Pass out values")
    While Not rsStock.EOF
        strSKU = "'" & rsStock.Fields(0) & "'," & rsStock.Fields(1) & "," & rsStock.Fields(2) & "," & rsStock.Fields(3)
        On Error Resume Next
        Call stocksToAdd.Add(strSKU, strSKU)
        On Error GoTo 0
        Err.Clear
        rsStock.MoveNext
    Wend
    rsStock.Close
        
        
    Exit Sub

GetRelItemsError:

End Sub


Function Random(Lowerbound As Long, Upperbound As Long)
    Randomize
    Random = Int(Rnd * (Upperbound - Lowerbound)) + Lowerbound
End Function


Private Sub UpdateProgess(PercComplete As Double, strMessage As String)

    lblStatus.Caption = strMessage
    lblStatus.Refresh
    Call DebugMsg(MODULE_NAME, "Progress", endlDebug, PercComplete & ":" & strMessage)

End Sub

