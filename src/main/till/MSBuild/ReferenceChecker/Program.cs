﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TLI;
using System.Text.RegularExpressions;

namespace ReferenceChecker
{
	class Program
	{
		static void Main(string[] args)
		{
			string rootTillPath = @"..\..\";
			string compabilityPath = rootTillPath+@"COMPATIBILITY\";

			Dictionary<string, string> libGuidMap = CollectLibGuidMap(compabilityPath);

			bool result = CheckProjects(rootTillPath,libGuidMap);
			
			if (!result)
				Environment.Exit(1);
		}

		private static bool CheckProjects(string rootFolder, Dictionary<string, string> libGuidMap)
		{
			var vbpFiles = Directory.GetFiles(rootFolder, "*.vbp", SearchOption.AllDirectories);
			bool result = true;
			foreach (var vbpFile in vbpFiles)
			{
				string projectPath = Path.GetFullPath(vbpFile);
				bool projectResult = true;
				PrintProjectCheck(projectPath);
				string line;
				using (var reader = new StreamReader(vbpFile))
				{
					while ((line = reader.ReadLine()) != null)
					{
						projectResult &= CheckLine(line, libGuidMap);
						result &= projectResult;
					}
				}
				if (projectResult)
					PrintProjectSuccess(projectPath);
				else
					PrintProjectFail(projectPath);
			}
			return result;
		}

		private static Dictionary<String, String> CollectLibGuidMap(String libFolder)
		{
			Dictionary<string, string> libGuidMap = new Dictionary<string, string>();
			var libFiles = Directory.GetFiles(libFolder, "*.dll")
				.Union(Directory.GetFiles(libFolder, "*.ocx"));
			foreach (var libPath in libFiles)
			{
				var app = new TLIApplicationClass();
				var info = app.TypeLibInfoFromFile(libPath);
				var libGuid = info.GUID;
				var lib = Path.GetFileName(libPath);
				libGuidMap[lib] = libGuid;
			}
			return libGuidMap;
		}

		private static bool CheckLine(String line, Dictionary<string, string> libGuidMap)
		{
			if (LineIsReference(line))
			{
				string refGuid = GetGuid(line);
				string refLib = GetLib(line);
				string lib = libGuidMap.Keys.FirstOrDefault(x => x.Equals(refLib, StringComparison.CurrentCultureIgnoreCase));
				if (lib == null)
					return true;
				string guid = libGuidMap[lib];
				bool result = guid.Equals(refGuid, StringComparison.CurrentCultureIgnoreCase);
				if (!result) PrintGuidMismatch(lib, guid, refGuid);
				return result;
			}
			else
				return true;
		}

		#region printing

		private static void PrintProjectCheck(string project)
		{
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine("Chek references of project " + project);
		}

		private static void PrintProjectSuccess(string project)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("All references are OK in " + project);
			Console.ResetColor();
		}

		private static void PrintProjectFail(string project)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("Reference mismatch in " + project);
			Console.ResetColor();
		}

		private static void PrintGuidMismatch(string lib, string expectedGuid, string avtualGuid)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("  GUID mismatch of lib {0}: expected={1}, actual={2}", lib, expectedGuid, avtualGuid);
			Console.ResetColor();
		}
		#endregion

		private static bool LineIsReference(String line)
		{
			return line.StartsWith("Reference=");
		}

		private static string GetGuid(string line)
		{
			var guidRegex = new Regex(@"G(?<guid>(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}))#");
			var match = guidRegex.Match(line);
			string guid = match.Groups["guid"].Value;
			return guid;
		}

		private static string GetLib(string line)
		{
			var libRegex = new Regex(@"(\\|#)(?<lib>[a-zA-Z.0-9]*[.](dll)|(ocx))#");
			var match = libRegex.Match(line);
			string lib = match.Groups["lib"].Value;
			return lib;
		}
	}
}
