VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OutputCardDetailsTests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private myOutputCardDetails As New cOutputCardDetails

Public Sub GivenANonEFTCardWhenParseReceivedMessageIsCalledWithAValidMessageFromAn810PEDThenThePropertiesAreCorrectlyPopulated()
    Dim ValidMessage As String
    Dim Track2Data As String
    Dim Result As CardDetailsResult
    Dim Delimiter As String
    
    Track2Data = "9611008676311210"
    Delimiter = ","
    Result = CardDetailsResult.ecdrSuccess
    ValidMessage = CStr(Result) + Delimiter + Track2Data + Delimiter
    With myOutputCardDetails
        .Initialise False
        .ParseReceivedMessage ValidMessage

        Assert.AreEqual CardDetailsResult.ecdrSuccess, .Result, "Result has not been parsed correctly"
        Assert.AreEqual Track2Data, .CardData, "CardData has not been parsed correctly"
    End With
End Sub

Public Sub GivenANonEFTCardWhenParseReceivedMessageIsCalledWithAValidMessageFromAn820PEDThenThePropertiesAreCorrectlyPopulated()
    Dim ValidMessage As String
    Dim Track2Data As String
    Dim Result As CardDetailsResult
    Dim Delimiter As String
    Dim CardNumber As String
    
    CardNumber = "9611008676311210"
    Track2Data = ";" + CardNumber + "?" + Chr(6)
    Delimiter = ","
    Result = CardDetailsResult.ecdrSuccess
    ValidMessage = CStr(Result) + Delimiter + Track2Data + Delimiter
    With myOutputCardDetails
        .Initialise False
        .ParseReceivedMessage ValidMessage

        Assert.AreEqual CardDetailsResult.ecdrSuccess, .Result, "Result has not been parsed correctly"
        Assert.AreEqual CardNumber, .CardData, "CardData has not been parsed correctly"
    End With
End Sub

