VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TransactionEngineFactoryTests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub WhenGetTransactionEngineIsCalled_ThenItReturnsATransaction820StateEngine()
    Dim factory As New TransactionEngineFactory
    
    Dim TestTransactionEngine As WSSCommidea.ITransactionEngine
    Set TestTransactionEngine = factory.GetTransactionEngine(New CommideaConfiguration)
    
    Assert.IsTrue TypeOf TestTransactionEngine Is VB.Form, "TransactionEngineFactory.GetTransactionEngine is returning instance of type VB.Form"
    
    Dim TestTransactionEngineAsForm As VB.Form
    Set TestTransactionEngineAsForm = TestTransactionEngine
    Assert.AreEqual "frmTransaction", TestTransactionEngineAsForm.Name, "StateEngineFactory.GetTransactionEngine is returning instance of named 'frmTransaction'"
End Sub
