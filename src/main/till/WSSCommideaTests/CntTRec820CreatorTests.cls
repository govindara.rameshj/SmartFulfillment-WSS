VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CntTRec820CreatorTests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private myContinueTRecordCreator As IContinueTRecordCreator

Public Sub FixtureSetup()
    Set myContinueTRecordCreator = New cContinueTRecord820Creator
End Sub

Public Sub GivenAnAuthorisationCodeArgument_WhenGetVoiceReferralAuthorisedContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim AuthorisationCode As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    AuthorisationCode = "TestAuthCode"
    ExpectedIntegrationMessage = "CONTTXN,7,AUTHCODE=TestAuthCode" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetVoiceReferralAuthorisedContinueTransactionRecord(AuthorisationCode)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenABlankAuthorisationCodeArgument_WhenGetVoiceReferralAuthorisedContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim AuthorisationCode As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    AuthorisationCode = ""
    ExpectedIntegrationMessage = "CONTTXN,7,AUTHCODE=" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetVoiceReferralAuthorisedContinueTransactionRecord(AuthorisationCode)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetReprintContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,5," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetReprintContinueTransactionRecord()
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithContinueTxn_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,2," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraContinueTxn)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithConfirmSignature_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,3," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraConfirmSignature)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithRejectSignature_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,4," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraRejectSignature)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithVoiceReferralRejected_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,8," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraVoiceReferralRejected)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithCancelTransaction_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,12," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraCancelTransaction)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithConfirmAuthCode_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,20," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraConfirmAuthCode)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithRejectAuthCode_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,21," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraRejectAuthCode)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithCashbackNotRequired_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,30," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraCashbackNotRequired)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithAcceptLicenceKey_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,46," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraAcceptLicenceKey)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub WhenGetContinueTransactionRecordForContinueTRecordActionTypeIsCalledWithCancelLicenceKeyVerification_ThenCorrectContinueTRecordIsCreated()
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ExpectedIntegrationMessage = "CONTTXN,48," & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetContinueTransactionRecordForContinueTRecordActionType(etraCancelLicenceKeyVerification)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenAnAuthorisationCodeArgument_WhenGetGetChargeAuthCodeContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim AuthorisationCode As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    AuthorisationCode = "TestAuthCode"
    ExpectedIntegrationMessage = "CONTTXN,22,AUTHCODE=TestAuthCode" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetChargeAuthCodeContinueTransactionRecord(AuthorisationCode)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenABlankAuthorisationCodeArgument_WhenGetChargeAuthCodeContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim AuthorisationCode As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    AuthorisationCode = ""
    ExpectedIntegrationMessage = "CONTTXN,22,AUTHCODE=" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetChargeAuthCodeContinueTransactionRecord(AuthorisationCode)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenACashbackAmountArgument_WhenGetCashbackRequiredContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim CashbackAmount As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    CashbackAmount = "10.00"
    ExpectedIntegrationMessage = "CONTTXN,29,CASHBACK=10.00" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetCashbackRequiredContinueTransactionRecord(CashbackAmount)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenABlankCashbackAmountArgument_WhenGetCashbackRequiredContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim CashbackAmount As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    CashbackAmount = ""
    ExpectedIntegrationMessage = "CONTTXN,29,CASHBACK=" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetCashbackRequiredContinueTransactionRecord(CashbackAmount)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenAManagerPinArgument_WhenGetReplaceAccountContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim ManagerPin As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ManagerPin = "1111"
    ExpectedIntegrationMessage = "CONTTXN,34,MGRPIN=1111" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetReplaceAccountContinueTransactionRecord(ManagerPin)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenABlankManagerPinArgument_WhenGetReplaceAccountContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim ManagerPin As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    ManagerPin = ""
    ExpectedIntegrationMessage = "CONTTXN,34,MGRPIN=" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetReplaceAccountContinueTransactionRecord(ManagerPin)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenAManagerPinArgument_WhenGetSelectPaypointAccountContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim AccountId As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    AccountId = "2222"
    ExpectedIntegrationMessage = "CONTTXN,42,PAYPOINTACCID=2222" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetSelectPaypointAccountContinueTransactionRecord(AccountId)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenABlankManagerPinArgument_WhenGetSelectPaypointAccountContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim AccountId As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    AccountId = ""
    ExpectedIntegrationMessage = "CONTTXN,42,PAYPOINTACCID=" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetSelectPaypointAccountContinueTransactionRecord(AccountId)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenAManagerPinArgument_WhenGetSelectPaypointOptionContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim OptionId As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    OptionId = "2222"
    ExpectedIntegrationMessage = "CONTTXN,43,PAYPOINTOPTIONID=2222" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetSelectPaypointOptionContinueTransactionRecord(OptionId)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub

Public Sub GivenABlankManagerPinArgument_WhenGetSelectPaypointOptionContinueTransactionRecordIsCalled_ThenCorrectContinueTRecordIsCreated()
    Dim OptionId As String
    Dim ContinueTRecord As cContinueTransactionRecord
    Dim ExpectedIntegrationMessage As String
    
    OptionId = ""
    ExpectedIntegrationMessage = "CONTTXN,43,PAYPOINTOPTIONID=" & vbCrLf
    Set ContinueTRecord = myContinueTRecordCreator.GetSelectPaypointOptionContinueTransactionRecord(OptionId)
    
    Assert.AreEqual ExpectedIntegrationMessage, ContinueTRecord.ToIntegrationMessage
End Sub
