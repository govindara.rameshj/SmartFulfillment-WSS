VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EventsToFileRedirector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements SimplyVBComp.IEventListener

Dim myOutputFilePath As String
Dim doc As New MSXML2.DOMDocument

Dim rootElementsStack() As IXMLDOMElement  '10 should be enough
Dim rootElementsStackLevel As Integer

Private Sub PushRootElement(rootElement As IXMLDOMElement)
    rootElementsStackLevel = rootElementsStackLevel + 1
    Set rootElementsStack(rootElementsStackLevel) = rootElement
End Sub

Private Function PopRootElement()
    rootElementsStackLevel = rootElementsStackLevel - 1
    Set PopRootElement = rootElementsStack(rootElementsStackLevel + 1)
End Function

Private Function PeekRootElement()
    Set PeekRootElement = rootElementsStack(rootElementsStackLevel)
End Function

Private Function CreateDomElement(ByVal parent As IXMLDOMElement, ByVal tagName As String, Optional ByVal value As String = "") As IXMLDOMElement
    
    Dim result As IXMLDOMElement
    Set result = doc.createElement(tagName)
    result.Text = value
    parent.appendChild result
    
    Set CreateDomElement = result

End Function

Private Function GetStatusText(ByVal Source As TestResult) As String
    Dim result As String
    
    If Source.IsSuccess Then
        result = "Success"
    ElseIf Source.IsFailure Then
        result = "Failure"
    ElseIf Source.IsError Then
        result = "Error"
    Else
        result = "Ignored"
    End If
    
    GetStatusText = result
End Function

Private Sub Finish()
    doc.save myOutputFilePath
End Sub

Public Sub Init(outputFilePath As String)
    myOutputFilePath = outputFilePath
End Sub

''
' This method is called when an <i>ITestRunner</i> is started.
'
' @param Name The name of the first test in the run (usually a <i>TestSuite</i>).
' @param TestCount The total number of tests to be run. TestSuites and TestFixtures do not count.
'
Public Sub IEventListener_RunStarted(ByVal Name As String, ByVal TestCount As Long)
    Dim rootElement As IXMLDOMElement
    Set rootElement = doc.createElement("test-results")
    doc.appendChild rootElement
    
    ReDim rootElementsStack(10)
    rootElementsStackLevel = -1
    PushRootElement rootElement
    
End Sub

''
' This method is called when an <i>ITestRunner</i> has finished.
'
' @param Result The final result of the tests that were run.
'
Public Sub IEventListener_RunFinished(ByVal result As TestResult)
    Finish
End Sub

''
' This method is called when an error occurs outside of a test.
'
' @param ErrInfo Information about the error that occurred.
'
Public Sub IEventListener_RunException(ByVal info As ErrorInfo)
    CreateDomElement rootElementsStack(0), "error", info.Description
    Finish
End Sub

''
' This method is called when a <i>TestSuite</i> is started.
'
' @param Suite The <i>TestSuite</i> that is starting.
'
Public Sub IEventListener_TestSuiteStarted(ByVal Suite As TestSuite)
    Dim parent As IXMLDOMElement
    Set parent = PeekRootElement()

    Dim suiteElement As IXMLDOMElement
    Set suiteElement = CreateDomElement(parent, "test-suite")
    CreateDomElement suiteElement, "name", Suite.Name
    
    PushRootElement suiteElement
    
End Sub

''
' This method is called when a <i>TestSuite</i> has finished running.
'
' @param Result The final result for the test suite.
'
Public Sub IEventListener_TestSuiteFinished(ByVal result As TestResult)
    PopRootElement
End Sub

''
' This method is called when a test case is started running.
'
' @param Test The test case to be run.
'
Public Sub IEventListener_TestCaseStarted(ByVal Test As TestCase)
End Sub

''
' This method is called when a test case has finished running.
'
' @param Result The final result for the test case.
'
Public Sub IEventListener_TestCaseFinished(ByVal result As TestResult)
    Dim parent As IXMLDOMElement
    Set parent = PeekRootElement()
    
    Dim caseElement As IXMLDOMElement
    Set caseElement = CreateDomElement(parent, "test-case")
    
    CreateDomElement caseElement, "name", result.Name
    CreateDomElement caseElement, "status", GetStatusText(result)
    CreateDomElement caseElement, "message", result.Message
    
End Sub

''
' This method is called when user output is being sent.
'
' @param Output The output the user wishes to send.
'
Public Sub IEventListener_TestOutput(ByVal Output As TestOutput)

End Sub

''
' This method is called when an unhandled error occurs during a test.
'
' @param Info Information about the error.
'
Public Sub IEventListener_UnhandledError(ByVal info As ErrorInfo)

End Sub

