Attribute VB_Name = "Program"
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Sub Main()
    Dim sCommandLine() As String
    sCommandLine = Split(Command$)
    
    Dim RunSilently As Boolean
    If (UBound(sCommandLine) >= 0) Then
        RunSilently = sCommandLine(0) = "-s"
    Else
        RunSilently = False
    End If
    
    If (RunSilently) Then
        Dim outputFilePath As String
        outputFilePath = sCommandLine(1)
        Dim listener As New EventsToFileRedirector
        listener.Init outputFilePath
        frmTestRunner.RunSilently listener
        Unload frmTestRunner
    Else
        frmTestRunner.Show
    End If
    
End Sub

