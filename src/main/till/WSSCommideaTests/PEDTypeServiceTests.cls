VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PEDTypeServiceTests"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const ExpectedPED820OciusSentinelStartVersion  As String = "3.06.4.43"

Public Sub GivenAnOciusSentinelVersionOneRevisionPriorToThe820StartVersion_WhenGetPEDTypeIsCalled_Then810PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType810, TestPEDTypeService.GetPEDType("3.06.4.42"), "TestPEDTypeService.GetPEDType(""3.06.4.42"") does not return an '810' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionSameAs820StartVersionSaveForNoRevision_WhenGetPEDTypeIsCalled_Then810PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType810, TestPEDTypeService.GetPEDType("3.6.4"), "TestPEDTypeService.GetPEDType(""3.6.4"") does not return an '810' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionSameAs820StartVersionSaveForNoBuildAndRevision_WhenGetPEDTypeIsCalled_Then810PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType810, TestPEDTypeService.GetPEDType("3.06"), "TestPEDTypeService.GetPEDType(""3.06"") does not return an '810' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionSameAs820StartVersionSaveForNoMinorAndBuildAndRevision_WhenGetPEDTypeIsCalled_Then810PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType810, TestPEDTypeService.GetPEDType("3"), "TestPEDTypeService.GetPEDType(""3"") does not return an '810' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionEarlierByOneRevisionAndWithAdditionalSubRevision_WhenGetPEDTypeIsCalled_Then810PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType810, TestPEDTypeService.GetPEDType("3.06.4.42.1"), "TestPEDTypeService.GetPEDType(""3.06.4.42.1"") does not return an '810' PED type"
End Sub

Public Sub GivenAnEmptyOciusSentinelVersion_WhenGetPEDTypeIsCalled_Then810PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType810, TestPEDTypeService.GetPEDType(""), "TestPEDTypeService.GetPEDType("""") does not return an '810' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionSameAs820StartVersion_WhenGetPEDTypeIsCalled_Then820PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual PEDType.ePEDType820, TestPEDTypeService.GetPEDType(TestPEDTypeService.PED820OciusSentinelStartVersion), "TestPEDTypeService.GetPEDType(""the same version as PED 820 Start Version"") does not return an '820' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionOneRevisionLaterThanThe820StartVersion_WhenGetPEDTypeIsCalled_Then820PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType820, TestPEDTypeService.GetPEDType("3.06.4.44"), "TestPEDTypeService.GetPEDType(""3.06.4.44"") does not return an '820' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionOneBuildLaterAndNoRevision_WhenGetPEDTypeIsCalled_Then820PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType820, TestPEDTypeService.GetPEDType("3.06.5"), "TestPEDTypeService.GetPEDType(""3.06.5"") does not return an '820' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionOneMinorLaterAndNoBuildAndRevision_WhenGetPEDTypeIsCalled_Then820PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType820, TestPEDTypeService.GetPEDType("3.7"), "TestPEDTypeService.GetPEDType(""3.7"") does not return an '820' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionOneMajorLaterAndNoMinorAndBuildAndRevision_WhenGetPEDTypeIsCalled_Then820PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType820, TestPEDTypeService.GetPEDType("4"), "TestPEDTypeService.GetPEDType(""4"") does not return an '820' PED type"
End Sub

Public Sub GivenAnOciusSentinelVersionSameAs820StartVersionExceptHasAdditionalSubRevision_WhenGetPEDTypeIsCalled_Then820PEDTypeIsReturned()
    Dim TestPEDTypeService As New WSSCommidea.PEDTypeService
    
    Assert.AreEqual ExpectedPED820OciusSentinelStartVersion, TestPEDTypeService.PED820OciusSentinelStartVersion, "Pre-test test...PED 820 Ocius Sentinel Start Version is not """ & ExpectedPED820OciusSentinelStartVersion & """"
    Assert.AreEqual PEDType.ePEDType820, TestPEDTypeService.GetPEDType("3.06.4.43.0"), "TestPEDTypeService.GetPEDType(""3.06.4.43.0"") does not return an '820' PED type"
End Sub
