﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="text" indent="yes"/>

    <xsl:template match="test-results">
      <xsl:apply-templates select = "test-suite">
        <xsl:with-param name ="ident"/>
      </xsl:apply-templates>

      <xsl:text>&#10;</xsl:text>
      
      <xsl:choose>
        <xsl:when test ="error">
          <xsl:text>Tests run failed with exception:&#10;</xsl:text>
          <xsl:value-of select ="error" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>Tests run results:</xsl:text>
          <xsl:text>&#10;    Total:   </xsl:text>
          <xsl:value-of select ="count(//test-case)"/>
          <xsl:text>&#10;    Success: </xsl:text>
          <xsl:value-of select ="count(//test-case[status='Success'])"/>
          <xsl:text>&#10;    Failure: </xsl:text>
          <xsl:value-of select ="count(//test-case[status='Failure'])"/>
          <xsl:text>&#10;    Error:   </xsl:text>
          <xsl:value-of select ="count(//test-case[status='Error'])"/>
          <xsl:text>&#10;    Ignored: </xsl:text>
          <xsl:value-of select ="count(//test-case[status='Ignored'])"/>
        </xsl:otherwise>
      </xsl:choose >


    </xsl:template>

  <xsl:template match="test-suite">
    <xsl:param name ="ident" />

    <xsl:value-of select ="$ident"/>
    <xsl:variable name ="next-ident" select ="concat($ident, '   ')" />
    
    <xsl:text>Suite </xsl:text>
    <xsl:value-of select ="name"/>
    <xsl:text>&#10;</xsl:text>

    <xsl:apply-templates select = "test-suite">
      <xsl:with-param name ="ident" select ="$next-ident" />
    </xsl:apply-templates>

    <xsl:apply-templates select = "test-case">
      <xsl:with-param name ="ident" select ="$next-ident" />
    </xsl:apply-templates>
    
  </xsl:template>

  <xsl:template match="test-case">
    <xsl:param name ="ident" />
    
    <xsl:value-of select ="$ident"/>
    <xsl:choose>
      <xsl:when test ="status/text() != 'Success'">
        <xsl:text>(!) </xsl:text>
      </xsl:when >
      <xsl:otherwise>
        <xsl:text>    </xsl:text>
      </xsl:otherwise> 
    </xsl:choose>
    <xsl:value-of select ="status"/>
    <xsl:text>: </xsl:text>
    <xsl:value-of select ="name"/>
    <xsl:text>&#10;</xsl:text>

    <xsl:choose>
      <xsl:when test ="status/text() != 'Success'">
        <xsl:value-of select ="$ident"/>
        <xsl:text>        </xsl:text>

        <xsl:variable name="newtext">
          <xsl:call-template name="string-replace-all">
            <xsl:with-param name="text" select="message/text()" />
            <xsl:with-param name="replace" select="'&#10;'" />
            <xsl:with-param name="by" select="concat('&#10;', $ident, '        ')" />
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:value-of select ="$newtext"/>
        <xsl:text>&#10;</xsl:text>
      </xsl:when >
    </xsl:choose>
    
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
