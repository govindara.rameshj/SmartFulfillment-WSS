<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes"/>
  <xsl:variable name="id"><xsl:number/>0</xsl:variable>
  <xsl:template match="/test-results">
    <testsuites>
      <xsl:apply-templates select="test-suite/test-suite"/>
      <xsl:apply-templates select="error"/>
    </testsuites>
  </xsl:template>
  <xsl:template match="test-suite/test-suite" >
    <testsuite>
      <xsl:attribute name="id">
        <xsl:value-of select="count(preceding::node())"/>
      </xsl:attribute>
      <xsl:attribute name="name">
        <xsl:value-of disable-output-escaping="yes" select="name" />
      </xsl:attribute>
      <xsl:attribute name="tests">
        <xsl:value-of select="count(./test-case)"/>
      </xsl:attribute>
      <xsl:attribute name="failures">
        <xsl:value-of select="count(./test-case[./status = 'Failure'])"/>
      </xsl:attribute>
      <xsl:attribute name="errors">
        <xsl:value-of select="count(./test-case[./status = 'Error'])"/>
      </xsl:attribute>
      <xsl:attribute name="skipped">
        <xsl:value-of select="count(./test-case[./status = 'Ignored'])"/>
      </xsl:attribute>
      <!-- Time not implemented -->
      <xsl:attribute name="time">0</xsl:attribute>
      <xsl:apply-templates select="test-case" />
    </testsuite>
  </xsl:template>
  <xsl:template match="test-case" >
    <testcase>
      <xsl:attribute name="name">
        <xsl:value-of select="name" />
      </xsl:attribute>
      <xsl:attribute name="classname">
        <xsl:value-of select="concat(../../name,'.',../name)" />
      </xsl:attribute>
      <!-- Time not implemented -->
      <xsl:attribute name="time">0</xsl:attribute>
      <xsl:if test="./status  = 'Failure'">
        <failure>
          <xsl:value-of select="./message" />
        </failure>
      </xsl:if>
      <xsl:if test="./status  = 'Error'">
        <error>
          <xsl:value-of select="./message" />
        </error>
      </xsl:if>
      <xsl:if test="./status  = 'Ignored'">
        <skipped>
          <xsl:value-of select="./message" />
        </skipped>
      </xsl:if>
    </testcase>
  </xsl:template>
  <xsl:template match="test-results/error" >
    <testsuite>
      <xsl:attribute name="id">$id</xsl:attribute>
      <xsl:attribute name="name">Test run failed</xsl:attribute>
      <xsl:attribute name="tests">1</xsl:attribute>
      <xsl:attribute name="failures">0</xsl:attribute>
      <xsl:attribute name="errors">1</xsl:attribute>
      <xsl:attribute name="skipped">0</xsl:attribute>
      <xsl:attribute name="time">0</xsl:attribute>
      <testcase>
        <error>
          <xsl:value-of select="."/>
        </error>
      </testcase>
    </testsuite>
  </xsl:template>
</xsl:stylesheet>