VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StockDatabase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IStockDatabase

Public vvv As Boolean

Public Function IStockDatabase_StockItemEnquiry(ByRef DB As IBoDatabase, _
                                                ByVal ProductCode As String, ByVal EanNumber As String, _
                                                ByVal ProductDescription As String, ByVal SaleType As String, _
                                                ByVal Supplier As String, ByVal HierarchyCategory As String, _
                                                ByVal HierarchyGroup As String, ByVal HierarchySubGroup As String, _
                                                ByVal HierarchyStyleNumber As String, ByVal ExcludeNonStock As Boolean, _
                                                ByVal ExcludeDeletedStock As Boolean, _
                                                ByVal ExcludeObsoleteStock As Boolean, ByVal ExactMatch As Boolean, _
                                                ByVal FuzzyMethod As Integer, ByVal MaxResults As Integer, _
                                                ByVal AllowMisSpelling As Boolean, _
                                                ByVal ElevateForHierarchy As Boolean) As ADODB.Recordset
   Dim goDatabase As IBoDatabase
   Dim cmd As New ADODB.Command
   Dim rsStock As New ADODB.Recordset
   Dim strSql As String

   With cmd
      .ActiveConnection = DB.Connection
      .CommandType = adCmdStoredProc
      .CommandText = "StockItemEnquiry"
      
      If Len(ProductCode) > 0 Then
         .Parameters.Append .CreateParameter("@ProductCode", adVarChar, adParamInput, 6, ProductCode)
      Else
         .Parameters.Append .CreateParameter("@ProductCode", adVarChar, adParamInput, 6, Null)
      End If
      
      If Len(EanNumber) > 0 Then
         .Parameters.Append .CreateParameter("@EanNumber", adVarChar, adParamInput, 16, EanNumber)
      Else
         .Parameters.Append .CreateParameter("@EanNumber", adVarChar, adParamInput, 16, Null)
      End If
      
      If Len(ProductDescription) > 0 Then
         .Parameters.Append .CreateParameter("@ProductDescription", adVarChar, adParamInput, 40, ProductDescription)
      Else
         .Parameters.Append .CreateParameter("@ProductDescription", adVarChar, adParamInput, 40, Null)
      End If

      If Len(SaleType) > 0 Then
         .Parameters.Append .CreateParameter("@SaleType", adVarChar, adParamInput, 1, SaleType)
      Else
         .Parameters.Append .CreateParameter("@SaleType", adVarChar, adParamInput, 1, Null)
      End If
      
      If Len(Supplier) > 0 Then
         .Parameters.Append .CreateParameter("@Supplier", adVarChar, adParamInput, 5, Supplier)
      Else
         .Parameters.Append .CreateParameter("@Supplier", adVarChar, adParamInput, 5, Null)
      End If
      
      If Len(HierarchyCategory) > 0 Then
         .Parameters.Append .CreateParameter("@HierarchyCategory", adVarChar, adParamInput, 6, HierarchyCategory)
      Else
         .Parameters.Append .CreateParameter("@HierarchyCategory", adVarChar, adParamInput, 6, Null)
      End If

      If Len(HierarchyGroup) > 0 Then
         .Parameters.Append .CreateParameter("@HierarchyGroup", adVarChar, adParamInput, 6, HierarchyGroup)
      Else
         .Parameters.Append .CreateParameter("@HierarchyGroup", adVarChar, adParamInput, 6, Null)
      End If
      
      If Len(HierarchySubGroup) > 0 Then
         .Parameters.Append .CreateParameter("@HierarchySubGroup", adVarChar, adParamInput, 6, HierarchySubGroup)
      Else
         .Parameters.Append .CreateParameter("@HierarchySubGroup", adVarChar, adParamInput, 6, Null)
      End If
      
      If Len(HierarchyStyleNumber) > 0 Then
         .Parameters.Append .CreateParameter("@HierarchyStyleNumber", adVarChar, adParamInput, 6, HierarchyStyleNumber)
      Else
         .Parameters.Append .CreateParameter("@HierarchyStyleNumber", adVarChar, adParamInput, 6, Null)
      End If

      If ExcludeNonStock = True Then
         .Parameters.Append .CreateParameter("@ExcludeNonStock", adBoolean, adParamInput, , ExcludeNonStock)
      Else
         .Parameters.Append .CreateParameter("@ExcludeNonStock", adBoolean, adParamInput, , Null)
      End If
      
      If ExcludeDeletedStock = True Then
         .Parameters.Append .CreateParameter("@ExcludeDeletedStock", adBoolean, adParamInput, , ExcludeDeletedStock)
      Else
         .Parameters.Append .CreateParameter("@ExcludeDeletedStock", adBoolean, adParamInput, , Null)
      End If
      
      If ExcludeObsoleteStock = True Then
         .Parameters.Append .CreateParameter("@ExcludeObsoleteStock", adBoolean, adParamInput, , ExcludeObsoleteStock)
      Else
         .Parameters.Append .CreateParameter("@ExcludeObsoleteStock", adBoolean, adParamInput, , Null)
      End If

      .Parameters.Append .CreateParameter("@ExactMatch", adBoolean, adParamInput, , ExactMatch)
      .Parameters.Append .CreateParameter("@FuzzyMethod", adInteger, adParamInput, , FuzzyMethod)
      .Parameters.Append .CreateParameter("@MaxResults", adInteger, adParamInput, , MaxResults)
      .Parameters.Append .CreateParameter("@AllowMisSpelling", adBoolean, adParamInput, , AllowMisSpelling)
      .Parameters.Append .CreateParameter("@ElevateForHierarchy", adBoolean, adParamInput, , ElevateForHierarchy)
   End With

   
   strSql = "Stored Procedure: StockItemEnquiry"
   strSql = strSql & " @ProductCode = " & IIf(Len(ProductCode) > 0, ProductCode, "null")
   strSql = strSql & " ,@EanNumber = " & IIf(Len(EanNumber) > 0, EanNumber, "null")
   strSql = strSql & " ,@ProductDescription = " & IIf(Len(ProductDescription) > 0, ProductDescription, "null")
   strSql = strSql & " ,@SaleType = " & IIf(Len(SaleType) > 0, SaleType, "null")
   strSql = strSql & " ,@Supplier = " & IIf(Len(Supplier) > 0, Supplier, "null")
   strSql = strSql & " ,@HierarchyCategory = " & IIf(Len(HierarchyCategory) > 0, HierarchyCategory, "null")
   strSql = strSql & " ,@HierarchyGroup = " & IIf(Len(HierarchyGroup) > 0, HierarchyGroup, "null")
   strSql = strSql & " ,@HierarchySubGroup = " & IIf(Len(HierarchySubGroup) > 0, HierarchySubGroup, "null")
   strSql = strSql & " ,@HierarchyStyleNumber = " & IIf(Len(HierarchyStyleNumber) > 0, HierarchyStyleNumber, "null")
   strSql = strSql & " ,@ExcludeNonStock = " & IIf(ExcludeNonStock = True, "1", "null")
   strSql = strSql & " ,@ExcludeDeletedStock = " & IIf(ExcludeDeletedStock = True, "1", "null")
   strSql = strSql & " ,@ExcludeObsoleteStock = " & IIf(ExcludeObsoleteStock = True, "1", "null")
   strSql = strSql & " ,@ExactMatch = " & IIf(ExactMatch = True, "1", "null")
   strSql = strSql & " ,@FuzzyMethod = " & LTrim(RTrim(CStr(FuzzyMethod)))
   strSql = strSql & " ,@MaxResults = " & LTrim(RTrim(CStr(MaxResults)))
   strSql = strSql & " ,@AllowMisSpelling = " & LTrim(RTrim(CStr(AllowMisSpelling)))
   strSql = strSql & " ,@ElevateForHierarchy = " & LTrim(RTrim(CStr(ElevateForHierarchy)))
   
   DebugMsg "StockDatabase", "StockItemEnquiry", endlDebug, strSql
   
   
   
   Set rsStock = cmd.Execute

   Set IStockDatabase_StockItemEnquiry = rsStock

End Function
