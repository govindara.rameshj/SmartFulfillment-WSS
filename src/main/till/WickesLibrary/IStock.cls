VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function LoadStock(ByVal ProductOrEANCode As String, ByVal ProductDescription As String, _
                          ByVal SaleType As String, ByVal Supplier As String, _
                          ByVal HierarchyCategory As String, ByVal HierarchyGroup As String, _
                          ByVal HierarchySubGroup As String, ByVal HierarchyStyleNumber As String, _
                          ByVal ExcludeNonStock As Boolean, ByVal ExcludeDeletedStock As Boolean, _
                          ByVal ExcludeObsoleteStock As Boolean, ByVal ExactMatch As Boolean, _
                          ByVal FuzzyMethod As Integer) As ADODB.Recordset

End Function
