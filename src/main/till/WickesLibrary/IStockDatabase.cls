VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IStockDatabase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function StockItemEnquiry(ByRef DB As IBoDatabase, _
                                 ByVal ProductCode As String, ByVal EanNumber As String, _
                                 ByVal ProductDescription As String, ByVal SaleType As String, _
                                 ByVal Supplier As String, ByVal HierarchyCategory As String, _
                                 ByVal HierarchyGroup As String, ByVal HierarchySubGroup As String, _
                                 ByVal HierarchyStyleNumber As String, ByVal ExcludeNonStock As Boolean, _
                                 ByVal ExcludeDeletedStock As Boolean, _
                                 ByVal ExcludeObsoleteStock As Boolean, ByVal ExactMatch As Boolean, _
                                 ByVal FuzzyMethod As Integer, ByVal MaxResults As Integer, _
                                 ByVal AllowMisSpelling As Boolean, _
                                 ByVal ElevateForHierarchy As Boolean) As ADODB.Recordset

End Function


