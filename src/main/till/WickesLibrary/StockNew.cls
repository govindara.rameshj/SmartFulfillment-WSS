VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StockNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IStock

Private mStockDatabase As IStockDatabase
Private mDatabaseConnection As IBoDatabase
Private mMaximumRecordsReturned As Integer
Private mAllowMisSpelling As Boolean
Private mElevateForHierarchy As Boolean


Public Property Get StockDatabase() As IStockDatabase
   Set StockDatabase = mStockDatabase
End Property

Public Property Let StockDatabase(ByVal SD As IStockDatabase)
   Set mStockDatabase = SD
End Property

Public Property Get DatabaseConnection() As IBoDatabase
   Set DatabaseConnection = mDatabaseConnection
End Property

Public Property Let DatabaseConnection(ByVal DB As IBoDatabase)
   Set mDatabaseConnection = DB
End Property

Public Property Get MaximumRecordsReturned() As Integer
   MaximumRecordsReturned = mMaximumRecordsReturned
End Property

Public Property Let MaximumRecordsReturned(ByVal intValue As Integer)
   mMaximumRecordsReturned = intValue
End Property

Public Property Get AllowMisSpelling() As Boolean

    AllowMisSpelling = mAllowMisSpelling
End Property

Public Property Let AllowMisSpelling(ByVal Value As Boolean)

    mAllowMisSpelling = Value
End Property

Public Property Get ElevateForHierarchy() As Boolean

    ElevateForHierarchy = mElevateForHierarchy
End Property

Public Property Let ElevateForHierarchy(ByVal Value As Boolean)

    mElevateForHierarchy = Value
End Property


Private Function IStock_LoadStock(ByVal ProductOrEANCode As String, ByVal ProductDescription As String, _
                                  ByVal SaleType As String, ByVal Supplier As String, _
                                  ByVal HierarchyCategory As String, ByVal HierarchyGroup As String, _
                                  ByVal HierarchySubGroup As String, ByVal HierarchyStyleNumber As String, _
                                  ByVal ExcludeNonStock As Boolean, ByVal ExcludeDeletedStock As Boolean, _
                                  ByVal ExcludeObsoleteStock As Boolean, ByVal ExactMatch As Boolean, _
                                  ByVal FuzzyMethod As Integer) As ADODB.Recordset
   Dim ProductCode As String
   Dim EANCode As String

   If Len(LTrim(RTrim(ProductOrEANCode))) > 0 Then
      If Len(LTrim(RTrim(ProductOrEANCode))) > 6 Then
         EANCode = Left("0000000000000000", 16 - Len(LTrim(RTrim(ProductOrEANCode)))) & LTrim(RTrim(ProductOrEANCode))
      Else
         ProductCode = LTrim(RTrim(ProductOrEANCode))
      End If
   End If

  Set IStock_LoadStock = StockDatabase.StockItemEnquiry(DatabaseConnection, _
                                                         ProductCode, EANCode, ProductDescription, SaleType, Supplier, HierarchyCategory, HierarchyGroup, _
                                                         HierarchySubGroup, HierarchyStyleNumber, ExcludeNonStock, ExcludeDeletedStock, ExcludeObsoleteStock, _
                                                         ExactMatch, FuzzyMethod, Me.MaximumRecordsReturned, Me.AllowMisSpelling, Me.ElevateForHierarchy)

End Function
