VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StockCurrent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IStock

Private mDB As IStockDatabase
Private mMaximumRecordsReturned As Integer


Public Property Get Database() As IStockDatabase
   Set Database = mDB
End Property

Public Property Let Database(ByVal DB As IStockDatabase)
   Set mDB = DB
End Property

Public Property Get MaximumRecordsReturned() As Integer
   MaximumRecordsReturned = mMaximumRecordsReturned
End Property

Public Property Let MaximumRecordsReturned(ByVal intValue As Integer)
   mMaximumRecordsReturned = intValue
End Property


Private Function IStock_LoadStock(ByVal ProductOrEANCode As String, ByVal ProductDescription As String, ByVal SaleType As String, ByVal Supplier As String, ByVal HierarchyCategory As String, ByVal HierarchyGroup As String, ByVal HierarchySubGroup As String, ByVal HierarchyStyleNumber As String, ByVal ExcludeNonStock As Boolean, ByVal ExcludeDeletedStock As Boolean, ByVal ExcludeObsoleteStock As Boolean, ByVal ExactMatch As Boolean, ByVal FuzzyMethod As Integer) As ADODB.Recordset

End Function
