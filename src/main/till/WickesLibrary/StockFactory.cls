VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StockFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Issue One: Using the GetParameter method in the Session class
'           It will pick up all ParameterID values greater than 0 and populate a collection object
'           Cannot remove dynamic sql filter to pick up negative ParameterIDs because the position of existing entries
'           will have moved

Private Const mClassName As String = "StockFactory"

Public Function Create(ByRef DatabaseConnection As IBoDatabase) As IStock
    Dim DatabaseFactory As New StockDatabaseFactory
    Dim StockDatabase As New IStockDatabase
    Dim StockNew As New StockNew

    Set StockDatabase = DatabaseFactory.Create
    With StockNew
        .StockDatabase = StockDatabase
        .DatabaseConnection = DatabaseConnection
        .MaximumRecordsReturned = MaximumRecordsReturned(DatabaseConnection)
        .AllowMisSpelling = GetAllowMisSpelling(DatabaseConnection)
        .ElevateForHierarchy = GetElevateForHierarchy(DatabaseConnection)
    End With

    Set Create = StockNew
End Function

Private Function MaximumRecordsReturned(ByRef DatabaseConnection As IBoDatabase) As Integer

   Dim goDatabase As IBoDatabase
   Dim cmd As New ADODB.Command
   Dim rsParameter As New ADODB.Recordset
   
   DebugMsg mClassName, "MaximumRecordsReturned", endlDebug, "Stored Procedure: ParametersGetIntegerValueById @Id = 5003"
   With cmd
      .ActiveConnection = DatabaseConnection.Connection
      .CommandType = adCmdStoredProc
      .CommandText = "ParametersGetIntegerValueById"
      .Parameters.Append cmd.CreateParameter("Id", adInteger, adParamInput, , 5003)
   End With
   Set rsParameter = cmd.Execute

   MaximumRecordsReturned = False
   If rsParameter Is Nothing = False Then
      rsParameter.MoveFirst
      If rsParameter.BOF = False And rsParameter.EOF = False Then
         MaximumRecordsReturned = rsParameter.Fields("LongValue")
      End If
   End If
End Function

Private Function GetAllowMisSpelling(ByRef DatabaseConnection As IBoDatabase) As Integer

   GetAllowMisSpelling = GetBooleanValueParameterById(DatabaseConnection, "AllowMisSpelling", 5100)
End Function

Private Function GetElevateForHierarchy(ByRef DatabaseConnection As IBoDatabase) As Boolean

    GetElevateForHierarchy = GetBooleanValueParameterById(DatabaseConnection, "ElevateForHierarchy", 5101)
End Function

Private Function GetBooleanValueParameterById(ByRef DatabaseConnection As IBoDatabase, ByVal ParameterName As String, ByVal ParameterId As Integer) As Boolean
    Dim rsParameter As New ADODB.Recordset
    
    DebugMsg mClassName, "GetBooleanValueParameterById", endlDebug, "Stored Procedure: ParametersGetBooleanValueById @Id = " & Trim(CStr(ParameterId))
    
    GetBooleanValueParameterById = False
    
On Error GoTo Quit
    
    With GetStoredProcedureCommand(DatabaseConnection, "ParametersGetBooleanValueById")
        .Parameters.Append .CreateParameter("Id", adInteger, adParamInput, , ParameterId)
        Set rsParameter = .Execute
    End With
    
    If rsParameter Is Nothing = False Then
        rsParameter.MoveFirst
        If rsParameter.BOF = False And rsParameter.EOF = False Then
            GetBooleanValueParameterById = rsParameter.Fields("BooleanValue")
        End If
    End If
Quit:
On Error GoTo 0
End Function

Private Function GetStoredProcedureCommand(ByRef DatabaseConnection As IBoDatabase, ByVal StoredProcedureName As String) As ADODB.Command
   Dim goDatabase As IBoDatabase
   Dim cmd As New ADODB.Command
   Dim rsParameter As New ADODB.Recordset
   
   DebugMsg mClassName, "GetStoredProcedureCommand", endlDebug, "Stored Procedure: " & StoredProcedureName
   
   With cmd
      .ActiveConnection = DatabaseConnection.Connection
      .CommandType = adCmdStoredProc
      .CommandText = StoredProcedureName
    End With
    Set GetStoredProcedureCommand = cmd
End Function

