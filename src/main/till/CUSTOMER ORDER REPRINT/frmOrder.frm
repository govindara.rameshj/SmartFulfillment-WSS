VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{FDAC2480-F4ED-4632-AA78-DCA210A74E49}#6.0#0"; "SPR32X60.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Begin VB.Form frmOrder 
   Caption         =   "Customer Order Reprint"
   ClientHeight    =   4230
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7230
   Icon            =   "frmOrder.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   4230
   ScaleWidth      =   7230
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPrintAll 
      Caption         =   "Alt F9-Print All"
      Height          =   375
      Left            =   1680
      TabIndex        =   5
      Top             =   3360
      Width           =   1455
   End
   Begin VB.Timer tmrClose 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   4920
      Top             =   2280
   End
   Begin VB.CommandButton cmdPreview 
      Caption         =   "Ctrl F8-Preview"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   3240
      TabIndex        =   6
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Frame fraPrintType 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Print Type"
      Height          =   975
      Left            =   120
      TabIndex        =   11
      Top             =   2160
      Width           =   4335
      Begin LpLib.fpCombo cmbPrintType 
         Height          =   315
         Left            =   1320
         TabIndex        =   3
         Top             =   360
         Width           =   2685
         _Version        =   196608
         _ExtentX        =   4736
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   1
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmOrder.frx":058A
      End
      Begin VB.Label lblPrinted 
         BackStyle       =   0  'Transparent
         Caption         =   "Print Type"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame fraCriteria 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Customer Order Search Criteria"
      Height          =   1935
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   6975
      Begin VB.CommandButton cmdClear 
         Caption         =   "F3-Clear"
         Height          =   375
         Left            =   5520
         TabIndex        =   2
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "F7-Search"
         Height          =   375
         Left            =   4200
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox txtOrderNumber 
         BackColor       =   &H8000000E&
         Height          =   285
         Left            =   1320
         MaxLength       =   6
         TabIndex        =   0
         Top             =   360
         Width           =   2775
      End
      Begin VB.Label lblCustomer 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   1320
         TabIndex        =   17
         Top             =   1200
         Width           =   2655
      End
      Begin VB.Label lblOrderNo 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   1320
         TabIndex        =   16
         Top             =   840
         Width           =   2775
      End
      Begin VB.Label lblCustomerlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Customer:"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblOrderNolbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Order Number:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label lblOrder 
         BackStyle       =   0  'Transparent
         Caption         =   "Order Number"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "F10-Close"
      Height          =   375
      Left            =   6360
      TabIndex        =   7
      Top             =   3360
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   10
      Top             =   3855
      Width           =   7230
      _ExtentX        =   12753
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmOrder.frx":09A1
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4940
            MinWidth        =   707
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:10"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin FPSpread.vaSpread sprdOrder 
      Height          =   495
      Left            =   2040
      TabIndex        =   13
      Top             =   3600
      Visible         =   0   'False
      Width           =   615
      _Version        =   393216
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   0
      MaxRows         =   0
      NoBeep          =   -1  'True
      OperationMode   =   2
      RowHeaderDisplay=   2
      ScrollBarExtMode=   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmOrder.frx":2B6D
      ScrollBarTrack  =   1
   End
End
Attribute VB_Name = "frmOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmOrder
'* Date   : 02/04/04
'* Author : damians
'*$Archive: /Projects/OasysV2/VB/Customer Order Reprint/frmOrder.frm $
'**********************************************************************************************
'* Summary: The main order form which is called first by the EXE.  This is called from the menu
'*          and is passed in an Action type.  This provides the functionality as follows :
'*              "PTI" : + B Print a File Copy and Warehouse Copy of a given orderno (i.e. PTIB000006)
'*                      + F Print a File Copy of a given orderno (i.e. PTIF000006)
'*                      + W Print a Warehouse Copy of a given orderno (i.e. PTIW000006)
'*              "PTA" : + B Print a File Copy and Warehouse Copy of all unprinted Customer Orders
'*                      + F Print a File Copy of all unprinted Customer Orders
'*                      + W Print a Warehouse Copy of all unprinted Customer Orders
'*              "DSI" : Preview a File Copy and Warehouse Copy of a given orderno (i.e. PTI000006)
'*
'*
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME           As String = "frmOrder"

'Command Line Actions
Const ACTION_PRINT_INDIVIDUAL_ORDER         As String = "PTI"  'Print Individual Orders De
Const ACTION_PRINT_ALL_ORDERS               As String = "PTA"  'Print All Orders
Const ACTION_DISPLAY_INDIVIDUAL_ORDER       As String = "DSI"  'Print Individual Orders De

'Order Warehouse Copy Spread Sheet Constants
Const COL_ORDER_WAREHOUSE_ITEMNO            As Long = 1
Const COL_ORDER_WAREHOUSE_SKU               As Long = 2
Const COL_ORDER_WAREHOUSE_DESCRIPTION1      As Long = 3
Const COL_ORDER_WAREHOUSE_DESCRIPTION2      As Long = 4
Const COL_ORDER_WAREHOUSE_SIZE              As Long = 5
Const COL_ORDER_WAREHOUSE_QTY               As Long = 6
Const COL_ORDER_WAREHOUSE_DATEDUE           As Long = 7
Const COL_ORDER_WAREHOUSE_DATEIN            As Long = 8
Const COL_ORDER_WAREHOUSE_DATESOLD          As Long = 9
Const COL_ORDER_WAREHOUSE_NOTES1            As Long = 10
Const COL_ORDER_WAREHOUSE_NOTES2            As Long = 11
Const COL_ORDER_WAREHOUSE_SOURCE            As Long = 12

Const ORDER_WAREHOUSE_HEADER_ROWS           As Long = 4
Const ORDER_WAREHOUSE_MAX_COLUMNS           As Long = 12

'Order File Copy Spread Sheet Constants
Const COL_ORDER_FILE_ITEMNO                 As Long = 1
Const COL_ORDER_FILE_SKU                    As Long = 2
Const COL_ORDER_FILE_DESCRIPTION1           As Long = 3
Const COL_ORDER_FILE_DESCRIPTION2           As Long = 4
Const COL_ORDER_FILE_SIZE                   As Long = 5
Const COL_ORDER_FILE_QTY                    As Long = 6
Const COL_ORDER_FILE_RATE                   As Long = 7
Const COL_ORDER_FILE_TOTAL                  As Long = 8
Const COL_ORDER_FILE_STATUS                 As Long = 9

Const ORDER_FILE_HEADER_ROWS                As Long = 11
Const ORDER_FILE_MAX_COLUMNS                As Long = 9

'Print Type Combobox Constants
Const CMB_PRINTTYPE_BOTH                    As Integer = 0
Const CMB_PRINTTYPE_FILE                    As Integer = 1
Const CMB_PRINTTYPE_WAREHOUSE               As Integer = 2

Dim mlngQtyDecNum       As Long 'hold number of decimal places to show for quantities
Dim mlngValueDecNum     As Long 'hold number of decimal places to show for values

Dim mintTimerCount      As Integer  'hold the number of minutes for the close timer
Dim mbolClose           As Boolean

Private Sub cmbPrintType_GotFocus()
    
    cmbPrintType.ListDown = True
    
End Sub

Private Sub cmbPrintType_KeyDown(KeyCode As Integer, Shift As Integer)

    If (Shift = 0) And (KeyCode = vbKeyReturn) Then
        Call cmbPrintType_KeyPress(vbKeyReturn)
    End If
    
End Sub

Private Sub cmbPrintType_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        cmdPrint.SetFocus
    End If
    
End Sub

Private Sub cmdClear_Click()

    txtOrderNumber.Text = ""

    lblOrderNo.Caption = ""
    
    lblCustomer.Caption = ""
    
    txtOrderNumber.SetFocus
    
End Sub

Private Sub ResetCloseTimer()

    mintTimerCount = 0

    tmrClose.Enabled = False
    
    tmrClose.Enabled = True

End Sub

Private Sub DisableCloseTimer()

    mintTimerCount = 0

    tmrClose.Enabled = False

End Sub

Private Sub EnableCloseTimer()

    mintTimerCount = 0
    
    tmrClose.Enabled = True

End Sub

Private Sub cmdPreview_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdPreview_Click"

    On Error GoTo errHandler

    If lblOrderNo.Caption = "" Then
        'Check that we are not in unattended mode
        If Me.Visible = True Then
            Call MsgBox("You can only preview a Customer Order that you have searched on.", vbOKOnly, "Customer Order Preview")
        End If
        
        Exit Sub
    End If
    
    DisableCloseTimer

    Select Case cmbPrintType.ListIndex
        Case CMB_PRINTTYPE_BOTH
            Call DoWHCopy(lblOrderNo.Caption, False)
            
            Load frmPreview
            Call frmPreview.SetPreview("Customer Order Preview", sprdOrder)
            frmPreview.chkSmartPrint.Value = 1
            Call frmPreview.Show(vbModal)
            
            Call DoFCopy(lblOrderNo.Caption, False)
            
            Load frmPreview
            Call frmPreview.SetPreview("Customer Order Preview", sprdOrder)
            frmPreview.chkSmartPrint.Value = 1
            Call frmPreview.Show(vbModal)
                        
        Case CMB_PRINTTYPE_FILE
            Call DoFCopy(lblOrderNo.Caption, False)
            
            Load frmPreview
            Call frmPreview.SetPreview("Customer Order Preview", sprdOrder)
            frmPreview.chkSmartPrint.Value = 1
            Call frmPreview.Show(vbModal)
            
        Case CMB_PRINTTYPE_WAREHOUSE
            Call DoWHCopy(lblOrderNo.Caption, False)
            
            Load frmPreview
            Call frmPreview.SetPreview("Customer Order Preview", sprdOrder)
            frmPreview.chkSmartPrint.Value = 1
            Call frmPreview.Show(vbModal)
                        
    End Select
    
    EnableCloseTimer
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
        
End Sub

Private Sub cmdClear_GotFocus()

    cmdClear.FontBold = True

End Sub

Private Sub cmdClear_LostFocus()

    cmdClear.FontBold = False

End Sub

Private Sub cmdPreview_GotFocus()

    cmdPreview.FontBold = True

End Sub

Private Sub cmdPreview_LostFocus()

    cmdPreview.FontBold = False

End Sub

Private Sub PrintIndividCustomerOrder(ByVal strOrderNo As String, ByVal intPrintType As Integer)

Const PROCEDURE_NAME As String = MODULE_NAME & ".PrintIndividCustomerOrder"

Dim oOrder          As Object

    On Error GoTo errHandler
    
    Me.MousePointer = vbHourglass
    
    DisableCloseTimer
    
    'Create business object
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
    
    'Load Required Fields
    Call oOrder.IBo_AddLoadField(FID_SOHEADER_OrderNo)
    
    'Load Printed Filter
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, strOrderNo)
    
    If oOrder.IBo_Load = True Then
        Select Case intPrintType
            Case CMB_PRINTTYPE_BOTH
                Call DoWHCopy(oOrder.OrderNo, True)
                
                Call DoFCopy(oOrder.OrderNo, True)
                
            Case CMB_PRINTTYPE_FILE
                Call DoFCopy(oOrder.OrderNo, True)
                
            Case CMB_PRINTTYPE_WAREHOUSE
                Call DoWHCopy(oOrder.OrderNo, True)
                            
        End Select
    End If
    
    EnableCloseTimer

    Me.MousePointer = vbDefault
    
    Exit Sub
    
errHandler:

    Me.MousePointer = vbDefault

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub PrintAllCustomerOrders(ByVal intPrintType As Integer)

Const PROCEDURE_NAME As String = MODULE_NAME & ".PrintAllCustomerOrders"

Dim oOrder          As Object
Dim oCol            As Collection
Dim intLoop         As Integer
    
    On Error GoTo errHandler
    
    Me.MousePointer = vbHourglass
    
    DisableCloseTimer
    
    'Create business object
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
    
    'Load Required Fields
    Call oOrder.IBo_AddLoadField(FID_SOHEADER_OrderNo)
    
    'Load Printed Filter
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_Printed, False)
    
    'Get matches
    Set oCol = oOrder.IBo_LoadMatches
    
    For intLoop = 1 To oCol.Count Step 1
        Select Case intPrintType
            Case CMB_PRINTTYPE_BOTH
                Call DoWHCopy(oCol(intLoop).OrderNo, True)
                
                Call DoFCopy(oCol(intLoop).OrderNo, True)
                
            Case CMB_PRINTTYPE_FILE
                Call DoFCopy(oCol(intLoop).OrderNo, True)
                
            Case CMB_PRINTTYPE_WAREHOUSE
                Call DoWHCopy(oCol(intLoop).OrderNo, True)
                            
        End Select
        
    Next
    
    EnableCloseTimer
    
    Me.MousePointer = vbDefault
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub cmdPrint_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdPrint_Click"
    
    On Error GoTo errHandler
    
    If lblOrderNo.Caption <> "" Then
        DisableCloseTimer
        
        Select Case cmbPrintType.ListIndex
            Case CMB_PRINTTYPE_BOTH
                Call DoWHCopy(lblOrderNo.Caption, True)
                
                Call DoFCopy(lblOrderNo.Caption, True)
                
            Case CMB_PRINTTYPE_FILE
                Call DoFCopy(lblOrderNo.Caption, True)
                
            Case CMB_PRINTTYPE_WAREHOUSE
                Call DoWHCopy(lblOrderNo.Caption, True)
                            
        End Select
        
        EnableCloseTimer
    End If
    
    Exit Sub
    
errHandler:

    Me.MousePointer = vbDefault

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub cmdPrintAll_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdPrintAll_Click"
    
    On Error GoTo errHandler
    
    Call PrintAllCustomerOrders(cmbPrintType.ListIndex)
    
    Exit Sub
    
errHandler:

    Me.MousePointer = vbDefault

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub cmdSearch_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdSearch_Click"

Dim oOrder          As Object
Dim oCol            As Collection

    On Error GoTo errHandler
    
    'Create business object
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
            
    'Load Required Fields
    Call oOrder.IBo_AddLoadField(FID_SOHEADER_OrderNo)
    Call oOrder.IBo_AddLoadField(FID_SOHEADER_Name)
    
    'Load Printed Filter
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, Format(txtOrderNumber.Text, "000000"))
    
    'Load matches
    Set oCol = oOrder.IBo_LoadMatches()
    
    If oCol.Count = 1 Then
        lblOrderNo.Caption = oCol(1).OrderNo
        
        lblCustomer.Caption = oCol(1).Name
        
        cmbPrintType.SetFocus
    Else
        'Check that we are not in unattended mode
        If Me.Visible = True Then
            Call MsgBox("The Customer Order Number you have entered can not be found.", vbOKOnly, "Customer Order Preview")
        
            txtOrderNumber.SetFocus
        End If
    End If
        
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)

End Sub

Private Sub Form_Resize()
    
    'Stop the user from making the form to small
    If Me.Width < 7350 Then
        Me.Width = 7350
    End If
    
    If Me.Height < 4635 Then
        Me.Height = 4635
    End If
    
    'Move the close button the bottom right corner
    cmdClose.Left = Me.Width - 1455
    
    cmdClose.Top = Me.Height - 1290
    
End Sub

Private Sub tmrClose_Timer()

    If mintTimerCount > 0 Then
        Unload Me
    Else
        mintTimerCount = 1
    End If

End Sub

Private Sub txtOrderNumber_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        cmdSearch_Click
    End If
    
End Sub

'<CACH>****************************************************************************************
'* Sub:  Form_Load()
'**********************************************************************************************
'* Description: Determines what action to perform
'*              If no actions are given the gui interface is displayed
'**********************************************************************************************
'* Parameters:
'**********************************************************************************************
'* History:
'* 02/04/04    damians
'*             Header added.
'</CACH>***************************************************************************************
Private Sub Form_Load()
    
Const PROCEDURE_NAME As String = MODULE_NAME & ".Form_Load"

    On Error GoTo errHandler
    
    Call GetRoot

    Call InitialiseStatusBar(sbStatus)
    
    mlngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    mlngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    
    Call DebugMsg("frmOrder", "Form_Load", endlDebug, "Program Params - " & goSession.ProgramParams)
        
    'Check that a valid action has been given
    If Len(goSession.ProgramParams) >= 2 Then
        Select Case Left(goSession.ProgramParams, 3)
            Case ACTION_PRINT_INDIVIDUAL_ORDER   'Print Individual Order
                'Check that we have a Order No
                If Len(goSession.ProgramParams) > 4 Then
                    If Mid(goSession.ProgramParams, 4, 1) = "B" Then 'Both
                        Call PrintIndividCustomerOrder(Format(Right(goSession.ProgramParams, Len(goSession.ProgramParams) - 4), "000000"), CMB_PRINTTYPE_BOTH)
                    ElseIf Mid(goSession.ProgramParams, 4, 1) = "F" Then 'File
                        Call PrintIndividCustomerOrder(Format(Right(goSession.ProgramParams, Len(goSession.ProgramParams) - 4), "000000"), CMB_PRINTTYPE_FILE)
                    ElseIf Mid(goSession.ProgramParams, 4, 1) = "W" Then 'Warehouse
                        Call PrintIndividCustomerOrder(Format(Right(goSession.ProgramParams, Len(goSession.ProgramParams) - 4), "000000"), CMB_PRINTTYPE_WAREHOUSE)
                    End If
                End If
                
                Unload Me
                
                Exit Sub

            Case ACTION_PRINT_ALL_ORDERS    'Print All Orders
                Call PrintAllCustomerOrders(CMB_PRINTTYPE_BOTH)
                
                If Len(goSession.ProgramParams) = 4 Then
                    If Mid(goSession.ProgramParams, 4, 1) = "B" Then 'Both
                        Call PrintAllCustomerOrders(CMB_PRINTTYPE_BOTH)
                    ElseIf Mid(goSession.ProgramParams, 4, 1) = "F" Then 'File
                        Call PrintAllCustomerOrders(CMB_PRINTTYPE_FILE)
                    ElseIf Mid(goSession.ProgramParams, 4, 1) = "W" Then 'Warehouse
                        Call PrintAllCustomerOrders(CMB_PRINTTYPE_WAREHOUSE)
                    End If
                End If
                
                Unload Me
                
                Exit Sub
                
            Case ACTION_DISPLAY_INDIVIDUAL_ORDER ' Display Individual Order
                'Check that we have a Order No
                If Len(goSession.ProgramParams) > 3 Then
                    txtOrderNumber.Text = Right(goSession.ProgramParams, Len(goSession.ProgramParams) - 3)
                    
                    cmdSearch_Click
                    
                    cmbPrintType.ListIndex = 0
                    
                    cmdPreview_Click
                    
                    Unload Me
                    
                    Exit Sub
                End If
            
            Case Else
                'Show GUI
                
        End Select
    Else
        'Show GUI
    End If
    
    'Set the background colour
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    cmbPrintType.ListIndex = 0
    
    Me.WindowState = vbMaximized
    
    EnableCloseTimer
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    ResetCloseTimer

    Select Case Shift
        Case 0
            Select Case KeyCode
                Case vbKeyF3  'Clear has been pressed
                    cmdClear_Click
                
                Case vbKeyF7  'Close has been pressed
                    cmdSearch_Click
                                        
                Case vbKeyF9  'Print has been pressed
                    cmdPrint_Click
                    
                Case vbKeyF10  'Close has been pressed
                    cmdClose_Click
                    
            End Select
            
        Case 2 'Ctrl Key Press
            Select Case KeyCode
                Case vbKeyF8  'Preview has been pressed
                    cmdPreview_Click
            End Select
            
        Case 4
            Select Case KeyCode
                Case vbKeyF9  'Print all has been pressed
                    cmdPrintAll_Click
                    
            End Select
            
    End Select
    
End Sub

'<CACH>****************************************************************************************
'* Sub:  DoWHCopy()
'**********************************************************************************************
'* Description: Creates the Warehouse Copy of a Customer Order
'**********************************************************************************************
'* Parameters:
'* In strOrderNo : String - which customer order to print
'* In blnPrint   : Boolean - Whether to print or not
'**********************************************************************************************
'* History:
'* 04/10/02    damians
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DoWHCopy(ByVal strOrderNo As String, blnPrint As Boolean)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoWHCopy"

Dim oOrderItem      As Object
Dim oOrder          As Object
Dim oColOrderItem   As Collection
Dim oCol            As Collection
Dim intLoop         As Integer

    On Error GoTo errHandler
    
    Screen.MousePointer = vbHourglass
    
    'Clear out spread sheet
    sprdOrder.MaxCols = 0
    sprdOrder.MaxRows = 0
    
    'Create business object
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
    
    'Load order number filter
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, strOrderNo)
    
    'Load matches
    Set oCol = oOrder.IBo_LoadMatches()
    
    'Check we have one match
    If oCol.Count = 1 Then
        'Create business object
        Set oOrderItem = goDatabase.CreateBusinessObject(CLASSID_SOLINE)

        'Add Order Number Filter
        Call oOrderItem.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_OrderNo, strOrderNo)

        'Get Matches
        Set oColOrderItem = oOrderItem.IBo_LoadMatches()
        
        Call DoWHCopyHeader(oCol(1))
               
        Call DoWHCopyItems(oColOrderItem)
        
        Call DoWHCopyFooter(oCol(1))
        
        'Set printing options
        sprdOrder.PrintOrientation = PrintOrientationLandscape
        sprdOrder.PrintColHeaders = False
        sprdOrder.PrintRowHeaders = False
        sprdOrder.PrintSmartPrint = False
        sprdOrder.PrintGrid = False
        sprdOrder.PrintMarginTop = 500
        sprdOrder.PrintMarginLeft = 500
        sprdOrder.PrintMarginRight = 400
        sprdOrder.PrintMarginBottom = 500
        
        If blnPrint = True Then
            sprdOrder.PrintJobName = "Customer Order Reprint (" & strOrderNo & ")"
            
            Call sprdOrder.PrintSheet
            
            'Set the printed flag to true
            Call oOrder.RecordAsPrinted
        End If
    End If
      
    Screen.MousePointer = vbDefault
    
    Exit Sub
    
errHandler:

    Screen.MousePointer = vbDefault

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)

End Sub

Private Sub DoWHCopyItems(oCol As Collection)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoWHCopyItems"

Dim intLoop         As Integer
Dim oStockItem      As Object
Dim oStockItemCol   As Collection
Dim oPOrder         As Object
Dim oPOLine         As Object
Dim oSupplier       As Object

    On Error GoTo errHandler
    
    For intLoop = 1 To oCol.Count Step 1
        sprdOrder.MaxRows = sprdOrder.MaxRows + 1
        
        'Set Row Height
        sprdOrder.RowHeight(sprdOrder.MaxRows) = 24
        
        'Create description and notes columns
         
        Call sprdOrder.AddCellSpan(COL_ORDER_WAREHOUSE_DESCRIPTION1, sprdOrder.MaxRows, 2, 1)
    
        Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_NOTES1, sprdOrder.MaxRows, COL_ORDER_WAREHOUSE_NOTES2, sprdOrder.MaxRows, 15, -1, CellBorderStyleBlank)
        Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_NOTES1, sprdOrder.MaxRows, COL_ORDER_WAREHOUSE_NOTES2, sprdOrder.MaxRows, 16, -1, CellBorderStyleSolid)
        
        'Item Details
        Call PrintCellText(sprdOrder, intLoop, 11, False, COL_ORDER_WAREHOUSE_ITEMNO, sprdOrder.MaxRows)
        Call PrintCellText(sprdOrder, oCol(intLoop).PartCode, 11, False, COL_ORDER_WAREHOUSE_SKU, sprdOrder.MaxRows)
        Call PrintCellText(sprdOrder, oCol(intLoop).Description, 11, False, COL_ORDER_WAREHOUSE_DESCRIPTION1, sprdOrder.MaxRows)
        sprdOrder.CellType = CellTypeEdit
        sprdOrder.TypeMaxEditLen = 500
        sprdOrder.TypeEditMultiLine = True
        
        Call PrintCellText(sprdOrder, oCol(intLoop).OrderQuantity, 11, False, COL_ORDER_WAREHOUSE_QTY, sprdOrder.MaxRows)
        sprdOrder.CellType = CellTypeNumber
        sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
        
        'Add printed data
        Call PrintCellText(sprdOrder, "     /     /     ", 11, False, COL_ORDER_WAREHOUSE_DATEDUE, sprdOrder.MaxRows)
        Call PrintCellText(sprdOrder, "     /     /     ", 11, False, COL_ORDER_WAREHOUSE_DATEIN, sprdOrder.MaxRows)
        Call PrintCellText(sprdOrder, "     /     /     ", 11, False, COL_ORDER_WAREHOUSE_DATESOLD, sprdOrder.MaxRows)
        
        If (oCol(intLoop).Cancelled <> True) And (oCol(intLoop).Complete <> True) Then
            If oCol(intLoop).RaiseOrder = True Then
                If Val(oCol(intLoop).PurchaseOrderNo) > 0 Then
                    'Create Purchase Order Business
                    Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
                    
                    'Add fields to load
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_DueDate)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_ReceivedAll)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_CompletedDate)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_CancelledBy)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_Key)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_SupplierNo)
                    
                    'Add load filter
                    Call oPOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_OrderNumber, oCol(intLoop).PurchaseOrderNo)
                    
                    'Load Matches
                    Call oPOrder.IBo_LoadMatches
                    
                    sprdOrder.Col = COL_ORDER_WAREHOUSE_SOURCE
                    sprdOrder.Row = sprdOrder.MaxRows
                    sprdOrder.TypeEditMultiLine = True
                    
                    'If cancelled
                    If Val(oPOrder.CancelledBy) <> 0 Then
                        Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Cancelled", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
                    Else
                        If (oPOrder.ReceivedAll = True) Then
                            Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Comp(" & DisplayDate(oPOrder.CompletedDate, False) & ")", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
                            
                            Call PrintCellText(sprdOrder, DisplayDate(oPOrder.DueDate, False), 11, False, COL_ORDER_WAREHOUSE_DATEDUE, sprdOrder.MaxRows)
                            
                            Call PrintCellText(sprdOrder, DisplayDate(oPOrder.CompletedDate, False), 11, False, COL_ORDER_WAREHOUSE_DATEIN, sprdOrder.MaxRows)
                        Else
                            Set oPOLine = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDERLINE)
                            Call oPOLine.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDERLINE_PurchaseOrderNo, oPOrder.Key)
                            Call oPOLine.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDERLINE_PartCode, oCol(intLoop).PartCode)
                            Call oPOLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_PURCHASEORDERLINE_LineNo, 0)
                            Call oPOLine.IBo_AddLoadField(FID_PURCHASEORDERLINE_QuantityReceived)
                            Call oPOLine.IBo_AddLoadField(FID_PURCHASEORDERLINE_OrderQuantity)
                            Call oPOLine.IBo_AddLoadField(FID_PURCHASEORDERLINE_DateLastReceipt)
                            Call oPOLine.IBo_LoadMatches
                                                       
                            If oPOLine.QuantityReceived >= oPOLine.OrderQuantity Then
                                Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Received Line", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
                                
                                Call PrintCellText(sprdOrder, DisplayDate(oPOrder.DueDate, False), 11, False, COL_ORDER_WAREHOUSE_DATEDUE, sprdOrder.MaxRows)
                                
                                Call PrintCellText(sprdOrder, DisplayDate(oPOLine.DateLastReceipt, False), 11, False, COL_ORDER_WAREHOUSE_DATEIN, sprdOrder.MaxRows)
                            Else
                                'If overdue
                                If (oPOrder.DueDate >= Now()) Then
                                    Set oSupplier = goDatabase.CreateBusinessObject(CLASSID_SUPPLIER)
                                    Call oSupplier.IBo_AddLoadFilter(CMP_EQUAL, FID_SUPPLIER_SupplierNo, oPOrder.SupplierNo)
                                    Call oSupplier.IBo_AddLoadField(FID_SUPPLIER_NAME)
                                    Call oSupplier.IBo_LoadMatches

                                    Call PrintCellText(sprdOrder, oSupplier.Name, 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
                                    
                                    Call PrintCellText(sprdOrder, DisplayDate(oPOrder.DueDate, False), 11, False, COL_ORDER_WAREHOUSE_DATEDUE, sprdOrder.MaxRows)
                                Else
                                    Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Overdue", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
                                End If
                            End If
                        End If
                    End If
                Else
                    Call PrintCellText(sprdOrder, "To Order", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
                End If
            Else
                Call PrintCellText(sprdOrder, "Ex Stock", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
            End If
        End If
        
        'If cancelled
        If oCol(intLoop).Cancelled = True Then
            sprdOrder.Col = COL_ORDER_WAREHOUSE_SOURCE
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.TypeEditMultiLine = True
            
            Call PrintCellText(sprdOrder, "Cancelled " & oCol(intLoop).CancellationReason, 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
            
            'Strike thru text
            sprdOrder.Col = COL_ORDER_WAREHOUSE_SKU
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DESCRIPTION1
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DESCRIPTION2
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_SIZE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_QTY
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DATEDUE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DATEIN
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DATESOLD
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_NOTES1
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_NOTES2
            sprdOrder.FontStrikethru = True
        End If
        
        'If complete
        If (oCol(intLoop).Complete = True) And (oCol(intLoop).Cancelled = False) Then
            sprdOrder.Col = COL_ORDER_WAREHOUSE_SOURCE
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.TypeEditMultiLine = True
            
            Call PrintCellText(sprdOrder, "Sold", 11, False, COL_ORDER_WAREHOUSE_SOURCE, sprdOrder.MaxRows)
            
            'Strike thru text
            sprdOrder.Col = COL_ORDER_WAREHOUSE_SKU
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DESCRIPTION1
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DESCRIPTION2
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_SIZE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_QTY
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DATEDUE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DATEIN
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_DATESOLD
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_NOTES1
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_WAREHOUSE_NOTES2
            sprdOrder.FontStrikethru = True
        End If
        
        'Load Inventory BO
        Set oStockItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        
        'Load Required Fields
        Call oStockItem.IBo_AddLoadField(FID_INVENTORY_SizeDescription)

        'Add PartCode Filter
        Call oStockItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oCol(intLoop).PartCode)
        
        'Load Matches
        Set oStockItemCol = oStockItem.IBo_LoadMatches()
        
        'Check we have one match
        If oStockItemCol.Count = 1 Then
            'If the size is blank put some spaces in to stop the cell from being overwritten
            If oStockItemCol(1).SizeDescription <> "" Then
                Call PrintCellText(sprdOrder, oStockItemCol(1).SizeDescription, 11, False, COL_ORDER_WAREHOUSE_SIZE, sprdOrder.MaxRows)
            Else
                Call PrintCellText(sprdOrder, "   ", 11, False, COL_ORDER_WAREHOUSE_SIZE, sprdOrder.MaxRows)
            End If
        Else
            Call PrintCellText(sprdOrder, "   ", 11, False, COL_ORDER_WAREHOUSE_SIZE, sprdOrder.MaxRows)
        End If
        
        sprdOrder.TypeEditMultiLine = False
    Next
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

'<CACH>****************************************************************************************
'* Sub:  DoFCopy()
'**********************************************************************************************
'* Description: Creates the File Copy of a Customer Order
'**********************************************************************************************
'* Parameters:
'* In strOrderNo : String - which customer order to print
'* In blnPrint   : Boolean - Whether to print or not
'**********************************************************************************************
'* History:
'* 04/10/02    damians
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DoFCopy(strOrderNo As String, blnPrint As Boolean)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoFCopy"

Dim oOrderItem      As Object
Dim oOrder          As Object
Dim oCol            As Collection
Dim oOrderItemCol   As Collection
Dim intLoop         As Integer

    On Error GoTo errHandler
    
    Screen.MousePointer = vbHourglass
    
    'Clear out spread sheet
    sprdOrder.MaxCols = 0
    sprdOrder.MaxRows = 0
    
    'Create business object
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
    
    'Load order number filter
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, strOrderNo)
    
    'Load Matches
    Set oCol = oOrder.IBo_LoadMatches()
    
    'check we have one match
    If oCol.Count = 1 Then
        'Create business object
        Set oOrderItem = goDatabase.CreateBusinessObject(CLASSID_SOLINE)
        
        'Add Order Number Filter
        Call oOrderItem.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_OrderNo, strOrderNo)

        'Get Matches
        Set oOrderItemCol = oOrderItem.IBo_LoadMatches()
        
        Call DoFCopyHeader(oCol(1))
               
        Call DoFCopyItems(oOrderItemCol)

        Call DoFCopyFooter(oCol(1))
        
        'Set printing options
        sprdOrder.PrintOrientation = PrintOrientationPortrait
        sprdOrder.PrintColHeaders = False
        sprdOrder.PrintRowHeaders = False
        sprdOrder.PrintSmartPrint = False
        sprdOrder.PrintGrid = False
        sprdOrder.PrintMarginTop = 500
        sprdOrder.PrintMarginLeft = 500
        sprdOrder.PrintMarginRight = 400
        sprdOrder.PrintMarginBottom = 500
        
        If blnPrint = True Then
            sprdOrder.PrintJobName = "Customer Order Reprint (" & strOrderNo & ")"
            
            Call sprdOrder.PrintSheet
            
            'Set the printed flag to true
            Call oOrder.RecordAsPrinted
        End If
    End If
    
    Screen.MousePointer = vbDefault
            
    Exit Sub
    
errHandler:

    Screen.MousePointer = vbDefault

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)

End Sub

Private Sub DoFCopyHeader(oOrder As Object)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoFCopyHeader"
    
    On Error GoTo errHandler
    
    sprdOrder.MaxCols = ORDER_FILE_MAX_COLUMNS
    sprdOrder.MaxRows = ORDER_FILE_HEADER_ROWS
    
    'Set Column Widths
    sprdOrder.ColWidth(COL_ORDER_FILE_ITEMNO) = 4
    sprdOrder.ColWidth(COL_ORDER_FILE_SKU) = 9
    sprdOrder.ColWidth(COL_ORDER_FILE_DESCRIPTION1) = 6
    sprdOrder.ColWidth(COL_ORDER_FILE_DESCRIPTION2) = 15
    sprdOrder.ColWidth(COL_ORDER_FILE_SIZE) = 10
    sprdOrder.ColWidth(COL_ORDER_FILE_QTY) = 6
    sprdOrder.ColWidth(COL_ORDER_FILE_RATE) = 8
    sprdOrder.ColWidth(COL_ORDER_FILE_TOTAL) = 8
    sprdOrder.ColWidth(COL_ORDER_FILE_STATUS) = 17
    
    'Set Row Heights
    sprdOrder.RowHeight(1) = 15
    sprdOrder.RowHeight(3) = 16
    sprdOrder.RowHeight(4) = 16
    
    'Create Header Borders
    Call sprdOrder.SetCellBorder(COL_ORDER_FILE_ITEMNO, 1, ORDER_FILE_MAX_COLUMNS, ORDER_FILE_HEADER_ROWS - 1, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_FILE_ITEMNO, 2, ORDER_FILE_MAX_COLUMNS, ORDER_FILE_HEADER_ROWS - 1, 16, -1, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_ORDER_FILE_DESCRIPTION1, ORDER_FILE_HEADER_ROWS, COL_ORDER_FILE_DESCRIPTION2, ORDER_FILE_HEADER_ROWS, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_FILE_DESCRIPTION1, ORDER_FILE_HEADER_ROWS, COL_ORDER_FILE_DESCRIPTION2, ORDER_FILE_HEADER_ROWS, 16, -1, CellBorderStyleSolid)
    
    'Header Text
    Call PrintCellText(sprdOrder, "Order No:", 10, False, COL_ORDER_FILE_SKU, 2)
    Call PrintCellText(sprdOrder, "Order Date:", 10, False, COL_ORDER_FILE_DESCRIPTION2, 2)
    Call PrintCellText(sprdOrder, "Customer Name:", 10, False, COL_ORDER_FILE_SIZE, 2)
    Call PrintCellText(sprdOrder, "Telephone:", 10, False, COL_ORDER_FILE_TOTAL, 2)
    Call PrintCellText(sprdOrder, "Address:", 10, False, COL_ORDER_FILE_SKU, 5)
    Call PrintCellText(sprdOrder, "Order Total:", 10, False, COL_ORDER_FILE_QTY, 6)
    Call PrintCellText(sprdOrder, "Balance To Sell:", 10, False, COL_ORDER_FILE_QTY, 7)
    Call PrintCellText(sprdOrder, "Deposit Taken:", 10, False, COL_ORDER_FILE_QTY, 8)
    Call PrintCellText(sprdOrder, "To Pay:", 10, False, COL_ORDER_FILE_QTY, 9)
    sprdOrder.FontBold = True
    
    'Title
    Call sprdOrder.AddCellSpan(1, 1, ORDER_FILE_MAX_COLUMNS, 1)
    Call PrintCellText(sprdOrder, "Customer Order - File Copy", 11, False, COL_ORDER_FILE_ITEMNO, 1)
    sprdOrder.TypeHAlign = TypeHAlignCenter
    
    'Order Details
    Call PrintCellText(sprdOrder, oOrder.OrderNo, 16, False, COL_ORDER_FILE_SKU, 3)
    Call PrintCellText(sprdOrder, DisplayDate(oOrder.OrderDate, False), 16, False, COL_ORDER_FILE_DESCRIPTION2, 3)
    Call PrintCellText(sprdOrder, oOrder.Name, 16, False, COL_ORDER_FILE_SIZE, 3)
    Call PrintCellText(sprdOrder, oOrder.TelephoneNo, 14, False, COL_ORDER_FILE_TOTAL, 3)
    Call PrintCellText(sprdOrder, oOrder.SecondTelNo, 14, False, COL_ORDER_FILE_TOTAL, 4)
    Call PrintCellText(sprdOrder, oOrder.AddressLine1, 11, False, COL_ORDER_FILE_SKU, 6)
    Call PrintCellText(sprdOrder, oOrder.AddressLine2, 11, False, COL_ORDER_FILE_SKU, 7)
    Call PrintCellText(sprdOrder, oOrder.AddressLine3, 11, False, COL_ORDER_FILE_SKU, 8)
    Call PrintCellText(sprdOrder, oOrder.PostCode, 11, False, COL_ORDER_FILE_SKU, 9)
        
    Call PrintCellText(sprdOrder, oOrder.OrderValue, 11, False, COL_ORDER_FILE_TOTAL, 6)
    sprdOrder.CellType = CellTypeCurrency
    
    
    Call PrintCellText(sprdOrder, oOrder.OutstandingValue, 11, False, COL_ORDER_FILE_TOTAL, 7)
    sprdOrder.CellType = CellTypeCurrency
    sprdOrder.TypeCurrencyDecPlaces = mlngValueDecNum
    
    Call PrintCellText(sprdOrder, oOrder.DepositValue, 11, False, COL_ORDER_FILE_TOTAL, 8)
    sprdOrder.CellType = CellTypeCurrency
    sprdOrder.TypeCurrencyDecPlaces = mlngValueDecNum
    
    Call PrintCellText(sprdOrder, oOrder.OutstandingValue - oOrder.DepositValue, 11, False, COL_ORDER_FILE_TOTAL, 9)
    sprdOrder.CellType = CellTypeCurrency
    sprdOrder.TypeCurrencyDecPlaces = mlngValueDecNum
    
    sprdOrder.FontBold = True
    
    'Item Headers
    Call PrintCellText(sprdOrder, "SKU", 11, False, COL_ORDER_FILE_SKU, ORDER_FILE_HEADER_ROWS)
    Call PrintCellText(sprdOrder, "Description", 11, False, COL_ORDER_FILE_DESCRIPTION1, ORDER_FILE_HEADER_ROWS)
    Call PrintCellText(sprdOrder, "Size", 11, False, COL_ORDER_FILE_SIZE, ORDER_FILE_HEADER_ROWS)
    Call PrintCellText(sprdOrder, "Qty", 11, False, COL_ORDER_FILE_QTY, ORDER_FILE_HEADER_ROWS)
    Call PrintCellText(sprdOrder, "Rate", 11, False, COL_ORDER_FILE_RATE, ORDER_FILE_HEADER_ROWS)
    Call PrintCellText(sprdOrder, "Total", 11, False, COL_ORDER_FILE_TOTAL, ORDER_FILE_HEADER_ROWS)
    Call PrintCellText(sprdOrder, "Status", 11, False, COL_ORDER_FILE_STATUS, ORDER_FILE_HEADER_ROWS)
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub DoFCopyItems(oCol As Collection)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoFCopyItems"

Dim intLoop         As Integer
Dim oStockItem      As Object
Dim oStockItemCol   As Object
Dim oPOrder         As Object
Dim oPOLine         As Object

    On Error GoTo errHandler
    
    For intLoop = 1 To oCol.Count Step 1
        sprdOrder.MaxRows = sprdOrder.MaxRows + 1
        
        'Set Row Height
        sprdOrder.RowHeight(sprdOrder.MaxRows) = 24
        
        'Create description
        Call sprdOrder.AddCellSpan(COL_ORDER_FILE_DESCRIPTION1, sprdOrder.MaxRows, 2, 1)
                    
        'Item Details
        Call PrintCellText(sprdOrder, intLoop, 11, False, COL_ORDER_FILE_ITEMNO, sprdOrder.MaxRows)
        Call PrintCellText(sprdOrder, oCol(intLoop).PartCode, 11, False, COL_ORDER_FILE_SKU, sprdOrder.MaxRows)
        
        Call PrintCellText(sprdOrder, oCol(intLoop).Description, 11, False, COL_ORDER_FILE_DESCRIPTION1, sprdOrder.MaxRows)
        sprdOrder.CellType = CellTypeEdit
        sprdOrder.TypeMaxEditLen = 500
        sprdOrder.TypeEditMultiLine = True
        
        Call PrintCellText(sprdOrder, oCol(intLoop).OrderQuantity, 11, False, COL_ORDER_FILE_QTY, sprdOrder.MaxRows)
        sprdOrder.CellType = CellTypeNumber
        sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
        
        Call PrintCellText(sprdOrder, oCol(intLoop).OrderPrice, 11, False, COL_ORDER_FILE_RATE, sprdOrder.MaxRows)
        sprdOrder.CellType = CellTypeNumber
        sprdOrder.TypeNumberDecPlaces = mlngValueDecNum
        
        Call PrintCellText(sprdOrder, oCol(intLoop).OrderPrice * oCol(intLoop).OrderQuantity, 11, False, COL_ORDER_FILE_TOTAL, sprdOrder.MaxRows)
        sprdOrder.CellType = CellTypeNumber
        sprdOrder.TypeNumberDecPlaces = mlngValueDecNum
        
        sprdOrder.Col = COL_ORDER_FILE_STATUS
        sprdOrder.Row = sprdOrder.MaxRows
        sprdOrder.TypeEditMultiLine = True
        
        'If the order has not been cancelled or completed
        If (oCol(intLoop).Cancelled <> True) And (oCol(intLoop).Complete <> True) Then
            If oCol(intLoop).RaiseOrder = True Then
                If Val(oCol(intLoop).PurchaseOrderNo) > 0 Then
                    'Create Purchase Order Business
                    Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
                    
                    'Add fields to load
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_DueDate)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_ReceivedAll)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_CompletedDate)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_CancelledBy)
                    Call oPOrder.IBo_AddLoadField(FID_PURCHASEORDER_Key)
                    
                    'Add load filter
                    Call oPOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_OrderNumber, oCol(intLoop).PurchaseOrderNo)
                    
                    'Load Matches
                    Call oPOrder.IBo_LoadMatches
                    
                    sprdOrder.Col = COL_ORDER_FILE_STATUS
                    sprdOrder.Row = sprdOrder.MaxRows
                    sprdOrder.TypeEditMultiLine = True
                    
                    'If cancelled
                    If Val(oPOrder.CancelledBy) <> 0 Then
                        Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Cancelled", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
                    Else
                        If (oPOrder.ReceivedAll = True) Then
                            Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Comp(" & DisplayDate(oPOrder.CompletedDate, False) & ")", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
                        Else
                            Set oPOLine = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDERLINE)
                            Call oPOLine.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDERLINE_PurchaseOrderNo, oPOrder.Key)
                            Call oPOLine.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDERLINE_PartCode, oCol(intLoop).PartCode)
                            Call oPOLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_PURCHASEORDERLINE_LineNo, 0)
                            Call oPOLine.IBo_AddLoadField(FID_PURCHASEORDERLINE_QuantityReceived)
                            Call oPOLine.IBo_AddLoadField(FID_PURCHASEORDERLINE_OrderQuantity)
                            Call oPOLine.IBo_AddLoadField(FID_PURCHASEORDERLINE_DateLastReceipt)
                            Call oPOLine.IBo_LoadMatches
                            
                            If oPOLine.QuantityReceived >= oPOLine.OrderQuantity Then
                                Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Received Line", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
                            Else
                                'If overdue
                                If (oPOrder.DueDate >= Now()) Then
                                    Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Due(" & DisplayDate(oPOrder.DueDate, False) & ")", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
                                Else
                                    Call PrintCellText(sprdOrder, "PO: " & oPOrder.OrderNumber & vbCrLf & "Overdue", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
                                End If
                            End If
                        End If
                    End If
                Else
                    Call PrintCellText(sprdOrder, "To Order", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
                End If
            Else
                Call PrintCellText(sprdOrder, "Ex Stock", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
            End If
        End If
        
        'If cancelled
        If oCol(intLoop).Cancelled = True Then
            sprdOrder.Col = COL_ORDER_FILE_STATUS
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.TypeEditMultiLine = True
            
            Call PrintCellText(sprdOrder, "Cancelled " & oCol(intLoop).CancellationReason, 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
            
            'Strike thru text
            sprdOrder.Col = COL_ORDER_FILE_SKU
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_DESCRIPTION1
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_DESCRIPTION2
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_SIZE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_QTY
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_RATE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_TOTAL
            sprdOrder.FontStrikethru = True
        End If
        
        'If complete
        If (oCol(intLoop).Complete = True) And (oCol(intLoop).Cancelled = False) Then
            sprdOrder.Col = COL_ORDER_FILE_STATUS
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.TypeEditMultiLine = True
            
            Call PrintCellText(sprdOrder, "Sold", 11, False, COL_ORDER_FILE_STATUS, sprdOrder.MaxRows)
            
            'Strike thru text
            sprdOrder.Col = COL_ORDER_FILE_SKU
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_DESCRIPTION1
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_DESCRIPTION2
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_SIZE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_QTY
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_RATE
            sprdOrder.FontStrikethru = True
            sprdOrder.Col = COL_ORDER_FILE_TOTAL
            sprdOrder.FontStrikethru = True
        End If
        
        'Load Inventory BO
        Set oStockItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        
        'Load Required Fields
        Call oStockItem.IBo_AddLoadField(FID_INVENTORY_SizeDescription)

        'Add PartCode Filter
        Call oStockItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oCol(intLoop).PartCode)
        
        'Load Matches
        Set oStockItemCol = oStockItem.IBo_LoadMatches()
        
        'Check we have one result
        If oStockItemCol.Count = 1 Then
            'If the size is blank put some spaces in to stop the cell from being overwritten
            If oStockItemCol(1).SizeDescription <> "" Then
                Call PrintCellText(sprdOrder, oStockItemCol(1).SizeDescription, 11, False, COL_ORDER_FILE_SIZE, sprdOrder.MaxRows)
            Else
                Call PrintCellText(sprdOrder, "   ", 11, False, COL_ORDER_FILE_SIZE, sprdOrder.MaxRows)
            End If
        Else
            Call PrintCellText(sprdOrder, "   ", 11, False, COL_ORDER_FILE_SIZE, sprdOrder.MaxRows)
        End If
        
        sprdOrder.TypeEditMultiLine = False
    Next
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
End Sub

Private Sub DoFCopyFooter(oOrder As Object)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoFCopyFooter"
    
    On Error GoTo errHandler
    
    sprdOrder.MaxRows = sprdOrder.MaxRows + 2
    
    'Create footer borders
    Call sprdOrder.SetCellBorder(COL_ORDER_FILE_ITEMNO, sprdOrder.MaxRows - 1, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_FILE_ITEMNO, sprdOrder.MaxRows - 1, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, 16, -1, CellBorderStyleSolid)
    
    Call sprdOrder.AddCellSpan(COL_ORDER_FILE_ITEMNO, sprdOrder.MaxRows, sprdOrder.MaxCols, 1)
        
    'Set Row Heights
    sprdOrder.RowHeight(sprdOrder.MaxRows - 1) = 21
    sprdOrder.RowHeight(sprdOrder.MaxRows) = 200
    
    'Footer Text
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Col = COL_ORDER_FILE_ITEMNO
    sprdOrder.CellType = CellTypeEdit
    sprdOrder.TypeMaxEditLen = 500
    sprdOrder.TypeEditMultiLine = True
    
    Call PrintCellText(sprdOrder, "Notes: " & vbCrLf & oOrder.Remarks, 11, False, COL_ORDER_FILE_ITEMNO, sprdOrder.MaxRows)
        
    sprdOrder.PrintFooter = GetFooter
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)

End Sub

Private Sub DoWHCopyHeader(oOrder As Object)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoWHCopyHeader"
    
    On Error GoTo errHandler
    
    sprdOrder.MaxCols = ORDER_WAREHOUSE_MAX_COLUMNS
    sprdOrder.MaxRows = ORDER_WAREHOUSE_HEADER_ROWS
    
    'Set Column Widths
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_ITEMNO) = 4
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_SKU) = 8
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_DESCRIPTION1) = 9
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_DESCRIPTION2) = 17
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_SIZE) = 10
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_QTY) = 6
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_DATEDUE) = 10
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_DATEIN) = 10
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_DATESOLD) = 10
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_NOTES1) = 21
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_NOTES2) = 6
    sprdOrder.ColWidth(COL_ORDER_WAREHOUSE_SOURCE) = 15
    
    'Set Row Heights
    sprdOrder.RowHeight(1) = 15
    sprdOrder.RowHeight(3) = 21
    
    'Create Header borders
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_ITEMNO, 1, ORDER_WAREHOUSE_MAX_COLUMNS, 1, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_ITEMNO, 2, ORDER_WAREHOUSE_MAX_COLUMNS, 3, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_ITEMNO, 2, COL_ORDER_WAREHOUSE_DESCRIPTION1, 3, 16, -1, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_DESCRIPTION2, 2, COL_ORDER_WAREHOUSE_DESCRIPTION2, 3, 16, -1, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_SIZE, 2, COL_ORDER_WAREHOUSE_DATESOLD, 3, 16, -1, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_NOTES1, 2, COL_ORDER_WAREHOUSE_NOTES1, 3, 16, -1, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_NOTES2, 2, COL_ORDER_WAREHOUSE_SOURCE, 3, 16, -1, CellBorderStyleSolid)
    
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_DESCRIPTION1, 4, COL_ORDER_WAREHOUSE_DESCRIPTION2, 4, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_DESCRIPTION1, 4, COL_ORDER_WAREHOUSE_DESCRIPTION2, 4, 16, -1, CellBorderStyleSolid)
    
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_NOTES1, 4, COL_ORDER_WAREHOUSE_NOTES2, 4, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_NOTES1, 4, COL_ORDER_WAREHOUSE_NOTES2, 4, 16, -1, CellBorderStyleSolid)
    
    'Header Text
    Call PrintCellText(sprdOrder, "Order No:", 11, False, COL_ORDER_WAREHOUSE_ITEMNO, 2)
    Call PrintCellText(sprdOrder, "Order Date:", 11, False, COL_ORDER_WAREHOUSE_DESCRIPTION2, 2)
    Call PrintCellText(sprdOrder, "Customer Name:", 11, False, COL_ORDER_WAREHOUSE_SIZE, 2)
    Call PrintCellText(sprdOrder, "Tel 1:", 11, False, COL_ORDER_WAREHOUSE_NOTES1, 2)
    Call PrintCellText(sprdOrder, "Tel 2:", 11, False, COL_ORDER_WAREHOUSE_NOTES2, 2)
    
    'Title
    Call sprdOrder.AddCellSpan(1, 1, ORDER_WAREHOUSE_MAX_COLUMNS, 1)
    Call PrintCellText(sprdOrder, "Customer Order Slip: Stock Reserved For This Order", 11, False, COL_ORDER_FILE_ITEMNO, 1)
    sprdOrder.TypeHAlign = TypeHAlignCenter
 
    'Item Headers
    Call PrintCellText(sprdOrder, "SKU", 11, False, COL_ORDER_WAREHOUSE_SKU, 4)
    Call PrintCellText(sprdOrder, "Description", 11, False, COL_ORDER_WAREHOUSE_DESCRIPTION1, 4)
    Call PrintCellText(sprdOrder, "Size", 11, False, COL_ORDER_WAREHOUSE_SIZE, 4)
    Call PrintCellText(sprdOrder, "Qty", 11, False, COL_ORDER_WAREHOUSE_QTY, 4)
    Call PrintCellText(sprdOrder, "Date Due", 11, False, COL_ORDER_WAREHOUSE_DATEDUE, 4)
    Call PrintCellText(sprdOrder, "Date In", 11, False, COL_ORDER_WAREHOUSE_DATEIN, 4)
    Call PrintCellText(sprdOrder, "Date Sold", 11, False, COL_ORDER_WAREHOUSE_DATESOLD, 4)
    Call PrintCellText(sprdOrder, "Notes", 11, False, COL_ORDER_WAREHOUSE_NOTES1, 4)
    Call PrintCellText(sprdOrder, "Source", 11, False, COL_ORDER_WAREHOUSE_SOURCE, 4)
    
    'Order Details
    Call PrintCellText(sprdOrder, oOrder.OrderNo, 22, False, COL_ORDER_WAREHOUSE_ITEMNO, 3)
    Call PrintCellText(sprdOrder, DisplayDate(oOrder.OrderDate, False), 22, False, COL_ORDER_WAREHOUSE_DESCRIPTION2, 3)
    Call PrintCellText(sprdOrder, oOrder.Name, 22, False, COL_ORDER_WAREHOUSE_SIZE, 3)
    Call PrintCellText(sprdOrder, oOrder.TelephoneNo, 14, False, COL_ORDER_WAREHOUSE_NOTES1, 3)
    Call PrintCellText(sprdOrder, oOrder.SecondTelNo, 14, False, COL_ORDER_WAREHOUSE_NOTES2, 3)

    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub DoWHCopyFooter(oOrder As Object)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoWHCopyFooter"
    
    On Error GoTo errHandler
    
    sprdOrder.MaxRows = sprdOrder.MaxRows + 2
    
    'Create footer borders
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_ITEMNO, sprdOrder.MaxRows - 1, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, 15, -1, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_ORDER_WAREHOUSE_ITEMNO, sprdOrder.MaxRows - 1, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, 16, -1, CellBorderStyleSolid)
    
    'Add cell span
    Call sprdOrder.AddCellSpan(COL_ORDER_WAREHOUSE_ITEMNO, sprdOrder.MaxRows, sprdOrder.MaxCols, 1)
        
    'Set Row Heights
    sprdOrder.RowHeight(sprdOrder.MaxRows - 1) = 21
    sprdOrder.RowHeight(sprdOrder.MaxRows) = 120
    
    'Footer Text
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Col = COL_ORDER_WAREHOUSE_ITEMNO
    sprdOrder.CellType = CellTypeEdit
    sprdOrder.TypeMaxEditLen = 500
    sprdOrder.TypeEditMultiLine = True
    
    Call PrintCellText(sprdOrder, "Notes: " & vbCrLf & oOrder.Remarks, 11, False, COL_ORDER_WAREHOUSE_ITEMNO, sprdOrder.MaxRows)
        
    sprdOrder.PrintFooter = GetFooter
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)

End Sub

Private Sub PrintCellText(ByRef sprdSpread As vaSpread, ByVal strText As String, _
                        ByVal intFontSize As Integer, ByVal blnFontBold As Boolean, _
                        lngCol As Long, lngRow As Long)
    
    sprdSpread.Col = lngCol
    sprdSpread.Row = lngRow
    
    sprdSpread.Text = strText
    
    sprdSpread.FontSize = intFontSize
    sprdSpread.FontBold = blnFontBold
    
End Sub

Private Sub cmdClose_Click()

    Unload Me

End Sub

Private Sub cmdClose_LostFocus()
    
    cmdClose.FontBold = False
    
End Sub

Private Sub cmdClose_GotFocus()

    cmdClose.FontBold = True
    
End Sub

Private Sub cmdSearch_GotFocus()

    cmdSearch.FontBold = True
    
End Sub

Private Sub cmdSearch_LostFocus()

    cmdSearch.FontBold = False
    
End Sub

Private Sub cmdPrint_GotFocus()

    cmdPrint.FontBold = True
    
End Sub

Private Sub cmdPrint_LostFocus()

    cmdPrint.FontBold = False
    
End Sub

Private Sub cmdPrintAll_GotFocus()

    cmdPrintAll.FontBold = True
    
End Sub

Private Sub cmdPrintAll_LostFocus()

    cmdPrintAll.FontBold = False
    
End Sub
