VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmWINORD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "WINORD - Auto SOQ Program Running"
   ClientHeight    =   2100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2100
   ScaleWidth      =   4710
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   2505
      TabIndex        =   10
      Top             =   1560
      Width           =   1215
   End
   Begin VB.PictureBox picRed 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   660
      Left            =   5055
      Picture         =   "frmWINORD.frx":0000
      ScaleHeight     =   660
      ScaleWidth      =   645
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.PictureBox PicGreen 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   660
      Left            =   5040
      Picture         =   "frmWINORD.frx":185A
      ScaleHeight     =   660
      ScaleWidth      =   660
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1560
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.PictureBox picYellow 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   600
      Left            =   5070
      Picture         =   "frmWINORD.frx":30B4
      ScaleHeight     =   600
      ScaleWidth      =   600
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   840
      Width           =   600
   End
   Begin VB.Timer Timer1 
      Interval        =   50000
      Left            =   3720
      Top             =   1560
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4200
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&k"
      Height          =   495
      Left            =   990
      TabIndex        =   6
      Top             =   1560
      Width           =   1215
   End
   Begin VB.CommandButton SearchPIF 
      Caption         =   "Search &PIF"
      Height          =   375
      Left            =   3360
      TabIndex        =   5
      Top             =   1080
      Width           =   1095
   End
   Begin VB.TextBox txtWinordbat 
      Height          =   405
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Text            =   "Text1"
      Top             =   1080
      Width           =   3135
   End
   Begin VB.CommandButton SearchTXT 
      Caption         =   "Search &TXT"
      Height          =   375
      Left            =   3360
      TabIndex        =   2
      Top             =   240
      Width           =   1095
   End
   Begin VB.TextBox txtWinordtxt 
      Height          =   405
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   0
      TabStop         =   0   'False
      Text            =   "Text1"
      Top             =   240
      Width           =   3135
   End
   Begin VB.Line Line1 
      BorderWidth     =   3
      X1              =   0
      X2              =   4680
      Y1              =   720
      Y2              =   720
   End
   Begin VB.Label Label2 
      Caption         =   "Location of WINORD.BAT"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   840
      Width           =   2775
   End
   Begin VB.Label Label1 
      Caption         =   "Location of WINORD.TXT:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   2775
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Visible         =   0   'False
      Begin VB.Menu mnuView 
         Caption         =   "View"
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "frmWINORD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'1.02 - 27.08.2002 - AB - Verbal referral - Make form visible
'1.01 - 27.08.2002 - AB - Referral WIXCTS.287
'                       - Location of WINORD.TXT and WINORD.BAT not editable.
'1.00 - 23.08.2002 - AB - BSD WIX1054 Initial creation.
Option Explicit

Private Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Private Declare Function ModifyMenu Lib "user32" Alias "ModifyMenuA" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpString As Any) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long
Const MF_BYCOMMAND = &H0&
Const MF_GRAYED = &H1&
Const SC_CLOSE = &HF060&
Const PROCESS_QUERY_INFORMATION = &H400
Private mintfileNum As Integer
Private mdateActivateTime As Date
Private mstrReturned As String
Private strFirstLine() As String
Private mblnbusy As Boolean
Public gblnExit As Boolean

Private Sub cmdExit_Click()
    If AllowExit Then
        Unload Me
    End If
End Sub

'Private Sub cmdOK_Click()
'    If Dir(TextDirectory & "winord.txt") = "" Then
'        MsgBox "File: " & TextDirectory & "winord.txt" & " NOT FOUND.", vbCritical
'        Exit Sub
'    End If
'    If Dir(BatchDirectory & "winord.pif") = "" Then
'        MsgBox "File: " & TextDirectory & "winord.pif" & " NOT FOUND.", vbCritical
'        Exit Sub
'    End If
'    Me.Visible = False
'    mnuView.Enabled = True
'End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
    'disable system menu close
    Dim hMenu&
    Dim wFlags&
    Dim Success&
    hMenu& = GetSystemMenu(Me.hwnd, 0)
    wFlags& = MF_BYCOMMAND Or MF_GRAYED
    Success& = ModifyMenu(hMenu&, SC_CLOSE, wFlags&, -10, "Close")
    
    'Allow only one instance of the application to run
    If App.PrevInstance = True Then
        End
    End If
    
    'Defualt directories
    txtWinordtxt.Text = TextDirectory & "winord.txt"
    txtWinordbat.Text = BatchDirectory & "winord.pif"
    
    If Dir(TextDirectory & "winord.txt") = "" Then
        MsgBox "File: " & TextDirectory & "winord.txt" & " NOT FOUND. Application STOPPED.", vbCritical
        End
    End If
    
    mintfileNum = FreeFile()
    Open txtWinordtxt.Text For Input As mintfileNum
    Input #mintfileNum, mstrReturned
    Debug.Print mstrReturned
    Close mintfileNum
    
    strFirstLine = Split(mstrReturned, ";")
    mdateActivateTime = Format(Trim(strFirstLine(1)), "hh:mm")
    Debug.Print CDate(mdateActivateTime)
    
    AddIcon PicGreen, "WINORD - Active"
    
    Call RunTheBatch
End Sub
Public Sub RunTheBatch()
    Do
        If mdateActivateTime = Format(Time, "hh:mm") Then
        
            mnuExit.Enabled = False
            ChangeIcon picRed, "WINORD - Running"
            Call RunShell(txtWinordbat.Text)
            mnuExit.Enabled = True
            ChangeIcon PicGreen, "WINORD - Active"
        
        End If
        Exit Do
    Loop
End Sub
Public Sub RunShell(cmdline$)
Dim hProcess As Long
Dim ProcessId As Long
Dim exitCode As Long
Dim r As Long
Dim MyModal As Boolean
Dim MyRet As Long

    DoEvents
    
    ProcessId& = Shell(cmdline$, 1)
    hProcess& = OpenProcess(PROCESS_QUERY_INFORMATION, False, ProcessId&)
    Do
        Call GetExitCodeProcess(hProcess&, exitCode&)
        DoEvents
    Loop While exitCode& > 0
    r = CloseHandle(hProcess)
    
    'On Error Resume Next
    'AppActivate ProcessId&, False
    'SendKeys "^c", True  'key press CTL+c
    
    frmMessage.Show vbModal
End Sub

Private Sub mnuExit_Click()
    If AllowExit Then
        Unload Me
    End If
End Sub

Private Sub SearchPIF_Click()
  ' Set CancelError is True
  CommonDialog1.CancelError = True
  On Error GoTo ErrHandler
  ' Set flags
  CommonDialog1.Flags = cdlOFNHideReadOnly & cdlOFNFileMustExist
  ' Set filters
  CommonDialog1.Filter = "All Files (*.*)|*.*|Pif Files (*.pif)|*.pif"
  ' Specify default filter
  CommonDialog1.FilterIndex = 2
  ' Set initial directory
  CommonDialog1.InitDir = BatchDirectory
  ' Display the Open dialog box
  CommonDialog1.ShowOpen
  ' Display name in textbox of selected file
'1.01AB Begin'''''''''''''''''''
'  If MsgBox("Set Winord.pif location to: " & CommonDialog1.FileName, vbYesNo) = vbYes Then
'    txtWinordbat.Text = CommonDialog1.FileName
'  Else
'    If Dir(CommonDialog1.FileName) = "" Then
'        txtWinordbat.Text = BatchDirectory & "winord.pif"
'    End If
'  End If
'1.01AB End'''''''''''''''''''''
  Exit Sub
  
ErrHandler:
  'User pressed the Cancel button
  Exit Sub
End Sub
Private Sub SearchTXT_Click()
  ' Set CancelError is True
  CommonDialog1.CancelError = True
  On Error GoTo ErrHandler
  ' Set flags
  CommonDialog1.Flags = cdlOFNHideReadOnly & cdlOFNFileMustExist
  ' Set filters
  CommonDialog1.Filter = "All Files (*.*)|*.*|Text Files (*.txt)|*.txt"
  ' Specify default filter
  CommonDialog1.FilterIndex = 2
  ' Set initial directory
  CommonDialog1.InitDir = TextDirectory
  ' Display the Open dialog box
  CommonDialog1.ShowOpen
  
'1.01AB Begin'''''''''''''''''''
'  If MsgBox("Set Winord.txt location to: " & CommonDialog1.FileName, vbYesNo) = vbYes Then
'    ' Display in textbox of selected file
'     txtWinordtxt.Text = CommonDialog1.FileName
'  Else
'    If Dir(CommonDialog1.FileName) = "" Then
'        txtWinordbat.Text = TextDirectory & "winord.txt"
'    End If
'  End If
'1.01AB End'''''''''''''''''''''
  
  Exit Sub
  
ErrHandler:
  'User pressed the Cancel button
  Exit Sub
End Sub

Private Sub Timer1_Timer()

    If mblnbusy = True Then Exit Sub

    If mdateActivateTime = Format(Time, "hh:mm") Then
        'start processing data..
        'ghost the Exit and pause menu options
        mnuExit.Enabled = False
        ChangeIcon picRed, "WINORD - Running"
        Call RunShell(txtWinordbat.Text)
        mnuExit.Enabled = True
        ChangeIcon PicGreen, "WINORD - Active"
    End If
    mblnbusy = False
End Sub
Private Sub picred_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
   
    Select Case x
        Case trayLBUTTONDOWN
            ' Left button is down
        Case trayLBUTTONUP
            ' Left Button Click
        Case trayLBUTTONDBLCLK
            ' Left Button Double Click
        Case trayRBUTTONDOWN
            ' Right Button Down
            ' This is where you would put support for the menu
            frmWINORD.PopupMenu mnuFile, vbPopupMenuRightAlign
        Case trayRBUTTONUP
            ' Right Button Click
        Case trayRBUTTONDBLCLK
            ' Right Button Double CLick
        Case trayMOUSEMOVE
            ' Mouse Move
        Case Else
    End Select
    
End Sub
Private Sub picgreen_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  
    Select Case x
        Case trayLBUTTONDOWN
            ' Left button is down
        Case trayLBUTTONUP
            ' Left Button Click
        Case trayLBUTTONDBLCLK
            ' Left Button Double Click
        Case trayRBUTTONDOWN
            ' Right Button Down
            ' This is where you would put support for the menu
            frmWINORD.PopupMenu mnuFile, vbPopupMenuRightAlign
        Case trayRBUTTONUP
            ' Right Button Click
        Case trayRBUTTONDBLCLK
            ' Right Button Double CLick
        Case trayMOUSEMOVE
            ' Mouse Move
        Case Else
    End Select
    
End Sub
Private Sub mnuView_Click()
    Dim MyRet As Long
    If Me.Visible = False Then
        frmWINORD.Show
        mnuView.Enabled = False
        MyRet = BringWindowToTop(Me.hwnd)
    End If
End Sub

Public Function AllowExit() As Boolean
    frmServiceDeskPassword.Show vbModal
    AllowExit = gblnExit
End Function

