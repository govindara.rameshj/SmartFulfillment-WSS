VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmServiceDeskPassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Service Desk Password Entry"
   ClientHeight    =   2190
   ClientLeft      =   1710
   ClientTop       =   2715
   ClientWidth     =   6090
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   1293.924
   ScaleMode       =   0  'User
   ScaleWidth      =   5718.18
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   495
      Left            =   1748
      TabIndex        =   1
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   495
      Left            =   3128
      TabIndex        =   2
      Top             =   1200
      Width           =   1215
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   4080
      MaxLength       =   5
      TabIndex        =   0
      Top             =   540
      Width           =   675
   End
   Begin ComctlLib.StatusBar staStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   1815
      Width           =   6090
      _ExtentX        =   10742
      _ExtentY        =   661
      Style           =   1
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblSD_NumberLabel 
      Caption         =   "Number:"
      Height          =   195
      Left            =   3060
      TabIndex        =   7
      Top             =   240
      Width           =   915
   End
   Begin VB.Label lblSD_Number 
      Height          =   195
      Left            =   4080
      TabIndex        =   6
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label lblServDeskPassReq 
      Caption         =   "Service Desk Password Required -"
      Height          =   195
      Left            =   240
      TabIndex        =   4
      Top             =   240
      Width           =   2655
   End
   Begin VB.Label lblPasswordLabel 
      Caption         =   "Password:"
      Height          =   195
      Left            =   3060
      TabIndex        =   3
      Top             =   600
      Width           =   915
   End
End
Attribute VB_Name = "frmServiceDeskPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SD_Password As String
Private Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Private Declare Function ModifyMenu Lib "user32" Alias "ModifyMenuA" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpString As Any) As Long
Const MF_BYCOMMAND = &H0&
Const MF_GRAYED = &H1&
Const SC_CLOSE = &HF060&

Private Sub cmdCancel_Click()
    cmdCancel.SetFocus
    DoEvents
    frmMessage.gblnExit = False
    Unload Me
End Sub

Private Sub cmdOK_Click()
    cmdOK.SetFocus
    DoEvents
    
    If SD_Password = txtPassword.Text Then
        frmMessage.gblnExit = True
        Unload Me
    Else
        staStatus.SimpleText = "INVALID PASSWORD"
    End If
End Sub

Private Sub Form_Load()
    'disable system menu close
    Dim hMenu&
    Dim wFlags&
    Dim Success&
    hMenu& = GetSystemMenu(Me.hwnd, 0)
    wFlags& = MF_BYCOMMAND Or MF_GRAYED
    Success& = ModifyMenu(hMenu&, SC_CLOSE, wFlags&, -10, "Close")
    Dim wkRandom As Long
    Dim wkDouble As Double
    Dim SD_Number As String
    
    Randomize
    
    wkRandom = 99999 * Rnd + 1
    SD_Number = "W" & Format(wkRandom, "0000#")

    wkDouble = 98765432
    wkDouble = wkDouble - wkRandom
    wkDouble = Fix(wkDouble * 119)
    wkDouble = Fix(wkDouble / 19)
    wkDouble = Fix(wkDouble * 256)
    wkDouble = Fix(wkDouble / 27)
    wkDouble = Fix(wkDouble * 5656)
    wkDouble = Fix(wkDouble / 321)
    
    SD_Password = Format(wkDouble)
    SD_Password = Right(SD_Password, 5)
    SD_Password = Format(CLng(SD_Password), "0000#")
    lblSD_Number.Caption = SD_Number
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = True
    End If
End Sub

Private Sub txtPassword_GotFocus()
    staStatus.SimpleText = "Enter Service Desk Password"
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)
    If (KeyAscii > 31 And KeyAscii < 48) Or (KeyAscii > 57) Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtPassword_LostFocus()
    staStatus.SimpleText = ""
    If Not (IsNumeric(txtPassword.Text)) Then
        txtPassword.Text = ""
        Exit Sub
    End If
    
    txtPassword.Text = Format(CLng(txtPassword.Text), "0000#")
End Sub
