VERSION 5.00
Begin VB.Form frmMessage 
   Caption         =   "WINORD - Auto SOQ Program Running"
   ClientHeight    =   4890
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7650
   Icon            =   "frmMessage.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4890
   ScaleWidth      =   7650
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Interval        =   50000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   3218
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox txtMessage 
      Height          =   4215
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   720
      Width           =   7695
   End
   Begin VB.PictureBox picRed 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   660
      Left            =   15
      Picture         =   "frmMessage.frx":058A
      ScaleHeight     =   660
      ScaleWidth      =   645
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   840
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.PictureBox PicGreen 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   660
      Left            =   0
      Picture         =   "frmMessage.frx":1DE4
      ScaleHeight     =   660
      ScaleWidth      =   660
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2400
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.PictureBox picYellow 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   600
      Left            =   30
      Picture         =   "frmMessage.frx":363E
      ScaleHeight     =   600
      ScaleWidth      =   600
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1680
      Width           =   600
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Visible         =   0   'False
      Begin VB.Menu mnuView 
         Caption         =   "&View"
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
      End
   End
End
Attribute VB_Name = "frmMessage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'1.05 - 27.02.2008 - MM - Changed system to read in Activate Date from config file when started and at 12:00 each night
'1.04 - 18.03.2003 - AB - Referral - Verbal - Kernel32.dll error when running in win95 environment
'                                   Removed all system tray code and unused code referencing kernel32.dll.
'1.03 - 30.08.2002 - AB - Referral - WIXCTS.288 Major re-working due to requested
'                         design changes
'1.02 - 27.08.2002 - AB - Verbal referral - Make form visible
'1.01 - 27.08.2002 - AB - Referral WIXCTS.287
'                       - Location of WINORD.TXT and WINORD.BAT not editable.
'1.00 - 23.08.2002 - AB - BSD WIX1054 Initial creation.

Option Explicit

Private Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Private Declare Function ModifyMenu Lib "user32" Alias "ModifyMenuA" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpString As Any) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
'Private Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long
Const MF_BYCOMMAND = &H0&
Const MF_GRAYED = &H1&
Const SC_CLOSE = &HF060&
Const PROCESS_QUERY_INFORMATION = &H400

Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Private Const SW_RESTORE = 9
Private Const SW_MINIMIZE = 6

Private mintfileNum As Integer
Private mstrReturned As String
Private mdateActivateTime As Date
Private strFirstLine() As String
Private mblnbusy As Boolean
Public gblnExit As Boolean
Private mstrWinordtxt As String
Private mstrWinordPif As String
Private dtAppIniDate As Date
Private dteReadIniFile As Date

Private Sub cmdOK_Click()
    
    Unload Me

End Sub

Private Sub cmdExit_Click()
    
    If AllowExit Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()
    
    'disable system menu close variables
    Dim hMenu&
    Dim wFlags&
    Dim Success&
    
    'Allow only one instance of the application to run
    If App.PrevInstance = True Then
        End
    End If
    
    'disable system menu close
    hMenu& = GetSystemMenu(Me.hwnd, 0)
    wFlags& = MF_BYCOMMAND Or MF_GRAYED
    Success& = ModifyMenu(hMenu&, SC_CLOSE, wFlags&, -10, "Close")
    
    
    'Default directories
    mstrWinordtxt = gstrconstTextDirectory & "winord.txt"
    mstrWinordPif = gstrconstBatchDirectory & "winord.pif"
    
    'Save application initialise date
    dtAppIniDate = Date
    
    If Dir(mstrWinordtxt) = "" Then
        MsgBox "File: " & gstrconstTextDirectory & "winord.txt" & " NOT FOUND. Application STOPPED.", vbCritical
        End
    End If
    
    Call ReadActivate
        
    'AddIcon PicGreen, "WINORD - Active"  '1.04 AB
    
    Call RunTheBatch
    
    If Dir(gstrconstTextDirectory & "winord.msg") = "" Then
        MsgBox "File: " & gstrconstTextDirectory & "winord.msg" & " NOT FOUND. Application STOPPED. Please create file", vbCritical
        End
    End If
    
    Call WritetoWinordMsg
    Call DisplayWinordMsg

    Exit Sub
    
End Sub

'1.05 MM Added to allow the config file to be updated automatically without restarting
Private Sub ReadActivate()
    
    mintfileNum = FreeFile()
    Open mstrWinordtxt For Input As mintfileNum
    Input #mintfileNum, mstrReturned
    Debug.Print mstrReturned
    Close #mintfileNum
    
    strFirstLine = Split(mstrReturned, ";")
    mdateActivateTime = Format(Trim(strFirstLine(1)), "hh:mm")
    Debug.Print CDate(mdateActivateTime)
    dteReadIniFile = Date

End Sub

Private Sub Form_Resize()
    
    On Error Resume Next
    txtMessage.Height = Me.Height - 1081
    txtMessage.Width = Me.Width - 100
    cmdExit.Left = Me.Width / 2 - cmdExit.Width / 2

End Sub
Public Sub RunTheBatch()
    
    If mdateActivateTime = Format(Time, "hh:mm") Then
        'mnuExit.Enabled = False                '1.04 AB
        'ChangeIcon picRed, "WINORD - Running"  '1.04 AB
        Call RunShell(mstrWinordPif)
        'mnuExit.Enabled = True                 '1.04 AB
        'ChangeIcon PicGreen, "WINORD - Active" '1.04 AB
    End If

End Sub

Public Sub RunShell(cmdLine As String)

Dim hProcess As Long
Dim ProcessId As Long
Dim exitCode As Long
Dim r As Long

    DoEvents
    
    ProcessId& = Shell(cmdLine$, 1)
    hProcess& = OpenProcess(PROCESS_QUERY_INFORMATION, False, ProcessId&)
    Do
        Call GetExitCodeProcess(hProcess&, exitCode&)
        DoEvents
    Loop While exitCode& > 0
    r = CloseHandle(hProcess)
    
    'On Error Resume Next
    'AppActivate ProcessId&, False
    'SendKeys "^c", True  'key press CTL+c

End Sub

Private Sub Timer1_Timer()
    
    Dim l As Long

    If mblnbusy = True Then Exit Sub

    If mdateActivateTime = Format(Time, "hh:mm") Then
        'start processing data..
        'ghost the Exit and pause menu options
        'mnuExit.Enabled = False                 '1.04 AB
        'ChangeIcon picRed, "WINORD - Running"   '1.04 AB
        Call RunShell(mstrWinordPif)
        
        txtMessage.Text = ""
        Call WritetoWinordMsg("n")
        Call DisplayWinordMsg
        
        'mnuExit.Enabled = True                  '1.04 AB
        'ChangeIcon PicGreen, "WINORD - Active"  '1.04 AB
            
        l = ShowWindow(hwnd:=Me.hwnd, _
                    nCmdShow:=SW_RESTORE)
    End If
    mblnbusy = False
    
    If (Date <> dteReadIniFile) Then Call ReadActivate
End Sub
'1.04 AB Begin'''''''''''''''''''''''
'Private Sub picred_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
'
'    Select Case x
'        Case trayLBUTTONDOWN
'            ' Left button is down
'        Case trayLBUTTONUP
'            ' Left Button Click
'        Case trayLBUTTONDBLCLK
'            ' Left Button Double Click
'        Case trayRBUTTONDOWN
'            ' Right Button Down
'            ' This is where you would put support for the menu
'            frmMessage.PopupMenu mnuFile, vbPopupMenuRightAlign
'        Case trayRBUTTONUP
'            ' Right Button Click
'        Case trayRBUTTONDBLCLK
'            ' Right Button Double CLick
'        Case trayMOUSEMOVE
'            ' Mouse Move
'        Case Else
'    End Select
'
'End Sub
'
'Private Sub picgreen_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
'
'    Select Case x
'        Case trayLBUTTONDOWN
'            ' Left button is down
'        Case trayLBUTTONUP
'            ' Left Button Click
'        Case trayLBUTTONDBLCLK
'            ' Left Button Double Click
'        Case trayRBUTTONDOWN
'            ' Right Button Down
'            ' This is where you would put support for the menu
'            frmMessage.PopupMenu mnuFile, vbPopupMenuRightAlign
'        Case trayRBUTTONUP
'            ' Right Button Click
'        Case trayRBUTTONDBLCLK
'            ' Right Button Double CLick
'        Case trayMOUSEMOVE
'            ' Mouse Move
'        Case Else
'    End Select
'
'End Sub
'Private Sub mnuView_Click()
'    Dim MyRet As Long
'    If Me.Visible = False Then
'        frmMessage.Show
'        mnuView.Enabled = False
'        MyRet = BringWindowToTop(Me.hwnd)
'    End If
'End Sub
'1.04 AB End'''''''''''''''''''''''''

Public Function AllowExit() As Boolean
    
    frmServiceDeskPassword.Show vbModal
    AllowExit = gblnExit

End Function

Public Sub WritetoWinordMsg(Optional chrN As String)
    
    mintfileNum = FreeFile()
    Open gstrconstTextDirectory & "WINORD.msg" For Append Access Write Lock Write As mintfileNum
    Write #mintfileNum, "Application Initialisation date: " & dtAppIniDate & " time: " & Format(Time, "hh:mm")
    If chrN = "" Then
        If Format(mdateActivateTime, "hh:mm") > Format(Time, "hh:mm") Then
            Write #mintfileNum, "Application will next run batch on date: " & Date & " time: " & Format(mdateActivateTime, "hh:mm")
        Else
            Write #mintfileNum, "Application will next run batch on date: " & (Date + 1) & " time: " & Format(mdateActivateTime, "hh:mm")
        End If
    Else
        Write #mintfileNum, "Application will next run batch on date: " & (Date + 1) & " time: " & Format(mdateActivateTime, "hh:mm")
    End If
    Close #mintfileNum

End Sub

Public Sub DisplayWinordMsg()
    
    mintfileNum = FreeFile()
    Open gstrconstTextDirectory & "WINORD.msg" For Input As mintfileNum
    Do
        On Error GoTo myErrHandler
        Input #mintfileNum, mstrReturned
        txtMessage.Text = txtMessage.Text & mstrReturned & vbCrLf
        mstrReturned = ""
    Loop
    
myErrHandler:
    Close

End Sub
    
