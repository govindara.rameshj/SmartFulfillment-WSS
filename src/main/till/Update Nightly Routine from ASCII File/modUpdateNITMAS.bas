Attribute VB_Name = "modUpdateNightlyRoutineFromASCIIFile"
'<CAMH>****************************************************************************************
'* Module : modUpdateNightlyRoutineFromASCIIFile
'* Date   : 21/06/04
'* Author :
'*$Archive: /Projects/OasysV2/VB/Update Nightly Routine from ASCII File/modUpdateNightlyRoutineFromASCIIFile.bas $Author:  $ $Date: 15/07/04 14:45 $ $Revision: 9 $
'* Versions:
'</CAMH>***************************************************************************************

Option Explicit

Const MODULE_NAME             As String = "modUpdateNightlyRoutineFromASCIIFile"
Const MODULE_FUNCTION         As String = "Update Nightly Routine from ASCII File"
Const DISPLAY_UPDATE_INTERVAL As Long = 1

Const LOADING_MESSAGE         As String = "Loading Data..."
Const PROCESSING_MESSAGE      As String = "Processing Data..."
Const BLANK_MESSAGE           As String = ""

Const OASYS_ERR_COMMS_PATH_NOT_FOUND = vbObjectError + 131
Const OASYS_ERR_NIGHTLY_ROUTINE_FILE_NOT_FOUND = vbObjectError + 143
Const OASYS_ERR_NIGHTLY_ROUTINE_FILE_EMPTY = vbObjectError + 144

Dim mblnFromNightlyClose      As Boolean
Dim mstrStartDate             As String

' Fields used for ADO data access
Dim mcon                    As ADODB.Connection
Dim mcmd                    As ADODB.Command
Dim mrsWork                 As ADODB.Recordset

Dim mstrInputPath           As String
Dim mintInputFileNumber     As Integer

Dim mlngRecordsAffected     As Long

Public Sub Main()
    
Const PROCEDURE_NAME As String = MODULE_NAME & ".Main"

    'Initialise Oasys objects
    GetRoot
    
    Call DecodeParameters(goSession.ProgramParams)
    
    Set mcon = New ADODB.Connection
    mcon.Open goDatabase.ConnectionString
    
    Set mcmd = New ADODB.Command
    mcmd.ActiveConnection = mcon
    mcmd.CommandType = adCmdText
    
    Set mrsWork = New ADODB.Recordset
    
    mrsWork.ActiveConnection = mcon
    mrsWork.CursorLocation = adUseClient
    mrsWork.CursorType = adOpenStatic
    
    mstrInputPath = goSession.GetParameter(PRM_COMMS_FOLDER)
    If Dir(mstrInputPath, vbDirectory) = "" Then
        Err.Raise OASYS_ERR_COMMS_PATH_NOT_FOUND, PROCEDURE_NAME, "COMMS Folder Not Found."
        End
    End If
    

    If mblnFromNightlyClose = False Then
        If MsgBox("Update Nightly Routine from ASCII File?", _
            vbYesNo + vbQuestion, MODULE_FUNCTION) = vbNo Then
            End
        End If
    End If
    
    frmUpdateNightlyRoutineFromASCIIFile.Show
    DoEvents
    frmUpdateNightlyRoutineFromASCIIFile.lngUpdateInterval = DISPLAY_UPDATE_INTERVAL
    Call UpdateNightlyRoutineFromASCIIFile

    If mblnFromNightlyClose = False Then
        Call MsgBox("Nightly Routine Updated.", vbInformation, MODULE_FUNCTION)
    End If
    
    End
End Sub

Private Sub DecodeParameters(strCommand As String)

Const CALLED_FROM As String = "CF"
Const CF_CLOSE    As String = "C"
Dim strTempDate As String
Dim dteEndDate  As Date
Dim strSection  As String
Dim vntSection  As Variant
Dim lngSectNo   As Long
Dim blnSetOpts  As Boolean
                    Call DebugMsg(MODULE_NAME, "Updating Nightly Routine", endlDebug, _
                                "Parameters Passed = " & strCommand)

    vntSection = Split(strCommand, ",")
    
    mblnFromNightlyClose = False
    
    If UBound(vntSection) = 0 Then Exit Sub
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        
        Select Case (Right$(strSection, 1))
        Case CF_CLOSE
            Debug.Print Mid$(strSection, 4)
            If Mid(strSection, 3) = CF_CLOSE Then
                mblnFromNightlyClose = True
            End If
        End Select
       
    Next lngSectNo
    
End Sub

Public Sub UpdateNightlyRoutineFromASCIIFile()

Const PROCEDURE_NAME As String = MODULE_NAME & ".UpdateNightlyRoutineFromASCIIFile"

Dim strSQLStatement As String
Dim strInputLine    As String
Dim lngLineCount    As Long

    'On Error GoTo errHandler

    If frmUpdateNightlyRoutineFromASCIIFile.Visible Then
        frmUpdateNightlyRoutineFromASCIIFile.strStatus = "Updating Nightly Routine"
    End If
        
    'Check to make sure file exists
    If Dir(mstrInputPath & "NITMAS", vbNormal) = "" Then
        Err.Raise OASYS_ERR_NIGHTLY_ROUTINE_FILE_NOT_FOUND, PROCEDURE_NAME, "No Data to Process."
        End
    End If
        
    'Open file
    mintInputFileNumber = FreeFile()
    Open mstrInputPath & "NITMAS" For Input As mintInputFileNumber
    
    If EOF(mintInputFileNumber) Then
        Err.Raise OASYS_ERR_NIGHTLY_ROUTINE_FILE_EMPTY, PROCEDURE_NAME, "No Data to Process."
    End If
    
    mcon.BeginTrans
    
    'First, delete existing records
    mcmd.CommandText = "Delete from NITMAS"
    
    mcmd.Execute mlngRecordsAffected
    
    'Line Input #mintInputFileNumber, strInputLine
    
    lngLineCount = 0
    
    Do While Not EOF(mintInputFileNumber)
        Line Input #mintInputFileNumber, strInputLine
        
        lngLineCount = lngLineCount + 1
        If IsNumeric(Mid(strInputLine, 1, 1)) = True Then ' To Ensure comments are missed
            If frmUpdateNightlyRoutineFromASCIIFile.Visible Then
                frmUpdateNightlyRoutineFromASCIIFile.strStatus = "Updating Nightly Routine - " & _
                    Mid(strInputLine, 1, 1) & " " & Mid(strInputLine, 2, 3)
            End If
            
            mcmd.CommandText = "Insert Into NITMAS(" & _
                "NSET, " & _
                "TASK, " & _
                "DESCR, " & _
                "PROG, " & _
                "NITE, " & _
                "RETY, " & _
                "WEEK, " & _
                "PEND, " & _
                "LIVE, " & _
                "OPTN, " & _
                "ABOR, " & _
                "JRUN, " & _
                "DAYS1, " & _
                "DAYS2, " & _
                "DAYS3, " & _
                "DAYS4, " & _
                "DAYS5, " & _
                "DAYS6, " & _
                "DAYS7) " '", & _
                '"BEGD, " & _
                '"ENDD) "
            mcmd.CommandText = mcmd.CommandText & "Values('" & _
                Mid(strInputLine, 1, 1) & "', '" & _
                Format(CInt(Mid(strInputLine, 2, 3)), "000") & "', '" & _
                ReturnSQLString(Mid(strInputLine, 5, 40)) & "', '" & _
                ReturnSQLString(Mid(strInputLine, 76, 70)) & "', " & _
                IIf(Mid(strInputLine, 45, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 46, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 47, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 48, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 49, 1) = "Y", "1", "0") & ", " & _
                CInt(Mid(strInputLine, 50, 1)) & ", " & _
                IIf(Mid(strInputLine, 52, 1) = "Y", "1", "0") & ", '" & _
                Mid(strInputLine, 201, 70) & "', " & _
                IIf(Mid(strInputLine, 53, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 54, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 55, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 56, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 57, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 58, 1) = "Y", "1", "0") & ", " & _
                IIf(Mid(strInputLine, 59, 1) = "Y", "1", "0") & ") "
                'Below commented out as it was giving an Error 13 if the date was "--/--/--"
                'IIf(IsDate(Mid(strInputLine, 60, 8)), "'" & Format(CDate(Mid(strInputLine, 60, 8)), _
                '    "YYYY-MM-DD") & "'", "Null") & ", " & _
                'IIf(IsDate(Mid(strInputLine, 68, 8)), "'" & Format(CDate(Mid(strInputLine, 68, 8)), _
                '    "YYYY-MM-DD") & "'", "Null") & ")"
    
            mcmd.Execute mlngRecordsAffected
            If mlngRecordsAffected <> 1 Then
                Err.Raise OASYS_ERR_INSERT_FAILED, PROCEDURE_NAME, "Error Inserting Nightly Routine Record."
            End If
            If IsDate(Mid(strInputLine, 60, 8)) Then
                mcmd.CommandText = "UPDATE NITMAS SET BEGD = '" & _
                Format(CDate(Mid(strInputLine, 60, 8)), "YYYY-MM-DD") & "' " & _
                "WHERE NSET = '" & Mid(strInputLine, 1, 1) & "'" & _
                "AND TASK = '" & Format(CInt(Mid(strInputLine, 2, 3)), "000") & "'"
                mcmd.Execute mlngRecordsAffected
            End If
            If IsDate(Mid(strInputLine, 68, 8)) Then
                mcmd.CommandText = "UPDATE NITMAS SET ENDD = '" & _
                Format(CDate(Mid(strInputLine, 68, 8)), "YYYY-MM-DD") & "' " & _
                "WHERE NSET = '" & Mid(strInputLine, 1, 1) & "'" & _
                "AND TASK = '" & Format(CInt(Mid(strInputLine, 2, 3)), "000") & "'"
                mcmd.Execute mlngRecordsAffected
            End If
        End If
    Loop

    mcon.CommitTrans
    
    If frmUpdateNightlyRoutineFromASCIIFile.Visible Then
        frmUpdateNightlyRoutineFromASCIIFile.strStatus = ""
        frmUpdateNightlyRoutineFromASCIIFile.strStatusMessage = BLANK_MESSAGE
    End If
    
    Call Successful_End
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    End
End Sub

Private Sub Successful_End()
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Erase file so that close program will know that this program
' completed successfully
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Dir(App.Path & "\TASKCOMP") <> "" Then
        Kill (App.Path & "\TASKCOMP")
    End If
End Sub

Private Function FormatDateForSQL(strDATE) As String
    
    If IsDate(strDATE) Then
        FormatDateForSQL = "'" & Format(CDate(strDATE), "YYYY-MM-DD") & "'"
    Else
        FormatDateForSQL = "Null"
    End If

End Function

Public Function ReturnSQLString(strIn As String) As String

    ReturnSQLString = Replace(strIn, "'", "''")

End Function
