VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDepartment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D8724090384"
'<CAMH>****************************************************************************************
'* Module : cDepartment
'* Date   : 17/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/LookUps/cDepartment.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Revision: 15 $
'* $Date: 11/05/04 19:06 $
'* $Author: Mauricem $
'* Versions:
'* 17/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cDepartment"

Implements IBo
Implements ISysBo

'##ModelId=3D87251B0334
Private mDepartmentCode As String

'##ModelId=3D8725360122
Private mDescription As String

'##ModelId=3D87253A0352
Private mSize As Long

'##ModelId=3D87253E01A4
Private mVatCode As Long

'##ModelId=3D8725540370
Private mCostFactor As Double

'##ModelId=3D87255E0226
Private mNoPartLines(7) As Long

'##ModelId=3D87256A01D6
Private mNoDeptLines(7) As Long

'##ModelId=3D8725720032
Private mPartRetailSales(7) As Double

'##ModelId=3D87257A0046
Private mDeptRetailSales(7) As Double

'##ModelId=3D8725830082
Private mPartCostOfSales(7) As Double

'##ModelId=3D87258A017C
Private mDeptCostOfSales(7) As Double

'##ModelId=3D8725980302
Private mNoPriceViolations(7) As Long

'##ModelId=3D8725A10154
Private mValueOfViolations(7) As Double

'##ModelId=3D8725C00096
Private mWeeklySalesValue(6) As Double

'##ModelId=3D8725C903A2
Private mWeeklyItemLookups(6) As Long

'##ModelId=3D8725D8010E
Private mWeeklyOutOfStock(6) As Long

'##ModelId=3D8725E30320
Private mAllowZeroPrices As Boolean

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


'##ModelId=3D8727F802E4
Public Property Get AllowZeroPrices() As Boolean
   Let AllowZeroPrices = mAllowZeroPrices
End Property

'##ModelId=3D8727F80208
Public Property Let AllowZeroPrices(ByVal Value As Boolean)
    Let mAllowZeroPrices = Value
End Property

'##ModelId=3D8727F800FA
Public Property Get WeeklyOutOfStock(Index As Variant) As Long
    Let WeeklyOutOfStock = mWeeklyOutOfStock(Index)
End Property

'##ModelId=3D8727F7035C
Public Property Let WeeklyOutOfStock(Index As Variant, ByVal Value As Long)
    Let mWeeklyOutOfStock(Index) = Value
End Property

'##ModelId=3D8727F70280
Public Property Get WeeklyItemLookups(Index As Variant) As Long
    Let WeeklyItemLookups = mWeeklyItemLookups(Index)
End Property

'##ModelId=3D8727F70104
Public Property Let WeeklyItemLookups(Index As Variant, ByVal Value As Long)
    Let mWeeklyItemLookups(Index) = Value
End Property

'##ModelId=3D8727F603A2
Public Property Get WeeklySalesValue(Index As Variant) As Double
    Let WeeklySalesValue = mWeeklySalesValue(Index)
End Property

'##ModelId=3D8727F60258
Public Property Let WeeklySalesValue(Index As Variant, ByVal Value As Double)
    Let mWeeklySalesValue(Index) = Value
End Property

'##ModelId=3D8727F6017C
Public Property Get ValueOfViolations(Index As Variant) As Double
    Let ValueOfViolations = mValueOfViolations(Index)
End Property

'##ModelId=3D8727F60032
Public Property Let ValueOfViolations(Index As Variant, ByVal Value As Double)
    Let mValueOfViolations(Index) = Value
End Property

'##ModelId=3D8727F5033E
Public Property Get NoPriceViolations(Index As Variant) As Long
    Let NoPriceViolations = mNoPriceViolations(Index)
End Property

'##ModelId=3D8727F501F4
Public Property Let NoPriceViolations(Index As Variant, ByVal Value As Long)
    Let mNoPriceViolations(Index) = Value
End Property

'##ModelId=3D8727F50118
Public Property Get DeptCostOfSales(Index As Variant) As Double
    Let DeptCostOfSales = mDeptCostOfSales(Index)
End Property

'##ModelId=3D8727F5000A
Public Property Let DeptCostOfSales(Index As Variant, ByVal Value As Double)
    Let mDeptCostOfSales(Index) = Value
End Property

'##ModelId=3D8727F40316
Public Property Get PartCostOfSales(Index As Variant) As Double
    Let PartCostOfSales = mPartCostOfSales(Index)
End Property

'##ModelId=3D8727F401FE
Public Property Let PartCostOfSales(Index As Variant, ByVal Value As Double)
    Let mPartCostOfSales(Index) = Value
End Property

'##ModelId=3D8727F40122
Public Property Get DeptRetailSales(Index As Variant) As Double
    Let DeptRetailSales = mDeptRetailSales(Index)
End Property

'##ModelId=3D8727F40014
Public Property Let DeptRetailSales(Index As Variant, ByVal Value As Double)
    Let mDeptRetailSales(Index) = Value
End Property

'##ModelId=3D8727F30352
Public Property Get PartRetailSales(Index As Variant) As Double
    Let PartRetailSales = mPartRetailSales(Index)
End Property

'##ModelId=3D8727F30244
Public Property Let PartRetailSales(Index As Variant, ByVal Value As Double)
    Let mPartRetailSales(Index) = Value
End Property

'##ModelId=3D8727F30168
Public Property Get NoDeptLines(Index As Variant) As Long
    Let NoDeptLines = mNoDeptLines(Index)
End Property

'##ModelId=3D8727F3005A
Public Property Let NoDeptLines(Index As Variant, ByVal Value As Long)
    Let mNoDeptLines(Index) = Value
End Property

'##ModelId=3D8727F20398
Public Property Get NoPartLines(Index As Variant) As Long
    Let NoPartLines = mNoPartLines(Index)
End Property

'##ModelId=3D8727F202BC
Public Property Let NoPartLines(Index As Variant, ByVal Value As Long)
    Let mNoPartLines(Index) = Value
End Property

'##ModelId=3D8727F2024E
Public Property Get CostFactor() As Double
   Let CostFactor = mCostFactor
End Property

'##ModelId=3D8727F201AE
Public Property Let CostFactor(ByVal Value As Double)
    Let mCostFactor = Value
End Property

'##ModelId=3D8727F20172
Public Property Get VatCode() As Long
   Let VatCode = mVatCode
End Property

'##ModelId=3D8727F200D2
Public Property Let VatCode(ByVal Value As Long)
    Let mVatCode = Value
End Property

'##ModelId=3D8727F20064
Public Property Get Size() As Long
   Let Size = mSize
End Property

'##ModelId=3D8727F103DE
Public Property Let Size(ByVal Value As Long)
    Let mSize = Value
End Property

'##ModelId=3D8727F10370
Public Property Get Description() As String
   Let Description = mDescription
End Property

'##ModelId=3D8727F10302
Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

'##ModelId=3D8727F102C6
Public Property Get DepartmentCode() As String
   Let DepartmentCode = mDepartmentCode
End Property


'##ModelId=3D8727F10226
Public Property Let DepartmentCode(ByVal Value As String)
    Let mDepartmentCode = Value
End Property

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cDepartment

End Function

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Department Code " & mDepartmentCode

End Property

Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function Initialise(oSession As ISession) As cDepartment
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_DEPARTMENT

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function

Public Function Interface(Optional eInterfaceType As Long) As cDepartment
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    'added M.Milne 2/10/02 - to set all fields used by Load to perform db retrieval
    IBo_SetLoadField = False
End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function GetField(lFieldID As Long) As IField
    
    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_DEPARTMENT_DepartmentCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepartmentCode
        Case (FID_DEPARTMENT_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_DEPARTMENT_Size):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSize
        Case (FID_DEPARTMENT_VatCode):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mVatCode
        Case (FID_DEPARTMENT_CostFactor):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mCostFactor
        Case (FID_DEPARTMENT_NoPartLines):          Set GetField = SaveLongArray(mNoPartLines, m_oSession)
        Case (FID_DEPARTMENT_NoDeptLines):          Set GetField = SaveLongArray(mNoDeptLines, m_oSession)
        Case (FID_DEPARTMENT_PartRetailSales):      Set GetField = SaveDoubleArray(mPartRetailSales, m_oSession)
        Case (FID_DEPARTMENT_DeptRetailSales):      Set GetField = SaveDoubleArray(mDeptRetailSales, m_oSession)
        Case (FID_DEPARTMENT_PartCostOfSales):      Set GetField = SaveDoubleArray(mPartCostOfSales, m_oSession)
        Case (FID_DEPARTMENT_DeptCostOfSales):      Set GetField = SaveDoubleArray(mDeptCostOfSales, m_oSession)
        Case (FID_DEPARTMENT_NoPriceViolations):    Set GetField = SaveLongArray(mNoPriceViolations, m_oSession)
        Case (FID_DEPARTMENT_ValueOfViolations):    Set GetField = SaveDoubleArray(mValueOfViolations, m_oSession)
        Case (FID_DEPARTMENT_WeeklySalesValue):     Set GetField = SaveDoubleArray(mWeeklySalesValue, m_oSession)
        Case (FID_DEPARTMENT_WeeklyItemLookups):    Set GetField = SaveLongArray(mWeeklyItemLookups, m_oSession)
        Case (FID_DEPARTMENT_WeeklyOutOfStock):     Set GetField = SaveLongArray(mWeeklyOutOfStock, m_oSession)
        Case (FID_DEPARTMENT_AllowZeroPrices):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mAllowZeroPrices
        Case Else
            Debug.Print "No case for short fid " & lFieldID
            Debug.Assert False
    End Select
    
    If GetField Is Nothing Then
        Debug.Assert False
    Else
        GetField.Id = lFieldID
    End If
    
    Exit Function
    
Error_Handler:

    Call MsgBox("Damn")
    Debug.Assert False
    Resume Next

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRowSelector object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_DEPARTMENT, FID_DEPARTMENT_END_OF_STATIC, m_oSession)

End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_DEPARTMENT * &H10000) + 1 To FID_DEPARTMENT_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

Public Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
'##ModelId=3D749F8A038E
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_DEPARTMENT_DepartmentCode):    mDepartmentCode = oField.ValueAsVariant
            Case (FID_DEPARTMENT_Description):       mDescription = oField.ValueAsVariant
            Case (FID_DEPARTMENT_Size):              mSize = oField.ValueAsVariant
            Case (FID_DEPARTMENT_VatCode):           mVatCode = oField.ValueAsVariant
            Case (FID_DEPARTMENT_CostFactor):        mCostFactor = oField.ValueAsVariant
            Case (FID_DEPARTMENT_NoPartLines):       Call LoadLongArray(mNoPartLines, oField, m_oSession)
            Case (FID_DEPARTMENT_NoDeptLines):       Call LoadLongArray(mNoDeptLines, oField, m_oSession)
            Case (FID_DEPARTMENT_PartRetailSales):   Call LoadDoubleArray(mPartRetailSales, oField, m_oSession)
            Case (FID_DEPARTMENT_DeptRetailSales):   Call LoadDoubleArray(mDeptRetailSales, oField, m_oSession)
            Case (FID_DEPARTMENT_PartCostOfSales):   Call LoadDoubleArray(mPartCostOfSales, oField, m_oSession)
            Case (FID_DEPARTMENT_DeptCostOfSales):   Call LoadDoubleArray(mDeptCostOfSales, oField, m_oSession)
            Case (FID_DEPARTMENT_NoPriceViolations): Call LoadLongArray(mNoPriceViolations, oField, m_oSession)
            Case (FID_DEPARTMENT_ValueOfViolations): Call LoadDoubleArray(mValueOfViolations, oField, m_oSession)
            Case (FID_DEPARTMENT_WeeklySalesValue):  Call LoadDoubleArray(mWeeklySalesValue, oField, m_oSession)
            Case (FID_DEPARTMENT_WeeklyItemLookups): Call LoadLongArray(mWeeklyItemLookups, oField, m_oSession)
            Case (FID_DEPARTMENT_WeeklyOutOfStock):  Call LoadLongArray(mWeeklyOutOfStock, oField, m_oSession)
            Case (FID_DEPARTMENT_AllowZeroPrices):   mAllowZeroPrices = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Public Function Retrieve() As Collection
    
Dim oBOCol      As Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    ' Pass the selector to the database to get the data view
    Set Retrieve = m_oSession.Database.GetBoCollection(GetSelectAllRow)

End Function

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)

End Function 'IBo_AddLoadFilter

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    
    Call AddLoadField(FieldID)

End Sub      'IBo_AddLoadField

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()
    
    Set moLoadRow = Nothing

End Sub

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    
    Set IBo_Interface = Interface(eInterfaceType)

End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
     
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_DEPARTMENT_END_OF_STATIC

End Function


