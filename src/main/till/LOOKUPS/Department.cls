VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Department"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D3D722E0244"
'
Option Base 0
Option Explicit


'set this to 0 to disable debug code in this class
#Const DebugMode = 1
#If DebugMode Then
    'local variable to hold the serialized class ID that was created in Class_Initialize
    '##ModelId=3D3D732A0212
    Private mlClassDebugID As Long
#End If

'##ModelId=3D3D72950190
Private DeptCode As Variant

'##ModelId=3D3D729B006E
Private TotalSales As Variant

'##ModelId=3D3D72AD02DA
Private ValueSales As Variant

'##ModelId=3D3D72B3029E
Private NumberItems As Variant

'##ModelId=3D3D724F0028
Public Sub Get()
    On Error GoTo GetErr

    'your code goes here...

    Exit Sub
GetErr:
    Call RaiseError(MyUnhandledError, "Department:Get Method")
End Sub

'##ModelId=3D3D732B01FE
Private Sub Class_Terminate()
    #If DebugMode Then
    'the class is being destroyed
    Debug.Print "'" & TypeName(Me) & "' instance " & CStr(mlClassDebugID) & " is terminating"
    #End If
End Sub

'##ModelId=3D3D732B015E
Private Sub Class_Initialize()
    #If DebugMode Then
        'get the next available class ID, and print out
        'that the class was created successfully
        mlClassDebugID = GetNextClassDebugID()
        Debug.Print "'" & TypeName(Me) & "' instance " & CStr(mlClassDebugID) & " created"
    #End If
End Sub

'##ModelId=3D3D72400032
Public Sub Add()
    On Error GoTo AddErr

    'your code goes here...

    Exit Sub
AddErr:
    Call RaiseError(MyUnhandledError, "Department:Add Method")
End Sub

'##ModelId=3D3D72470208
Public Sub Update()
    On Error GoTo UpdateErr

    'your code goes here...

    Exit Sub
UpdateErr:
    Call RaiseError(MyUnhandledError, "Department:Update Method")
End Sub

'##ModelId=3D3D724A0366
Public Sub Delete()
    On Error GoTo DeleteErr

    'your code goes here...

    Exit Sub
DeleteErr:
    Call RaiseError(MyUnhandledError, "Department:Delete Method")
End Sub

'##ModelId=3D3D725200E6
Public Sub List()
    On Error GoTo ListErr

    'your code goes here...

    Exit Sub
ListErr:
    Call RaiseError(MyUnhandledError, "Department:List Method")
End Sub

#If DebugMode Then
    '##ModelId=3D3D732A02EE
    Public Property Get ClassDebugID() As Long
        'if we are in debug mode, surface this property that consumers can query
        ClassDebugID = mlClassDebugID
    End Property
#End If
