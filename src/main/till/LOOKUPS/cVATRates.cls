VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cVATRates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3D8AE736032A"
'<CAMH>****************************************************************************************
'* Module : cVATRates
'* Date   : 20/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/LookUps/cVATRates.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Martynw $
'* $Date: 19/11/02 12:14 $
'* $Revision: 14 $
'* Versions:
'* 20/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cVATRates"

Const VATRATE_COUNT_MAX = 9

Implements IBo
Implements ISysBo

Private mId As String

'##ModelId=3D8AE7550384
Private mVATRateCount As Byte

'##ModelId=3D8AE76602F8
Private mVATRate(VATRATE_COUNT_MAX) As Double

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get Id() As Long
   Id = CDbl(mId)
End Property
Private Property Let Id(ByVal Value As Long)
   mId = Format$(Value, "00")
End Property

'##ModelId=3D8AE8A003A2
Public Property Get VATRate(Index As Variant) As Double
    VATRate = -1    ' Flag Error
    If IsNumeric(Index) Then
        If Index > 0 And Index <= VATRATE_COUNT_MAX Then
            Let VATRate = mVATRate(Index)
        End If
    End If
    Debug.Assert (VATRate <> -1)
End Property

'##ModelId=3D8AE8A00334
Public Property Let VATRate(Index As Variant, ByVal Value As Double)
    If IsNumeric(Index) Then
        If Index > 0 And Index <= VATRATE_COUNT_MAX Then
            Let mVATRate(Index) = Value
            If Index > VATRateCount Then
                VATRateCount = Index
            End If
        End If
    End If
End Property

'##ModelId=3D8AE8A00302
Public Property Get VATRateCount() As Byte
   Let VATRateCount = mVATRateCount
End Property


'##ModelId=3D8AE8A00258
Private Property Let VATRateCount(ByVal Value As Byte)
    ' This is a private method as users can't change the count directly
    If Value > VATRATE_COUNT_MAX Then
        mVATRateCount = VATRATE_COUNT_MAX
    Else
        Let mVATRateCount = Value
    End If
End Property


Private Sub Class_Initialize()
    VATRateCount = 0
    Id = 1          ' Only one record in the RETOPT table
End Sub

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cVATRates

End Function

Public Property Get IBo_DebugString() As String
    Dim i As Integer
    IBo_DebugString = "VAT Rates (" & VATRateCount & "): "
    For i = 1 To VATRateCount
        If i > 1 Then IBo_DebugString = IBo_DebugString & ", "
        IBo_DebugString = IBo_DebugString & VATRate(i)
    Next i

End Property

Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Public Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Private Function Initialise(oSession As ISession) As cVATRates
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_VATRATES

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Function IBo_Interface(Optional eInterfaceType As Long) As IBo

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function

Private Property Get IBo_Version() As String
    
    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_SetLoadField(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    'added M.Milne 2/10/02 - to set all fields used by Load to perform db retrieval
    IBo_SetLoadField = False
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_VATRATES_ID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mId
        Case (FID_VATRATES_VATRateCount):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = VATRateCount
        Case (FID_VATRATES_VATRate): Set GetField = SaveDoubleArray(mVATRate, m_oSession)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox("Damn - error in cVATRates::GetField()")
    Resume Next

End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_VATRATES, FID_VATRATES_END_OF_STATIC, m_oSession)

End Function

Public Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_VATRATES_ID):  Id = oField.ValueAsVariant
            Case (FID_VATRATES_VATRateCount):  VATRateCount = oField.ValueAsVariant
                                             'set a default value if no VAT Rates exist
                                             If VATRateCount = 0 Then VATRateCount = 1
            Case (FID_VATRATES_VATRate):     Call LoadDoubleArray(mVATRate, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Public Sub Retrieve()
    Call Load
End Sub


Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function
Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function Interface(Optional eInterfaceType As Long) As cVATRates
    
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set IBo_Interface = oBO
    Case Else:
        Set IBo_Interface = Me
    End Select

End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_VATRATES * &H10000) + 1 To FID_VATRATES_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
' Note that for VAT Rates we cannot create new as the VAT stuff is embedded within
' the other RETOPTs columns.
    Dim leSave As enSaveType    ' local copy of eSave that we change
    leSave = eSave
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        If eSave = SaveTypeIfNew Then
            ' We can't create if new!
            Debug.Assert False
        Else
            If eSave = SaveTypeAllCases Then
                leSave = SaveTypeIfExists
            End If
           Save = m_oSession.Database.SaveBo(leSave, Me)
        End If
    End If

End Function



'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)

End Function 'IBo_AddLoadFilter

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

'Public Function IField_Interface(Optional eInterfaceType As Long) As Object
'
'    Set IField_Interface = Interface(eInterfaceType)
'
'End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_VATRATES_END_OF_STATIC

End Function








