VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTillTenderType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E2BC859026C"
'<CAMH>****************************************************************************************
'* Module : cTillTenderType
'* Date   : 27/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/LookUps/cTillTenderTypes.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 1/28/03 3:14p $
'* $Revision: 2 $
'* Versions:
'* 20/01/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cTillTenderType"

Private mTenderTypeKey As Long

Private mTenderTypeID As Long

Private mDescription As String

Private mDisplayOrder As Long

Private mOverTenderAllowed As Boolean

Private mCaptureCreditCard As Boolean

Private mUseEFTPOS As Boolean

Private mOpenCashDrawer As Boolean

Private mDefaultRemainingAmount As Boolean

Private mPrintChequeFront As Boolean

Private mPrintChequeBack As Boolean

Private mMinimumAccepted As Long

Private mMaximumAccepted As Long

Private mSupervisorRequired As Boolean

Private mOtherAuthorisationRequired As Boolean

Private mSecurityLevel As Long

Private mConversionRate As Double

Private mConversionType As String

Private mTrailerFileName As String

Private mCaptureCCDetails As Long

Private mCaptureAuthCode As Long

Private mCaptureCVVNo As Long

Private mFloorLimit As Long

Private mCreditLimitCheck As Long

Private mActive As Boolean

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session


Public Property Get DefaultRemainingAmount() As Boolean
    Let DefaultRemainingAmount = mDefaultRemainingAmount
End Property

Public Property Let DefaultRemainingAmount(ByVal Value As Boolean)
    Let mDefaultRemainingAmount = Value
End Property

Public Property Get OpenCashDrawer() As Boolean
    Let OpenCashDrawer = mOpenCashDrawer
End Property

Public Property Let OpenCashDrawer(ByVal Value As Boolean)
    Let mOpenCashDrawer = Value
End Property

Public Property Get UseEFTPOS() As Boolean
    Let UseEFTPOS = mUseEFTPOS
End Property

Public Property Let UseEFTPOS(ByVal Value As Boolean)
    Let mUseEFTPOS = Value
End Property

Public Property Get CaptureCreditCard() As Boolean
    Let CaptureCreditCard = mCaptureCreditCard
End Property

Public Property Let CaptureCreditCard(ByVal Value As Boolean)
    Let mCaptureCreditCard = Value
End Property

Public Property Get OverTenderAllowed() As Boolean
    Let OverTenderAllowed = mOverTenderAllowed
End Property

Public Property Let OverTenderAllowed(ByVal Value As Boolean)
    Let mOverTenderAllowed = Value
End Property

Public Property Get DisplayOrder() As Long
    Let DisplayOrder = mDisplayOrder
End Property

Public Property Let DisplayOrder(ByVal Value As Long)
    Let mDisplayOrder = Value
End Property

Public Property Get TenderTypeID() As Long
    Let TenderTypeID = mTenderTypeID
End Property

Public Property Let TenderTypeID(ByVal Value As Long)
    Let mTenderTypeID = Value
End Property

Public Property Get Description() As String
    Let Description = mDescription
End Property

Public Property Let Description(ByVal Value As String)
    Let mDescription = Value
End Property

Public Property Let PrintChequeFront(ByVal Value As Boolean)
    Let mPrintChequeFront = Value
End Property

Public Property Get PrintChequeFront() As Boolean
    PrintChequeFront = mPrintChequeFront
End Property

Public Property Let PrintChequeBack(ByVal Value As Boolean)
    mPrintChequeBack = Value
End Property

Public Property Get PrintChequeBack() As Boolean
    PrintChequeBack = mPrintChequeBack
End Property

Public Property Let MinimumAccepted(ByVal Value As Long)
    mMinimumAccepted = Value
End Property

Public Property Get MinimumAccepted() As Long
    MinimumAccepted = mMinimumAccepted
End Property

Public Property Let MaximumAccepted(ByVal Value As Long)
    mMaximumAccepted = Value
End Property

Public Property Get MaximumAccepted() As Long
    MaximumAccepted = mMaximumAccepted
End Property

Public Property Let SupervisorRequired(ByVal Value As Boolean)
    mSupervisorRequired = Value
End Property

Public Property Get SupervisorRequired() As Boolean
    SupervisorRequired = mSupervisorRequired
End Property

Public Property Let OtherAuthorisationRequired(ByVal Value As Boolean)
    mOtherAuthorisationRequired = Value
End Property

Public Property Get OtherAuthorisationRequired() As Boolean
    OtherAuthorisationRequired = mOtherAuthorisationRequired
End Property

Public Property Let SecurityLevel(ByVal Value As Long)
    mSecurityLevel = Value
End Property

Public Property Get SecurityLevel() As Long
    SecurityLevel = mSecurityLevel
End Property

Public Property Let ConversionRate(ByVal Value As Double)
    mConversionRate = Value
End Property

Public Property Get ConversionRate() As Double
    ConversionRate = mConversionRate
End Property

Public Property Let ConversionType(ByVal Value As String)
    mConversionType = Value
End Property

Public Property Get ConversionType() As String
    ConversionType = mConversionType
End Property

Public Property Let TrailerFileName(ByVal Value As String)
    mTrailerFileName = Value
End Property

Public Property Get TrailerFileName() As String
    TrailerFileName = mTrailerFileName
End Property

Public Property Let CaptureCCDetails(ByVal Value As Long)
    mCaptureCCDetails = Value
End Property

Public Property Get CaptureCCDetails() As Long
    CaptureCCDetails = mCaptureCCDetails
End Property

Public Property Let CaptureAuthCode(ByVal Value As Long)
    mCaptureAuthCode = Value
End Property

Public Property Get CaptureAuthCode() As Long
    CaptureAuthCode = mCaptureAuthCode
End Property

Public Property Let CaptureCVVNo(ByVal Value As Long)
    mCaptureCVVNo = Value
End Property

Public Property Get CaptureCVVNo() As Long
    CaptureCVVNo = mCaptureCVVNo
End Property

Public Property Let FloorLimit(ByVal Value As Long)
    mFloorLimit = Value
End Property

Public Property Get FloorLimit() As Long
    FloorLimit = mFloorLimit
End Property

Public Property Let CreditLimitCheck(ByVal Value As Long)
    mCreditLimitCheck = Value
End Property

Public Property Get CreditLimitCheck() As Long
    CreditLimitCheck = mCreditLimitCheck
End Property

Public Property Let Active(ByVal Value As Boolean)
    mActive = Value
End Property

Public Property Get Active() As Boolean
    Active = mActive
End Property



Public Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_TILLTENDERTYPE * &H10000) + 1 To FID_TILLTENDERTYPE_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cTillTenderType

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_TILLTENDERTYPE, FID_TILLTENDERTYPE_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_TILLTENDERTYPE_TenderTypeKey):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mTenderTypeKey
        Case (FID_TILLTENDERTYPE_TenderTypeID):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mTenderTypeID
        Case (FID_TILLTENDERTYPE_Description):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDescription
        Case (FID_TILLTENDERTYPE_DisplayOrder):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mDisplayOrder
        Case (FID_TILLTENDERTYPE_OverTenderAllowed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOverTenderAllowed
        Case (FID_TILLTENDERTYPE_CaptureCreditCard):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mCaptureCreditCard
        Case (FID_TILLTENDERTYPE_UseEFTPOS):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mUseEFTPOS
        Case (FID_TILLTENDERTYPE_OpenCashDrawer):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOpenCashDrawer
        Case (FID_TILLTENDERTYPE_DefaultRemainingAmount):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDefaultRemainingAmount
        Case (FID_TILLTENDERTYPE_PrintChequeFront):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPrintChequeFront
        Case (FID_TILLTENDERTYPE_PrintChequeBack):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPrintChequeBack
        Case (FID_TILLTENDERTYPE_MinimumAccepted):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mMinimumAccepted
        Case (FID_TILLTENDERTYPE_MaximumAccepted):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mMaximumAccepted
        Case (FID_TILLTENDERTYPE_SupervisorRequired):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mSupervisorRequired
        Case (FID_TILLTENDERTYPE_OtherAuthorisationRequired):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mOtherAuthorisationRequired
        Case (FID_TILLTENDERTYPE_SecurityLevel):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSecurityLevel
        Case (FID_TILLTENDERTYPE_ConversionRate):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mConversionRate
        Case (FID_TILLTENDERTYPE_ConversionType):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mConversionType
        Case (FID_TILLTENDERTYPE_TrailerFileName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTrailerFileName
        Case (FID_TILLTENDERTYPE_CaptureCCDetails):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mCaptureCCDetails
        Case (FID_TILLTENDERTYPE_CaptureAuthCode):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mCaptureAuthCode
        Case (FID_TILLTENDERTYPE_CaptureCVVNo):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mCaptureCVVNo
        Case (FID_TILLTENDERTYPE_FloorLimit):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mFloorLimit
        Case (FID_TILLTENDERTYPE_CreditLimitCheck):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mCreditLimitCheck
        Case (FID_TILLTENDERTYPE_Active):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mActive
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cTillTenderType
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_TILLTENDERTYPE

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Tender Type " & mTenderTypeKey & "-" & mDescription

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_TILLTENDERTYPE_TenderTypeKey):          mTenderTypeKey = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_TenderTypeID):           mTenderTypeID = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_Description):            mDescription = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_DisplayOrder):           mDisplayOrder = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_OverTenderAllowed):      mOverTenderAllowed = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_CaptureCreditCard):      mCaptureCreditCard = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_UseEFTPOS):              mUseEFTPOS = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_OpenCashDrawer):         mOpenCashDrawer = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_DefaultRemainingAmount): mDefaultRemainingAmount = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_PrintChequeFront):       mPrintChequeFront = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_PrintChequeBack):        mPrintChequeBack = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_MinimumAccepted):        mMinimumAccepted = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_MaximumAccepted):        mMaximumAccepted = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_SupervisorRequired):     mSupervisorRequired = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_OtherAuthorisationRequired): mOtherAuthorisationRequired = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_SecurityLevel):          mSecurityLevel = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_ConversionRate):         mConversionRate = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_ConversionType):         mConversionType = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_TrailerFileName):        mTrailerFileName = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_CaptureCCDetails):       mCaptureCCDetails = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_CaptureAuthCode):        mCaptureAuthCode = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_CaptureCVVNo):           mCaptureCVVNo = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_FloorLimit):             mFloorLimit = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_CreditLimitCheck):       mCreditLimitCheck = oField.ValueAsVariant
            Case (FID_TILLTENDERTYPE_Active):                 mActive = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_TILLTENDERTYPE_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cTillTenderType

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


