VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTenderTypes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"3E2BC859026C"
'<CAMH>****************************************************************************************
'* Module : cTenderTypes
'* Date   : 20/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/LookUps/cTenderTypes.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 1/28/03 3:14p $
'* $Revision: 2 $
'* Versions:
'* 20/01/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cTenderTypes"

'##ModelId=3E2BC8A30226
Private mKey As String ' = 01

'##ModelId=3E2BC8A8021C
Private mDescription(20) As String

'##ModelId=3E2BC8AE02BC
Private mTenderType(20) As Long

'##ModelId=3E2BC8BC00D2
Private mDisplayOrder(20) As Long

'##ModelId=3E2BC8C001EA
Private mOverTenderAllowed(20) As Boolean

'##ModelId=3E2BC8CC0244
Private mCaptureCreditCard(20) As Boolean

'##ModelId=3E2BC8D303DE
Private mUseEFTPOS(20) As Boolean

'##ModelId=3E2BC8D903D4
Private mOpenCashDrawer(20) As Boolean

'##ModelId=3E2BC8E203A2
Private mDefaultRemainingAmount(20) As Boolean

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=3E2BCBB302D0
Public Property Get DefaultRemainingAmount(Index As Variant) As Boolean
    Let DefaultRemainingAmount = mDefaultRemainingAmount(Index)
End Property

'##ModelId=3E2BCBB3014A
Public Property Let DefaultRemainingAmount(Index As Variant, ByVal Value As Boolean)
    Let mDefaultRemainingAmount(Index) = Value
End Property

'##ModelId=3E2BCBB300AA
Public Property Get OpenCashDrawer(Index As Variant) As Boolean
    Let OpenCashDrawer = mOpenCashDrawer(Index)
End Property

'##ModelId=3E2BCBB2037A
Public Property Let OpenCashDrawer(Index As Variant, ByVal Value As Boolean)
    Let mOpenCashDrawer(Index) = Value
End Property

'##ModelId=3E2BCBB202DA
Public Property Get UseEFTPOS(Index As Variant) As Boolean
    Let UseEFTPOS = mUseEFTPOS(Index)
End Property

'##ModelId=3E2BCBB201C2
Public Property Let UseEFTPOS(Index As Variant, ByVal Value As Boolean)
    Let mUseEFTPOS(Index) = Value
End Property

'##ModelId=3E2BCBB20122
Public Property Get CaptureCreditCard(Index As Variant) As Boolean
    Let CaptureCreditCard = mCaptureCreditCard(Index)
End Property

'##ModelId=3E2BCBB20046
Public Property Let CaptureCreditCard(Index As Variant, ByVal Value As Boolean)
    Let mCaptureCreditCard(Index) = Value
End Property

'##ModelId=3E2BCBB10384
Public Property Get OverTenderAllowed(Index As Variant) As Boolean
    Let OverTenderAllowed = mOverTenderAllowed(Index)
End Property

'##ModelId=3E2BCBB102A8
Public Property Let OverTenderAllowed(Index As Variant, ByVal Value As Boolean)
    Let mOverTenderAllowed(Index) = Value
End Property

'##ModelId=3E2BCBB10208
Public Property Get DisplayOrder(Index As Variant) As Long
    Let DisplayOrder = mDisplayOrder(Index)
End Property

'##ModelId=3E2BCBB1012C
Public Property Let DisplayOrder(Index As Variant, ByVal Value As Long)
    Let mDisplayOrder(Index) = Value
End Property

'##ModelId=3E2BCBB100BE
Public Property Get TenderType(Index As Variant) As Long
    Let TenderType = mTenderType(Index)
End Property

'##ModelId=3E2BCBB003CA
Public Property Let TenderType(Index As Variant, ByVal Value As Long)
    Let mTenderType(Index) = Value
End Property

'##ModelId=3E2BCBB0035C
Public Property Get Description(Index As Variant) As String
    Let Description = mDescription(Index)
End Property

'##ModelId=3E2BCBB002BC
Public Property Let Description(Index As Variant, ByVal Value As String)
    Let mDescription(Index) = Value
End Property

Private Sub Class_Initialize()

    mKey = "01"

End Sub

Public Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_TENDERTYPES * &H10000) + 1 To FID_TENDERTYPES_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)

End Function 'IBo_AddLoadFilter

Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function

Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cTenderTypes

End Function


Private Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_TENDERTYPES, FID_TENDERTYPES_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_TENDERTYPES_Key):               Set GetField = New CFieldString
                                                      GetField.ValueAsVariant = mKey
        Case (FID_TENDERTYPES_Description):       Set GetField = SaveStringArray(mDescription, m_oSession)
        Case (FID_TENDERTYPES_TenderType):        Set GetField = SaveLongArray(mTenderType, m_oSession)
        Case (FID_TENDERTYPES_DisplayOrder):      Set GetField = SaveLongArray(mDisplayOrder, m_oSession)
        Case (FID_TENDERTYPES_OverTenderAllowed): Set GetField = SaveBooleanArray(mOverTenderAllowed, m_oSession)
        Case (FID_TENDERTYPES_CaptureCreditCard): Set GetField = SaveBooleanArray(mCaptureCreditCard, m_oSession)
        Case (FID_TENDERTYPES_UseEFTPOS):         Set GetField = SaveBooleanArray(mUseEFTPOS, m_oSession)
        Case (FID_TENDERTYPES_OpenCashDrawer):    Set GetField = SaveBooleanArray(mOpenCashDrawer, m_oSession)
        Case (FID_TENDERTYPES_DefaultRemainingAmount): Set GetField = SaveBooleanArray(mDefaultRemainingAmount, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cTenderTypes
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_TENDERTYPES

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Tender Types " & mKey

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_TENDERTYPES_Key):               mKey = oField.ValueAsVariant
            Case (FID_TENDERTYPES_Description):       Call LoadStringArray(mDescription, oField, m_oSession)
            Case (FID_TENDERTYPES_TenderType):        Call LoadLongArray(mTenderType, oField, m_oSession)
            Case (FID_TENDERTYPES_DisplayOrder):      Call LoadLongArray(mDisplayOrder, oField, m_oSession)
            Case (FID_TENDERTYPES_OverTenderAllowed): Call LoadBooleanArray(mOverTenderAllowed, oField, m_oSession)
            Case (FID_TENDERTYPES_CaptureCreditCard): Call LoadBooleanArray(mCaptureCreditCard, oField, m_oSession)
            Case (FID_TENDERTYPES_UseEFTPOS):         Call LoadBooleanArray(mUseEFTPOS, oField, m_oSession)
            Case (FID_TENDERTYPES_OpenCashDrawer):    Call LoadBooleanArray(mOpenCashDrawer, oField, m_oSession)
            Case (FID_TENDERTYPES_DefaultRemainingAmount): Call LoadBooleanArray(mDefaultRemainingAmount, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_TENDERTYPES_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cTenderTypes

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


