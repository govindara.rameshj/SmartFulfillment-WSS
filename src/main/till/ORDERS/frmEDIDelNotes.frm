VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "fpSPR70.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmEDIDelNotes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Retrieve Electronic Delivery Note"
   ClientHeight    =   7515
   ClientLeft      =   1680
   ClientTop       =   960
   ClientWidth     =   7470
   Icon            =   "frmEDIDelNotes.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   7470
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   495
      Left            =   5040
      TabIndex        =   6
      Top             =   6480
      Width           =   975
   End
   Begin FPSpread.vaSpread sprdDNotes 
      Height          =   5055
      Left            =   240
      TabIndex        =   4
      Top             =   1200
      Width           =   6975
      _Version        =   393216
      _ExtentX        =   12303
      _ExtentY        =   8916
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   3
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      ScrollBars      =   2
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmEDIDelNotes.frx":058A
      UserResize      =   0
   End
   Begin VB.CommandButton cmdRetrieve 
      Caption         =   "F5-Retrieve"
      Height          =   495
      Left            =   6240
      TabIndex        =   7
      Top             =   6480
      Width           =   975
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "F12-Close"
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   6480
      Width           =   975
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   7140
      Width           =   7470
      _ExtentX        =   13176
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmEDIDelNotes.frx":097A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5345
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:04"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblOrderNo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label lblSupplier 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1560
      TabIndex        =   3
      Top             =   600
      Width           =   4815
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Order No"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Supplier"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
End
Attribute VB_Name = "frmEDIDelNotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const COL_DELNOTE As Long = 1
Const COL_DELDATE As Long = 2
Const COL_COLPOS  As Long = 3

Private moEDIDelNote As Object
Private mcolHeaders  As Collection

Public Function RetrieveEDIDelNote(strOrderNumber As String, _
                                   strSupplierName As String) As Object
                              
Dim oDelNote    As Object
Dim lngHeaderNo As Long
Dim lngLineNo   As Long

    lblOrderNo.Caption = strOrderNumber
    lblSupplier.Caption = strSupplierName
        
    Set moEDIDelNote = Nothing
    
    Set oDelNote = goDatabase.CreateBusinessObject(CLASSID_EDIDELHEADER)
    Call oDelNote.IBo_AddLoadFilter(CMP_EQUAL, FID_EDIDELHEADER_PurchaseOrderNumber, strOrderNumber)
    Call oDelNote.IBo_AddLoadFilter(CMP_EQUAL, FID_EDIDELHEADER_ReceivedInDate, "")
    
    Set mcolHeaders = oDelNote.IBo_Loadmatches
    
    sprdDNotes.MaxRows = 0
    For lngHeaderNo = 1 To mcolHeaders.Count Step 1
        sprdDNotes.MaxRows = sprdDNotes.MaxRows + 1
        sprdDNotes.Row = sprdDNotes.MaxRows
        sprdDNotes.Col = COL_DELNOTE
        sprdDNotes.Text = mcolHeaders(CLng(lngHeaderNo)).DeliveryNoteNumber
        sprdDNotes.Col = COL_DELDATE
        sprdDNotes.Text = DisplayDate(mcolHeaders(CLng(lngHeaderNo)).DeliveryDate, False)
        sprdDNotes.Col = COL_COLPOS
        sprdDNotes.Text = lngHeaderNo
    Next lngHeaderNo
    
    Set oDelNote = Nothing
    
    If sprdDNotes.MaxRows > 0 Then Call Me.Show(vbModal)
    DoEvents
    
    Set RetrieveEDIDelNote = moEDIDelNote
    
End Function

Private Sub cmdClose_Click()

    Set moEDIDelNote = Nothing
    Call Me.Hide
    
End Sub

Private Sub cmdPrint_Click()

    sprdDNotes.PrintHeader = "/cPending EDI Delivery Notes" & vbCrLf & "/cOrder No : " & lblOrderNo.Caption & " from " & lblSupplier.Caption
    sprdDNotes.PrintFooter = GetFooter
    Call sprdDNotes.PrintSheet

End Sub

Private Sub cmdRetrieve_Click()

    sprdDNotes.Row = sprdDNotes.ActiveRow
    sprdDNotes.Col = COL_COLPOS
    Set moEDIDelNote = mcolHeaders(Val(sprdDNotes.Text))
    Call Me.Hide
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF5): 'if F5 then call save/process
                    KeyCode = 0
                    Call cmdRetrieve_Click
                Case (vbKeyF9):  'if F9 then call print list
                    KeyCode = 0
                    Call cmdPrint_Click
                Case (vbKeyF12): 'if F12 then close
                    KeyCode = 0
                    Call cmdClose_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select
    
End Sub

Private Sub Form_Load()
    
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

End Sub

Private Sub sprdDNotes_KeyPress(KeyAscii As Integer)

    If sprdDNotes.MaxRows > 0 Then Call cmdRetrieve_Click
        
End Sub
