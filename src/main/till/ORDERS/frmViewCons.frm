VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "fpSPR70.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmViewCons 
   Caption         =   "View purchase order consolidations"
   ClientHeight    =   5715
   ClientLeft      =   795
   ClientTop       =   450
   ClientWidth     =   10215
   Icon            =   "frmViewCons.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5715
   ScaleWidth      =   10215
   WindowState     =   2  'Maximized
   Begin FPSpread.vaSpread sprdCons 
      Height          =   4215
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   9975
      _Version        =   393216
      _ExtentX        =   17595
      _ExtentY        =   7435
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   16
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmViewCons.frx":058A
      UserResize      =   0
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   9000
      TabIndex        =   3
      Top             =   4800
      Width           =   1095
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   4800
      Width           =   1095
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   5340
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmViewCons.frx":0EED
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10186
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "14:08"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblOrderDate 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   4320
      TabIndex        =   7
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Order Date"
      Height          =   255
      Left            =   3360
      TabIndex        =   6
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblOrderNo 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   960
      TabIndex        =   1
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Order No."
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "frmViewCons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmViewCons
'* Date   : 09/01/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Orders/frmViewCons.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 23/10/03 11:08 $ $Revision: 6 $
'* Versions:
'* 09/01/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmViewCons"

Private mstrSupplier As String
Private mstrDelNoteNumber As String
Private mdteDelNoteDate   As Date

Private Sub SetQuantityPlaces()

Dim lngQtyDecNum   As Long
Dim lngValueDecNum As Long

    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    lngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)

    sprdCons.Row = -1
    sprdCons.Col = COL_CONS_PORDQTY
    sprdCons.TypeNumberDecPlaces = lngQtyDecNum
    sprdCons.Col = COL_CONS_CORDQTY
    sprdCons.TypeNumberDecPlaces = lngQtyDecNum
    sprdCons.Col = COL_CONS_PRN_CORDLINEQTY
    sprdCons.TypeNumberDecPlaces = lngQtyDecNum
    sprdCons.Col = COL_CONS_SELLUNITS
    sprdCons.TypeNumberDecPlaces = lngQtyDecNum
    
    sprdCons.Col = COL_CONS_PRICE
    sprdCons.TypeNumberDecPlaces = lngValueDecNum
    sprdCons.Col = COL_CONS_COST
    sprdCons.TypeNumberDecPlaces = lngValueDecNum

End Sub
Public Sub SetConsolidateLine(strPartCode As String, lngPOLineNumber As Long, dblTotalQty As Double)

Dim lngLineNo As Long

    For lngLineNo = 1 To Me.sprdCons.MaxRows Step 1
        sprdCons.Col = COL_CONS_PARTCODE
        sprdCons.Row = lngLineNo
        If sprdCons.Text = strPartCode Then
            sprdCons.Col = COL_CONS_PORDQTY
            sprdCons.Text = dblTotalQty
            sprdCons.Col = COL_CONS_POLINENO
            sprdCons.Text = lngPOLineNumber
        End If
    Next lngLineNo

End Sub

Public Sub DisplayConsolidation(strPurchaseOrderNumber As String, _
                                dtePurchaseOrderDate As Date, _
                                Optional mcolConsolLines As Collection = Nothing, _
                                Optional ByRef sprdOrder As vaSpread = Nothing, _
                                Optional ByVal lngColDesc As Long = 0, _
                                Optional ByVal lngColOrderQty As Long = 0, _
                                Optional ByVal lngColSize As Long = 0, _
                                Optional ByVal strSupplier As String)

Dim lngLineNo     As Long
Dim colCustNames  As New Collection
Dim strCustName   As String
Dim oCOrder       As Object

    If (mstrSupplier = "") And (strSupplier <> "") Then mstrSupplier = strSupplier
    If mcolConsolLines Is Nothing Then Exit Sub

    sprdCons.Col = COL_CONS_ITEMNO
    sprdCons.ColHidden = True
    sprdCons.Col = COL_CONS_CORDQTY
    sprdCons.ColHidden = True
    sprdCons.Col = COL_CONS_POLINENO
    sprdCons.ColHidden = True
    lblOrderNo.Caption = strPurchaseOrderNumber
    lblOrderDate.Caption = DisplayDate(dtePurchaseOrderDate, False)
    sprdCons.MaxRows = mcolConsolLines.Count
    For lngLineNo = 1 To mcolConsolLines.Count Step 1
        sprdCons.Row = lngLineNo
        sprdCons.Col = COL_CONS_ITEMNO
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).LineNumber
        sprdCons.Col = COL_CONS_PARTCODE
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).PartCode
        If (sprdOrder Is Nothing) = False Then
            sprdOrder.Col = lngColDesc
            sprdOrder.Row = mcolConsolLines(CLng(lngLineNo)).PurchaseOrderLineNo + 1
            sprdCons.Col = COL_CONS_DESC
            sprdCons.Text = sprdOrder.Text
            sprdCons.Col = COL_CONS_PORDQTY
            sprdOrder.Col = lngColOrderQty
            sprdCons.Text = sprdOrder.Text
            'Transfer Item Size
            sprdCons.Col = COL_CONS_SIZE
            sprdOrder.Col = lngColSize
            sprdCons.Text = sprdOrder.Text
        End If
        sprdCons.Col = COL_CONS_CORDQTY
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).Quantity
        sprdCons.Col = COL_CONS_PRN_CORDLINEQTY
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).Quantity
        sprdCons.Col = COL_CONS_MANUCODE
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).ProductCode
        sprdCons.Col = COL_CONS_PRICE
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).Price
        sprdCons.Col = COL_CONS_COST
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).Cost
        sprdCons.Col = COL_CONS_SELLUNITS
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).QuantityPerUnit
        sprdCons.Col = COL_CONS_CORDNO
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).CustOrderNumber
        'Check if item for stock
        If sprdCons.Text = "" Then
            sprdCons.BackColor = RGB_GREY
        Else 'If not for stock then show Customer Name
            strCustName = ""
            On Error Resume Next
            'attempt to get Customer Name from list of previously retrieved orders
            strCustName = colCustNames(sprdCons.Text)
            If Err.Number <> 0 Then 'if not found then get from database
                Set oCOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
                Call oCOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, sprdCons.Text)
                Call oCOrder.IBo_AddLoadField(FID_SOHEADER_Name)
                Call oCOrder.IBo_LoadMatches
                strCustName = oCOrder.Name
                Set oCOrder = Nothing
                'Add retrieved name to list, so that it does not have to be retrieved again
                Call colCustNames.Add(strCustName, sprdCons.Text)
            End If
            Call Err.Clear
            'Display Customer name for Order
            sprdCons.Col = COL_CONS_CORDNAME
            sprdCons.Text = strCustName
        End If
        sprdCons.Col = COL_CONS_CORDLINENO
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).CustOrderLineNumber
        sprdCons.Col = COL_CONS_PRN_POLINENO
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).PurchaseOrderLineNo
        sprdCons.Col = COL_CONS_POLINENO
        sprdCons.Text = mcolConsolLines(CLng(lngLineNo)).PurchaseOrderLineNo
    Next lngLineNo
    
    sprdCons.Row = 1
    sprdCons.Col = COL_CONS_POLINENO
    strCustName = sprdCons.Text
    For lngLineNo = 2 To sprdCons.MaxRows Step 1
        sprdCons.Row = lngLineNo
        sprdCons.Col = COL_CONS_PRN_POLINENO
        If strCustName = sprdCons.Text Then
            sprdCons.Col = COL_CONS_ITEMNO
            sprdCons.Col2 = COL_CONS_MANUCODE
            sprdCons.Row2 = lngLineNo
            sprdCons.BlockMode = True
            sprdCons.Text = ""
            sprdCons.BlockMode = True
        Else
            strCustName = sprdCons.Text
        End If
    Next

End Sub

Private Sub cmdExit_Click()

    Call Me.Hide
'    Unload Me

End Sub

Private Sub cmdExit_GotFocus()

    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Public Sub PrintConsolidationSheet(strPurchaseOrderNumber As String, _
                                   dtePurchaseOrderDate As Date, _
                                   mcolConsolLines As Collection, _
                                   ByRef sprdOrder As vaSpread, _
                                   ByVal lngColDesc As Long, _
                                   ByVal lngColOrderQty As Long, _
                                   ByVal lngColSize As Long, _
                                   ByVal strSupplier As String, _
                                   ByVal strDelNoteNumber As String, _
                                   ByVal dteDelNoteDate As Date)
                                   
                                   
    mstrDelNoteNumber = strDelNoteNumber
    mdteDelNoteDate = dteDelNoteDate
    mstrSupplier = strSupplier
    Call DisplayConsolidation(strPurchaseOrderNumber, dtePurchaseOrderDate, mcolConsolLines, sprdOrder, lngColDesc, lngColOrderQty, lngColSize, strSupplier)
    Call cmdPrint_Click
    Unload Me

End Sub

Private Sub cmdPrint_Click()

Const COL_CONS_ALL As Long = -1

Dim lngMaxRows As Long
    
    sprdCons.ColWidth(COL_CONS_ITEMNO) = 0
    sprdCons.Col = COL_CONS_CORDQTY
    sprdCons.ColHidden = True
    sprdCons.Col = COL_CONS_POLINENO
    sprdCons.ColHidden = True
    sprdCons.Col = COL_CONS_PRN_CORDLINEQTY
    sprdCons.ColHidden = False
    lngMaxRows = sprdCons.MaxRows
    Call SetLastPrintCol
    
    'add extra rows to hold Order Header
    sprdCons.MaxRows = sprdCons.MaxRows + 9 'Add 7 rows for header + 2 footer
    Call sprdCons.InsertRows(1, 7)
    
    'Clear out Header Grid Lines
    sprdCons.Col = 1
    sprdCons.Col2 = sprdCons.MaxCols
    sprdCons.Row = 1
    sprdCons.Row2 = 7
    sprdCons.BlockMode = True
    sprdCons.CellType = CellTypeStaticText
    sprdCons.BlockMode = False
    
    'Create Order Header
    'Put in Logo
    sprdCons.Row = 1
    sprdCons.Col = COL_CONS_ITEMNO
    sprdCons.CellType = CellTypeEdit
    Call sprdCons.AddCellSpan(COL_CONS_ITEMNO, 1, sprdCons.MaxCols, 1)
    sprdCons.Text = "PURCHASE ORDER - Consolidation Sheet"
    sprdCons.TypeHAlign = TypeHAlignCenter
    sprdCons.FontSize = 11
    sprdCons.FontBold = True
    sprdCons.Col = COL_CONS_CORDQTY
    'Create Order Header
    sprdCons.Col = 2
    sprdCons.Row = 2
    sprdCons.Text = "Supplier"
    'Call sprdCons.AddCellSpan(5, 2, , 5)
    Call sprdCons.AddCellSpan(COL_CONS_PARTCODE, 2, 2, 5)
    sprdCons.Col = COL_CONS_PARTCODE
    sprdCons.Row = 2
    sprdCons.TypeEditMultiLine = True
    sprdCons.CellType = CellTypeEdit
    sprdCons.TypeMaxEditLen = 500
    sprdCons.Text = mstrSupplier
    
    sprdCons.Col = COL_CONS_PORDQTY
    sprdCons.Row = 2
    sprdCons.Text = "Doc Number"
    sprdCons.Row = 3
    sprdCons.Text = "Receipt Date"
    sprdCons.Row = 4
    sprdCons.Text = "Raised By"
    sprdCons.Row = 5
    sprdCons.Text = "PO Number"
    sprdCons.Row = 6
    sprdCons.Text = "Order Date"
    
    sprdCons.Col = COL_CONS_CORDNAME
    sprdCons.Row = 2
    sprdCons.Text = mstrDelNoteNumber
    sprdCons.Row = 3
    sprdCons.Text = DisplayDate(mdteDelNoteDate, False)
    sprdCons.Row = 4
    sprdCons.Text = goSession.UserInitials
    sprdCons.Row = 5
    sprdCons.Text = goSession.CurrentEnterprise.IEnterprise_StoreNumber & "/" & lblOrderNo.Caption
    sprdCons.Row = 6
    sprdCons.Text = lblOrderDate.Caption

    'Draw boxes around Order Addresses(3 Off) and Summary
    Call sprdCons.SetCellBorder(1, 1, sprdCons.MaxCols, 6, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdCons.SetCellBorder(1, 1, sprdCons.MaxCols, 3, 16, 192, CellBorderStyleSolid)
    Call sprdCons.SetCellBorder(1, 2, 5, 6, 16, 192, CellBorderStyleSolid)
    Call sprdCons.SetCellBorder(COL_CONS_PORDQTY, 2, sprdCons.MaxCols, 6, 16, 192, CellBorderStyleSolid)
    
    'Display Order Footer
    
    sprdCons.RowHeight(7) = 20
    'Display Line item column headings
    sprdCons.Row = 7
    sprdCons.Col = COL_CONS_ALL
    sprdCons.FontBold = True
    sprdCons.Col = COL_CONS_ITEMNO
    sprdCons.CellType = CellTypeStaticText
    sprdCons.TypeEditMultiLine = True
    sprdCons.Text = "Item No"
    sprdCons.Col = COL_CONS_PARTCODE
    sprdCons.Text = "SKU"
    sprdCons.Col = COL_CONS_DESC
    sprdCons.Text = "Description"
    sprdCons.Col = COL_CONS_PORDQTY
    sprdCons.CellType = CellTypeEdit
    sprdCons.TypeEditMultiLine = True
    sprdCons.Text = "Total Quantity"
    sprdCons.Col = COL_CONS_SIZE
    sprdCons.Text = "Size"
    sprdCons.Col = COL_CONS_CORDNAME
    sprdCons.Text = "Customer Name"
    sprdCons.Col = COL_CONS_PRN_CORDLINEQTY
    sprdCons.CellType = CellTypeEdit
    sprdCons.TypeEditMultiLine = True
    sprdCons.Text = "Customer Quantity"
    sprdCons.Col = COL_CONS_CORDQTY
    sprdCons.CellType = CellTypeEdit
    sprdCons.TypeEditMultiLine = True
    sprdCons.Text = "Line Quantity"
    sprdCons.Col = COL_CONS_MANUCODE
    sprdCons.Text = "Manu Code"
    sprdCons.Col = COL_CONS_PRN_POLINENO
    sprdCons.CellType = CellTypeEdit
    sprdCons.TypeEditMultiLine = True
    sprdCons.Text = "P.Order Line No"
    sprdCons.Col = COL_CONS_PRICE
    sprdCons.Text = "Price"
    sprdCons.Col = COL_CONS_COST
    sprdCons.Text = "Cost"
    sprdCons.Col = COL_CONS_CORDNO
    sprdCons.Text = "Order No"
    
    'clear out last line and create block around Transmission status
    Call sprdCons.SetCellBorder(1, sprdCons.MaxRows - 1, sprdCons.MaxCols, sprdCons.MaxRows, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdCons.SetCellBorder(COL_CONS_PRN_POLINENO, sprdCons.MaxRows - 1, sprdCons.MaxCols, sprdCons.MaxRows - 1, 16, RGB_BLACK, CellBorderStyleSolid)
    
    sprdCons.Col = COL_CONS_DESC
    sprdCons.Row = sprdCons.MaxRows - 1
    sprdCons.Text = "END OF SHEET"
    sprdCons.FontBold = True

    'Perform print functions
    sprdCons.PrintColHeaders = False
    sprdCons.PrintRowHeaders = False
    sprdCons.PrintSmartPrint = False
    sprdCons.PrintGrid = False
    sprdCons.PrintFooter = GetFooter
    
    sprdCons.PrintJobName = "Consolidation Sheet Purchase Order - " & lblOrderNo.Caption
    Call sprdCons.PrintSheet
       
    'Remove header
    Call sprdCons.DeleteRows(1, 7)
    sprdCons.MaxRows = lngMaxRows
    sprdCons.Row = -1
    sprdCons.Col = COL_CONS_ALL
    sprdCons.FontBold = False

End Sub

Private Sub cmdPrint_GotFocus()
    
    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF9):  'if F9 then call print document
                    Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    KeyCode = 0
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

Private Sub Form_Load()

    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    Call SetQuantityPlaces

End Sub
Private Sub SetLastPrintCol()

Dim dblSpreadSize As Double
Dim lngColNo      As Long
Dim lngLastCol        As Long
Dim dblOrigWidth As Double
    
    'Once columns have been hidden, locate last column displayed and calculate width
    For lngColNo = 1 To sprdCons.MaxCols Step 1
        dblSpreadSize = dblSpreadSize + sprdCons.ColWidth(lngColNo)
        sprdCons.Col = lngColNo
        If sprdCons.ColHidden = False Then lngLastCol = lngColNo
    Next lngColNo
        
    'set last column width to fill in page width
    dblOrigWidth = sprdCons.ColWidth(lngLastCol)
    sprdCons.ColWidth(lngLastCol) = 90 - dblSpreadSize + sprdCons.ColWidth(lngLastCol)

End Sub

Public Function SaveConsolidation(lngOrderKey As Long, strOrderNumber As String, dteOrderDate As Date, blnPrintSheet As Boolean, strSupplier As String) As Boolean

Dim oConsLine     As Object
Dim lngLineNo     As Long
Dim colCustNames  As New Collection
Dim strCustName   As String
Dim oCOrder       As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".SaveConsolidation"

    If strSupplier <> "" Then mstrSupplier = strSupplier
    lblOrderNo.Caption = strOrderNumber
    lblOrderDate.Caption = DisplayDate(dteOrderDate, False)
    Call SetLastPrintCol

    'Sort items so that they are in the same order as the Purchase Order
    sprdCons.SortKey(1) = COL_CONS_POLINENO
    sprdCons.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdCons.Sort(1, 1, sprdCons.MaxCols, sprdCons.MaxRows, SortByRow)
    
    SaveConsolidation = False
    
    'Step through each line and save to database
    For lngLineNo = 1 To sprdCons.MaxRows Step 1
        Set oConsLine = goDatabase.CreateBusinessObject(CLASSID_PURCHASECONSLINE)

        oConsLine.PurchaseOrderKey = lngOrderKey
        oConsLine.PurchaseOrderNumber = strOrderNumber
        oConsLine.Complete = False
        oConsLine.OrderDate = dteOrderDate
        
        sprdCons.Row = lngLineNo
        sprdCons.Col = COL_CONS_ITEMNO
        oConsLine.LineNumber = sprdCons.Text
        sprdCons.Col = COL_CONS_PARTCODE
        oConsLine.PartCode = sprdCons.Text
        sprdCons.Col = COL_CONS_CORDQTY
        oConsLine.Quantity = Val(sprdCons.Text)
        sprdCons.Col = COL_CONS_PRN_CORDLINEQTY
        sprdCons.Text = oConsLine.Quantity
        sprdCons.Col = COL_CONS_MANUCODE
        oConsLine.ProductCode = sprdCons.Text
        sprdCons.Col = COL_CONS_PRICE
        oConsLine.Price = Val(sprdCons.Text)
        sprdCons.Col = COL_CONS_COST
        oConsLine.Cost = Val(sprdCons.Text)
        sprdCons.Col = COL_CONS_SELLUNITS
        oConsLine.QuantityPerUnit = Val(sprdCons.Text)
        
        sprdCons.Col = COL_CONS_POLINENO
        oConsLine.PurchaseOrderLineNo = sprdCons.Text
        sprdCons.Col = COL_CONS_PRN_POLINENO
        sprdCons.Text = oConsLine.PurchaseOrderLineNo
        sprdCons.Col = COL_CONS_CORDNO
        oConsLine.CustOrderNumber = sprdCons.Text
        sprdCons.Col = COL_CONS_CORDLINENO
        oConsLine.CustOrderLineNumber = sprdCons.Text
        Call oConsLine.IBo_SaveIfNew

    Next lngLineNo
    
    If blnPrintSheet Then Call cmdPrint_Click
    SaveConsolidation = True
        
End Function 'SaveConsolidation


Private Sub Form_Resize()

    If Me.WindowState = vbMinimized Then Exit Sub
    sprdCons.Width = Me.Width - sprdCons.Left * 3
    sprdCons.Height = Me.Height - (sbStatus.Height * 2) - sprdCons.Top - cmdPrint.Height - 240
    cmdPrint.Top = sprdCons.Top + sprdCons.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdExit.Left = sprdCons.Left + sprdCons.Width - cmdExit.Width

End Sub

