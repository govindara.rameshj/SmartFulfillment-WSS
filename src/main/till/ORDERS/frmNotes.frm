VERSION 5.00
Begin VB.Form frmNotes 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Enter Notes"
   ClientHeight    =   2130
   ClientLeft      =   3210
   ClientTop       =   2340
   ClientWidth     =   5070
   Icon            =   "frmNotes.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   5070
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbReasons 
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   720
      Visible         =   0   'False
      Width           =   4815
   End
   Begin VB.CommandButton cmdList 
      Caption         =   "F4-List"
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   1560
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox txtNotes1 
      Height          =   285
      Left            =   120
      MaxLength       =   40
      TabIndex        =   2
      Top             =   720
      Width           =   4815
   End
   Begin VB.TextBox txtNotes2 
      Height          =   285
      Left            =   120
      MaxLength       =   40
      TabIndex        =   3
      Top             =   1080
      Width           =   4815
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "F5-Complete"
      Height          =   375
      Left            =   2280
      TabIndex        =   4
      Top             =   1560
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "F10-Cancel"
      Height          =   375
      Left            =   3720
      TabIndex        =   5
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label lblInstruction 
      BackStyle       =   0  'Transparent
      Caption         =   "Please enter remarks"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4815
   End
End
Attribute VB_Name = "frmNotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmNotes
'* Date   : 04/02/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Orders/frmNotes.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 23/10/03 11:09 $
'* $Revision: 15 $
'* Versions:
'* 04/02/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmNotes"

Dim mblnCaptured As Boolean

Public Function CaptureNotes(ByRef strNotes As String, ByRef strReasons As String, ByVal NotesCode As enActionType) As Boolean

Dim vntReasons
Dim lngReasonNo As Long

    mblnCaptured = False
    If InStr(strNotes, vbCrLf) > 0 Then
        If InStr(strNotes, vbCrLf) > 1 Then txtNotes1.Text = Left$(strNotes, InStr(strNotes, vbCrLf) - 1)
        strNotes = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
        strNotes = Replace(strNotes, vbCrLf, "")
        txtNotes2.Text = strNotes
    Else
        txtNotes1.Text = strNotes
    End If
    If strReasons = "" Then strReasons = FillInLineComments(NotesCode, False)
    If strReasons <> "" Then
        vntReasons = Split(strReasons, vbTab)
        For lngReasonNo = 0 To UBound(vntReasons) Step 1
            If vntReasons(lngReasonNo) <> "" Then
                cmbReasons.AddItem (vntReasons(lngReasonNo))
                If vntReasons(lngReasonNo) = txtNotes1.Text Then cmbReasons.ListIndex = cmbReasons.NewIndex
            End If
        Next lngReasonNo
        txtNotes1.Enabled = False
        cmbReasons.Visible = True
        If cmbReasons.ListIndex = -1 Then cmbReasons.ListIndex = 0 'force to use item 1 in list
    End If
    Call DebugMsg(MODULE_NAME, "CaptureNotes", endlDebug, cmbReasons.ListCount & " " & strReasons)
    Call Me.Show(vbModal)
    strNotes = txtNotes1.Text & vbCrLf & txtNotes2.Text
    If strNotes = vbCrLf Then strNotes = ""
    CaptureNotes = mblnCaptured

End Function

Public Sub ViewNotes(ByVal strNotes As String)

    Call DebugMsg(MODULE_NAME, "ViewNotes", endlDebug)
    txtNotes1.Enabled = False
    txtNotes2.Enabled = False
    cmdList.Visible = False
    cmdProcess.Visible = False
    Caption = "View Notes"
    lblInstruction.Caption = "View notes"
    If InStr(strNotes, vbCrLf) > 0 Then
        If InStr(strNotes, vbCrLf) > 1 Then txtNotes1.Text = Left$(strNotes, InStr(strNotes, vbCrLf) - 1)
        strNotes = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
        strNotes = Replace(strNotes, vbCrLf, "")
        txtNotes2.Text = strNotes
    Else
        txtNotes1.Text = strNotes
    End If
    Call Me.Show(vbModal)

End Sub

Private Sub cmbReasons_GotFocus()

    Call SendKeys("%{down}")

End Sub

Private Sub cmbReasons_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        txtNotes1.Text = cmbReasons.Text
'        cmbReasons.Visible = False
        txtNotes2.SetFocus
    End If
    
    If KeyAscii = vbKeyEscape Then KeyAscii = 0

End Sub

Private Sub cmbReasons_LostFocus()

        txtNotes1.Text = cmbReasons.Text
'    cmbReasons.Visible = False

End Sub

Private Sub cmdCancel_Click()

    mblnCaptured = False
    Me.Hide

End Sub

Private Sub cmdCancel_GotFocus()

    cmdCancel.FontBold = True

End Sub

Private Sub cmdCancel_LostFocus()

    cmdCancel.FontBold = False
    
End Sub

Private Sub cmdList_Click()

    cmbReasons.Visible = True
    DoEvents
    Call cmbReasons.SetFocus

End Sub

Private Sub cmdList_LostFocus()

    cmbReasons.Visible = False

End Sub

Private Sub cmdProcess_Click()
    
    If (txtNotes1.Text = "") And (cmbReasons.ListCount > 0) Then txtNotes1.Text = cmbReasons.Text
    If (cmbReasons.ListCount > 0) And (txtNotes1.Text = "") Then
        Call MsgBoxEx("No comment entered on line 1" & vbCrLf & "Select from list", vbOKOnly, "Save Notes", , , , , RGBMsgBox_WarnColour)
        Call cmdList_Click
        Call cmbReasons_GotFocus
        Exit Sub
    End If
    mblnCaptured = True
    Me.Hide

End Sub

Private Sub cmdProcess_GotFocus()

    cmdProcess.FontBold = True

End Sub

Private Sub cmdProcess_LostFocus()
    
    cmdProcess.FontBold = False
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Call DebugMsg("KD", "", KeyCode)
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF4): 'if F4 then call list
                    If cmdList.Visible = True Then Call cmdList_Click
                    KeyCode = 0
                Case (vbKeyF5): 'if F5 then call save
                    If cmdProcess.Visible = True Then Call cmdProcess_Click
                    KeyCode = 0
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdCancel_Click
                    KeyCode = 0
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    Call DebugMsg("KD", "", KeyAscii)
    If (KeyAscii = vbKeyReturn) And (TypeOf ActiveControl Is TextBox) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If
    
End Sub

Private Sub Form_Load()
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)

End Sub

Private Sub txtNotes1_GotFocus()

    If cmbReasons.ListCount > 0 Then cmdList.Visible = True
    If (cmbReasons.ListCount > 0) And (cmbReasons.Visible = False) And (ActiveControl.Name = "txtNotes1") Then
    End If
    txtNotes1.SelStart = 0
    txtNotes1.SelLength = Len(txtNotes1.Text)
    txtNotes1.BackColor = RGBEdit_Colour

End Sub

Private Sub txtNotes1_LostFocus()
    
    cmdList.Visible = False
    txtNotes1.BackColor = RGB_WHITE

End Sub

Private Sub txtNotes2_GotFocus()

    txtNotes2.SelStart = 0
    txtNotes2.SelLength = Len(txtNotes2.Text)
    txtNotes2.BackColor = RGBEdit_Colour
    
End Sub

Private Sub txtNotes2_LostFocus()
    
    txtNotes2.BackColor = RGB_WHITE

End Sub
