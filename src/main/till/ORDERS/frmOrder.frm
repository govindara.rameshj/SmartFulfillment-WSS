VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "fpSPR70.OCX"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0F4DB82B-A487-11D6-B06E-0020AFC9A0C8}#1.0#0"; "CTSProgBar.ocx"
Object = "{F537A415-F16A-11D6-B070-0020AFC9A0C8}#1.0#0"; "HotkeyList.ocx"
Object = "{81F2A5C4-00BD-11D7-B070-0020AFC9A0C8}#5.0#0"; "DelNoteFilter.ocx"
Object = "{324C984B-22F9-11D7-B071-0020AFC9A0C8}#2.0#0"; "ISTOutFilter.ocx"
Object = "{905F077A-73D3-48CB-BCBB-2EF90EBA28A1}#1.0#0"; "EditDateCtl.ocx"
Object = "{DC2E9BE3-2864-11D7-B072-0020AFC9A0C8}#1.1#0"; "SuppReturnFilter.ocx"
Object = "{2A047DCB-DAD3-11D6-B06F-0020AFC9A0C8}#3.0#0"; "OrderFilter.ocx"
Object = "{495C39ED-6AA6-4DC6-BF21-594402C8726B}#1.0#0"; "ItemFilter.ocx"
Begin VB.Form frmOrder 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Raise Order"
   ClientHeight    =   7980
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   11820
   Icon            =   "frmOrder.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7980
   ScaleWidth      =   11820
   WindowState     =   2  'Maximized
   Begin prjSuppRetFilter.ucSuppRetFilter ucsrReturns 
      Height          =   7590
      Left            =   240
      TabIndex        =   58
      Top             =   240
      Visible         =   0   'False
      Width           =   11085
      _ExtentX        =   19553
      _ExtentY        =   13388
      Caption         =   "Select Supplier Return Note Filter Criteria"
      Caption         =   "Select Supplier Return Note Filter Criteria"
   End
   Begin prjItemFilter.ucItemFilter ucfiItemSearch 
      Height          =   7725
      Left            =   120
      TabIndex        =   51
      Top             =   120
      Visible         =   0   'False
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   13626
      UseList         =   0   'False
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   3720
      TabIndex        =   43
      Top             =   2400
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin prjHotkeyList.ucHotkeyList uchlHelpKeys 
      Height          =   2835
      Left            =   4320
      TabIndex        =   48
      Top             =   2040
      Visible         =   0   'False
      Width           =   3510
      _ExtentX        =   6191
      _ExtentY        =   5001
   End
   Begin prjDelNoteFilter.ucDelNoteFilter ucdnDelNotes 
      Height          =   7470
      Left            =   120
      TabIndex        =   47
      Top             =   120
      Visible         =   0   'False
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   13176
   End
   Begin prjOrderFilter.ucOrderFilter ucofOrders 
      Height          =   7605
      Left            =   120
      TabIndex        =   53
      Top             =   120
      Visible         =   0   'False
      Width           =   8940
      _ExtentX        =   15769
      _ExtentY        =   13414
      Caption         =   "Select Order Filter Criteria"
      Caption         =   "Select Order Filter Criteria"
   End
   Begin prjISTOutFilter.ucISTOutFilter ucioLookup 
      Height          =   7605
      Left            =   600
      TabIndex        =   50
      Top             =   120
      Visible         =   0   'False
      Width           =   6045
      _ExtentX        =   10663
      _ExtentY        =   13414
   End
   Begin VB.CommandButton cmdClose 
      BackColor       =   &H00E0E0E0&
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   27
      Top             =   7440
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5-Complete"
      Height          =   375
      Left            =   10680
      TabIndex        =   34
      Top             =   7440
      Width           =   1215
   End
   Begin VB.CommandButton cmdDelLine 
      Caption         =   "F8-Del Line"
      Height          =   375
      Left            =   9480
      TabIndex        =   33
      Top             =   7440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin FPSpread.vaSpread sprdTotal 
      Height          =   540
      Left            =   120
      TabIndex        =   35
      Top             =   6840
      Visible         =   0   'False
      Width           =   11775
      _Version        =   393216
      _ExtentX        =   20770
      _ExtentY        =   953
      _StockProps     =   64
      Enabled         =   0   'False
      ColHeaderDisplay=   0
      DisplayColHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   42
      MaxRows         =   3
      MoveActiveOnFocus=   0   'False
      RowHeaderDisplay=   0
      ScrollBars      =   0
      SpreadDesigner  =   "frmOrder.frx":058A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   46
      Top             =   7605
      Width           =   11820
      _ExtentX        =   20849
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmOrder.frx":1A8D
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13044
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "09:33"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdVoid 
      Caption         =   "F3-Void"
      Height          =   375
      Left            =   4560
      TabIndex        =   52
      Top             =   7440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton cmdViewCOrders 
      Caption         =   "F4-View Order"
      Height          =   375
      Left            =   3000
      TabIndex        =   54
      Top             =   7440
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Ctrl F8-Delete"
      Height          =   375
      Left            =   3000
      TabIndex        =   30
      Top             =   7440
      Width           =   1455
   End
   Begin VB.CommandButton cmdRePrint 
      Caption         =   "F9-Re-print"
      Height          =   375
      Left            =   1560
      TabIndex        =   29
      Top             =   7440
      Width           =   1335
   End
   Begin VB.CommandButton cmdViewParked 
      Caption         =   "F7-Get Parked"
      Height          =   375
      Left            =   1560
      TabIndex        =   28
      Top             =   7440
      Width           =   1335
   End
   Begin VB.CommandButton cmdViewNotes 
      BackColor       =   &H0000FFFF&
      Caption         =   "F7-Notes"
      Height          =   375
      Left            =   5520
      TabIndex        =   31
      Top             =   7440
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdViewCons 
      Caption         =   "F4-View Cons"
      Height          =   375
      Left            =   8280
      TabIndex        =   32
      Top             =   7440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdSaveHO 
      Caption         =   "Ctrl F5-Submit H/O"
      Height          =   375
      Left            =   6600
      TabIndex        =   41
      Top             =   7440
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Frame fraSelection 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Raise Order"
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   6615
      Begin LpLib.fpCombo cmbStore 
         Height          =   285
         Left            =   1200
         TabIndex        =   2
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
         _Version        =   196608
         _ExtentX        =   7646
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   16777215
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   1
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   -1
         ComboGap        =   7
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   2
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmOrder.frx":3C59
      End
      Begin LpLib.fpCombo cmbSupplier 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         Visible         =   0   'False
         Width           =   4815
         _Version        =   196608
         _ExtentX        =   8493
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   16777215
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   1
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   -1
         ComboGap        =   7
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   2
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmOrder.frx":4109
      End
      Begin LpLib.fpCombo cmbDispatchType 
         Height          =   315
         Left            =   1200
         TabIndex        =   56
         Top             =   1320
         Visible         =   0   'False
         Width           =   1815
         _Version        =   196608
         _ExtentX        =   3201
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmOrder.frx":45AF
      End
      Begin ucEditDate.ucDateText dtxtDueDate 
         Height          =   285
         Left            =   1200
         TabIndex        =   19
         Top             =   1320
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "03/06/03"
      End
      Begin FPSpread.vaSpread sprdDelNotes 
         Height          =   975
         Left            =   4200
         TabIndex        =   11
         Top             =   600
         Width           =   2295
         _Version        =   393216
         _ExtentX        =   4048
         _ExtentY        =   1720
         _StockProps     =   64
         ColHeaderDisplay=   0
         DisplayColHeaders=   0   'False
         DisplayRowHeaders=   0   'False
         EditModeReplace =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   9
         RetainSelBlock  =   0   'False
         RowHeaderDisplay=   0
         ScrollBars      =   2
         SpreadDesigner  =   "frmOrder.frx":4A5F
         UserResize      =   0
      End
      Begin VB.CheckBox chkStoresOnly 
         Alignment       =   1  'Right Justify
         Caption         =   "Display S&tores"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   600
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtReference 
         Height          =   285
         Left            =   4200
         MaxLength       =   20
         TabIndex        =   22
         Top             =   1320
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.Label lblRetRequestID 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1200
         TabIndex        =   61
         Top             =   600
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lblReturnID 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1200
         TabIndex        =   60
         Top             =   960
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lblReferencelbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Reference"
         Height          =   255
         Left            =   3120
         TabIndex        =   21
         Top             =   1320
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblDateDay 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   2460
         TabIndex        =   57
         Top             =   1320
         Width           =   375
      End
      Begin VB.Label lblDispTypelbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Dispatch By"
         Height          =   255
         Left            =   240
         TabIndex        =   55
         Top             =   1320
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblSuppNo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1200
         TabIndex        =   4
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblReleaseNo 
         BackStyle       =   0  'Transparent
         Caption         =   "NA"
         Height          =   255
         Left            =   3120
         TabIndex        =   14
         Top             =   1080
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label lblOrderNo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1200
         TabIndex        =   9
         Top             =   600
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lblOrderNolbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Order No"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   600
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblDelNoteNoLbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Del Note No"
         Height          =   255
         Left            =   3120
         TabIndex        =   10
         Top             =   600
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblLeadDays 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Left            =   4200
         TabIndex        =   45
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label lblLeadDayslbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Lead Days"
         Height          =   255
         Left            =   3120
         TabIndex        =   44
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label lblSuppName 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   2640
         TabIndex        =   6
         Top             =   240
         Width           =   3855
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Name"
         Height          =   255
         Left            =   2160
         TabIndex        =   5
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblMCPMinlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Min Order"
         Height          =   255
         Left            =   3120
         TabIndex        =   15
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblMCPMin 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Left            =   4200
         TabIndex        =   16
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblMCPType 
         BackStyle       =   0  'Transparent
         Caption         =   "(Value)"
         Height          =   255
         Left            =   5400
         TabIndex        =   17
         Top             =   960
         Width           =   495
      End
      Begin VB.Label lblContactName 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   4200
         TabIndex        =   23
         Top             =   1320
         Width           =   2295
      End
      Begin VB.Label lblContactNamelbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Contact"
         Height          =   255
         Left            =   3120
         TabIndex        =   20
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label lblSupplierlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier No"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblOrderDatelbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblUserDate 
         BackStyle       =   0  'Transparent
         Caption         =   "Due Date"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblOrderDate 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1200
         TabIndex        =   13
         Top             =   960
         Width           =   1095
      End
   End
   Begin FPSpread.vaSpread sprdOrder 
      Height          =   4935
      Left            =   120
      TabIndex        =   26
      Top             =   1920
      Visible         =   0   'False
      Width           =   11775
      _Version        =   393216
      _ExtentX        =   20770
      _ExtentY        =   8705
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      ColsFrozen      =   3
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   53
      MaxRows         =   2
      NoBeep          =   -1  'True
      OperationMode   =   2
      RowHeaderDisplay=   2
      ScrollBarExtMode=   -1  'True
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmOrder.frx":4E27
      ScrollBarTrack  =   1
   End
   Begin VB.Frame fraAddress 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Supplier Details"
      Height          =   1695
      Left            =   6840
      TabIndex        =   36
      Top             =   120
      Visible         =   0   'False
      Width           =   5055
      Begin FPSpread.vaSpread sprdAddress 
         Height          =   975
         Left            =   960
         TabIndex        =   25
         Top             =   240
         Width           =   3975
         _Version        =   393216
         _ExtentX        =   7011
         _ExtentY        =   1720
         _StockProps     =   64
         Enabled         =   0   'False
         ColHeaderDisplay=   0
         DisplayColHeaders=   0   'False
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   4
         RetainSelBlock  =   0   'False
         RowHeaderDisplay=   0
         ScrollBars      =   0
         SpreadDesigner  =   "frmOrder.frx":6E2A
      End
      Begin VB.Label lblEDIFlag 
         AutoSize        =   -1  'True
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "EDI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4560
         TabIndex        =   62
         Top             =   1320
         Visible         =   0   'False
         Width           =   390
      End
      Begin VB.Label lblFaxNo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   3000
         TabIndex        =   40
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Phone No"
         Height          =   255
         Left            =   180
         TabIndex        =   37
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Fax No"
         Height          =   255
         Left            =   2460
         TabIndex        =   39
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label lblPhoneNo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   960
         TabIndex        =   38
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label lblSupplierAddlbl 
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier"
         Height          =   255
         Left            =   180
         TabIndex        =   24
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Label lblSubmitted 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Submitted to Head Office"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8040
      TabIndex        =   59
      Top             =   7440
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.Label lblSOQNumber 
      BackStyle       =   0  'Transparent
      Caption         =   "lblSOQNumber"
      Height          =   255
      Left            =   5160
      TabIndex        =   49
      Top             =   7560
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblOrderID 
      BackStyle       =   0  'Transparent
      Height          =   255
      Left            =   6720
      TabIndex        =   42
      Top             =   1560
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "frmOrder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmOrder
'* Date   : 27/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Orders/frmOrder.frm $
'**********************************************************************************************
'* Summary: The main order form which is called first by the EXE.  This is called from the menu
'*          and is passed in an Action type which is passed into 'SetFormAction'.  This
'*          provides the functionality as follows :
'*              "D" : Delivery Note     "U" : Maintain Delivery Note
'*              "P" : Purchase Order    "E" : Maintain Purchase Order
'*              "O" : Raise IST Out     "T" : Maintain IST Out
'*              "I" : Record IST In     "C" : Record consignment (not working yet)
'*              "S" : Supplier Return   "R" : Maintian Return
'*              "N" : Maintain IST In
'**********************************************************************************************
'* $Author: Damians $ $Date: 7/06/04 10:09 $ $Revision: 17 $
'* Versions:
'* 27/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmOrder"
Const APP_NAME As String = "ORDERS"

Const PARTCODE_LEN As Long = 6
Const PARTCODE_PAD As String = "000000"

Const LINES_PER_PAGE = 35
Const UPDATE_FLAG As String = ">>" 'displayed as row visual marker to show edited

Const PRM_SUPP_ITEMS_ONLY   As Long = 117
Const PRM_MAX_LINE_QTY      As Long = 200
Const PRM_MAX_LINE_VALUE    As Long = 205
Const PRM_DUP_ITEMS_ALLOWED As Long = 215
Const PRM_AUTO_PRINT_PO     As Long = 216
Const PRM_CUTOFF_TIME       As Long = 220
Const PRM_SHOW_LASTSOLD     As Long = 221
Const PRM_MAX_PARKED_ORDERS As Long = 225
Const PRM_BOOK_EXCEPT       As Long = 250
Const PRM_AUTO_PRINT_DN     As Long = 251
Const PRM_DN_ADD_LINES      As Long = 255
Const PRM_DN_DISP_MSG       As Long = 256
Const PRM_AUTO_CONSIGN_DN   As Long = 260
Const PRM_ALLOW_BACK_DATE_DEL_DATE As Long = 261
Const PRM_AUTO_PRINT_SR     As Long = 355
Const PRM_DEF_SHORTAGE_CODE As Long = 360
Const PRM_RETURN_CHARGES    As Long = 361
Const PRM_AUTO_PRINT_ISTOUT As Long = 400
Const PRM_IST_ADD_LINES     As Long = 450
Const PRM_MANUAL_ISTS       As Long = 451
Const PRM_ISTIN_DISP_MSG    As Long = 452

Const PRM_SUPP_RET_COL_NOTE_REQ As Long = 350

Const COL_ALL As Long = -1
Const COL_ITEMNO As Long = 1
Const COL_PARTCODE As Long = 2
Const COL_DESC As Long = 3
Const COL_SIZE As Long = 4
Const COL_QTY As Long = 5
Const COL_QTYOS As Long = 6
Const COL_QTYDEL As Long = 7
Const COL_QTYBORD As Long = 8
Const COL_QTYSHORT As Long = 9
Const COL_SHORTCODE As Long = 10
Const COL_COMMENT As Long = 11
Const COL_QTYRET As Long = 12
Const COL_RETCODE As Long = 13
Const COL_RETCMNT As Long = 14
Const COL_UNITS As Long = 15
Const COL_SPARE As Long = 16
Const COL_PACKQTY As Long = 17
Const COL_PACKSORD As Long = 18
Const COL_COST As Long = 19
Const COL_DISC As Long = 20
Const COL_EXCTOTAL As Long = 21
Const COL_VATTOTAL As Long = 22
Const COL_INCTOTAL As Long = 23
Const COL_COSTTOTAL As Long = 24
Const COL_FREE As Long = 25
Const COL_ONHAND As Long = 26
Const COL_MINLEVEL As Long = 27
Const COL_MAXLEVEL As Long = 28
Const COL_LASTSOLD As Long = 29
Const COL_CUSTONO As Long = 30
Const COL_VATRATE As Long = 31
Const COL_MANUCODE As Long = 32
Const COL_ORIGLINENO As Long = 33
Const COL_SELLPRICE As Long = 34
Const COL_LASTORDER As Long = 35
Const COL_QTYSELLIN As Long = 36
Const COL_DELTOTAL As Long = 37
Const COL_SPARE2 As Long = 38
Const COL_WEIGHT As Long = 39
Const COL_WGHTTOT As Long = 40
Const COL_RETVAL As Long = 41
Const COL_DELCOST As Long = 42
Const COL_RETCOST As Long = 43
Const COL_RETLINENO As Long = 44
Const COL_SHORTCOST As Long = 45
Const COL_SHORTVAL As Long = 46
Const COL_SHORTLINENO As Long = 47
Const COL_SUPPMANCODE As Long = 48
Const COL_ORIGPOQTY As Long = 49
Const COL_OVERVAL As Long = 50
Const COL_OVERCOST As Long = 51
Const COL_QTYOVER As Long = 52
Const COL_OVERLINENO As Long = 53

Const COL_TOTINFO As Long = 1
Const COL_TOTQTY As Long = 4
Const COL_TOTWGHT As Long = 8
Const COL_TOTSPARE As Long = 12
Const COL_TOTVAL As Long = 16
Const COL_TOTQTYOS As Long = 20
Const COL_TOTQTYDEL As Long = 24
Const COL_TOTQTYRET As Long = 28
Const COL_TOTRETVAL As Long = 32
Const COL_TOTQTYSHRT As Long = 36
Const COL_TOTSHRTVAL As Long = 40

Const ACTION_DELNOTE As String = "D" '
Const ACTION_PORDER As String = "P" '
Const ACTION_ISTOUT As String = "O"
Const ACTION_ISTIN As String = "I"
Const ACTION_CONSIGN As String = "C"
Const ACTION_MAINTPO As String = "E" '
Const ACTION_MAINTDN As String = "U"
Const ACTION_RETURN As String = "S"
Const ACTION_MAINTISTOUT As String = "T"
Const ACTION_MAINTRETURN As String = "R"
Const ACTION_MAINTISTIN As String = "N"

Const DEFAULT_MSG As String = "F1-Help"

Const PRINT_NONE As Long = 0
Const PRINT_PROMPT As Long = 1
Const PRINT_ALWAYS As Long = 2

Const ROW_TOTALS As Long = 1
Const ROW_FIRST As Long = 2

Const TYPE_SHORTAGE = 0
Const TYPE_RETURN = 1
Const TYPE_OVER = 2

Dim mcolStores   As Collection 'local copy of Stores - only used if chkStores is clicked
Dim arDispCols() As Long
Dim dblOrigWidth As Double
Dim lngLastCol   As Long
Dim mstrRetCode  As String 'local copy of Return Codes - only used if Supplier Returns
Dim mstrLineCmnt As String 'local copy of line comments summary - added MM 3/3/03
Dim mstrDocCmnt  As String 'local copy of document comments summary - added MM 3/3/03

Dim strActionCode   As String 'Flag to indicate current function of Form
Dim strItemSuppNo   As String 'flag to indicate if Item must be for supplier or any
Dim mblnWarehouse   As Boolean 'Flag if supplier is actually a Warehouse -limit items to process
Dim mblnUseAllItems As Boolean 'Flag if supplier can use any item as defined in the Category setting 'A'
Dim mblnEDI         As Boolean 'flag if document must be electronically despatched
Dim mstrEDICode     As String  'if supplier uses EDI then get EDI code for file name
Dim mblnBookExcept  As Boolean 'flag to indicate if quantity despatched should be filled in
Dim mblnDupItems    As Boolean 'flag if Item can be duplicated on Purchase Order
Dim mblnUpdRetNote  As Boolean 'if Del Note maintained with Returns, flag to update Ret Note
Dim mblnUpdShrtNote As Boolean 'if Del Note maintained with Shortages, flag to update Short Note
'Added dslater mis-pick
Dim mblnUpdOver     As Boolean 'If Del Note maintained with Overs, flag to update Over Note
Dim mblnAllowBOrder As Boolean 'flag if supplier allows back orders
Dim mlngPOAutoPrint As Long    'flag if Purchase Order must be printed or prompt user(0=No,1=Prompt, 2=Always)
Dim mlngDNAutoPrint As Long    'flag if Delivery Note must be printed or prompt user(0=No,1=Prompt, 2=Always)
Dim mlngSRAutoPrint As Long    'flag if Supplier Return must be printed or prompt user(0=No,1=Prompt, 2=Always)
Dim mlngISTOutPrint As Long    'flag if IST Out must be printed or prompt user (0=No,1=Prompt, 2=Always)
Dim mblnDueEdited   As Boolean 'flag if due date edited when maint Purchase Order
Dim mstrDispMsg     As String  'System message displayed after saving Del Note
Dim mblnDNAddLines  As Boolean 'flag to allow user to add line to Delivery Note
Dim mblnISTAddLines As Boolean 'flag to allow user to add lines to Electronic IST's
Dim strPOCompanyName As String 'used to store Company Name for Purchase Order
Dim mstrDeliveryTo   As String 'used to store Delivery Address for Purchase Order
Dim mstrInvoiceTo    As String 'used to store Invoice Address for Purchase Order
Dim mstrDelContact   As String 'used to store Delivery Contact details for Purchase Order
Dim mstrInvContact   As String 'used to store Invoice Contact details for Purchase Order
Dim mblnUseObsItems  As Boolean 'whether to allow Obselete items to be added (IST's mainly)

Dim mstrMispickCode As String 'Mis-pick reason code

Dim strMaintUser     As String 'used when maintaining document to get user initials fo reprints

Dim mstrFunction     As String 'short description used in Message Headers of current Function
Dim mstrLogoFile     As String 'Location of Logo file for printing on documents
Dim lngCutOffTime    As Long   'Used when saving Purchase orders to determine when Cut Off time
Dim blnAutoConsignDN As Boolean 'System flag to determine if Del Note- auto creates Consigment Note
Dim blnSuppItemsOnly As Boolean 'flag if supplier to Use only their items - else prompt

Dim moEDIISTIn       As Object 'used by processing EDI based IST Receipts

Dim mblnSRColNoteReq As Boolean 'hold system parameter to check if collection note is required on save

Dim mcolConLines     As Collection ' list of consolidated items
Dim mstrNotes        As String 'hold notes from original document when maintaining document
Dim mlngQtyDecNum    As Long 'hold number of decimal places to show for quantities
Dim mlngValueDecNum  As Long 'hold number of decimal places to show for values

Dim mdblRestockRate   As Double 'used by supplier returns to calculate restock charges
Dim mdblRestockCharge As Double 'used by supplier returns to calculate restock charges

Dim moDelNote       As Object 'used when updating an existing delivery note
Dim moSuppRetNote   As Object 'used when updating a Supplier Return for a Delivery Note
Dim moSuppShortNote As Object 'used when updating a Shortage Note for a Delivery Note
Dim moOverDelNote   As Object 'used when updating an over

Dim mdblMaxQty   As Double 'holds maximum line quantity that can be entered with out confirmation
Dim mdblMaxValue As Double 'holds maximum line value that can be entered with out confirmation

Dim mblnSubmitHO As Boolean 'used to flag if Del Note/Supplier Return must be sent to H/O

Dim mblnWasShortage  As Boolean 'used to flag is the over\short was previously a shortage
Dim mblnWasOverage   As Boolean 'used to flag is the over\short was previously a overage

Dim mstrDefShortageCode As String 'for Del Note to auto code shortages
Dim mstrDefShortageDesc As String 'for Del Note to auto code shortages

Dim moEDIDelNote  As Object 'for marking EDI Note as processed

Dim mlngMaxParkedOrders As Long

Private Sub SetQuantityPlaces()

    sprdOrder.Row = -1
    sprdOrder.Col = COL_QTY
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_QTYOS
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_QTYBORD
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_QTYRET
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_PACKQTY
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_FREE
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_ONHAND
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_MINLEVEL
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_MAXLEVEL
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum
    sprdOrder.Col = COL_QTYSELLIN
    sprdOrder.TypeNumberDecPlaces = mlngQtyDecNum

    sprdTotal.Row = 2
    sprdTotal.Col = COL_TOTQTY
    sprdTotal.TypeNumberDecPlaces = mlngQtyDecNum
    sprdTotal.Col = COL_TOTQTYOS
    sprdTotal.TypeNumberDecPlaces = mlngQtyDecNum
    sprdTotal.Col = COL_TOTQTYDEL
    sprdTotal.TypeNumberDecPlaces = mlngQtyDecNum
    sprdTotal.Col = COL_TOTQTYRET
    sprdTotal.TypeNumberDecPlaces = mlngQtyDecNum
    sprdTotal.Col = COL_TOTQTYSHRT
    sprdTotal.TypeNumberDecPlaces = mlngQtyDecNum

End Sub

Private Sub ShowTotal(ByVal lngTotalPos As Long, blnShow As Boolean)

Dim lngColNo As Long

    'hide or show column selected together with previous 2 spacer columns and after column
    For lngColNo = lngTotalPos - 2 To lngTotalPos + 1 Step 1
        sprdTotal.Col = lngColNo
        sprdTotal.ColHidden = Not blnShow
    Next lngColNo
    
End Sub

Private Sub SetTotalsWidth()

Dim lngColNo      As Long
Dim lngTotalWidth As Long
Dim dblTotWidth   As Double
Dim lngWidth      As Single

    'Add up the width of all displayed columns
    dblTotWidth = sprdTotal.ColWidth(0)
    For lngColNo = 2 To sprdTotal.MaxCols Step 1
        sprdTotal.Col = lngColNo
        If sprdTotal.ColHidden = False Then dblTotWidth = dblTotWidth + sprdTotal.ColWidth(lngColNo) + 0.15
    Next lngColNo
    'Convert Width to Twips to measure against Spread Width
    Call sprdTotal.ColWidthToTwips(dblTotWidth, lngTotalWidth)
    Call sprdTotal.TwipsToColWidth((sprdTotal.Width - lngTotalWidth), lngWidth)
    'Set column 1 (blank padding column) to the calculated width
    sprdTotal.ColWidth(1) = lngWidth
    
End Sub

'<CACH>****************************************************************************************
'* Sub:  SaveOrder()
'**********************************************************************************************
'* Description: Used to create the Purchase Order object and save the Order and the Lines
'**********************************************************************************************
'* Parameters:
'* In blnParkOrder : Boolean. Flag passed in to indicate save mode
'* In blnOrderPrinted : Boolean. Indicates if order has been printed, to record in table
'**********************************************************************************************
'* History:
'* 04/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function SaveOrder(ByVal blnParkOrder As Boolean, ByVal blnOrderPrinted As Boolean) As Boolean

Dim oOrder      As Object
Dim oLine       As Object
Dim blnUpdate   As Boolean
Dim lngLineNo   As Long
Dim blnSaveCons As Boolean
Dim strNotes    As String
Dim strExtraMsg As String
Dim dblCharges  As Double
Dim blnSentEDI  As Boolean

Const PROCEDURE_NAME As String = MODULE_NAME & ".SaveOrder"

    SaveOrder = False
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
    
    blnUpdate = False
    If (lblEDIFlag.Visible = True) Then blnSentEDI = True
    If Val(lblOrderID.Caption) <> 0 Then 'retrieve existing order, ready for update
        oOrder.Key = Val(lblOrderID.Caption)
        Call oOrder.IBo_Load
        blnUpdate = True
    End If
    If (strActionCode = ACTION_PORDER) And (blnParkOrder = False) Then
        'check if any comments for new Purchase Order
        Load frmNotes
        If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatPurchaseOrder) = False Then 'cancelled pressed so exit save
            Unload frmNotes
            Exit Function
        End If
        Unload frmNotes
        mstrNotes = strNotes
        
        'Added 12/4/04 - prompt for Delivery Charges
        dblCharges = Val(InputBox$("Enter any delivery charges" & vbCrLf & "Leave blank for none", "Record " & mstrFunction & " delivery charges", "0.00"))
    End If
    
    ucpbProgress.Caption1 = "Saving Purchase Order header"
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Max = 1
    DoEvents
    
    oOrder.SupplierNo = lblSuppNo.Caption
    oOrder.OrderDate = Now
    oOrder.DueDate = GetDate(dtxtDueDate.Text)
    If blnOrderPrinted = True Then
        oOrder.PrintFlag = "Y"
    Else
        oOrder.PrintFlag = "N"
    End If
    oOrder.Source = "E"
    'Transfer order totals onto Order BO
    sprdTotal.Row = 2
    sprdTotal.Col = COL_TOTQTY
    oOrder.QuantityOnOrder = Val(sprdTotal.Text)
    sprdTotal.Col = COL_TOTWGHT
    oOrder.Weight = Val(sprdTotal.Text)
    oOrder.CarraigeValue = dblCharges
    If oOrder.Weight > 1000000 Then oOrder.Weight = 0
    sprdOrder.Row = 1
    sprdOrder.Col = COL_EXCTOTAL
    oOrder.Value = Val(sprdOrder.Value)
    oOrder.RaisedBy = goSession.UserInitials
    oOrder.Narrative = strNotes
    
    If blnParkOrder = False Then blnSaveCons = RequiresConsolidation
    
    ucpbProgress.Caption1 = "Saving Purchase Order lines"
    ucpbProgress.Max = sprdOrder.MaxRows + 2  'add 2 extra - Save function + Save Return
    'Add lines to Order
    For lngLineNo = 2 To sprdOrder.MaxRows - 1 Step 1
        ucpbProgress.Value = lngLineNo
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_PARTCODE
        Set oLine = oOrder.AddItem(sprdOrder.Text)
        oLine.PartAlphaKey = sprdOrder.Text
        oLine.LineNo = lngLineNo - 1
        sprdOrder.Col = COL_SELLPRICE
        oLine.OrderPrice = Val(sprdOrder.Text)
        sprdOrder.Col = COL_COST
        oLine.CostPrice = Val(sprdOrder.Text)
        sprdOrder.Col = COL_QTY
        oLine.OrderQuantity = Val(sprdOrder.Text)
        sprdOrder.Col = COL_MANUCODE
        oLine.SupplierPartCode = sprdOrder.Text
        sprdOrder.Col = COL_LASTORDER
        If sprdOrder.Text <> "" Then oLine.LastOrderDate = CDate(sprdOrder.Text)
        sprdOrder.Col = COL_QTYSELLIN
        oLine.QuantityPerUnit = sprdOrder.Text
        sprdOrder.Col = COL_CUSTONO
        oLine.CustomerOrderNo = sprdOrder.Text
        sprdOrder.Col = COL_ORIGLINENO
        oLine.CustomerOrderLineNo = sprdOrder.Text
        sprdOrder.Col = COL_SUPPMANCODE
        oLine.ManufacturerCode = sprdOrder.Text
    Next lngLineNo
    
    ucpbProgress.Caption1 = "Saving to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    
    If blnParkOrder = True Then
        Call oOrder.ParkOrder 'save order with no Order and Release number
    Else
        If blnUpdate = True Then 'update existing order that was previously parked
            Call oOrder.IBo_SaveIfExists
        Else
            'Create new Purchase order
            oOrder.IBo_SaveIfNew
            If blnSaveCons = True Then 'create consolidation lines
                Call frmViewCons.SaveConsolidation(oOrder.Key, oOrder.OrderNumber, oOrder.OrderDate, blnOrderPrinted, lblSuppNo.Caption & " : " & lblSuppName.Caption)
                Unload frmViewCons
             End If
        End If
        If blnSentEDI = True Then
            Call oOrder.CreateFile(mstrEDICode)
            'Display msgbox showing the purchase order has been saved for EDI despatch
            Call MsgBoxEx("Purchase Order successfully saved." & vbCrLf & _
                mstrDispMsg & "Purchase Order sent electronically " & _
                lblOrderNo.Caption, vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
        End If
    End If 'not parked so save
    ucpbProgress.Visible = False
    lblOrderNo.Caption = oOrder.OrderNumber
    lblReleaseNo.Caption = oOrder.ReleaseNumber
    lblSOQNumber.Caption = oOrder.SOQNumber
    SaveOrder = True
    
        
End Function 'SaveOrder
'<CACH>****************************************************************************************
'* Sub:  SaveDelNote()
'**********************************************************************************************
'* Description: Used to save the Delivery Note with the Lines.
'**********************************************************************************************
'* Parameters:
'* In blnSubmit : Boolean - flag if system must Create file and Send to H/O
'**********************************************************************************************
'* History:
'* 04/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function SaveDelNote(ByVal blnSubmit As Boolean) As Boolean

Dim oDelNote    As Object
Dim oLine       As Object
Dim lngLineNo   As Long
Dim oRetNote    As Object
Dim oRetLine    As Object
Dim oShortNote  As Object
Dim oShortLine  As Object
Dim dblCharges  As Double
Dim strNotes    As String
Dim blnPrint    As Boolean
Dim strConMsg   As String
Dim strRetMsg   As String 'hold Cost message for returns note to be displayed upon save
Dim strShrtMsg  As String 'hold Cost message for Shortages note to be displayed upon save
Dim blnBDateDel As Boolean
Dim strDelNotes As String
Dim oOverDelNote    As Object 'Used for creating overs delivery note
Dim oOverDelLine    As Object 'Used for creating overs delivery note


Const PROCEDURE_NAME As String = MODULE_NAME & ".SaveDelNote"

    blnBDateDel = goSession.GetParameter(PRM_ALLOW_BACK_DATE_DEL_DATE)
    SaveDelNote = False
    sprdDelNotes.Row = 1
    If sprdDelNotes.Text = "" Then
        Call MsgBoxEx("Delivery Note Number not entered" & vbCrLf & "Enter valid value and re-save", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        sprdDelNotes.SetFocus
        Exit Function
    End If
    
    'check receipt date is not in the future
    If (GetDate(dtxtDueDate.Text) > CDate(Now)) Then
        Call MsgBoxEx("Delivery Note receipt date invalid" & vbCrLf & "Receipt date must be not later than today", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        dtxtDueDate.SetFocus
        Exit Function
    End If
    
    'Check Receipt date is not before order date
    If (GetDate(dtxtDueDate.Text) < GetDate(lblOrderDate.Caption)) And (blnBDateDel = False) Then
        Call MsgBoxEx("Date entered can not be before order date", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
        Call dtxtDueDate.SetFocus
        Exit Function
    End If
    
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_QTYSHORT
    
    'If Qty Short is greater than 0 then default note 1 to Hold Payment
    If Val(sprdOrder.Text) > 0 Then
        mstrDocCmnt = "Problems - Hold Payment" & vbTab
    End If
    
    sprdOrder.Col = COL_QTYRET
    
    'If Qty Returned is greater than 0 then default note 1 to Hold Payment
    If Val(sprdOrder.Text) > 0 Then
        mstrDocCmnt = "Problems - Hold Payment" & vbTab
    End If
    
    'If we have not defaulted a note 1 then set it to Delivery OK
    If mstrDocCmnt = "" Then
        mstrDocCmnt = "Delivery OK" & vbTab
    End If

    'check if any comments for Delivery Note
    Load frmNotes
    If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatDeliveryNote) = False Then 'cancelled pressed so exit save
        Unload frmNotes
        Exit Function
    End If
    Unload frmNotes
    
    dblCharges = 0
    If ((moEDIDelNote Is Nothing) = False) Then
        dblCharges = moEDIDelNote.CarraigeValue
        strShrtMsg = vbCrLf & "(Advised delivery charge is " & Format(dblCharges, "0.00") & ")"
    End If
    dblCharges = Val(InputBoxEx("Enter any delivery charges" & vbCrLf & "Leave blank for none" & strShrtMsg, "Record " & mstrFunction & " charges", Format(dblCharges, "0.00"), enifNumeric, False, RGBMSGBox_PromptColour, 2))
    
    ucpbProgress.Caption1 = "Saving Delivery Note header"
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Max = 1
    DoEvents
    
    'Populate delivery note header
    Set oDelNote = goDatabase.CreateBusinessObject(CLASSID_DELNOTEHEADER)
    oDelNote.SupplierONumber = lblSuppNo.Caption
    oDelNote.DelNoteDate = Now
    oDelNote.PODate = GetDate(lblOrderDate.Caption)
    oDelNote.ReceiptDate = GetDate(dtxtDueDate.Text)
    oDelNote.PONumber = lblOrderNo.Caption
    oDelNote.AssignedPORelease = lblReleaseNo.Caption
    oDelNote.SOQControlNumber = lblSOQNumber.Caption
    oDelNote.OrigDocKey = lblOrderID.Caption
    oDelNote.CarraigeValue = dblCharges
    oDelNote.Commit = "N"
    oDelNote.Source = "E"
    oDelNote.Narrative = strNotes
    oDelNote.EnteredBy = goSession.UserInitials
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_DELTOTAL
    oDelNote.Value = Val(sprdOrder.Text)
    sprdOrder.Col = COL_DELCOST
    oDelNote.Cost = Val(sprdOrder.Text)
    
    If blnSubmit = True Then oDelNote.Commit = "Y"
    'Move entered supplier delivery note numbers onto Del Note object
    For lngLineNo = 1 To sprdDelNotes.MaxRows Step 1
        sprdDelNotes.Row = lngLineNo
        oDelNote.SupplierDelNote(lngLineNo) = sprdDelNotes.Text
        strDelNotes = strDelNotes & sprdDelNotes.Text & ","
    Next lngLineNo
    
    'Check supplier delivery notes to omit gaps, for Returns print-out
    While (InStr(strDelNotes, ",,") > 0)
        strDelNotes = Replace(strDelNotes, ",,", ",")
    Wend
    If Right$(strDelNotes, 1) = "," Then strDelNotes = Left$(strDelNotes, Len(strDelNotes) - 1)
    
    ucpbProgress.Caption1 = "Saving Delivery Note lines"
    ucpbProgress.Max = sprdOrder.MaxRows + 3  'add 3 extra - Save function + Save Return + Save Over
    
    'Add lines to Order
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        ucpbProgress.Value = lngLineNo
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_PARTCODE
        If sprdOrder.Text <> "" Then 'add valid line to Delivery Note
            sprdOrder.Col = COL_PARTCODE
            Set oLine = oDelNote.AddItem(sprdOrder.Text)
            oLine.PartCodeAlphaCode = sprdOrder.Text
            sprdOrder.Col = COL_SELLPRICE
            oLine.OrderPrice = Val(sprdOrder.Text)
            oLine.CurrentPrice = Val(sprdOrder.Text)
            sprdOrder.Col = COL_COST
            oLine.CurrentCost = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTY
            oLine.OrderQty = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTYDEL
            oLine.ReceivedQty = Val(sprdOrder.Text)
            'Force override to copy Del Qty into Order Qty if line added after order
            If (oLine.OrderQty = 0) And (oLine.ReceivedQty > 0) Then oLine.OrderQty = oLine.ReceivedQty
            sprdOrder.Col = COL_QTYBORD
            If sprdOrder.ColHidden = False Then oLine.ToFollowQty = Val(sprdOrder.Text)
            sprdOrder.Col = COL_ORIGLINENO
            oLine.POLineNumber = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTYSELLIN
            oLine.SinglesSellingUnit = Val(sprdOrder.Text)
            sprdOrder.Col = COL_COMMENT
            oLine.Narrative = sprdOrder.Text
            'Check if any Return quantities entered, if so populate Return Note
            sprdOrder.Col = COL_QTYRET
            If Val(sprdOrder.Text) > 0 Then
                If oRetNote Is Nothing Then 'create Return Note Obj if not done yet
                    Set oRetNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
                    oRetNote.SupplierNumber = lblSuppNo.Caption
                    oRetNote.DelNoteDate = Now
                    oRetNote.OrigPONumber = lblOrderNo.Caption
                    oRetNote.EnteredBy = goSession.UserInitials
                    oRetNote.Comment = strDelNotes
                    Set oRetNote.DelNote = oDelNote
                    sprdOrder.Row = ROW_TOTALS
                    sprdOrder.Col = COL_RETVAL
                    oRetNote.Value = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_RETCOST
                    oRetNote.Cost = Val(sprdOrder.Text)
                    oRetNote.Commit = "N"
                    If blnSubmit = True Then oRetNote.Commit = "Y"
                    oRetNote.Narrative = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
                End If 'Returns Note header must be created
                sprdOrder.Row = lngLineNo
                sprdOrder.Col = COL_PARTCODE
                Set oRetLine = oRetNote.AddItem(sprdOrder.Text)
                oRetLine.PartCodeAlphaCode = sprdOrder.Text
                sprdOrder.Col = COL_QTYRET
                oRetLine.ReturnQty = sprdOrder.Text
                oRetLine.ReturnedQty = sprdOrder.Text
                sprdOrder.Col = COL_QTYSELLIN
                oRetLine.SinglesSellingUnit = Val(sprdOrder.Text)
                sprdOrder.Col = COL_SELLPRICE
                oRetLine.OrderPrice = Val(sprdOrder.Text)
                oRetLine.CurrentPrice = Val(sprdOrder.Text)
                sprdOrder.Col = COL_COST
                oRetLine.CurrentCost = Val(sprdOrder.Text)
                sprdOrder.Col = COL_RETCODE
                oRetLine.ReturnReasonCode = Left$(sprdOrder.Text, InStr(sprdOrder.Text, "-") - 1)
                sprdOrder.Col = COL_RETCMNT
                oRetLine.Narrative = sprdOrder.Text
                sprdOrder.Col = COL_ORIGLINENO
                oRetLine.POLineNumber = Val(sprdOrder.Text)
            End If 'Updating Returns Note
            'Check if any items where short delivered and if so populate Shortages Note
            sprdOrder.Col = COL_QTYSHORT
            If (Val(sprdOrder.Text) > 0) And (sprdOrder.ForeColor = vbRed) Then
                If oShortNote Is Nothing Then 'create Return Note Obj if not done yet
                    Set oShortNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
                    oShortNote.SupplierNumber = lblSuppNo.Caption
                    oShortNote.DelNoteDate = Now
                    oShortNote.OrigPONumber = lblOrderNo.Caption
                    oShortNote.EnteredBy = goSession.UserInitials
                    oShortNote.ShortageNote = True
                    oShortNote.Comment = strDelNotes
                    Set oShortNote.DelNote = oDelNote
                    sprdOrder.Row = ROW_TOTALS
                    sprdOrder.Col = COL_SHORTVAL
                    oShortNote.Value = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_SHORTCOST
                    oShortNote.Cost = Val(sprdOrder.Text)
                    oShortNote.Commit = "N"
                    oShortNote.Narrative = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
                    If blnSubmit = True Then oShortNote.Commit = "Y"
                End If 'Returns Note header must be created
                sprdOrder.Row = lngLineNo
                sprdOrder.Col = COL_PARTCODE
                Set oShortLine = oShortNote.AddItem(sprdOrder.Text)
                oShortLine.PartCodeAlphaCode = sprdOrder.Text
                sprdOrder.Col = COL_QTYSHORT
                oShortLine.ReturnQty = sprdOrder.Text
                oShortLine.ReturnedQty = sprdOrder.Text
                sprdOrder.Col = COL_QTYSELLIN
                oShortLine.SinglesSellingUnit = Val(sprdOrder.Text)
                sprdOrder.Col = COL_SELLPRICE
                oShortLine.OrderPrice = Val(sprdOrder.Text)
                oShortLine.CurrentPrice = Val(sprdOrder.Text)
                sprdOrder.Col = COL_COST
                oShortLine.CurrentCost = Val(sprdOrder.Text)
                sprdOrder.Col = COL_RETCODE
                oShortLine.ReturnReasonCode = mstrDefShortageCode
                sprdOrder.Col = COL_ORIGLINENO
                oShortLine.POLineNumber = Val(sprdOrder.Text)
            End If
            
            If (Val(sprdOrder.Text) > 0) And (sprdOrder.ForeColor = vbBlue) Then 'Over's
                'Populate delivery note header
                If oOverDelNote Is Nothing Then
                    Set oOverDelNote = goDatabase.CreateBusinessObject(CLASSID_DELNOTEHEADER)
                    oOverDelNote.SupplierONumber = lblSuppNo.Caption
                    oOverDelNote.DelNoteDate = Now
                    oOverDelNote.PODate = GetDate(lblOrderDate.Caption)
                    oOverDelNote.ReceiptDate = GetDate(dtxtDueDate.Text)
                    'We don't assign a PO number as this is an over
                    oOverDelNote.PONumber = ""
                    oOverDelNote.AssignedPORelease = lblReleaseNo.Caption
                    oOverDelNote.SOQControlNumber = lblSOQNumber.Caption
                    oOverDelNote.OrigDocKey = 0
                    oOverDelNote.CarraigeValue = dblCharges
                    oOverDelNote.Commit = "N"
                    oOverDelNote.Source = "X"
                    oOverDelNote.Narrative = strNotes
                    oOverDelNote.EnteredBy = goSession.UserInitials
                    'oOverDelNote.Memo = oDelNote.DocNo
                    sprdOrder.Row = ROW_TOTALS
                    sprdOrder.Col = COL_OVERVAL
                    oOverDelNote.Value = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_OVERCOST
                    oOverDelNote.Cost = Val(sprdOrder.Text)
                    sprdOrder.Row = lngLineNo
                End If
                
                sprdOrder.Col = COL_PARTCODE
                Set oOverDelLine = oOverDelNote.AddItem(sprdOrder.Text)
                oOverDelLine.PartCodeAlphaCode = sprdOrder.Text
                sprdOrder.Col = COL_SELLPRICE
                oOverDelLine.OrderPrice = Val(sprdOrder.Text)
                oOverDelLine.CurrentPrice = Val(sprdOrder.Text)
                sprdOrder.Col = COL_COST
                oOverDelLine.CurrentCost = Val(sprdOrder.Text)
                'Set to 0 to show an over
                oOverDelLine.OrderQty = 0
                sprdOrder.Col = COL_QTYOVER
                'Set to over field
                oOverDelLine.ReceivedQty = Val(sprdOrder.Text)
                sprdOrder.Col = COL_QTYBORD
                oOverDelLine.ToFollowQty = 0
                sprdOrder.Col = COL_ORIGLINENO
                oOverDelLine.POLineNumber = 0
                sprdOrder.Col = COL_QTYSELLIN
                oOverDelLine.SinglesSellingUnit = Val(sprdOrder.Text)
                sprdOrder.Col = COL_COMMENT
                oOverDelLine.Narrative = sprdOrder.Text
                oOverDelLine.OverQty = 0
                
                sprdOrder.Col = COL_QTYSHORT
                oLine.OverQty = Val(sprdOrder.Text)
            End If 'Updating Returns Note
        End If 'delivery quantity entered
    Next lngLineNo
    
    'check if Supplier return has been created and if any restock charges must be captured
    If ((oRetNote Is Nothing) = False) And ((mdblRestockCharge > 0) Or (mdblRestockRate > 0)) And (goSession.GetParameter(PRM_RETURN_CHARGES) = True) Then
        If mdblRestockRate > 0 Then 'calculate restock charge based on % of value
            dblCharges = oRetNote.Value * mdblRestockRate / 100
        Else
            dblCharges = mdblRestockCharge
        End If
        'HARD CODED FIX
        dblCharges = 0
        'Confirm restock charge
        dblCharges = Val(InputBox("Confirm/enter any restock charges" & vbCrLf, "Record restock charges", Format(dblCharges, "0.00")))
        oRetNote.RestockCharge = dblCharges
    End If
    
    'Perform saving functions within Transaction
    ucpbProgress.Caption1 = "Saving to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    Call goDatabase.StartTransaction
    
    ucpbProgress.Caption1 = "Saving overs to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    
    'Check for mis-picks
    If (oOverDelNote Is Nothing) = False Then
        'Save the mis-picks first so that we can get the drl no
        Call oOverDelNote.IBo_SaveIfNew
        
        'Set the Over doc note to OrigDocNo
        oDelNote.OrigDocNo = oOverDelNote.DocNo
    End If
    
    Call oDelNote.IBo_SaveIfNew
    ucpbProgress.Caption1 = "Saving return note to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    
    'Check for mis-picks
    If (oOverDelNote Is Nothing) = False Then
        'Save the OrigDocNo
        Call oOverDelNote.RecordOrigDocNo(oDelNote.DocNo)
    End If
    
    If (oRetNote Is Nothing) = False Then 'save Returns notes
        oRetNote.OriginalDelNoteDRL = oDelNote.DocNo
        Call oRetNote.IBo_SaveIfNew
        strRetMsg = vbCrLf & "Total Cost of Return Note is " & Format(oRetNote.Cost, "0.00")
    End If
    ucpbProgress.Caption1 = "Saving shortages note to database"
    If (oShortNote Is Nothing) = False Then 'save Returns notes
        oShortNote.OriginalDelNoteDRL = oDelNote.DocNo
        Call oShortNote.IBo_SaveIfNew
        strShrtMsg = vbCrLf & "Total Cost of Shortage Note is " & Format(oShortNote.Cost, "0.00")
    End If
    If ((moEDIDelNote Is Nothing) = False) Then Call moEDIDelNote.RecordAsReceived(Date)
    
    Call goDatabase.CommitTransaction
    
    ucpbProgress.Caption1 = "Creating print-outs"
    blnPrint = False
    
    Select Case (mlngDNAutoPrint)
        Case (PRINT_NONE): blnPrint = False
        Case (PRINT_PROMPT):
                            If cmdViewCons.Visible Then strConMsg = "Consolidation Sheet and "
                            If MsgBoxEx("Delivery Note completed - Print " & strConMsg & " Delivery Note Now", vbYesNoCancel + vbQuestion, "Process Delivery Note", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
        Case Else: blnPrint = True
    End Select
    If blnPrint Then Call PrintDeliveryNote(oDelNote.DocNo, oDelNote.ConsignmentRef, oDelNote.DelNoteDate)
    If (oRetNote Is Nothing) = False Then Call PrintSupplierReturn(oRetNote.SupplierReturnNumber, oRetNote.DocNo & "/" & oRetNote.ReleaseNumber, oRetNote.DelNoteDate, oRetNote.Narrative, goSession.UserInitials, oDelNote.DocNo, strDelNotes, False, False)
    If (oShortNote Is Nothing) = False Then Call PrintSupplierReturn(oShortNote.SupplierReturnNumber, oShortNote.DocNo & "/" & oShortNote.ReleaseNumber, oShortNote.DelNoteDate, oShortNote.Narrative, goSession.UserInitials, oDelNote.DocNo, strDelNotes, True, False)
    ucpbProgress.Visible = False
    SaveDelNote = True
    ucpbProgress.Visible = False
    Call MsgBoxEx("Delivery note successfully saved" & vbCrLf & mstrDispMsg & oDelNote.DocNo & vbCrLf & "Total Cost of Del Note is " & Format(oDelNote.Cost, "0.00") & strRetMsg & strShrtMsg, vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
    If blnSubmit Then
        Call oDelNote.CreateFile
        If ((oRetNote Is Nothing) = False) Then Call oRetNote.CreateFile
        If ((oShortNote Is Nothing) = False) Then Call oShortNote.CreateFile
    End If
        
End Function 'SaveDelNote
'<CACH>****************************************************************************************
'* Sub:  UpdateDeliveryNote()
'**********************************************************************************************
'* Description: Used to update an existing Delivery Note with the Lines.
'**********************************************************************************************
'* Parameters:
'* In blnSubmit : Boolean - flag if system must Create file and Send to H/O
'**********************************************************************************************
'* History:
'* 04/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function UpdateDeliveryNote(ByVal blnSubmit As Boolean) As Boolean

Dim oLine           As Object
Dim oRetLine        As Object
Dim lngLineNo       As Long
Dim dblCharges      As Double
Dim dblQty          As Double
Dim dblShortages    As Double
Dim strNotes        As String
Dim blnPrint        As Boolean
Dim strConMsg       As String
Dim strPCode        As String
Dim strRetCode      As String
Dim strRetComment   As String
Dim blnAllowBDate   As Boolean
Dim strRetMsg       As String
Dim strShrtMsg      As String
Dim oOverDelLine    As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".UpdateDelNote"

    blnAllowBDate = goSession.GetParameter(PRM_ALLOW_BACK_DATE_DEL_DATE)
    UpdateDeliveryNote = False
    sprdDelNotes.Row = 1
    If sprdDelNotes.Text = "" Then
        Call MsgBoxEx("Delivery Note Number not entered" & vbCrLf & "Enter valid value and re-save", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        sprdDelNotes.SetFocus
        Exit Function
    End If
    
    If (GetDate(lblOrderDate.Caption) > GetDate(dtxtDueDate.Text)) And (blnAllowBDate = False) Then
        Call MsgBoxEx("Delivery Note receipt date invalid" & vbCrLf & "Receipt date must be greater than Order date", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        dtxtDueDate.SetFocus
        Exit Function
    End If
    
    If (GetDate(dtxtDueDate.Text) > CDate(Now)) Then
        Call MsgBoxEx("Delivery Note receipt date invalid" & vbCrLf & "Receipt date must be later than today", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        dtxtDueDate.SetFocus
        Exit Function
    End If
    
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_QTYSHORT
    
    'If Qty Short is greater than 0 then default note 1 to Hold Payment
    If Val(sprdOrder.Text) > 0 Then
        mstrDocCmnt = "Problems - Hold Payment" & vbTab
    End If
    
    sprdOrder.Col = COL_QTYRET
    
    'If Qty Returned is greater than 0 then default note 1 to Hold Payment
    If Val(sprdOrder.Text) > 0 Then
        mstrDocCmnt = "Problems - Hold Payment" & vbTab
    End If
    
    'If we have not defaulted a note 1 then set it to Delivery OK
    If mstrDocCmnt = "" Then
        mstrDocCmnt = "Delivery OK" & vbTab
    End If

    'check if any comments for Delivery Note
    Load frmNotes
    If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatDeliveryNote) = False Then 'cancelled pressed so exit save
        Unload frmNotes
        Exit Function
    End If
    Unload frmNotes
    
    moDelNote.CarraigeValue = Val(InputBox$("Enter any delivery charges" & vbCrLf & "Leave blank for none", "Record " & mstrFunction & " charges", moDelNote.CarraigeValue))
    
    ucpbProgress.Caption1 = "Updating Delivery Note header"
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Max = 1
    DoEvents
    
    'Populate delivery note header
    moDelNote.ReceiptDate = GetDate(dtxtDueDate.Text)
    moDelNote.Commit = "N"
    moDelNote.Narrative = strNotes
    moDelNote.EnteredBy = goSession.UserInitials
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_DELTOTAL
    moDelNote.Value = Val(sprdOrder.Text)
    sprdOrder.Col = COL_DELCOST
    moDelNote.Cost = Val(sprdOrder.Text)
    If blnSubmit = True Then moDelNote.Commit = "Y"
    'Move entered supplier delivery note numbers onto Del Note object
    For lngLineNo = 1 To 9 Step 1
        moDelNote.SupplierDelNote(lngLineNo) = ""
    Next lngLineNo
    For lngLineNo = 1 To sprdDelNotes.MaxRows Step 1
        sprdDelNotes.Row = lngLineNo
        moDelNote.SupplierDelNote(lngLineNo) = sprdDelNotes.Text
    Next lngLineNo
    
    ucpbProgress.Caption1 = "Saving Delivery Note lines"
    ucpbProgress.Max = sprdOrder.MaxRows + 2  'add 2 extra - Save function + Save Ret Note
    
    'check if Returns Note has been modified and needs saving
    If mblnUpdRetNote = True Then
        If moSuppRetNote Is Nothing Then 'create Return Note Obj if not done yet
            Set moSuppRetNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
            moSuppRetNote.SupplierNumber = lblSuppNo.Caption
            moSuppRetNote.DelNoteDate = Now
            moSuppRetNote.OrigPONumber = lblOrderNo.Caption
            moSuppRetNote.EnteredBy = goSession.UserInitials
            moSuppRetNote.Commit = "N"
            moSuppRetNote.OriginalDelNoteDRL = moDelNote.DocNo
            Set moSuppRetNote.DelNote = moDelNote
        End If 'return note must be created
        
        'Update header with new totals
        sprdOrder.Row = ROW_TOTALS
        sprdOrder.Col = COL_RETVAL
        moSuppRetNote.Value = Val(sprdOrder.Text)
        sprdOrder.Col = COL_RETCOST
        moSuppRetNote.Cost = Val(sprdOrder.Text)
        moSuppRetNote.Narrative = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
    End If
    
    'check if Shortages Note has been modified and needs saving
    If mblnUpdShrtNote = True Then
        If moSuppShortNote Is Nothing Then 'create Return Note Obj if not done yet
            Set moSuppShortNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
            moSuppShortNote.SupplierNumber = lblSuppNo.Caption
            moSuppShortNote.DelNoteDate = Now
            moSuppShortNote.OrigPONumber = lblOrderNo.Caption
            moSuppShortNote.EnteredBy = goSession.UserInitials
            moSuppShortNote.Commit = "N"
            moSuppShortNote.OriginalDelNoteDRL = moDelNote.DocNo
            moSuppShortNote.ShortageNote = True
            Set moSuppShortNote.DelNote = moDelNote
        End If 'return note must be created
        
        'Update header with new totals
        sprdOrder.Row = ROW_TOTALS
        sprdOrder.Col = COL_SHORTVAL
        moSuppShortNote.Value = Val(sprdOrder.Text)
        sprdOrder.Col = COL_SHORTCOST
        moSuppShortNote.Cost = Val(sprdOrder.Text)
        moSuppShortNote.Narrative = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
    End If
    
    'check if overs has been modified and needs saving
    If mblnUpdOver = True Then
        If moOverDelNote Is Nothing Then 'create Return Note Obj if not done yet
            Set moOverDelNote = goDatabase.CreateBusinessObject(CLASSID_DELNOTEHEADER)
            moOverDelNote.SupplierONumber = lblSuppNo.Caption
            moOverDelNote.DelNoteDate = Now
            moOverDelNote.PODate = GetDate(lblOrderDate.Caption)
            moOverDelNote.ReceiptDate = GetDate(dtxtDueDate.Text)
            'We don't assign a PO number as this is an over
            moOverDelNote.PONumber = ""
            moOverDelNote.AssignedPORelease = lblReleaseNo.Caption
            moOverDelNote.SOQControlNumber = lblSOQNumber.Caption
            moOverDelNote.OrigDocKey = 0
            moOverDelNote.CarraigeValue = dblCharges
            moOverDelNote.Commit = "N"
            moOverDelNote.Source = "X"
            moOverDelNote.Narrative = strNotes
            moOverDelNote.EnteredBy = goSession.UserInitials
            moOverDelNote.OrigDocNo = moDelNote.OrigDocNo
        End If
        
        'oOverDelNote.Memo = oDelNote.DocNo
        sprdOrder.Row = ROW_TOTALS
        sprdOrder.Col = COL_OVERVAL
        moOverDelNote.Value = Val(sprdOrder.Text)
        sprdOrder.Col = COL_OVERCOST
        moOverDelNote.Cost = Val(sprdOrder.Text)
        sprdOrder.Row = lngLineNo
    End If
    
    'Update lines on Order
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        ucpbProgress.Value = lngLineNo
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = 0
        If sprdOrder.Text = UPDATE_FLAG Then 'line has been marked for updating in DB
            sprdOrder.Col = COL_PARTCODE
            'check if part code is editable, this is a NEW line added
            If (sprdOrder.Lock = False) And (sprdOrder.Text <> "") Then  'add valid line to Delivery Note
                sprdOrder.Col = COL_PARTCODE
                Set oLine = moDelNote.AddItem(sprdOrder.Text)
                oLine.PartCodeAlphaCode = sprdOrder.Text
                sprdOrder.Col = COL_SELLPRICE
                oLine.OrderPrice = Val(sprdOrder.Text)
                oLine.CurrentPrice = Val(sprdOrder.Text)
                sprdOrder.Col = COL_COST
                oLine.CurrentCost = Val(sprdOrder.Text)
                sprdOrder.Col = COL_QTY
                oLine.OrderQty = Val(sprdOrder.Text)
                sprdOrder.Col = COL_QTYDEL
                oLine.ReceivedQty = Val(sprdOrder.Text)
                sprdOrder.Col = COL_QTYBORD
                If sprdOrder.ColHidden = False Then oLine.ToFollowQty = Val(sprdOrder.Text)
                sprdOrder.Col = COL_ORIGLINENO
                oLine.POLineNumber = Val(sprdOrder.Text)
                sprdOrder.Col = COL_QTYSELLIN
                oLine.SinglesSellingUnit = Val(sprdOrder.Text)
                sprdOrder.Col = COL_COMMENT
                oLine.Narrative = sprdOrder.Text
            End If 'add new line to delivery note
            'check if part code is locked, then update existing line
            sprdOrder.Col = COL_PARTCODE
            If (sprdOrder.Lock = True) Then  'update line on Delivery Note
                strPCode = sprdOrder.Text
                sprdOrder.Col = COL_QTYDEL
                If Val(sprdOrder.Text) = 0 Then
                    sprdOrder.Col = COL_ITEMNO
                    Call moDelNote.DeleteItem(sprdOrder.Text, strPCode)
                Else
                    dblQty = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_ITEMNO
                    Call moDelNote.UpdateItemQuantity(sprdOrder.Text, strPCode, dblQty)
                End If
            End If 'existing line must be edited
            
            If mblnUpdRetNote = True Then
                'Check if any Return quantities entered, if so populate Return Note
                sprdOrder.Col = COL_QTYRET
                If Val(sprdOrder.Text) > 0 Then
                    sprdOrder.Row = lngLineNo
                    sprdOrder.Col = COL_QTYRET
                    dblQty = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_PARTCODE
                    strPCode = sprdOrder.Text
                    'Update/Append the line
                    sprdOrder.Col = COL_RETLINENO
                    If Val(sprdOrder.Text) = 0 Then
                        sprdOrder.Col = COL_PARTCODE
                        Set oRetLine = moSuppRetNote.AddItem(sprdOrder.Text)
                        oRetLine.PartCodeAlphaCode = sprdOrder.Text
                        sprdOrder.Col = COL_QTYRET
                        oRetLine.ReturnQty = sprdOrder.Text
                        oRetLine.ReturnedQty = sprdOrder.Text
                        sprdOrder.Col = COL_QTYSELLIN
                        oRetLine.SinglesSellingUnit = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_SELLPRICE
                        oRetLine.OrderPrice = Val(sprdOrder.Text)
                        oRetLine.CurrentPrice = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_COST
                        oRetLine.CurrentCost = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_RETCODE
                        oRetLine.ReturnReasonCode = Left$(sprdOrder.Text, InStr(sprdOrder.Text, "-") - 1)
                        sprdOrder.Col = COL_RETCMNT
                        oRetLine.Narrative = sprdOrder.Text
                        sprdOrder.Col = COL_ORIGLINENO
                        oRetLine.POLineNumber = Val(sprdOrder.Text)
                    Else
                        sprdOrder.Col = COL_RETCODE
                        strRetCode = Left$(sprdOrder.Text, InStr(sprdOrder.Text, "-") - 1)
                        sprdOrder.Col = COL_RETCMNT
                        strRetComment = sprdOrder.Text
                        sprdOrder.Col = COL_RETLINENO
                        Call moSuppRetNote.UpdateItemQuantity(sprdOrder.Text, strPCode, dblQty, strRetCode, strRetComment)
                    End If
                Else 'check if no quantity but still on Return Note - clear value
                    sprdOrder.Col = COL_RETLINENO
                    If Val(sprdOrder.Text) > 0 Then
                        sprdOrder.Col = COL_PARTCODE
                        strPCode = sprdOrder.Text
                        'delete line from Supplier Return
                        sprdOrder.Col = COL_RETLINENO
                        Call moSuppRetNote.DeleteItem(sprdOrder.Text, strPCode)
                    End If
                End If 'valid quantity entered for Returning
            End If 'check Supplier return was updated, so check action for line
            If mblnUpdShrtNote = True Then
                'Check if any Short quantities entered, if so populate Shortage Note
                sprdOrder.Col = COL_QTYSHORT
                               
                If (Val(sprdOrder.Text) > 0) And (sprdOrder.ForeColor = vbRed) Then
                    sprdOrder.Row = lngLineNo
                    sprdOrder.Col = COL_QTYSHORT
                    dblQty = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_PARTCODE
                    strPCode = sprdOrder.Text
                    'Update/Append the line
                    sprdOrder.Col = COL_SHORTLINENO
                    If Val(sprdOrder.Text) = 0 Then
                        sprdOrder.Col = COL_PARTCODE
                        Set oRetLine = moSuppShortNote.AddItem(sprdOrder.Text)
                        oRetLine.PartCodeAlphaCode = sprdOrder.Text
                        sprdOrder.Col = COL_QTYSHORT
                        oRetLine.ReturnQty = sprdOrder.Text
                        oRetLine.ReturnedQty = sprdOrder.Text
                        sprdOrder.Col = COL_QTYSELLIN
                        oRetLine.SinglesSellingUnit = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_SELLPRICE
                        oRetLine.OrderPrice = Val(sprdOrder.Text)
                        oRetLine.CurrentPrice = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_COST
                        oRetLine.CurrentCost = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_RETCODE
                        oRetLine.ReturnReasonCode = mstrDefShortageCode
                        sprdOrder.Col = COL_ORIGLINENO
                        oRetLine.POLineNumber = Val(sprdOrder.Text)
                    Else
                        strRetCode = mstrDefShortageCode
                        sprdOrder.Col = COL_SHORTLINENO
                        Call moSuppShortNote.UpdateItemQuantity(sprdOrder.Text, strPCode, dblQty, strRetCode, "")
                    End If
                End If
                
                'If it is equal to 0 or and over
                If (Val(sprdOrder.Text) = 0) Or (sprdOrder.ForeColor = vbBlue) Then
                    sprdOrder.Col = COL_SHORTLINENO
                    If Val(sprdOrder.Text) > 0 Then
                        sprdOrder.Col = COL_PARTCODE
                        strPCode = sprdOrder.Text
                        'delete line from Supplier Return
                        sprdOrder.Col = COL_SHORTLINENO
                        Call moSuppShortNote.DeleteItem(sprdOrder.Text, strPCode)
                    End If
                End If
            End If 'check Supplier return was updated, so check action for line
            
            If mblnUpdOver = True Then
                'Check if any over quantities entered, if so populate Over Note
                sprdOrder.Col = COL_PARTCODE
                strPCode = sprdOrder.Text
                
                sprdOrder.Col = COL_QTYSHORT
                                
                dblQty = Val(sprdOrder.Text)
                
                If (dblQty > 0) And (sprdOrder.ForeColor = vbBlue) Then
                    sprdOrder.Col = COL_OVERLINENO
                    
                    'New Item
                    If Val(sprdOrder.Text) = 0 Then
                        sprdOrder.Col = COL_PARTCODE
                        Set oOverDelLine = moOverDelNote.AddItem(sprdOrder.Text)
                        oOverDelLine.PartCodeAlphaCode = sprdOrder.Text
                        sprdOrder.Col = COL_SELLPRICE
                        oOverDelLine.OrderPrice = Val(sprdOrder.Text)
                        oOverDelLine.CurrentPrice = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_COST
                        oOverDelLine.CurrentCost = Val(sprdOrder.Text)
                        'Set to 0 to show an over
                        oOverDelLine.OrderQty = 0
                        sprdOrder.Col = COL_QTYOVER
                        'Set to over field
                        oOverDelLine.ReceivedQty = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_QTYBORD
                        oOverDelLine.ToFollowQty = 0
                        sprdOrder.Col = COL_ORIGLINENO
                        oOverDelLine.POLineNumber = 0
                        sprdOrder.Col = COL_QTYSELLIN
                        oOverDelLine.SinglesSellingUnit = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_COMMENT
                        oOverDelLine.Narrative = sprdOrder.Text
                        
                        oOverDelLine.OverQty = 0
                        
                        sprdOrder.Col = COL_PARTCODE
                        
                        'Is this a new line
                        If sprdOrder.Lock = True Then
                            sprdOrder.Col = COL_ITEMNO
                            
                            Call moDelNote.UpdateOverQuantity(sprdOrder.Text, strPCode, dblQty)
                        Else
                            oLine.OverQty = dblQty
                        End If
                    Else
                        Call moOverDelNote.UpdateItemQuantity(sprdOrder.Text, strPCode, dblQty)
                                              
                        sprdOrder.Col = COL_ITEMNO
                    
                        Call moDelNote.UpdateOverQuantity(sprdOrder.Text, strPCode, dblQty)
                    End If
                End If
                
                'If it is equal to 0 or and over
                If (Val(sprdOrder.Text) = 0) Or (sprdOrder.ForeColor = vbRed) Then
                    sprdOrder.Col = COL_OVERLINENO
                    
                    Call moOverDelNote.DeleteItem(sprdOrder.Text, strPCode)
                    
                    sprdOrder.Col = COL_ITEMNO
                    
                    Call moOverDelNote.UpdateOverQuantity(sprdOrder.Text, strPCode, 0)
                End If
            End If 'Do we have change to overs
        End If 'line was flagged for updating
    Next lngLineNo
    
    'check if Supplier return has been created and if any restock charges must be captured
    If ((mblnUpdRetNote = True) And ((mdblRestockCharge > 0) Or (mdblRestockRate > 0)) And (goSession.GetParameter(PRM_RETURN_CHARGES) = True)) Then
        If mdblRestockRate > 0 Then 'calculate restock charge based on % of value
            dblCharges = moSuppRetNote.Value * mdblRestockRate / 100
        Else
            dblCharges = mdblRestockCharge
        End If
        'HARD CODED FIX
        dblCharges = 0
        'Confirm restock charge
        dblCharges = Val(InputBox$("Confirm/enter any restock charges" & vbCrLf, "Record restock charges", Format(dblCharges, "0.00")))
        moSuppRetNote.RestockCharge = dblCharges
    End If
    
    'Perform saving functions within Transaction
    ucpbProgress.Caption1 = "Saving to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    Call goDatabase.StartTransaction
    Call moDelNote.IBo_SaveIfExists
    If (mblnUpdRetNote = True) Then
        ucpbProgress.Caption1 = "Saving return note to database"
        ucpbProgress.Value = ucpbProgress.Value + 1
        If (moSuppRetNote.DocNo = "") Then
            Call moSuppRetNote.IBo_SaveIfNew
        Else
            Call moSuppRetNote.IBo_SaveIfExists
        End If
        strRetMsg = vbCrLf & "Total Cost of Return Note is " & Format(moSuppRetNote.Cost, "0.00")
    End If
    If (mblnUpdShrtNote = True) Then
        ucpbProgress.Caption1 = "Saving Shortages note to database"
        If (moSuppShortNote.DocNo = "") Then
            Call moSuppShortNote.IBo_SaveIfNew
        Else
            Call moSuppShortNote.IBo_SaveIfExists
        End If
        strShrtMsg = vbCrLf & "Total Cost of Shortage Note is " & Format(moSuppShortNote.Cost, "0.00")
    End If
    If (mblnUpdOver = True) Then
        ucpbProgress.Caption1 = "Saving Overs note to database"
        If (moOverDelNote.DocNo = "") Then
            Call moOverDelNote.IBo_SaveIfNew
        Else
            Call moOverDelNote.IBo_SaveIfExists
        End If
        
        If moDelNote.OrigDocNo = "" Then
            Call moDelNote.RecordOrigDocNo(moOverDelNote.DocNo)
        End If
    End If
    Call goDatabase.CommitTransaction
    
    ucpbProgress.Caption1 = "Creating print-outs"
    blnPrint = False
    
    Select Case (mlngDNAutoPrint)
        Case (PRINT_NONE): blnPrint = False
        Case (PRINT_PROMPT):
                            If cmdViewCons.Visible Then strConMsg = "Consolidation Sheet and "
                            If MsgBoxEx("Delivery Note completed - Print " & strConMsg & " Delivery Note Now", vbYesNoCancel + vbQuestion, "Process Delivery Note", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
        Case Else: blnPrint = True
    End Select
    If blnPrint Then Call PrintDeliveryNote(moDelNote.DocNo, moDelNote.ConsignmentRef, moDelNote.DelNoteDate)
    If (mblnUpdRetNote = True) Then Call PrintSupplierReturn(moSuppRetNote.SupplierReturnNumber, moSuppRetNote.DocNo & "/" & moSuppRetNote.ReleaseNumber, moSuppRetNote.DelNoteDate, moSuppRetNote.Narrative, goSession.UserInitials, moSuppRetNote.DocNo, moSuppRetNote.OriginalDelNoteDRL, False, False)
    
    If (mblnUpdShrtNote = True) Then Call PrintSupplierReturn(moSuppShortNote.SupplierReturnNumber, moSuppShortNote.DocNo & "/" & moSuppShortNote.ReleaseNumber, moSuppShortNote.DelNoteDate, moSuppShortNote.Narrative, goSession.UserInitials, moSuppShortNote.DocNo, moSuppShortNote.OriginalDelNoteDRL, True, False)
    ucpbProgress.Visible = False
    UpdateDeliveryNote = True
    Call MsgBoxEx("Delivery note successfully updated" & vbCrLf & mstrDispMsg & moDelNote.DocNo & vbCrLf & "Total Cost of Del Note is " & Format(moDelNote.Cost, "0.00") & strRetMsg & strShrtMsg, vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
        
End Function 'UpdateDeliveryNote

'<CACH>****************************************************************************************
'* Sub:  UpdateSupplierReturn()
'**********************************************************************************************
'* Description: Used to update an existing Supplier Return/ Shortage Note with the Lines.
'**********************************************************************************************
'* Parameters:
'* In blnSubmit : Boolean - flag if system must Create file and Send to H/O
'**********************************************************************************************
'* History:
'* 04/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function UpdateSupplierReturn(ByVal blnSubmit As Boolean) As Boolean

Dim oLine         As Object
Dim oRetLine      As Object
Dim lngLineNo     As Long
Dim dblCharges    As Double
Dim dblQty        As Double
Dim dblShortages  As Double
Dim strNotes      As String
Dim blnPrint      As Boolean
Dim strConMsg     As String
Dim strPCode      As String
Dim strRetCode    As String
Dim strRetComment As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".UpdateDelNote"

    UpdateSupplierReturn = False
    sprdDelNotes.Row = 1
    
    If (dtxtDueDate.Visible = True) And (dtxtDueDate.Text <> "") Then 'check date if Return Note
        If (GetDate(lblOrderDate.Caption) > GetDate(dtxtDueDate.Text)) Then
            Call MsgBoxEx("Collection date invalid" & vbCrLf & "Collection date cannot be before Request date", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
            dtxtDueDate.SetFocus
            Exit Function
        End If
    End If
    
    'check if any comments for Supplier Return Note if not a shortage
    strNotes = mstrNotes
    If moSuppRetNote.ShortageNote = False Then
        Load frmNotes
        If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatReturn) = False Then 'cancelled pressed so exit save
            Unload frmNotes
            Exit Function
        End If
        Unload frmNotes
    End If
      
    ucpbProgress.Caption1 = "Updating Credit Note header"
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Max = 1
    DoEvents
    
    'Populate delivery note header
    moSuppRetNote.ReturnDate = GetDate(dtxtDueDate.Text)
    moSuppRetNote.Commit = "N"
    moSuppRetNote.Narrative = strNotes
    moSuppRetNote.EnteredBy = goSession.UserInitials
    moSuppRetNote.Comment = txtReference.Text 'store captured delivery note no's
    moSuppRetNote.CollectionNoteNumber = sprdDelNotes.Text
    If blnSubmit = True Then moSuppRetNote.Commit = "Y"
    
    ucpbProgress.Caption1 = "Saving Credit Note lines"
    ucpbProgress.Max = sprdOrder.MaxRows + 2  'add 2 extra - Save function + Save Ret Note
    
    'Update header with new totals
    sprdOrder.Row = ROW_TOTALS
    If moSuppRetNote.ShortageNote = False Then
        sprdOrder.Col = COL_RETVAL
        moSuppRetNote.Value = Val(sprdOrder.Text)
        sprdOrder.Col = COL_RETCOST
        moSuppRetNote.Cost = Val(sprdOrder.Text)
    Else
        sprdOrder.Col = COL_SHORTVAL
        moSuppRetNote.Value = Val(sprdOrder.Text)
        sprdOrder.Col = COL_SHORTCOST
        moSuppRetNote.Cost = Val(sprdOrder.Text)
    End If
    
    'Update lines on Order
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        ucpbProgress.Value = lngLineNo
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = 0
        If sprdOrder.Text = UPDATE_FLAG Then 'line has been marked for updating in DB
            If moSuppRetNote.ShortageNote = False Then 'update Credit Request
                'Check if any Return quantities entered, if so populate Return Note
                sprdOrder.Col = COL_QTYRET
                If Val(sprdOrder.Text) > 0 Then
                    sprdOrder.Row = lngLineNo
                    sprdOrder.Col = COL_QTYRET
                    dblQty = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_PARTCODE
                    strPCode = sprdOrder.Text
                    'Update/Append the line
                    sprdOrder.Col = COL_RETCODE
                    strRetCode = Left$(sprdOrder.Text, InStr(sprdOrder.Text, "-") - 1)
                    sprdOrder.Col = COL_RETCMNT
                    strRetComment = sprdOrder.Text
                    sprdOrder.Col = COL_RETLINENO
                    Call moSuppRetNote.UpdateItemQuantity(sprdOrder.Text, strPCode, dblQty, strRetCode, strRetComment)
                Else 'check if no quantity but still on Return Note - clear value
                    sprdOrder.Col = COL_PARTCODE
                    strPCode = sprdOrder.Text
                    'delete line from Supplier Return
                    sprdOrder.Col = COL_RETLINENO
                    Call moSuppRetNote.DeleteItem(sprdOrder.Text, strPCode)
                End If 'valid quantity entered for Returning
            Else 'process Shortage Note updates
                'Check if any Short quantities entered, if so populate Shortage Note
                sprdOrder.Col = COL_QTYSHORT
                If Val(sprdOrder.Text) > 0 Then
                    sprdOrder.Row = lngLineNo
                    sprdOrder.Col = COL_QTYSHORT
                    dblQty = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_PARTCODE
                    strPCode = sprdOrder.Text
                    'Update/Append the line
                    strRetCode = mstrDefShortageCode
                    sprdOrder.Col = COL_RETLINENO
                    Call moSuppRetNote.UpdateItemQuantity(sprdOrder.Text, strPCode, dblQty, strRetCode, "")
                Else 'check if no quantity but still on Shortage Note - clear value
                    sprdOrder.Col = COL_PARTCODE
                    strPCode = sprdOrder.Text
                    'delete line from Supplier Return
                    sprdOrder.Col = COL_RETLINENO
                    Call moSuppRetNote.DeleteItem(sprdOrder.Text, strPCode)
                End If 'valid quantity entered for Returning
            End If 'check Supplier return was updated, so check action for line
        End If 'line was flagged for updating
    Next lngLineNo
    
    'check if Supplier return has been created and if any restock charges must be captured
    If ((mdblRestockCharge > 0) Or (mdblRestockRate > 0)) And (goSession.GetParameter(PRM_RETURN_CHARGES) = True) And (moSuppRetNote.ShortageNote = False) Then
        If mdblRestockRate > 0 Then 'calculate restock charge based on % of value
            dblCharges = moSuppRetNote.Value * mdblRestockRate / 100
        Else
            dblCharges = mdblRestockCharge
        End If
        'HARD CODED FIX
        dblCharges = 0
        'Confirm restock charge
        dblCharges = Val(InputBox$("Confirm/enter any restock charges" & vbCrLf, "Record restock charges", Format(dblCharges, "0.00")))
        moSuppRetNote.RestockCharge = dblCharges
    End If
    
    'Perform saving functions within Transaction
    ucpbProgress.Caption1 = "Saving to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    Call goDatabase.StartTransaction
    Call moSuppRetNote.IBo_SaveIfExists
    Call goDatabase.CommitTransaction
    
    ucpbProgress.Caption1 = "Creating print-outs"
    blnPrint = False
    
    Call PrintSupplierReturn(moSuppRetNote.SupplierReturnNumber, moSuppRetNote.DocNo & "/" & moSuppRetNote.ReleaseNumber, moSuppRetNote.DelNoteDate, strNotes, goSession.UserInitials, moSuppRetNote.OriginalDelNoteDRL, moSuppRetNote.Comment, moSuppRetNote.ShortageNote, False)
    
    If blnSubmit Then Call moSuppRetNote.CreateFile
    
    Call MsgBoxEx("Credit request successfully updated" & vbCrLf & "Total Cost is " & Format(moSuppRetNote.Cost, "0.00"), vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
    
    ucpbProgress.Visible = False
    UpdateSupplierReturn = True
        
End Function 'UpdateSupplierReturn

'<CACH>****************************************************************************************
'* Sub:  SaveReturn()
'**********************************************************************************************
'* Description: Used to save the Supplier Return Note with the Lines.
'**********************************************************************************************
'* Parameters:
'* In blnSubmit : Boolean - flag if system must Create file and Send to H/O
'**********************************************************************************************
'* History:
'* 04/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function SaveReturn(ByVal blnSubmit As Boolean) As Boolean

Dim lngLineNo  As Long
Dim oRetNote   As Object
Dim oRetLine   As Object
Dim strNotes   As String
Dim dblCharges As Double
Dim blnPrint   As Boolean

Const PROCEDURE_NAME As String = MODULE_NAME & ".SaveReturn"

    SaveReturn = False
    sprdDelNotes.Col = 1
    sprdDelNotes.Row = 1
    If (sprdDelNotes.Text = "") And (mblnSRColNoteReq = True) Then
        Call MsgBoxEx("Supplier Collection Note Number not entered" & vbCrLf & "Enter valid value and re-save", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        sprdDelNotes.SetFocus
        Exit Function
    End If
    
    If (GetDate(dtxtDueDate.Text) < Date) And (dtxtDueDate.Enabled = True) Then
        Call MsgBoxEx("Return Note collection date invalid" & vbCrLf & "Collection date cannot be before today", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        dtxtDueDate.SetFocus
        Exit Function
    End If
    
    If txtReference.Text = "" Then 'Original supplier delivery note not captured
        Call MsgBoxEx("Supplier delivery note number(s) not entered", vbExclamation, "Capture delivery note", , , , , RGBMsgBox_WarnColour)
        Call txtReference.SetFocus
        Exit Function
    End If
    
    'check if any comments for Supplier Returns
    Load frmNotes
    If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatReturn) = False Then 'cancelled pressed so exit save
        Unload frmNotes
        Exit Function
    End If
    Unload frmNotes
    
    ucpbProgress.Caption1 = "Saving Supplier Credit Request header"
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Max = 1
    DoEvents
    
    'Create and populate Supplier Return note
    Set oRetNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
    oRetNote.SupplierNumber = lblSuppNo.Caption
    oRetNote.DelNoteDate = Now
    oRetNote.PODate = GetDate(lblOrderDate.Caption)
    If dtxtDueDate.Enabled = True Then oRetNote.ReturnDate = GetDate(dtxtDueDate.Text)
    oRetNote.PONumber = lblOrderNo.Caption
    oRetNote.Commit = "N"
    If blnSubmit = True Then oRetNote.Commit = "Y"
    oRetNote.EnteredBy = goSession.UserInitials
    oRetNote.Narrative = strNotes
'    oRetNote.OriginalDelNoteDRL = txtReference.Text
    oRetNote.Comment = txtReference.Text
'    oRetNote.Release
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_RETVAL
    oRetNote.Value = Val(sprdOrder.Text)
    sprdOrder.Col = COL_RETCOST
    oRetNote.Cost = Val(sprdOrder.Text)
    
    oRetNote.CollectionNoteNumber = sprdDelNotes.Text
    
    'check if Supplier return has been created and if any restock charges must be captured
    If ((mdblRestockCharge > 0) Or (mdblRestockRate > 0)) And (goSession.GetParameter(PRM_RETURN_CHARGES) = True) Then
        If mdblRestockRate > 0 Then 'calculate restock charge based on % of value
            dblCharges = oRetNote.Value * mdblRestockRate / 100
        Else
            dblCharges = mdblRestockCharge
        End If
        'HARD CODED FIX
        dblCharges = 0
        'Confirm restock charge
        dblCharges = Val(InputBox$("Confirm/enter any restock charges" & vbCrLf, "Record restock charges", Format(dblCharges, "0.00")))
        oRetNote.RestockCharge = dblCharges
    End If
   
    ucpbProgress.Caption1 = "Saving Credit Request Note lines"
    ucpbProgress.Max = sprdOrder.MaxRows + 2  'add 2 extra - Save function + Save Return
    
    'Add lines to Order
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows - 1 Step 1
        ucpbProgress.Value = lngLineNo
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_PARTCODE
        Set oRetLine = oRetNote.AddItem(sprdOrder.Text)
        oRetLine.PartCodeAlphaCode = sprdOrder.Text
        sprdOrder.Col = COL_SELLPRICE
        oRetLine.OrderPrice = Val(sprdOrder.Text)
        oRetLine.CurrentPrice = Val(sprdOrder.Text)
        sprdOrder.Col = COL_COST
        oRetLine.CurrentCost = Val(sprdOrder.Text)
        sprdOrder.Col = COL_QTYRET
        oRetLine.OrderQty = Val(sprdOrder.Text)
        oRetLine.ReturnQty = Val(sprdOrder.Text)
        sprdOrder.Col = COL_ORIGLINENO
        oRetLine.POLineNumber = Val(sprdOrder.Text)
        sprdOrder.Col = COL_QTYSELLIN
        oRetLine.SinglesSellingUnit = Val(sprdOrder.Text)
        sprdOrder.Col = COL_QTYRET
        oRetLine.ReturnedQty = Val(sprdOrder.Text)
        sprdOrder.Col = COL_RETCODE
        oRetLine.ReturnReasonCode = Left$(sprdOrder.Text, InStr(sprdOrder.Text, "-") - 1)
        sprdOrder.Col = COL_RETCMNT
        oRetLine.Narrative = sprdOrder.Text
    Next lngLineNo
    
    'Perform saving functions within Transaction
    ucpbProgress.Caption1 = "Saving to database"
    ucpbProgress.Value = ucpbProgress.Value + 1
    Call goDatabase.StartTransaction
    Call oRetNote.IBo_SaveIfNew
    Call goDatabase.CommitTransaction
    blnPrint = False
    ucpbProgress.Caption1 = "Creating print-outs"
    Select Case (mlngSRAutoPrint)
        Case (PRINT_NONE): blnPrint = False
        Case (PRINT_PROMPT):
                           If MsgBoxEx("Supplier Return completed - Print Now", vbYesNoCancel + vbQuestion, "Process Supplier Return", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
        Case Else: blnPrint = True
    End Select
    If blnPrint = True Then Call PrintSupplierReturn(oRetNote.SupplierReturnNumber, oRetNote.DocNo & "/" & oRetNote.ReleaseNumber, oRetNote.DelNoteDate, strNotes, goSession.UserInitials, "N/A", txtReference.Text, False, False)
    SaveReturn = True
    Call MsgBoxEx("Credit request successfully saved" & vbCrLf & "Total Cost is " & Format(oRetNote.Cost, "0.00"), vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
    ucpbProgress.Caption1 = "Creating Head Office file"
    If blnSubmit Then Call oRetNote.CreateFile
    ucpbProgress.Visible = False
        
End Function 'SaveReturn


'<CACH>****************************************************************************************
'* Sub:  SaveISTOut()
'**********************************************************************************************
'* Description: Used to save the IST Out Note with the Lines and print-out if required.
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 01/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function SaveISTOut() As Boolean

Dim oISTNote  As Object
Dim oLine     As Object
Dim lngLineNo As Long
Dim strNotes  As String
Dim blnPrint  As Boolean

Const PROCEDURE_NAME As String = MODULE_NAME & ".SaveISTOut"

    SaveISTOut = False
    If (sprdDelNotes.Text = "") And (sprdDelNotes.Visible = True) Then
        Call MsgBoxEx("Delivery Note Number not entered" & vbCrLf & "Enter valid value and re-save", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        sprdDelNotes.SetFocus
        Exit Function
    End If
       
    'check if any comments for Delivery Note
    Load frmNotes
    If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatStockWriteOff) = False Then    'cancelled pressed so exit save
        Unload frmNotes
        Exit Function
    End If
    Unload frmNotes
    mstrNotes = strNotes 'pass out to printing routine
    
    'Create and populate IST Out Header
    Set oISTNote = goDatabase.CreateBusinessObject(CLASSID_ISTOUTHEADER)
    oISTNote.StoreNumber = lblSuppNo.Caption
    oISTNote.DelNoteDate = Now
    oISTNote.OrigDocKey = Val(lblOrderID.Caption)
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_DELTOTAL
    oISTNote.Value = Val(sprdOrder.Text)
    sprdOrder.Col = COL_DELCOST
    oISTNote.Cost = Val(sprdOrder.Text)
    oISTNote.EnteredBy = goSession.UserInitials
    oISTNote.Narrative = strNotes
    If cmbDispatchType.Visible = True Then oISTNote.DespatchMethod = Left$(cmbDispatchType.Text, InStr(cmbDispatchType.Text, vbTab) - 1)
    
    'Add lines to Order
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows - 1 Step 1
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_QTY
        If Val(sprdOrder.Text) > 0 Then
            sprdOrder.Col = COL_PARTCODE
            Set oLine = oISTNote.AddItem(sprdOrder.Text)
            oLine.PartCodeAlphaCode = sprdOrder.Text
            sprdOrder.Col = COL_SELLPRICE
            oLine.OrderPrice = Val(sprdOrder.Text)
            oLine.CurrentPrice = Val(sprdOrder.Text)
            sprdOrder.Col = COL_COST
            oLine.CurrentCost = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTY
            oLine.OrderQty = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTYDEL
            oLine.ReceivedQty = Val(sprdOrder.Text)
            oLine.ISTQuantity = Val(sprdOrder.Text)
            sprdOrder.Col = COL_COMMENT
            oLine.Narrative = sprdOrder.Text
            sprdOrder.Col = COL_ORIGLINENO
            oLine.POLineNumber = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTYSELLIN
            oLine.SinglesSellingUnit = Val(sprdOrder.Text)
        End If 'delivery quantity entered
    Next lngLineNo
    
    'Perform saving functions within Transaction
    Call goDatabase.StartTransaction
    Call oISTNote.IBo_SaveIfNew
    Call goDatabase.CommitTransaction
    Call oISTNote.CreateFile
    blnPrint = False
    Select Case (mlngISTOutPrint)
        Case (PRINT_NONE): blnPrint = False
        Case (PRINT_PROMPT):
                           If MsgBoxEx("IST Out completed - Print Now", vbYesNoCancel + vbQuestion, "Process IST Out", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
        Case Else: blnPrint = True
    End Select
    If blnPrint = True Then Call PrintISTOutNote(oISTNote.DocNo, oISTNote.DelNoteDate, goSession.UserInitials)
    
    SaveISTOut = True
    Call MsgBoxEx("IST Out successfully saved (DRL No:" & oISTNote.DocNo & ")" & vbCrLf & "Total cost is " & Format(oISTNote.Cost, "0.00"), vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
        
End Function 'SaveISTOut

'<CACH>****************************************************************************************
'* Sub:  SaveISTIn()
'**********************************************************************************************
'* Description: Used to save the IST In Note with the Lines.
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 01/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function SaveISTIn() As Boolean

Dim oISTNote   As Object
Dim oLine      As Object
Dim lngLineNo  As Long
Dim strNotes   As String
Dim lngEDINo   As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".SaveISTIn"

    SaveISTIn = False
    sprdDelNotes.Col = 1
    sprdDelNotes.Row = 1
    If (sprdDelNotes.Text = "") And (sprdDelNotes.Visible = True) Then
        Call MsgBoxEx("IST Note Number not entered" & vbCrLf & "Enter valid value and re-save", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
        sprdDelNotes.SetFocus
        Exit Function
    End If
    
    'check if any comments for Delivery Note
    Load frmNotes
    If frmNotes.CaptureNotes(strNotes, mstrDocCmnt, enatISTIn) = False Then 'cancelled pressed so exit save
        Unload frmNotes
        Exit Function
    End If
    Unload frmNotes
    
    Screen.MousePointer = vbHourglass
    Set oISTNote = goDatabase.CreateBusinessObject(CLASSID_ISTINHEADER)
    oISTNote.StoreNumber = lblSuppNo.Caption
    oISTNote.DelNoteDate = Now
    oISTNote.IBT = sprdDelNotes.Text
'    oISTNote.PODate = txtDueDate.Text
    oISTNote.OrigDocKey = Val(lblOrderID.Caption)
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_DELTOTAL
    oISTNote.Value = Val(sprdOrder.Text)
    sprdOrder.Col = COL_DELCOST
    oISTNote.Cost = Val(sprdOrder.Text)
    oISTNote.EnteredBy = goSession.UserInitials
    oISTNote.ReceiptDate = GetDate(dtxtDueDate.Text)
    oISTNote.Narrative = strNotes
    
    'remove last line if Auto Add so that it is not saved
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Col = COL_PARTCODE
    If sprdOrder.Text = "" Then sprdOrder.MaxRows = sprdOrder.MaxRows - 1
    
    'Add lines to Order
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_QTYDEL
        If Val(sprdOrder.Text) > 0 Then
            sprdOrder.Col = COL_PARTCODE
            Set oLine = oISTNote.AddItem(sprdOrder.Text)
            oLine.PartCodeAlphaCode = sprdOrder.Text
            sprdOrder.Col = COL_SELLPRICE
            oLine.OrderPrice = Val(sprdOrder.Text)
            oLine.CurrentPrice = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTY
            oLine.OrderQty = Val(sprdOrder.Text)
            sprdOrder.Col = COL_COST
            oLine.CurrentCost = Val(sprdOrder.Text)
            sprdOrder.Col = COL_QTYDEL
            oLine.ReceivedQty = Val(sprdOrder.Text)
            oLine.ISTQuantity = Val(sprdOrder.Text)
            sprdOrder.Col = COL_COMMENT
            oLine.Narrative = sprdOrder.Text
            sprdOrder.Col = COL_ORIGLINENO
            oLine.POLineNumber = Val(sprdOrder.Text)
            lngEDINo = oLine.POLineNumber
            sprdOrder.Col = COL_QTYSELLIN
            oLine.SinglesSellingUnit = Val(sprdOrder.Text)
            If ((moEDIISTIn Is Nothing) = False) And (lngEDINo > 0) Then Call moEDIISTIn.RecordItemReceived(oLine.PartCode, oLine.POLineNumber, oLine.ISTQuantity)
        End If 'delivery quantity entered
    Next lngLineNo
    
    'Perform saving functions within Transaction
    Call goDatabase.StartTransaction
    Call oISTNote.IBo_SaveIfNew
    If Not moEDIISTIn Is Nothing Then 'update EDI IST In record
        sprdTotal.Row = 2
        sprdTotal.Col = COL_TOTQTYDEL
        Call moEDIISTIn.RecordReceived(oISTNote.DocNo, GetDate(dtxtDueDate.Text), Val(sprdTotal.Text), goSession.UserInitials)
    End If
    Call goDatabase.CommitTransaction
    
    Call MsgBoxEx("IST In successfully saved" & vbCrLf & mstrDispMsg & oISTNote.DocNo & vbCrLf & "Total cost is " & Format(oISTNote.Cost, "0.00"), vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
    
    SaveISTIn = True
    Set moEDIISTIn = Nothing
    Screen.MousePointer = vbNormal
        
End Function 'SaveISTIn

Private Sub ConfigColumns()

    'This hides the columns not required by any of the Functions
    sprdOrder.Col = COL_VATRATE
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_UNITS
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_COST
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_DISC
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_VATTOTAL
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_INCTOTAL
    sprdOrder.ColHidden = True
    
End Sub

Private Sub HidePrintColumn(lngColumnNo As Long)

    sprdOrder.Col = lngColumnNo
    If sprdOrder.ColHidden = False Then
        ReDim Preserve arDispCols(UBound(arDispCols) + 1)
        arDispCols(UBound(arDispCols)) = lngColumnNo
        sprdOrder.ColHidden = True
    End If

End Sub

Private Sub SetLastPrintCol()

Dim dblSpreadSize As Double
Dim lngColNo      As Long
    
    'Once columns have been hidden, locate last column displayed and calculate width
    For lngColNo = 1 To sprdOrder.MaxCols Step 1
        dblSpreadSize = dblSpreadSize + sprdOrder.ColWidth(lngColNo)
        sprdOrder.Col = lngColNo
        If sprdOrder.ColHidden = False Then lngLastCol = lngColNo
    Next lngColNo
        
    'set last column width to fill in page width
    dblOrigWidth = sprdOrder.ColWidth(lngLastCol)
    sprdOrder.ColWidth(lngLastCol) = 90 - dblSpreadSize + sprdOrder.ColWidth(lngLastCol)

End Sub
Private Sub DisplayPrintCols()

Dim lngColPos As Long

    For lngColPos = 1 To UBound(arDispCols) Step 1
        sprdOrder.Col = arDispCols(lngColPos)
        sprdOrder.ColHidden = False
    Next lngColPos
    sprdOrder.ColWidth(lngLastCol) = dblOrigWidth
    ReDim arDispCols(0)

End Sub
'<CACH>****************************************************************************************
'* Sub:  chkStoresOnly_Click()
'**********************************************************************************************
'* Description: Allows operator to switch between Purchase Order and Inter Branch Transfers
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 30/09/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub chkStoresOnly_Click()

Dim lngStoreNo   As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".chkStoresOnly_Click"

    Screen.MousePointer = vbHourglass
    If chkStoresOnly.Value = 0 Then 'display all Supplier details
        lblSupplierlbl.Caption = "Supplier No"
        lblSupplierAddlbl.Caption = "Supplier"
        fraAddress.Caption = "Supplier Details"
'        cmbSupplier.Visible = True
        cmbStore.Visible = False
        lblContactName.Visible = True
        lblContactNamelbl.Visible = True
        If cmbSupplier.Visible Then Call cmbSupplier.SetFocus
        lblMCPMin.Visible = True
        lblMCPMinlbl.Visible = True
        lblMCPType.Visible = True
        lblLeadDays.Visible = True
        lblLeadDayslbl.Visible = True
    Else
        lblSupplierlbl.Caption = "Store No"
        lblSupplierAddlbl.Caption = "Store"
        fraAddress.Caption = "Store Details"
        cmbSupplier.Visible = False
        If fraSelection.Visible = True Then cmbStore.Visible = True
        lblContactName.Visible = False
        lblContactNamelbl.Visible = False
        lblMCPMin.Visible = False
        lblMCPMinlbl.Visible = False
        lblMCPType.Visible = False
        lblLeadDays.Visible = False
        lblLeadDayslbl.Visible = False
        'if combo for Stores is empty, then retrieve list and populate combo
        If cmbStore.ListCount = 0 Then
            Set mcolStores = goRoot.CreateBoColFromClassID(CLASSID_STORE, goSession)
            For lngStoreNo = 1 To mcolStores.Count Step 1
                If mcolStores(CLng(lngStoreNo)).StoreNumber <> goSession.CurrentEnterprise.IEnterprise_StoreNumber Then
                    Call cmbStore.AddItem(mcolStores(CLng(lngStoreNo)).StoreNumber & vbTab & mcolStores(CLng(lngStoreNo)).AddressLine1 & vbTab & Val(mcolStores(CLng(lngStoreNo)).StoreNumber))
                    cmbStore.ItemData(cmbStore.NewIndex) = lngStoreNo 'point reference to locate Store in Collection
                End If
            Next lngStoreNo
        End If
        If cmbStore.Visible = True Then Call cmbStore.SetFocus
    End If
    'clear out address and other selected values
    sprdAddress.Col = 1
    sprdAddress.Row = -1
    sprdAddress.Text = ""
    dtxtDueDate.Text = ""
    lblContactName.Caption = ""
    
    Screen.MousePointer = vbNormal
    
End Sub 'chkStoresOnly_Click

Private Sub cmbDispatchType_GotFocus()

    cmbDispatchType.ListDown = True

End Sub

Private Sub cmbDispatchType_KeyDown(KeyCode As Integer, Shift As Integer)

    Call DebugMsg(MODULE_NAME, "cmbDT_KD", endlDebug, KeyCode & "-" & cmbDispatchType.ListDown)
    If KeyCode = vbKeyDown And cmbDispatchType.ListDown = False Then
        cmbDispatchType.ListDown = True
    End If
    If (KeyCode = vbKeyReturn) And (cmbDispatchType.ListDown = True) Then
        Call cmbDispatchType_KeyPress(13)
    End If

End Sub

Private Sub cmbDispatchType_KeyPress(KeyAscii As Integer)

Dim lngReferenceStatus As Long
Dim strDispString      As String

    If KeyAscii = vbKeyReturn Then
        If cmbDispatchType.ListIndex = -1 Then
            cmbDispatchType.ListDown = True
        Else
            cmbDispatchType.Row = cmbDispatchType.ListIndex
            strDispString = Mid$(cmbDispatchType.List, InStr(cmbDispatchType.List, vbTab) + 1)
            lngReferenceStatus = Val(Mid$(strDispString, InStr(strDispString, vbTab) + 1))
            If lngReferenceStatus = 0 Then
                lblReferencelbl.Visible = False
                txtReference.Visible = False
                sprdOrder.Visible = True
                sprdTotal.Visible = True
                cmdDelLine.Visible = True
                Call sprdOrder.SetFocus
                sprdOrder.EditMode = True
            Else
                lblReferencelbl.Visible = True
                txtReference.Visible = True
                Call txtReference.SetFocus
            End If
        End If
    End If

End Sub

Private Sub cmbStore_GotFocus()

    cmbStore.ListDown = True
    cmbStore.Action = ActionClearSearchBuffer

End Sub

Private Sub cmbStore_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDown And cmbStore.ListDown = False Then
        cmbStore.ListDown = True
    End If
    If (KeyCode = vbKeyReturn) And (cmbStore.ListDown = True) Then
        Call cmbStore_KeyPress(vbKeyReturn)
    End If

End Sub

Private Sub cmbStore_KeyPress(KeyAscii As Integer)

Dim cStore As Object
        
    If KeyAscii = vbKeyReturn Then
        'Get store selected from drop down list
        cmbStore.Visible = False
        cmbStore.Row = cmbStore.ListIndex
        lblSuppNo.Caption = cmbStore.ColText
        Call ResetGrid
        If lblSuppNo.Caption = "" Then
            fraAddress.Visible = False
            cmdVoid.Visible = False
            Exit Sub
        End If
        
        fraAddress.Visible = True
        If strActionCode = ACTION_ISTIN Then
            lblOrderDate.Visible = False
            lblOrderDatelbl.Visible = False
            sprdOrder.Col = COL_QTY
            sprdOrder.ColHidden = True
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call SetTotalsWidth
        End If
    
        'Get store details and display
        Set cStore = mcolStores(cmbStore.ItemData)
        lblSuppName.Caption = cStore.AddressLine1
        sprdAddress.Row = 1
        sprdAddress.Text = cStore.AddressLine2
        sprdAddress.Row = 2
        sprdAddress.Text = cStore.AddressLine3
        sprdAddress.Row = 3
        sprdAddress.Text = cStore.AddressLine4
        sprdAddress.Row = 4
        sprdAddress.Text = cStore.postcode
        
        lblContactName.Caption = ""
        lblPhoneNo.Caption = cStore.PhoneNo
        lblFaxNo.Caption = cStore.FaxNo
        lblOrderDate.Caption = DisplayDate(Date, False)
        dtxtDueDate.Text = DisplayDate(DateAdd("d", 1, Date), False)
        dtxtDueDate.Text = DisplayDate(Date, False)
            
        'Clear out any delivery note numbers
        sprdDelNotes.MaxRows = 1
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = -1
        sprdDelNotes.Text = ""
        
        sprdOrder.LeftCol = 1
        Call sprdOrder.SetActiveCell(COL_PARTCODE, ROW_FIRST)
        
        Select Case (strActionCode)
            Case (ACTION_ISTIN):
                lblUserDate.Visible = True
                dtxtDueDate.Visible = True
                sprdDelNotes.SetFocus
            Case (ACTION_ISTOUT): 'select despatch method if any created
                If cmbDispatchType.ListCount > 0 Then
                    lblDispTypelbl.Visible = True
                    cmbDispatchType.Visible = True
                    Call cmbDispatchType.SetFocus
                Else
                    sprdOrder.Visible = True
                    sprdOrder.SetFocus
                    sprdOrder.EditMode = True
                End If
            Case (ACTION_MAINTISTOUT):
                sprdOrder.Visible = True
            Case Else
                sprdOrder.SetFocus
                sprdOrder.EditMode = True
        End Select
    End If
    If KeyAscii = vbKeyEscape Then cmbStore.ListDown = True

End Sub

Private Sub cmbSupplier_GotFocus()
    
    cmbSupplier.ListDown = True
    cmbSupplier.Action = ActionClearSearchBuffer

End Sub

Private Sub cmbSupplier_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyDown And cmbSupplier.ListDown = False Then
        cmbSupplier.ListDown = True
    End If
    If (KeyCode = vbKeyReturn) And (cmbSupplier.ListDown = True) Then
        Call cmbSupplier_KeyPress(vbKeyReturn)
    End If

End Sub

Private Sub cmbSupplier_KeyPress(KeyAscii As Integer)

Dim oPOrder       As Object
Dim lLineNo       As Long
Dim oRow          As Object
Dim blnCustOrders As Boolean

       
    If (KeyAscii = vbKeyReturn) And (cmbSupplier.ListIndex <> -1) Then
        lblOrderID.Caption = ""
        
        'Retrieve selected supplier and display details
        If strActionCode = ACTION_PORDER Then cmdDelete.Visible = False
        
        cmbSupplier.Visible = False
        cmbSupplier.Row = cmbSupplier.ListIndex
        cmbSupplier.Col = 0
        lblSuppNo.Caption = cmbSupplier.ColText
        Call ResetGrid
        If lblSuppNo.Caption = "" Then
            Call ClearSupplier
            Exit Sub
        End If
            
        'display Progress Bar
        ucpbProgress.Visible = True
        ucpbProgress.Value = 0
        ucpbProgress.Max = 1
        ucpbProgress.Title = "Retrieving Supplier Details"
        ucpbProgress.Caption1 = "Retrieving Supplier"
        DoEvents
        
        Call DisplaySupplier(lblSuppNo.Caption, blnCustOrders) 'display supplier and check for Sales Orders
        
        lblOrderDate.Caption = DisplayDate(Date, False)
        
        ucpbProgress.Caption1 = "Checking for Parked Orders"
        Call DebugMsg(MODULE_NAME, "cmbSupplier_closeup", endlDebug, "Started Retrieve-" & Now())
        If strActionCode = ACTION_PORDER Then
            'After displaying supplier - check if there is a Parked Order - if so - display
            Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
            If oPOrder.GetParkedOrder(lblSuppNo.Caption) Then
                'display Progress Bar
                ucpbProgress.Max = oPOrder.Lines.Count + 1
                ucpbProgress.Caption1 = "Retrieving Lines for Parked Order"
                lblOrderID.Caption = oPOrder.Key
                
               ' dtxtDueDate.Text = DisplayDate(Date, False)
                'oPOrder.DueDate = GetDate(Date)
                'dtxtDueDate.Text = DisplayDate(oPOrder.DueDate, False)
                
                Call DebugMsg(MODULE_NAME, "cmbSupplier_closeup", endlDebug, "Displaying Lines-" & Now())
                Call DisplayOrderLines(oPOrder, False, False, Nothing)
                sprdOrder.Row = sprdOrder.MaxRows
                cmdDelete.Visible = True
            End If
            Call DebugMsg(MODULE_NAME, "cmbSupplier_closeup", endlDebug, "Finished Retrieve-" & Now())
            Set oPOrder = Nothing
            
            If blnCustOrders Then Call cmdViewCOrders_Click
            
        End If
        ucpbProgress.Visible = False
        
        If sprdDelNotes.Visible = True Then
            sprdDelNotes.MaxRows = 1
            sprdDelNotes.Col = 1
            sprdDelNotes.Row = -1
            sprdDelNotes.Text = ""
        End If
        
        sprdOrder.LeftCol = 1
        Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.MaxRows)
        lblUserDate.Visible = True
        dtxtDueDate.Visible = True
        Select Case (strActionCode)
            Case (ACTION_PORDER): If dtxtDueDate.Enabled = True Then dtxtDueDate.SetFocus
            Case (ACTION_RETURN): lblDelNoteNoLbl.Visible = True
                                  sprdDelNotes.Visible = True
                                  lblReferencelbl.Visible = True
                                  txtReference.Visible = True
                                  If sprdDelNotes.Enabled = True Then sprdDelNotes.SetFocus
        End Select
    '    cmdClose.Visible = False
    '    cmdNoAction.Visible = True
    End If
    If KeyAscii = vbKeyEscape Then cmbSupplier.ListDown = True

End Sub

Private Sub cmdSaveHO_Click()

    mblnSubmitHO = True
    Call cmdSave_Click
    mblnSubmitHO = False

End Sub

Private Sub cmdViewCOrders_Click()

Dim lngPosCount   As Long
Dim lngLineNo     As Long
Dim strPartCode   As String
Dim strOrderNo    As String
Dim strOrderLine  As String
Dim dblQty        As Double
Dim lngOrigLineNo As Long

    'reset all selected Customer Order lines
    ReDim alDelLine(0)
    Load frmDetails
    If frmDetails.LoadCustomerOrders(lblSuppNo.Caption, lblSuppName.Caption) = False Then
        Unload frmDetails
        Exit Sub
    End If
    
    lngPosCount = 1
    'Check if any items on Order are part of Customer Orders
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_CUSTONO
        If sprdOrder.Text <> "" Then
            lngPosCount = lngPosCount + 1
            ReDim Preserve alDelLine(lngPosCount)
            alDelLine(lngPosCount - 1) = lngLineNo
            sprdOrder.Row = lngLineNo
            sprdOrder.Col = COL_PARTCODE
            strPartCode = sprdOrder.Text
            sprdOrder.Col = COL_CUSTONO
            strOrderNo = sprdOrder.Text
            sprdOrder.Col = COL_ORIGLINENO
            strOrderLine = sprdOrder.Text
            Call frmDetails.AddSelectedLine(strPartCode, strOrderNo, strOrderLine, lngLineNo)
        End If
    Next lngLineNo
    
    Call frmDetails.CountSelectedItems
    'show user the form and wait for user
    frmDetails.Show 1
    
    If frmDetails.SelectedLinesCount <> -1 Then
        'Get any selected values and update the order form
        For lngLineNo = 1 To frmDetails.SelectedLinesCount Step 1
            Call frmDetails.GetSelectedLine(lngLineNo, strPartCode, dblQty, strOrderNo, strOrderLine, lngOrigLineNo)
            If lngOrigLineNo = 0 Then
                sprdOrder.Row = sprdOrder.MaxRows
                Call GetItem(strPartCode, False, True)
                sprdOrder.Col = COL_PARTCODE
                sprdOrder.Lock = True
                sprdOrder.Col = COL_CUSTONO
                sprdOrder.Text = strOrderNo
                sprdOrder.Col = COL_ORIGLINENO
                sprdOrder.Text = strOrderLine
            Else
                'if already entered then move to line and confirm Quantity
                sprdOrder.Row = lngOrigLineNo
                'Remove line from delete list
                For lngPosCount = 1 To UBound(alDelLine) Step 1
                    If alDelLine(lngPosCount - 1) = lngOrigLineNo Then alDelLine(lngPosCount - 1) = 0
                Next lngPosCount
            End If
            sprdOrder.Col = COL_QTY
            sprdOrder.Text = dblQty
            sprdOrder.Lock = True
            sprdOrder.BackColor = RGB_LTGREY
            If lngOrigLineNo = 0 Then Call sprdOrder_EditMode(COL_QTY, sprdOrder.Row, 0, True) 'add a new line
            Call DebugMsg(MODULE_NAME, "sprdOrder_Keydown", endlDebug, lngLineNo & " " & strPartCode)
        Next lngLineNo
        'Remove any Customer Order Lines that should no longer appear on the Original Order
        For lngPosCount = UBound(alDelLine) To 1 Step -1
            If alDelLine(lngPosCount - 1) > 0 Then
                Call sprdOrder.DeleteRows(alDelLine(lngPosCount - 1), 1)
                sprdOrder.MaxRows = sprdOrder.MaxRows - 1
            End If
        Next lngPosCount
        
        'ensure that Item Numbers are in sequence
        For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
            sprdOrder.Row = lngLineNo
            sprdOrder.Col = COL_ITEMNO
            sprdOrder.Text = lngLineNo - 1
        Next lngLineNo
    
    End If 'user pressed process button
    Unload frmDetails
    Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.MaxRows)
    Call UpdateTotals
    DoEvents
    Me.SetFocus
    If sprdOrder.Visible = True Then sprdOrder.SetFocus

End Sub

Private Sub cmdClose_Click()

    Unload Me 'force checking of anything to save
    
End Sub

Private Sub cmdClose_GotFocus()
    
    cmdClose.FontBold = True

End Sub

Private Sub cmdClose_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then SendKeys ("+{tab}")

End Sub

Private Sub cmdClose_LostFocus()
    
    cmdClose.FontBold = False

End Sub

Private Sub ConsolidateItems()

Dim lngRowNo     As Long
Dim strOldPart   As String
Dim dblQuantity  As Double
Dim strCustONo   As String
Dim strCustName  As String
Dim colCustNames As New Collection
Dim oCOrder      As Object

    'Preserve existing purchase order for breakdown for consolidated lines
    Load frmViewCons
    frmViewCons.sprdCons.MaxRows = sprdOrder.MaxRows - 2
    For lngRowNo = ROW_FIRST To sprdOrder.MaxRows - 1 Step 1
        sprdOrder.Row = lngRowNo
        frmViewCons.sprdCons.Row = lngRowNo - 1
        frmViewCons.sprdCons.Col = COL_CONS_ITEMNO
        frmViewCons.sprdCons.Text = frmViewCons.sprdCons.Row
        frmViewCons.sprdCons.Col = COL_CONS_PARTCODE
        sprdOrder.Col = COL_PARTCODE
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_DESC
        sprdOrder.Col = COL_DESC
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_SIZE
        sprdOrder.Col = COL_SIZE
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_PORDQTY
        sprdOrder.Col = COL_QTY
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_CORDQTY
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_MANUCODE
        sprdOrder.Col = COL_MANUCODE
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_PRICE
        sprdOrder.Col = COL_SELLPRICE
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_COST
        sprdOrder.Col = COL_COST
        frmViewCons.sprdCons.Text = sprdOrder.Text
        frmViewCons.sprdCons.Col = COL_CONS_SELLUNITS
        sprdOrder.Col = COL_QTYSELLIN
        frmViewCons.sprdCons.Text = sprdOrder.Text
        
        frmViewCons.sprdCons.Col = COL_CONS_CORDNO
        sprdOrder.Col = COL_CUSTONO
        frmViewCons.sprdCons.Text = sprdOrder.Text
        
        'Check if item for stock
        If frmViewCons.sprdCons.Text = "" Then
            frmViewCons.sprdCons.BackColor = RGB_GREY
        Else 'If not for stock then show Customer Name
            strCustName = ""
            On Error Resume Next
            'attempt to get Customer Name from list of previously retrieved orders
            strCustName = colCustNames(frmViewCons.sprdCons.Text)
            If Err.Number <> 0 Then 'if not found then get from database
                Set oCOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
                Call oCOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, frmViewCons.sprdCons.Text)
                Call oCOrder.IBo_AddLoadField(FID_SOHEADER_Name)
                Call oCOrder.IBo_Loadmatches
                strCustName = oCOrder.Name
                Set oCOrder = Nothing
                'Add retrieved name to list, so that it does not have to be retrieved again
                Call colCustNames.Add(strCustName, frmViewCons.sprdCons.Text)
            End If
            Call Err.Clear
            'Display Customer name for Order
            frmViewCons.sprdCons.Col = COL_CONS_CORDNAME
            frmViewCons.sprdCons.Text = strCustName
        End If
        
        frmViewCons.sprdCons.Col = COL_CONS_CORDLINENO
        sprdOrder.Col = COL_ORIGLINENO
        frmViewCons.sprdCons.Text = sprdOrder.Text
    Next lngRowNo
    
    'Sort items on purchase order so that they are grouped by Part Code
    sprdOrder.SortKey(1) = COL_PARTCODE
    sprdOrder.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdOrder.Sort(COL_ITEMNO, ROW_FIRST, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, SortByRow)
    'Step through list and add all items with the same partcode
    For lngRowNo = sprdOrder.MaxRows - 1 To ROW_FIRST Step -1
        sprdOrder.Row = lngRowNo
        sprdOrder.Col = COL_PARTCODE
        If strOldPart = sprdOrder.Text Then 'Item matches so add lines together
            sprdOrder.Row = sprdOrder.Row + 1 'move to next line (should be same Part code)
            sprdOrder.Col = COL_QTY
            dblQuantity = Val(sprdOrder.Text)
            sprdOrder.Col = COL_CUSTONO
            strCustONo = sprdOrder.Text
            'move to current line and addin quantity
            sprdOrder.Row = lngRowNo
            sprdOrder.Col = COL_QTY
            sprdOrder.Text = Val(sprdOrder.Text) + dblQuantity
            'Check if either is part of a Customer Order - if so then override as CONSOLIDATED
            sprdOrder.Col = COL_CUSTONO
            If sprdOrder.Text <> "" Or strCustONo <> "" Then
                sprdOrder.Text = "CONSOL"
                sprdOrder.Col = COL_ORIGLINENO
                sprdOrder.Text = ""
            End If
            'Delete added in line so that overall quantity matches
            Call sprdOrder.DeleteRows(sprdOrder.Row + 1, 1)
            sprdOrder.MaxRows = sprdOrder.MaxRows - 1
        End If
        sprdOrder.Row = lngRowNo
        sprdOrder.Col = COL_PARTCODE
        strOldPart = sprdOrder.Text
    Next lngRowNo
    'Step through consolidated order and update consolidations with references
    For lngRowNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngRowNo
        'Set Item number in order
        sprdOrder.Col = COL_ITEMNO
        sprdOrder.Text = lngRowNo - 1
        'get total item qty for storage in consolidated view
        sprdOrder.Col = COL_QTY
        dblQuantity = Val(sprdOrder.Text)
        sprdOrder.Col = COL_PARTCODE
        'Update consolidations to reference originating purchase order lines
        Call frmViewCons.SetConsolidateLine(sprdOrder.Text, lngRowNo - 1, dblQuantity)
    Next lngRowNo
    
End Sub

Private Function RequiresConsolidation() As Boolean

Dim lngRowNo   As Long
Dim strOldPart As String

    RequiresConsolidation = False
    
    'Sort items by Partcode to group together
    sprdOrder.SortKey(1) = COL_PARTCODE
    sprdOrder.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdOrder.Sort(COL_ITEMNO, ROW_FIRST, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, SortByRow)
    
    'Step through list and check if Part code is repeated
    For lngRowNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngRowNo
        sprdOrder.Col = COL_PARTCODE
        If strOldPart = sprdOrder.Text Then 'Item matches so add lines together
            RequiresConsolidation = True
            Call ConsolidateItems
            Exit For
        End If
        strOldPart = sprdOrder.Text
    Next lngRowNo
    'Reset items to display by Item No
    sprdOrder.SortKey(1) = COL_ITEMNO
    sprdOrder.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdOrder.Sort(COL_ITEMNO, ROW_FIRST, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, SortByRow)

End Function


Private Sub cmdDelete_Click()
                        
Dim oOrder      As Object
Dim lngLineNo   As Long
Dim strOrders   As String
Dim strCancel   As String
Dim dblOrderQty As Double
    
    cmdVoid.Visible = False
    Select Case (strActionCode)
        Case (ACTION_PORDER):
                If MsgBoxEx("Confirm delete Parked Order for '" & lblSuppNo.Caption & "-" & lblSuppName.Caption & "'", vbYesNo + vbQuestion, "Delete Purchase Order", , , , , RGBMSGBox_PromptColour) = vbNo Then Exit Sub
                Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
                oOrder.Key = Val(lblOrderID.Caption)
                Call oOrder.IBo_Load
                Call oOrder.IBo_Delete
                Call ClearSupplier
                Call ResetGrid
                Call cmdVoid_Click
        Case (ACTION_MAINTPO):
                strOrders = ""
                strCancel = ""
                For lngLineNo = ROW_FIRST To sprdOrder.MaxRows + 1 Step 1
                    sprdOrder.Col = COL_CUSTONO
                    sprdOrder.Row = lngLineNo
                    'check if item was created via a customer order
                    If (sprdOrder.Text <> "") And (sprdOrder.Text <> "000000") Then
                        'check no items have been delivered
                        sprdOrder.Col = COL_QTY
                        dblOrderQty = Val(sprdOrder.Text)
                        sprdOrder.Col = COL_QTYOS
                        If Val(sprdOrder.Text) > 0 Then  'if still outstanding quantity
                            If dblOrderQty = Val(sprdOrder.Text) Then
                                sprdOrder.Col = COL_PARTCODE
                                strOrders = strOrders & vbCrLf & vbTab & sprdOrder.Text & "-"
                                sprdOrder.Col = COL_DESC
                                strOrders = strOrders & sprdOrder.Text
                            End If
                            sprdOrder.Col = COL_QTYOS
                            If dblOrderQty <> Val(sprdOrder.Text) Then
                                sprdOrder.Col = COL_PARTCODE
                                strCancel = strCancel & vbCrLf & vbTab & sprdOrder.Text & "-"
                                sprdOrder.Col = COL_DESC
                                strCancel = strCancel & sprdOrder.Text
                            End If
                        End If
                    End If
                Next lngLineNo
                If strOrders <> "" Then strOrders = vbCrLf & "The following customer order items on the Purchase Order will need re-ordering" & vbCrLf & strOrders
                If strCancel <> "" Then strCancel = vbCrLf & "The following customer order items on the Purchase Order have been part delivered and will be cancelled" & vbCrLf & strCancel
                If MsgBoxEx("Confirm delete Purchase Order - '" & lblOrderNo.Caption & "'" & vbCrLf & strOrders & vbCrLf & strCancel, vbYesNo + vbQuestion, "Confirm delete Purchase Order - '" & lblOrderNo.Caption & "'", , , , , RGBMSGBox_PromptColour) = vbNo Then Exit Sub
                Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
                oOrder.Key = Val(lblOrderID.Caption)
                Call oOrder.IBo_Load
                Call oOrder.CancelOrder
                Call ClearSupplier
                Call ResetGrid
                Call cmdVoid_Click
        Case (ACTION_MAINTISTOUT):
                If MsgBoxEx("Confirm delete IST Out - '" & lblOrderNo.Caption & "'", vbYesNoCancel + vbQuestion, "Delete IST Out", , , , , RGBMSGBox_PromptColour) = vbNo Then Exit Sub
                Set oOrder = goDatabase.CreateBusinessObject(CLASSID_ISTOUTHEADER)
                Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTOUTHEADER_DocNo, lblOrderNo.Caption)
                If oOrder.IBo_Load = True Then
                    Call oOrder.Cancel
                    Call ClearSupplier
                    Call ResetGrid
                    Call cmdVoid_Click
                End If
    End Select
    
                        
End Sub

Private Sub cmdDelete_GotFocus()
    
    cmdDelete.FontBold = True

End Sub

Private Sub cmdDelete_LostFocus()

    cmdDelete.FontBold = False

End Sub

Private Sub cmdDelLine_Click()

Dim strPartDesc As String
Dim lngRowNo    As Long
Dim strOrderMsg As String

    sprdOrder.SetFocus
    sprdOrder.Row = sprdOrder.SelBlockRow
    sprdOrder.Col = COL_PARTCODE
    If (sprdOrder.Text = "") And (sprdOrder.BackColor = RGB_WHITE) Then Exit Sub
    strPartDesc = "'" & sprdOrder.Text & "'"
    sprdOrder.Col = COL_DESC
    strPartDesc = strPartDesc & "-" & sprdOrder.Text
    sprdOrder.Col = COL_ORIGLINENO
    If ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) And (sprdOrder.Text <> "") Then
        If MsgBoxEx("Unable to delete line '" & strPartDesc & "' from Purchase Order." & vbCrLf & "Mark quantity delivered and oustanding as 0", vbYesNo + vbQuestion, "Delete Line", , , , , RGBMSGBox_PromptColour) = vbNo Then Exit Sub
        sprdOrder.Col = COL_QTYDEL
        sprdOrder.Text = 0
        sprdOrder.Col = COL_QTYBORD
        sprdOrder.Text = 0
    Else
        sprdOrder.Col = COL_CUSTONO
        If sprdOrder.Text <> "" Then
            strOrderMsg = vbCrLf & vbTab & "Item from Customer Order - '" & sprdOrder.Text & "'" & vbCrLf
        End If
        If MsgBoxEx("Confirm delete line" & vbCrLf & strOrderMsg & vbTab & strPartDesc, vbYesNo + vbQuestion, "Delete Line", , , , , RGBMSGBox_PromptColour) = vbNo Then Exit Sub
        'Call delete row
        Call sprdOrder.DeleteRows(sprdOrder.Row, 1)
        If sprdOrder.Row < sprdOrder.MaxRows Then sprdOrder.MaxRows = sprdOrder.MaxRows - 1
        
        'ensure that Item Numbers are in sequence
        For lngRowNo = ROW_FIRST To sprdOrder.MaxRows Step 1
            sprdOrder.Row = lngRowNo
            sprdOrder.Col = COL_ITEMNO
            sprdOrder.Text = lngRowNo - 1
        Next lngRowNo
    End If
    
    Call UpdateTotals
    
    sprdOrder.Row = sprdOrder.MaxRows
    Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.MaxRows)
    
End Sub

Private Sub cmdDelLine_GotFocus()
    
    cmdDelLine.FontBold = True

End Sub

Private Sub cmdDelLine_LostFocus()
    
    cmdDelLine.FontBold = False

End Sub

Private Sub cmdRePrint_Click()

Dim oOrder As Object

    If cmdRePrint.Visible = False Then Exit Sub
    Select Case (strActionCode)
        Case (ACTION_MAINTPO):
                sprdOrder.Col = COL_QTYOS
                sprdOrder.ColHidden = True
                Call PrintPurchaseOrder(True, False)
                sprdOrder.Col = COL_QTYOS
                sprdOrder.ColHidden = False
                Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
                oOrder.Key = Val(lblOrderID.Caption)
                Call oOrder.IBo_Load
                Call oOrder.RecordRePrint
                Set oOrder = Nothing
        Case (ACTION_MAINTISTOUT):
                Call PrintISTOutNote(lblOrderNo.Caption, GetDate(lblOrderDate.Caption), strMaintUser)
        Case (ACTION_MAINTRETURN):
                Call PrintSupplierReturn(lblRetRequestID.Caption, lblOrderID.Caption & "/" & lblReleaseNo.Caption, GetDate(lblOrderDate.Caption), mstrNotes, strMaintUser, lblMCPMin.Caption, txtReference.Text, moSuppRetNote.ShortageNote, True)  'oRetNote.OriginalDelNoteDRL)
        Case (ACTION_MAINTDN):
                Call PrintDeliveryNote(moDelNote.DocNo, moDelNote.ConsignmentRef, moDelNote.DelNoteDate)
    End Select

End Sub

Private Sub cmdRePrint_GotFocus()
    
    cmdRePrint.FontBold = True

End Sub

Private Sub cmdRePrint_LostFocus()
    
    cmdRePrint.FontBold = False

End Sub

Private Sub cmdSave_Click()

Dim lngLineNo    As Long
Dim blnPrint     As Boolean
Dim strSubmitMsg As String
    
    If cmdSave.Visible = False Then Exit Sub
    
    If mblnSubmitHO = True Then
        strSubmitMsg = " and submit to H/O?" & vbCrLf & vbCrLf & "WARNING : Once sent to H/O, this cannot be maintained!"
    Else
        strSubmitMsg = "?"
    End If
    
    If (strActionCode = ACTION_MAINTPO) And (mblnDueEdited = False) Then 'nothing to save so just tidy up
        Call DebugMsg(MODULE_NAME, "cmdSave_click", endlDebug, "Clear unedited Purchase Order " & lblOrderNo.Caption)
        Call ClearSupplier
        Call ResetGrid
        cmdVoid.Visible = False 'switch off user prompt
        Call cmdVoid_Click
        Exit Sub
    End If
    
    'Check that at least first line is filled in
    If (sprdOrder.MaxRows = 2) Then
        sprdOrder.Col = COL_QTY
        sprdOrder.Row = sprdOrder.MaxRows
        If Val(sprdOrder.Text) = 0 Then
            Call MsgBoxEx("Invalid Line captured" & vbCrLf & "Rectify entry and resubmit", vbOKOnly, "Submit Order Validation Failed", , , , , RGBMsgBox_WarnColour)
            sprdOrder.SetFocus
            Exit Sub
        End If
    End If
    
    'Check that all lines are displayed in White and are acceptable
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNo
        'Check line has not been flagged as invalid Part Code (Description in RED)
        sprdOrder.Col = COL_DESC
        If (sprdOrder.BackColor) = RGB_RED Then
            Call MsgBoxEx("Invalid item captured" & vbCrLf & "Rectify entry and resubmit", vbOKOnly, "Submit Order Validation Failed", , , , , RGBMsgBox_WarnColour)
            sprdOrder.SetFocus
            Exit Sub
        End If
        'Check line has valid quantity (Order Quantity in RED)
        sprdOrder.Col = COL_QTY
        If (sprdOrder.BackColor) = RGB_RED Then
            Call MsgBoxEx("Invalid quantity captured" & vbCrLf & "Rectify entry and resubmit", vbOKOnly, "Submit Order Validation Failed", , , , , RGBMsgBox_WarnColour)
            sprdOrder.SetFocus
            Exit Sub
        End If
        'If a Return the check each Return Quantity is greater than 0 and has a valid retrun code
        If (strActionCode = ACTION_RETURN) Then
            sprdOrder.Col = COL_PARTCODE
            If (sprdOrder.Text <> "") Then 'check only if item selected
                sprdOrder.Col = COL_RETCODE
                If (sprdOrder.Text = "") Then 'no return reason
                    Call MsgBoxEx("No return code/reason captured for line" & vbCrLf & "Rectify entry and resubmit", vbOKOnly, "Submit Supplier Return Validation Failed", , , , , RGBMsgBox_WarnColour)
                    Call sprdOrder.SetActiveCell(COL_RETCODE, lngLineNo)
                    sprdOrder.SetFocus
                    Exit Sub
                End If
            End If 'Valid Partcode entered for line
        End If 'Supplier Return reason code check
        'if a Delivery Note, then check if line returned then check Return Qty and Code
        sprdOrder.Col = COL_QTYRET
        If ((strActionCode = ACTION_DELNOTE) And (sprdOrder.ColHidden = False)) Then 'check that each return item has a reason code
            sprdOrder.Col = COL_PARTCODE
            If (sprdOrder.Text <> "") Then 'check only if item selected
                sprdOrder.Col = COL_QTYRET
                If (Val(sprdOrder.Text) > 0) Then   'Only check return qty if entered
                    sprdOrder.Col = COL_RETCODE
                    If (sprdOrder.Text = "") Then   'no return reason
                        Call MsgBoxEx("No return code/reason captured for line" & vbCrLf & "Rectify entry and resubmit", vbOKOnly, "Submit Supplier Return Validation Failed", , , , , RGBMsgBox_WarnColour)
                        Call sprdOrder.SetActiveCell(COL_RETCODE, lngLineNo)
                        sprdOrder.SetFocus
                        Exit Sub
                    End If 'Return Code not entered
                End If 'Return quantity entered
            End If 'Valid Partcode entered for line
        End If 'Supplier Return reason code check for Delivery Notes
    Next lngLineNo
                
    If MsgBoxEx(" Confirm " & mstrFunction & " completed, Save Now" & strSubmitMsg, vbYesNo + vbQuestion, "Process " & mstrFunction, , , , , RGBMSGBox_PromptColour) = vbYes Then
        Select Case (strActionCode)
            Case (ACTION_DELNOTE):
                    If SaveDelNote(mblnSubmitHO) = False Then Exit Sub
            Case (ACTION_RETURN):
                    If SaveReturn(mblnSubmitHO) = False Then Exit Sub
            Case (ACTION_PORDER):
                    blnPrint = False
                    If (lblEDIFlag.Visible = False) Then 'if not EDI then determine if print
                        Select Case (mlngPOAutoPrint)
                            Case (PRINT_NONE): blnPrint = False
                            Case (PRINT_PROMPT):
                                               If MsgBoxEx("Purchase Order completed - Print Now", vbYesNoCancel + vbQuestion, "Process Purchase Order", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
                            Case Else: blnPrint = True
                        End Select
                    End If
                    If SaveOrder(False, blnPrint) = False Then
                        Call MsgBoxEx("WARNING - Error occurred on Saving Purchase Order", vbCritical, "Save failed", , , , , RGBMsgBox_WarnColour)
                        Exit Sub
                    End If
                    If blnPrint = True Then Call PrintPurchaseOrder(False, False)
            Case (ACTION_ISTOUT):
                    If SaveISTOut = False Then Exit Sub
            Case (ACTION_ISTIN):
                    If SaveISTIn = False Then Exit Sub
            Case (ACTION_MAINTPO):
                    blnPrint = False
                    Select Case (mlngPOAutoPrint)
                        Case (PRINT_NONE): blnPrint = False
                        Case (PRINT_PROMPT):
                                           If MsgBoxEx("Purchase Order completed - Print Now", vbYesNoCancel + vbQuestion, "Process Purchase Order", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
                        Case Else: blnPrint = True
                    End Select
                    Call UpdateDeliveryDate
            Case (ACTION_MAINTDN):
                    blnPrint = False
                    Select Case (mlngDNAutoPrint)
                        Case (PRINT_NONE): blnPrint = False
                        Case (PRINT_PROMPT):
                                           If MsgBoxEx("Delivery Note completed - Print Now", vbYesNoCancel + vbQuestion, "Process Delivery Note", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
                        Case Else: blnPrint = True
                    End Select
                    Call UpdateDeliveryNote(mblnSubmitHO)
            Case (ACTION_MAINTRETURN):
                    If UpdateSupplierReturn(mblnSubmitHO) = False Then Exit Sub
        End Select
        sprdOrder.LeftCol = 1
        Call ResetGrid
        Call ClearSupplier
        cmbSupplier.Text = ""
        cmbSupplier.ListIndex = -1
        cmbStore.Text = ""
        cmbStore.ListIndex = -1
        cmdVoid.Visible = False 'switch off user prompt
        Call cmdVoid_Click 'force drop down of Supplier/Store list for next order
        lblOrderID.Caption = ""
    End If 'user confirmed Complete and Save prompt

End Sub

Private Sub GetAddresses()

Dim oAddress   As Object

    
    'Get this stores contact details
    Set oAddress = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_StoreNumber)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_StoreName)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_AddressLine1)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_AddressLine2)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_AddressLine3)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_PostCode)
    Call oAddress.IBo_AddLoadField(FID_RETAILEROPTIONS_PhoneNumber)
    Call oAddress.IBo_Load
    
    'Store contact details into local variables
    strPOCompanyName = oAddress.StoreName
    mstrDeliveryTo = oAddress.StoreName & vbCrLf & oAddress.AddressLine1 & vbCrLf & _
                     oAddress.AddressLine2 & vbCrLf & oAddress.AddressLine3 & vbCrLf & _
                      oAddress.postcode
    mstrDelContact = "Phone No : " & oAddress.PhoneNumber

    'Get Head Office contact details
    Set oAddress = goDatabase.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeName)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress1)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress2)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress3)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeAddress4)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficePostCode)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficePhoneNo)
    Call oAddress.IBo_AddLoadField(FID_SYSTEMOPTIONS_HeadOfficeFaxNumber)
    Call oAddress.IBo_Load
    
    'Store contact details into local variables
    mstrInvoiceTo = oAddress.HeadOfficeName & vbCrLf & oAddress.HeadOfficeAddress1 & vbCrLf & _
                    oAddress.HeadOfficeAddress2 & vbCrLf & oAddress.HeadOfficeAddress3 & vbCrLf & _
                    oAddress.HeadOfficeAddress4 & vbCrLf & oAddress.HeadOfficePostCode
    mstrInvContact = "Phone No : " & oAddress.HeadOfficePhoneNo & "     Fax No : " & oAddress.HeadOfficeFaxNumber
                       
    Set oAddress = Nothing

End Sub

Private Sub PrintPurchaseOrder(blnDuplicate As Boolean, ByVal blnParked As Boolean)

Const HEADER_LINES As Long = 12
Const FOOTER_LINES As Long = 2
Const LAST_FOOTER_LINES As Long = 8

Dim lngLineNo    As Long
Dim lngPagePos   As Long
Dim strTempStr   As String
Dim lngMaxRows   As Long
Dim dblEstWeight As Double
Dim lngNoPages   As Long
Dim lngColsFroz  As Long

    If mstrDeliveryTo = "" Then GetAddresses
    
    lngColsFroz = sprdOrder.ColsFrozen
    sprdOrder.ColsFrozen = 0
    
    sprdOrder.Col = COL_SPARE
    sprdOrder.ColHidden = False
    sprdOrder.PrintFooter = GetFooter
    
    ReDim arDispCols(0) 'reset hidden column flag
    Call HidePrintColumn(COL_ITEMNO)
    Call HidePrintColumn(COL_UNITS)
    Call HidePrintColumn(COL_COST)
    Call HidePrintColumn(COL_DISC)
    Call HidePrintColumn(COL_EXCTOTAL)
    Call HidePrintColumn(COL_VATTOTAL)
    Call HidePrintColumn(COL_INCTOTAL)
    Call HidePrintColumn(COL_ONHAND)
    Call HidePrintColumn(COL_FREE)
    Call HidePrintColumn(COL_MINLEVEL)
    Call HidePrintColumn(COL_MAXLEVEL)
    Call HidePrintColumn(COL_LASTSOLD)
    Call HidePrintColumn(COL_VATRATE)
    Call HidePrintColumn(COL_ORIGLINENO)
    Call HidePrintColumn(COL_SELLPRICE)
    Call HidePrintColumn(COL_LASTORDER)
    Call HidePrintColumn(COL_QTYSELLIN)
    Call HidePrintColumn(COL_CUSTONO)
    Call SetLastPrintCol
    
    lngNoPages = Int((sprdOrder.MaxRows - 1) / LINES_PER_PAGE) + 1
    'get estimated weight from Totals row - on line 1 before inserting lines
    sprdOrder.Col = COL_WGHTTOT
    sprdOrder.Row = 1
    dblEstWeight = Val(sprdOrder.Text)
    
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = False
    lngMaxRows = sprdOrder.MaxRows
    'add extra rows to hold Order Header
    sprdOrder.MaxRows = (lngNoPages * (HEADER_LINES + FOOTER_LINES + LINES_PER_PAGE)) + LAST_FOOTER_LINES
    For lngPagePos = 0 To lngNoPages - 1 Step 1
        Call sprdOrder.InsertRows(lngPagePos * (LINES_PER_PAGE + HEADER_LINES + FOOTER_LINES) + 1, HEADER_LINES)
        Call sprdOrder.InsertRows((lngPagePos + 1) * (LINES_PER_PAGE + HEADER_LINES + FOOTER_LINES) - FOOTER_LINES + 1, FOOTER_LINES)
    Next lngPagePos
'    sprdOrder.MaxRows = sprdOrder.MaxRows + 14 'Add 12 rows for header + 2 footer
    
    'Clear out Header Grid Lines
    sprdOrder.Col = 1
    sprdOrder.Col2 = sprdOrder.MaxCols
    sprdOrder.Row = 1
    sprdOrder.Row2 = HEADER_LINES
    sprdOrder.BlockMode = True
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.BlockMode = False
    
    'Create Order Header
    'Put in Logo
    sprdOrder.Row = 1
    sprdOrder.Col = COL_DESC
    sprdOrder.CellType = CellTypeEdit
    If blnParked = True Then
        sprdOrder.Text = "DRAFT PURCHASE ORDER"
    Else
        sprdOrder.Text = "PURCHASE ORDER"
    End If
    sprdOrder.FontSize = 16
    sprdOrder.FontBold = True
    sprdOrder.Col = COL_QTY
    sprdOrder.Text = "Page 1/" & lngNoPages
    sprdOrder.Col = COL_PACKQTY
    'Put in Logo
    sprdOrder.Col = lngLastCol
    sprdOrder.CellType = CellTypePicture
    If mstrLogoFile <> "" Then
        sprdOrder.TypePictPicture = LoadPicture(mstrLogoFile)
        sprdOrder.TypeHAlign = TypeHAlignRight
        sprdOrder.RowHeight(1) = sprdOrder.MaxTextRowHeight(1)
    End If
    'Create Order Header
    sprdOrder.Col = 2
    sprdOrder.Row = 2
    sprdOrder.Text = "TO"
    Call sprdOrder.AddCellSpan(3, 2, 1, 5)
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 2
    sprdOrder.TypeEditMultiLine = True
    sprdOrder.CellType = CellTypeEdit
    sprdOrder.TypeMaxEditLen = 500
    strTempStr = lblSuppName.Caption
    If blnParked = False Then 'if not parking, then display full supplier details
        For lngLineNo = 1 To 4 Step 1
            sprdAddress.Row = lngLineNo
            strTempStr = strTempStr & vbCrLf & sprdAddress.Text
        Next lngLineNo
        sprdOrder.Text = strTempStr
    
        sprdOrder.Row = 7
        sprdOrder.Text = "Phone : " & lblPhoneNo.Caption & "   Fax : " & lblFaxNo.Caption
        
        sprdOrder.Col = COL_QTY
        sprdOrder.Row = 2
        sprdOrder.Text = "PO Number"
        sprdOrder.Row = 3
        sprdOrder.Text = "Release"
        sprdOrder.Row = 4
        sprdOrder.Text = "Order Date"
        sprdOrder.Row = 5
        sprdOrder.Text = "Raised By"
        sprdOrder.Row = 6
        sprdOrder.Text = "ETA"
        sprdOrder.Row = 7
        sprdOrder.Text = "Confirmed"
        
        sprdOrder.Col = COL_PACKQTY
        sprdOrder.Row = 2
        sprdOrder.Text = goSession.CurrentEnterprise.IEnterprise_StoreNumber & "/" & lblOrderNo.Caption
        sprdOrder.Row = 3
        sprdOrder.Text = lblReleaseNo.Caption
        sprdOrder.Row = 4
        sprdOrder.Text = lblOrderDate.Caption
        sprdOrder.Row = 5
        sprdOrder.Text = goSession.UserInitials
        sprdOrder.Row = 6
        sprdOrder.Text = dtxtDueDate.Text & Format(GetDate(dtxtDueDate.Text), " DDD")
        sprdOrder.Row = 7
        sprdOrder.Text = ""
    
        sprdOrder.Row = 8
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Text = "DELIVER TO"
        sprdOrder.RowHeight(9) = 55
        sprdOrder.Row = 9
        sprdOrder.Col = COL_DESC
        sprdOrder.CellType = CellTypeEdit
        sprdOrder.TypeEditMultiLine = True
        sprdOrder.Text = mstrDeliveryTo
        sprdOrder.Row = 10
        sprdOrder.Text = mstrDelContact
        
        sprdOrder.Col = COL_QTY
        sprdOrder.Row = 8
        sprdOrder.Text = "INVOICE TO"
        sprdOrder.Col = COL_SPARE
        sprdOrder.Row = 9
        Call sprdOrder.AddCellSpan(COL_SPARE, 9, (lngLastCol - COL_SPARE) + 1, 1)
        sprdOrder.CellType = CellTypeEdit
        sprdOrder.TypeEditMultiLine = True
        sprdOrder.Text = mstrInvoiceTo
        sprdOrder.Row = 10
        sprdOrder.Text = mstrInvContact
    Else 'print summary for Parked Order
        sprdOrder.Text = strTempStr
        sprdOrder.Col = COL_QTY
        sprdOrder.Row = 2
        sprdOrder.Text = "Date"
        sprdOrder.Row = 3
        sprdOrder.Text = "Raised By"
        
        sprdOrder.Col = COL_PACKQTY
        sprdOrder.Row = 2
        sprdOrder.Text = lblOrderDate.Caption
        sprdOrder.Row = 3
        'hide extra rows not required by Parked Orders
        For lngLineNo = 4 To 10 Step 1
            sprdOrder.Row = lngLineNo
            sprdOrder.RowHidden = True
        Next lngLineNo
    End If 'print headers for Parked or Raised Purchase Order
    
    'Draw boxes around Order Addresses(3 Off) and Summary
    Call sprdOrder.SetCellBorder(1, 1, sprdOrder.MaxCols, 11, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(2, 1, sprdOrder.MaxCols, 3, 16, 192, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(2, 2, 4, 7, 16, 192, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(5, 2, sprdOrder.MaxCols, 7, 16, 192, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(2, 8, 4, 11, 16, 192, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(5, 8, sprdOrder.MaxCols, 11, 16, 192, CellBorderStyleSolid)
    
    'Display Line item column headings
    sprdOrder.Row = 12
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "Part Code"
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "SKU"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = "Description"
    sprdOrder.Col = COL_SIZE
    sprdOrder.Text = "Size"
    sprdOrder.Col = COL_QTY
    sprdOrder.Text = "Qty Singles"
    sprdOrder.Col = COL_UNITS
    sprdOrder.Text = "Units"
    sprdOrder.Col = COL_PACKQTY
    sprdOrder.Text = "Pack Size"
    sprdOrder.Col = COL_SPARE
    sprdOrder.Text = "Int Use"
    sprdOrder.Col = COL_COST
    sprdOrder.Text = "Price"
    sprdOrder.Col = COL_DISC
    sprdOrder.Text = "Disc"
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.Text = "Total Ex"
    sprdOrder.Col = COL_VATTOTAL
    sprdOrder.Text = "VAT"
    sprdOrder.Col = COL_INCTOTAL
    sprdOrder.Text = "Total Inc"
    sprdOrder.Col = COL_ONHAND
    sprdOrder.Text = "On Hand"
    sprdOrder.Col = COL_MINLEVEL
    sprdOrder.Text = "Min Level"
    sprdOrder.Col = COL_MAXLEVEL
    sprdOrder.Text = "Max Level"
    sprdOrder.Col = COL_LASTSOLD
    sprdOrder.Text = "Last Sold"
    sprdOrder.Col = COL_VATRATE
    sprdOrder.Text = "VAT Code"
    sprdOrder.Col = COL_MANUCODE
    sprdOrder.Text = "Manu Code"
    
    'copy page header to any other pages
    If lngNoPages > 0 Then
        For lngPagePos = 1 To lngNoPages - 1 Step 1
'            sprdOrder.MaxRows = sprdOrder.MaxRows + HEADER_LINES
            Call sprdOrder.CopyRange(1, 1, sprdOrder.MaxCols, HEADER_LINES, 1, ((LINES_PER_PAGE + HEADER_LINES + FOOTER_LINES) * lngPagePos) + 1)
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + LINES_PER_PAGE)
            Call sprdOrder.AddCellSpan(COL_PARTCODE, sprdOrder.Row, sprdOrder.MaxCols, 1)
            sprdOrder.Col = COL_PARTCODE
            
            sprdOrder.Text = "Continued on next page"
            sprdOrder.FontBold = True
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + LINES_PER_PAGE) + 1
            sprdOrder.RowPageBreak = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Text = "Page " & (lngPagePos + 1) & "/" & lngNoPages
            For lngLineNo = 1 To HEADER_LINES Step 1
                sprdOrder.RowHeight(lngPagePos * (FOOTER_LINES + HEADER_LINES + LINES_PER_PAGE) + lngLineNo) = sprdOrder.RowHeight(lngLineNo)
            Next lngLineNo
            Call sprdOrder.AddCellSpan(3, (FOOTER_LINES + HEADER_LINES + LINES_PER_PAGE) + 2, 1, 5)
            Call sprdOrder.AddCellSpan(COL_SPARE, (FOOTER_LINES + HEADER_LINES + LINES_PER_PAGE) + 9, (lngLastCol - COL_SPARE) + 1, 1)
        Next lngPagePos
    End If
    
    'Display Order Footer
    sprdOrder.Row = sprdOrder.MaxRows - 9
    sprdOrder.Col = COL_DESC
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.Text = "No Items "
    
    sprdOrder.Col = COL_QTY
    sprdTotal.Row = 2
    sprdTotal.Col = COL_TOTQTY
    sprdOrder.Text = sprdTotal.Text
    
    sprdOrder.Col = COL_PACKQTY
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.Text = "Weight"
    
    sprdOrder.Col = COL_MANUCODE
    sprdOrder.Text = Format(dblEstWeight, "0.000")
    
    sprdOrder.Row = sprdOrder.MaxRows - 2
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "End of Order"
    sprdOrder.FontBold = True
    
    'clear out last line and create block around Transmission status
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 8, sprdOrder.MaxCols, sprdOrder.MaxRows, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(COL_PARTCODE, sprdOrder.MaxRows - 8, sprdOrder.MaxCols, sprdOrder.MaxRows - 1, 16, RGB_BLACK, CellBorderStyleSolid)
    
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = sprdOrder.MaxRows - 8
    sprdOrder.Text = "Comments : "
    sprdOrder.FontBold = True
    sprdOrder.FontSize = 14
    sprdOrder.RowHeight(sprdOrder.Row) = sprdOrder.MaxTextRowHeight(sprdOrder.Row)
    If mstrNotes <> "" Then
        Call sprdOrder.AddCellSpan(COL_PARTCODE, sprdOrder.MaxRows - 7, 3, 2)
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Row = sprdOrder.MaxRows - 7
        sprdOrder.TypeEditMultiLine = True
        sprdOrder.Text = mstrNotes
        sprdOrder.FontBold = True
        sprdOrder.FontSize = 11
        sprdOrder.RowHeight(sprdOrder.Row) = sprdOrder.MaxTextRowHeight(sprdOrder.Row)
    End If
    
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = sprdOrder.MaxRows - 5
    If blnParked = False Then sprdOrder.Text = "Transmitted : "

    'if duplicate copy the add extra line at top of print out to indicate Duplicate
    If blnDuplicate Then
        sprdOrder.MaxRows = sprdOrder.MaxRows + 1 'Add row for message
        Call sprdOrder.InsertRows(2, 1) 'Add after Logo
        Call sprdOrder.AddCellSpan(COL_PARTCODE, 2, sprdOrder.MaxCols, 1)
        sprdOrder.Row = 2
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Text = "DUPLICATE"
        sprdOrder.TypeHAlign = TypeHAlignCenter
        sprdOrder.FontSize = 16
        sprdOrder.RowHeight(2) = sprdOrder.MaxTextCellHeight
    End If
    
    'Perform print functions
    sprdOrder.PrintColHeaders = False
    sprdOrder.PrintRowHeaders = False
    sprdOrder.PrintSmartPrint = False
    sprdOrder.PrintGrid = False
    
    sprdOrder.PrintJobName = "Purchase Order - " & lblOrderNo.Caption
    Call sprdOrder.PrintSheet
    
    'Display msgbox showing the purchase order has been printed
    If blnDuplicate = False And blnParked = False Then
        Call MsgBoxEx("Purchase Order successfully saved" & vbCrLf & _
            mstrDispMsg & "Printing Purchase Order " & _
            lblOrderNo.Caption, vbInformation, "Save complete", , , , , RGBMSGBox_PromptColour)
    End If
       
    If blnDuplicate Then
        Call sprdOrder.DeleteRows(1, 1)
        sprdOrder.MaxRows = sprdOrder.MaxRows - 1 'Add row for message
    End If
    
    For lngPagePos = lngNoPages - 1 To 0 Step -1
        Call sprdOrder.DeleteRows((lngPagePos + 1) * (LINES_PER_PAGE + HEADER_LINES + FOOTER_LINES) - FOOTER_LINES + 1, FOOTER_LINES)
        Call sprdOrder.DeleteRows(lngPagePos * (LINES_PER_PAGE + HEADER_LINES + FOOTER_LINES) + 1, HEADER_LINES)
    Next lngPagePos

    'Remove header
'    Call sprdOrder.DeleteRows(1, 12)
    sprdOrder.MaxRows = lngMaxRows
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    
    'Display column not printed
    Call DisplayPrintCols
    sprdOrder.Col = COL_SPARE
    sprdOrder.ColHidden = True
    sprdOrder.ColsFrozen = lngColsFroz

End Sub 'PrintPurchaseOrder

Private Sub PrintDeliveryNote(strDelNoteNumber As String, strConsignmentNo As String, dteDelNoteDate As Date)

Const HEADER_LINES As Long = 10
Const FOOTER_LINES As Long = 1
Const ROWS_PER_PAGE As Long = 45
Const LAST_FOOTER_LINES As Long = 1

Dim lngLineNo   As Long
Dim strTempStr  As String
Dim lngMaxRows  As Long
Dim lngNoPages  As Long
Dim lngPagePos  As Long
                        
    ReDim arDispCols(0) 'reset hidden column flag
    Call HidePrintColumn(COL_SIZE)
    Call HidePrintColumn(COL_QTYOS)
    Call HidePrintColumn(COL_QTYRET)
    Call HidePrintColumn(COL_RETCODE)
    Call HidePrintColumn(COL_COMMENT)
    Call HidePrintColumn(COL_QTYSHORT)
    Call HidePrintColumn(COL_PACKQTY)
    Call HidePrintColumn(COL_RETCMNT)
    Call SetLastPrintCol
            
    sprdOrder.PrintFooter = GetFooter
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = False
    lngMaxRows = sprdOrder.MaxRows
    
    'Work out number of pages
    lngNoPages = Int((sprdOrder.MaxRows - 1) / ROWS_PER_PAGE) + 1
    
    'Set maxrows to pages * rows per page
    sprdOrder.MaxRows = (lngNoPages * (HEADER_LINES + FOOTER_LINES + ROWS_PER_PAGE)) + LAST_FOOTER_LINES
    
    'Insert blank lines for page headers and footers
    For lngPagePos = 0 To lngNoPages - 1 Step 1
        Call sprdOrder.InsertRows(lngPagePos * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) + 1, HEADER_LINES)
        Call sprdOrder.InsertRows((lngPagePos + 1) * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) - FOOTER_LINES + 1, FOOTER_LINES)
    Next lngPagePos
    
    'Clear out Header Grid Lines
    sprdOrder.Col = 1
    sprdOrder.Col2 = sprdOrder.MaxCols
    sprdOrder.Row = 1
    sprdOrder.Row2 = 10
    sprdOrder.BlockMode = True
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.BlockMode = False
    
    'Create Order Header
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 1
    sprdOrder.Text = "Receipt Details"
    sprdOrder.FontBold = True
    sprdOrder.FontSize = 11
    
    'Create Order Header
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row = 3
    sprdOrder.Text = "Supplier : " & lblSuppNo.Caption & " : " & lblSuppName.Caption
    sprdOrder.Row = 7
    sprdOrder.Text = "Document No :"
    sprdOrder.Row = 9
    sprdOrder.Text = "Supp Del Note No's"
    sprdOrder.Col = COL_QTY
    sprdOrder.Row = 3
    sprdOrder.Text = "Created :"
    sprdOrder.Row = 5
    sprdOrder.Text = "Order No :"
    sprdOrder.Row = 7 'if SOQ then display SOQ Number header
    If Val(lblSOQNumber.Caption) > 0 Then sprdOrder.Text = "SOQ No :"
    sprdOrder.Col = COL_CUSTONO
    sprdOrder.Row = 3
    sprdOrder.Text = "Receipt :"
    sprdOrder.Row = 5
    sprdOrder.Text = "PO Date :"
    sprdOrder.Row = 7 'display Consignment No. Header if selected by user
    If blnAutoConsignDN = False Then sprdOrder.Text = "Cons No :"
    
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 7
    sprdOrder.Text = strDelNoteNumber
    strTempStr = ""
    For lngLineNo = 1 To sprdDelNotes.MaxRows Step 1
        sprdDelNotes.Row = lngLineNo
        strTempStr = strTempStr & sprdDelNotes.Text
        If lngLineNo < sprdDelNotes.MaxRows Then strTempStr = strTempStr & ", "
    Next lngLineNo
    sprdOrder.Row = 9
    sprdOrder.Text = strTempStr
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Row = 3
    sprdOrder.Text = DisplayDate(dteDelNoteDate, False)
    sprdOrder.Row = 5
    sprdOrder.Text = lblOrderNo.Caption & "/" & Me.lblReleaseNo
    sprdOrder.Row = 7 'if SOQ then display SOQ Number
    If Val(lblSOQNumber.Caption) > 0 Then sprdOrder.Text = lblSOQNumber.Caption
    sprdOrder.Col = lngLastCol
    sprdOrder.Row = 3
    sprdOrder.Text = dtxtDueDate.Text
    sprdOrder.Row = 5
    sprdOrder.Text = lblOrderDate.Caption
    sprdOrder.Row = 7 'display Consignment No. if selected by user
    If blnAutoConsignDN = False Then sprdOrder.Text = strConsignmentNo
    
    'Clear boxes from top of delivery note
    Call sprdOrder.SetCellBorder(1, 1, sprdOrder.MaxCols, 9, 15, RGB_GREY, CellBorderStyleBlank)
    
    'Display Order Footer
    sprdOrder.RowHeight(sprdOrder.MaxRows - 3) = 1
    sprdOrder.RowHeight(sprdOrder.MaxRows - 1) = 1
    sprdOrder.Row = sprdOrder.MaxRows - 2
    sprdOrder.Col = COL_DESC
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.Text = "No Items "
    
    sprdTotal.Row = 2
    sprdTotal.Col = COL_TOTQTY
    sprdOrder.Col = COL_QTY
    sprdOrder.Text = sprdTotal.Text
    
    sprdTotal.Col = COL_TOTQTYDEL
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Text = sprdTotal.Text
    
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "End of Del Note"
    sprdOrder.FontBold = True
    
    'Display Line item column headings
    sprdOrder.Row = 10
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "Item No"
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "SKU"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = "Description"
    sprdOrder.Col = COL_SIZE
    sprdOrder.Text = "Size"
    sprdOrder.Col = COL_QTY
    sprdOrder.Text = "Quantity"
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Text = "Received"
    sprdOrder.Col = COL_QTYBORD
    sprdOrder.Text = "To Follow"
    sprdOrder.Col = COL_COMMENT
    sprdOrder.Text = "Comment"
    sprdOrder.Col = COL_UNITS
    sprdOrder.Text = "Units"
    sprdOrder.Col = COL_COST
    sprdOrder.Text = "Price"
    sprdOrder.Col = COL_DISC
    sprdOrder.Text = "Disc"
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.Text = "Total Ex"
    sprdOrder.Col = COL_CUSTONO
    sprdOrder.Text = "Cust O.No"
    sprdOrder.Col = COL_MANUCODE
    sprdOrder.Text = "Manu Code"
    
    'copy page header to any other pages
    If lngNoPages > 0 Then
        sprdOrder.Row = 1
        sprdOrder.Col = COL_QTY
        
        'Add page count to first page
        sprdOrder.Text = "Page 1/" & lngNoPages
        
        'Copy header and footer to each page
        For lngPagePos = 1 To lngNoPages - 1 Step 1
            Call sprdOrder.CopyRange(1, 1, sprdOrder.MaxCols, HEADER_LINES, 1, ((ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) * lngPagePos) + 1)
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE)
            Call sprdOrder.AddCellSpan(COL_PARTCODE, sprdOrder.Row, sprdOrder.MaxCols, 1)
            sprdOrder.Col = COL_PARTCODE
            
            sprdOrder.Text = "Continued on next page"
            sprdOrder.FontBold = True
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 1
            sprdOrder.RowPageBreak = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Text = "Page " & (lngPagePos + 1) & "/" & lngNoPages
            
            For lngLineNo = 1 To HEADER_LINES Step 1
                sprdOrder.RowHeight(lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + lngLineNo) = sprdOrder.RowHeight(lngLineNo)
            Next lngLineNo
            
            Call sprdOrder.AddCellSpan(3, (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 2, 1, 5)
            Call sprdOrder.AddCellSpan(COL_SPARE, (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 9, (lngLastCol - COL_SPARE) + 1, 1)
        Next lngPagePos
    End If
    
    'Perform print functions
    sprdOrder.PrintColHeaders = False
    sprdOrder.PrintRowHeaders = False
    sprdOrder.PrintSmartPrint = False
    sprdOrder.PrintGrid = False
    
    'Clear out last line for footer
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows, sprdOrder.MaxCols, sprdOrder.MaxRows, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows, sprdOrder.MaxCols, sprdOrder.MaxRows, 16, RGB_BLACK, CellBorderStyleSolid)

    sprdOrder.PrintJobName = "Del Note - " & strDelNoteNumber
    Call sprdOrder.Refresh
    Call sprdOrder.PrintSheet
    
    'Remove header and footer from each page
    For lngPagePos = lngNoPages - 1 To 0 Step -1
        Call sprdOrder.DeleteRows((lngPagePos + 1) * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) - FOOTER_LINES + 1, FOOTER_LINES)
        Call sprdOrder.DeleteRows(lngPagePos * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) + 1, HEADER_LINES)
    Next lngPagePos
    
    sprdOrder.MaxRows = lngMaxRows
    
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
   
    If cmdViewCons.Visible = True Then
        Load frmViewCons
        Call frmViewCons.PrintConsolidationSheet(lblOrderNo.Caption, GetDate(lblOrderDate.Caption), mcolConLines, sprdOrder, COL_DESC, COL_QTY, COL_SIZE, lblSuppNo.Caption & " : " & lblSuppName.Caption, strDelNoteNumber, GetDate(dtxtDueDate.Text))
    End If
    
    'Display column not printed
    Call DisplayPrintCols
    Call sprdOrder.Refresh

End Sub

Private Sub PrintISTOutNote(strISTOutNoteNumber As String, dteCreatedDate As Date, strCreatedBy As String)

Const HEADER_LINES As Long = 10
Const FOOTER_LINES As Long = 2
Const ROWS_PER_PAGE As Long = 40
Const LAST_FOOTER_LINES As Long = 8

Dim strTempStr   As String
Dim lngMaxRows   As Long
Dim oUser        As Object
Dim lngNoPages   As Long
Dim lngLineNo    As Long
Dim lngPagePos   As Long
                        
    Call DebugMsg(MODULE_NAME, "PrintISTOutNote", endlDebug, "Printing IST " & strISTOutNoteNumber)
    sprdOrder.PrintFooter = GetFooter
    'get name and address for current store to put at top of document
    Call GetPrintStore
        
    ReDim arDispCols(0) 'reset hidden column flag
    Call HidePrintColumn(COL_QTYOS)
    Call HidePrintColumn(COL_QTYRET)
    Call HidePrintColumn(COL_RETCODE)
    Call HidePrintColumn(COL_PACKQTY)
    Call HidePrintColumn(COL_ONHAND)
    Call HidePrintColumn(COL_FREE)
    Call HidePrintColumn(COL_MANUCODE)
    Call HidePrintColumn(COL_SELLPRICE)
    Call HidePrintColumn(COL_LASTSOLD)
    Call HidePrintColumn(COL_QTYSELLIN)
    Call HidePrintColumn(COL_DELTOTAL)
    sprdOrder.Col = COL_SPARE
    sprdOrder.ColHidden = False
    sprdOrder.Col = COL_SPARE2
    sprdOrder.ColHidden = False
    Call SetLastPrintCol
    
    lngNoPages = Int((sprdOrder.MaxRows - 1) / ROWS_PER_PAGE) + 1
        
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = False
    
    lngMaxRows = sprdOrder.MaxRows
    
    'add extra rows to hold Order Header
    sprdOrder.MaxRows = (lngNoPages * (HEADER_LINES + FOOTER_LINES + ROWS_PER_PAGE)) + LAST_FOOTER_LINES
    For lngPagePos = 0 To lngNoPages - 1 Step 1
        Call sprdOrder.InsertRows(lngPagePos * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) + 1, HEADER_LINES)
        Call sprdOrder.InsertRows((lngPagePos + 1) * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) - FOOTER_LINES + 1, FOOTER_LINES)
    Next lngPagePos
   
    'Clear out Header Grid Lines
    sprdOrder.Col = 1
    sprdOrder.Col2 = sprdOrder.MaxCols
    sprdOrder.Row = 1
    sprdOrder.Row2 = 10
    sprdOrder.BlockMode = True
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.BlockMode = False
    
    'Create Order Header
    sprdOrder.Row = 1
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "INTER STORE TRANSFER"
    sprdOrder.FontSize = 16
    sprdOrder.RowHeight(1) = sprdOrder.MaxTextCellHeight
    
    'Put in Logo
    sprdOrder.Col = COL_SPARE
    sprdOrder.Row = 2
    sprdOrder.Col = lngLastCol
    sprdOrder.CellType = CellTypePicture
    If mstrLogoFile <> "" Then
        sprdOrder.TypePictPicture = LoadPicture(mstrLogoFile)
        sprdOrder.TypeHAlign = TypeHAlignRight
        sprdOrder.RowHeight(2) = sprdOrder.MaxTextCellHeight
    End If
    
    'Create Order Header
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 2
    sprdOrder.Text = "No : " & strISTOutNoteNumber
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row = 3
    sprdOrder.Text = "From : " & goSession.CurrentEnterprise.IEnterprise_StoreNumber & " - " & poPrintStore.AddressLine1
    sprdOrder.Row = 5
    sprdOrder.Text = "To : " & lblSuppNo.Caption & " - " & lblSuppName.Caption
    sprdOrder.Row = 7
    cmbDispatchType.Col = 1
    sprdOrder.Text = "Being dispatched via :" & cmbDispatchType.ColText & " " & txtReference.Text
    
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.RowHeight(4) = 5
    sprdOrder.RowHeight(6) = 5
    sprdOrder.Row = 3
    sprdOrder.Text = "Created :"
    sprdOrder.Row = 5
    sprdOrder.Text = "By :"
    sprdOrder.Row = 7
'    sprdOrder.Text = "Page: " & (lngPagePos - 1) & " of " & lngNoPages
    
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 3
'    sprdOrder.Text =
    sprdOrder.Col = lngLastCol
    sprdOrder.Row = 3
    sprdOrder.Text = DisplayDate(dteCreatedDate, False)
    sprdOrder.Row = 5
    If strCreatedBy = goSession.UserInitials Then
        sprdOrder.Text = goSession.UserFullname
    Else
        Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call oUser.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_Initials, strCreatedBy)
        Call oUser.IBo_AddLoadField(FID_USER_FullName)
        Call oUser.IBo_Loadmatches
        sprdOrder.Text = oUser.FullName
        Set oUser = Nothing
    End If
        
    'Clear boxes from top of delivery note
    Call sprdOrder.SetCellBorder(1, 1, lngLastCol, 9, 15, RGB_GREY, CellBorderStyleBlank)
    'Clear boxes from top of delivery note
    Call sprdOrder.SetCellBorder(1, 1, lngLastCol, 2, 16, RGB_BLACK, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 3, lngLastCol, 9, 16, RGB_BLACK, CellBorderStyleSolid)
    
    'Display Order Footer
    sprdOrder.RowHeight(sprdOrder.MaxRows - 16) = 1
    sprdOrder.RowHeight(sprdOrder.MaxRows - 14) = 1
    sprdOrder.Row = sprdOrder.MaxRows - 15
    sprdOrder.Col = COL_DESC
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.Text = "No Items "
    
    sprdOrder.Col = COL_QTYDEL
    sprdTotal.Row = 2
    sprdTotal.Col = COL_TOTQTYDEL
    sprdOrder.Text = sprdTotal.Text
    
    sprdOrder.Col = 6
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.Text = "Totals "
    
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = sprdOrder.MaxRows - 13
    sprdOrder.Text = "Dispatched"
    sprdOrder.Row = sprdOrder.MaxRows - 10
    sprdOrder.Text = "Collected"
    sprdOrder.Row = sprdOrder.MaxRows - 7
    sprdOrder.Text = "Delivered"
    sprdOrder.Row = sprdOrder.MaxRows - 4
    sprdOrder.Text = "Received"
    
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = sprdOrder.MaxRows - 12
    sprdOrder.Text = "By :"
    sprdOrder.Row = sprdOrder.MaxRows - 11
    sprdOrder.Text = "On :"
    sprdOrder.Row = sprdOrder.MaxRows - 9
    sprdOrder.Text = "By :"
    sprdOrder.Row = sprdOrder.MaxRows - 8
    sprdOrder.Text = "On :"
    sprdOrder.Row = sprdOrder.MaxRows - 6
    sprdOrder.Text = "By :"
    sprdOrder.Row = sprdOrder.MaxRows - 5
    sprdOrder.Text = "On :"
    sprdOrder.Row = sprdOrder.MaxRows - 3
    sprdOrder.Text = "By :"
    sprdOrder.Row = sprdOrder.MaxRows - 2
    sprdOrder.Text = "On :"
    
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.Text = "Please record any reasonably apparent damage or missing packages"
'    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows, lngLastCol, 9, 16, RGB_BLACK, CellBorderStyleSolid)
    sprdOrder.Row = sprdOrder.MaxRows - 13
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.Text = "Comments"
    
    Call sprdOrder.AddCellSpan(COL_QTYDEL, sprdOrder.MaxRows - 12, 10, 5)
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Row = sprdOrder.MaxRows - 12
    sprdOrder.CellType = CellTypeEdit
    sprdOrder.TypeEditMultiLine = True
    sprdOrder.TypeMaxEditLen = 200
    sprdOrder.Text = mstrNotes
    
    'Display Line item column headings
    sprdOrder.Row = 10
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "Item"
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "SKU"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = "Description"
    sprdOrder.Col = COL_SIZE
    sprdOrder.Text = "Size"
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Text = "Quantity"
    sprdOrder.Col = COL_UNITS
    sprdOrder.Text = "Units"
    sprdOrder.Col = COL_SPARE
    sprdOrder.Text = ""
    sprdOrder.Col = COL_COMMENT
    sprdOrder.Text = "Comment"
    sprdOrder.Col = COL_COST
    sprdOrder.Text = "Price"
    sprdOrder.Col = COL_DISC
    sprdOrder.Text = "Disc"
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.Text = "Total Ex"
    sprdOrder.Col = COL_CUSTONO
    sprdOrder.Text = "Cust O.No"
    sprdOrder.Col = COL_MANUCODE
    sprdOrder.Text = "Manu Code"
    sprdOrder.Col = COL_SPARE2
    sprdOrder.Text = "Int Use"
        
    'copy page header to any other pages
    If lngNoPages > 0 Then
        For lngPagePos = 1 To lngNoPages - 1 Step 1
            'sprdOrder.MaxRows = sprdOrder.MaxRows + HEADER_LINES
            Call sprdOrder.CopyRange(1, 1, sprdOrder.MaxCols, HEADER_LINES, 1, ((ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) * lngPagePos) + 1)
            sprdOrder.Col = COL_SPARE
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 1
            sprdOrder.FontSize = 16
            sprdOrder.Text = "DELIVERY NOTE"
            If strActionCode = ACTION_MAINTISTOUT Then
                sprdOrder.Text = sprdOrder.Text & " - Reprint"
            End If
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE)
            Call sprdOrder.AddCellSpan(COL_PARTCODE, sprdOrder.Row, sprdOrder.MaxCols, 1)
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Text = "Continued on next page"
            sprdOrder.FontBold = True
            sprdOrder.Row = lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 1
            sprdOrder.RowPageBreak = True
            sprdOrder.Col = COL_QTYDEL
            For lngLineNo = 1 To HEADER_LINES Step 1
                sprdOrder.RowHeight(lngPagePos * (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + lngLineNo) = sprdOrder.RowHeight(lngLineNo)
            Next lngLineNo
            Call sprdOrder.AddCellSpan(3, (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 2, 1, 5)
            Call sprdOrder.AddCellSpan(COL_SPARE, (FOOTER_LINES + HEADER_LINES + ROWS_PER_PAGE) + 9, (lngLastCol - COL_SPARE) + 1, 1)
        Next lngPagePos
    End If

    'Perform print functions
    sprdOrder.PrintColHeaders = False
    sprdOrder.PrintRowHeaders = False
    sprdOrder.PrintSmartPrint = False
    sprdOrder.PrintGrid = False
    
    'clear out last line and create block around Transmission status
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 13, sprdOrder.MaxCols, sprdOrder.MaxRows, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 13, sprdOrder.MaxCols, sprdOrder.MaxRows, 16, RGB_BLACK, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows, sprdOrder.MaxCols, sprdOrder.MaxRows, 16, RGB_BLACK, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_QTYDEL, sprdOrder.MaxRows - 13, sprdOrder.MaxCols, sprdOrder.MaxRows - 5, 16, RGB_BLACK, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_QTYDEL, sprdOrder.MaxRows - 5, sprdOrder.MaxCols, sprdOrder.MaxRows, 16, RGB_BLACK, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(COL_QTYDEL, sprdOrder.MaxRows - 13, sprdOrder.MaxCols, sprdOrder.MaxRows - 13, 16, RGB_BLACK, CellBorderStyleSolid)

    sprdOrder.Col = COL_SPARE
    sprdOrder.Row = 1
    sprdOrder.Text = "DELIVERY NOTE"
    
    If strActionCode = ACTION_MAINTISTOUT Then
        sprdOrder.Text = sprdOrder.Text & " - Reprint"
        sprdOrder.PrintFooter = "/c/fb1REPRINT/n" & sprdOrder.PrintFooter
    End If
    
    sprdOrder.FontSize = 16
    sprdOrder.RowHeight(1) = sprdOrder.MaxTextCellHeight
    sprdOrder.PrintJobName = "IST Out(Del Note) - " & strISTOutNoteNumber
    Call sprdOrder.PrintSheet
    
    If strActionCode = ACTION_ISTOUT Then
        sprdOrder.Col = COL_SPARE
        sprdOrder.Row = 1
        sprdOrder.Text = "STORE COPY"
        sprdOrder.PrintJobName = "IST Out(Store Copy) - " & strISTOutNoteNumber
        sprdOrder.Refresh
        Call sprdOrder.PrintSheet
    End If
    
    For lngLineNo = 1 To sprdOrder.MaxRows - 1 Step 1
        sprdOrder.Row = lngLineNo
        sprdOrder.RowPageBreak = False
    Next lngLineNo
        
    For lngPagePos = lngNoPages - 1 To 0 Step -1
        Call sprdOrder.DeleteRows((lngPagePos + 1) * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) - FOOTER_LINES + 1, FOOTER_LINES)
        Call sprdOrder.DeleteRows(lngPagePos * (ROWS_PER_PAGE + HEADER_LINES + FOOTER_LINES) + 1, HEADER_LINES)
    Next lngPagePos

    'Remove header
'    Call sprdOrder.DeleteRows(1, 10)
    sprdOrder.MaxRows = lngMaxRows
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    
    sprdOrder.Col = COL_SPARE
    sprdOrder.ColHidden = True
    sprdOrder.Col = COL_SPARE2
    sprdOrder.ColHidden = True
    
    'Display column not printed
    Call DisplayPrintCols


End Sub

Private Sub PrintSupplierReturn(strSupplierRequestNo As String, strRequestDRLNo As String, dteCreateDate As Date, strReturnNotes As String, strCreatedBy As String, strDelNoDRLNo As String, strOrigDelNoteNo As String, blnShortagesNote As Boolean, blnReprint As Boolean)

Const NO_HEADER_LINES As Long = 19
Const NO_FOOTER_LINES As Long = 16
Const NO_LINES_PER_PAGE As Long = 28
Const LAST_FOOTER_LINES As Long = 8

Dim lngLineNo    As Long
Dim lngMaxRows   As Long
Dim lngQtyCol    As Long
Dim lngReasCol   As Long
Dim lngItemNo    As Long
Dim lngColsFroz  As Long
Dim lngRowsShown As Long
Dim strTotShort  As String
Dim strComment   As String
Dim oUser        As Object
Dim lngPagePos   As Long
Dim lngNoPages   As Long
Dim blnLastItem  As Boolean
    
    lngColsFroz = sprdOrder.ColsFrozen
    lngRowsShown = 0
    sprdOrder.ColsFrozen = 0
    sprdOrder.PrintFooter = GetFooter
    
    If strCreatedBy = goSession.UserInitials Then
        strCreatedBy = goSession.UserFullname
    Else
        Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        Call oUser.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_Initials, strCreatedBy)
        Call oUser.IBo_AddLoadField(FID_USER_FullName)
        Call oUser.IBo_Loadmatches
        strCreatedBy = oUser.FullName
        If strCreatedBy = "" Then strCreatedBy = "UNKNOWN"
        Set oUser = Nothing
    End If
    
    'get name and address for current store to put at top of document
    Call GetPrintStore
                               
    'get total short, in case it is displayed further on
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.Row = 1
    strTotShort = sprdOrder.Text
    
    ReDim arDispCols(0) 'reset hidden column flag
    'need to hide columns for delivery notes - so not always shown
    Call HidePrintColumn(COL_QTY)
    Call HidePrintColumn(COL_QTYDEL)
    Call HidePrintColumn(COL_QTYBORD)
    Call HidePrintColumn(COL_CUSTONO)
    Call HidePrintColumn(COL_COMMENT)
    
    Call HidePrintColumn(COL_QTYOS)
    Call HidePrintColumn(COL_PACKQTY)
    Call HidePrintColumn(COL_QTY)
    Call HidePrintColumn(COL_ONHAND)
    Call HidePrintColumn(COL_RETCMNT)
    Call HidePrintColumn(COL_FREE)
    Call HidePrintColumn(COL_LASTORDER)
    Call HidePrintColumn(COL_DELTOTAL)
    
    'Switch between Request for Credit and Goods Returned
    If blnShortagesNote = True Then
        sprdOrder.Col = COL_SHORTCODE
        sprdOrder.ColHidden = False
        sprdOrder.ColWidth(COL_SHORTCODE) = sprdOrder.ColWidth(COL_RETCODE)
        HidePrintColumn (COL_QTYRET)
        lngQtyCol = COL_QTYSHORT
        HidePrintColumn (COL_RETCODE)
        lngReasCol = COL_SHORTCODE
        Call HidePrintColumn(COL_RETCMNT) 'hide comment - Short is Short
    Else
        HidePrintColumn (COL_QTYSHORT)
        lngQtyCol = COL_QTYRET
        lngReasCol = COL_RETCODE
    End If
    
    Call SetLastPrintCol
    
    'Perform a check to ensure that all captured lines have a return quantity else
    'hide them - caused by Returns on a delivery note, renumber item no into sequence
    lngItemNo = 1
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Col = lngQtyCol
        sprdOrder.Row = lngLineNo
        If (Val(sprdOrder.Text) = 0) Or (sprdOrder.ForeColor = vbBlue) Then
            sprdOrder.RowHidden = True
        Else
            sprdOrder.Col = COL_ITEMNO
            sprdOrder.Text = lngItemNo
            lngItemNo = lngItemNo + 1
        End If
    Next lngLineNo
    
    'On returns note, move comments onto line below to allow for more space
    If blnShortagesNote = False Then
        For lngLineNo = sprdOrder.MaxRows To ROW_FIRST Step -1
            sprdOrder.Row = lngLineNo
            If sprdOrder.RowHidden = False Then
                sprdOrder.Col = COL_RETCMNT
                strComment = sprdOrder.Text
                sprdOrder.MaxRows = sprdOrder.MaxRows + 1
                Call sprdOrder.InsertRows(sprdOrder.Row + 1, 1)
                sprdOrder.Row = sprdOrder.Row + 1
                sprdOrder.Col = COL_PARTCODE
                Call sprdOrder.AddCellSpan(COL_PARTCODE, sprdOrder.Row, COL_MANUCODE - COL_PARTCODE + 1, 1)
                sprdOrder.TypeMaxEditLen = 100
                sprdOrder.Text = "  Comment:" & strComment
            End If
        Next lngLineNo
    End If 'move comments for Return Notes
    
    'Once only required lines are displayed and any comments added, count up the number of lines to print
    sprdOrder.Col = COL_PARTCODE
    lngNoPages = 1
    lngMaxRows = 0
    blnLastItem = False 'ensure last item is not at end of page - else blank page printed
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNo
        If sprdOrder.RowHidden = False Then
            lngRowsShown = lngRowsShown + 1
            blnLastItem = False
            If (lngRowsShown Mod (NO_LINES_PER_PAGE - 2)) = 0 Then
                lngNoPages = lngNoPages + 1
                'Mark line as page break required
                sprdOrder.BackColor = RGB_RED
                blnLastItem = True
                lngMaxRows = sprdOrder.Row
            End If
        End If
    Next lngLineNo
    If blnLastItem = True Then 'omit last page as it will be blank
        lngNoPages = lngNoPages - 1
        sprdOrder.Row = lngMaxRows
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.BackColor = RGB_WHITE
    End If
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = False
    lngMaxRows = sprdOrder.MaxRows
    'add extra rows to hold Order Header
    sprdOrder.MaxRows = (lngNoPages * (NO_HEADER_LINES + NO_FOOTER_LINES + NO_LINES_PER_PAGE))
    For lngLineNo = lngMaxRows To 1 Step -1
        sprdOrder.Row = lngLineNo
        sprdOrder.Col = COL_PARTCODE
        If sprdOrder.BackColor = RGB_RED Then
            Call sprdOrder.InsertRows(sprdOrder.Row + 1, NO_HEADER_LINES + NO_FOOTER_LINES)
            sprdOrder.Col = -1
            sprdOrder.Row = lngLineNo + 1
            sprdOrder.BackColor = RGB_LTGREY
            sprdOrder.Row = lngLineNo + NO_HEADER_LINES + NO_FOOTER_LINES
            sprdOrder.BackColor = RGB_LTGREY
        End If
    Next lngLineNo
    Call sprdOrder.InsertRows(1, NO_HEADER_LINES)
    
    'Clear out Header Grid Lines
    sprdOrder.Col = 1
    sprdOrder.Col2 = sprdOrder.MaxCols
    sprdOrder.Row = 1
    sprdOrder.Row2 = NO_HEADER_LINES
    sprdOrder.BlockMode = True
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.BlockMode = False
    
    'Create Order Header
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row = 1
    sprdOrder.Text = goSession.CurrentEnterprise.IEnterprise_FullName
    sprdOrder.TypeHAlign = TypeHAlignCenter
    sprdOrder.FontSize = 16
    sprdOrder.RowHeight(1) = sprdOrder.MaxTextCellHeight
    'Put in Logo
    sprdOrder.Col = lngLastCol
    sprdOrder.CellType = CellTypePicture
    If mstrLogoFile <> "" Then
        sprdOrder.TypePictPicture = LoadPicture(mstrLogoFile)
        sprdOrder.TypeHAlign = TypeHAlignRight
        sprdOrder.RowHeight(1) = sprdOrder.MaxTextRowHeight(1)
    End If
    
    Call sprdOrder.AddCellSpan(COL_ITEMNO, 2, COL_QTYRET, 1)
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row = 2
    sprdOrder.Text = "REQUEST FOR CREDIT"
    sprdOrder.FontSize = 16
    sprdOrder.TypeVAlign = TypeVAlignCenter
    If blnShortagesNote = True Then
        sprdOrder.Text = sprdOrder.Text & "(Shortage)"
    Else
        sprdOrder.Text = sprdOrder.Text & "(Return)"
    End If
    If strActionCode = ACTION_MAINTRETURN Then
        If blnReprint = True Then
            sprdOrder.Text = sprdOrder.Text & "-REPRINT"
        Else
            sprdOrder.Text = sprdOrder.Text & "-MAINTAINED"
        End If
        sprdOrder.FontBold = True
    End If
    Call sprdOrder.SetCellBorder(1, 2, 1, 2, 15, RGB_RED, CellBorderStyleSolid)
    'Put in Logo
    'Create Order Header
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = 4
    sprdOrder.Text = poPrintStore.AddressLine1
    sprdOrder.Col = lngQtyCol
    sprdOrder.Text = "Store"
    sprdOrder.Col = lngReasCol
    sprdOrder.Text = poPrintStore.StoreNumber
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 5
    sprdOrder.Text = poPrintStore.AddressLine2
    sprdOrder.Row = 6
    sprdOrder.Text = poPrintStore.AddressLine3
    sprdOrder.Row = 7
    sprdOrder.Text = poPrintStore.AddressLine4
    sprdOrder.Row = 8
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "Phone :"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = poPrintStore.PhoneNo
    sprdOrder.Row = 8
    sprdOrder.Col = lngQtyCol
    sprdOrder.Text = "Fax :"
    sprdOrder.Col = lngReasCol
    sprdOrder.Text = poPrintStore.FaxNo
    
    sprdOrder.Row = 16
    sprdOrder.Col = lngQtyCol
    sprdOrder.Text = "DRL : "
    sprdOrder.Col = lngReasCol
    sprdOrder.Text = strRequestDRLNo
    
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = 10
    sprdOrder.Text = lblSuppNo.Caption & ":" & lblSuppName.Caption
    sprdOrder.Row = 11
    sprdOrder.Text = "Phone No :"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = lblPhoneNo.Caption
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = 12
    sprdOrder.Text = "Fax No :"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = lblFaxNo.Caption
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = 13
    sprdOrder.Text = "Contact :"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = lblContactName.Caption
    
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row = 14
    sprdOrder.Text = "Orig Del Note No :"
    
    sprdOrder.Row = 16
    sprdOrder.Text = "Request No :"
    
    sprdOrder.Row = 17
    sprdOrder.Text = "Please deal with the following :"
    
    sprdOrder.Row = 18
    If blnShortagesNote = True Then
        sprdOrder.Text = sprdOrder.Text & "Credit needing no collection :"
    Else
        sprdOrder.Text = sprdOrder.Text & "Items to be collected :"
    End If
    
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = 14
    sprdDelNotes.Col = 1
    sprdDelNotes.Row = 1
    If strOrigDelNoteNo <> "" Then
        sprdOrder.Text = strOrigDelNoteNo
    Else
        sprdOrder.Text = "Not Specified"
    End If
    sprdOrder.Row = 16
    sprdOrder.Text = strSupplierRequestNo
    
    sprdOrder.RowHeight(3) = 3
    sprdOrder.RowHeight(9) = 3
    sprdOrder.RowHeight(15) = 3
    'Clear boxes from top of delivery note
    Call sprdOrder.SetCellBorder(1, 1, sprdOrder.MaxCols, 19, 15, RGB_GREY, CellBorderStyleBlank)
    Call sprdOrder.SetCellBorder(1, 2, lngLastCol, 2, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 3, lngLastCol, 3, 4, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 4, lngLastCol, 8, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 9, lngLastCol, 14, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 14, lngLastCol, 14, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 15, lngLastCol, 15, 4, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 16, lngLastCol, 18, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 17, lngLastCol, 17, 4, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 19, lngLastCol, 19, 4, RGB_RED, CellBorderStyleSolid)
    
    Call sprdOrder.SetCellBorder(1, 11, lngLastCol, 15, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 11, lngLastCol, 15, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, 11, lngLastCol, 15, 16, RGB_RED, CellBorderStyleSolid)
    
    'Display Order Footer
    sprdOrder.RowHeight(sprdOrder.MaxRows - 16) = 1
    sprdOrder.RowHeight(sprdOrder.MaxRows - 14) = 1
    sprdOrder.Row = sprdOrder.MaxRows - 15
    sprdOrder.Col = COL_DESC
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.Text = "No. of Items "
    
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.Row = sprdOrder.MaxRows - 15
    sprdOrder.Text = strTotShort
    
    sprdOrder.Col = COL_QTYRET
    sprdTotal.Row = 2
    sprdTotal.Col = COL_TOTQTYRET
    sprdOrder.Text = sprdTotal.Text
    
    sprdOrder.Col = 6
    sprdOrder.CellType = CellTypeStaticText
    sprdOrder.TypeHAlign = TypeHAlignRight
    sprdOrder.Text = "Totals "
    sprdOrder.Col = COL_QTY
    sprdOrder.Text = "0"
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Text = "0"
    
    'Display Line item column headings
    sprdOrder.Row = 19
    Call sprdOrder.SetCellBorder(1, 19, lngLastCol, 19, 15, RGB_RED, CellBorderStyleSolid)
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "No"
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = "SKU"
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = "Description"
    sprdOrder.Col = COL_SIZE
    sprdOrder.Text = "Size"
    sprdOrder.Col = lngReasCol
    sprdOrder.Text = "Reason"
    sprdOrder.Col = COL_RETCMNT
    sprdOrder.Text = "Comment"
    sprdOrder.Col = COL_QTYRET
    sprdOrder.Text = "Quantity"
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.Text = "Quantity"
    sprdOrder.Col = COL_UNITS
    sprdOrder.Text = "Units"
    sprdOrder.Col = COL_DISC
    sprdOrder.Text = "Disc"
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.Text = "Total Ex"
    sprdOrder.Col = COL_CUSTONO
    sprdOrder.Text = "Cust O.No"
    sprdOrder.Col = COL_MANUCODE
    sprdOrder.Text = "Manu Code"
    
    'Perform print functions
    sprdOrder.PrintColHeaders = False
    sprdOrder.PrintRowHeaders = False
    sprdOrder.PrintSmartPrint = False
    sprdOrder.PrintGrid = False
    
    'clear out last line and create block around Transmission status
    sprdOrder.Col = 1
    sprdOrder.Col2 = sprdOrder.MaxCols
    sprdOrder.Row = sprdOrder.MaxRows - 13
    sprdOrder.Row2 = sprdOrder.MaxRows
    sprdOrder.BlockMode = True
    sprdOrder.CellType = CellTypeEdit
    sprdOrder.BlockMode = False
    
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 12, sprdOrder.MaxCols, sprdOrder.MaxRows, 15, RGB_GREY, CellBorderStyleBlank)
    sprdOrder.RowHeight(sprdOrder.MaxRows - 13) = 3
    sprdOrder.RowHeight(sprdOrder.MaxRows - 9) = 3
    sprdOrder.RowHeight(sprdOrder.MaxRows - 6) = 3
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 12, lngLastCol, sprdOrder.MaxRows - 10, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 9, lngLastCol, sprdOrder.MaxRows - 9, 4, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 8, lngLastCol, sprdOrder.MaxRows - 7, 16, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 6, lngLastCol, sprdOrder.MaxRows - 6, 4, RGB_RED, CellBorderStyleSolid)
    Call sprdOrder.SetCellBorder(1, sprdOrder.MaxRows - 5, lngLastCol, sprdOrder.MaxRows, 16, RGB_RED, CellBorderStyleSolid)
    
    'ensure footer does not have multi-line on, else texts wraps round and disappears
    sprdOrder.Row = sprdOrder.MaxRows - 12
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row2 = sprdOrder.MaxRows
    sprdOrder.Col2 = sprdOrder.MaxCols
    sprdOrder.BlockMode = True
    sprdOrder.TypeEditMultiLine = False
    sprdOrder.BlockMode = False
    
    sprdOrder.Row = sprdOrder.MaxRows - 12
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "Comment"
    sprdOrder.FontBold = True
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Row = sprdOrder.MaxRows - 11
    Call sprdOrder.AddCellSpan(COL_PARTCODE, sprdOrder.Row, lngLastCol - COL_PARTCODE, 2)
    sprdOrder.CellType = CellTypeEdit
    sprdOrder.TypeMaxEditLen = 200
    sprdOrder.TypeEditMultiLine = True
    sprdOrder.Text = strReturnNotes
    sprdOrder.FontBold = True
    
    sprdOrder.Row = sprdOrder.MaxRows - 8
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = "Raised by"
    
    sprdOrder.Row = sprdOrder.MaxRows - 7
    sprdOrder.Text = "Date"
    
    sprdOrder.Col = COL_DESC
    sprdOrder.Row = sprdOrder.MaxRows - 8
    sprdOrder.Text = strCreatedBy
    sprdOrder.Row = sprdOrder.MaxRows - 7
    sprdOrder.Text = DisplayDate(dteCreateDate, False)
    
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Row = sprdOrder.MaxRows - 5
    sprdOrder.Text = "For Office Use Only "
    sprdOrder.FontBold = True
    sprdOrder.FontUnderline = True
    sprdOrder.Row = sprdOrder.MaxRows - 7
    sprdOrder.Text = "Date"
    
    If blnShortagesNote = False Then 'display collection information if items to be returned
        sprdOrder.Row = sprdOrder.MaxRows - 4
        If (sprdDelNotes.Text <> "") And ((strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_MAINTRETURN)) Then
            sprdOrder.Text = "Date collected : " & dtxtDueDate.Text
        Else
            sprdOrder.Text = "Date collected : "
        End If
        sprdOrder.Row = sprdOrder.MaxRows - 3
        If ((strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_MAINTRETURN)) Then
            sprdOrder.Text = "Supplier collection note number : " & sprdDelNotes.Text
        Else
            sprdOrder.Text = "Supplier collection note number : "
        End If
    Else
    If blnShortagesNote = False Then 'display collection information if items to be returned
        sprdOrder.Row = sprdOrder.MaxRows - 4
        sprdOrder.RowHidden = True
        sprdOrder.Row = sprdOrder.MaxRows - 3
        sprdOrder.RowHidden = True
    End If
    
    End If
    
    sprdOrder.Row = sprdOrder.MaxRows - 1
    sprdOrder.Text = "Sent to Supplier"
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Text = "By"

    sprdOrder.PrintJobName = "Supp Return - " & strSupplierRequestNo
    
    'Step through identified lines and insert header/footers
    lngPagePos = 0
    For lngLineNo = 1 To sprdOrder.MaxRows Step 1
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Row = lngLineNo
'        sprdOrder.MaxRows = sprdOrder.MaxRows + NO_HEADER_LINES + NO_FOOTER_LINES
        If sprdOrder.BackColor = RGB_RED Then
            lngPagePos = lngPagePos + 1
            Call sprdOrder.CopyRange(COL_ITEMNO, (sprdOrder.MaxRows - NO_FOOTER_LINES) + 1, sprdOrder.MaxCols, sprdOrder.MaxRows, COL_ITEMNO, sprdOrder.Row + 1)
            sprdOrder.Row = sprdOrder.Row + NO_FOOTER_LINES - 1
            If lngPagePos <> lngNoPages Then
                Call sprdOrder.AddCellSpan(COL_ITEMNO, sprdOrder.Row, sprdOrder.MaxCols, 2)
                sprdOrder.Col = COL_ITEMNO
                
                sprdOrder.Text = "    Continued on next page"
                sprdOrder.FontBold = True
            End If
            
            sprdOrder.Row = sprdOrder.Row + 1
            If lngPagePos <> lngNoPages Then sprdOrder.RowPageBreak = True
            sprdOrder.Row = sprdOrder.Row + 1
            
            Call sprdOrder.CopyRange(COL_ITEMNO, 1, sprdOrder.MaxCols, NO_HEADER_LINES, COL_ITEMNO, sprdOrder.Row)
            For lngItemNo = 1 To NO_HEADER_LINES Step 1
                sprdOrder.RowHeight(sprdOrder.Row + lngItemNo - 1) = sprdOrder.RowHeight(lngItemNo)
            Next lngItemNo
            'Fill in current page number on Header of print out
            sprdOrder.Row = sprdOrder.Row + 6
            sprdOrder.Col = lngQtyCol
            sprdOrder.Text = "Page "
            sprdOrder.Col = lngReasCol
            sprdOrder.Text = lngPagePos & "/" & lngNoPages
        End If
    Next lngLineNo
    
    If (blnShortagesNote = False) And (strActionCode <> ACTION_MAINTRETURN) Then 'print 2 copies, with header for Store/ Goods
        'Print copy of returns note to go with the goods
        sprdOrder.PrintJobName = "Supp Return - " & strSupplierRequestNo & "(Goods Copy)"
        sprdOrder.Col = COL_RETCODE
        sprdOrder.Row = 2
        sprdOrder.Text = "Copy for Goods"
        sprdOrder.FontSize = 14
        sprdOrder.Refresh
        Call sprdOrder.PrintSheet
        'Now print Store copy of returns note
        sprdOrder.PrintJobName = "Supp Return - " & strSupplierRequestNo & "(Store Copy)"
        sprdOrder.Col = COL_RETCODE
        sprdOrder.Row = 2
        sprdOrder.Text = "Store Copy"
        sprdOrder.FontSize = 14
        sprdOrder.Refresh
        Call sprdOrder.PrintSheet
    Else
        sprdOrder.Refresh
        Call sprdOrder.PrintSheet
    End If
    
    sprdOrder.Col = COL_PARTCODE
    For lngLineNo = 1 To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNo
        If sprdOrder.BackColor = RGB_RED Then
            Call sprdOrder.DeleteRows(sprdOrder.Row + 1, NO_HEADER_LINES + NO_FOOTER_LINES)
        End If
    Next lngLineNo
    
    'Remove header
    Call sprdOrder.DeleteRows(1, 19)
    
    sprdOrder.MaxRows = lngMaxRows
    sprdOrder.Row = -1
    sprdOrder.Col = COL_ALL
    sprdOrder.FontBold = True
    
    'On returns note, remove comments from following line
    If blnShortagesNote = False Then
        For lngLineNo = sprdOrder.MaxRows To ROW_FIRST Step -1
            sprdOrder.Row = lngLineNo
            If sprdOrder.RowHidden = False Then
                sprdOrder.Col = COL_ITEMNO
                If Val(sprdOrder.Text) > 0 Then
                    Call sprdOrder.DeleteRows(sprdOrder.Row + 1, 1)
                    sprdOrder.MaxRows = sprdOrder.MaxRows - 1
                End If
            End If
        Next lngLineNo
    End If 'move comments for Return Notes
    
    'Display column not printed
    Call DisplayPrintCols
    sprdOrder.Col = COL_SHORTCODE
    sprdOrder.ColHidden = True

    're-show any hidden rows as they may need to be printed next
    lngItemNo = 1
    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNo
        If sprdOrder.RowHidden = True Then
            sprdOrder.RowHidden = False
            sprdOrder.Col = COL_ITEMNO
            sprdOrder.Text = lngItemNo
            lngItemNo = lngItemNo + 1
        End If
    Next lngLineNo
    Call sprdOrder.Refresh
    sprdOrder.ColsFrozen = lngColsFroz
    
End Sub

Private Sub ResetGrid()

    Call DebugMsg(MODULE_NAME, "ResetGrid", endlTraceIn)
    ucpbProgress.Caption1 = "Clearing Line Items"
'    Call sprdOrder.DeleteRows(ROW_FIRST, sprdOrder.MaxRows - 1)
    sprdOrder.MaxRows = 1
    Call AddLine
    sprdOrder.MaxRows = ROW_FIRST
    DoEvents
    sprdOrder.Row = ROW_FIRST
    sprdOrder.Col = COL_ALL
    sprdOrder.BackColor = RGB_WHITE
    sprdOrder.ForeColor = RGB_BLACK
    sprdOrder.Col = COL_COMMENT
    sprdOrder.BackColor = RGB_GREY
    sprdOrder.Col = 0
    sprdOrder.Text = " "
                
    Call ResetTotals
    cmdSave.Visible = False
    cmdSaveHO.Visible = False

End Sub
Private Sub UpdateDeliveryDate()

Dim oOrder As Object
    
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
    If Val(lblOrderID.Caption) <> 0 Then
        oOrder.Key = Val(lblOrderID.Caption)
        Call oOrder.IBo_Load
        Call oOrder.UpdateDeliveryDate(GetDate(dtxtDueDate.Text))
    End If
    Set oOrder = Nothing

End Sub

Private Function UpdateCollectionDate() As Boolean

Dim oReturn As Object
    
    UpdateCollectionDate = False
'    If (GetDate(lblOrderDate.Caption) > GetDate(dtxtDueDate.Text)) Or (GetDate(dtxtDueDate.Text) > Date) Then
'        Call MsgBoxEx("Collection date invalid" & vbCrLf & "Collection date cannot be less than Request date and not later than today", vbExclamation, "Unable to process " & mstrFunction, , , , , RGBMsgBox_WarnColour)
'        dtxtDueDate.SetFocus
'        Exit Function
'    End If
    Set oReturn = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
    If lblOrderID.Caption <> "" Then
        oReturn.DocNo = lblOrderID.Caption
        Call oReturn.IBo_Load
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = 1
        Call oReturn.RecordCollected(lblOrderID.Caption, sprdDelNotes.Text, GetDate(dtxtDueDate.Text))
    End If
    Set oReturn = Nothing
    UpdateCollectionDate = True

End Function

Private Sub cmdSave_GotFocus()

    cmdSave.FontBold = True

End Sub

Private Sub cmdSave_LostFocus()
    
    cmdSave.FontBold = False

End Sub

Private Sub cmdViewCons_Click()

    Load frmViewCons
    Call frmViewCons.DisplayConsolidation(lblOrderNo.Caption, GetDate(lblOrderDate.Caption), mcolConLines, sprdOrder, COL_DESC, COL_QTY, COL_SIZE, lblSuppNo.Caption & " : " & lblSuppName.Caption)
    Call frmViewCons.Show(vbModal)
    Unload frmViewCons

End Sub

Private Sub cmdViewCons_GotFocus()

    cmdViewCons.FontBold = True

End Sub

Private Sub cmdViewCons_LostFocus()
    
    cmdViewCons.FontBold = False

End Sub

Private Sub ViewPendingISTs()
'Called to list IST's from other stores waiting to be processed
                    
    Load frmDetails
    If frmDetails.LoadPendingISTs(mcolStores) = False Then
        Unload frmDetails
        Exit Sub
    End If
    
    'show user the form and wait for user
    Call frmDetails.Show(vbModal)
    
    If frmDetails.StoreNumber = "" Then
        Unload frmDetails
        Exit Sub
    End If 'user pressed process button
    
    Call LoadPendingIST(frmDetails.StoreNumber, frmDetails.ISTOutNumber)
    fraAddress.Visible = True
    lblUserDate.Visible = True
    dtxtDueDate.Visible = True
    dtxtDueDate.Text = DisplayDate(Now(), False)
    Unload frmDetails
    Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.MaxRows)
    Call UpdateTotals
    DoEvents
    Me.SetFocus
    dtxtDueDate.SetFocus

End Sub

Private Sub cmdViewNotes_Click()

    Load frmNotes
    If (strActionCode = ACTION_MAINTDN) And (fraSelection.Enabled = False) Then
        Call frmNotes.ViewNotes(mstrNotes)
    Else
        Call frmNotes.CaptureNotes(mstrNotes, mstrDocCmnt, enatAdjustment)
    End If
    Unload frmNotes
    
End Sub

Private Sub cmdViewNotes_GotFocus()

    cmdViewNotes.FontBold = True

End Sub

Private Sub cmdViewNotes_LostFocus()
    
    cmdViewNotes.FontBold = False

End Sub

Private Sub cmdViewParked_Click()

Dim oPOrder       As Object
Dim strSuppNumber As String
Dim blnCustOrders As Boolean

    If cmdViewParked.Visible = False Then Exit Sub
    lblOrderID.Caption = ""
    Load frmDetails
    Call frmDetails.LoadParkedOrders(cmbSupplier)
    Call frmDetails.Show(vbModal)
    strSuppNumber = frmDetails.SupplierNo
    Unload frmDetails
    cmdDelete.Visible = False
    If strSuppNumber <> "" Then
        lblSuppNo.Caption = strSuppNumber
        Call DisplaySupplier(lblSuppNo.Caption, blnCustOrders)
        ucpbProgress.Visible = True
        DoEvents
        ucpbProgress.Caption1 = "Retrieving for Parked Orders"
        Call DebugMsg(MODULE_NAME, "cmdViewParked_Click", endlDebug, "Started Retrieve-" & Now())
        Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
        If oPOrder.GetParkedOrder(lblSuppNo.Caption) Then
            'lblOrderDate.Caption = DisplayDate(oPOrder.OrderDate, False)
            lblOrderDate.Caption = DisplayDate(Date, False)
            
            'display Progress Bar
            ucpbProgress.Max = oPOrder.Lines.Count + 1
            ucpbProgress.Caption1 = "Retrieving Lines for Parked Order"
            lblOrderID.Caption = oPOrder.Key
            
            Call DebugMsg(MODULE_NAME, "cmbSupplier_closeup", endlDebug, "Displaying Lines-" & Now())
            Call DisplayOrderLines(oPOrder, False, False, Nothing)
            cmdDelete.Visible = True
            
            If blnCustOrders Then Call cmdViewCOrders_Click
            
            dtxtDueDate.Visible = True
            lblUserDate.Visible = True
        End If
        ucpbProgress.Visible = False
        cmdVoid.Visible = True
    End If
    If sprdOrder.Visible = True Then sprdOrder.SetFocus
    If dtxtDueDate.Visible = True And dtxtDueDate.Enabled = True Then dtxtDueDate.SetFocus

End Sub


Private Sub cmdViewParked_GotFocus()
    
    cmdViewParked.FontBold = True

End Sub

Private Sub cmdViewParked_LostFocus()
    
    cmdViewParked.FontBold = False

End Sub

Private Sub cmdVoid_Click()
    
    'Before doing anything - check if any changes have been made and if so, confirm Void
    If cmdVoid.Visible = True Then
        Select Case (strActionCode)
            Case (ACTION_DELNOTE), (ACTION_MAINTPO):
                        If MsgBoxEx("Confirm void current document and lose any changes made", vbOKCancel + vbQuestion, "Void document", , , , , RGBMSGBox_PromptColour) = vbCancel Then
                            Exit Sub
                        End If
                        
            Case (ACTION_MAINTDN):
                        If MsgBoxEx("Confirm void current document and lose any changes made", vbOKCancel + vbQuestion, "Void document", , , , , RGBMSGBox_PromptColour) = vbCancel Then
                            Exit Sub
                        End If
                        
            Case (ACTION_PORDER):
                        sprdTotal.Row = 2
                        sprdTotal.Col = COL_TOTQTY
                        If Val(sprdTotal.Text) > 0 Then
                            If MsgBoxEx("Confirm void current purchase order and lose items entered", vbOKCancel + vbQuestion, "Void purchase order", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        Else
                            If MsgBoxEx("Confirm void current purchase order and lose any changes made", vbOKCancel + vbQuestion, "Void purchase order", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        End If
                        lblOrderID.Caption = ""
            Case (ACTION_RETURN):
                        sprdTotal.Row = 2
                        sprdTotal.Col = COL_TOTQTYRET
                        If Val(sprdTotal.Text) > 0 Then
                            If MsgBoxEx("Confirm void supplier return and lose items entered", vbOKCancel + vbQuestion, "Void supplier return", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        Else
                            If MsgBoxEx("Confirm void supplier return and lose any changes made", vbOKCancel + vbQuestion, "Void supplier return", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        End If
            Case (ACTION_ISTIN):
                        sprdTotal.Row = 2
                        sprdTotal.Col = COL_TOTQTY
                        If Val(sprdTotal.Text) > 0 Then
                            If MsgBoxEx("Confirm void current IST and lose items entered", vbOKCancel + vbQuestion, "Void IST receipt", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        Else
                            If MsgBoxEx("Confirm void current IST and lose any changes made", vbOKCancel + vbQuestion, "Void IST receipt", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        End If
            Case (ACTION_ISTOUT):
                        sprdTotal.Row = 2
                        sprdTotal.Col = COL_TOTQTY
                        If Val(sprdTotal.Text) > 0 Then
                            If MsgBoxEx("Confirm void current IST and lose items entered", vbOKCancel + vbQuestion, "Void IST", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        Else
                            If MsgBoxEx("Confirm void current IST and lose any changes made", vbOKCancel + vbQuestion, "Void IST", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
                        End If
            Case (ACTION_MAINTRETURN):
                        If MsgBoxEx("Confirm void current document and lose any changes made", vbOKCancel + vbQuestion, "Void document", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
            Case (ACTION_MAINTISTOUT):
                        If MsgBoxEx("Confirm void current document and lose any changes made", vbOKCancel + vbQuestion, "Void document", , , , , RGBMSGBox_PromptColour) = vbCancel Then Exit Sub
        End Select
    End If 'check required as user pressed Void - not required if system called
    
    'Hide standard buttons
    cmdVoid.Visible = False
    cmdViewNotes.Visible = False
    fraSelection.Visible = False
    fraAddress.Visible = False
    sprdOrder.Visible = False
    sprdTotal.Visible = False
    cmdSave.Visible = False
    cmdSaveHO.Visible = False
    txtReference.Text = ""
    lblReleaseNo.Caption = "00"
    Set moEDIDelNote = Nothing
    Call ResetGrid
    DoEvents
    
    'Hide action specific codes and return to start new transaction
    Select Case (strActionCode)
        Case (ACTION_DELNOTE), (ACTION_MAINTPO):
                cmdRePrint.Visible = False
                cmdDelete.Visible = False
                cmdViewCons.Visible = False
                cmdClose.Visible = False
                cmdDelLine.Visible = False
                Call ucofOrders.Reset
                Call ucofOrders.Show
                ucofOrders.Visible = True
        Case (ACTION_MAINTDN):
                cmdDelete.Visible = False
                cmdRePrint.Visible = False
                cmdClose.Visible = False
                Call ucdnDelNotes.Reset
                ucdnDelNotes.Visible = True
                ucdnDelNotes.SetFocus
        Case (ACTION_PORDER), (ACTION_RETURN):
                cmdDelLine.Visible = False
                cmdDelete.Visible = False
                lblUserDate.Visible = False
                dtxtDueDate.Visible = False
                lblReferencelbl.Visible = False
                txtReference.Visible = False
                sprdDelNotes.Visible = False
                lblDelNoteNoLbl.Visible = False
                If strActionCode = ACTION_PORDER Then 'check if Park Orders must be processed
                    cmdViewParked.Visible = True
                    If NumberParkedOrders > mlngMaxParkedOrders Then
                        Call cmdViewParked_Click
                        If lblSuppNo.Caption = "" Then Call cmdClose_Click
                    Else
                        'Select new supplier/store to access
                        fraSelection.Visible = True
                        If chkStoresOnly.Value = 0 Then
                            cmbSupplier.Visible = True
                            cmbSupplier.SetFocus
                        Else
                            cmbStore.Visible = True
                            cmbStore.SetFocus
                        End If
                    End If
                Else 'if Supplier Return then display list of suppliers
                    fraSelection.Visible = True
                    cmbSupplier.Visible = True
                    cmbSupplier.SetFocus
                End If
                        
        Case (ACTION_ISTOUT), (ACTION_ISTIN):
                cmdDelLine.Visible = False
                lblUserDate.Visible = False
                dtxtDueDate.Visible = False
                lblDispTypelbl.Visible = False
                cmbDispatchType.Visible = False
                fraSelection.Visible = True
                If strActionCode = ACTION_ISTIN Then
                    If NumberPendingISTs > 0 Then ' (sbStatus)
                        Call ViewPendingISTs
                        If sprdDelNotes.Text = "" Then Call cmdClose_Click
                    Else
                        'if no EDI IST's then check if manual capture allowed
                        If goSession.GetParameter(PRM_MANUAL_ISTS) = False Then
                            Call MsgBoxEx("No pending IST's waiting to be processed" & vbCrLf & "Manual capture of IST receipts not allowed" & vbCrLf & "Exiting option", vbExclamation, "Record IST receipt", , , , , RGBMsgBox_WarnColour)
                            End
                        End If
                        'Select new supplier/store to access
                        cmbStore.Visible = True
                        cmbStore.SetFocus
                    End If
                Else 'if IST Out then display list of stores
                    cmbStore.Visible = True
                    cmbStore.SetFocus
                End If
        Case (ACTION_MAINTRETURN):
                cmdDelete.Visible = False
                cmdRePrint.Visible = False
                cmdClose.Visible = False
                cmdDelLine.Visible = False
                Call ucsrReturns.Reset
                ucsrReturns.Visible = True
                ucsrReturns.SetFocus
        Case (ACTION_MAINTISTOUT):
                cmdDelete.Visible = False
                cmdRePrint.Visible = False
                cmdClose.Visible = False
                cmdVoid.Visible = False
                Call ucioLookup.Reset
                DoEvents
                ucioLookup.Visible = True
'                ucioLookup.SetFocus
    End Select

End Sub

Private Sub dtxtDueDate_Change()

    mblnDueEdited = True
    lblDateDay.Caption = ""

End Sub

Private Sub dtxtDueDate_KeyPress(KeyAscii As Integer)
    
    Call DebugMsg(MODULE_NAME, "dtxtDueDate_keyPress", endlDebug, "Key=" & KeyAscii)
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If (sprdOrder.Visible = False) Then
            sprdOrder.Visible = True
            sprdTotal.Visible = True
            If (strActionCode <> ACTION_MAINTDN) Then cmdDelLine.Visible = True
            cmdViewParked.Visible = False
            Call sprdOrder.Refresh
            Call SendKeys(vbTab)
            DoEvents
            sprdOrder.EditMode = True
        Else
            Call SendKeys(vbTab)
            Exit Sub
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then
        If (sprdDelNotes.Visible = True) And (sprdDelNotes.Enabled = True) Then
            Call sprdDelNotes.SetFocus
        Else
            cmdVoid.Visible = False 'switch off user prompt
            Call cmdVoid_Click
        End If
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Call DebugMsg(MODULE_NAME, "Form_KeyDown", endlDebug, "Key" & KeyCode)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF1): 'if F1 then display Help List
                    uchlHelpKeys.Visible = True
                Case (vbKeyF3): 'if F3 then display Void transaction
                    KeyCode = 0
                    If cmdVoid.Visible Then Call cmdVoid_Click
                Case (vbKeyF4): 'process F4 dependant on Form Type
                    If ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTPO)) And (cmdViewCons.Visible = True) Then
                        Call cmdViewCons_Click
                    End If 'process view consolidation sheet
                    If (strActionCode = ACTION_PORDER) And (cmdViewCOrders.Visible = True) Then
                        Call cmdViewCOrders_Click
                    End If 'if F4 then display Customer Orders for Purchase Orders
                Case (vbKeyF5): 'if F5 then call save
                    If cmdSave.Visible Then
                        If sprdOrder.EditMode = True Then sprdOrder.EditMode = False
                        DoEvents
                        cmdSave_Click
                    End If
                Case (vbKeyF7):  'if F7 then call view parked or view notes for retrieved transaction
                    If cmdViewParked.Visible Then Call cmdViewParked_Click
                    If cmdViewNotes.Visible = True Then Call cmdViewNotes_Click
                Case (vbKeyF8):  'if F8 then call delete line
                    If cmdDelLine.Visible Then Call cmdDelLine_Click
                Case (vbKeyF9):  'if F9 then call reprint document
                    KeyCode = 0
                    Call cmdRePrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdClose_Click
            End Select 'Key pressed with no Shift/Alt combination
        Case (2) 'Ctrl Pressed
            Select Case (KeyCode)
                Case (vbKeyF5): 'if F5 then call saveHO
                    If cmdSaveHO.Visible Then cmdSaveHO_Click
                Case (vbKeyF8):  'if Ctrl - F8 then call delete order
                    If cmdDelete.Visible = True Then Call cmdDelete_Click
            End Select 'ALT pressed
    End Select

End Sub
Private Sub LoadPendingIST(strStoreNumber As String, strISTOutNumber As String)

Dim oHeader   As Object
Dim lngLineNo As Long
    
    Set oHeader = goDatabase.CreateBusinessObject(CLASSID_ISTINEDIHEADER)
    
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_SourceStoreNumber, strStoreNumber)
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_ISTOutNumber, strISTOutNumber)
    oHeader.GetLines = True
    If oHeader.IBo_Load Then
        cmdDelLine.Visible = False
        'Display sending store details
        lblSuppNo.Caption = strStoreNumber
        cmbStore.SearchText = strStoreNumber
        cmbStore.SearchMethod = SearchMethodPartialMatch
        cmbStore.Action = ActionSearch
        cmbStore.ListIndex = cmbStore.SearchIndex
        cmbStore.Text = cmbStore.List(cmbStore.ListIndex)
        Call cmbStore_KeyPress(13)
        
        Call ShowTotal(COL_TOTQTY, True)
        Call ShowTotal(COL_TOTQTYDEL, True)
        Call SetTotalsWidth
        'Display Header details
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = 1
        sprdDelNotes.MaxRows = 1
        lblOrderDate.Caption = DisplayDate(oHeader.SentDate, False)
        lblOrderDate.Visible = True
        lblOrderDatelbl.Visible = True
        sprdOrder.Col = COL_QTY
        sprdOrder.ColHidden = False
        sprdDelNotes.Text = oHeader.ISTOutNumber
        For lngLineNo = 1 To oHeader.Lines.Count Step 1
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.Col = COL_ITEMNO
            sprdOrder.Text = lngLineNo
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).PartCode
            Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.MaxRows, 0, True)
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).Quantity
            sprdOrder.BackColor = RGB_LTGREY
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).Quantity
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).Quantity
            sprdOrder.Col = COL_ORIGLINENO
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).LineNumber
            Call SetLineFormulas(sprdOrder.MaxRows)
            Call AddLine
        Next lngLineNo
        If mblnISTAddLines = False Then
            sprdOrder.MaxRows = sprdOrder.MaxRows - 1 'remove Auto-Add line
        Else
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.Col = COL_QTY
            sprdOrder.BackColor = RGB_LTGREY
            sprdOrder.Lock = True
        End If
    End If
    sprdOrder.Row = 1
    
    Set moEDIISTIn = oHeader 'preserve header for saving details against
    
End Sub

Private Sub LoadISTOut(strStoreNumber As String, strISTOutNumber As String)

Dim oHeader   As Object
Dim lngLineNo As Long
    
    Set oHeader = goDatabase.CreateBusinessObject(CLASSID_ISTOUTHEADER)
    
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTOUTHEADER_DocNo, strISTOutNumber)
    oHeader.GetLines = True
    If oHeader.IBo_Load Then
        cmdDelLine.Visible = False
        'Display sending store details
        cmbStore.SearchText = oHeader.StoreNumber
        cmbStore.SearchMethod = SearchMethodPartialMatch
        cmbStore.Action = ActionSearch
        cmbStore.ListIndex = cmbStore.SearchIndex
        Call cmbStore_KeyPress(13)
        
        Call SetTotalsWidth
        'Display Header details
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = 1
        sprdDelNotes.MaxRows = 1
        lblOrderDate.Caption = DisplayDate(oHeader.DelNoteDate, False)
        lblOrderDate.Visible = True
        lblOrderDatelbl.Visible = True
        lblOrderNo.Caption = oHeader.DocNo
        mstrNotes = oHeader.Narrative
        strMaintUser = oHeader.EnteredBy
        
        If cmbDispatchType.ListCount > 0 Then
            lblDispTypelbl.Visible = True
            cmbDispatchType.Visible = True
            cmbDispatchType.SearchText = oHeader.DespatchMethod
            cmbDispatchType.SearchMethod = SearchMethodPartialMatch
            cmbDispatchType.Action = ActionSearch
            cmbDispatchType.ListIndex = cmbDispatchType.SearchIndex
        End If
        
        For lngLineNo = 1 To oHeader.Lines.Count Step 1
            sprdOrder.Row = sprdOrder.MaxRows
            sprdOrder.Col = COL_ITEMNO
            sprdOrder.Text = lngLineNo
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).PartCode
            Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.MaxRows, 0, True)
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).OrderQty
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).OrderQty
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).OrderQty
            sprdOrder.Col = COL_ORIGLINENO
            sprdOrder.Text = oHeader.Lines(CLng(lngLineNo)).LineNo
            Call AddLine
        Next lngLineNo
        sprdOrder.MaxRows = sprdOrder.MaxRows - 1 'remove Auto-Add line
    End If
    Call UpdateTotals
'    mstrNotes = oHeader.Narrative
    If mstrNotes <> "" Then cmdViewNotes.Visible = True
    
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) And (TypeOf ActiveControl Is CommandButton) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyF1 Then 'if F1 then display Help List
        uchlHelpKeys.Visible = False
    End If

End Sub

Private Sub Form_Load()

Dim oFSO As FileSystemObject

    If Command = "" Then
        Call MsgBoxEx("This option can only be accessed from the Menu", vbCritical, "Order Function", , , , , RGBMsgBox_WarnColour)
        End
    End If
    
    ' Set up the object and field that we wish to select on
    Set goRoot = GetRoot
    
    Me.Show
    Call Me.Refresh
    
    On Error GoTo Error_FormLoad
    
    'Display initial values in Status Bar
    Call InitialiseStatusBar(sbStatus)
    
    ucpbProgress.Title = "Current Progress"
    
    ucfiItemSearch.ShowSelectActiveOnly = False
    ucfiItemSearch.ShowDepartment = False
    ucfiItemSearch.ShowSKU = False
    ucfiItemSearch.ShowSupplier = False
    ucfiItemSearch.ShowUse = False
       
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    mstrLogoFile = goSession.GetParameter(PRM_LOGOFILENAME)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    blnSuppItemsOnly = goSession.GetParameter(PRM_SUPP_ITEMS_ONLY)
    mlngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    mlngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    mblnDupItems = goSession.GetParameter(PRM_DUP_ITEMS_ALLOWED)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    
    If mstrLogoFile <> "" Then
        If Right$(App.Path, 1) <> "\" Then
            mstrLogoFile = App.Path & "\" & mstrLogoFile
        Else
            mstrLogoFile = App.Path & mstrLogoFile
        End If
        'Check that logo file exists before loading in Printing routines
        Set oFSO = New FileSystemObject
        If oFSO.FileExists(mstrLogoFile) = False Then
            Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Logo file missing - " & mstrLogoFile)
            mstrLogoFile = ""
        End If
        Set oFSO = Nothing
    End If
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Logo file is - '" & mstrLogoFile & "'")
    
    Call sprdTotal.SetActiveCell(0, 0)
    Call SetFormAction(goSession.ProgramParams)
    
    'If Purchase Order or Maintain Purchase Order then get High Qty and Value levels
    If (strActionCode = ACTION_PORDER) Or (strActionCode = ACTION_MAINTPO) Then
        mdblMaxQty = goSession.GetParameter(PRM_MAX_LINE_QTY)
        mdblMaxValue = goSession.GetParameter(PRM_MAX_LINE_VALUE)
    End If
    
    Call ConfigColumns
    
    Call SetQuantityPlaces
    
    DoEvents
    
    sprdOrder.Col = COL_SPARE
    sprdOrder.ColHidden = True
    sbStatus.Panels(PANEL_INFO).Text = "F1-Help"
    Call cmdVoid_Click
    
    Exit Sub
    
Error_FormLoad:

    If Err.Number = OASYS_ERR_PARAMETER_NOT_KNOWN Then
        Call Err.Report(MODULE_NAME, "Form_Load", 1, True, "Loading System", "Parameter required to configure system is missing" & vbCrLf & "System will continue.  Use parameter editor to correct fault")
        Call Err.Clear
        Resume Next
    End If
    
    Call Err.Report(MODULE_NAME, "Form_Load", 1, True, "Loading System", "Unexpected error occurred in system initialisation" & vbCrLf & "System will exit")
    End
    
End Sub

Private Sub DisplaySupplier(ByVal strSupplierCode As String, ByRef blnAnyCustomerOrders As Boolean)

Dim cSupplier As Object

    Set cSupplier = goDatabase.CreateBusinessObject(CLASSID_SUPPLIER)
    Call cSupplier.IBo_AddLoadFilter(CMP_EQUAL, FID_SUPPLIER_SupplierNo, strSupplierCode)
    
    'load only fields required to save time
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_SupplierNo)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_NAME)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_StoreAddressLine1)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_StoreAddressLine2)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_StoreAddressLine3)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_PostCode)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_ContactName)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_TelNumber)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_FaxNumber)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_LeadTimeDays)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_BackOrdersAllowed)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_ElectronicOrders)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_Category)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_ElectronicOrders)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_RestockCharge)
    Call cSupplier.IBo_AddLoadField(FID_SUPPLIER_RestockRate)
    
    If cSupplier.IBo_Load = False Then
        Exit Sub
    End If
    
    ucpbProgress.Caption1 = "Displaying Supplier Details"
    sprdAddress.Col = 1
    lblSuppNo.Caption = cSupplier.SupplierNo
    lblSuppName.Caption = cSupplier.Name
    sprdAddress.Row = 1
    sprdAddress.Text = cSupplier.StoreAddressLine1
    sprdAddress.Row = 2
    sprdAddress.Text = cSupplier.StoreAddressLine2
    sprdAddress.Row = 3
    sprdAddress.Text = cSupplier.StoreAddressLine3
    sprdAddress.Row = 4
    sprdAddress.Text = cSupplier.postcode
    
    'Display static supplier information
    lblEDIFlag.Visible = False
    lblContactName.Caption = cSupplier.ContactName
    lblPhoneNo.Caption = cSupplier.TelNumber
    lblFaxNo.Caption = cSupplier.FaxNumber
    lblLeadDays.Caption = cSupplier.LeadTimeDays
    If cSupplier.ElectronicOrders = True Then lblEDIFlag.Visible = True
    
    'Store Supplier information used later
    mblnAllowBOrder = cSupplier.BackOrdersAllowed
    mdblRestockRate = cSupplier.RestockRate
    mdblRestockCharge = cSupplier.RestockCharge
    
    dtxtDueDate.Text = DisplayDate(cSupplier.NextDeliveryDate, False)
    If (strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_MAINTRETURN) Then dtxtDueDate.Text = DisplayDate(Now(), False)
    
    strItemSuppNo = ""
    mblnWarehouse = False
    mblnUseAllItems = False
    mblnEDI = cSupplier.ElectronicOrders
    mstrEDICode = cSupplier.EDICode
    If (strActionCode <> ACTION_MAINTPO) And ((strActionCode <> ACTION_DELNOTE) And (mblnDNAddLines = False)) Then
        If (cSupplier.Category = "") Then 'if normal supplier then prompt to use selected items
            If (mblnEDI = False) And (blnSuppItemsOnly = False) Then 'only prompt is orders are not electronic and all items allowed
                If MsgBoxEx("Select items for this supplier only (Y/N)", vbYesNo + vbQuestion, Me.Caption, , , , , RGBMSGBox_PromptColour) = vbYes Then strItemSuppNo = strSupplierCode
            Else
                strItemSuppNo = strSupplierCode 'force only Supplier Items for electronic orders
            End If
        End If
    End If
    
    If (strActionCode <> ACTION_DELNOTE) Or (strActionCode <> ACTION_MAINTDN) Then
        strItemSuppNo = strSupplierCode
    End If
    
    'Check if supplier is actually a warehouse
    If cSupplier.Category = "W" Then
        mblnWarehouse = True
        ucfiItemSearch.WarehouseItemsOnly = True
    Else
        mblnWarehouse = False
        ucfiItemSearch.WarehouseItemsOnly = False
    End If
    
    'Check if supplier is allowed to use any item regardless of status
    If cSupplier.Category = "A" Then
        mblnUseAllItems = True
        ucfiItemSearch.ShowISTButton = True
    Else
        ucfiItemSearch.ShowISTButton = False
    End If
    
    If strActionCode = ACTION_PORDER Then 'check if any Customer orders to process for Supplier
        If cSupplier.NumberOfCustomerOrders > 0 Then
            If MsgBoxEx("Pending customer orders for this supplier" & vbCrLf & "Process now?", vbYesNo + vbQuestion, "Process customer orders", , , , , RGBMSGBox_PromptColour) = vbYes Then blnAnyCustomerOrders = True
        End If
    End If 'recording Purchase Orders
    
    'Check delivery notes to detemine if Back Order column should be shown
    If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
        If mblnAllowBOrder = True Then
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = False
        Else
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
        End If
    End If 'Recording delivery notes
    
    'Tidy up and show contact details
    Set cSupplier = Nothing
    fraAddress.Visible = True

End Sub
Private Sub ClearSupplier()
    
    sprdAddress.Col = 1
    lblSuppNo.Caption = ""
    lblSuppName.Caption = ""
    sprdAddress.Row = -1
    sprdAddress.Text = ""
    lblEDIFlag.Visible = False
    
    dtxtDueDate.Text = ""
    lblOrderDate.Caption = ""
    lblOrderNo.Caption = ""
    sprdDelNotes.Row = -1
    sprdDelNotes.Col = -1
    sprdDelNotes.Text = ""
    
    lblContactName.Caption = ""
    lblPhoneNo.Caption = ""
    lblFaxNo.Caption = ""
    lblLeadDays.Caption = ""
    mblnAllowBOrder = False
    
    strItemSuppNo = ""
    mblnWarehouse = False
    mdblRestockRate = 0
    mdblRestockCharge = 0

End Sub
'<CACH>****************************************************************************************
'* Sub:  DisplayOrderLines()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'*In/Out:oPOrder    Object.
'*In/Out:blnFillDelQuantity Boolean.
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DisplayOrderLines(oPOrder As Object, _
                              blnFillDelQuantity As Boolean, _
                              blnCheckPackSize As Boolean, _
                              oEDIDelNote As Object)

Dim lLineNo As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayOrderLines"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    ucpbProgress.Max = oPOrder.Lines.Count + 1
    ucpbProgress.Title = "Current Progress"
    ucpbProgress.Caption1 = "Retrieving Lines for Order"
    sprdOrder.ReDraw = False
    sprdTotal.ReDraw = False
    sprdOrder.MaxRows = ROW_FIRST
    DoEvents
    For lLineNo = 1 To oPOrder.Lines.Count Step 1
        ucpbProgress.Value = lLineNo
        sprdOrder.Row = sprdOrder.MaxRows
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).PartCode
        sprdOrder.Lock = True
        Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.Row, 0, True)
        sprdOrder.Col = COL_QTY
        If blnCheckPackSize = False Then sprdOrder.BackColor = RGB_LTGREY
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).OrderQuantity
        Call sprdOrder_EditMode(COL_QTY, sprdOrder.Row, 0, True)
        sprdOrder.Col = COL_QTY
        sprdOrder.Row = sprdOrder.MaxRows - 1
        If blnCheckPackSize = False Then sprdOrder.BackColor = RGB_WHITE
        
        If strActionCode = ACTION_DELNOTE Then 'lock Part code and Order quantity
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Lock = True
        End If
        
        sprdOrder.Col = COL_ORIGPOQTY
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).OrderQuantity
        
        sprdOrder.Col = COL_QTYOS
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).OrderQuantity - oPOrder.Lines(CLng(lLineNo)).QuantityReceived
        sprdOrder.Col = COL_SELLPRICE
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).OrderPrice
        sprdOrder.Col = COL_COST
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).CostPrice
        sprdOrder.Col = COL_QTYDEL
        If blnFillDelQuantity Then
            sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).OrderQuantity - oPOrder.Lines(CLng(lLineNo)).QuantityReceived
            If Val(sprdOrder.Text) < 0 Then sprdOrder.Text = 0
        Else
            sprdOrder.Text = 0
        End If
        sprdOrder.Col = COL_QTYSHORT
        sprdOrder.Text = 0
        sprdOrder.Col = COL_QTYRET
        sprdOrder.Text = 0
        
        'Added dslater mis-picks
        sprdOrder.Col = COL_QTYRET
        sprdOrder.Lock = False
        
        sprdOrder.Col = COL_QTYOVER
        sprdOrder.Text = 0
        'check to see if return qty must be unlocked
        If (blnFillDelQuantity = True) And ((oPOrder.Lines(CLng(lLineNo)).OrderQuantity - oPOrder.Lines(CLng(lLineNo)).QuantityReceived) > 0) Then sprdOrder.Lock = False
        sprdOrder.Col = COL_CUSTONO
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).CustomerOrderNo
        sprdOrder.Col = COL_ORIGLINENO
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).LineNo
        sprdOrder.Col = COL_QTYSELLIN
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).QuantityPerUnit
        sprdOrder.Col = COL_SUPPMANCODE
        sprdOrder.Text = oPOrder.Lines(CLng(lLineNo)).ManufacturerCode
    Next lLineNo
    Call UpdateTotals
    sprdOrder.ReDraw = True
    sprdTotal.ReDraw = True

    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, Erl)
    Exit Sub

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Sub 'DisplayOrderLines


'<CACH>****************************************************************************************
'* Sub:  DisplayDelNoteLines()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'*In/Out:oDelNote    Object.
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DisplayDelNoteLines(oDelNote As Object)

Dim lLineNo As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayDelNoteLines"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    ucpbProgress.Max = oDelNote.Lines.Count + 1
    ucpbProgress.Caption1 = "Retrieving Lines for Delivery Note"
    sprdOrder.ReDraw = False
    sprdTotal.ReDraw = False
    Call ResetGrid
    DoEvents
    For lLineNo = 1 To oDelNote.Lines.Count Step 1
        ucpbProgress.Value = lLineNo
        sprdOrder.Row = sprdOrder.MaxRows
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).PartCode
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Lock = True
        Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.Row, 0, True)
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Lock = True
        sprdOrder.Col = COL_QTY
        sprdOrder.Text = 1
        Call sprdOrder_EditMode(COL_QTY, sprdOrder.Row, 0, True)
        sprdOrder.Row = sprdOrder.MaxRows - 1
        sprdOrder.Col = COL_QTY
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).OrderQty
        sprdOrder.Lock = True
        sprdOrder.Col = COL_QTYOS
        sprdOrder.Text = 0
        If oDelNote.Lines(CLng(lLineNo)).POLineNumber > 0 Then
            sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).OrderQty - oDelNote.Lines(CLng(lLineNo)).ReceivedQty
        Else
            sprdOrder.Col = COL_QTY
            sprdOrder.BackColor = RGB_LTGREY
            sprdOrder.Text = "0"
            sprdOrder.Col = COL_QTYOS
            sprdOrder.BackColor = RGB_LTGREY
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.BackColor = RGB_LTGREY
        End If
        sprdOrder.Col = COL_SELLPRICE
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).OrderPrice
        sprdOrder.Col = COL_COST
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).CurrentCost
        sprdOrder.Col = COL_QTYDEL
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).ReceivedQty
        sprdOrder.Col = COL_QTYRET
        sprdOrder.Text = 0
        sprdOrder.Col = COL_COMMENT
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).Narrative
        sprdOrder.Col = COL_ORIGLINENO
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).POLineNumber
        sprdOrder.Col = COL_QTYSELLIN
        sprdOrder.Text = oDelNote.Lines(CLng(lLineNo)).SinglesSellingUnit
    Next lLineNo
    If mblnDNAddLines = False Then
        sprdOrder.MaxRows = sprdOrder.MaxRows - 1
    Else
        sprdOrder.Row = sprdOrder.MaxRows
        sprdOrder.Col = COL_QTY
        sprdOrder.BackColor = RGB_LTGREY
        sprdOrder.Col = COL_QTYOS
        sprdOrder.BackColor = RGB_LTGREY
        sprdOrder.Col = COL_QTYBORD
        sprdOrder.BackColor = RGB_LTGREY
    End If
    Call UpdateTotals
    sprdOrder.ReDraw = True
    sprdTotal.ReDraw = True

    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, Erl)
    Exit Sub

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Sub 'DisplayDelNoteLines


'<CACH>****************************************************************************************
'* Sub:  AppendReturnedLine()
'**********************************************************************************************
'* Description: Used when maintaining delivery notes, appends returned line details
'**********************************************************************************************
'* Parameters:
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub AppendReturnedLine(strPartCode As String, _
                               strRetLineNo As String, _
                               dblQuantity As Double, _
                               strReasonCode As String, _
                               strComment As String, _
                               strPOLineNumber As String, _
                               lngType As Long)
                               'blnShortages As Boolean)

Dim lngLineNum As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".AppendReturnedLine"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    For lngLineNum = ROW_FIRST To sprdOrder.MaxRows Step 1
        sprdOrder.Row = lngLineNum
        sprdOrder.Col = COL_PARTCODE
        If (sprdOrder.Text = strPartCode) Then
            sprdOrder.Col = COL_ORIGLINENO
            If (Val(sprdOrder.Text) = strPOLineNumber) Or (lngType = TYPE_OVER) Then
                If lngType = TYPE_RETURN Then
                    'Append Returned details to delivery note line
                    sprdOrder.Col = COL_QTYRET
                    sprdOrder.Text = dblQuantity
                    sprdOrder.Col = COL_RETCODE
                    sprdOrder.Lock = False
                    strReasonCode = Mid$(mstrRetCode, InStr(mstrRetCode, strReasonCode))
                    strReasonCode = Left$(strReasonCode, InStr(strReasonCode, vbTab) - 1)
                    sprdOrder.CellType = CellTypeComboBox
                    sprdOrder.TypeComboBoxList = mstrRetCode
                    sprdOrder.Lock = False
                    sprdOrder.Text = strReasonCode
                    sprdOrder.Col = COL_RETCMNT
                    sprdOrder.Text = strComment
                    sprdOrder.Lock = False
                    sprdOrder.Col = COL_RETLINENO
                    sprdOrder.Text = strRetLineNo
                End If
                If lngType = TYPE_SHORTAGE Then
                    'Append Returned details to delivery note line
                    sprdOrder.Col = COL_QTYSHORT
                    'Change colour for mis-picks
                    sprdOrder.ForeColor = vbRed
                    sprdOrder.Text = dblQuantity
                    sprdOrder.Col = COL_SHORTCODE
                    sprdOrder.Text = mstrDefShortageDesc
                    sprdOrder.Col = COL_SHORTLINENO
                    sprdOrder.Text = strRetLineNo
                End If
                If lngType = TYPE_OVER Then
                    'overs
                    'Append Returned details to delivery note line
                    sprdOrder.Col = COL_QTYSHORT
                    'Change colour for mis-picks
                    sprdOrder.ForeColor = vbBlue
                    sprdOrder.Text = dblQuantity
                    sprdOrder.Col = COL_OVERLINENO
                    sprdOrder.Text = strRetLineNo
                End If
            End If
        End If
    Next lngLineNum
    Call UpdateTotals
    sprdOrder.ReDraw = True
    sprdTotal.ReDraw = True

    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, Erl)
    Exit Sub

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Sub 'AppendReturnedLine


'<CACH>****************************************************************************************
'* Sub:  DisplayReturnNoteLines()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'*In/Out:oReturn    Object -Return note to display information from
'*In/Out:oOriginalDelNote Object - Delivery note to get Original Quantities from
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DisplayReturnNoteLines(oReturn As Object, oOriginalDelNote As Object)

Dim lngLineNo     As Long
Dim strRetCode    As String
Dim lngNoDelLines As Long
Dim lngDelLineNo  As Long

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayReturnNoteLines"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    ucpbProgress.Max = oReturn.Lines.Count + 1
    ucpbProgress.Caption1 = "Retrieving Lines for Supplier Return"
    sprdOrder.ReDraw = False
    sprdTotal.ReDraw = False
    sprdOrder.MaxRows = ROW_FIRST
    If ((oOriginalDelNote Is Nothing) = False) Then lngNoDelLines = oOriginalDelNote.Lines.Count
    DoEvents
    For lngLineNo = 1 To oReturn.Lines.Count Step 1
        ucpbProgress.Value = lngLineNo
        sprdOrder.Row = lngLineNo + 1
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).PartCode
        Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.Row, 0, True)
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Lock = True
        sprdOrder.Col = COL_QTY
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).ReturnedQty
        Call sprdOrder_EditMode(COL_QTY, sprdOrder.Row, 0, True)
        sprdOrder.Text = 1
        Call sprdOrder_EditMode(COL_QTY, sprdOrder.Row, 0, True)
        sprdOrder.Row = lngLineNo + 1
'        sprdOrder.Row = sprdOrder.MaxRows
        sprdOrder.Col = COL_QTY
        sprdOrder.Lock = True
        sprdOrder.Col = COL_SELLPRICE
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).OrderPrice
        sprdOrder.Col = COL_COST
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).CurrentCost
        If oReturn.ShortageNote = True Then
            sprdOrder.Col = COL_QTYSHORT
        Else
            sprdOrder.Col = COL_QTYRET
        End If
        sprdOrder.Lock = False
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).ReturnedQty
        sprdOrder.Col = COL_RETCODE
        If (mstrDefShortageCode <> oReturn.Lines(CLng(lngLineNo)).ReturnReasonCode) Then
            strRetCode = Mid$(mstrRetCode, InStr(mstrRetCode, oReturn.Lines(CLng(lngLineNo)).ReturnReasonCode))
            If InStr(strRetCode, vbTab) > 0 Then strRetCode = Left$(strRetCode, InStr(strRetCode, vbTab) - 1)
            sprdOrder.TypeComboBoxList = mstrRetCode
            sprdOrder.Lock = False
        Else
            strRetCode = mstrDefShortageDesc
            sprdOrder.CellType = CellTypeEdit
            sprdOrder.Lock = True
        End If
        sprdOrder.Text = strRetCode
        sprdOrder.Col = COL_RETCMNT
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).Narrative
        sprdOrder.Lock = False
        sprdOrder.Col = COL_RETLINENO
        sprdOrder.Text = oReturn.Lines(CLng(lngLineNo)).LineNo
        'Once line has been display - check in delivery note for matching SKU
        For lngDelLineNo = 1 To lngNoDelLines Step 1
            sprdOrder.Col = COL_PARTCODE
            If sprdOrder.Text = moDelNote.Lines(lngDelLineNo).PartCode Then
                sprdOrder.Col = COL_QTYDEL
                sprdOrder.Text = moDelNote.Lines(lngDelLineNo).ReceivedQty
                If moSuppRetNote.ShortageNote = True Then
                    sprdOrder.Col = COL_QTYRET
                    sprdOrder.Text = moDelNote.Lines(lngDelLineNo).ReturnQty
                Else
                    'Check if we have an over
                    If moDelNote.Lines(lngDelLineNo).OverQty > 0 Then
                        sprdOrder.Col = COL_QTYSHORT
                        sprdOrder.Text = moDelNote.Lines(lngDelLineNo).OverQty * -1
                    Else
                        sprdOrder.Col = COL_QTYSHORT
                        sprdOrder.Text = moDelNote.Lines(lngDelLineNo).ShortageQty
                    End If
                End If
                Exit For
            End If 'match found
        Next lngDelLineNo
            
    Next lngLineNo
    Call UpdateTotals
    sprdOrder.ReDraw = True
    sprdTotal.ReDraw = True

    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, Erl)
    Exit Sub

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Sub 'DisplayReturnNoteLines


Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)

End Sub

Private Sub SetFormAction(sCommandStr As String)

Dim cSuppliers      As Object
Dim lCodeNo         As Long
Dim blnShowLastSold As Boolean

On Error GoTo BadFormAction

    Call ResetGrid
    Call ShowTotal(COL_TOTQTYSHRT, False)
    Call ShowTotal(COL_TOTSHRTVAL, False)
    strActionCode = sCommandStr
    mblnUseObsItems = False
    Call DebugMsg(MODULE_NAME, "SetFormAction", endlDebug, sCommandStr)
    Select Case (strActionCode)
        Case (ACTION_PORDER):
            ucfiItemSearch.ShowISTButton = False
            Call uchlHelpKeys.AddHotKey("F2", "Item Fuzzy Match")
            Call uchlHelpKeys.AddHotKey("F3", "Select supplier")
            Call uchlHelpKeys.AddHotKey("F4", "Retrieve Customer Orders")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Purchase Order")
            Call uchlHelpKeys.AddHotKey("F8", "Delete Line")
            Call uchlHelpKeys.AddHotKey("F10", "Exit/Park Purchase Order")
            lngCutOffTime = goSession.GetParameter(PRM_CUTOFF_TIME)
            blnShowLastSold = goSession.GetParameter(PRM_SHOW_LASTSOLD)
            mlngPOAutoPrint = goSession.GetParameter(PRM_AUTO_PRINT_PO)
            mlngMaxParkedOrders = goSession.GetParameter(PRM_MAX_PARKED_ORDERS)
            mstrFunction = "Purchase Order"
            Me.Caption = "Raise Purchase Order"
            cmdClose.Caption = "F10-E&xit/Park"
            lblUserDate.Caption = "Due Date"
            lblUserDate.Visible = False
            dtxtDueDate.Visible = False
            lblOrderDatelbl.Caption = "Order Date"
            lblContactNamelbl.Visible = True
            lblContactName.Visible = True
            lblDelNoteNoLbl.Visible = False
            sprdDelNotes.Visible = False
            lblOrderNo.Visible = False
            lblOrderNolbl.Visible = False
            
            'Hide columns not used by Purchase Order
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            If blnShowLastSold = False Then
                sprdOrder.Col = COL_LASTSOLD
                sprdOrder.ColHidden = True
            End If
            cmdDelete.Visible = False
            cmdDelete.Caption = "Ctrl F8-Delete P/O"
            cmdRePrint.Visible = False
            DoEvents
            
            'Get List of Suppliers and populate User drop down list
            Set cSuppliers = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERS)
        
            Call cSuppliers.GetList
        
            'load up Supplier combo with Supplier List
            cmbSupplier.Clear
            Call cSuppliers.MoveFirst
            While Not (cSuppliers.EndOfList)
                Call cmbSupplier.AddItem(cSuppliers.SupplierNo & vbTab & cSuppliers.SupplierName & vbTab & Left$(cSuppliers.SupplierNo, 2) & Val(Mid$(cSuppliers.SupplierNo, 3)))
                cmbSupplier.Row = cmbSupplier.NewIndex
                Call cSuppliers.MoveNext
                If cSuppliers.EndOfList = True Then Call cmbSupplier.AddItem(cSuppliers.SupplierNo & vbTab & cSuppliers.SupplierName & vbTab & Left$(cSuppliers.SupplierNo, 2) & Val(Mid$(cSuppliers.SupplierNo, 3)))
            Wend
            
            Call chkStoresOnly_Click
        
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, True)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, True)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call ShowTotal(COL_TOTQTYRET, False)
            Call ShowTotal(COL_TOTRETVAL, False)
        
        Case (ACTION_MAINTPO):
            ucfiItemSearch.ShowISTButton = False
            Call ucofOrders.Initialise(goSession, Me)
            ucofOrders.CancelKey = 10
            
            Call uchlHelpKeys.AddHotKey("F3", "Purchase Order Search")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Purchase Order")
            Call uchlHelpKeys.AddHotKey("F10", "Exit Purchase Order")
            mlngPOAutoPrint = goSession.GetParameter(PRM_AUTO_PRINT_PO)
            mstrFunction = "Purchase Order - Maintain Delivery Date"
            Me.Caption = "Maintain Purchase Order Delivery Date"
            fraSelection.Caption = "Supplier Details"
            lblUserDate.Caption = "Due Date"
            lblOrderDatelbl.Caption = "Order Date"
            cmdSave.Caption = "F5-Update"
            lblContactNamelbl.Visible = True
            lblContactName.Visible = True
            lblDelNoteNoLbl.Visible = False
            sprdDelNotes.Visible = False
            lblOrderNo.Visible = True
            lblOrderNolbl.Visible = True
            cmdViewParked.Visible = False
            cmdRePrint.Visible = True
            cmdDelLine.Visible = False
            cmdDelete.Visible = True
            cmdDelete.Caption = "Ctrl F8-Delete P/O"
            
            'Hide columns not used by Purchase Order Maintenance
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = True
            
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_FREE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_ONHAND
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_SELLPRICE
            sprdOrder.ColHidden = True
'            sprdOrder.Col = COL_PACKQTY
'            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYSELLIN
            sprdOrder.ColHidden = True
                    
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, True)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, True)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call ShowTotal(COL_TOTQTYRET, False)
            Call ShowTotal(COL_TOTRETVAL, False)
            
            sprdOrder.Col = -1
            sprdOrder.Row = -1
            sprdOrder.Lock = True
            chkStoresOnly.Visible = False
            
        Case (ACTION_DELNOTE):
            ucfiItemSearch.ShowISTButton = False
            Call ucofOrders.Initialise(goSession, Me)
            ucofOrders.CancelKey = 10
            
            Call uchlHelpKeys.AddHotKey("F2", "Fuzzy matching")
            Call uchlHelpKeys.AddHotKey("F3", "Locate Purchase Order")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Delivery Note")
            Call uchlHelpKeys.AddHotKey("F10", "Exit Delivery Note")
            blnAutoConsignDN = goSession.GetParameter(PRM_AUTO_CONSIGN_DN)
            mblnDNAddLines = goSession.GetParameter(PRM_DN_ADD_LINES)
            mlngDNAutoPrint = goSession.GetParameter(PRM_AUTO_PRINT_DN)
            mblnBookExcept = goSession.GetParameter(PRM_BOOK_EXCEPT)
            mstrDispMsg = goSession.GetParameter(PRM_DN_DISP_MSG)
            mstrFunction = "Delivery Note"
            Me.Caption = "Record Delivery Note"
            fraSelection.Caption = "Supplier Details"
            lblUserDate.Caption = "Receipt Date"
            lblOrderDatelbl.Caption = "Order Date"
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            cmdDelLine.Visible = False
            cmdClose.Visible = False 'replaced by Order selection close button
            lblLeadDayslbl.Visible = False
            lblLeadDays.Visible = False
            lblMCPMinlbl.Visible = False
            lblMCPType.Visible = False
            lblMCPMin.Visible = False
            lblContactNamelbl.Visible = False
            lblContactName.Visible = False
            lblDelNoteNoLbl.Visible = True
            sprdDelNotes.Visible = True
            lblOrderNo.Visible = True
            lblOrderNolbl.Visible = True
            cmdDelete.Visible = False
            cmdRePrint.Visible = False
            
            mblnDupItems = False
            
            sprdOrder.Col = COL_QTYSHORT
            sprdOrder.Row = 0
            sprdOrder.Text = "Short \ Over"
            
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.Row = 0
            sprdOrder.Text = "Quantity On Delivery Note"
            sprdOrder.Col = COL_QTYSHORT
            sprdOrder.ColHidden = False
            sprdOrder.ColWidth(COL_QTYSHORT) = 10
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCMNT
            sprdOrder.ColHidden = False
            sprdOrder.ColWidth(COL_RETCMNT) = 15
            sprdOrder.Col = COL_ONHAND
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_FREE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            
            Call FillInReturnCodes
            
            Call FillInMispickReasonCodes
            
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, True)
            Call ShowTotal(COL_TOTQTYDEL, True)
            Call ShowTotal(COL_TOTQTYRET, True)
            Call ShowTotal(COL_TOTRETVAL, False)
        
        Case (ACTION_MAINTDN):
            ucfiItemSearch.ShowISTButton = False
            Call uchlHelpKeys.AddHotKey("F2", "Fuzzy matching")
            Call uchlHelpKeys.AddHotKey("F3", "Locate Delivery Note")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Delivery Note")
            Call uchlHelpKeys.AddHotKey("Alt-F8", "Delete Delivery Note")
            Call uchlHelpKeys.AddHotKey("F8", "Delete Line")
            Call uchlHelpKeys.AddHotKey("F9", "Re-print Delivery Note")
            Call uchlHelpKeys.AddHotKey("F10", "Exit Maintain Delivery Note")
            blnAutoConsignDN = goSession.GetParameter(PRM_AUTO_CONSIGN_DN)
            mblnDNAddLines = goSession.GetParameter(PRM_DN_ADD_LINES)
            mlngDNAutoPrint = goSession.GetParameter(PRM_AUTO_PRINT_DN)
            mblnBookExcept = goSession.GetParameter(PRM_BOOK_EXCEPT)
            mstrDispMsg = goSession.GetParameter(PRM_DN_DISP_MSG)
            mstrFunction = "Maintain Delivery Notes"
            Me.Caption = "Maintain Delivery Note"
            fraSelection.Caption = "Supplier Details"
            lblUserDate.Caption = "Receipt Date"
            lblOrderDatelbl.Caption = "Return No"
            lblOrderNolbl.Caption = "DRL No"
            lblOrderDate.Visible = False
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            cmdDelLine.Visible = False
            lblLeadDayslbl.Visible = False
            lblLeadDays.Visible = False
            lblMCPMinlbl.Visible = False
            lblMCPType.Visible = False
            lblMCPMin.Visible = False
            lblContactNamelbl.Visible = False
            lblContactName.Visible = False
            lblDelNoteNoLbl.Visible = True
            sprdDelNotes.Visible = True
            dtxtDueDate.Locked = True
            lblOrderNo.Visible = True
            lblOrderNolbl.Visible = True
            cmdDelete.Visible = False
            cmdRePrint.Visible = False
            
            mblnDupItems = False
            
            sprdOrder.Col = COL_QTYSHORT
            sprdOrder.Row = 0
            sprdOrder.Text = "Short \ Over"
            
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.Row = 0
            sprdOrder.Text = "Quantity on Delivery Note"
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCMNT
            sprdOrder.ColHidden = False
            sprdOrder.ColWidth(COL_RETCMNT) = 10
            sprdOrder.Col = COL_QTYSHORT
            sprdOrder.ColHidden = False
            sprdOrder.ColWidth(COL_QTYSHORT) = 10
            sprdOrder.Col = COL_ONHAND
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_FREE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            Call ucdnDelNotes.Initialise(goSession, Me)
            ucdnDelNotes.CancelKey = 10 'force key to be F10
            ucdnDelNotes.Visible = True
            
            Call FillInReturnCodes
            
            Call FillInMispickReasonCodes
            
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, True)
            Call ShowTotal(COL_TOTQTYDEL, True)
            Call ShowTotal(COL_TOTQTYRET, True)
            Call ShowTotal(COL_TOTRETVAL, False)
        
        Case (ACTION_ISTOUT):
            cmdDelete.Visible = False
            cmdRePrint.Visible = False
            mblnUseObsItems = True
            ucfiItemSearch.ShowISTButton = True
            Call uchlHelpKeys.AddHotKey("F2", "Item Fuzzy Match")
            Call uchlHelpKeys.AddHotKey("F3", "Select Store")
            Call uchlHelpKeys.AddHotKey("F5", "Complete IST Out")
            Call uchlHelpKeys.AddHotKey("F10", "Exit IST Out")
            
            mlngISTOutPrint = goSession.GetParameter(PRM_AUTO_PRINT_ISTOUT)
            mstrFunction = "Raise IST Out"
            Me.Caption = "Raise IST Out"
            lblUserDate.Visible = False
            lblOrderDatelbl.Caption = "IST Date"
            dtxtDueDate.Visible = False
            lblContactNamelbl.Visible = True
            lblContactName.Visible = True
            lblDelNoteNoLbl.Visible = False
            Call FillInDispatchCodes
            lblDispTypelbl.Visible = True
            cmbDispatchType.Visible = True
            sprdDelNotes.Visible = False
            lblOrderNo.Visible = False
            lblOrderNolbl.Visible = False
            fraSelection.Caption = "Raise IST Out"
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            
            'Hide columns not used by IST
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_CUSTONO
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTY
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MANUCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.Row = 0
            sprdOrder.Text = "Delivery Quantity"
                        
            chkStoresOnly.Value = 1
            Call chkStoresOnly_Click
        
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call ShowTotal(COL_TOTQTYRET, False)
            Call ShowTotal(COL_TOTRETVAL, False)
        
        Case (ACTION_MAINTISTOUT):
            Call ucioLookup.Initialise(goSession, Me)
            mblnUseObsItems = True
            ucioLookup.CancelKey = 10
            cmdDelete.Visible = True
            cmdRePrint.Visible = True
            Call uchlHelpKeys.AddHotKey("F3", "Select IST Out")
            Call uchlHelpKeys.AddHotKey("Alt-F8", "Delete IST Out")
            Call uchlHelpKeys.AddHotKey("F9", "Reprint IST Out")
            Call uchlHelpKeys.AddHotKey("F10", "Exit IST Out")
            mstrFunction = "Delete IST Out"
            Me.Caption = "Delete IST Out"
            lblUserDate.Caption = "Due Date"
            lblOrderDatelbl.Caption = "IST Date"
            lblContactNamelbl.Visible = True
            lblContactName.Visible = True
            lblDelNoteNoLbl.Visible = False
            lblUserDate.Visible = False
            sprdDelNotes.Visible = False
            lblDispTypelbl.Visible = True
            
            Call FillInDispatchCodes
            cmbDispatchType.Visible = True
            cmbDispatchType.Enabled = False
            lblOrderNo.Visible = True
            lblOrderNolbl.Visible = True
            lblOrderNolbl.Caption = "IST Number"
            fraSelection.Caption = "Delete IST Out"
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            cmdDelete.Caption = "Ctrl F8-Delete IST"
                        
            'Hide columns not used by IST
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_CUSTONO
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTY
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MANUCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.Row = 0
            sprdOrder.Text = "Delivery Quantity"
            DoEvents
                        
            chkStoresOnly.Value = 1
            Call chkStoresOnly_Click
        
            Call ShowTotal(COL_TOTQTY, False)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, True)
            Call ShowTotal(COL_TOTQTYRET, False)
            Call ShowTotal(COL_TOTRETVAL, False)
            
        Case (ACTION_ISTIN):
            cmdDelete.Visible = False
            cmdRePrint.Visible = False
            mblnUseObsItems = True
            Call uchlHelpKeys.AddHotKey("F2", "Item Fuzzy Match")
            Call uchlHelpKeys.AddHotKey("F3", "Select Store")
            Call uchlHelpKeys.AddHotKey("F4", "Retrieve Pending IST's")
            Call uchlHelpKeys.AddHotKey("F5", "Complete IST In")
            Call uchlHelpKeys.AddHotKey("F10", "Exit IST In")
            mblnISTAddLines = goSession.GetParameter(PRM_IST_ADD_LINES)
            mstrDispMsg = goSession.GetParameter(PRM_ISTIN_DISP_MSG)
            mstrFunction = "Record IST In"
            Me.Caption = "Record IST In"
            lblUserDate.Visible = False
            dtxtDueDate.Visible = False
            lblUserDate.Caption = "Receipt Date"
            lblOrderDatelbl.Visible = False
            lblOrderDatelbl.Caption = "IST Out Date"
            lblOrderDate.Visible = False
            lblContactNamelbl.Visible = True
            lblContactName.Visible = True
            lblDelNoteNoLbl.Visible = True
            lblDelNoteNoLbl.Caption = "IST No"
            sprdDelNotes.Visible = True
            lblOrderNo.Visible = False
            lblOrderNolbl.Visible = False
            fraSelection.Caption = "Record IST In"
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            
            'Hide columns not used by IST
            sprdOrder.Col = COL_FREE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_ONHAND
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_CUSTONO
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTY
            sprdOrder.ColHidden = True
            sprdOrder.Row = 0
            sprdOrder.Text = "Despatched Quantity"
'            sprdOrder.Col = COL_QTYRET
'            sprdOrder.ColHidden = False
'            sprdOrder.Col = COL_RETCODE
'            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MANUCODE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            DoEvents
                        
            chkStoresOnly.Value = 1
            Call chkStoresOnly_Click
        
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call ShowTotal(COL_TOTQTYRET, False)
            Call ShowTotal(COL_TOTRETVAL, False)
            
        Case (ACTION_CONSIGN):
            Call uchlHelpKeys.AddHotKey("F2", "Item Fuzzy Match")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Consignment")
            Call uchlHelpKeys.AddHotKey("F10", "Exit Consignment")
            mstrFunction = "Consignment Entry"
            Me.Caption = "Record Consignment"
            fraSelection.Caption = "Supplier Details"
            cmdViewParked.Visible = False
            cmdDelLine.Visible = False
            lblLeadDayslbl.Visible = False
            lblLeadDays.Visible = False
            lblMCPMinlbl.Visible = False
            lblMCPType.Visible = False
            lblMCPMin.Visible = False
            lblContactNamelbl.Visible = False
            lblContactName.Visible = False
            lblDelNoteNoLbl.Visible = True
            sprdDelNotes.Visible = True
            dtxtDueDate.Locked = True
            lblOrderNo.Visible = True
            lblOrderNolbl.Visible = True
            
            sprdOrder.Col = COL_PARTCODE
            sprdOrder.Lock = True
            sprdOrder.Col = COL_QTY
            sprdOrder.Lock = True
            sprdOrder.Col = COL_ONHAND
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            ucofOrders.Visible = True
            Call ucofOrders.Initialise(goSession, Me)
            
            Call ShowTotal(COL_TOTQTY, True)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, True)
            Call ShowTotal(COL_TOTQTYDEL, True)
            Call ShowTotal(COL_TOTQTYRET, True)
            Call ShowTotal(COL_TOTRETVAL, True)
            Call uchlHelpKeys.AddHotKey("F3", "Purchase Order Search")
        
        Case (ACTION_RETURN):
            Call uchlHelpKeys.AddHotKey("F2", "Item Fuzzy Match")
            Call uchlHelpKeys.AddHotKey("F3", "Select supplier")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Supplier Return")
            Call uchlHelpKeys.AddHotKey("F10", "Exit Supplier Return")
            mblnUseObsItems = True
            mblnSRColNoteReq = goSession.GetParameter(PRM_SUPP_RET_COL_NOTE_REQ)
            mlngSRAutoPrint = goSession.GetParameter(PRM_AUTO_PRINT_SR)
            mstrFunction = "Supplier Return"
            Me.Caption = "Raise Supplier Return"
            fraSelection.Caption = "Supplier Details"
            lblUserDate.Caption = "Collect Date"
            lblOrderDatelbl.Caption = "Return Date"
            lblReferencelbl.Caption = "Orig D.Note(s)"
            sprdDelNotes.Height = txtReference.Height
            sprdDelNotes.ScrollBars = ScrollBarsNone
            dtxtDueDate.Enabled = False 'disable Collection Note Date until Note No entered
            lblReferencelbl.Visible = True
            txtReference.Visible = True
            lblContactNamelbl.Visible = False
            lblContactName.Visible = False
            lblDelNoteNoLbl.Visible = True
            sprdDelNotes.Visible = True
            lblOrderNo.Visible = False
            lblOrderNolbl.Visible = False
            lblLeadDayslbl.Visible = False
            lblLeadDays.Visible = False
            lblMCPMinlbl.Visible = False
            lblMCPType.Visible = False
            lblMCPMin.Visible = False
            lblDelNoteNoLbl.Caption = "Col Note No"
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            
            'Hide columns not used by Supplier Returns
            sprdOrder.Col = COL_QTY
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_CUSTONO
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_RETCMNT
            sprdOrder.ColHidden = False
            sprdOrder.ColWidth(COL_RETCMNT) = 15
            
            cmbSupplier.Visible = True
            cmdDelete.Visible = False
            cmdRePrint.Visible = False
            DoEvents
            
            Call FillInReturnCodes
            
            'Get List of Suppliers and populate User drop down list
            Set cSuppliers = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERS)
        
            Call cSuppliers.GetList
        
            'load up Supplier combo with Supplier List
            cmbSupplier.Clear
            Call cSuppliers.MoveFirst
            While Not (cSuppliers.EndOfList)
                Call cmbSupplier.AddItem(cSuppliers.SupplierNo & vbTab & cSuppliers.SupplierName & vbTab & Left$(cSuppliers.SupplierNo, 2) & Val(Mid$(cSuppliers.SupplierNo, 3)))
                cmbSupplier.Row = cmbSupplier.NewIndex
                Call cSuppliers.MoveNext
            Wend
            
            Call ShowTotal(COL_TOTQTY, False)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call ShowTotal(COL_TOTQTYRET, True)
            Call ShowTotal(COL_TOTRETVAL, True)
        
        Case (ACTION_MAINTRETURN):
            Call uchlHelpKeys.AddHotKey("F2", "Item Fuzzy Match")
            Call uchlHelpKeys.AddHotKey("F3", "Select Return Note")
            Call uchlHelpKeys.AddHotKey("F5", "Complete Supplier Return")
            Call uchlHelpKeys.AddHotKey("F10", "Exit Supplier Return")
            mblnSRColNoteReq = goSession.GetParameter(PRM_SUPP_RET_COL_NOTE_REQ)
            mstrFunction = "Maintain Supplier Returns"
            Me.Caption = "Maintain Supplier Returns"
            mblnUseObsItems = True
            fraSelection.Caption = "Supplier Details"
            lblUserDate.Caption = "Collect Date"
            lblOrderDatelbl.Caption = "Request Date"
            lblContactNamelbl.Visible = False
            lblContactName.Visible = False
            lblDelNoteNoLbl.Visible = True
            
            lblReferencelbl.Caption = "Orig D.Note(s)"
            lblReferencelbl.Visible = True
            sprdDelNotes.Height = txtReference.Height
            sprdDelNotes.ScrollBars = ScrollBarsNone
            txtReference.Visible = True
            
            'Used to display origianl DRL that Return may apply to
            lblMCPMinlbl.Caption = "D.Note DRL"
            lblMCPMinlbl.Visible = True
            lblMCPMin.Caption = ""
            lblMCPMin.Visible = True
            
            sprdDelNotes.Visible = True
            lblOrderNo.Visible = False
            lblLeadDayslbl.Visible = False
            lblLeadDays.Visible = False
            lblMCPType.Visible = False
            lblDelNoteNoLbl.Caption = "Col Note No"
            lblRetRequestID.Visible = True
            lblOrderNolbl.Visible = True
            lblOrderNolbl.Caption = "Request No"
            chkStoresOnly.Visible = False
            cmdViewParked.Visible = False
            
            'Hide columns not used by Supplier Returns
            sprdOrder.Col = COL_QTY
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_PACKQTY
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_FREE
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_ONHAND
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MINLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_MAXLEVEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTSOLD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_LASTORDER
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_CUSTONO
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYBORD
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYOS
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_COMMENT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYDEL
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_RETCODE
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_RETCMNT
            sprdOrder.ColHidden = False
            sprdOrder.ColWidth(COL_RETCMNT) = 15
            
            cmdDelete.Visible = False
            cmdDelLine.Visible = False
            cmdRePrint.Visible = True
            
            Call ucsrReturns.Initialise(goSession, Me)
            ucsrReturns.CancelKey = 10
            ucsrReturns.Visible = True
            
            DoEvents
            
            Call FillInReturnCodes
            
            Call ShowTotal(COL_TOTQTY, False)
            Call ShowTotal(COL_TOTWGHT, False)
            Call ShowTotal(COL_TOTSPARE, False)
            Call ShowTotal(COL_TOTVAL, False)
            Call ShowTotal(COL_TOTQTYOS, False)
            Call ShowTotal(COL_TOTQTYDEL, False)
            Call ShowTotal(COL_TOTQTYRET, True)
            Call ShowTotal(COL_TOTRETVAL, True)
    End Select
    Call uchlHelpKeys.AddHotKey("F11", "Item Enquiry on Line")
    
    Call SetTotalsWidth
    
    Exit Sub
    
BadFormAction:

    If Err.Number = OASYS_ERR_PARAMETER_NOT_KNOWN Then
        Call Err.Report(MODULE_NAME, "SetFormAction", 1, True, "Configuring System", "Parameter required to configure system is missing" & vbCrLf & "System will continue.  Use parameter editor to correct fault")
        Call Err.Clear
        Resume Next
    End If
    
    Call Err.Bubble(MODULE_NAME, "SetFormAction", 1)

End Sub

Private Sub FillInReturnCodes()

Dim cRetCode    As Object
Dim colRetCode  As Collection
Dim lngCodeNo   As Long

    'Get List of Return option codes and store is mstrRetCode for later use
    Set cRetCode = goDatabase.CreateBusinessObject(CLASSID_RETURNCODE)
    If (strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_DELNOTE) Then Call cRetCode.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCODE_NormalSupplierReturn, False)

    Set colRetCode = cRetCode.IBo_Loadmatches
    
    mstrDefShortageCode = goSession.GetParameter(PRM_DEF_SHORTAGE_CODE)
    
    'load up mstrRetCode with Code-Description
    mstrRetCode = ""
    For lngCodeNo = 1 To colRetCode.Count Step 1
        If colRetCode(CLng(lngCodeNo)).Code = mstrDefShortageCode Then
            mstrDefShortageDesc = colRetCode(CLng(lngCodeNo)).Code & "-" & colRetCode(CLng(lngCodeNo)).Description & vbTab
        Else
            mstrRetCode = mstrRetCode & colRetCode(CLng(lngCodeNo)).Code & "-" & colRetCode(CLng(lngCodeNo)).Description & vbTab
        End If
    Next lngCodeNo

End Sub

Private Sub FillInMispickReasonCodes()

Dim cRetCode    As Object
Dim colRetCode  As Collection
Dim lngCodeNo   As Long
        
    On Error Resume Next
    mstrMispickCode = ""
    
    'Get List of Return option codes and store is mstrRetCode for later use
    Set cRetCode = goDatabase.CreateBusinessObject(CLASSID_RETURNCODE)

    'Add Filters
    Call cRetCode.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCODE_Code, "03")
    Call cRetCode.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCODE_NormalSupplierReturn, False)
        
    Set colRetCode = cRetCode.IBo_Loadmatches
       
    mstrMispickCode = mstrMispickCode & colRetCode(1).Code & "-" & colRetCode(1).Description & vbTab
    
    'Get List of Return option codes and store is mstrRetCode for later use
    Set cRetCode = goDatabase.CreateBusinessObject(CLASSID_RETURNCODE)

    'Add Filters
    Call cRetCode.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCODE_Code, "05")
    Call cRetCode.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNCODE_NormalSupplierReturn, False)
    
    Set colRetCode = cRetCode.IBo_Loadmatches
       
    mstrMispickCode = mstrMispickCode & colRetCode(1).Code & "-" & colRetCode(1).Description & vbTab

End Sub

Private Sub FillInDispatchCodes()

Dim cDespType    As Object
Dim colDespType  As Collection
Dim lngCodeNo    As Long

    'Get List of Despatch Methods Code and store is mstrDespCode for later use
    Set cDespType = goDatabase.CreateBusinessObject(CLASSID_DESPATCHMETHOD)
    Call cDespType.IBo_AddLoadFilter(CMP_EQUAL, FID_DESPATCHMETHOD_Active, True)

    Set colDespType = cDespType.IBo_Loadmatches
    
    'load up mstrDespCode with Code-Description
    Call cmbDispatchType.Clear
    For lngCodeNo = 1 To colDespType.Count Step 1
        cmbDispatchType.AddItem (colDespType(CLng(lngCodeNo)).ID & vbTab & colDespType(CLng(lngCodeNo)).Description & vbTab & colDespType(CLng(lngCodeNo)).ReferenceRequired)
        Debug.Print colDespType(CLng(lngCodeNo)).ID & vbTab & colDespType(CLng(lngCodeNo)).Description & vbTab & colDespType(CLng(lngCodeNo)).ReferenceRequired
    Next lngCodeNo

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

Dim lngRespKey As Long
Dim blnPrint   As Boolean

    sprdTotal.Row = 2
    Select Case (strActionCode)
        Case (ACTION_PORDER):
            sprdTotal.Col = COL_TOTQTY
            If Val(sprdTotal.Text) > 0 Then 'save order as line items entered
                lngRespKey = MsgBoxEx("Detail lines captured on " & mstrFunction & vbCrLf & "Save " & mstrFunction & " for later processing", vbYesNoCancel + vbQuestion, "Exit " & mstrFunction & " Entry", , , , , RGBMSGBox_PromptColour)
                If lngRespKey = vbCancel Then
                    sprdOrder.SetFocus
                    Cancel = True
                    Exit Sub
                End If
                If lngRespKey = vbYes Then
                    blnPrint = False
                    If MsgBoxEx("Print parked order now?", vbYesNo + vbQuestion, "Print Parked Order", , , , , RGBMSGBox_PromptColour) = vbYes Then blnPrint = True
                    Call SaveOrder(True, blnPrint)
                    If blnPrint = True Then Call PrintPurchaseOrder(False, True)
                End If
            Else 'check that Parked order has not had lines deleted
                If Val(lblOrderID.Caption) <> 0 Then Call cmdDelete_Click
            End If
        Case (ACTION_DELNOTE):
            sprdTotal.Col = COL_TOTQTYDEL
            If (Val(sprdTotal.Text) > 0) And (sprdOrder.Visible = True) Then
                lngRespKey = MsgBoxEx("Delivery quantities captured " & vbCrLf & "Return to save " & mstrFunction & " ", vbYesNoCancel + vbQuestion, "Exit " & mstrFunction & " Entry", , , , , RGBMSGBox_PromptColour)
                If (lngRespKey = vbCancel) Or (lngRespKey = vbYes) Then
                    sprdOrder.SetFocus
                    Cancel = True
                    Exit Sub
                End If
            End If
        Case (ACTION_ISTOUT):
            sprdTotal.Col = COL_TOTQTY
            If Val(sprdTotal.Text) > 0 Then
                lngRespKey = MsgBoxEx("Dispatch quantities captured" & vbCrLf & "Return to save " & mstrFunction & " ", vbYesNoCancel + vbQuestion, "Exit " & mstrFunction & " Entry", , , , , RGBMSGBox_PromptColour)
                If (lngRespKey = vbCancel) Or (lngRespKey = vbYes) Then
                    sprdOrder.SetFocus
                    Cancel = True
                    Exit Sub
                End If
            End If
    End Select
    DoEvents
    If Cancel = False Then End

End Sub

Private Sub sprdDelNotes_GotFocus()

    sprdDelNotes.Row = -1
    sprdDelNotes.BackColor = RGBEdit_Colour
    sprdDelNotes.Row = 1
    sbStatus.Panels(PANEL_INFO).Text = "Enter Delivery Notes"

End Sub

Private Sub sprdDelNotes_KeyDown(KeyCode As Integer, Shift As Integer)

    'check if Down Arrow pressed then add line to delivery notes to enter upto 9
    Call DebugMsg("key=", KeyCode, endlDebug)
    If (KeyCode = 40) And ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) Then
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = sprdDelNotes.ActiveRow
        If sprdDelNotes.Row = -1 Then sprdDelNotes.Row = sprdDelNotes.MaxRows
        If (sprdDelNotes.Text <> "") And (sprdDelNotes.Row = sprdDelNotes.MaxRows) Then
            If sprdDelNotes.MaxRows < 9 Then
                sprdDelNotes.MaxRows = sprdDelNotes.MaxRows + 1
                sprdDelNotes.Row = sprdDelNotes.MaxRows
                sprdDelNotes.EditMode = True
            End If
        End If
        KeyCode = 0
    End If

End Sub

Private Sub sprdDelNotes_KeyPress(KeyAscii As Integer)
        
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        DoEvents
        If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
            sprdDelNotes.Col = 1
            sprdDelNotes.Row = sprdDelNotes.ActiveRow
            If sprdDelNotes.Row = -1 Then sprdDelNotes.Row = sprdDelNotes.MaxRows
            If (sprdDelNotes.Text <> "") And (sprdDelNotes.Row = sprdDelNotes.MaxRows) Then
                If sprdDelNotes.MaxRows < 9 Then
                    sprdDelNotes.MaxRows = sprdDelNotes.MaxRows + 1
                    sprdDelNotes.Row = sprdDelNotes.MaxRows
                    Call sprdDelNotes.SetActiveCell(1, sprdDelNotes.MaxRows)
                    sprdDelNotes.EditMode = True
                Else 'can not add anymore than 9 so move to next field
                    Call SendKeys(vbTab)
                End If
            Else 'no more spaces - so move
                If (sprdDelNotes.Text <> "") And (sprdDelNotes.Row < sprdDelNotes.MaxRows) Then
                    sprdDelNotes.Row = sprdDelNotes.Row + 1
                    Call sprdDelNotes.SetActiveCell(1, sprdDelNotes.Row)
                    sprdDelNotes.EditMode = True
                Else
                    Call SendKeys(vbTab)
                End If
            End If
        Else 'only 1 delivery note can be entered - so move on
            If (strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_MAINTRETURN) Then
                Call sprdDelNotes_LostFocus
            End If
            Call SendKeys(vbTab)
            Exit Sub
        End If
    End If
    If (KeyAscii = vbKeyEscape) And ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) Then
        KeyAscii = 0
        If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
            sprdDelNotes.Col = 1
            sprdDelNotes.Row = sprdDelNotes.ActiveRow
            If sprdDelNotes.Row > 1 Then
                sprdDelNotes.Row = sprdDelNotes.Row - 1
                Call sprdDelNotes.SetActiveCell(1, sprdDelNotes.Row)
                sprdDelNotes.EditMode = True
            End If
        End If
    End If

End Sub

Private Sub sprdDelNotes_LostFocus()
    
    sbStatus.Panels(PANEL_INFO).Text = DEFAULT_MSG
    sprdDelNotes.Row = -1
    sprdDelNotes.Col = 1
    sprdDelNotes.BackColor = RGB_WHITE
    DoEvents
    If (sprdDelNotes.Visible = True) And (sprdDelNotes.Enabled = True) And (fraSelection.Visible = True) And ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) Then
        sprdDelNotes.Row = 1
        If sprdDelNotes.Text = "" Then Call MsgBoxEx("Supplier delivery note number(s) not entered", vbExclamation, "Capture delivery note", , , , , RGBMsgBox_WarnColour)
    End If
    If (sprdDelNotes.Visible = True) And (sprdDelNotes.Enabled = True) And (fraSelection.Visible = True) And ((strActionCode = ACTION_MAINTRETURN) Or (strActionCode = ACTION_RETURN)) Then
        sprdDelNotes.Row = 1
        If sprdDelNotes.Text = "" Then
            dtxtDueDate.Enabled = False
            dtxtDueDate.Text = ""
        Else
            dtxtDueDate.Enabled = True
            If dtxtDueDate.Text = "" Then dtxtDueDate.Text = lblOrderDate.Caption
'            Call dtxtDueDate.SetFocus
        End If
    End If


End Sub

Private Sub sprdOrder_ComboCloseUp(ByVal Col As Long, ByVal Row As Long, ByVal SelChange As Integer)

    Call DebugMsg(MODULE_NAME, "CCUp", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",SelChange=" & SelChange)

End Sub

Private Sub sprdOrder_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

Dim lngLineNo    As Long
Dim lngColNo     As Long 'used for moving to next available column to edit
Dim strPartCode  As String
Dim strDesc      As String
Dim dblOrderQty  As Double 'used to hold quatity order in delivered qty checking
Dim dblQtyOS     As Double
Dim dblQtyDel    As Double 'used for back orders
Dim dblNewQty    As Double 'used to compare entered value agaianst Pack Size
Dim dblOrigPOQty As Double 'used to compare EDI Orders to ensure qty's no increased
Dim blnMoveLine  As Boolean
Dim blnDropDown  As Boolean 'used to force combo's to drop down list
Dim strQtyHigh   As String 'used for displaying message to user to confirm high quantity entered
Dim strValueHigh As String 'used for displaying message to user to confirm high value entered
Dim lngMsgResult As Long 'used to get return value from mis-pick msg box
Dim dblQtyOver   As Long

Dim blnIgnFlag   As Boolean

    blnIgnFlag = False 'should be the normal to process the Flag 5-status when calling GetItem
    Call DebugMsg(MODULE_NAME, "EM", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
    If (Mode = 0) And (ChangeMade = True) Then
        Call DebugMsg(MODULE_NAME, "EM", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
        'Col = Col + 1
        sprdOrder.Col = Col
        sprdOrder.Row = Row
        Select Case (Col)
            Case (COL_PARTCODE):
                'Reset line to Valid
                sprdOrder.Col = COL_ITEMNO
                sprdOrder.Col2 = COL_SIZE
                sprdOrder.Row2 = sprdOrder.Row
                sprdOrder.BlockMode = True
                sprdOrder.BackColor = RGB_WHITE
                sprdOrder.BlockMode = False
                sprdOrder.Col = COL_QTY
                If sprdOrder.BackColor <> RGB_LTGREY Then sprdOrder.Lock = False
                
                'Check that Part code has been entered
                sprdOrder.Col = COL_PARTCODE
                If sprdOrder.Text = "" Then Exit Sub
                If Len(sprdOrder.Text) < PARTCODE_LEN Then sprdOrder.Text = Left$(PARTCODE_PAD, PARTCODE_LEN - Len(sprdOrder.Text)) & sprdOrder.Text
                strPartCode = sprdOrder.Text
                
                'Check that entered part code has not already been entered
                If mblnDupItems = False Then
                    For lngLineNo = ROW_FIRST To sprdOrder.MaxRows Step 1
                        sprdOrder.Row = lngLineNo
                        If (sprdOrder.Text = strPartCode) And (lngLineNo <> Row) Then
                            'Duplicate found so highlight line
                            sprdOrder.Row = Row
                            sprdOrder.Col = COL_DESC
                            sprdOrder.Text = "DUPLICATE"
                            sbStatus.Panels(PANEL_INFO).Text = strPartCode & "-" & sprdOrder.Text
                            sprdOrder.Col = COL_PARTCODE
                            sprdOrder.Col2 = COL_SIZE
                            sprdOrder.Row2 = sprdOrder.Row
                            sprdOrder.BlockMode = True
                            sprdOrder.BackColor = RGB_RED
                            sprdOrder.BlockMode = False
                            Col = COL_PARTCODE
                            Screen.MousePointer = vbNormal
                            If sprdOrder.ReDraw = True Then
                                Call sprdOrder.SetActiveCell(Col, Row)
                                Call sprdOrder.SetFocus
                                sprdOrder.EditMode = True
                            End If
                            Exit Sub
                        End If
                    Next lngLineNo
                End If 'duplicate items not allowed
                
                Screen.MousePointer = vbHourglass
                sprdOrder.Row = Row
                      
                If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Or (strActionCode = ACTION_MAINTPO) Then
                    sprdOrder.Col = COL_PARTCODE
                    sprdOrder.Row = Row
                    If sprdOrder.Lock = True Then blnIgnFlag = True
                End If
                
                If GetItem(strPartCode, True, blnIgnFlag) = True Then
                    'check if for Purchase Order and a special Item then get Manu Part Code
                    If (strActionCode = ACTION_PORDER) And (Left$(strPartCode, 2) = "90") Then
                        sprdOrder.Col = COL_SUPPMANCODE
                        If sprdOrder.Text = "" Then
                            sprdOrder.Col = COL_DESC
                            strDesc = sprdOrder.Text
                            sprdOrder.Col = COL_SUPPMANCODE
                            sprdOrder.Text = InputBoxEx("Enter Manufacturer Product Code for item" & vbCrLf & "SKU " & strPartCode & "-" & strDesc, "Order Special Item", "", enifAlphaNum, False, RGBMSGBox_PromptColour, , 10)
                        End If
                    End If
                    'Item retrieved and displayed, so unlock required Quantity column
                    sprdOrder.Col = COL_QTY
                    If sprdOrder.ColHidden = False Then
                        If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Or (strActionCode = ACTION_ISTIN) Then
                            'if working with del notes then reset Return Qty and Codes
                            sprdOrder.Col = COL_QTYRET
                            sprdOrder.BackColor = RGB_WHITE
                            sprdOrder.Text = 0
                            sprdOrder.Lock = True
                            sprdOrder.Col = COL_RETCODE
                            sprdOrder.BackColor = RGB_WHITE
                            sprdOrder.Text = ""
                            sprdOrder.Lock = True
                            'if working with del notes then unlock del qty
                            sprdOrder.Col = COL_QTYDEL
                            Col = COL_QTYDEL
                        Else
                            sprdOrder.Col = COL_QTY
                            Col = COL_QTY
                        End If
                        sprdOrder.Lock = False
                    Else 'Order Quantity hidden to unlock other column
                        sprdOrder.Col = COL_QTYDEL
                        If sprdOrder.ColHidden = False Then
                            sprdOrder.Lock = False
                            Col = COL_QTYDEL
                            sprdOrder.Text = "0"
                        Else
                            Col = COL_QTYRET
                            sprdOrder.Col = Col
                            sprdOrder.Lock = False
                            sprdOrder.Text = "0"
                        End If
                    End If
                    Call DebugMsg(MODULE_NAME, "EM Unlock", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                End If
                
                Screen.MousePointer = vbNormal
                
            Case (COL_QTY)
                Call DebugMsg(MODULE_NAME, "EM QTY", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                sprdOrder.Col = Col
                sprdOrder.Row = Row
                If (strActionCode = ACTION_MAINTPO) Then
                    'ensure new quantity does not exceed original quantity ordered
                    sprdOrder.Col = COL_ORIGPOQTY
                    dblOrigPOQty = Val(sprdOrder.Value)
                    sprdOrder.Col = COL_QTY
                    If (Val(sprdOrder.Value) > dblOrigPOQty) Then
                    End If
                    'Check if item was raised as part of customer order
                    sprdOrder.Col = COL_CUSTONO
                    If sprdOrder.Text <> "" Then
                        If MsgBoxEx("Customer orders are linked to this line, do you wish to re-order " & vbCrLf & _
                                        "from the supplier OR source through alternate means", vbYesNo, _
                                        "Amend Customer Order lines", , , , , RGBMSGBox_PromptColour) = vbYes Then
                            sprdOrder.Text = "CLEAR"
                        Else
                            sprdOrder.Text = "CANCEL"
                        End If
                    End If
                End If
                dblNewQty = Val(sprdOrder.Value) 'used to calculate nearest pack size
                If (sprdOrder.BackColor <> RGB_LTGREY) And (strActionCode = ACTION_PORDER) Then 'ignore pack size check if from Customer Order
                    sprdOrder.Col = COL_PACKQTY
                    If Val(sprdOrder.Value) > 1 Then 'check pack size is met
                        If (dblNewQty / Val(sprdOrder.Value)) <> (dblNewQty \ Val(sprdOrder.Value)) Then
'                            If mblnEDI = True Then 'if Electronic then auto round to next pack size
'                                Call msgboxex("Quantity entered does not fit pack Size" & vbCrLf & "Increasing number of items to next pack size", vbInformation, Me.Caption)
'                                dblNewQty = ((dblNewQty \ Val(sprdOrder.Value)) + 1) * Val(sprdOrder.Value)
'                                sprdOrder.Col = COL_QTY
'                                sprdOrder.Text = dblNewQty
'                            Else
                                If MsgBoxEx("Quantity entered does not fit pack Size" & vbCrLf & "Increase number of items to next pack size", vbYesNo + vbQuestion, Me.Caption, , , , , RGBMSGBox_PromptColour) = vbYes Then
                                    dblNewQty = ((dblNewQty \ Val(sprdOrder.Value)) + 1) * Val(sprdOrder.Value)
                                    sprdOrder.Col = COL_QTY
                                    sprdOrder.Text = dblNewQty
                                End If
'                            End If
                        End If 'pack Size mismatched
                    End If 'valid quantity entered
                    'calculate number of packs ordered and display
                    sprdOrder.Col = COL_QTY
                    dblNewQty = Val(sprdOrder.Text)
                    sprdOrder.Col = COL_PACKQTY
                    If Val(sprdOrder.Text) > 0 Then dblNewQty = dblNewQty \ Val(sprdOrder.Text)
                    sprdOrder.Col = COL_PACKSORD
                    sprdOrder.Text = dblNewQty
                End If 'customer order
                'Flag item as updated for saving
                If strActionCode = ACTION_MAINTPO Then
                    sprdOrder.Row = Row
                    sprdOrder.Col = 0
                    sprdOrder.Text = UPDATE_FLAG
                End If
                Call SetLineFormulas(Row)
                'perform check to ensure line entered does not have high value or quantity, if not order qty entered
                sprdOrder.Col = Col
                sprdOrder.Row = Row
                'ignore quantity/value check if qty is Greyed out
                If ((mdblMaxQty > 0) Or (mdblMaxValue > 0)) And (sprdOrder.BackColor <> RGB_LTGREY) Then
                    sprdOrder.Col = COL_QTY
                    If (Val(sprdOrder.Value) > mdblMaxQty) And (mdblMaxQty > 0) Then strQtyHigh = "Warning - Quantity ordered is high (above " & mdblMaxQty & " items)" & vbCrLf
                    sprdOrder.Col = COL_COSTTOTAL
                    'check entered quantity does not force line value above maximum level
                    If (Val(sprdOrder.Value) > mdblMaxValue) And (mdblMaxValue > 0) Then strValueHigh = "Warning - Total line value ordered is high (above " & ChrW(163) & mdblMaxQty & ")" & vbCrLf
                    sprdOrder.Col = COL_QTY
                    'display message to cancel or accept values
                    If (strQtyHigh <> "") Or (strValueHigh <> "") Then
                        If MsgBoxEx(strQtyHigh & strValueHigh & vbCrLf & "Enter-Continue" & vbTab & "ESC-BackUp", vbOKCancel + vbExclamation, "Ordered quantity/value high", , , , , RGBMSGBox_PromptColour) = vbCancel Then sprdOrder.Text = 0
                    End If
                End If
                sprdOrder.Col = COL_QTY
                If Val(sprdOrder.Value) = 0 Then
                    If sprdOrder.Row < sprdOrder.MaxRows Then
                        sprdOrder.Col = COL_QTY
                        sprdOrder.BackColor = RGB_RED
                    End If
                    Call UpdateTotals
                    Exit Sub 'if no Quantity then exit
                End If
                If sprdOrder.BackColor = RGB_RED Then sprdOrder.BackColor = RGB_WHITE
                'Add next line if required
                If Row = sprdOrder.MaxRows Then
                    Call AddLine
                    Row = sprdOrder.MaxRows
                    sprdOrder.LeftCol = 1
                    Col = COL_PARTCODE
                End If
                Call UpdateTotals
                Call DebugMsg(MODULE_NAME, "EM-QTY", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                Col = COL_PARTCODE
                
            Case (COL_QTYDEL)
                Call DebugMsg(MODULE_NAME, "EM DEL", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                sprdOrder.Row = Row
                sprdOrder.Row2 = Row
                If (strActionCode = ACTION_ISTIN) Or (strActionCode = ACTION_ISTOUT) Then
                    sprdOrder.Col = COL_QTYDEL
                    dblQtyOS = Val(sprdOrder.Value)
                    sprdOrder.Col = COL_QTY
                    'If Order Qty is hidden then override qty checking
                    If sprdOrder.ColHidden = True Then
                        sprdOrder.Text = dblQtyOS
                        sprdOrder.Col = COL_QTYOS
                        sprdOrder.Value = dblQtyOS
                        'Add next line if required
                        If Row = sprdOrder.MaxRows Then
                            Call SetLineFormulas(Row)
                            Call AddLine
                            sprdOrder.LeftCol = 1
                            sprdOrder.Row = Row
'                            If (strActionCode = ACTION_ISTIN) Then blnMoveLine = True
                            blnMoveLine = True
                        End If
                    End If
                End If 'IST being captured
                
                'get ordered quantity and quantity out-standing for line
                sprdOrder.Col = COL_QTY
                dblOrderQty = Val(sprdOrder.Value)
                sprdOrder.Col = COL_QTYOS
                dblQtyOS = Val(sprdOrder.Value)
                
                'Reset line to OKAY status
                If dblOrderQty = 0 Then sprdOrder.Col = COL_QTYDEL 'adding line to do not reset O/S Quantity
                sprdOrder.BlockMode = True
                sprdOrder.BackColor = RGB_WHITE
                sprdOrder.BlockMode = False
                sprdOrder.Col = Col
                            
                'Get quantity entered
                If IsNumeric(sprdOrder.Text) = False Then sprdOrder.Value = 0
                dblQtyDel = CDbl(sprdOrder.Value)
                
                '** Check Quantity entered for comparison to Return Qty
                If (dblQtyDel = 0) Then  'if Delivered Qty is 0 then clear and lock Return Qty
                    sprdOrder.Col = COL_QTYRET
                    
                    'flag line as edited, so save updates to lines
                    If (strActionCode = ACTION_MAINTDN) And (Val(sprdOrder.Text) > 0) Then
                        sprdOrder.Row = Row
                        sprdOrder.Col = 0
                        sprdOrder.Text = UPDATE_FLAG
                        
                        mblnUpdRetNote = True
                    End If
                    
                    sprdOrder.Text = 0
                    sprdOrder.Lock = True
                    sprdOrder.Col = COL_RETCODE
                    sprdOrder.Text = ""
                    sprdOrder.Lock = True
                    sprdOrder.Col = COL_RETCMNT
                    sprdOrder.Text = ""
                    sprdOrder.Lock = True
                Else
                    If dblOrderQty = 0 Then
                        'adding line to existing order so grey out fields not required
                        dblQtyOS = Val(sprdOrder.Value)
                        If Row = sprdOrder.MaxRows Then 'check if new row required
                            Call SetLineFormulas(Row)
                            Call AddLine
                            sprdOrder.Row = sprdOrder.MaxRows
                            sprdOrder.Col = COL_QTY
                            sprdOrder.BackColor = RGB_LTGREY
                            sprdOrder.Col = COL_QTYOS
                            sprdOrder.BackColor = RGB_LTGREY
                            sprdOrder.Col = COL_QTYDEL
                            sprdOrder.Lock = True
                            sprdOrder.Col = COL_QTYBORD
                            sprdOrder.BackColor = RGB_LTGREY
                            sprdOrder.Row = Row
                        End If
                    End If
                    'check if amount delivered > amount ordered
                    If Val(sprdOrder.Value) > dblQtyOS Then
                        sprdOrder.Col = COL_QTYOS
                        sprdOrder.BlockMode = True
                        sprdOrder.BackColor = RGB_LTRED
                        sprdOrder.BlockMode = False
                    End If
                    sprdOrder.Col = COL_QTYRET
                    sprdOrder.Lock = False
                End If 'Valid Quantity delivered entered
                
                If strActionCode = ACTION_MAINTDN Then
                    sprdOrder.Row = Row
                    sprdOrder.Col = 0
                    sprdOrder.Text = UPDATE_FLAG
                End If
                
                '** Check quantity entered for back orders
                If (dblQtyDel < dblQtyOS) And ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) Then
                    'if Delivered Qty is less then Qty O/S then fill in Back Order Qty
                    sprdOrder.Col = COL_QTYBORD
                    sprdOrder.Text = (dblQtyOS - dblQtyDel)
                    sprdOrder.Lock = False
                    sprdOrder.Col = COL_QTYBORD
                    Col = COL_QTYBORD

                    If sprdOrder.ColHidden = True Then 'no back order qty so allow comment
                        sprdOrder.Col = COL_QTYSHORT
                        Col = COL_QTYSHORT
                    End If
                Else
                    'if Delivered Qty is equal to O/S Qty then clear any Back Order Qty
                    sprdOrder.Col = COL_QTYBORD
                    sprdOrder.Text = 0
                    sprdOrder.Lock = True
                    sprdOrder.Col = COL_COMMENT
                    sprdOrder.Lock = True
                    sprdOrder.BackColor = RGB_GREY
                    sprdOrder.Text = ""
                    sprdOrder.Col = COL_QTY
                End If
                
                'if IST Out or Delivery Note(Maint) and Item not ordered, then allow comment for each line entered
                If (strActionCode = ACTION_ISTOUT) Then 'Or (((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) And dblOrderQty = 0) Then
                    Col = COL_COMMENT
                    sprdOrder.Row = Row
                    sprdOrder.Col = Col
                    sprdOrder.Lock = False
                    sprdOrder.BackColor = RGB_WHITE
                End If 'Comment column must be unlocked
                
                'if Delivery Note( and Maint) then reset return and short quantity
                If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
                    sprdOrder.Row = Row
                    sprdOrder.Col = COL_QTYRET
                    
                    If dblQtyDel < Val(sprdOrder.Text) Then
                        Call MsgBoxEx("Entered quantity exceeds returned quantity" & vbCrLf & "Return quantity decreased", vbExclamation, "Return quantity high", , , , , RGBMsgBox_WarnColour)
                        
                        sprdOrder.Text = dblQtyDel
                    End If 'return quantity needed to be decreased

                    sprdOrder.Row = Row
                    sprdOrder.Col = COL_QTYSHORT
                    
                    'If we have a shortage
                    If (Val(sprdOrder.Text) > 0) And (sprdOrder.ForeColor = vbRed) Then
                        Call MsgBoxEx("Delivery note amount is equal to 0" & vbCrLf & "Setting shortage quantity to 0", vbExclamation, "Reseting shortage quantity", , , , , RGBMsgBox_WarnColour)
                        
                        sprdOrder.Col = COL_QTYSHORT
                        sprdOrder.Text = 0
                        
                        sprdOrder.ForeColor = vbBlack
                    End If
                End If 'Delivery note required Return and Shortage Quantity checking
                                
                Call UpdateTotals
                
                If blnMoveLine = True Then
                    Col = COL_PARTCODE
                    Row = sprdOrder.MaxRows
                End If
                
            Case (COL_QTYSHORT)
                Call DebugMsg(MODULE_NAME, "EM SHORT", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                
                sprdOrder.Row = Row
                
                'force default Short Reason Code
                sprdOrder.Col = COL_SHORTCODE
                
                If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
                    sprdOrder.Col = COL_QTYSHORT
                    
                    If sprdOrder.ForeColor = vbRed Then ' If we are entering a short
                        'if short quantity > 0 then ensure does not exceed despatched qty
                        sprdOrder.Col = COL_SHORTCODE
                        
                        'force default Short Reason Code
                        sprdOrder.Text = mstrDefShortageDesc
                        
                        sprdOrder.Col = COL_QTYDEL
                        dblQtyDel = Val(sprdOrder.Value)
                        
                        'compare short against delivered qty - qty returned, ensure is not greater
                        sprdOrder.Col = COL_QTYRET
                        dblQtyOS = dblQtyDel - Val(sprdOrder.Text)
                        sprdOrder.Col = COL_QTYSHORT
                                        
                        If ((sprdOrder.Text > dblQtyOS)) Then 'if Short Qty > Qty Delivered
                            Call MsgBoxEx("Entered short quantity exceeds actual despatched quantity (less any returns)" & vbCrLf & "Short quantity decreased to match actual despatched quantity", vbExclamation, "Short quantity high", , , , , RGBMsgBox_WarnColour)
                                        
                            'If Qty Delivered (minus returns) is less than 0 then turn it into an over
                            If dblQtyOS < 0 Then
                                dblQtyOS = Abs(dblQtyOS)
                                
                                sprdOrder.ForeColor = vbBlue
                                
                                sprdOrder.Col = COL_QTYOVER
                                sprdOrder.Text = Abs(dblQtyOS)
                            End If
                            
                            'If Qty Delivered (minus returns) is equal to 0 then set short to 0
                            If dblQtyOS = 0 Then
                                sprdOrder.ForeColor = vbBlack
                                
                                sprdOrder.Col = COL_QTYOVER
                                sprdOrder.Text = 0
                            End If
                            
                            'If Qty Delivered (minus returns) is equal to 0 then set short to Qty Delivered (minus returns)
                            If dblQtyOS > 0 Then
                                sprdOrder.Col = COL_QTYOVER
                                sprdOrder.Text = dblQtyOS
                            End If
                            
                            sprdOrder.Col = COL_QTYSHORT
                            
                            sprdOrder.Text = dblQtyOS
                        Else
                            sprdOrder.Col = COL_QTYOVER
                            sprdOrder.Text = 0
                        End If
                        
                        Call UpdateTotals
                        
                        sprdOrder.Row = Row
                        
                        'flag line as edited, so save updates to lines
                        If (strActionCode = ACTION_MAINTDN) Then
                            sprdOrder.Row = Row
                            sprdOrder.Col = 0
                            sprdOrder.Text = UPDATE_FLAG
                            
                            sprdOrder.Col = COL_QTYSHORT
                            
                            Select Case sprdOrder.ForeColor
                                Case vbRed
                                    'Check if this line used to be an over or short so that we know which DRL's to edit
                                    If (mblnWasShortage = True) Then 'Was a shortage
                                        mblnUpdShrtNote = True
                                    ElseIf (mblnWasOverage = True) Then 'Was an overage
                                        mblnUpdShrtNote = True
                                        mblnUpdOver = True
                                    ElseIf (mblnWasShortage = False) And (mblnWasOverage = False) Then 'Was neither shortage or overage
                                        mblnUpdShrtNote = True
                                    End If
                                    
                                Case vbBlue
                                    'Check if this line used to be an over or short so that we know which DRL's to edit
                                    If (mblnWasShortage = True) Then 'Was a shortage
                                        mblnUpdShrtNote = True
                                    ElseIf (mblnWasOverage = True) Then 'Was an overage
                                        mblnUpdOver = True
                                    ElseIf (mblnWasShortage = False) And (mblnWasOverage = False) Then 'Was neither shortage or overage
                                        mblnUpdOver = True
                                    End If
                                
                                Case vbBlack
                                    'Check if this line used to be an over or short so that we know which DRL's to edit
                                    If (mblnWasShortage = True) Then 'Was a shortage
                                        mblnUpdShrtNote = True
                                    ElseIf (mblnWasOverage = True) Then 'Was an overage
                                        mblnUpdOver = True
                                    End If
                                    
                            End Select
                        End If
                    Else 'Over entered
                        sprdOrder.Col = COL_QTY
                        
                        If Val(sprdOrder.Text) = 0 Then
                            'adding line to existing order so grey out fields not required
                            If Row = sprdOrder.MaxRows Then 'check if new row required
                                Call SetLineFormulas(Row)
                                Call AddLine
                                sprdOrder.Row = sprdOrder.MaxRows
                                sprdOrder.Col = COL_QTY
                                sprdOrder.BackColor = RGB_LTGREY
                                sprdOrder.Col = COL_QTYOS
                                sprdOrder.BackColor = RGB_LTGREY
                                sprdOrder.Col = COL_QTYDEL
                                sprdOrder.Lock = True
                                sprdOrder.Col = COL_QTYBORD
                                sprdOrder.BackColor = RGB_LTGREY
                                sprdOrder.Row = Row
                            End If
                        End If
                    
                        sprdOrder.Col = COL_QTYSHORT
                        dblQtyOver = Val(sprdOrder.Text)
                        
                        sprdOrder.Col = COL_QTYDEL
                        dblQtyDel = Val(sprdOrder.Value)
                        
                        'Get return qty
                        sprdOrder.Col = COL_QTYRET
                        dblQtyOS = Val(sprdOrder.Text)
                                            
                        sprdOrder.Col = COL_QTYSHORT
                        
                        If (dblQtyOS > dblQtyDel + dblQtyOver) Then
                            Call MsgBoxEx("Overs quantity increased to match received quantity less returns", vbExclamation, "Over quantity low", , , , , RGBMsgBox_WarnColour)
                            
                            dblQtyOver = dblQtyOS - dblQtyDel
                            
                            sprdOrder.Text = dblQtyOver
                        End If
                        
                        'Unlock the return quantity field
                        If dblQtyOver > 0 Then
                            sprdOrder.Col = COL_QTYRET
                            sprdOrder.Lock = False
                        End If
                        
                        sprdOrder.Col = COL_QTYOVER
                        
                        sprdOrder.Text = dblQtyOver
                        
                        'flag line as edited, so save updates to lines
                        If (strActionCode = ACTION_MAINTDN) Then
                            sprdOrder.Row = Row
                            sprdOrder.Col = 0
                            sprdOrder.Text = UPDATE_FLAG
                            
                            'Check if this line used to be an over or short so that we know which DRL's to edit
                            If (mblnWasOverage = True) Then
                                mblnUpdOver = True
                            ElseIf (mblnWasShortage = True) Then
                                mblnUpdShrtNote = True
                                mblnUpdOver = True
                            ElseIf (mblnWasShortage = False) And (mblnWasOverage = False) Then
                                mblnUpdOver = True
                            End If
                        End If
                    End If
                Else
                    sprdOrder.Text = mstrDefShortageDesc
                    sprdOrder.Col = COL_QTYDEL
                    dblQtyDel = Val(sprdOrder.Value)
                    'compare short against delivered qty - qty returned, ensure is not greater
                    sprdOrder.Col = COL_QTYRET
                    dblQtyOS = dblQtyDel - Val(sprdOrder.Text)
                    sprdOrder.Col = COL_QTYSHORT
                    If (strActionCode = ACTION_MAINTRETURN) And (dblQtyDel = 0) Then dblQtyOS = Val(sprdOrder.Text)
                    If sprdOrder.Text > dblQtyOS Then 'if Return Qty > Qty Delivered then flag
                        Call MsgBoxEx("Entered short quantity exceeds actual despatched quantity (less any returns)" & vbCrLf & "Short quantity decreased to match actual despatched quantity", vbExclamation, "Short quantity high", , , , , RGBMsgBox_WarnColour)
                        sprdOrder.Text = dblQtyOS
                    End If
                    'flag line as edited, so save updates to lines
                    If (strActionCode = ACTION_MAINTRETURN) Then
                        sprdOrder.Row = Row
                        sprdOrder.Col = 0
                        sprdOrder.Text = UPDATE_FLAG
                        mblnUpdShrtNote = True
                    End If
                    Call UpdateTotals
                    sprdOrder.Row = Row
                End If
                
            Case (COL_COMMENT):
                If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
                    'if coming out of edit mode then jump to next column
                    Col = Col + 1
                    sprdOrder.Col = Col
                    sprdOrder.Row = Row
                    'move to next column and further until editable cell found
                    While ((sprdOrder.Lock = True) Or (sprdOrder.ColHidden = True)) And (Col <= sprdOrder.MaxCols)
                        Col = Col + 1
                        sprdOrder.Col = Col
                    Wend
                    'check that cell was found reset back to comment
                    If Col > sprdOrder.MaxCols Then Col = COL_COMMENT
                End If
                
                If (strActionCode = ACTION_ISTOUT) Then
                    Col = COL_PARTCODE
                    Row = sprdOrder.MaxRows
                End If
                
            Case (COL_QTYBORD)
                Call DebugMsg(MODULE_NAME, "EM DEL", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                sprdOrder.Row = Row
                sprdOrder.Row2 = Row
                sprdOrder.Col = COL_QTYOS
                dblQtyOS = Val(sprdOrder.Value)
                sprdOrder.Col2 = COL_QTYDEL
                sprdOrder.BlockMode = True
                sprdOrder.BackColor = RGB_WHITE
                sprdOrder.BlockMode = False
                
                'Find out how much should be on Back-Order
                sprdOrder.Col = COL_QTYDEL
                dblQtyOS = dblQtyOS - Val(sprdOrder.Value)
                sprdOrder.Col = Col
                
                If Val(sprdOrder.Value) <> dblQtyOS Then 'if value is 0 then record comment
                    sprdOrder.Col = COL_COMMENT
                    sprdOrder.Lock = False
                    sprdOrder.BackColor = RGB_WHITE
                    Col = COL_COMMENT
                Else
                    sprdOrder.Col = COL_COMMENT
                    sprdOrder.Lock = True
                    sprdOrder.Text = ""
                    sprdOrder.BackColor = RGB_GREY
                End If
                
                Call UpdateTotals
                
                sprdOrder.Row = Row
            Case (COL_QTYRET) 'if return quantity > 0 then enable reason code entry
                Call DebugMsg(MODULE_NAME, "EM RET", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                sprdOrder.Col = Col
                sprdOrder.Row = Row
                
                sprdOrder.BackColor = RGB_WHITE 'reset status to valid entry
                If Val(sprdOrder.Value) = 0 Then 'if value is 0 then clear and lock Return Qty
                    sprdOrder.Col = COL_RETCODE
                    sprdOrder.Lock = True
                    sprdOrder.Text = ""
                    sprdOrder.Col = COL_RETCMNT
                    sprdOrder.Lock = True
                    sprdOrder.Text = ""
                Else
                    If (strActionCode = ACTION_RETURN) Then 'override del qty checking
                        Call SetLineFormulas(Row)
                        sprdOrder.Col = COL_QTYRET
                        dblQtyOS = Val(sprdOrder.Value)
                        sprdOrder.Col = COL_QTYDEL
                        sprdOrder.Text = dblQtyOS
                    End If 'Supplier return being captured
                    
                    sprdOrder.Col = COL_QTYDEL
                    dblQtyDel = Val(sprdOrder.Value)
                    'compare returned against delivered qty - qty short, ensure is not greater
                    sprdOrder.Col = COL_QTYSHORT
                    
                    'Added dslater mis-pick
                    If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
                        If sprdOrder.ForeColor = vbRed Then ' If we are entering a short
                            'compare returned against delivered qty - qty short, ensure is not greater
                            sprdOrder.Col = COL_QTYSHORT
                            dblQtyOS = dblQtyDel - Val(sprdOrder.Text)
                            sprdOrder.Col = COL_QTYRET
                            
                            If sprdOrder.Text <= dblQtyOS Then 'if Return Qty > Qty Delivered then flag
                                sprdOrder.BackColor = RGB_WHITE
                            Else
                                Call MsgBoxEx("Entered return quantity exceeds actual received quantity (less any shortages)" & vbCrLf & "Return quantity decreased to match actual received quantity", vbExclamation, "Return quantity high", , , , , RGBMsgBox_WarnColour)
                                sprdOrder.Text = dblQtyOS
                            End If
                        Else
                            'Get over quantity
                            sprdOrder.Col = COL_QTYSHORT
                            
                            dblQtyOS = dblQtyDel + Val(sprdOrder.Text)
                            
                            sprdOrder.Col = COL_QTYRET
                            
                            If (Val(sprdOrder.Text) > dblQtyDel) And (Val(sprdOrder.Text) <= dblQtyOS) Then
                                Call MsgBoxEx("Entered return quantity exceeds delivery note quantity", vbExclamation, "Return quantity high", , , , , RGBMsgBox_WarnColour)
                            End If
                            
                            If (Val(sprdOrder.Text) > dblQtyOS) Then
                                Call MsgBoxEx("Entered return quantity exceeds actual received quantity (plus any overs)" & vbCrLf & "Return quantity decreased to match actual received quantity", vbExclamation, "Return quantity high", , , , , RGBMsgBox_WarnColour)
                                
                                sprdOrder.Text = dblQtyOS
                            End If
                        End If
                        
                        sprdOrder.Col = COL_QTYSHORT
                        
                        If dblQtyOS > 0 Then
                            'Unlock Return Reason field and set default to first entry
                            sprdOrder.Row = Row
                            sprdOrder.Col = COL_RETCMNT
                            sprdOrder.Lock = False
                            sprdOrder.Col = COL_RETCODE
                            
        '                    If sprdOrder.TypeComboBoxList = "" Then sprdOrder.TypeComboBoxList = mstrRetCode
        
                            If (dblQtyDel = 0) Then
                                If (sprdOrder.TypeComboBoxList = mstrRetCode) Or (sprdOrder.TypeComboBoxList = "") Then
                                    sprdOrder.TypeComboBoxList = mstrMispickCode
                                End If
                                
                                If sprdOrder.Text = "" Then
                                    sprdOrder.Text = Left$(mstrMispickCode, InStr(mstrMispickCode, vbTab) - 1)
                                    sprdOrder.Col = COL_RETCMNT
                                    sprdOrder.Lock = False
                                End If
                            Else
                                If (sprdOrder.TypeComboBoxList = mstrMispickCode) Or (sprdOrder.TypeComboBoxList = "") Then
                                    sprdOrder.TypeComboBoxList = mstrRetCode
                                End If
                                
                                If sprdOrder.Text = "" Then
                                    sprdOrder.Text = Left$(mstrRetCode, InStr(mstrRetCode, vbTab) - 1)
                                    sprdOrder.Col = COL_RETCMNT
                                    sprdOrder.Lock = False
                                End If
                            End If
                            
                            sprdOrder.Col = COL_RETCODE
                            
                            sprdOrder.Lock = False
                            
                            Call sprdOrder.Refresh
                            
                            If sprdOrder.Text = "" Then
                                sprdOrder.Text = Left$(mstrRetCode, InStr(mstrRetCode, vbTab) - 1)
                                sprdOrder.Col = COL_RETCMNT
                                sprdOrder.Lock = False
                            End If
                            
                            Call sprdOrder.Refresh
        '                   Call sprdOrder.SetActiveCell(COL_RETCODE, Row)
                            Col = COL_RETCODE 'move to select return code
                            blnDropDown = True
                            Call DebugMsg(MODULE_NAME, "EM", endlDebug, "Ready to drop down list")
                        End If
                    Else
                        dblQtyOS = dblQtyDel - Val(sprdOrder.Text)
                        sprdOrder.Col = COL_QTYRET
                        If (strActionCode = ACTION_MAINTRETURN) And (dblQtyDel = 0) Then dblQtyOS = Val(sprdOrder.Text)
                        If sprdOrder.Text <= dblQtyOS Then 'if Return Qty > Qty Delivered then flag
                            sprdOrder.BackColor = RGB_WHITE
                        Else
                            Call MsgBoxEx("Entered return quantity exceeds actual received quantity (less any shortages)" & vbCrLf & "Return quantity decreased to match actual received quantity", vbExclamation, "Return quantity high", , , , , RGBMsgBox_WarnColour)
                            sprdOrder.Text = dblQtyOS
                        End If
                        
                        'Unlock Return Reason field and set default to first entry
                        sprdOrder.Row = Row
                        sprdOrder.Col = COL_RETCMNT
                        sprdOrder.Lock = False
                        sprdOrder.Col = COL_RETCODE
    '                    If sprdOrder.TypeComboBoxList = "" Then sprdOrder.TypeComboBoxList = mstrRetCode
                        sprdOrder.Lock = False
                        Call sprdOrder.Refresh
                        If sprdOrder.Text = "" Then
                            sprdOrder.Text = Left$(mstrRetCode, InStr(mstrRetCode, vbTab) - 1)
                            sprdOrder.Col = COL_RETCMNT
                            sprdOrder.Lock = False
                        End If
                        Call sprdOrder.Refresh
    '                   Call sprdOrder.SetActiveCell(COL_RETCODE, Row)
                       Col = COL_RETCODE 'move to select return code
                       blnDropDown = True
                       Call DebugMsg(MODULE_NAME, "EM", endlDebug, "Ready to drop down list")
                    End If
                End If
                
                'flag line as edited, so save updates to lines
                If (strActionCode = ACTION_MAINTDN) Or (strActionCode = ACTION_MAINTRETURN) Then
                    sprdOrder.Row = Row
                    sprdOrder.Col = 0
                    sprdOrder.Text = UPDATE_FLAG
                    mblnUpdRetNote = True
                End If
                
                Call UpdateTotals
                
                sprdOrder.Row = Row
            Case (COL_RETCODE) 'if return code selected then enabled Return comment
                Call DebugMsg(MODULE_NAME, "EM RETCODE", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                sprdOrder.Col = Col
                sprdOrder.Row = Row
                If (strActionCode = ACTION_RETURN) Then 'override del qty checking
                    sprdOrder.Col = COL_QTYRET
                    dblQtyOS = Val(sprdOrder.Value)
                    sprdOrder.Col = COL_QTYDEL
                    sprdOrder.Text = dblQtyOS
                End If 'Supplier return being captured
                DoEvents
                sprdOrder.Col = COL_RETCMNT
                sprdOrder.Row = Row
                sprdOrder.Lock = False
                'if maintain del note, then mark line for update, once return code entered
                If (strActionCode = ACTION_MAINTDN) Or (strActionCode = ACTION_MAINTRETURN) Then
                    sprdOrder.Row = Row
                    sprdOrder.Col = 0
                    sprdOrder.Text = UPDATE_FLAG
                    mblnUpdRetNote = True
                End If
                Col = COL_RETCMNT
                Call UpdateTotals
                sprdOrder.Row = Row
            Case (COL_RETCMNT) 'if return code selected then check if Return and to add new line
                Call DebugMsg(MODULE_NAME, "EM RETCMNT", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
                sprdOrder.Col = Col
                sprdOrder.Row = Row
                'check if new row required
                If (strActionCode = ACTION_RETURN) And (Row = sprdOrder.MaxRows) Then
                    Call AddLine
                    sprdOrder.LeftCol = 1
                    Row = sprdOrder.MaxRows
                    Col = COL_PARTCODE
                End If 'Supplier return being captured
                If (strActionCode = ACTION_MAINTDN) Or (strActionCode = ACTION_MAINTRETURN) Then
                    sprdOrder.Row = Row
                    sprdOrder.Col = 0
                    sprdOrder.Text = UPDATE_FLAG
                    mblnUpdRetNote = True
                End If
                sprdOrder.Row = Row
        End Select
        If sprdOrder.ReDraw = True Then
            Call DebugMsg(MODULE_NAME, "SET", endlTraceIn, "Col=" & Col & ",Row=" & Row & ",Mode=" & Mode & ",CM=" & ChangeMade)
            sprdOrder.Col = Col
            sprdOrder.Row = Row
            Call sprdOrder.SetActiveCell(Col, Row)
            'sprdOrder.EditMode = True
        End If
    End If 'If coming out of edit mode (Mode=0) and Changes made to data
    
    If (Col = COL_COMMENT) And (Mode = 1) Then 'load up comment list
        If (mstrLineCmnt = "") Then 'list not yet loaded, then get list of comments
            Select Case (strActionCode)
                Case (ACTION_PORDER): mstrLineCmnt = FillInLineComments(enatPurchaseOrder, True)
            End Select
        End If
        sprdOrder.Col = Col
        sprdOrder.Row = Row
        If sprdOrder.CellType <> CellTypeComboBox Then
            sprdOrder.CellType = CellTypeComboBox
            sprdOrder.TypeComboBoxEditable = True
            sprdOrder.TypeComboBoxList = mstrLineCmnt
        End If
        Call SendKeys(KEY_DROPDOWN)
    End If
    
    If (Mode = 0) And (ChangeMade = False) And (Col = COL_PARTCODE) Then
        'Move to next edit field
        If sprdOrder.BackColor = RGB_WHITE Then
            For lngColNo = Col + 1 To sprdOrder.MaxCols Step 1
                sprdOrder.Col = lngColNo
                If (sprdOrder.Lock = False) And (sprdOrder.ColHidden = False) Then
                    sprdOrder.Col = lngColNo
                    sprdOrder.Row = Row
                    Col = lngColNo
                    Call sprdOrder.SetActiveCell(lngColNo, Row)
                    sprdOrder.EditMode = True
                    Exit For
                End If
            Next lngColNo
        End If
    End If
    
    If (Mode = 0) And (ChangeMade = False) And (Col = COL_RETCODE) Then
        Call DebugMsg(MODULE_NAME, "EM", endlDebug, "RetCode" & Col)
        'Move to next edit field
        For lngColNo = Col + 1 To sprdOrder.MaxCols Step 1
            sprdOrder.Col = lngColNo
            If (sprdOrder.Lock = False) And (sprdOrder.ColHidden = False) Then
                sprdOrder.Col = lngColNo
                sprdOrder.Row = Row
                Col = lngColNo
                Call sprdOrder.SetActiveCell(lngColNo, Row)
                sprdOrder.EditMode = True
                Exit For
            End If
        Next lngColNo
    End If
    
    'Added dslater for Mis-Picks
    'Display Short \ Over Messagebox
    If (Col = COL_QTYSHORT) And (Mode = 1) And ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) Then
        sprdOrder.Col = COL_QTYDEL
        
        If Val(sprdOrder.Text) > 0 Then
            lngMsgResult = MsgBoxEx("Are you entering a short or over?" & vbCrLf, vbYesNoCancel + vbQuestion, "Mis-pick", "Short", "Over", "Cancel", , RGBMSGBox_PromptColour)
        Else
            Call MsgBoxEx("Entered delivery note quantity is equal 0" & vbCrLf & "You can only enter an over", vbOKOnly + vbInformation, "Delivery Note Quantity", , , , , RGBMsgBox_WarnColour)
            
            lngMsgResult = vbNo
        End If
        
        sprdOrder.Col = COL_QTYSHORT
        
        If sprdOrder.ForeColor = vbRed Then ' was this a short
            mblnWasShortage = True
            mblnWasOverage = False
        ElseIf sprdOrder.ForeColor = vbBlue Then ' was this an over
            mblnWasOverage = True
            mblnWasShortage = False
        Else
            mblnWasOverage = False
            mblnWasShortage = False
        End If
        
        Select Case lngMsgResult
            Case vbCancel 'Cancel Pressed
                Col = COL_QTYRET
                
                sprdOrder.Col = Col
                
                sprdOrder.Row = sprdOrder.SelBlockRow
                
                Call sprdOrder.SetActiveCell(sprdOrder.Col, sprdOrder.Row)
                                            
            Case vbYes 'Short Pressed
                sprdOrder.Col = COL_QTYSHORT
                sprdOrder.ForeColor = vbRed
                
                sprdOrder.Text = 0
                
            Case vbNo 'Over Pressed
                sprdOrder.Col = COL_QTYSHORT
                sprdOrder.ForeColor = vbBlue
                
                sprdOrder.Text = 0
            
        End Select
        
        DoEvents
        
        Call sprdOrder.Refresh
    End If
    
    If (Col = COL_QTYSHORT) And (Mode = 0) And ((strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN)) Then
        sprdOrder.Col = COL_QTYSHORT
        
        If (Val(sprdOrder.Text) <= 0) And (sprdOrder.ForeColor <> vbBlack) Then
            Call MsgBoxEx("Quantity must be greater than 0" & vbCrLf, vbOKOnly + vbInformation, "Invalid Quantity", , , , , RGBMsgBox_WarnColour)
            
            sprdOrder.Text = 0
            
            sprdOrder.ForeColor = vbBlack
        End If
    End If
    
    If blnDropDown = True Then
        Call sprdOrder.SetActiveCell(Col, Row)
        sprdOrder.EditMode = True
        Call SendKeys(KEY_DROPDOWN)
    End If
    Call DebugMsg(MODULE_NAME, "EM", endlTraceOut, "R/C=" & sprdOrder.Row & "/" & sprdOrder.Col)

End Sub
Private Sub SetLineFormulas(ByVal lngRowNo As Long)

    sprdOrder.Row = lngRowNo
    sprdOrder.Col = COL_ITEMNO
    sprdOrder.Text = lngRowNo - 1
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    sprdOrder.Col = COL_VATTOTAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_EXCTOTAL) & "# * 0.175"
    sprdOrder.Col = COL_QTYBORD
    sprdOrder.Text = 0
    sprdOrder.Col = COL_INCTOTAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_EXCTOTAL) & "# + " & ColToText(COL_VATTOTAL) & "#"
    sprdOrder.Col = COL_COSTTOTAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    sprdOrder.Col = COL_DELTOTAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTYDEL) & "# * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    sprdOrder.Col = COL_WGHTTOT
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_WEIGHT) & "#"
    sprdOrder.Col = COL_RETVAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTYRET) & "# * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    sprdOrder.Col = COL_DELCOST
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTYDEL) & "# * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    
    'Changed dslater mis-picks
'    sprdOrder.Col = COL_SHORTVAL
'    sprdOrder.Text = 0
'    sprdOrder.Formula = ColToText(COL_QTYSHORT) & "# * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"

    sprdOrder.Col = COL_SHORTVAL
    sprdOrder.Text = 0
    sprdOrder.Formula = "(" & ColToText(COL_QTYSHORT) & "# - " & ColToText(COL_QTYOVER) & "#) * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    
'    sprdOrder.Col = COL_SHORTCOST
'    sprdOrder.Text = 0
'    sprdOrder.Formula = ColToText(COL_QTYSHORT) & "# * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"

    sprdOrder.Col = COL_SHORTCOST
    sprdOrder.Text = 0
    sprdOrder.Formula = "(" & ColToText(COL_QTYSHORT) & "# - " & ColToText(COL_QTYOVER) & "#) * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    
    sprdOrder.Col = COL_OVERVAL
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTYOVER) & "# * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    
    sprdOrder.Col = COL_OVERCOST
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTYOVER) & "# * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"

    sprdOrder.Col = COL_RETCOST
    sprdOrder.Text = 0
    sprdOrder.Formula = ColToText(COL_QTYRET) & "# * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"
    sprdOrder.Col = COL_RETCODE
    If sprdOrder.TypeComboBoxList = "" Then sprdOrder.TypeComboBoxList = mstrRetCode

End Sub
Private Sub AddLine()

Dim strFormula As String
    
    sprdOrder.MaxRows = sprdOrder.MaxRows + 1
    sprdOrder.Col = 0
    sprdOrder.Row = sprdOrder.MaxRows
    sprdOrder.Text = " "
    'create formula for all columns except qtyshort
    strFormula = "SUM(#" & ROW_FIRST & ":#" & sprdOrder.MaxRows & ")"
    'Update formulas for new row value
    sprdOrder.Row = ROW_TOTALS
    sprdOrder.Col = COL_QTY
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_EXCTOTAL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_QTYOS
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_COSTTOTAL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_QTYRET
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_DELTOTAL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_WGHTTOT
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_RETVAL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_DELCOST
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_RETCOST
    sprdOrder.Formula = strFormula
    
'    sprdOrder.Col = COL_QTYSHORT
'    sprdOrder.Formula = strFormula
    
    'Added dslater mis-picks
    'We need to remove QtyOver from QtyShort as we store Overs and Shorts in QtyShort
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.Formula = "SUM(#" & ROW_FIRST & ":#" & sprdOrder.MaxRows & ") - SUM(" & ColToText(COL_QTYOVER) & ROW_FIRST & ":" & ColToText(COL_QTYOVER) & sprdOrder.MaxRows & ")"
    
    sprdOrder.Col = COL_SHORTCOST
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_SHORTVAL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_OVERCOST
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_OVERVAL
    sprdOrder.Formula = strFormula
    sprdOrder.Col = COL_QTYOVER
    sprdOrder.Formula = strFormula
    
    sprdOrder.LeftCol = 1
    sprdOrder.Row = sprdOrder.MaxRows
    If strActionCode = ACTION_ISTIN Then
        sprdOrder.Col = COL_QTY
        sprdOrder.BackColor = RGB_LTGREY
    End If
    sprdOrder.Col = COL_COMMENT
    sprdOrder.BackColor = RGB_GREY
    
    'Debug Code
'    sprdOrder.Row = ROW_TOTALS
'    sprdOrder.RowHidden = False
'
'    sprdOrder.Col = COL_DELTOTAL
'    sprdOrder.ColHidden = False
'
'    sprdOrder.Col = COL_DELCOST
'    sprdOrder.ColHidden = False

End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean GetItem()
'**********************************************************************************************
'* Description: Used to retrieve an Item given the SKU and display in spread sheet.  Also used
'*              to initialise line values to 0 as each item is selected/ displayed.
'**********************************************************************************************
'* Parameters:
'*In/Out:strPartCode  String.-Part Code used to retrieve item - must be ready formatted
'*In/Out:blnCheckItem Boolean.-As item is retrieved the status flag is checked to ignore obselete items
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function GetItem(strPartCode As String, ByVal blnCheckItem As Boolean, ByVal blnIgnoreFlag As Boolean) As Boolean

Dim oItem   As Object
Dim lRow    As Long
Dim sErrMsg As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetItem"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
                
    lRow = sprdOrder.Row
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Description)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_SupplierNo)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_ItemObsolete)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_SizeDescription)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_BuyInPacksOf)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QtyBuyIn)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_CostPrice)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityCustOrder)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityForBranches)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityOnDisplay)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_MinQuantity)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_MaxQuantity)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_LastSold)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_VATRate)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_ManufProductCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_LastOrdered)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_NormalSellPrice)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityToUOM)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_SellInUnitOccNo)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Warehouse)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Flag5)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Weight)
    
    Call oItem.IBo_Loadmatches
    
    If (blnCheckItem = True) And (strItemSuppNo <> "") And (oItem.SupplierNo <> strItemSuppNo) Then sErrMsg = "ITEM NOT FOR THIS SUPPLIER"
    If (mblnWarehouse = False) And ((oItem.ItemObsolete = True) Or (oItem.Flag5 <> "C")) And (mblnUseObsItems = False) And (blnIgnoreFlag = False) Then sErrMsg = "ITEM IS OBSOLETE"
    If (mblnWarehouse = True) And (oItem.Warehouse <> "W") Then sErrMsg = "NON WAREHOUSE ITEM SELECTED"
    'Added 19/1/04 - implement Category 'A' for use ALL items regardless
    If mblnUseAllItems = True Then sErrMsg = ""
    If oItem.Description = "" Then sErrMsg = "NOT FOUND"
    sprdOrder.LeftCol = sprdOrder.ColsFrozen  'ensure moved to start of grid

    If sErrMsg <> "" Then
        sprdOrder.Col = COL_DESC
        sprdOrder.Text = sErrMsg
        sbStatus.Panels(PANEL_INFO).Text = sprdOrder.Text
        sprdOrder.Col = COL_PARTCODE
        sprdOrder.Col2 = COL_SIZE
        sprdOrder.Row2 = sprdOrder.Row
        sprdOrder.BlockMode = True
        sprdOrder.BackColor = RGB_RED
        sprdOrder.BlockMode = False
        Screen.MousePointer = vbNormal
        If sprdOrder.ReDraw = True Then
            Call sprdOrder.SetActiveCell(COL_PARTCODE, lRow)
            If sprdOrder.Visible = True Then Call sprdOrder.SetFocus
            sprdOrder.EditMode = True
        End If
        GetItem = False
        Exit Function
    End If

    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = oItem.PartCode
    sprdOrder.Col = COL_DESC
    sprdOrder.Text = oItem.Description
    sbStatus.Panels(PANEL_INFO).Text = strPartCode & "-" & sprdOrder.Text
    sprdOrder.Col = COL_SIZE
    sprdOrder.Text = oItem.SizeDescription
    sprdOrder.Col = COL_UNITS
    sprdOrder.Text = oItem.BuyInPacksOf
    sprdOrder.Col = COL_QTY
    sprdOrder.Text = 0
    sprdOrder.Col = COL_QTYOS
    sprdOrder.Text = 0
    sprdOrder.Col = COL_QTYDEL
    sprdOrder.Text = 0
    sprdOrder.Col = COL_QTYRET
    sprdOrder.Text = 0
    sprdOrder.Col = COL_QTYSHORT
    sprdOrder.Text = 0
    sprdOrder.Col = COL_PACKQTY
    sprdOrder.Text = oItem.QtyBuyIn
    sprdOrder.Col = COL_COST
    sprdOrder.Text = oItem.CostPrice
    sprdOrder.Col = COL_FREE
    sprdOrder.Text = oItem.QuantityAtHand - (oItem.QuantityCustOrder + oItem.QuantityForBranches + oItem.QuantityOnDisplay)
    sprdOrder.Col = COL_ONHAND
    sprdOrder.Text = oItem.QuantityAtHand
    sprdOrder.Col = COL_MINLEVEL
    sprdOrder.Text = oItem.MinQuantity
    sprdOrder.Col = COL_MAXLEVEL
    sprdOrder.Text = oItem.MaxQuantity
    sprdOrder.Col = COL_LASTSOLD
    sprdOrder.Text = DisplayDate(oItem.LastSold, False)
    sprdOrder.Col = COL_VATRATE
    sprdOrder.Text = oItem.VatRate
    sprdOrder.Col = COL_MANUCODE
    sprdOrder.Text = oItem.ManufProductCode
    sprdOrder.Col = COL_LASTORDER
    If Year(oItem.LastOrdered) > 1899 Then sprdOrder.Value = Format(oItem.LastOrdered, "MMDDYYYY")
    sprdOrder.Col = COL_SELLPRICE
    sprdOrder.Text = oItem.NormalSellPrice
    sprdOrder.Col = COL_QTYSELLIN
    sprdOrder.Text = oItem.QuantityToUOM(oItem.SellInUnitOccNo)
    sprdOrder.Col = COL_WEIGHT
    sprdOrder.Text = oItem.Weight
    Set oItem = Nothing
    GetItem = True

    'Tidy up
    GoSub Finally

    Exit Function

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, 1)
    Exit Function

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Function 'GetItem

Private Sub UpdateTotals()

Dim lngOrderRow As Long
Dim lngOrderCol As Long
    
    lngOrderRow = sprdOrder.Row
    lngOrderCol = sprdOrder.Col
    'Transfer Quantity and Value Totals from Line 1 on Order to display Totals grid
    sprdTotal.Row = 2
    sprdOrder.Row = ROW_TOTALS
    
    sprdTotal.Col = COL_TOTQTY
    sprdOrder.Col = COL_QTY
    sprdTotal.Text = sprdOrder.Text 'get total number of items
    
    sprdTotal.Col = COL_TOTWGHT
    sprdOrder.Col = COL_WGHTTOT
    sprdTotal.Text = sprdOrder.Text 'get Order Weight
    
    sprdTotal.Col = COL_TOTVAL
    sprdOrder.Col = COL_COSTTOTAL
    sprdTotal.Text = sprdOrder.Text 'get Order Value
    
    sprdTotal.Col = COL_TOTQTYOS
    sprdOrder.Col = COL_QTYOS
    sprdTotal.Text = sprdOrder.Text 'get total number of items outstanding
    
    sprdTotal.Col = COL_TOTQTYDEL
    sprdOrder.Col = COL_QTYDEL
    sprdTotal.Text = sprdOrder.Text 'get total number of items delivered
    
    sprdTotal.Col = COL_TOTQTYRET
    sprdOrder.Col = COL_QTYRET
    sprdTotal.Text = sprdOrder.Text 'get total number of items returned
    
    sprdTotal.Col = COL_TOTRETVAL
    sprdOrder.Col = COL_RETCOST
    sprdTotal.Text = sprdOrder.Text 'get total value of items returned
    
    sprdTotal.Col = COL_TOTQTYSHRT
    sprdOrder.Col = COL_QTYSHORT
    sprdTotal.Text = sprdOrder.Text 'get total number of items returned
    
    sprdTotal.Col = COL_TOTSHRTVAL
    sprdOrder.Col = COL_SHORTCOST
    sprdTotal.Text = sprdOrder.Text 'get total value of items returned
    
    sprdOrder.Row = lngOrderRow
    sprdOrder.Col = lngOrderCol
    
End Sub
Private Sub ResetTotals()
    
    'Reset Quantity and Value Totals to 0
    sprdTotal.Row = 2
    
    sprdTotal.Col = COL_TOTQTY
    sprdTotal.Text = 0
    
    sprdTotal.Col = COL_TOTWGHT
    sprdTotal.Text = 0
    
    sprdTotal.Col = COL_TOTVAL
    sprdTotal.Text = 0

    sprdTotal.Col = COL_TOTQTYOS
    sprdTotal.Text = 0
    
    sprdTotal.Col = COL_TOTQTYDEL
    sprdTotal.Text = 0
    
    sprdTotal.Col = COL_TOTQTYRET
    sprdTotal.Text = 0
    

End Sub

Private Sub sprdOrder_GotFocus()

    Call DebugMsg(MODULE_NAME, "Form_GotFocus", endlDebug, "Row=" & sprdOrder.Row)
    If strActionCode <> ACTION_MAINTISTOUT Then cmdSave.Visible = True
    If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Or _
        (strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_MAINTRETURN) Then cmdSaveHO.Visible = True
        
    If lblSubmitted.Visible = True Then
        cmdSave.Visible = False
        cmdSaveHO.Visible = False
    End If
    
    sprdOrder.SelBackColor = RGB_BLUE
    sprdOrder.SelForeColor = RGB_WHITE
    If sprdOrder.Row < ROW_FIRST Then sprdOrder.Row = ROW_FIRST
    sprdOrder.Col = COL_PARTCODE
    Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.Row)
    If sprdOrder.Row = ROW_FIRST And sprdOrder.Text = "" Then sprdOrder.EditMode = True
    cmdVoid.Visible = True

End Sub

Private Sub sprdOrder_KeyDown(KeyCode As Integer, Shift As Integer)

Dim strPartCode   As String
Dim strEnquiryEXE As String

    Call DebugMsg(MODULE_NAME, "KD", endlTraceIn, "KeyCode=" & KeyCode)
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF2): 'if F2 then display Fuzzy Match
                If fraSelection.Enabled = False Then Exit Sub 'no fuzzy if in Read Only mode
                sprdOrder.Col = COL_PARTCODE
                'check that Fuzzy is not being called on fixed line i.e. ordered item
                If sprdOrder.Lock = True Then
                    sprdOrder.Row = sprdOrder.MaxRows
                    If sprdOrder.Lock = True Then Exit Sub
                End If
                Call ucfiItemSearch.Initialise(goSession, Me)
                Call ucfiItemSearch.FillScreen(ucfiItemSearch)
                ucfiItemSearch.SupplierNo = strItemSuppNo
                ucfiItemSearch.Visible = True
                Exit Sub
            Case (vbKeyF11): 'if F11 then display item enquiry
                On Error GoTo Bad_Enquiry
                sprdOrder.Col = COL_PARTCODE
                strPartCode = sprdOrder.Text
                strEnquiryEXE = goSession.GetParameter(PRM_ENQUIRY_EXE)
                strEnquiryEXE = strEnquiryEXE & " " & goSession.CreateCommandLine("SKU=" & strPartCode)
                Call DebugMsg(MODULE_NAME, "callMenuOption", endlDebug, strEnquiryEXE)
                Call ShellWait(strEnquiryEXE, SW_MAX)
        End Select
    End If 'no shift selected

Exit Sub

Bad_Enquiry:

    Call MsgBoxEx("Invalid set-up for item enquiry" & vbCrLf & "  " & strEnquiryEXE & vbCrLf & Err.Number & "-" & Err.Description, vbExclamation, "Select Option", , , , , RGBMsgBox_WarnColour)
    Call Err.Clear
    Resume Next

End Sub

Private Sub sprdOrder_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) And (sprdOrder.EditMode = False) Then
        Call DebugMsg(MODULE_NAME, "KP", endlTraceIn, "KeyAscii=" & KeyAscii)

        Select Case (strActionCode)
            Case (ACTION_PORDER):  Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.SelBlockRow)
            Case (ACTION_MAINTPO):
                                    If lblEDIFlag.Visible = True Then
                                        Call sprdOrder.SetActiveCell(COL_QTY, sprdOrder.SelBlockRow)
                                    End If
            Case (ACTION_DELNOTE):
                                sprdOrder.Col = COL_QTY 'check if line for Purchase Order
                
                                sprdOrder.Row = sprdOrder.SelBlockRow
                                
                                Call DebugMsg("frmOrder", "sprdOrder_KeyPress", endlDebug, "ActiveCol " & sprdOrder.ActiveCol)
                                Call DebugMsg("frmOrder", "sprdOrder_KeyPress", endlDebug, "SelBlockCol " & sprdOrder.SelBlockCol)
                                
                                'Seems to work
                                If sprdOrder.ActiveCol = 1 Then
                                    If ((moEDIDelNote Is Nothing) = True) Then
                                        If Val(sprdOrder.Value) <> 0 Then
                                            Call sprdOrder.SetActiveCell(COL_QTYDEL, sprdOrder.SelBlockRow)
                                        Else
                                            Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.SelBlockRow)
                                        End If
                                    Else
                                        Call sprdOrder.SetActiveCell(COL_QTYSHORT, sprdOrder.SelBlockRow)
                                    End If
                                    
                                    Call sprdOrder.Refresh
                                End If

            Case (ACTION_ISTOUT):  Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.SelBlockRow)
            Case (ACTION_ISTIN):    sprdOrder.Col = COL_PARTCODE
                                    sprdOrder.Row = sprdOrder.SelBlockRow
                                    If sprdOrder.Lock = True Then
                                        Call sprdOrder.SetActiveCell(COL_QTYDEL, sprdOrder.SelBlockRow)
                                    Else
                                        Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.SelBlockRow)
                                    End If
            Case (ACTION_RETURN):  Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.SelBlockRow)
            Case (ACTION_MAINTDN): sprdOrder.Col = COL_PARTCODE
                                   sprdOrder.Row = sprdOrder.SelBlockRow
                                   If sprdOrder.Lock = False Then 'item exists on delivery note so can only edit qty's
                                       Call sprdOrder.SetActiveCell(COL_PARTCODE, sprdOrder.SelBlockRow)
                                   Else
                                       Call sprdOrder.SetActiveCell(COL_QTYDEL, sprdOrder.SelBlockRow)
                                       sprdOrder.Col = COL_QTYDEL
                                   End If
            Case (ACTION_MAINTRETURN): sprdOrder.Col = COL_QTYRET
                                   sprdOrder.Row = sprdOrder.SelBlockRow
                                   If sprdOrder.ColHidden = False Then 'item exists on delivery note so can only edit qty's
                                       Call sprdOrder.SetActiveCell(COL_QTYRET, sprdOrder.SelBlockRow)
                                       sprdOrder.Col = COL_QTYRET
                                   Else
                                       Call sprdOrder.SetActiveCell(COL_QTYSHORT, sprdOrder.SelBlockRow)
                                       sprdOrder.Col = COL_QTYSHORT
                                   End If
        End Select
        sprdOrder.EditMode = True
    End If
    If (KeyAscii = vbKeyEscape) And (sprdOrder.EditMode = False) Then
        Select Case (strActionCode)
            Case (ACTION_MAINTPO): Call SendKeys("+{tab}")
            Case Else
                If sprdOrder.Row = ROW_FIRST Then Call SendKeys("+{tab}")
        End Select
    End If

End Sub

Private Sub sprdOrder_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)

Dim strPart As String
    
    Call DebugMsg(MODULE_NAME, "sprdOrder_LeaveRow", endlDebug, "Row " & Row & "/" & sprdOrder.Col)
    sprdOrder.Row = NewRow
    sprdOrder.Col = COL_PARTCODE
    strPart = sprdOrder.Text
    sprdOrder.Col = COL_DESC
    sbStatus.Panels(PANEL_INFO).Text = strPart & "-" & sprdOrder.Text

End Sub

Private Sub sprdOrder_LostFocus()
    
    sprdOrder.SelBackColor = -1
    sprdOrder.SelForeColor = -1
    sbStatus.Panels(PANEL_INFO).Text = DEFAULT_MSG

End Sub



Private Sub dtxtDueDate_GotFocus()
    
    sbStatus.Panels(PANEL_INFO).Text = "Enter Date"
    dtxtDueDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtDueDate_LostFocus()

Dim dteDate             As Date
Dim blnAllowBackDateDel As Boolean
Dim strActControl       As String
    
    Call DebugMsg(MODULE_NAME, "dtxtDueDate_LostFocus", endlDebug, "date=" & dtxtDueDate.Text)
    sbStatus.Panels(PANEL_INFO).Text = "F1-Help"
    blnAllowBackDateDel = goSession.GetParameter(PRM_ALLOW_BACK_DATE_DEL_DATE)
    dteDate = GetDate(dtxtDueDate.Text)
    dtxtDueDate.BackColor = RGB_WHITE
    If dtxtDueDate.Text = "" Then Exit Sub 'do not check as processing not started
    If (dtxtDueDate.Visible = False) Or (dtxtDueDate.Enabled = False) Then Exit Sub  'do not check as not processing not started
    If Year(dteDate) = 1899 Then
        Call MsgBoxEx("Invalid date entered", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
        Call dtxtDueDate.SetFocus
        Exit Sub
    End If
    'if not capturing IST's then check entered date is a week day only
    If (strActionCode <> ACTION_ISTIN) And (strActionCode <> ACTION_ISTOUT) And (strActionCode <> ACTION_MAINTISTOUT) And (strActionCode <> ACTION_RETURN) And (strActionCode <> ACTION_MAINTRETURN) Then
        If Format(dteDate, "W") = 1 Or Format(dteDate, "W") = 7 Then
            strActControl = ActiveControl.Name
            Call MsgBoxEx("Date entered must occur on week day only" & vbCrLf & "Date rolled forward to next working date", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
            'Roll date forward to following Monday
            If Format(dteDate, "W") = 7 Then
                dtxtDueDate.Text = DisplayDate(DateAdd("d", 2, dteDate), False)
            Else
                dtxtDueDate.Text = DisplayDate(DateAdd("d", 1, dteDate), False)
            End If
            lblDateDay.Caption = Format(GetDate(dtxtDueDate.Text), "DDD") 'Altered M.Milne 17/11/03
            If strActControl = "sprdOrder" Then Call sprdOrder_GotFocus
            Exit Sub
        End If
    End If
    Select Case (strActionCode)
        Case (ACTION_MAINTPO):
            If dteDate < Format(Now, "YYYY-M-D") And (mblnDueEdited = True) Then
                Call MsgBoxEx("Date entered can not be before today", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
                Call dtxtDueDate.SetFocus
                Exit Sub
            End If
        Case (ACTION_PORDER), (ACTION_MAINTPO):
            If dteDate < Format(Now, "YYYY-M-D") Then
                Call MsgBoxEx("Date entered can not be before today", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
                dtxtDueDate.Text = DisplayDate(Now, False)
                Call dtxtDueDate.SetFocus
                Exit Sub
            End If
        Case (ACTION_RETURN), (ACTION_MAINTRETURN):
            If dteDate < GetDate(lblOrderDate.Caption) Then
                Call MsgBoxEx("Date entered can not be before request date", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
                dtxtDueDate.Text = lblOrderDate.Caption
                Call dtxtDueDate.SetFocus
                Exit Sub
            End If
            If dteDate > Format(Now, "YYYY-M-D") Then
                Call MsgBoxEx("Date entered can not be after today", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
                Call dtxtDueDate.SetFocus
                Exit Sub
            End If
        Case Else:
            If dteDate > Format(Now, "YYYY-M-D") Then
                Call MsgBoxEx("Date entered can not be after today", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
                Call dtxtDueDate.SetFocus
                Exit Sub
            End If
            If (strActionCode = ACTION_DELNOTE) Or (strActionCode = ACTION_MAINTDN) Then
                If (dteDate < GetDate(lblOrderDate.Caption)) And (blnAllowBackDateDel = False) Then
                    Call MsgBoxEx("Date entered can not be before order date", vbCritical, Me.Caption, , , , , RGBMsgBox_WarnColour)
                    Call dtxtDueDate.SetFocus
                    Exit Sub
                End If
            End If
    End Select
    lblDateDay.Caption = Format(dteDate, "DDD")
        
End Sub

Private Sub txtReference_GotFocus()

    txtReference.BackColor = RGBEdit_Colour
    txtReference.SelStart = 0
    txtReference.SelLength = Len(txtReference.Text)

End Sub

Private Sub txtReference_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        sprdOrder.Visible = True
        sprdTotal.Visible = True
        If (strActionCode <> ACTION_MAINTRETURN) Then cmdDelLine.Visible = True
        Call sprdOrder.SetFocus
        sprdOrder.EditMode = True
    End If
    If KeyAscii = vbKeyEscape Then
        If cmbDispatchType.Visible = True Then
            cmbDispatchType.SetFocus
        Else
            Call SendKeys("+{tab}")
        End If
    End If
    
End Sub

Private Sub txtReference_LostFocus()

    txtReference.BackColor = RGB_WHITE
    If ((strActionCode = ACTION_RETURN) Or (strActionCode = ACTION_MAINTRETURN)) And (txtReference.Visible = True) Then
        If txtReference.Text = "" Then
            Call MsgBoxEx("Supplier delivery note number(s) not entered", vbExclamation, "Capture delivery note", , , , , RGBMsgBox_WarnColour)
            Call txtReference.SetFocus
        End If
    End If
    
End Sub

Private Sub ucdnDelNotes_Apply(DeliveryNoteNumber As String)

Dim oDelNote     As Object
Dim lngLineNo    As Long
Dim oSuppRetLine As Object

Const PROCEDURE_NAME As String = MODULE_NAME & ".ucdnDelNotes_Apply"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    Set moDelNote = Nothing
    ucdnDelNotes.Visible = False
    sprdOrder.Visible = True
    sprdTotal.Visible = True
    fraSelection.Enabled = True
    If strActionCode = ACTION_MAINTDN Then
        fraSelection.Visible = True
        dtxtDueDate.SetFocus
    Else
        sprdOrder.SetFocus
        Call sprdOrder.SetActiveCell(COL_PARTCODE, ROW_FIRST) 'set focus bar on top line
    End If
    
    ucpbProgress.Visible = True
    DoEvents
    
    Set oDelNote = goDatabase.CreateBusinessObject(CLASSID_DELNOTEHEADER)
    Call oDelNote.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTEHEADER_DocNo, DeliveryNoteNumber)
    cmdViewNotes.Visible = False
    oDelNote.GetLines = True
    mstrNotes = ""
    If oDelNote.IBo_Load Then
        'display Progress Bar
        mblnUpdRetNote = False
        mblnUpdShrtNote = False
        fraSelection.Visible = True
        lblOrderID.Caption = oDelNote.DocNo
        Call DisplaySupplier(oDelNote.SupplierONumber, False)
        Call DisplayDelNoteLines(oDelNote)
        If strActionCode = ACTION_MAINTDN Then
            dtxtDueDate.Enabled = True
            lblReturnID.Caption = ""
            lblOrderDatelbl.Visible = False
            lblReturnID.Visible = False
            'for selected delivery note - check if return has been created
            Set moSuppRetNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
            Call moSuppRetNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_OriginalDelNoteDRL, DeliveryNoteNumber)
            Call moSuppRetNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_ShortageNote, False)
            moSuppRetNote.GetLines = True
            Call moSuppRetNote.IBo_Loadmatches
            If moSuppRetNote.DocNo <> "" Then
                lblReturnID.Caption = moSuppRetNote.SupplierReturnNumber
                lblOrderDatelbl.Visible = True
                lblReturnID.Visible = True
                Set moSuppRetNote.DelNote = oDelNote
                For lngLineNo = 1 To moSuppRetNote.Lines.Count Step 1
                    Set oSuppRetLine = moSuppRetNote.Lines(lngLineNo)
                    If oSuppRetLine.ReturnQty > 0 Then Call AppendReturnedLine(oSuppRetLine.PartCode, oSuppRetLine.LineNo, oSuppRetLine.ReturnQty, _
                            oSuppRetLine.ReturnReasonCode, oSuppRetLine.Narrative, oSuppRetLine.POLineNumber, TYPE_RETURN)
                Next lngLineNo
            Else
                Set moSuppRetNote = Nothing
            End If
            
            'Check if shortage note for Delivery Note
            Set moSuppShortNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
            Call moSuppShortNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_OriginalDelNoteDRL, DeliveryNoteNumber)
            Call moSuppShortNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_ShortageNote, True)
            moSuppShortNote.GetLines = True
            Call moSuppShortNote.IBo_Loadmatches
            If moSuppShortNote.DocNo <> "" Then
                lblReturnID.Caption = lblReturnID & "/" & moSuppShortNote.SupplierReturnNumber
                lblOrderDatelbl.Visible = True
                lblReturnID.Visible = True
                Set moSuppShortNote.DelNote = oDelNote
                For lngLineNo = 1 To moSuppShortNote.Lines.Count Step 1
                    Set oSuppRetLine = moSuppShortNote.Lines(lngLineNo)
                    If oSuppRetLine.ReturnQty > 0 Then Call AppendReturnedLine(oSuppRetLine.PartCode, oSuppRetLine.LineNo, oSuppRetLine.ReturnQty, _
                            "", "", oSuppRetLine.POLineNumber, TYPE_SHORTAGE)
                Next lngLineNo
            Else
                Set moSuppShortNote = Nothing
            End If
            
            'Check for overs
            If oDelNote.OrigDocNo <> "" Then
                Set moOverDelNote = goDatabase.CreateBusinessObject(CLASSID_DELNOTEHEADER)
                Call moOverDelNote.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTEHEADER_DocNo, oDelNote.OrigDocNo)
                
                moOverDelNote.GetLines = True
                Call moOverDelNote.IBo_Loadmatches
                
                If moOverDelNote.DocNo <> "" Then
                    For lngLineNo = 1 To moOverDelNote.Lines.Count Step 1
                        If moOverDelNote.Lines(lngLineNo).ReceivedQty > 0 Then
                            Call AppendReturnedLine(moOverDelNote.Lines(lngLineNo).PartCode, moOverDelNote.Lines(lngLineNo).LineNo, moOverDelNote.Lines(lngLineNo).ReceivedQty, _
                                                    "", "", 0, TYPE_OVER)
                        End If
                    Next lngLineNo
                End If
            End If
            
            'lock Order Quantity column for displayed order
            sprdOrder.Col = COL_QTY
            sprdOrder.Row = -1
            sprdOrder.Lock = True
            'Unlock Return Quantity column for displayed delivery note
            sprdOrder.Col = COL_QTYRET
            sprdOrder.Row = -1
            sprdOrder.Lock = False
            sprdDelNotes.MaxRows = 9
            sprdDelNotes.Col = 1
            'Display Delivery Note Numbers
            For lngLineNo = 1 To 9 Step 1
                sprdDelNotes.Row = lngLineNo
                sprdDelNotes.Text = oDelNote.SupplierDelNote(lngLineNo)
            Next lngLineNo
            'Step back through delivery notes and clear out blanks
            For lngLineNo = 9 To 1 Step -1
                sprdDelNotes.Row = lngLineNo
                If sprdDelNotes.Text <> "" Then
                    sprdDelNotes.MaxRows = lngLineNo
                    Exit For
                End If
            Next lngLineNo
            dtxtDueDate.Text = DisplayDate(oDelNote.ReceiptDate, False)
            lblOrderDate.Caption = DisplayDate(oDelNote.DelNoteDate, False)
            lblSOQNumber.Caption = oDelNote.SOQControlNumber
'            lblReleaseNo.Caption = oDelNote.ConsignmentRef
            lblReleaseNo.Caption = oDelNote.AssignedPORelease
            mstrNotes = oDelNote.Narrative
            If mstrNotes <> "" Then cmdViewNotes.Visible = True
            cmdVoid.Visible = True
            If oDelNote.Commit = "Y" Then
                fraSelection.Enabled = False
                If (mblnDNAddLines = True) Then sprdOrder.MaxRows = sprdOrder.MaxRows - 1
                sprdOrder.Col = -1
                sprdOrder.Row = -1
                sprdOrder.Lock = True
                lblSubmitted.Visible = True
            Else
                fraSelection.Enabled = True
                lblSubmitted.Visible = False
            End If
        End If
        lblOrderNo.Caption = oDelNote.PONumber
        lblOrderNo.Caption = oDelNote.DocNo
        sprdOrder.Row = ROW_FIRST
        If fraSelection.Enabled = True Then
            sprdDelNotes.SetFocus
        Else
            sprdOrder.SetFocus
        End If
    End If
    sprdOrder.Refresh
'    cmdDelete.Visible = True
    cmdRePrint.Visible = True
    cmdSave.Visible = True
    Set moDelNote = oDelNote
    
    'Tidy up
    GoSub Finally

   
    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report(APP_NAME, PROCEDURE_NAME, Erl)
    Call Err.Clear
    Exit Sub
    Resume Next

Finally:
    'Perform tidy up here
    ucpbProgress.Visible = False
    Return
    '</CAEH>

End Sub
Private Sub ucdnDelNotes_Cancel()
    
    ucdnDelNotes.Visible = False
    Call cmdClose_Click

End Sub

Private Sub ucdnDelNotes_LostFocus()
    
    If ucdnDelNotes.Visible = True Then Call ucdnDelNotes_Cancel

End Sub

Private Sub ucfiItemSearch_Apply(PartCode As String)

    ucfiItemSearch.Visible = False
    DoEvents
    sprdOrder.SetFocus
    sprdOrder.Col = COL_PARTCODE
    Call sprdOrder.SetActiveCell(sprdOrder.Col, sprdOrder.Row)
    sprdOrder.Col = COL_PARTCODE
    sprdOrder.Text = PartCode
    sprdOrder.Col = COL_PARTCODE
    Call sprdOrder.SetActiveCell(sprdOrder.Col, sprdOrder.Row)
    Call DebugMsg(MODULE_NAME, "Apply", endlDebug, "Apply-" & PartCode & " into R/C(" & sprdOrder.Row & "/" & sprdOrder.Col & ")")
    Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.Row, 0, True)
    sprdOrder.EditMode = True

End Sub

Private Sub ucfiItemSearch_Cancel()

    sprdOrder.SetFocus
    ucfiItemSearch.Visible = False

End Sub

Private Sub ucfiItemSearch_LostFocus()
    
    sprdOrder.SetFocus
    ucfiItemSearch.Visible = False

End Sub

Private Sub ucioLookup_Apply(ISTOutNumber As String)

Const PROCEDURE_NAME As String = MODULE_NAME & ".ucioLookUp_Apply"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    ucioLookup.Visible = False
    
    ucpbProgress.Visible = True
    DoEvents
    
    mstrNotes = ""
    cmdViewNotes.Visible = False
    Call LoadISTOut("", ISTOutNumber)
    fraSelection.Visible = True
    cmdDelete.Visible = True
    cmdRePrint.Visible = True
'    cmdSave.Visible = True
    cmdClose.Visible = True
    cmdVoid.Visible = True
    
    'Tidy up
    GoSub Finally

    sprdOrder.Col = -1
    sprdOrder.Row = -1
    sprdOrder.Lock = True
    sprdTotal.Visible = True
    Call sprdOrder.SetActiveCell(COL_PARTCODE, ROW_FIRST) 'set focus bar on top line
    
    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report(APP_NAME, PROCEDURE_NAME, Erl)
    Call Err.Clear
    Exit Sub
    Resume Next

Finally:
    'Perform tidy up here
    ucpbProgress.Visible = False
    Return
    '</CAEH>

End Sub

Private Sub ucioLookup_Cancel()

    ucioLookup.Visible = False
    Call cmdClose_Click

End Sub

Private Sub ucioLookup_LostFocus()

    If ucioLookup.Visible = True Then Call ucioLookup_Cancel

End Sub

'<CACH>****************************************************************************************
'* Sub:  ucofOrders_Apply()
'**********************************************************************************************
'* Description: called by the Purchase Order selection control.  Once called, this displays
'*              the selected order
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub ucofOrders_Apply()

Dim oPOrder      As Object
Dim lngDelLineNo As Long
Dim lngPOLineNo  As Long
Dim blnLineFound As Boolean

Const PROCEDURE_NAME As String = MODULE_NAME & ".ucofOrders_Apply"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
    ucofOrders.Visible = False

    fraSelection.Visible = True
    fraAddress.Visible = True
    cmdVoid.Visible = True
    cmdViewNotes.Visible = False
    mstrNotes = ""
    DoEvents
    cmdClose.Visible = True
    Select Case (strActionCode)
        Case (ACTION_MAINTPO):
                                dtxtDueDate.BackColor = RGB_RED
                                dtxtDueDate.SetFocus
                                sprdOrder.Visible = True
                                sprdTotal.Visible = True
        Case (ACTION_DELNOTE):  sprdDelNotes.SetFocus
        Case Else:              sprdOrder.Visible = True
                                sprdOrder.SetFocus
                                Call sprdOrder.SetActiveCell(COL_PARTCODE, ROW_FIRST) 'set focus bar on top line
    End Select
    
    ucpbProgress.Visible = True
    DoEvents
    
    Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
    Call oPOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_Key, ucofOrders.OrderKey)
    Select Case (strActionCode)
        Case (ACTION_MAINTPO):    oPOrder.GetAllLines = True
        Case (ACTION_DELNOTE):    oPOrder.GetOSLines = True
        Case Else:                oPOrder.GetAllLines = True
    End Select
    mstrNotes = ""
    If oPOrder.IBo_Load Then
        'display Progress Bar
        lblOrderID.Caption = oPOrder.Key
        Call DisplaySupplier(oPOrder.SupplierNo, False)
        If (lblEDIFlag.Visible = True) And (strActionCode = ACTION_MAINTPO) Then
            Call MsgBoxEx("Any amendments to this Purchase Order will be submitted to the Supplier " & _
                            "and are subject to the normal availability checks and routines applied " & _
                            "to our orders" & vbCrLf & vbCrLf & "You are advised to confirm this with the Supplier " & _
                            "before amending the order.", vbOKOnly, "Amend EDI Purchase Order", , , , , RGBMsgBox_WarnColour)
        End If
        Call DisplayOrderLines(oPOrder, mblnBookExcept, True, Nothing)
        If (strActionCode = ACTION_MAINTPO) Then
            dtxtDueDate.BackColor = RGB_RED
            dtxtDueDate.SetFocus
            'lock Order Quantity column for displayed order
            sprdOrder.Col = COL_QTY
            sprdOrder.Row = -1
            If (lblEDIFlag.Visible = False) Then
                sprdOrder.Lock = True
            Else
                sprdOrder.Lock = False
            End If
            mstrNotes = oPOrder.Narrative
            If mstrNotes <> "" Then cmdViewNotes.Visible = True
        End If
        If (strActionCode = ACTION_PORDER) Or (strActionCode = ACTION_MAINTPO) Then
            lblOrderDate.Caption = DisplayDate(oPOrder.OrderDate, False)
            dtxtDueDate.Text = DisplayDate(oPOrder.DueDate, False)
        Else
            lblOrderDate.Caption = DisplayDate(oPOrder.OrderDate, False)
            dtxtDueDate.Text = DisplayDate(Date, False)
        End If
        sprdDelNotes.MaxRows = 1
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = -1
        sprdDelNotes.Text = ""
        lblOrderNo.Caption = oPOrder.OrderNumber
        lblReleaseNo.Caption = oPOrder.ReleaseNumber
        lblSOQNumber.Caption = oPOrder.SOQNumber
        'Check for any Electronic Delivery Notes to process
        If (lblEDIFlag.Visible = True) And (strActionCode = ACTION_DELNOTE) Then
            Load frmEDIDelNotes
            Set moEDIDelNote = frmEDIDelNotes.RetrieveEDIDelNote(oPOrder.OrderNumber, lblSuppNo.Caption & "-" & lblSuppName.Caption)
            Unload frmEDIDelNotes
            If ((moEDIDelNote Is Nothing) = False) Then 'display EDI Del Note
                sprdDelNotes.Col = 1
                sprdDelNotes.Row = 1
                sprdDelNotes.MaxRows = 2
                sprdDelNotes.Text = moEDIDelNote.DeliveryNoteNumber
                dtxtDueDate.Text = DisplayDate(moEDIDelNote.DeliveryDate, False)
                'Match edi lines to Purchase Order Lines
                For lngDelLineNo = 1 To moEDIDelNote.Lines.Count Step 1
                    blnLineFound = False
                    For lngPOLineNo = 2 To sprdOrder.MaxRows Step 1
                        sprdOrder.Row = lngPOLineNo
                        sprdOrder.Col = COL_PARTCODE
                        If (sprdOrder.Text = moEDIDelNote.Lines(CLng(lngDelLineNo)).PartCode) Then
                            sprdOrder.Col = COL_QTYDEL
                            sprdOrder.Text = moEDIDelNote.Lines(CLng(lngDelLineNo)).DeliveryQuantity
                            Call sprdOrder_EditMode(COL_QTYDEL, sprdOrder.Row, 0, True)
                            sprdOrder.Col = COL_QTYDEL
                            sprdOrder.Lock = True
                            sprdOrder.Col = COL_QTYSHORT
                            sprdOrder.Lock = False
                            blnLineFound = True
                            Exit For
                        End If 'match found on Purchase Order
                    Next lngPOLineNo
                    If (blnLineFound = False) Then
                        sprdOrder.Row = sprdOrder.MaxRows
                        sprdOrder.Col = COL_PARTCODE
                        sprdOrder.Text = moEDIDelNote.Lines(CLng(lngDelLineNo)).PartCode
                        Call sprdOrder_EditMode(COL_PARTCODE, sprdOrder.Row, 0, True)
                        sprdOrder.Col = COL_PARTCODE
                        sprdOrder.Lock = True
                        sprdOrder.Col = COL_QTYDEL
                        sprdOrder.Text = moEDIDelNote.Lines(CLng(lngDelLineNo)).DeliveryQuantity
                        Call sprdOrder_EditMode(COL_QTYDEL, sprdOrder.Row, 0, True)
                        sprdOrder.Col = COL_QTYDEL
                        sprdOrder.Lock = True
                        sprdOrder.Col = COL_QTYSHORT
                        sprdOrder.Lock = False
                     End If
                Next lngDelLineNo
                sprdOrder.Row = 1
                Call sprdOrder.SetActiveCell(0, 1)
                If (dtxtDueDate.Visible = True) Then Call dtxtDueDate.SetFocus
            Else 'no EDI Delivery Note so prompt to cancel operation
                If MsgBoxEx("Unable to locate EDI Delivery Note for EDI Purchase Order.  Manual Stock Receipts should not be performed." & vbCrLf & vbCrLf & "Do you wish to proceed" & vbCrLf, vbOKCancel, "Perform Manual Stock Receipt", , , , , RGBMsgBox_WarnColour) = vbCancel Then
                    ucpbProgress.Visible = False
                    fraSelection.Visible = False
                    fraAddress.Visible = False
                    cmdVoid.Visible = True
                    cmdViewNotes.Visible = False
                    cmdClose.Visible = False
                    Call ucofOrders.Reset
                    Call ucofOrders.Show
                    ucofOrders.Visible = True
                    Exit Sub
                End If
            End If
        End If
        'Reset any Update flags stored in Column 0
        For lngPOLineNo = 2 To sprdOrder.MaxRows Step 1
            sprdOrder.Row = lngPOLineNo
            sprdOrder.Col = 0
            sprdOrder.Text = " "
        Next lngPOLineNo
        If (mblnDNAddLines = False) Then
            sprdOrder.MaxRows = sprdOrder.MaxRows - 1 'remove last Auto line added
        Else
            If ((moEDIDelNote Is Nothing) = True) Then
                sprdOrder.Row = sprdOrder.MaxRows
                sprdOrder.Col = COL_QTY
                sprdOrder.BackColor = RGB_LTGREY
                sprdOrder.Col = COL_QTYOS
                sprdOrder.BackColor = RGB_LTGREY
                sprdOrder.Col = COL_QTYBORD
                sprdOrder.BackColor = RGB_LTGREY
            Else
                sprdOrder.MaxRows = sprdOrder.MaxRows - 1 'remove last Auto line added
            End If
        End If
        
        sprdOrder.Row = ROW_FIRST
        If oPOrder.ConsolidationLines.Count > 0 Then
            cmdViewCons.Visible = True
            Set mcolConLines = oPOrder.ConsolidationLines
        Else
            cmdViewCons.Visible = False
            Set mcolConLines = Nothing
        End If
    End If
    sprdOrder.Refresh
    mblnDueEdited = False
    If strActionCode = ACTION_MAINTPO Then
        cmdDelete.Visible = True
        cmdRePrint.Visible = True
        cmdSave.Visible = True
    End If
    
    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report(APP_NAME, PROCEDURE_NAME, Erl)
    Call Err.Clear
    Exit Sub
Resume Next
Finally:
    'Perform tidy up here
    ucpbProgress.Visible = False
    Return
    '</CAEH>

End Sub 'ucofOrders_Apply


Private Sub ucofOrders_Cancel()

    'force control to perform Hide event, else error 440 occurs
    ucofOrders.Visible = False
    DoEvents
    Call cmdClose_Click

End Sub


Private Sub ucofOrders_LostFocus()

    If ucofOrders.Visible = True Then Call ucofOrders_Cancel

End Sub

Private Sub ucsrReturns_Apply()

Const PROCEDURE_NAME As String = MODULE_NAME & ".ucsrReturns_Apply"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    ucsrReturns.Visible = False
    
    ucpbProgress.Visible = True
    cmdViewNotes.Visible = False
    DoEvents
    Set moDelNote = Nothing
    Set moSuppRetNote = goDatabase.CreateBusinessObject(CLASSID_RETURNNOTE)
    Call moSuppRetNote.IBo_AddLoadFilter(CMP_EQUAL, FID_RETURNNOTE_DocNo, ucsrReturns.SupplierReturnNumber)
    moSuppRetNote.GetLines = True
    mstrNotes = ""
    Call moSuppRetNote.IBo_Loadmatches
    If moSuppRetNote.SupplierNumber <> "" Then
        'display Progress Bar
        fraSelection.Visible = True
'        txtReference.Text = mosuppretnote.OriginalDelNoteDRL
        txtReference.Text = moSuppRetNote.Comment
        lblMCPMin.Caption = moSuppRetNote.OriginalDelNoteDRL
        lblReleaseNo.Caption = moSuppRetNote.ReleaseNumber
        'if no Original DRL Number then mark as N/A and allow entry/edit of Supplier Del Notes No
        If lblMCPMin.Caption = "" Then
            lblMCPMin.Caption = "N/A"
            txtReference.Enabled = True
        Else
            txtReference.Enabled = False
            'If DRL number then get Delivery note
            Set moDelNote = goDatabase.CreateBusinessObject(CLASSID_DELNOTEHEADER)
            Call moDelNote.IBo_AddLoadFilter(CMP_EQUAL, FID_DELNOTEHEADER_DocNo, moSuppRetNote.OriginalDelNoteDRL)
            moDelNote.GetLines = True
            Call moDelNote.IBo_Loadmatches
            Set moSuppRetNote.DelNote = moDelNote
        End If
        If moSuppRetNote.ShortageNote = True Then
            sprdOrder.Col = COL_QTYSHORT
            sprdOrder.ColHidden = False
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_RETCMNT
            sprdOrder.ColHidden = True
            lblDelNoteNoLbl.Visible = False
            sprdDelNotes.Visible = False
            dtxtDueDate.Visible = False
            lblUserDate.Visible = False
            Call ShowTotal(COL_TOTQTYRET, False)
            Call ShowTotal(COL_TOTRETVAL, False)
            Call ShowTotal(COL_TOTQTYSHRT, True)
            Call ShowTotal(COL_TOTSHRTVAL, True)
            sprdTotal.Row = 1
            sprdTotal.Col = COL_TOTINFO
            sprdTotal.Text = "SHORTAGE"
            sprdTotal.Row = 2
        Else
            sprdOrder.Col = COL_QTYSHORT
            sprdOrder.ColHidden = True
            sprdOrder.Col = COL_QTYRET
            sprdOrder.ColHidden = False
            lblDelNoteNoLbl.Visible = True
            sprdDelNotes.Visible = True
            sprdOrder.Col = COL_RETCMNT
            sprdOrder.ColHidden = False
            dtxtDueDate.Visible = True
            lblUserDate.Visible = True
            Call ShowTotal(COL_TOTQTYRET, True)
            Call ShowTotal(COL_TOTRETVAL, True)
            Call ShowTotal(COL_TOTQTYSHRT, False)
            Call ShowTotal(COL_TOTSHRTVAL, False)
            sprdTotal.Row = 1
            sprdTotal.Col = COL_TOTINFO
            sprdTotal.Text = "RETURN"
            sprdTotal.Row = 2
        End If
        Call SetTotalsWidth
        lblOrderID.Caption = moSuppRetNote.DocNo
        lblRetRequestID.Caption = moSuppRetNote.SupplierReturnNumber
        Call DisplaySupplier(moSuppRetNote.SupplierNumber, False)
        Call DisplayReturnNoteLines(moSuppRetNote, moDelNote)
        If strActionCode = ACTION_MAINTRETURN Then
            If (dtxtDueDate.Visible = True) And (dtxtDueDate.Enabled = True) Then
                dtxtDueDate.SetFocus
            Else
                If txtReference.Enabled = True Then
                    txtReference.SetFocus
                Else
                    Call txtReference_KeyPress(vbKeyReturn)
                End If
            End If
            
            dtxtDueDate.Text = ""
            If (moSuppRetNote.ShortageNote = False) And (Year(moSuppRetNote.ReturnDate) > 1899) Then
                If moSuppRetNote.ReturnDate < moSuppRetNote.DelNoteDate Then
                    dtxtDueDate.Text = DisplayDate(Now(), False)
                Else
                    dtxtDueDate.Text = DisplayDate(moSuppRetNote.ReturnDate, False)
                End If
            End If
            lblOrderDate.Caption = DisplayDate(moSuppRetNote.DelNoteDate, False)
            
            mstrNotes = moSuppRetNote.Narrative
            If mstrNotes <> "" Then cmdViewNotes.Visible = True
            strMaintUser = moSuppRetNote.EnteredBy
        End If
        sprdDelNotes.MaxRows = 1
        sprdDelNotes.Col = 1
        sprdDelNotes.Row = -1
        sprdDelNotes.Text = moSuppRetNote.CollectionNoteNumber
        If moSuppRetNote.CollectionNoteNumber <> "" Then dtxtDueDate.Enabled = True
        lblOrderNo.Caption = moSuppRetNote.DocNo
        sprdOrder.MaxRows = sprdOrder.MaxRows - 1 'remove last Auto line added
        sprdOrder.Row = ROW_FIRST
        If (moSuppRetNote.CollectionNoteNumber = "") And (moSuppRetNote.ShortageNote = False) Then
            dtxtDueDate.Enabled = False
        End If
    End If
    sprdOrder.Visible = True
    sprdTotal.Visible = True
    sprdOrder.Refresh
    cmdRePrint.Visible = True
    cmdSave.Visible = True
    cmdVoid.Visible = True
    cmdClose.Visible = True
    
    'Tidy up
    GoSub Finally

    If sprdDelNotes.Visible = True Then
        Call sprdDelNotes.SetFocus
    Else
        If txtReference.Enabled Then
            Call txtReference.SetFocus
        Else
            Call sprdOrder.SetFocus
        End If
    End If
   
    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report(APP_NAME, PROCEDURE_NAME, Erl)
    Call Err.Clear
    Exit Sub
    Resume Next

Finally:
    'Perform tidy up here
    ucpbProgress.Visible = False
    Return
    '</CAEH>

End Sub

Private Sub ucsrReturns_Cancel()

    ucsrReturns.Visible = False
    Call cmdClose_Click

End Sub

Private Sub ucsrReturns_LostFocus()

    If ucsrReturns.Visible = True Then Call ucsrReturns_Cancel

End Sub
