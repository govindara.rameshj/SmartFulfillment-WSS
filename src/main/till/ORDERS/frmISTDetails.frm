VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "fpSPR70.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmISTDetails 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "View IST Details"
   ClientHeight    =   7425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10935
   Icon            =   "frmISTDetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   10935
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdUse 
      Caption         =   "F5-Use"
      Height          =   375
      Left            =   9600
      TabIndex        =   1
      Top             =   6480
      Width           =   1215
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   6480
      Width           =   1215
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "F12-Close"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   6480
      Width           =   1215
   End
   Begin VB.Frame fraDetails 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Details"
      Height          =   6255
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   10695
      Begin FPSpread.vaSpread sprdDetail 
         Height          =   5895
         Left            =   240
         TabIndex        =   0
         Top             =   240
         Width           =   10215
         _Version        =   393216
         _ExtentX        =   18018
         _ExtentY        =   10398
         _StockProps     =   64
         AllowCellOverflow=   -1  'True
         ColsFrozen      =   3
         EditModeReplace =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   6
         MaxRows         =   1
         NoBeep          =   -1  'True
         OperationMode   =   2
         RowHeaderDisplay=   0
         ScrollBarExtMode=   -1  'True
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmISTDetails.frx":058A
         UserResize      =   1
         ScrollBarTrack  =   1
      End
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   7050
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmISTDetails.frx":0B31
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11933
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:39"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmISTDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmISTDetails
'* Date   : 16/04/04
'* Author : damians
'*$Archive: /Projects/OasysV2/VB/Orders/frmISTDetails.frm $
'**********************************************************************************************
'* Summary: Displays the details of any pending IST's
'</CAMH>***************************************************************************************
Option Explicit

Private Const MODULE_NAME       As String = "frmISTDetails"

'sprdDetails column constants
Private Const COL_ITEMNO    As Long = 1
Private Const COL_SKU       As Long = 2
Private Const COL_DESC      As Long = 3
Private Const COL_SIZE      As Long = 4
Private Const COL_DQTY      As Long = 5
Private Const COL_ISTNO     As Long = 6

Private mlngQtyDecNum       As Long
Private mlngValueDecNum     As Long

Public mstrIST              As String ' Holds return value

Private Sub cmdPrint_Click()

    Call GetPrintStore
    
    sprdDetail.PrintFooter = GetFooter
    
    sprdDetail.PrintColHeaders = True
    sprdDetail.PrintRowHeaders = True
    sprdDetail.PrintSmartPrint = False
    sprdDetail.PrintGrid = False
    
    Call sprdDetail.PrintSheet

End Sub

Private Sub cmdUse_Click()
    
    sprdDetail.Row = sprdDetail.ActiveRow
    
    sprdDetail.Col = COL_ISTNO
    
    mstrIST = sprdDetail.Text
    
    Me.Hide
    
End Sub

Private Sub cmdClose_Click()

    Unload Me

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then 'Key pressed with no Shift/Alt/Ctrl etc. combination
        Select Case (KeyCode)
            Case (vbKeyF5): 'if F5 then process
                KeyCode = 0 'cancel keystroke or calling form receives it
                cmdUse_Click
            Case (vbKeyF9): 'if F9 then print details
                KeyCode = 0 'cancel keystroke or calling form receives it
                Call cmdPrint_Click
            Case (vbKeyF12): 'if F12 then close details
                KeyCode = 0 'cancel keystroke or calling form receives it
                Call cmdClose_Click
        End Select
    End If
    
End Sub

Private Sub Form_Load()

Const PROCEDURE_NAME As String = MODULE_NAME & ".Form_Load"

    On Error GoTo errHandler

    Screen.MousePointer = vbHourglass
    
    'Display initial values in Status Bar
    Call InitialiseStatusBar(sbStatus)
       
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    'Maximize the Form
    Me.WindowState = vbMaximized
    
    mlngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    mlngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
          
    sprdDetail.MaxRows = 0
    
    Call ShowPendingISTDetail
    
    Screen.MousePointer = vbDefault
    
    Exit Sub
    
errHandler:

    Screen.MousePointer = vbDefault
    
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub DoISTItems(ByRef oHeader As Object)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoISTItems"

Dim oItem           As Object
Dim oInventory      As Object
Dim lngFirstLine    As Long
Dim lngItemLoop     As Long

    On Error GoTo errHandler
    
    lngFirstLine = sprdDetail.MaxRows + 1
    
    'Populate items
    oHeader.GetLines = True
       
    For lngItemLoop = 1 To oHeader.Lines.Count
        Set oItem = oHeader.Lines(lngItemLoop)
        
        sprdDetail.MaxRows = sprdDetail.MaxRows + 1
        sprdDetail.Row = sprdDetail.MaxRows
        
        sprdDetail.Col = COL_ITEMNO
        sprdDetail.Text = oItem.LineNumber
        sprdDetail.FontBold = False
        
        'Add Partcode
        sprdDetail.Col = COL_SKU
        sprdDetail.Text = oItem.PartCode
        sprdDetail.FontBold = False
        
        'Create inventory business object
        Set oInventory = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        
        'Add fields we want to return
        Call oInventory.IBo_AddLoadField(FID_INVENTORY_PartCode)
        Call oInventory.IBo_AddLoadField(FID_INVENTORY_Description)
        Call oInventory.IBo_AddLoadField(FID_INVENTORY_SizeDescription)
        
        'Add partcode filter
        Call oInventory.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oItem.PartCode)
        
        'Find Match
        Call oInventory.IBo_LoadMatches
        
        If oInventory.PartCode <> "" Then
            sprdDetail.Col = COL_DESC
            sprdDetail.Text = oInventory.Description
            sprdDetail.FontBold = False
            
            sprdDetail.Col = COL_SIZE
            sprdDetail.Text = oInventory.SizeDescription
            sprdDetail.FontBold = False
        End If
        
        'IST Quantities
        sprdDetail.Col = COL_DQTY
        sprdDetail.Text = oItem.Quantity
        sprdDetail.FontBold = False
        sprdDetail.CellType = CellTypeNumber
        sprdDetail.TypeNumberDecPlaces = mlngQtyDecNum
        
        'Add IST Out Number for use with F5
        sprdDetail.Col = COL_ISTNO
        
        sprdDetail.Text = oHeader.ISTOutNumber
    Next
    
    'Sort Items
    Call sprdDetail.Sort(COL_ITEMNO, lngFirstLine, COL_DQTY, sprdDetail.Row, SortByRow, COL_ITEMNO, 1)
       
    Exit Sub
    
errHandler:
    
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub DoISTHeader(ByRef oHeader As Object)

Const PROCEDURE_NAME As String = MODULE_NAME & ".DoISTHeader"

    On Error GoTo errHandler
    
    sprdDetail.MaxRows = sprdDetail.MaxRows + 1
    sprdDetail.Row = sprdDetail.MaxRows
    
    'Add IST Out Number for use with F5
    sprdDetail.Col = COL_ISTNO
        
    sprdDetail.Text = oHeader.ISTOutNumber
    
    sprdDetail.MaxRows = sprdDetail.MaxRows + 1
    sprdDetail.Row = sprdDetail.MaxRows
    
    'Add IST Out Number for use with F5
    sprdDetail.Col = COL_ISTNO
        
    sprdDetail.Text = oHeader.ISTOutNumber
    
    'Store
    sprdDetail.Col = COL_SKU
    sprdDetail.Text = "Store:"
    sprdDetail.FontBold = True
    
    sprdDetail.Col = COL_DESC
    sprdDetail.Text = oHeader.SourceStoreNumber & " - " & GetStoreName(oHeader.SourceStoreNumber)
    
    sprdDetail.FontBold = False
    
    'IST Number
    sprdDetail.Col = COL_SIZE
    sprdDetail.Text = "IST No:"
    sprdDetail.FontBold = True
        
    sprdDetail.Col = COL_DQTY
    sprdDetail.Text = oHeader.ISTOutNumber
    sprdDetail.FontBold = False
    sprdDetail.CellType = CellTypeEdit
    
    sprdDetail.MaxRows = sprdDetail.MaxRows + 1
    sprdDetail.Row = sprdDetail.MaxRows
    
    'Add IST Out Number for use with F5
    sprdDetail.Col = COL_ISTNO
        
    sprdDetail.Text = oHeader.ISTOutNumber
    
    'Sent Date
    sprdDetail.Col = COL_SKU
    sprdDetail.Text = "Sent Date:"
    sprdDetail.FontBold = True
    
    sprdDetail.Col = COL_DESC
    sprdDetail.Text = DisplayDate(oHeader.SentDate, False)
    sprdDetail.FontBold = False
    sprdDetail.CellType = CellTypeDate
    
    'Total Quanity
    sprdDetail.Col = COL_SIZE
    sprdDetail.Text = "Total Qty:"
    sprdDetail.FontBold = True
    
    sprdDetail.Col = COL_DQTY
    sprdDetail.Text = oHeader.TotalQuantity
    sprdDetail.FontBold = False
    sprdDetail.CellType = CellTypeNumber
    sprdDetail.TypeNumberDecPlaces = mlngQtyDecNum
    
    sprdDetail.MaxRows = sprdDetail.MaxRows + 1
    sprdDetail.Row = sprdDetail.MaxRows
    
    'Add IST Out Number for use with F5
    sprdDetail.Col = COL_ISTNO
       
    sprdDetail.Text = oHeader.ISTOutNumber
    
    'Total Value
    sprdDetail.Col = COL_SIZE
    sprdDetail.Text = "Total Value:"
    sprdDetail.FontBold = True
   
    sprdDetail.Col = COL_DQTY
    sprdDetail.Text = oHeader.TotalValue
    sprdDetail.FontBold = False
    sprdDetail.CellType = CellTypeCurrency
    
    sprdDetail.MaxRows = sprdDetail.MaxRows + 1
    sprdDetail.Row = sprdDetail.MaxRows
    
    'Add IST Out Number for use with F5
    sprdDetail.Col = COL_ISTNO
        
    sprdDetail.Text = oHeader.ISTOutNumber
    
    'Total Cost
    sprdDetail.Col = COL_SIZE
    sprdDetail.Text = "Total Cost:"
    sprdDetail.FontBold = True
        
    sprdDetail.Col = COL_DQTY
    sprdDetail.Text = oHeader.TotalCost
    sprdDetail.FontBold = False
    sprdDetail.CellType = CellTypeCurrency
    
    sprdDetail.MaxRows = sprdDetail.MaxRows + 1
    sprdDetail.Row = sprdDetail.MaxRows
    
    'Add IST Out Number for use with F5
    sprdDetail.Col = COL_ISTNO
        
    sprdDetail.Text = oHeader.ISTOutNumber

    Exit Sub
    
errHandler:
    
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub ShowPendingISTDetail()

Const PROCEDURE_NAME As String = MODULE_NAME & ".ShowPendingISTDetail"

Dim oHeader         As Object
Dim oItem           As Object
Dim colIST          As Collection
Dim lngHeaderLoop   As Long
Dim lngItemLoop     As Long

    On Error GoTo errHandler
    
    'Create IST HEADER Business object
    Set oHeader = goDatabase.CreateBusinessObject(CLASSID_ISTINEDIHEADER)
    
    'Add Received filter
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_Received, False)
            
    'Find matches
    Set colIST = oHeader.IBo_LoadMatches()
    
    sprdDetail.ReDraw = False
    
    For lngHeaderLoop = 1 To colIST.Count
        'Add header
        Call DoISTHeader(colIST(lngHeaderLoop))
        
        'Add ist items
        Call DoISTItems(colIST(lngHeaderLoop))
        
        sprdDetail.MaxRows = sprdDetail.MaxRows + 2
        
        sprdDetail.Row = sprdDetail.MaxRows - 1
        
        'Add IST Out Number for use with F5
        sprdDetail.Col = COL_ISTNO
        
        sprdDetail.Text = colIST(lngHeaderLoop).ISTOutNumber
        
        'Add Footer row
        sprdDetail.Row = sprdDetail.MaxRows
        
        sprdDetail.RowHeight(sprdDetail.Row) = 1
    Next
    
    sprdDetail.ReDraw = True
    
    Exit Sub
    
errHandler:
    
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Sub

Private Sub Form_Resize()
    
    'Check the form is not minized
    If Me.WindowState = vbMinimized Then Exit Sub
    
    If Me.Width < 5000 Then
        Me.Width = 5000
        Exit Sub
    End If
    
    If Me.Height < 3000 Then
        Me.Height = 3000
        Exit Sub
    End If
    
    'Resize main frame
    fraDetails.Width = Me.Width - (fraDetails.Left * 3)
    fraDetails.Height = Me.Height - fraDetails.Top - cmdClose.Height - 240 - (sbStatus.Height * 2)
    
    'Move command buttons
    cmdClose.Top = fraDetails.Top + fraDetails.Height + 120
    cmdPrint.Top = cmdClose.Top
    cmdUse.Top = cmdClose.Top
    cmdUse.Left = fraDetails.Left + fraDetails.Width - cmdUse.Width
    
    'Resize spread sheet
    sprdDetail.Width = fraDetails.Width - sprdDetail.Left * 2
    sprdDetail.Height = fraDetails.Height - sprdDetail.Top - 240
        
End Sub

Private Function GetStoreName(strStoreNumber As String) As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetStoreName"

Dim oStore      As Object

    On Error GoTo errHandler

    Set oStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
    
    Call oStore.IBo_AddLoadField(FID_STORE_AddressLine1)
    
    Call oStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, strStoreNumber)
    
    Call oStore.IBo_LoadMatches
    
    GetStoreName = oStore.AddressLine1
    
    Exit Function
    
errHandler:
    
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    
End Function

Private Sub sprdDetail_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        Call cmdUse_Click
    End If
    
End Sub
