Attribute VB_Name = "modOrders"
Option Explicit

Public Const COL_CONS_ITEMNO As Long = 1
Public Const COL_CONS_PRN_POLINENO As Long = 2 'Added for print consolidation sheet
Public Const COL_CONS_PARTCODE As Long = 3
Public Const COL_CONS_DESC As Long = 4
Public Const COL_CONS_SIZE As Long = 5
Public Const COL_CONS_PORDQTY As Long = 6
Public Const COL_CONS_CORDQTY As Long = 7
Public Const COL_CONS_MANUCODE As Long = 8
Public Const COL_CONS_PRICE As Long = 9
Public Const COL_CONS_COST As Long = 10
Public Const COL_CONS_SELLUNITS As Long = 11
Public Const COL_CONS_POLINENO As Long = 12
Public Const COL_CONS_CORDNO As Long = 13
Public Const COL_CONS_CORDLINENO As Long = 14
Public Const COL_CONS_CORDNAME As Long = 15
Public Const COL_CONS_PRN_CORDLINEQTY As Long = 16 'Added for print consolidation sheet

Public Type udtCustItem
    sPartCode     As String
    dblQuantity   As Double
    sLineNo       As String
    sCustOrderNo  As String
    lSourceLineNo As Long
End Type

Public poPrintStore  As Object

Public Function GetPrintStore() As Object
    
    If poPrintStore Is Nothing Then
        Set poPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call poPrintStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call poPrintStore.IBo_Load
    End If

End Function

Public Function NumberParkedOrders() As Long

Dim oRow   As Object
Dim oField As Object
Dim oBO    As Object
    
    'Save updated fields back to Database
    Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
    
    Set oBO = goSession.Database.CreateBusinessObject(CLASSID_PURCHASEORDER)
    
    'Create field for selection criteria Key into
    Set oField = oBO.IBo_GetField(FID_PURCHASEORDER_OrderNumber)
    If oField Is Nothing Then Exit Function
    oField.IField_ValueAsVariant = ""
    Call oRow.AddSelection(CMP_EQUAL, oField)
    
    'Create field for selection criteria Key into
    Set oField = oBO.IBo_GetField(FID_PURCHASEORDER_ReleaseNumber)
    If oField Is Nothing Then Exit Function
    oField.IField_ValueAsVariant = ""
    Call oRow.AddSelection(CMP_EQUAL, oField)
    
    NumberParkedOrders = goSession.Database.getAggregateValue(AGG_COUNT, oBO.IBo_GetField(FID_PURCHASEORDER_OrderNumber), oRow)
    
    Set oRow = Nothing
    Set oBO = Nothing
        
End Function


Public Function NumberPendingISTs(Optional ByRef sbStatusBar As StatusBar = Nothing) As Long

Dim oRow   As Object
Dim oField As Object
Dim oBO    As Object
    
    Call ProcessISTFile(sbStatusBar)
    'Save updated fields back to Database
    Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
    
    Set oBO = goSession.Database.CreateBusinessObject(CLASSID_ISTINEDIHEADER)
    
    'Create field for selection criteria Key into (Received = False)
    Set oField = oBO.IBo_GetField(FID_ISTINEDIHEADER_Received)
    If oField Is Nothing Then Exit Function
    oField.IField_ValueAsVariant = False
    Call oRow.AddSelection(CMP_EQUAL, oField)
    
    NumberPendingISTs = goSession.Database.getAggregateValue(AGG_COUNT, oBO.IBo_GetField(FID_ISTINEDIHEADER_ISTInNumber), oRow)
    
    Set oRow = Nothing
    Set oBO = Nothing
        
End Function

Public Function FillInLineComments(ByVal ActionType As enActionType, ByVal blnLinesOnly As Boolean)

Dim cLineCmt       As cLineReasonNotes
Dim colLineCmt     As Collection
Dim lngCodeNo      As Long
Dim strCommentList As String

        
    'Get List of Line Comment and store is mstrLineCmnt for later use
    Set cLineCmt = goDatabase.CreateBusinessObject(CLASSID_LINECOMMENTS)
    
    cLineCmt.AddLoadFilter CMP_EQUAL, FID_LINECOMMENTS_Active, True
    
    Call cLineCmt.SetNoteTypeFilter(ActionType)
    Call cLineCmt.AddLoadFilter(CMP_EQUAL, FID_LINECOMMENTS_LineNote, blnLinesOnly)

    Set colLineCmt = cLineCmt.LoadMatches
    
    'load up mstrLineCmt with Code-Description
    For lngCodeNo = 1 To colLineCmt.Count Step 1
        strCommentList = strCommentList & colLineCmt(CLng(lngCodeNo)).Description & vbTab
    Next lngCodeNo
    
    FillInLineComments = strCommentList

End Function



