VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "fpSPR70.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDetails 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "View Details"
   ClientHeight    =   7425
   ClientLeft      =   1725
   ClientTop       =   510
   ClientWidth     =   10215
   Icon            =   "frmDetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   10215
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame fraDetails 
      Caption         =   "Details"
      Height          =   6855
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Visible         =   0   'False
      Width           =   10815
      Begin VB.CommandButton cmdDeleteView 
         Caption         =   "Ctrl F8-Delete"
         Height          =   375
         Left            =   4920
         TabIndex        =   26
         Top             =   6240
         Width           =   1215
      End
      Begin VB.CommandButton cmdISTUse 
         Caption         =   "F5-Use"
         Height          =   375
         Left            =   9240
         TabIndex        =   22
         Top             =   6240
         Width           =   1215
      End
      Begin VB.CommandButton cmdISTPrint 
         Caption         =   "F9-Print"
         Height          =   375
         Left            =   2640
         TabIndex        =   21
         Top             =   6240
         Width           =   1215
      End
      Begin VB.CommandButton cmdISTClose 
         Caption         =   "F12-Close"
         Height          =   375
         Left            =   240
         TabIndex        =   20
         Top             =   6240
         Width           =   1215
      End
      Begin FPSpread.vaSpread sprdDetail 
         Height          =   5535
         Left            =   240
         TabIndex        =   13
         Top             =   600
         Width           =   10215
         _Version        =   393216
         _ExtentX        =   18018
         _ExtentY        =   9763
         _StockProps     =   64
         AllowCellOverflow=   -1  'True
         ColsFrozen      =   3
         EditModeReplace =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   8
         MaxRows         =   1
         NoBeep          =   -1  'True
         OperationMode   =   2
         RowHeaderDisplay=   0
         ScrollBarExtMode=   -1  'True
         SelectBlockOptions=   0
         SpreadDesigner  =   "frmDetails.frx":058A
         UserResize      =   1
         ScrollBarTrack  =   1
      End
      Begin VB.Label lblISTNumber 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6600
         TabIndex        =   19
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "IST No"
         Height          =   255
         Left            =   6000
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblISTSentDate 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4560
         TabIndex        =   17
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Sent"
         Height          =   255
         Left            =   4200
         TabIndex        =   16
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblISTStore 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   720
         TabIndex        =   15
         Top             =   240
         Width           =   3135
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Store"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.CommandButton cmdPrintList 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   5520
      TabIndex        =   28
      Top             =   6600
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdDetails 
      Caption         =   "F11-Details"
      Height          =   375
      Left            =   120
      TabIndex        =   23
      Top             =   6600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select &All"
      Height          =   375
      Left            =   5520
      TabIndex        =   7
      Top             =   6600
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdRetrieve 
      Caption         =   "F5-Retrieve"
      Height          =   375
      Left            =   9360
      TabIndex        =   9
      Top             =   6600
      Width           =   1215
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   10680
      TabIndex        =   10
      Top             =   6600
      Width           =   975
   End
   Begin FPSpread.vaSpread sprdDetails 
      Height          =   6015
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   9975
      _Version        =   393216
      _ExtentX        =   17595
      _ExtentY        =   10610
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   21
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SelectBlockOptions=   0
      SpreadDesigner  =   "frmDetails.frx":0C4A
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   11
      Top             =   7050
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmDetails.frx":173E
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10663
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "11:42"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdConsolidate 
      Caption         =   "F4-Consolidate"
      Height          =   375
      Left            =   7920
      TabIndex        =   25
      Top             =   6600
      Width           =   1335
   End
   Begin VB.CommandButton cmdDeleteIST 
      Caption         =   "Ctrl F8-Delete"
      Height          =   375
      Left            =   6600
      TabIndex        =   27
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdDeselectAll 
      Caption         =   "&Deselect All"
      Height          =   375
      Left            =   6600
      Picture         =   "frmDetails.frx":390A
      TabIndex        =   8
      Top             =   6600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdShowDetails 
      Caption         =   "F4-Show Details"
      Height          =   375
      Left            =   1320
      TabIndex        =   24
      Top             =   6600
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblNoSelected 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "N/A"
      Height          =   255
      Left            =   4800
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblNoSelectedlbl 
      BackStyle       =   0  'Transparent
      Caption         =   "No Selected :"
      Height          =   255
      Left            =   3720
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblSupplierlbl 
      BackStyle       =   0  'Transparent
      Caption         =   "Supplier"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblNoMatches 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "N/A"
      Height          =   255
      Left            =   4080
      TabIndex        =   6
      Top             =   6600
      Width           =   1335
   End
   Begin VB.Label lblNoMatcheslbl 
      BackStyle       =   0  'Transparent
      Caption         =   "No of Matches :"
      Height          =   255
      Left            =   2880
      TabIndex        =   5
      Top             =   6600
      Width           =   1215
   End
   Begin VB.Label lblSupplier 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   840
      TabIndex        =   1
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "frmDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmDetails
'* Date   : 21/03/03
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Orders/frmDetails.frm $
'**********************************************************************************************
'* Summary: This displays any sub lists that are required by the Order Function.
'**********************************************************************************************
'* $Author: Damians $ $Date: 19/04/04 11:44 $ $Revision: 13 $
'* Versions:
'* 21/03/03    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmDetails"

Const COL_CUST      As Long = 1
Const COL_CUSTREF   As Long = 2
Const COL_SUPPNO    As Long = 3
Const COL_SUPPNAME  As Long = 4
Const COL_PCODE     As Long = 5
Const COL_DESC      As Long = 6
Const COL_QTYORDER  As Long = 7
Const COL_ORDERNO   As Long = 8
Const COL_ORDERDATE As Long = 9
Const COL_DUEDATE   As Long = 10
Const COL_RAISEDBY  As Long = 11
Const COL_NOCARTONS As Long = 12
Const COL_ORDERQTY  As Long = 13
Const COL_RECVDQTY  As Long = 14
Const COL_ORDERKEY  As Long = 15
Const COL_LINENO    As Long = 16
Const COL_PROCESS   As Long = 17
Const COL_CONSQTY   As Long = 18
Const COL_CONSPROC  As Long = 19
Const COL_SOURCENO  As Long = 20
Const COL_DISPPOS   As Long = 21

'Constants for the displaying of individual document line items
Const COL_DTL_ITEMNO As Long = 1
Const COL_DTL_PARTCODE As Long = 2
Const COL_DTL_DESC As Long = 3
Const COL_DTL_SIZE As Long = 4
Const COL_DTL_QTY As Long = 5
Const COL_DTL_PACKQTY As Long = 6
Const COL_DTL_MANUCODE As Long = 7

Const SEL_NO As Long = 0
Const SEL_YES As Long = 1

Const SEL_CONS_NONE As Long = 0
Const SEL_CONS_ALL As Long = 1
Const SEL_CONS_PART As Long = 2

Const DISP_CUSTORDERS As Byte = 1
Const DISP_PARKED     As Byte = 2
Const DISP_ISTS       As Byte = 3

Private mtItem()    As udtCustItem

Public SelectedLinesCount As Long

Public SupplierNo As String
Public StoreNumber As String
Public ISTOutNumber As String

Private mstrSupplierNo   As String
Private mstrSupplierName As String
Private mfpcmbSupplier   As fpCombo 'used to access Supplier Name from Supplier Code
Private mblnConsView     As Boolean 'flag if viewing consolidated Cust Orders

Private mblnDispType As Byte

Private mblnConfirmDelete As Boolean

Private Sub SetQuantityPlaces()

Dim lngQtyDecNum   As Long
Dim lngValueDecNum As Long

    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    lngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)

    sprdDetails.Row = -1
    sprdDetails.Col = COL_QTYORDER
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum
    sprdDetails.Col = COL_ORDERQTY
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum
    sprdDetails.Col = COL_RECVDQTY
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum
    sprdDetails.Col = COL_CONSQTY
    sprdDetails.TypeNumberDecPlaces = lngQtyDecNum

End Sub

Public Sub GetSelectedLine(ByVal lLineNo As Long, _
                           ByRef sPartCode, _
                           ByRef dblQuantity As Double, _
                           ByRef sCustOrderNo As String, _
                           ByRef sLineNo As String, _
                           ByRef lOrigLineNo As Long)

    If lLineNo > UBound(mtItem) Then
        sPartCode = "ERROR"
        dblQuantity = -1
        sLineNo = "ERROR"
        sCustOrderNo = "ERROR"
        Exit Sub
    End If
    
    sPartCode = mtItem(lLineNo - 1).sPartCode
    dblQuantity = mtItem(lLineNo - 1).dblQuantity
    sLineNo = mtItem(lLineNo - 1).sLineNo
    sCustOrderNo = mtItem(lLineNo - 1).sCustOrderNo
    lOrigLineNo = mtItem(lLineNo - 1).lSourceLineNo
    
End Sub
Public Sub AddSelectedLine(ByVal sPartCode, _
                           ByVal sCustOrderNo As String, _
                           ByVal sLineNo As String, _
                           ByVal lSourceLineNo As Long)
                           
Dim lRowNo    As Long
Dim lItemNo   As Long
   
    'find first entry to match Partcode
    lRowNo = sprdDetails.SearchCol(COL_PCODE, -1, -1, sPartCode, SearchFlagsNone)
    'Step through matching items and look for matching Order No and Line No
    For lRowNo = lRowNo To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lRowNo
        sprdDetails.Col = COL_ORDERNO
        If sprdDetails.Text = sCustOrderNo Then
            sprdDetails.Col = COL_LINENO
            If sprdDetails.Text = sLineNo Then
                sprdDetails.Col = COL_PROCESS
                sprdDetails.Value = SEL_YES
                sprdDetails.Col = COL_SOURCENO
                sprdDetails.Text = lSourceLineNo
                Exit For
            End If
        End If
    Next lRowNo
    
    Call CountSelectedItems
    
End Sub

Private Sub cmdConsolidate_Click()

Dim lRowNo  As Long
Dim blnHide As Boolean

    sprdDetails.ReDraw = False
    sprdDetails.Col = COL_CONSQTY
    mblnConsView = Not mblnConsView
    If mblnConsView = False Then
        'Sort by Order No/Line No
        sprdDetails.SortKey(1) = COL_ORDERNO
        sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
        sprdDetails.SortKey(2) = COL_LINENO
        sprdDetails.SortKeyOrder(2) = SortKeyOrderAscending
        Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
        
        blnHide = False
        cmdConsolidate.Caption = "F4-Consolidate"
        'Hide consolidation columns and display individual lines
        sprdDetails.Col = COL_QTYORDER
        sprdDetails.ColHidden = False
        sprdDetails.Col = COL_ORDERNO
        sprdDetails.ColHidden = False
        sprdDetails.Col = COL_ORDERDATE
        sprdDetails.ColHidden = False
        sprdDetails.Col = COL_PROCESS
        sprdDetails.ColHidden = False
        sprdDetails.Col = COL_CONSQTY
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_CONSPROC
        sprdDetails.ColHidden = True
    Else
        'Sort by Order No/Line No
        sprdDetails.SortKey(1) = COL_PCODE
        sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
        blnHide = True
        cmdConsolidate.Caption = "F4-Expand"
        'Show consolidation columns and hide individual lines
        sprdDetails.Col = COL_QTYORDER
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_ORDERNO
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_ORDERDATE
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_PROCESS
        sprdDetails.ColHidden = True
        sprdDetails.Col = COL_CONSQTY
        sprdDetails.ColHidden = False
        sprdDetails.Col = COL_CONSPROC
        sprdDetails.ColHidden = False
    End If
    
    'set individual lines to show or hide
    sprdDetails.Col = COL_CONSQTY
    For lRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lRowNo
        If sprdDetails.Text = "" Then sprdDetails.RowHidden = blnHide
    Next lRowNo
    For lRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lRowNo
        If sprdDetails.RowHidden = False Then
            sprdDetails.Row = lRowNo
            Call sprdDetails.SetActiveCell(COL_PCODE, sprdDetails.Row)
            Exit For
        End If
    Next lRowNo
    sprdDetails.ReDraw = True
    sprdDetails.SetFocus

End Sub

Private Sub cmdClose_Click()
    
    SupplierNo = ""
    SelectedLinesCount = -1 'flag that exit pressed
    Call Me.Hide

End Sub

Public Function LoadParkedOrders(ByRef SupplierfpCombo As fpCombo) As Boolean

Dim oOrder   As Object
Dim oMatches As Collection
Dim lMatchNo As Long
Dim oRow     As Object
Dim oField   As Object

On Error GoTo Bad_Apply
            
    
    LoadParkedOrders = False
    Screen.MousePointer = vbHourglass
    
    Set mfpcmbSupplier = SupplierfpCombo
    mblnDispType = DISP_PARKED
    Me.Caption = "Access parked orders"
    sprdDetails.MaxRows = 0
    lblNoMatches.Caption = "Searching..."
    cmdRetrieve.Visible = False
    
    DoEvents
'    If goDatabase Is Nothing Then Call Initialise
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
    
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_OrderNumber, "")
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReleaseNumber, "")
    
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_Key)
'    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_OrderNumber)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_SupplierNo)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_OrderDate)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_RaisedBy)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_NoOfCartons)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_QuantityOnOrder)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_ReceivedPart)
    Set oMatches = oOrder.IBo_LoadMatches
    
    sprdDetails.Col = COL_ORDERNO
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_QTYORDER
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CUST
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CUSTREF
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_PCODE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DESC
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DUEDATE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_RECVDQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_ORDERKEY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_LINENO
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_PROCESS
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CONSQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CONSPROC
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_SOURCENO
    sprdDetails.ColHidden = True
    lblNoMatches.Caption = oMatches.Count
    sprdDetails.MaxRows = oMatches.Count
    DoEvents
    SupplierfpCombo.SearchMethod = SearchMethodPartialMatch
    SupplierfpCombo.SearchIgnoreCase = True
    SupplierfpCombo.Col = 1
    For lMatchNo = 1 To oMatches.Count Step 1
        sprdDetails.Row = lMatchNo
        sprdDetails.Col = COL_ORDERNO
        sprdDetails.Text = oMatches(lMatchNo).OrderNumber
        sprdDetails.Col = COL_SUPPNO
        sprdDetails.Text = oMatches(lMatchNo).SupplierNo
        SupplierfpCombo.SearchText = sprdDetails.Text
        sprdDetails.Col = COL_SUPPNAME
        SupplierfpCombo.Action = ActionSearch
        SupplierfpCombo.Text = SupplierfpCombo.List(SupplierfpCombo.SearchIndex)
        sprdDetails.Text = SupplierfpCombo.ColText
        sprdDetails.Col = COL_ORDERDATE
        sprdDetails.Text = DisplayDate(oMatches(lMatchNo).OrderDate, False)
        sprdDetails.Col = COL_RAISEDBY
        sprdDetails.Text = oMatches(lMatchNo).RaisedBy
        sprdDetails.Col = COL_NOCARTONS
        sprdDetails.Text = oMatches(lMatchNo).NoOfCartons
        sprdDetails.Col = COL_ORDERQTY
        sprdDetails.Text = oMatches(lMatchNo).QuantityOnOrder
        sprdDetails.Col = COL_ORDERKEY
        sprdDetails.Text = oMatches(lMatchNo).Key
    Next lMatchNo
    
    If sprdDetails.MaxRows > 0 Then
        cmdRetrieve.Visible = True
        LoadParkedOrders = True
    End If
    
    cmdConsolidate.Visible = False
    lblNoSelected.Visible = False
    lblNoSelectedlbl.Visible = False
    lblSupplier.Visible = False
    lblSupplierlbl.Visible = False
    sprdDetails.Height = sprdDetails.Height + sprdDetails.Top - lblSupplierlbl.Top
    sprdDetails.Top = lblSupplier.Top
    Screen.MousePointer = vbNormal
    
    Exit Function
    
Bad_Apply:
    lblNoMatches.Caption = "ERROR" & Err.Number
    Screen.MousePointer = vbNormal
    Exit Function

End Function


Public Function LoadCustomerOrders(ByVal sSupplierNo As String, ByVal sSupplierName As String) As Boolean

Dim oOrderLine  As Object
Dim oOrder      As Object
Dim colOrders   As Collection
Dim colMatches  As Collection
Dim lngMatchNo  As Long
Dim strOldPCode As String
Dim strOldONo   As String
Dim dblQty      As Double

On Error GoTo Bad_Apply
            
    Screen.MousePointer = vbHourglass
    LoadCustomerOrders = True
    
    sprdDetails.MaxRows = 0
    Call sprdDetails.Refresh
    
    mblnDispType = DISP_CUSTORDERS
    sprdDetails.MaxRows = 0
    lblNoMatches.Caption = "Searching..."
    cmdRetrieve.Visible = False
    Me.Caption = "Displaying pending customer orders for supplier"
    DoEvents
    
    mstrSupplierNo = sSupplierNo
    mstrSupplierName = sSupplierName
    lblSupplier.Caption = sSupplierNo & " - " & mstrSupplierName
    
    DoEvents
'    If goDatabase Is Nothing Then Call Initialise
    Set oOrderLine = goDatabase.CreateBusinessObject(CLASSID_SOLINE)
    
    Call oOrderLine.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_RaiseOrder, True)
    Call oOrderLine.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_Cancelled, False)
    Call oOrderLine.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_Complete, False)
    Call oOrderLine.IBo_AddLoadFilter(CMP_GREATERTHAN, FID_SOLINE_OrderQuantity, 0)
    Call oOrderLine.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_SupplierNo, sSupplierNo)
    Call oOrderLine.IBo_AddLoadFilter(CMP_EQUAL, FID_SOLINE_PurchaseOrderNo, "000000")
    
    Call oOrderLine.IBo_AddLoadField(FID_SOLINE_PartCode)
    Call oOrderLine.IBo_AddLoadField(FID_SOLINE_OrderNo)
    Call oOrderLine.IBo_AddLoadField(FID_SOLINE_Description)
    Call oOrderLine.IBo_AddLoadField(FID_SOLINE_LineNo)
    Call oOrderLine.IBo_AddLoadField(FID_SOLINE_OrderQuantity)
    Call oOrderLine.IBo_AddLoadField(FID_SOLINE_DateCreated)
    Set colMatches = oOrderLine.IBo_LoadMatches
    
    DoEvents
    
    'Configure system to show/ hide columns to meet Customer Order Display
    sprdDetails.Col = COL_CUST
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CUSTREF
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_SUPPNO
    sprdDetails.ColHidden = (sSupplierNo <> "")
    sprdDetails.Col = COL_SUPPNAME
    sprdDetails.ColHidden = (sSupplierNo <> "")
    sprdDetails.Col = COL_DUEDATE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_RAISEDBY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_NOCARTONS
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_ORDERQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_RECVDQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_ORDERKEY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_LINENO
    sprdDetails.ColHidden = True
    
    sprdDetails.Col = COL_PROCESS
    sprdDetails.Row = -1
    sprdDetails.Lock = False
    
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
    
    'Display lines retrieved
    For lngMatchNo = 1 To colMatches.Count Step 1
        If oOrder.OrderNo <> colMatches(lngMatchNo).OrderNo Then
            Set oOrder = goDatabase.CreateBusinessObject(CLASSID_SOHEADER)
            Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_SOHEADER_OrderNo, colMatches(lngMatchNo).OrderNo)
            Set colOrders = oOrder.IBo_LoadMatches
        End If
        If colOrders.Count > 0 Then
            'Retrieve header and check that Order not Cancelled and was saved correctly
            If (colOrders(1).Complete = False) And (colOrders(1).Cancelled = False) And (colOrders(1).OrderProcessed = True) Then
                sprdDetails.MaxRows = sprdDetails.MaxRows + 1
                sprdDetails.Row = sprdDetails.MaxRows
                sprdDetails.Col = COL_ORDERNO
                sprdDetails.Text = colMatches(lngMatchNo).OrderNo
                sprdDetails.Col = COL_QTYORDER
                sprdDetails.Text = colMatches(lngMatchNo).OrderQuantity
                sprdDetails.Col = COL_PCODE
                sprdDetails.Text = colMatches(lngMatchNo).PartCode
                sprdDetails.Col = COL_DESC
                sprdDetails.Text = colMatches(lngMatchNo).Description
                sprdDetails.Col = COL_ORDERDATE
                sprdDetails.Text = DisplayDate(colMatches(lngMatchNo).DateCreated, False)
                sprdDetails.Col = COL_LINENO
                sprdDetails.Text = colMatches(lngMatchNo).LineNo
                sprdDetails.Col = COL_ORDERKEY
                sprdDetails.Text = colMatches(lngMatchNo).OrderNo
            End If
        End If
    Next lngMatchNo
    
    lblNoMatches.Caption = colMatches.Count
    If sprdDetails.MaxRows > 0 Then cmdRetrieve.Visible = True
    cmdRetrieve.Caption = "F5-Process"
    lblNoSelectedlbl.Visible = True
    lblNoSelected.Visible = True
    
    'Sort Customer orders by Partcode
    sprdDetails.SortKey(1) = COL_PCODE
    sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
    
    'Step through list and consolidate Part Codes
    sprdDetails.Col = COL_PCODE
    sprdDetails.Row = 1
    strOldPCode = sprdDetails.Text
    For lngMatchNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lngMatchNo
        sprdDetails.Col = COL_DISPPOS
        sprdDetails.Text = lngMatchNo
        sprdDetails.Col = COL_PCODE
        If strOldPCode <> sprdDetails.Text Then 'if other PartCode then display totals
            strOldPCode = sprdDetails.Text
            sprdDetails.Row = sprdDetails.Row - 1
            sprdDetails.Col = COL_CONSQTY
            sprdDetails.Text = dblQty
            sprdDetails.Col = COL_CONSPROC
            sprdDetails.Value = 0
            'Start new counter
            sprdDetails.Row = lngMatchNo
            sprdDetails.Col = COL_QTYORDER
            dblQty = Val(sprdDetails.Text)
        Else
            'Add quantity to current partcode counter
            sprdDetails.Col = COL_QTYORDER
            dblQty = dblQty + Val(sprdDetails.Text)
        End If
    Next lngMatchNo
    'Catch Last Row
    sprdDetails.Col = COL_CONSQTY
    sprdDetails.Text = dblQty
    'Hide consolidation columns until user selects
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CONSPROC
    sprdDetails.Value = 0
    sprdDetails.ColHidden = True
    
    'Sort Customer orders by Order Number/Line
    sprdDetails.SortKey(1) = COL_ORDERNO
    sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
    sprdDetails.SortKey(2) = COL_LINENO
    sprdDetails.SortKeyOrder(2) = SortKeyOrderAscending
    Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
    
    Screen.MousePointer = vbNormal
    
    Call cmdSelectAll_Click
    
    If Val(lblNoMatches.Caption) = 0 Then
        Call MsgBox("No customer orders exist for this supplier", vbInformation, "Access customer orders")
        LoadCustomerOrders = False
    End If
    sprdDetails.Row = 1
    
    Exit Function
    
Bad_Apply:
    lblNoMatches.Caption = "ERROR" & Err.Number
    Screen.MousePointer = vbNormal
    Exit Function

End Function
Public Function LoadPendingISTs(ByRef colStores As Collection) As Boolean

Dim oHeader    As Object
Dim oMatches   As Collection
Dim lngMatchNo As Long
Dim lngStoreNo As Long

On Error GoTo Bad_Apply
            
    Screen.MousePointer = vbHourglass
    LoadPendingISTs = True
    
    mblnDispType = DISP_ISTS
    sprdDetails.MaxRows = 0
    lblNoMatches.Caption = "Searching..."
    cmdRetrieve.Visible = False
    cmdConsolidate.Visible = False
    lblNoSelected.Visible = False
    lblNoSelectedlbl.Visible = False
    lblSupplier.Visible = False
    lblSupplierlbl.Visible = False
    sprdDetails.Height = sprdDetails.Height + sprdDetails.Top - lblSupplierlbl.Top
    sprdDetails.Top = lblSupplier.Top
    Me.Caption = "Displaying pending IST's"
  
    DoEvents
'    If goDatabase Is Nothing Then Call Initialise
    Set oHeader = goDatabase.CreateBusinessObject(CLASSID_ISTINEDIHEADER)
    
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_Received, False)
    
    Call oHeader.IBo_AddLoadField(FID_ISTINEDIHEADER_SourceStoreNumber)
    Call oHeader.IBo_AddLoadField(FID_ISTINEDIHEADER_SentDate)
    Call oHeader.IBo_AddLoadField(FID_ISTINEDIHEADER_ISTOutNumber)
    Set oMatches = oHeader.IBo_LoadMatches
    
    lblNoMatches.Caption = oMatches.Count
    sprdDetails.MaxRows = oMatches.Count
    DoEvents
    
    sprdDetails.Col = COL_CUST
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CUSTREF
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_PCODE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DESC
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_QTYORDER
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_DUEDATE
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_RAISEDBY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_NOCARTONS
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_ORDERQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_RECVDQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_ORDERKEY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_LINENO
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_PROCESS
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CONSQTY
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_CONSPROC
    sprdDetails.ColHidden = True
    sprdDetails.Col = COL_SOURCENO
    sprdDetails.ColHidden = True
    
    sprdDetails.Row = 0
    sprdDetails.Col = COL_SUPPNO
    sprdDetails.Text = "Store Number"
    sprdDetails.Col = COL_SUPPNAME
    sprdDetails.Text = "Store Name"
    sprdDetails.Col = COL_ORDERNO
    sprdDetails.Text = "IST Out Number"
    sprdDetails.Col = COL_ORDERDATE
    sprdDetails.Text = "Date Raised"
    
    For lngMatchNo = 1 To oMatches.Count Step 1
        sprdDetails.Row = lngMatchNo
        sprdDetails.Col = COL_SUPPNO
        sprdDetails.Text = oMatches(lngMatchNo).SourceStoreNumber
        sprdDetails.Col = COL_ORDERNO
        sprdDetails.Text = oMatches(lngMatchNo).ISTOutNumber
        sprdDetails.Col = COL_ORDERDATE
        sprdDetails.Text = oMatches(lngMatchNo).SentDate
        'look up store name of sending IST store
        For lngStoreNo = 1 To colStores.Count Step 1
            If colStores(CLng(lngStoreNo)).StoreNumber = oMatches(lngMatchNo).SourceStoreNumber Then
                sprdDetails.Col = COL_SUPPNAME
                sprdDetails.Text = colStores(CLng(lngStoreNo)).AddressLine1
            End If
        Next lngStoreNo
    Next lngMatchNo
    
    If sprdDetails.MaxRows > 0 Then
        cmdRetrieve.Visible = True
'        sprdDetails.Selected(0) = True
'        sprdDetails.SetFocus
    Else
    End If
    cmdRetrieve.Caption = "F5-Process"
    lblNoSelectedlbl.Visible = True
    lblNoSelected.Visible = True
    
    sprdDetails.SortKey(1) = COL_PCODE
    sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
    
    sprdDetails.Col = COL_SUPPNO
    sprdDetails.Row = 1
    
    Screen.MousePointer = vbNormal
    
    If Val(lblNoMatches.Caption) = 0 Then
        Call MsgBox("No pending IST's to process", vbInformation, "Access pending IST's")
        LoadPendingISTs = False
    Else
        cmdShowDetails.Visible = True
        cmdDetails.Visible = True
        cmdPrintList.Visible = True
        cmdDeleteIST.Visible = True
    End If
    
    Exit Function
    
Bad_Apply:
    lblNoMatches.Caption = "ERROR" & Err.Number
    Screen.MousePointer = vbNormal
    Exit Function

End Function


Private Sub cmdClose_GotFocus()
    
    cmdClose.FontBold = True

End Sub

Private Sub cmdClose_LostFocus()
    
    cmdClose.FontBold = False

End Sub

Private Sub cmdDeleteView_Click()

Dim oHeader      As Object
    
    Select Case (mblnDispType)
        Case (DISP_ISTS):
                If MsgBoxEx("Confirm delete IST - '" & lblISTNumber.Caption & "' from Store " & lblISTStore.Caption, vbYesNo + vbQuestion, "Delete IST In", , , , , RGBMSGBox_PromptColour) = vbNo Then
                    If mblnConfirmDelete = True Then Call cmdISTClose_Click
                    Exit Sub
                End If
                Set oHeader = goDatabase.CreateBusinessObject(CLASSID_ISTINEDIHEADER)

                Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_SourceStoreNumber, Left(lblISTStore.Caption, InStr(lblISTStore.Caption, " - ") - 1))
                Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_ISTOutNumber, lblISTNumber.Caption)
                If oHeader.IBo_Load Then
                    Call oHeader.RecordCancelled(Left(lblISTStore.Caption, InStr(lblISTStore.Caption, " - ") - 1), lblISTNumber.Caption, False)
' RecordCancelled(strSourceStoreNo As String, strISTOutNo As String, blnEDICancel As Boolean) As Boolean
                    Call sprdDetails.DeleteRows(sprdDetails.Row, 1)
                    sprdDetails.MaxRows = sprdDetails.MaxRows - 1
                    Call cmdISTClose_Click
                End If
    End Select
    
End Sub

Private Sub cmdDeleteIST_Click()

    mblnConfirmDelete = True
    Call cmdDetails_Click

End Sub

Private Sub cmdDeselectAll_Click()
    
    sprdDetails.Row = -1
    sprdDetails.Col = COL_PROCESS
    sprdDetails.Value = SEL_NO
    sprdDetails.Col = COL_CONSPROC
    sprdDetails.Value = SEL_CONS_NONE
    lblNoSelected.Caption = "0"
    sprdDetails.SetFocus

End Sub

Private Sub cmdDeselectAll_GotFocus()
    
    cmdDeselectAll.FontBold = True

End Sub

Private Sub cmdDeselectAll_LostFocus()
    
    cmdDeselectAll.FontBold = False

End Sub


Private Sub cmdDetails_Click()

Dim strStoreNo   As String
Dim strISTNo     As String
Dim strStoreName As String

    sprdDetails.Col = COL_SUPPNO
    strStoreNo = sprdDetails.Text
    sprdDetails.Col = COL_SUPPNAME
    strStoreName = sprdDetails.Text
    sprdDetails.Col = COL_ORDERNO
    strISTNo = sprdDetails.Text

    Call DisplayIST(strStoreNo, strISTNo, strStoreName)
    If mblnConfirmDelete = True Then Call cmdDeleteView_Click
    mblnConfirmDelete = False

End Sub

Private Sub cmdISTClose_Click()

    sprdDetail.MaxRows = 0
    fraDetails.Visible = False

End Sub

Private Sub cmdShowDetails_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdShowDetails_Click"

Dim strIST      As String
Dim lngRow      As Long

    On Error GoTo errHandler
    
    Call frmISTDetails.Show(vbModal, Me)
    
    strIST = frmISTDetails.mstrIST
    
    Unload frmISTDetails
    
    'check we have selected an ist
    If strIST <> "" Then
        'search for ist in spreadsheet
        lngRow = sprdDetails.SearchCol(COL_ORDERNO, -1, -1, strIST, SearchFlagsNone)
        
        'check we have a match
        If lngRow > -1 Then
            Call sprdDetails.SetSelection(-1, lngRow, -1, lngRow)
            
            Call cmdRetrieve_Click
        End If
    End If
    
    Exit Sub
    
errHandler:
    
    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
   
End Sub

Private Sub cmdISTPrint_Click()

    Call GetPrintStore
    sprdDetail.PrintHeader = "/c/fb1IST Details - " & lblISTNumber.Caption & "/n/c/fb1From store " & lblISTStore.Caption & " sent " & lblISTSentDate.Caption & _
                "/n/cStore : " & poPrintStore.StoreNumber & "-" & poPrintStore.AddressLine1
    sprdDetail.PrintJobName = "IST In Details-" & lblISTNumber.Caption
    sprdDetail.PrintFooter = GetFooter
    Call sprdDetail.PrintSheet
    
End Sub

Private Sub cmdISTUse_Click() 'if use from Detailed screen, then call Retrieve

    Call cmdRetrieve_Click

End Sub

Private Sub cmdPrintList_Click()

    Call GetPrintStore
    sprdDetails.PrintHeader = "/c/fb1Pending IST List/n/c/fb1Store : " & poPrintStore.StoreNumber & "-" & poPrintStore.AddressLine1
    sprdDetails.PrintJobName = "Pending IST List"
    sprdDetails.PrintFooter = GetFooter
    Call sprdDetails.PrintSheet

End Sub

Private Sub cmdRetrieve_Click()

Dim lngRowNo   As Long
Dim lngItemNo  As Long

    If mblnDispType = DISP_CUSTORDERS Then

        ReDim mtItem(0)
        
        lngItemNo = 0
        For lngRowNo = 1 To sprdDetails.MaxRows Step 1
            sprdDetails.Row = lngRowNo
            sprdDetails.Col = COL_PROCESS
            If sprdDetails.Value <> 0 Then
                lngItemNo = lngItemNo + 1
                ReDim Preserve mtItem(lngItemNo)
                sprdDetails.Col = COL_PCODE
                mtItem(lngItemNo - 1).sPartCode = sprdDetails.Text
                sprdDetails.Col = COL_QTYORDER
                mtItem(lngItemNo - 1).dblQuantity = sprdDetails.Text
                sprdDetails.Col = COL_ORDERNO
                mtItem(lngItemNo - 1).sCustOrderNo = sprdDetails.Text
                sprdDetails.Col = COL_LINENO
                mtItem(lngItemNo - 1).sLineNo = sprdDetails.Text
                sprdDetails.Col = COL_SOURCENO
                mtItem(lngItemNo - 1).lSourceLineNo = Val(sprdDetails.Text)
            End If ' Item was selected for adding to Purchase Order
        Next lngRowNo
        SelectedLinesCount = UBound(mtItem)
    End If
    If mblnDispType = DISP_PARKED Then
        sprdDetails.Row = sprdDetails.SelBlockRow
        sprdDetails.Col = COL_SUPPNO
        SupplierNo = sprdDetails.Text
    End If
    If mblnDispType = DISP_ISTS Then
        sprdDetails.Row = sprdDetails.SelBlockRow
        sprdDetails.Col = COL_SUPPNO
        StoreNumber = sprdDetails.Text
        sprdDetails.Col = COL_ORDERNO
        ISTOutNumber = sprdDetails.Text
    End If
    Me.Hide

End Sub

Private Sub cmdRetrieve_GotFocus()
    
    cmdRetrieve.FontBold = True

End Sub

Private Sub cmdRetrieve_LostFocus()
    
    cmdRetrieve.FontBold = False

End Sub

Private Sub cmdSelectAll_Click()

    sprdDetails.Row = -1
    sprdDetails.Col = COL_PROCESS
    sprdDetails.Value = SEL_YES
    sprdDetails.Col = COL_CONSPROC
    sprdDetails.Value = SEL_CONS_ALL
    Call CountSelectedItems
    If sprdDetails.Visible = True Then sprdDetails.SetFocus

End Sub


Private Sub cmdSelectAll_GotFocus()
    
    cmdSelectAll.FontBold = True

End Sub

Private Sub cmdSelectAll_LostFocus()
    
    cmdSelectAll.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = 0 Then
        Select Case (KeyCode)
                Case (vbKeyF4): 'if F4 then switch between consolidated view
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    If cmdConsolidate.Visible = True Then
                        Call cmdConsolidate_Click
                    ElseIf cmdShowDetails.Visible = True Then
                        Call cmdShowDetails_Click
                    End If
                Case (vbKeyF5): 'if F5 then process
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    If cmdRetrieve.Visible = True Then Call cmdRetrieve_Click
                Case (vbKeyF9): 'if F9 then print details
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    If (cmdISTPrint.Visible = True) Then Call cmdISTPrint_Click
                    If (fraDetails.Visible = False) And (cmdPrintList.Visible = True) Then Call cmdPrintList_Click
                Case (vbKeyF10): 'if F10 then exit
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    Call cmdClose_Click
                Case (vbKeyF11): 'if F11 then view details
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    If cmdDetails.Visible = True Then Call cmdDetails_Click
                Case (vbKeyF12): 'if F12 then close details
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    If cmdISTClose.Visible = True Then Call cmdISTClose_Click
        End Select 'Key pressed with no Shift/Alt combination
    End If
    If Shift = 2 Then 'CTRL pressed
        Select Case (KeyCode)
                Case (vbKeyF8): 'if F8 then delete, if required
                    KeyCode = 0 'cancel keystroke or calling form receives it
                    If cmdDeleteView.Visible = True Then
                        Call cmdDeleteView_Click
                    Else
                        If cmdDeleteIST.Visible = True Then Call cmdDeleteIST_Click
                    End If
        End Select
    End If

End Sub

Private Sub Form_Load()
    
    'Display initial values in Status Bar
    Call InitialiseStatusBar(sbStatus)
       
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
    Call SetQuantityPlaces
    
End Sub

Private Sub Form_Resize()

    If Me.WindowState = vbMinimized Then Exit Sub
    
    If Me.Width < 5000 Then
        Me.Width = 5000
        Exit Sub
    End If
    
    If Me.Height < 3000 Then
        Me.Height = 3000
        Exit Sub
    End If
    
    sprdDetails.Width = Me.Width - (sprdDetails.Left * 3)
    sprdDetails.Height = Me.Height - sprdDetails.Top - cmdClose.Height - 240 - (sbStatus.Height * 2)
    lblNoMatcheslbl.Top = sprdDetails.Top + sprdDetails.Height + 120
    lblNoMatches.Top = lblNoMatcheslbl.Top
    cmdDetails.Top = lblNoMatches.Top
    cmdSelectAll.Top = lblNoMatches.Top
    cmdDeselectAll.Top = lblNoMatches.Top
    cmdRetrieve.Top = lblNoMatches.Top
    cmdClose.Top = lblNoMatches.Top
    cmdPrintList.Top = lblNoMatches.Top
    cmdConsolidate.Top = lblNoMatches.Top
    cmdDeleteIST.Top = cmdClose.Top
    
    cmdShowDetails.Top = cmdClose.Top
    
    cmdClose.Left = sprdDetails.Left + sprdDetails.Width - cmdClose.Width
    cmdRetrieve.Left = cmdClose.Left - cmdRetrieve.Width - 120
    cmdConsolidate.Left = cmdRetrieve.Left - cmdConsolidate.Width - 120
    

    
    
    'Resize IST details if visible
    If fraDetails.Visible = True Then
        fraDetails.Width = Me.Width - (fraDetails.Left * 3)
        fraDetails.Height = cmdClose.Height + cmdClose.Top - fraDetails.Top
        sprdDetail.Width = fraDetails.Width - sprdDetail.Left * 2
        sprdDetail.Height = fraDetails.Height - sprdDetail.Top - 240 - cmdISTClose.Height
        cmdISTClose.Top = sprdDetail.Top + sprdDetail.Height + 120
        cmdDeleteView.Top = cmdISTClose.Top
        cmdISTPrint.Top = cmdISTClose.Top
        cmdISTUse.Top = cmdISTClose.Top
        cmdISTUse.Left = sprdDetail.Width + sprdDetail.Left - cmdISTUse.Width
        cmdISTPrint.Left = cmdISTUse.Left - 120 - cmdISTPrint.Width
    End If
    

End Sub

Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)

End Sub


Private Sub sprdDetails_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

Dim lngRowNo    As Long
Dim dblCount    As Double
Dim lngSelAll   As Long
Dim lngStart    As Long
Dim lngEnd      As Long
Dim lngSelCount As Long 'local counter for number of items selected in consolidation
Dim lngDispPos  As Long

    If Mode = 0 Then
        If (Col = COL_PROCESS) And (ChangeMade = True) Then
            sprdDetails.Row = Row
            sprdDetails.Col = COL_DISPPOS
            lngDispPos = Val(sprdDetails.Text)
            
            sprdDetails.ReDraw = False
            'Sort by Order No/Line No
            sprdDetails.SortKey(1) = COL_DISPPOS
            sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
            Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
                       
            'check if item is part of consolidation
            'Find end of consolidation
            sprdDetails.Col = COL_CONSQTY
            For lngRowNo = lngDispPos To sprdDetails.MaxRows Step 1
                sprdDetails.Row = lngRowNo
                If sprdDetails.Text <> "" Then 'end of consolidation found
                    lngEnd = lngRowNo
                    Exit For
                End If
            Next lngRowNo
            'find start of consolidation
            For lngRowNo = lngDispPos - 1 To 1 Step -1
                sprdDetails.Row = lngRowNo
                If sprdDetails.Text <> "" Then 'start of consolidation found
                    lngStart = lngRowNo + 1
                    Exit For
                End If
            Next lngRowNo
            If lngStart = 0 Then lngStart = 1 'catch start occurring at start of Spread
            
            'step through consolidation and found if all/part selected
            lngSelAll = SEL_CONS_ALL
            lngSelCount = 0
            sprdDetails.Col = COL_PROCESS
            For lngRowNo = lngStart To lngEnd Step 1
                sprdDetails.Row = lngRowNo
                If Val(sprdDetails.Text) = SEL_YES Then lngSelCount = lngSelCount + 1
            Next lngRowNo
            If lngSelCount <> (lngEnd - lngStart + 1) Then lngSelAll = SEL_CONS_PART
            If lngSelCount = 0 Then lngSelAll = SEL_CONS_NONE
            sprdDetails.Row = lngEnd
            sprdDetails.Col = COL_CONSPROC
            sprdDetails.Text = lngSelAll
            sprdDetails.Row = Row
            Call CountSelectedItems
        
            'Sort by Order No/Line No
            sprdDetails.SortKey(1) = COL_ORDERNO
            sprdDetails.SortKeyOrder(1) = SortKeyOrderAscending
            sprdDetails.SortKey(2) = COL_LINENO
            sprdDetails.SortKeyOrder(2) = SortKeyOrderAscending
            Call sprdDetails.Sort(-1, -1, -1, -1, SortByRow)
        
            sprdDetails.ReDraw = True
        End If 'editing Process Column
        
        If (Col = COL_CONSPROC) And (ChangeMade = True) Then
            lngEnd = Row
            sprdDetails.Col = COL_CONSPROC
            lngSelAll = Val(sprdDetails.Text)
            sprdDetails.Col = COL_CONSQTY
            'find start of consolidation
            For lngRowNo = Row - 1 To 1 Step -1
                sprdDetails.Row = lngRowNo
                If sprdDetails.Text <> "" Then 'start of consolidation found
                    lngStart = lngRowNo + 1
                    Exit For
                End If
            Next lngRowNo
            If lngStart = 0 Then lngStart = 1 'catch start occurring at start of Spread
            
            'step through consolidation and select all items
            sprdDetails.Col = COL_PROCESS
            For lngRowNo = lngStart To lngEnd Step 1
                sprdDetails.Row = lngRowNo
                sprdDetails.Text = lngSelAll
            Next lngRowNo
            
            Call CountSelectedItems
        End If 'editing consolidated process
    End If 'exiting edit mode

End Sub
Public Sub CountSelectedItems()

Dim lRowNo    As Long
Dim dblCount  As Double
Dim lOldRow   As Long

    'step through each line and add up selected entries
    lOldRow = sprdDetails.Row
    For lRowNo = 1 To sprdDetails.MaxRows Step 1
        sprdDetails.Row = lRowNo
        sprdDetails.Col = COL_PROCESS
        If sprdDetails.Value = SEL_YES Then
            sprdDetails.Col = COL_QTYORDER
            dblCount = dblCount + Val(sprdDetails.Value)
        End If
    Next lRowNo
    'reset to current line
    sprdDetails.Row = lOldRow
    lblNoSelected.Caption = dblCount

End Sub

Private Sub sprdDetails_GotFocus()
    
    sprdDetails.SelBackColor = RGB_BLUE
    sprdDetails.SelForeColor = RGB_WHITE

End Sub

Private Sub sprdDetails_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Select Case (mblnDispType)
            Case (DISP_CUSTORDERS):
                If mblnConsView = False Then
                    Call sprdDetails.SetActiveCell(COL_PROCESS, sprdDetails.Row)
                    sprdDetails.Col = COL_PROCESS
                    If sprdDetails.Value = SEL_NO Then
                        sprdDetails.Value = SEL_YES
                    Else
                        sprdDetails.Value = SEL_NO
                    End If
                    Call sprdDetails_EditMode(COL_PROCESS, sprdDetails.Row, 0, True)
                Else
                    Call sprdDetails.SetActiveCell(COL_CONSPROC, sprdDetails.Row)
                    sprdDetails.Col = COL_CONSPROC 'COL_CONSPROC
    '                sprdDetails.BackColor = RGB_WHITE
                    If sprdDetails.Value = SEL_CONS_NONE Then
                        sprdDetails.Value = SEL_CONS_ALL
                    Else
                        sprdDetails.Value = SEL_CONS_NONE
                    End If
                    Call sprdDetails_EditMode(COL_CONSPROC, sprdDetails.Row, 0, True)
                End If
            Case (DISP_PARKED):
                sprdDetails.Row = sprdDetails.SelBlockRow
                sprdDetails.Col = COL_SUPPNO
                SupplierNo = sprdDetails.Text
                Me.Hide
            Case (DISP_ISTS):
                sprdDetails.Row = sprdDetails.SelBlockRow
                sprdDetails.Col = COL_SUPPNO
                StoreNumber = sprdDetails.Text
                sprdDetails.Col = COL_ORDERNO
                ISTOutNumber = sprdDetails.Text
                Me.Hide
        End Select
    End If

End Sub

Private Sub sprdDetails_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)

    sprdDetails.Row = NewRow
    Call sprdDetails.SetActiveCell(COL_PROCESS, NewRow)

End Sub

Private Sub sprdDetails_LostFocus()
    
    sprdDetails.SelBackColor = -1
    sprdDetails.SelForeColor = -1

End Sub

Private Sub DisplayIST(strStoreNumber As String, strISTNo As String, strStoreName As String)

Dim oHeader   As Object
Dim lngLineNo As Long
    
    Set oHeader = goDatabase.CreateBusinessObject(CLASSID_ISTINEDIHEADER)
    
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_SourceStoreNumber, strStoreNumber)
    Call oHeader.IBo_AddLoadFilter(CMP_EQUAL, FID_ISTINEDIHEADER_ISTOutNumber, strISTNo)
    oHeader.GetLines = True
    If oHeader.IBo_Load Then
        'Display sending store details
        lblISTStore.Caption = strStoreNumber & " - " & strStoreName
        'Display Header details
        lblISTSentDate.Caption = DisplayDate(oHeader.SentDate, False)
        lblISTNumber.Caption = oHeader.ISTOutNumber
        sprdDetail.MaxRows = oHeader.Lines.Count
        For lngLineNo = 1 To oHeader.Lines.Count Step 1
            sprdDetail.Row = lngLineNo
            sprdDetail.Col = COL_DTL_ITEMNO
            sprdDetail.Text = lngLineNo
            sprdDetail.Col = COL_DTL_PARTCODE
            sprdDetail.Text = oHeader.Lines(CLng(lngLineNo)).PartCode
            Call GetItem(sprdDetail.Text)
            sprdDetail.Col = COL_DTL_QTY
            sprdDetail.Text = oHeader.Lines(CLng(lngLineNo)).Quantity
        Next lngLineNo
    End If
    fraDetails.Visible = True
    sprdDetail.SetFocus
    Call Form_Resize
       
End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean GetItem()
'**********************************************************************************************
'* Description: Used to retrieve an Item given the SKU and display in spread sheet.  Also used
'*              to initialise line values to 0 as each item is selected/ displayed.
'**********************************************************************************************
'* Parameters:
'*In/Out:strPartCode  String.-Part Code used to retrieve item - must be ready formatted
'*In/Out:blnCheckItem Boolean.-As item is retrieved the status flag is checked to ignore obselete items
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function GetItem(strPartCode As String) As Boolean

Dim oItem   As Object
Dim lRow    As Long
Dim sErrMsg As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetItem"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
                
    lRow = sprdDetail.Row
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Description)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_SizeDescription)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_BuyInPacksOf)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QtyBuyIn)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_ManufProductCode)
    
    Call oItem.IBo_LoadMatches
    
    If oItem.Description = "" Then sErrMsg = "NOT FOUND"

    If sErrMsg <> "" Then
        sprdDetail.Col = COL_DTL_DESC
        sprdDetail.Text = sErrMsg
        sbStatus.Panels(PANEL_INFO).Text = sprdDetail.Text
        sprdDetail.Col = COL_DTL_PARTCODE
        sprdDetail.Col2 = COL_DTL_SIZE
        sprdDetail.Row2 = sprdDetail.Row
        sprdDetail.BlockMode = True
        sprdDetail.BackColor = RGB_RED
        sprdDetail.BlockMode = False
        Screen.MousePointer = vbNormal
        If sprdDetail.ReDraw = True Then
            Call sprdDetail.SetActiveCell(COL_DTL_PARTCODE, lRow)
            If sprdDetail.Visible = True Then Call sprdDetail.SetFocus
            sprdDetail.EditMode = True
        End If
        GetItem = False
        Exit Function
    End If

    sprdDetail.Col = COL_DTL_PARTCODE
    sprdDetail.Text = oItem.PartCode
    sprdDetail.Col = COL_DTL_DESC
    sprdDetail.Text = oItem.Description
    sprdDetail.Col = COL_DTL_SIZE
    sprdDetail.Text = oItem.SizeDescription
    sprdDetail.Col = COL_DTL_PACKQTY
    sprdDetail.Text = oItem.QtyBuyIn
    sprdDetail.Col = COL_DTL_MANUCODE
    sprdDetail.Text = oItem.ManufProductCode
    Set oItem = Nothing
    GetItem = True

    'Tidy up
    GoSub Finally

    Exit Function

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble("Order Function", PROCEDURE_NAME, 1)
    Exit Function

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Function 'GetItem


