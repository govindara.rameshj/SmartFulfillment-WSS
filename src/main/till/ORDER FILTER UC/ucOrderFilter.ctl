VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.UserControl ucOrderFilter 
   BackColor       =   &H00404040&
   ClientHeight    =   7605
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8940
   KeyPreview      =   -1  'True
   ScaleHeight     =   7605
   ScaleWidth      =   8940
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   1920
      TabIndex        =   31
      Top             =   2880
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.Frame fraDetails 
      Height          =   7455
      Left            =   60
      TabIndex        =   13
      Top             =   60
      Visible         =   0   'False
      Width           =   8775
      Begin FPSpreadADO.fpSpread sprdPOLines 
         Height          =   4335
         Left            =   240
         TabIndex        =   30
         Top             =   1560
         Width           =   8415
         _Version        =   458752
         _ExtentX        =   14843
         _ExtentY        =   7646
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   5
         MaxRows         =   1
         ScrollBars      =   2
         SpreadDesigner  =   "ucOrderFilter.ctx":0000
         UserResize      =   1
      End
      Begin VB.CommandButton cmdDtlClose 
         Caption         =   "F12-Close"
         Height          =   375
         Left            =   7680
         TabIndex        =   15
         Top             =   6840
         Width           =   975
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "F9-Print"
         Height          =   375
         Left            =   240
         TabIndex        =   14
         Top             =   6840
         Width           =   855
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Notes"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   6120
         Width           =   495
      End
      Begin VB.Label lblNotes1 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   840
         TabIndex        =   28
         Top             =   6120
         Width           =   7815
      End
      Begin VB.Label lblNotes2 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   840
         TabIndex        =   27
         Top             =   6480
         Width           =   7815
      End
      Begin VB.Label lblOrderDate 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1200
         TabIndex        =   23
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label lblDueDate 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   5280
         TabIndex        =   22
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Due Date"
         Height          =   255
         Left            =   4440
         TabIndex        =   21
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Order Date"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label lblSupplier 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1200
         TabIndex        =   19
         Top             =   720
         Width           =   2655
      End
      Begin VB.Label lblOrderNo 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1200
         TabIndex        =   18
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Order No"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame fraOrderFilter 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Select Order Filter Criteria"
      ForeColor       =   &H00000000&
      Height          =   7455
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8775
      Begin LpLib.fpCombo cmbFilterSupp 
         Height          =   315
         Left            =   1200
         TabIndex        =   5
         Top             =   600
         Width           =   4095
         _Version        =   196608
         _ExtentX        =   7223
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   4
         Sorted          =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   1
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "ucOrderFilter.ctx":0483
      End
      Begin LpLib.fpList lstMatches 
         Height          =   5160
         Left            =   120
         TabIndex        =   9
         Top             =   1440
         Width           =   8535
         _Version        =   196608
         _ExtentX        =   15055
         _ExtentY        =   9102
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Columns         =   10
         Sorted          =   0
         LineWidth       =   1
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         MultiSelect     =   0
         WrapList        =   0   'False
         WrapWidth       =   0
         SelMax          =   -1
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   480
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         DataField       =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         ColDesigner     =   "ucOrderFilter.ctx":08DA
      End
      Begin FPSpreadADO.fpSpread sprdPOList 
         Height          =   975
         Left            =   120
         TabIndex        =   24
         Top             =   1320
         Visible         =   0   'False
         Width           =   7695
         _Version        =   458752
         _ExtentX        =   13573
         _ExtentY        =   1720
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "ucOrderFilter.ctx":0E34
         UserResize      =   1
      End
      Begin VB.CommandButton cmdPrintList 
         Caption         =   "F9-Print List"
         Height          =   375
         Left            =   3120
         TabIndex        =   25
         Top             =   6960
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdEnquiry 
         Caption         =   "F11-Details"
         Height          =   375
         Left            =   6360
         TabIndex        =   26
         Top             =   6960
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CheckBox chkActive 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Active Only"
         Height          =   255
         Left            =   3960
         TabIndex        =   3
         Top             =   240
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "F12-Close"
         Height          =   375
         Left            =   7560
         TabIndex        =   8
         Top             =   960
         Width           =   1095
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "F7-Search"
         Height          =   375
         Left            =   6360
         TabIndex        =   7
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox txtFilterOrderNo 
         Height          =   285
         Left            =   1200
         TabIndex        =   2
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "F3-Reset"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdUse 
         Caption         =   "F5-Use"
         Height          =   375
         Left            =   7680
         TabIndex        =   12
         Top             =   6960
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label Label44 
         BackStyle       =   0  'Transparent
         Caption         =   "&Order No"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label42 
         BackStyle       =   0  'Transparent
         Caption         =   "Supplier"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "No of Matches :"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   7020
         Width           =   1215
      End
      Begin VB.Label lblNoMatches 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "N/A"
         Height          =   255
         Left            =   1440
         TabIndex        =   11
         Top             =   7020
         Width           =   1575
      End
   End
End
Attribute VB_Name = "ucOrderFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : ucOrderFilter
'* Date   : 13/11/02
'* Author : mauricem
'*$Archive: $
'**********************************************************************************************
'* Summary: User control used to access all Purchase Orders.  Control provides selection
'*          criteria to search by Supplier or Purchase Order Number.
'**********************************************************************************************
'* $ Author: $ $ Date: $ $ Revision: $
'* Versions:
'* 13/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "ucOrderFilter"
Const APP_NAME As String = "ucOrderFilter"

Const COL_NO As Long = 1
Const COL_SKU As Long = 2
Const COL_DESC As Long = 3
Const COL_QTY As Long = 4
Const COL_QTYOS As Long = 5

'Event Declarations:
Event Cancel() 'MappingInfo=cmdClose,cmdClose,-1,Click
Event Apply()

Const ORDER_LEN As Long = 6
Const ORDER_PAD As String = "000000"

Private blnInit     As Boolean
'Default Property Values:
Const m_def_ForeColor = 0
Const m_def_Caption = vbNullString
Const m_def_OrderKey = 0
'Property Variables:
Dim m_ForeColor As OLE_COLOR
Dim m_Caption   As String
Dim m_OrderKey  As Long
Dim m_SearchKey As Long
Dim m_CancelKey As Long

Dim m_strQtyFormat As String

Dim blnPreview   As Boolean
Dim oParent      As Form
Dim lngOldColour As Long
Dim moPrintStore As Object

Private Sub chkActive_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Public Property Let SearchKey(ByVal lngFKeyID As Long)

Dim strCaption As String

    If (lngFKeyID > 0) And (lngFKeyID < 13) And (lngFKeyID <> m_CancelKey) Then
        m_SearchKey = lngFKeyID
        strCaption = Mid$(cmdSearch.Caption, InStr(cmdSearch.Caption, "-"))
        If lngFKeyID < 10 Then
            strCaption = "F" & Chr$(lngFKeyID + 48) & strCaption
        Else
            strCaption = "F1" & Chr$(lngFKeyID + 38) & strCaption
        End If
        cmdSearch.Caption = strCaption
    End If

End Property

Public Property Let CancelKey(ByVal lngFKeyID As Long)

Dim strCaption As String
    If (lngFKeyID > 0) And (lngFKeyID < 13) And (lngFKeyID <> m_SearchKey) Then
        m_CancelKey = lngFKeyID
        strCaption = Mid$(cmdClose.Caption, InStr(cmdClose.Caption, "-"))
        If lngFKeyID < 10 Then
            strCaption = "F" & Chr$(lngFKeyID + 48) & strCaption
        Else
            strCaption = "F1" & Chr$(lngFKeyID + 38) & strCaption
        End If
        cmdClose.Caption = strCaption
    End If

End Property

Private Sub cmbFilterSupp_CloseUp()
    
    cmdClear.Visible = True
    'if supplier selected then access orders for them
    If cmbFilterSupp.Text <> "*ALL*" & vbTab & "*ALL*" Then Call cmdSearch_Click

End Sub

Private Sub cmbFilterSupp_GotFocus()

    txtFilterOrderNo.BackColor = RGBEdit_Colour
    cmbFilterSupp.ListDown = True

End Sub

Private Sub cmbFilterSupp_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 27 Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
        DoEvents
    End If
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call cmdSearch_Click
        DoEvents
    End If

End Sub

Private Sub cmdDtlClose_Click()

    fraDetails.Visible = False
    Call lstMatches.SetFocus

End Sub

Private Sub cmdEnquiry_Click()

    Call ViewOrder(lstMatches.ItemData(lstMatches.ListIndex), lstMatches.List(lstMatches.ListIndex))

End Sub

Private Sub cmdPrint_Click()

    If moPrintStore Is Nothing Then
        Set moPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call moPrintStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call moPrintStore.IBo_Load
    End If
    
    sprdPOLines.PrintJobName = "Print Purchase Order " & lblOrderNo.Caption & " summary"
    sprdPOLines.PrintHeader = "/c/fb1Purchase Order -'" & lblOrderNo.Caption & "' from " & lblSupplier.Caption & "/n" & "/cOrdered " & lblOrderDate.Caption & "  Due " & lblDueDate.Caption
    sprdPOLines.PrintHeader = sprdPOLines.PrintHeader & "/nStore No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & moPrintStore.AddressLine1
    
    sprdPOLines.MaxRows = sprdPOLines.MaxRows + 1
    sprdPOLines.RowHeight(sprdPOLines.MaxRows) = sprdPOLines.RowHeight(sprdPOLines.MaxRows) * 3
    Call sprdPOLines.AddCellSpan(1, sprdPOLines.MaxRows, sprdPOLines.MaxCols, 1)
    sprdPOLines.Col = 1
    sprdPOLines.Row = sprdPOLines.MaxRows
    sprdPOLines.CellType = CellTypeEdit
    sprdPOLines.TypeMaxEditLen = 200
    sprdPOLines.TypeEditMultiLine = True
    sprdPOLines.Text = "COMMENTS:" & vbCrLf & "   " & lblNotes1.Caption & vbCrLf & "   " & lblNotes2.Caption
    sprdPOLines.RowHeight(sprdPOLines.MaxRows) = sprdPOLines.MaxTextRowHeight(sprdPOLines.MaxRows)
    sprdPOLines.PrintFooter = GetFooter
    Call sprdPOLines.Refresh
    Call sprdPOLines.PrintSheet
    sprdPOLines.MaxRows = sprdPOLines.MaxRows - 1

End Sub

Private Sub cmdPrintList_Click()

Dim lngColNo As Long
Dim lngRowNo As Long
Dim vntLine

    'Headers are set up in UserControl Initialise
    If moPrintStore Is Nothing Then
        Set moPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call moPrintStore.IBo_AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call moPrintStore.IBo_Load
    End If
    
    sprdPOList.PrintJobName = "Pending Purchase Orders List"
    sprdPOList.PrintHeader = "/c/fb1Pending Purchase Order List"
    sprdPOList.PrintHeader = sprdPOLines.PrintHeader & "/nStore No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & moPrintStore.AddressLine1
    sprdPOList.PrintFooter = GetFooter
    sprdPOList.PrintOrientation = PrintOrientationPortrait
    sprdPOList.MaxRows = 0
    sprdPOList.MaxRows = lstMatches.ListCount
    'Transfer over data
    For lngRowNo = 1 To lstMatches.ListCount Step 1
        sprdPOList.Row = lngRowNo
        lstMatches.Row = lngRowNo - 1
        vntLine = Split(lstMatches.List, vbTab)
        For lngColNo = 0 To lstMatches.Columns - 1 Step 1
            sprdPOList.Col = lngColNo + 1
            sprdPOList.Text = vntLine(lngColNo)
        Next lngColNo
    Next lngRowNo
    'Ensure Qty column is Right Aligned
    sprdPOList.Row = 0
    For lngColNo = 1 To sprdPOList.MaxCols Step 1
        sprdPOList.Col = lngColNo
        If sprdPOList.Text = "Qty" Then
            sprdPOList.Row = -1
            sprdPOList.TypeHAlign = TypeHAlignRight
            Exit For
        End If
    Next lngColNo
    Call sprdPOList.Refresh
    Call sprdPOList.PrintSheet
    sprdPOList.MaxRows = 0

End Sub

Private Sub cmdSearch_Click()

Dim oOrder      As Object
Dim oMatches    As Collection
Dim lngMatchNo  As Long
Dim oRow        As Object
Dim oField      As Object
Dim strDispPart As String
Dim strDispComp As String
Dim strSuppNo   As String
Dim strSuppName As String
Dim strOrderNo  As String
Dim strQtyFmt   As String
Dim strEDIFlag  As String
Dim blnAutoRet  As Boolean

On Error GoTo Bad_Apply
            
    Screen.MousePointer = vbHourglass
    
    Call lstMatches.Clear
    lstMatches.Enabled = False
    cmdEnquiry.Visible = False
    cmdPrintList.Visible = False
    
    lblNoMatches.BackColor = RGB_WHITE
    lblNoMatches.ForeColor = RGB_BLACK
    lblNoMatches.FontBold = False
    lblNoMatches.Caption = "Searching..."
    cmdUse.Visible = False
    
    DoEvents
    If goDatabase Is Nothing Then
        Call GetRoot
        Call Initialise(goSession)
    End If
        
    Set oOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
            
    'Display pending orders only
    Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_ReceivedAll, False)
    
    If LenB(txtFilterOrderNo.Text) <> 0 Then
        If InStr(txtFilterOrderNo.Text, "%") > 0 Then
            Call oOrder.IBo_AddLoadFilter(CMP_LIKE, FID_PURCHASEORDER_OrderNumber, txtFilterOrderNo.Text)
        Else
            strOrderNo = txtFilterOrderNo.Text
            If Len(strOrderNo) < ORDER_LEN Then strOrderNo = Left$(ORDER_PAD, ORDER_LEN - Len(strOrderNo)) & strOrderNo
            Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_OrderNumber, strOrderNo)
            blnAutoRet = True
        End If
    End If
    If chkActive.Value = 1 Then Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_DeletedByMaint, False)
        
    Call oOrder.IBo_AddLoadFilter(CMP_NOTEQUAL, FID_PURCHASEORDER_ReleaseNumber, "  ")
    If cmbFilterSupp.Text <> "*ALL*" & vbTab & "*ALL*" & vbTab & vbTab Then
        Call oOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_SupplierNo, cmbFilterSupp.ColText)
    End If
    
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_Key)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_OrderNumber)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_ReleaseNumber)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_SupplierNo)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_OrderDate)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_DueDate)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_RaisedBy)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_NoOfCartons)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_QuantityOnOrder)
    Call oOrder.IBo_AddLoadField(FID_PURCHASEORDER_ReceivedPart)
    Set oMatches = oOrder.IBo_LoadMatches
    
    
    lblNoMatches.Caption = oMatches.Count
    DoEvents
    
    For lngMatchNo = 1 To oMatches.Count Step 1
        If oMatches(lngMatchNo).ReceivedPart Then
            strDispPart = "Yes"
        Else
            strDispPart = "No"
        End If
        'Display supplier name using Supplier number to look up in selection combo
        If strSuppNo <> oMatches(lngMatchNo).SupplierNo Then
            strEDIFlag = "No"
            cmbFilterSupp.SearchMethod = SearchMethodPartialMatch
            cmbFilterSupp.SearchText = oMatches(lngMatchNo).SupplierNo
            cmbFilterSupp.Action = ActionSearch
            If cmbFilterSupp.SearchIndex >= 0 Then
                strSuppName = cmbFilterSupp.List(cmbFilterSupp.SearchIndex)
                strSuppNo = Left$(strSuppName, InStr(strSuppName, vbTab) - 1)
                strSuppName = Mid$(strSuppName, InStr(strSuppName, vbTab) + 1)
                strEDIFlag = Mid$(strSuppName, InStr(strSuppName, vbTab) + 1)
                strSuppName = Left$(strSuppName, InStr(strSuppName, vbTab) - 1)
                strEDIFlag = Mid$(strEDIFlag, InStr(strEDIFlag, vbTab) + 1)
            Else
                strSuppNo = oMatches(lngMatchNo).SupplierNo
                strSuppName = "NOT LOCATED"
                strEDIFlag = "N/A"
            End If
        End If
        Call lstMatches.AddItem(oMatches(lngMatchNo).OrderNumber & vbTab & oMatches(lngMatchNo).ReleaseNumber & vbTab & strSuppName & vbTab & strEDIFlag & vbTab & DisplayDate(oMatches(lngMatchNo).OrderDate, False) & vbTab & DisplayDate(oMatches(lngMatchNo).DueDate, False) & vbTab & oMatches(lngMatchNo).RaisedBy & vbTab & oMatches(lngMatchNo).NoOfCartons & vbTab & Format$(oMatches(lngMatchNo).QuantityOnOrder, m_strQtyFormat) & vbTab & strDispPart)
        lstMatches.ItemData(lstMatches.NewIndex) = oMatches(lngMatchNo).Key
    Next lngMatchNo
    
    If lstMatches.ListCount > 0 Then
        lstMatches.Enabled = True
        cmdUse.Visible = True
        cmdEnquiry.Visible = True
        cmdPrintList.Visible = True
        lstMatches.Selected(0) = True
        lstMatches.SetFocus
    Else
        Call Display_NoMatches(lblNoMatches)
        txtFilterOrderNo.SetFocus
        cmdEnquiry.Visible = False
        cmdPrintList.Visible = False
    End If
    
    Screen.MousePointer = vbNormal
    
    'if only 1 item retrieved then prompt to auto retrieve
    If lstMatches.ListCount = 1 Then
        If blnAutoRet = True Then
            Call cmdUse_Click
        Else
            If MsgBox("Only 1 purchase order retrieved - process (Y/N)", vbYesNo, "Retrieve purchase order") = vbYes Then Call cmdUse_Click
        End If
    End If
    
    Exit Sub
    
Bad_Apply:
    
    Screen.MousePointer = vbNormal
    lblNoMatches.Caption = "Error..." & Err.Number
    lblNoMatches.BackColor = RGB_RED
    lblNoMatches.ForeColor = RGB_WHITE
    lblNoMatches.FontBold = True
    Exit Sub

End Sub


Private Sub cmdClear_GotFocus()

    cmdClear.FontBold = True

End Sub

Private Sub cmdClear_LostFocus()
    
    cmdClear.FontBold = False

End Sub

Private Sub cmdClose_Click()
    
    RaiseEvent Cancel

End Sub
'

Public Property Let SupplierNo(ByVal sSupplierCode As String)
    
    Call cmbFilterSupp.Clear
    If LenB(sSupplierCode) <> 0 Then
        cmbFilterSupp.AddItem (sSupplierCode)
        cmbFilterSupp.ListIndex = 0
        cmbFilterSupp.Enabled = False
    Else
        cmbFilterSupp.Enabled = True
    End If
    
End Property
'
Private Sub cmdClear_Click()

    Screen.MousePointer = vbHourglass
    
    txtFilterOrderNo.Text = vbNullString
    cmbFilterSupp.Text = "*ALL*" & vbTab & "*ALL*" & vbTab & vbTab
    lstMatches.Clear
    cmdUse.Visible = False
    cmdEnquiry.Visible = False
    lblNoMatches.Caption = "0"
    
    If txtFilterOrderNo.Visible = True Then txtFilterOrderNo.SetFocus
    cmdClear.Visible = False
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdClose_GotFocus()

    cmdClose.FontBold = True

End Sub

Private Sub cmdClose_LostFocus()

    cmdClose.FontBold = False

End Sub

Private Sub cmdSearch_GotFocus()

    cmdSearch.FontBold = True

End Sub

Private Sub cmdSearch_LostFocus()

    cmdSearch.FontBold = False

End Sub

Private Sub cmdUse_Click()

    m_OrderKey = lstMatches.ItemData(lstMatches.ListIndex)
    RaiseEvent Apply

End Sub

Private Sub cmdUse_GotFocus()

    cmdUse.FontBold = True

End Sub

Private Sub cmdUse_LostFocus()
    
    cmdUse.FontBold = False

End Sub

Private Sub Label1_Click()
    
    Label1.ToolTipText = "Ver." & App.Major & "." & App.Minor & "." & App.Revision

End Sub

Private Sub lstMatches_GotFocus()

    cmdUse.Visible = True

End Sub

Private Sub lstMatches_KeyPress(KeyAscii As Integer)

    If KeyAscii = 27 Then txtFilterOrderNo.SetFocus
    If KeyAscii = 13 Then Call cmdUse_Click

End Sub


Private Sub txtFilterOrderNo_Change()

    cmdClear.Visible = True

End Sub

Private Sub txtFilterOrderNo_GotFocus()

    txtFilterOrderNo.BackColor = RGBEdit_Colour
    txtFilterOrderNo.SelStart = 0
    txtFilterOrderNo.SelLength = Len(txtFilterOrderNo.Text)
    
End Sub


Private Sub txtFilterOrderNo_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        If LenB(txtFilterOrderNo.Text) <> 0 Then
            Call cmdSearch_Click
            KeyAscii = vbNull
        Else
            Call SendKeys(vbTab)
            DoEvents
        End If
    End If

End Sub

Private Sub txtFilterOrderNo_LostFocus()
    
    txtFilterOrderNo.BackColor = lngOldColour

End Sub

Private Sub UserControl_EnterFocus()

    Call DebugMsg(MODULE_NAME, "UC_EnterFocus", endlTraceIn)
'    txtFilterOrderNo.SetFocus

End Sub

Private Sub UserControl_Hide()
    
    Call DebugMsg(MODULE_NAME, "UC_Hide", endlDebug, "Resetting preview")
    If Not oParent Is Nothing Then oParent.KeyPreview = blnPreview

End Sub
'<CACH>****************************************************************************************
'* Sub:  ViewOrder()
'**********************************************************************************************
'* Description: called to display the details of Purchase Order select from the retrieved list.
'**********************************************************************************************
'* Parameters:
'* None.
'**********************************************************************************************
'* History:
'* 02/06/03    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub ViewOrder(ByVal lngOrderID As Long, ByVal OrderString As String)

Dim oPOrder  As Object
Dim strNotes As String
'Dim strPOKey As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".ViewOrder"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
    strNotes = vbNullString
    ucpbProgress.Visible = True
    DoEvents
    
    Set oPOrder = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
'    strPOKey = Left$(OrderString, InStr(OrderString, vbTab) - 1)
    
    'Throw away Order and Release Number
    OrderString = Mid$(OrderString, InStr(OrderString, vbTab) + 1)
    OrderString = Mid$(OrderString, InStr(OrderString, vbTab) + 1)
    
    'Get Supplier name from string
    lblSupplier.Caption = Mid$(OrderString, InStr(OrderString, vbTab) + 1)
    lblSupplier.Caption = Left$(OrderString, InStr(OrderString, vbTab) - 1)

    Call oPOrder.IBo_AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_Key, lngOrderID)
    oPOrder.GetAllLines = True
    strNotes = vbNullString
    If oPOrder.IBo_Load Then
        Call DisplayOrderLines(oPOrder)
        strNotes = oPOrder.Narrative
        If InStr(strNotes, vbCrLf) > 0 Then
            lblNotes1.Caption = Left$(strNotes, InStr(strNotes, vbCrLf) - 1)
            lblNotes2.Caption = Mid$(strNotes, InStr(strNotes, vbCrLf) + 2)
        Else
            lblNotes1.Caption = strNotes
            lblNotes2.Caption = vbNullString
        End If
        lblOrderDate.Caption = DisplayDate(oPOrder.OrderDate, False)
        lblDueDate.Caption = DisplayDate(oPOrder.DueDate, False)
        lblOrderNo.Caption = oPOrder.OrderNumber & "/" & oPOrder.ReleaseNumber
        'if
    End If
    
    fraDetails.Visible = True
    Call sprdPOLines.SetFocus
    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Report(APP_NAME, PROCEDURE_NAME, Erl)
    Call Err.Clear
    Exit Sub
Resume Next
Finally:
    'Perform tidy up here
    ucpbProgress.Visible = False
    Return
    '</CAEH>

End Sub 'ucofOrders_Apply


Private Sub UserControl_Initialize()

Dim lngColNo As Long

    Call DebugMsg(MODULE_NAME, "UC_Initialise", endlTraceIn)
    RGBEdit_Colour = lblNoMatches.BackColor
    lngOldColour = lblNoMatches.BackColor
    m_SearchKey = 7
    m_CancelKey = 12
    
    'set up print list
    sprdPOList.MaxRows = 0
    sprdPOList.MaxCols = lstMatches.Columns
    sprdPOList.Row = 0
    'Transfer over headers and col widths
    For lngColNo = 0 To lstMatches.Columns - 1 Step 1
        sprdPOList.Col = lngColNo + 1
        lstMatches.Col = lngColNo
        sprdPOList.Text = lstMatches.ColHeaderText
        If lstMatches.ColHide = True Then sprdPOList.ColHidden = True
        sprdPOList.ColWidth(lngColNo + 1) = lstMatches.ColWidth * 0.8
    Next lngColNo

End Sub

Private Sub UserControl_InitProperties()

    UserControl.Width = 7365

    m_ForeColor = m_def_ForeColor
    m_Caption = m_def_Caption
    m_OrderKey = m_def_OrderKey
    
End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Call DebugMsg(MODULE_NAME, "kp", endlDebug, KeyCode)
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF3):
                            If cmdClear.Visible = True Then Call cmdClear_Click
            Case (vbKeyF5):
                            If cmdUse.Visible = True Then
                                KeyCode = 0
                                Call cmdUse_Click
                                Exit Sub
                            End If
            Case (vbKeyF9):
                            If cmdPrint.Visible = True Then
                                Call cmdPrint_Click
                            Else
                                If cmdPrintList.Visible = True Then Call cmdPrintList_Click
                            End If
            Case (vbKeyF11):
                            If cmdEnquiry.Visible = True Then Call cmdEnquiry_Click
            Case (m_SearchKey + 111):
                            KeyCode = 0
                            Call cmdSearch_Click
            Case (m_CancelKey + 111):
                            If cmdClose.Visible Then
                                KeyCode = 0
                                Call cmdClose_Click
                            End If
            Case (vbKeyF12):
                            If cmdDtlClose.Visible = True Then
                                KeyCode = 0
                                Call cmdDtlClose_Click
                            End If
             Case (vbKeyF10): KeyCode = 0
        End Select
    End If

End Sub

Private Sub UserControl_KeyPress(KeyAscii As Integer)

    If (TypeOf ActiveControl Is CommandButton) And (KeyAscii = 27) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

    fraOrderFilter.Caption = PropBag.ReadProperty("Caption", "Select Item Filter Criteria")
'    UserControl.Width = PropBag.ReadProperty("Width", 6765)
    
    m_ForeColor = PropBag.ReadProperty("ForeColor", m_def_ForeColor)
    m_Caption = PropBag.ReadProperty("Caption", m_def_Caption)
    m_OrderKey = PropBag.ReadProperty("OrderKey", m_def_OrderKey)
End Sub

Private Sub UserControl_Resize()

    UserControl.Width = 8940
    UserControl.Height = 7605

End Sub

Private Sub UserControl_Show()

Dim strMsg As String
    
    If oParent Is Nothing Then
        strMsg = "No Parent"
    Else
        strMsg = "Parent=" & oParent.Name
    End If
    Call DebugMsg(MODULE_NAME, "SHOW", endlDebug, "Parent is " & strMsg)
    If Not oParent Is Nothing Then
        blnPreview = oParent.KeyPreview
        oParent.KeyPreview = False
    End If
    txtFilterOrderNo.SetFocus

End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

    Call PropBag.WriteProperty("Caption", fraOrderFilter.Caption, "Select Item Filter Criteria")
    Call PropBag.WriteProperty("ForeColor", fraOrderFilter.ForeColor, 0)

    Call PropBag.WriteProperty("ForeColor", m_ForeColor, m_def_ForeColor)
    Call PropBag.WriteProperty("Caption", m_Caption, m_def_Caption)
    Call PropBag.WriteProperty("OrderKey", m_OrderKey, m_def_OrderKey)
End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=14
Public Sub Initialise(ByRef CurrentSession As Object, Optional ByRef ParentForm As Object = Nothing)

Dim oDept        As Object
Dim oSupp        As Object
Dim oBOCol       As Collection
Dim lItemNo      As Long
Dim lngQtyDecNum As Long
Dim strEDIFlag   As String

    Call DebugMsg(MODULE_NAME, "UC_EnterFocus", endlTraceIn, "Initialise-" & blnInit)
    If blnInit Then Exit Sub
    ' Set up the object and field that we wish to select on
    DoEvents
    Set oParent = ParentForm
    
    Set goSession = CurrentSession
    Set goDatabase = goSession.Database
    
    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBQuery_BackColour = goSession.GetParameter(PRM_QUERY_BACKCOLOUR)
    fraOrderFilter.BackColor = RGBQuery_BackColour
    fraDetails.BackColor = RGBQuery_BackColour
    chkActive.BackColor = RGBQuery_BackColour
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstMatches.ListApplyTo = ListApplyToEvenRows
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    lstMatches.ListApplyTo = ListApplyToOddRows
    lstMatches.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    lstMatches.Row = -1
    lstMatches.RowHeight = goSession.GetParameter(PRM_QUERY_ROWHEIGHT)
    
    'set user parameters for Details list
    sprdPOLines.BackColor = goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR)
    Call sprdPOLines.SetOddEvenRowColor(goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR), RGB_BLACK, goSession.GetParameter(PRM_QUERY_GRIDEVENCOLOUR), RGB_BLACK)
    sprdPOLines.Row = -1
    sprdPOLines.Col = COL_QTY
    sprdPOLines.TypeNumberDecPlaces = lngQtyDecNum
    sprdPOLines.Col = COL_QTYOS
    sprdPOLines.TypeNumberDecPlaces = lngQtyDecNum
    
    m_strQtyFormat = String$(lngQtyDecNum, "0")
    If LenB(m_strQtyFormat) <> 0 Then m_strQtyFormat = "0." & m_strQtyFormat
        
    If cmbFilterSupp.Enabled Then
        Call cmbFilterSupp.AddItem("*ALL*" & vbTab & "*ALL*")
        cmbFilterSupp.Row = cmbFilterSupp.NewIndex
        cmbFilterSupp.Text = cmbFilterSupp.List(cmbFilterSupp.Row)
        Set oSupp = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERS)
        
        Call oSupp.GetList
        While Not oSupp.EndOfList
            If oSupp.EDI = True Then
                strEDIFlag = "Yes"
            Else
                strEDIFlag = "No"
            End If
            Call cmbFilterSupp.AddItem(oSupp.EntryString & vbTab & Left$(oSupp.SupplierNo, 2) & Val(Mid$(oSupp.SupplierNo, 3)) & vbTab & strEDIFlag)
            Call oSupp.MoveNext
        Wend
        Set oBOCol = Nothing
    End If
    
    cmdClear.Visible = False
    blnInit = True
    Call DebugMsg(MODULE_NAME, "UC_EnterFocus", endlTraceOut, "Initialise-" & blnInit)

End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=14
Public Function Reset() As Variant
    
    If (cmbFilterSupp.Enabled) Then Call cmdClear_Click
    blnInit = False

End Function

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=10,0,0,
Public Property Get ForeColor() As OLE_COLOR
    ForeColor = m_ForeColor
End Property

Public Property Let ForeColor(ByVal New_ForeColor As OLE_COLOR)
    m_ForeColor = New_ForeColor
    PropertyChanged "ForeColor"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=13,0,0,
Public Property Get Caption() As String
    Caption = m_Caption
End Property

Public Property Let Caption(ByVal New_Caption As String)
    m_Caption = New_Caption
    PropertyChanged "Caption"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=8,1,2,0
Public Property Get OrderKey() As Long
    OrderKey = m_OrderKey
End Property
'
'
'Public Sub ShowGrid()
'
'    txtFilterOrderNo.SetFocus
'
'End Sub
'
'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MemberInfo=14
Public Function Show() As Variant

End Function


'<CACH>****************************************************************************************
'* Sub:  DisplayOrderLines()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'*In/Out:oPOrder    Object.
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Sub DisplayOrderLines(oPOrder As Object)

Dim lLineNo As Long
Dim strLine As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".DisplayOrderLines"

    On Error GoTo Catch
Try:
    'Main procedure code goes here

    ucpbProgress.Max = oPOrder.Lines.Count + 1
    ucpbProgress.Caption1 = "Retrieving Lines for Order"
    sprdPOLines.MaxRows = 0
    Call sprdPOLines.Refresh
    sprdPOLines.MaxRows = oPOrder.Lines.Count
    For lLineNo = 1 To oPOrder.Lines.Count Step 1
        ucpbProgress.Value = lLineNo
        sprdPOLines.Row = lLineNo
        sprdPOLines.Col = COL_NO
        sprdPOLines.Text = oPOrder.Lines(CLng(lLineNo)).LineNo
        sprdPOLines.Col = COL_SKU
        sprdPOLines.Text = oPOrder.Lines(CLng(lLineNo)).PartCode
        sprdPOLines.Col = COL_DESC
        sprdPOLines.Text = GetItemDescription(oPOrder.Lines(CLng(lLineNo)).PartCode)
        sprdPOLines.Col = COL_QTY
        sprdPOLines.Text = oPOrder.Lines(CLng(lLineNo)).OrderQuantity
        sprdPOLines.Col = COL_QTYOS
        sprdPOLines.Text = oPOrder.Lines(CLng(lLineNo)).OrderQuantity - oPOrder.Lines(CLng(lLineNo)).QuantityReceived
'        sprdPOLines.Text = oPOrder.Lines(CLng(lLineNo)).CostPrice
'        sprdPOLines.Text = oPOrder.Lines(CLng(lLineNo)).CustomerOrderNo
    Next lLineNo
    sprdPOLines.ReDraw = True

    'Tidy up
    GoSub Finally

    Exit Sub

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, Erl)
    Exit Sub

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Sub 'DisplayOrderLines


'<CACH>****************************************************************************************
'* Function:  Boolean GetItemDescription()
'**********************************************************************************************
'* Description: Used to retrieve an Item given the SKU and retrieve and return description.
'**********************************************************************************************
'* Parameters:
'*In/Out:strPartCode  String.-Part Code used to retrieve item - must be ready formatted
'**********************************************************************************************
'* Returns:  String - hold description for passed in SKU
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function GetItemDescription(strPartCode As String) As String

Dim oItem   As Object
Dim sErrMsg As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetItemDescription"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
                
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Description)
    
    Call oItem.IBo_LoadMatches
    
    GetItemDescription = oItem.Description
    Set oItem = Nothing

    'Tidy up
    GoSub Finally

    Exit Function

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, 1)
    Exit Function

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Function 'GetItem


