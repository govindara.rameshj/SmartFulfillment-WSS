Attribute VB_Name = "modReceipt"
'<CAMH>************************************************************************************
'* Module : modReceipt
'* Date   : 17/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Till/modReceipt.bas $
'******************************************************************************************
'* Summary: All of the printing routines used by the till.
'******************************************************************************************
'* $Author: Mauricem $ $Date: 20/07/04 9:13 $ $Revision: 39 $
'* Versions:
'* 17/02/03    Unknown
'*             Header added.
'</CAMH>***********************************************************************************
Option Explicit

Const MODULE_NAME As String = "modReceipt"

Const ESC_PRINT_LOGO      As String = "|1B"
Const ESC_PRINT_BIG       As String = "|2C"
Const ESC_PRINT_HUGE      As String = "|4C"
Const ESC_PRINT_BOLD      As String = "|bC"
Const ESC_PRINT_UNDERLINE As String = "|uC"
Const ESC_PRINT_NORMAL    As String = "|N"

Private ESC_PRINT_COMPRESSED As String

Const UNIT_1P_CLAUSE As String = "   UNIT PRICES SHOWN TO THE NEAREST 1p" & vbCrLf

Const SALES_ORDER_CONFIRM As String = "           I CONFIRM MY ORDER" & vbCrLf & "       FOR THE ABOVE STATED GOODS" & vbCrLf & vbCrLf & "Signed.................................." & vbCrLf

Const CUSTOMER_RECEIPT As String = "Customer Copy"
Const STORE_RECEIPT As String = "Store Copy"

Private mstrTextFilePath        As String
Private mstrRcptHeader          As String
Private mstrRcptPreTrailer      As String
Private mstrRcptTrailer         As String
Private mstrInvTrailer1         As String
Private mstrInvTrailer1Refund   As String
Private mstrInvTrailer2         As String
Private mstrInvTrailer3         As String
Private mstrRcptParked          As String
Private mstrCCardTrailer        As String
Private mstrCancelOTrailer      As String
Private mlngPrinterCode         As Long
Private mstrDivLine             As String

Private mblnCustomerCopy    As Boolean
Public mblnIgnorePrint      As Boolean 'Used to switch printing off - mainly for Training Mode

'''    mstrTextFilePath = goSession.GetParameter(PRM_TRANSNO)
'''    mlngPrinterCode = oPrinter.Open(goSession.GetParameter(PRM_PRINTERNAME))


' Public Sub GetPrinterHeaderTrailer(ByRef oPrinter As OPOSPOSPrinter)
Public Sub GetPrinterHeaderTrailer(ByRef oPrinter As Object)

Dim oFSO   As FileSystemObject
Dim tsFile As TextStream

    Set oFSO = New FileSystemObject
    
ESC_PRINT_COMPRESSED = "!" & Chr$(1)
    On Error Resume Next
    'Pre-Load document header data
    
    Call DebugMsg("modReceipt", "GetPrinterHeaderTrailer", endlDebug, "Open Path-" & mstrTextFilePath)
    mstrRcptHeader = "Header File Not Found"
    Set tsFile = oFSO.OpenTextFile(mstrTextFilePath & "header.", ForReading, False, TristateUseDefault)
    If ((tsFile Is Nothing) = False) Then
        mstrRcptHeader = tsFile.ReadAll
        Call tsFile.Close
    End If

    mstrRcptTrailer = "Trailer File Not Found"
    Set tsFile = oFSO.OpenTextFile(mstrTextFilePath & "trailer.", ForReading, False, TristateUseDefault)
    If ((tsFile Is Nothing) = False) Then
        mstrRcptTrailer = tsFile.ReadAll
        Call tsFile.Close
    End If

    mstrRcptParked = "Park Trailer File Not Found"
    Set tsFile = oFSO.OpenTextFile(mstrTextFilePath & "park_trlr.", ForReading, False, TristateUseDefault)
    If ((tsFile Is Nothing) = False) Then
        mstrRcptParked = tsFile.ReadAll
        Call tsFile.Close
    End If
    
    mstrRcptPreTrailer = "XXXX" & vbCrLf & "XXXX" & vbCrLf
    
    mstrCancelOTrailer = "I confirm cancellation of my order" & vbCrLf

    'load logo to Printer
    Call DebugMsg("modReceipt", "GetPrinterHeaderTrailer", endlDebug, "Open Return Code: " & CStr(mlngPrinterCode) & " : " & oPrinter.ResultCodeExtended)
    If mlngPrinterCode = 0 Then
        mlngPrinterCode = oPrinter.ClaimDevice(2000)
        Call DebugMsg("modReceipt", "GetPrinterHeaderTrailer", endlDebug, "Claim Return Code: " & CStr(mlngPrinterCode) & " : " & oPrinter.ResultCodeExtended)
    End If
    If mlngPrinterCode = 0 Then
        oPrinter.DeviceEnabled = True
        mlngPrinterCode = oPrinter.ResultCode
        Call DebugMsg("modReceipt", "GetPrinterHeaderTrailer", endlDebug, "Enable Return Code: " & CStr(mlngPrinterCode) & " : " & oPrinter.ResultCodeExtended)
    End If
    If mlngPrinterCode = 0 Then
        oPrinter.CharacterSet = 437
        If oFSO.FileExists("C:\logo.bmp") Then mlngPrinterCode = oPrinter.SetBitmap(1, FptrSReceipt, "c:\logo.bmp", oPrinter.RecLineWidth, PtrBmCenter)
        Call DebugMsg("modReceipt", "GetPrinterHeaderTrailer", endlDebug, "Load Bitmap Return Code: " & CStr(mlngPrinterCode) & " : " & oPrinter.ResultCodeExtended)
    End If
    If mlngPrinterCode <> 0 Then
        Set oPrinter = Nothing
    End If
End Sub

' Public Sub PrintHeader(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintHeader(ByRef oPrinter As Object)
    Dim lngSaveSpacing As Long
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    'End till receipt by cutting paper, if available else feed extra lines
'    If oPrinter.CapRecPapercut = True Then
'    'MUST ADD BACK IN
'        Call DoRecPrint(oPrinter, Chr$(27) + "|fP")
'    Else
'        Call DoRecPrint(oPrinter, Chr$(27) + "|" + CStr(oPrinter.RecLinesToPaperCut) + "lF")
'    End If
    
    lngSaveSpacing = oPrinter.RecLineSpacing
    oPrinter.RecLineSpacing = oPrinter.RecLineHeight
'    Call oPrinter.PrintNormal(FptrSReceipt, vbNewLine)
'    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_LOGO)
'    Call oPrinter.PrintImmediate(FptrSReceipt, Chr$(27) & Chr$(105))
    Call DoRecPrint(oPrinter, mstrRcptHeader)
    oPrinter.RecLineSpacing = lngSaveSpacing
End Sub

' Public Sub RePrintLogo(ByRef oPrinter As OPOSPOSPrinter)
Public Sub RePrintLogo(ByRef oPrinter As Object)

Dim lngSaveSpacing As Long

    lngSaveSpacing = oPrinter.RecLineSpacing
    oPrinter.RecLineSpacing = oPrinter.RecLineHeight
    Call oPrinter.PrintNormal(FptrSReceipt, vbNewLine)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_LOGO)
'    Call oPrinter.PrintImmediate(FptrSReceipt, Chr$(27) & Chr$(105))
    oPrinter.RecLineSpacing = lngSaveSpacing

End Sub

'Public Sub PrintTransactionHeader(ByRef oPrinter As OPOSPOSPrinter, _
'                                  ByVal dteTrandate As Date, _
'                                  ByVal strTranTime As String, _
'                                  ByVal strTillID As String, _
'                                  ByVal strTranID As String, _
'                                  ByVal strTranType As String, _
'                                  ByVal strUserId As String, _
'                                  ByVal strUserName As String, _
'                                  ByVal strOrderNo As String)
Public Sub PrintTransactionHeader(ByRef oPrinter As Object, _
                                  ByVal dteTrandate As Date, _
                                  ByVal strTranTime As String, _
                                  ByVal strTillID As String, _
                                  ByVal strTranID As String, _
                                  ByVal strTranType As String, _
                                  ByVal strUserId As String, _
                                  ByVal strUserName As String, _
                                  ByVal strOrderNo As String)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call PrintHeader(oPrinter)
    strTranTime = Left$(strTranTime, 4)
    strTranTime = Left$(strTranTime, 2) & ":" & Mid$(strTranTime, 3)
    
    strTranType = Space$((22 - Len(strTranType)) / 2) & strTranType
    strTranType = strTranType & Space$(22 - Len(strTranType))
    Call DoRecPrint(oPrinter, Format$(dteTrandate, "DD/MM/YY         ") & strTranTime & "           " & strTillID & "-" & strTranID & vbCrLf & vbNewLine)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD & strTranType & vbCrLf)
    Call DoRecPrint(oPrinter, strUserId & " " & strUserName & vbCrLf)
    If Val(strOrderNo) > 0 Then Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BOLD & Chr$(27) & ESC_PRINT_UNDERLINE & "Order Number : " & strOrderNo & vbCrLf)
    
    Call PrintDivideLine(oPrinter)
        
End Sub

'Public Sub PrintLine(ByRef oPrinter As OPOSPOSPrinter, _
'                     ByVal strPartCode As String, _
'                     ByVal strDescription As String, _
'                     ByVal strDescription2 As String, _
'                     ByVal dblQuantity As Double, _
'                     ByVal dblPrice As Double, _
'                     ByVal dblTotal As Double, _
'                     ByVal strVATCode As String, _
'                     ByVal blnCancelled As Boolean)
Public Sub PrintLine(ByRef oPrinter As Object, _
                     ByVal strPartCode As String, _
                     ByVal strDescription As String, _
                     ByVal strDescription2 As String, _
                     ByVal dblQuantity As Double, _
                     ByVal dblPrice As Double, _
                     ByVal dblTotal As Double, _
                     ByVal strVATCode As String, _
                     ByVal blnCancelled As Boolean)
                      
Dim strTotal As String
Dim strPrice As String
Dim strSign  As String
Dim lngPos   As Long

Dim strOPDiscTotal  As String
Dim strOPSplit      As String

Dim strQuantity    As String
Dim strQtySign     As String
                     
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    strPrice = Format$(dblPrice, "0.00")
    If blnCancelled = True Then
        strTotal = "Cancelled"
        strSign = " "
        strVATCode = " "
    Else
        strTotal = Format$(Abs(dblTotal), "0.00")
        strSign = " "
        If dblTotal < 0 Then strSign = "-"
    End If
    
    strQuantity = Format$(Abs(dblQuantity), "0")
    strQtySign = " "
    If dblQuantity < 0 Then strQtySign = "-"
    
    'join string together and right align
    strTotal = Space$(22 - Len(strTotal & strPrice & strQuantity & strQtySign)) & strTotal
    Call DoRecPrint(oPrinter, strDescription & vbCrLf)
    If strPartCode <> "000000" Then
        Call DoRecPrint(oPrinter, "Sku " & strPartCode & " " & dblQuantity & strQtySign & "@ " & Chr$(156) & strPrice & strTotal & strSign & " " & strVATCode & vbCrLf)
    Else
        Call DoRecPrint(oPrinter, dblQuantity & strQtySign & "@ " & Chr$(156) & strPrice & strTotal & strSign & " " & strVATCode & vbCrLf)
    End If

End Sub

' Public Sub PrintFooter(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintFooter(ByRef oPrinter As Object)
    Dim lngSaveSpacing As Long
    
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    lngSaveSpacing = oPrinter.RecLineSpacing
    oPrinter.RecLineSpacing = oPrinter.RecLineHeight
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_NORMAL & mstrRcptPreTrailer)
    Call DoRecPrint(oPrinter, mstrRcptTrailer)
    
CutPaper:
    Call DoRecPrint(oPrinter, vbNewLine)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_LOGO)
    'End till receipt by cutting paper,
    'if available else feed extra lines
    If oPrinter.CapRecPapercut = True Then 'MUST ADD BACK IN
        Call oPrinter.PrintImmediate(FptrSReceipt, Chr$(27) & Chr$(105))
    Else
        Call DoRecPrint(oPrinter, Chr$(27) + "|" + CStr(oPrinter.RecLinesToPaperCut) + "lF")
    End If
    oPrinter.RecLineSpacing = lngSaveSpacing

End Sub

'Public Sub PrintAddress(ByRef oPrinter As OPOSPOSPrinter, _
'                        ByVal strName As String, _
'                        ByVal strAddress As String, _
'                        ByVal strPostCode As String, _
'                        ByVal strTelNo As String)
Public Sub PrintAddress(ByRef oPrinter As Object, _
                        ByVal strName As String, _
                        ByVal strAddress As String, _
                        ByVal strPostCode As String, _
                        ByVal strTelNo As String)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    'Filter out any blank lines
    While (InStr(strAddress, vbCrLf & vbCrLf) > 0)
        strAddress = Replace(strAddress, vbCrLf & vbCrLf, vbCrLf)
    Wend
    If Right$(strAddress, 2) = vbCrLf Then strAddress = Left$(strAddress, Len(strAddress) - 2)
    
    Call DoRecPrint(oPrinter, strName & vbCrLf)
    Call DoRecPrint(oPrinter, strAddress & vbCrLf)
    Call DoRecPrint(oPrinter, strPostCode & vbCrLf)
    Call DoRecPrint(oPrinter, "Tel : " & strTelNo & vbCrLf)
    
End Sub 'PrintAddress


'Public Sub PrintDivideLine(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintDivideLine(ByRef oPrinter As Object)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    If LenB(mstrDivLine) = 0 Then
        mstrDivLine = String$(40, Chr$(205))
    End If
    Call DoRecPrint(oPrinter, mstrDivLine & vbCrLf)

End Sub

'Public Sub PrintTranTotal(ByRef oPrinter As OPOSPOSPrinter, _
'                          ByVal strTranDesc As String, _
'                          ByVal dblTotal As Double, _
'                          ByVal blnPrintBig As Boolean)
Public Sub PrintTranTotal(ByRef oPrinter As Object, _
                          ByVal strTranDesc As String, _
                          ByVal dblTotal As Double, _
                          ByVal blnPrintBig As Boolean)

Dim strTranType As String
Dim strAmount   As String

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call PrintDivideLine(oPrinter)
    strTranType = strTranDesc & " Total"
    strAmount = Format$(dblTotal, "0.00   ")
    strTranType = Space$(27 - Len(strTranType)) & strTranType
    strTranType = strTranType & Space$(13 - Len(strAmount)) & strAmount
    Call DoRecPrint(oPrinter, strTranType & vbCrLf)
    If blnPrintBig = True Then
        Call PrintDivideLine(oPrinter)
        strAmount = Format$(dblTotal, Chr$(156) & "0.00")
        strAmount = Space$(15 - Len(strAmount)) & strAmount
        Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD & strAmount & vbCrLf)
    End If
    Call PrintDivideLine(oPrinter)

End Sub

'Public Sub PrintPayment(ByRef oPrinter As OPOSPOSPrinter, ByVal dblAmount As Double, ByVal strPmtType As String)
Public Sub PrintPayment(ByRef oPrinter As Object, ByVal dblAmount As Double, ByVal strPmtType As String)

Dim strAmount   As String

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    'ensure that payment method is 15 characters long
    If Len(strPmtType) > 15 Then strPmtType = Left$(strPmtType, 15) 'if too large then truncate
    strPmtType = Right$(Space$(15) & strPmtType, 15)
    
    strAmount = Format$(dblAmount, "0.00")
    strPmtType = Space$(12) & strPmtType & Space$(15 - Len(strAmount)) & strAmount
    Call DoRecPrint(oPrinter, strPmtType & vbCrLf)

End Sub

'Public Sub PrintVATBreakdown(ByRef oPrinter As OPOSPOSPrinter, ByVal dblAmount As Double, ByVal strPmtType As String)
Public Sub PrintVATBreakdown(ByRef oPrinter As Object, ByVal dblAmount As Double, ByVal strPmtType As String)

Dim strAmount   As String

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    'ensure that payment method is 15 characters long
    If Len(strPmtType) > 15 Then strPmtType = Left$(strPmtType, 15) 'if too large then truncate
    strPmtType = Right$(Space$(15) & strPmtType, 15)
    
    strAmount = Format$(dblAmount, "0.00")
    strPmtType = Space$(12) & strPmtType & Space$(15 - Len(strAmount)) & strAmount
    Call DoRecPrint(oPrinter, strPmtType & vbCrLf)

End Sub

'Public Sub PrintSignatureLine(ByRef oPrinter As OPOSPOSPrinter, ByVal strSignPrompt As String)
Public Sub PrintSignatureLine(ByRef oPrinter As Object, ByVal strSignPrompt As String)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    'Pad out sign prompt so fixed line length
    While (Len(strSignPrompt) < 40) 'suffix prompt to make full line
        strSignPrompt = strSignPrompt & "."
    Wend
    If Len(strSignPrompt) > 40 Then strSignPrompt = Left$(strSignPrompt, 40) 'if too large then truncate
    
    Call DoRecPrint(oPrinter, vbCrLf & strSignPrompt & vbCrLf)

End Sub

'Public Sub PrintVoided(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintVoided(ByRef oPrinter As Object)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call DoRecPrint(oPrinter, "         * * * V O I D E D * * *" & vbCrLf)
    Call PrintDivideLine(oPrinter)
    
    Call PrintHeader(oPrinter)

End Sub


'Public Sub PrintDepositRefunded(ByRef oPrinter As OPOSPOSPrinter, _
'                                ByVal strCancelReason As String, _
'                                ByVal dblDepositAmount As Double)
Public Sub PrintDepositRefunded(ByRef oPrinter As Object, _
                                ByVal strCancelReason As String, _
                                ByVal dblDepositAmount As Double)
                                
Dim strDepAmount As String

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    strDepAmount = Format$(dblDepositAmount, "0.00")
    Call DoRecPrint(oPrinter, "Deposit Refunded" & strDepAmount & vbCrLf)
    Call DoRecPrint(oPrinter, "Reason for Cancellation :" & vbCrLf)
    Call DoRecPrint(oPrinter, strCancelReason & vbCrLf)
    Call PrintDivideLine(oPrinter)
    Call DoRecPrint(oPrinter, mstrCancelOTrailer & vbCrLf)
    'End till receipt by cutting paper, if available else feed extra lines
    If oPrinter.CapRecPapercut = True Then
        Call DoRecPrint(oPrinter, Chr$(27) + "|fP")
    Else
        Call DoRecPrint(oPrinter, Chr$(27) + "|" + CStr(oPrinter.RecLinesToPaperCut) + "lF")
    End If
    
    Call PrintHeader(oPrinter)

End Sub

'Public Sub PrintParked(ByRef oPrinter As OPOSPOSPrinter, _
'                       ByVal strReference As String)
Public Sub PrintParked(ByRef oPrinter As Object, _
                       ByVal strReference As String)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call DoRecPrint(oPrinter, "* P A R K E D   T R A N S A C T I O N *" & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, mstrRcptParked)
    
    Call PrintBarCode(oPrinter, strReference)

End Sub

'Function ParseData(ByVal strData As String) As String
Function ParseData(ByVal strData As String) As String

Dim lngCharPos   As Long
Dim strHexDigits As String
Dim strNewChar   As String

    'check if any \ appear in string
    While (InStr(1, strData, "\") > 0)
        'locate position
        lngCharPos = InStr(1, strData, "\")
        strHexDigits = Mid$(strData, lngCharPos + 1, 2)
        strNewChar = Chr$(Val("&H" + strHexDigits))
        'replace character with new value
        strData = Left$(strData, lngCharPos - 1) & strNewChar & Right$(strData, Len(strData) - lngCharPos - 2)
    Wend
    
    'Pass out updated data
    ParseData = strData

End Function

'Public Sub PrintBarCode(ByRef oPrinter As OPOSPOSPrinter, _
'                        ByVal strData As String)
Public Sub PrintBarCode(ByRef oPrinter As Object, _
                        ByVal strData As String)

Dim strBCData As String
Dim lngBCType As Long
    
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    lngBCType = PtrBcsCode39
    If lngBCType = PtrBcsCode128 Then
        strBCData = ParseData(strData)
    Else
        strBCData = strData
    End If
    mlngPrinterCode = oPrinter.PrintBarCode(FptrSReceipt, strBCData, lngBCType, 100, 162, PtrBcCenter, PtrBcTextBelow)
    Call DebugMsg("Receipt", "PrintBarCode", endlDebug, CStr(mlngPrinterCode))
    
'        Coptr1.PrintBarCode PTR_S_RECEIPT, BcData, PTR_BCS_UPCA, 1000, 4500, PTR_BC_LEFT, PTR_BC_TEXT_NONE 'PTR_BC_TEXT_BELOW
    
End Sub

'Public Sub PrintCustomerSign(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintCustomerSign(ByRef oPrinter As Object)

Dim strSignLine As String

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    strSignLine = Space$(20)
    Call DoRecPrint(oPrinter, "Cust Sign: " & strSignLine & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, "Auth 1   : " & strSignLine & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, "Auth 2   : " & strSignLine & vbCrLf & vbCrLf)

End Sub

'Public Sub ClosePrinter(ByRef oPrinter As OPOSPOSPrinter)
Public Sub ClosePrinter(ByRef oPrinter As Object)

    oPrinter.DeviceEnabled = False
    mlngPrinterCode = oPrinter.ReleaseDevice
    Call oPrinter.Close

End Sub

'Public Sub PrintCreditCardSlip(ByRef oPrinter As OPOSPOSPrinter, _
'                               ByVal blnSignature As Boolean, _
'                               ByVal strAuthCode As String, _
'                               ByVal strCardType As String, _
'                               ByVal strCardNumber As String, _
'                               ByVal strExpiryDate As String, _
'                               ByVal dblAmount As Double, _
'                               ByVal blnCNPTill As Boolean)
Public Sub PrintCreditCardSlip(ByRef oPrinter As Object, _
                               ByVal blnSignature As Boolean, _
                               ByVal strAuthCode As String, _
                               ByVal strCardType As String, _
                               ByVal strCardNumber As String, _
                               ByVal strExpiryDate As String, _
                               ByVal dblAmount As Double, _
                               ByVal blnCNPTill As Boolean)
                               
Dim strAmount       As String
Dim strExempt       As String
Dim lngSaveSpacing  As Long
                               
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    On Error GoTo Err_Print_CCSlip
    lngSaveSpacing = oPrinter.RecLineSpacing
    oPrinter.RecLineSpacing = oPrinter.RecLineHeight
    
    Call DoRecPrint(oPrinter, "Please Debit\Credit My Account" & vbCrLf)
    Call PrintDivideLine(oPrinter)
    'call DoRecPrint(oPrinter, dblAmount & vbCrLf)
    strAmount = Format$(dblAmount, Chr$(156) & "0.00")
    strAmount = Space$(15 - Len(strAmount)) & strAmount
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD & strAmount & vbCrLf)
        
    Call PrintDivideLine(oPrinter)
    If blnSignature = True Then
        If (blnCNPTill = True) Then
            Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD & "CUSTOMER NOT PRESENT" & vbCrLf)
        Else
            Call DoRecPrint(oPrinter, "Cardholder's Signature" & vbCrLf & vbCrLf & vbCrLf)
        End If
    Else
        Call DoRecPrint(oPrinter, "Please Retain for your Record" & vbCrLf & "This voucher is not a VAT Receipt" & vbCrLf)
    End If
    Call PrintDivideLine(oPrinter)
'    strExempt = "2.5% of this is paid by me to Wickes" & vbCrLf & _
            "Retail Services Ltd for handling this" & vbCrLf & _
            "transaction for me.  The total amount" & vbCrLf & _
            "paid is the same however I pay." & vbNewLine
'    Call DoRecPrint(oPrinter, strExempt)
    
CutPaper:
    'End till receipt by cutting paper,
    'if available else feed extra lines
    Call DoRecPrint(oPrinter, vbNewLine)
    Call DoRecPrint(oPrinter, vbNewLine)
    Call DoRecPrint(oPrinter, vbNewLine)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_LOGO)
    If oPrinter.CapRecPapercut = True Then 'MUST ADD BACK IN
        Call oPrinter.PrintImmediate(FptrSReceipt, Chr$(27) & Chr$(105))
    Else
        Call DoRecPrint(oPrinter, Chr$(27) + "|" + CStr(oPrinter.RecLinesToPaperCut) + "lF")
    End If
    oPrinter.RecLineSpacing = lngSaveSpacing
    Exit Sub
                               
Err_Print_CCSlip:

Dim strStage As String

    strStage = blnSignature & "," & strAuthCode & "," & strCardType & "," & strCardNumber & "," & strExpiryDate & "," & dblAmount

    Call MsgBoxEx("WARNING : An error has occurred when printing Credit Card Slip - system will attempt to continue to complete transaction", vbOKOnly, "Print Error")
    Call Err.Report(MODULE_NAME, "PrintCreditCardSlip-" & strStage, 1, False)
    Call Err.Clear

End Sub

'Public Sub PrintRefundSlip(ByRef oPrinter As OPOSPOSPrinter, _
'                           ByVal blnPrintCountersign As Boolean, _
'                           ByVal strStoreName As String, _
'                           ByVal strPaidWith As String, _
'                           ByVal dteSoldDate As Date, _
'                           ByVal strTranNo As String, _
'                           ByVal strReturnReason As String, _
'                           ByVal strUserName As String)
Public Sub PrintRefundSlip(ByRef oPrinter As Object, _
                           ByVal blnPrintCountersign As Boolean, _
                           ByVal strStoreName As String, _
                           ByVal strPaidWith As String, _
                           ByVal dteSoldDate As Date, _
                           ByVal strTranNo As String, _
                           ByVal strReturnReason As String, _
                           ByVal strUserName As String)
                               
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_UNDERLINE & "         STOCK REFUND AGREEMENT        " & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_COMPRESSED & "In respect of the goods refunded as above" & vbCrLf & "which were originally purchased from" & vbCrLf)
    Call DoRecPrint(oPrinter, strStoreName & " store on " & Format$(Date, "DD/MM/YY") & " on " & vbCrLf)
    Call DoRecPrint(oPrinter, "transaction no." & strTranNo & " and paid for by " & strPaidWith & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_NORMAL & "Reason for return :" & vbCrLf & strReturnReason & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_UNDERLINE & "          Customer Declaration          " & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_COMPRESSED & "I am returning these unused goods in an" & vbCrLf & "as purchased condition and have hereby requested" & vbCrLf)
    Call DoRecPrint(oPrinter, "a refund.  The Company's procedures with regard" & vbCrLf & "to repayment have been explained to me. I" & vbCrLf)
    Call DoRecPrint(oPrinter, "understand this refund is at the discretion" & vbCrLf & "of the company and this does not effect my" & vbCrLf)
    Call DoRecPrint(oPrinter, "Statutory rights." & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, "Signed..................................(Customer)" & vbCrLf & Chr$(27) & ESC_PRINT_NORMAL & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_UNDERLINE & "           Shop Declaration             " & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_COMPRESSED & "I have received the above goods in an as" & vbCrLf & "purchased condition and refunded the" & vbCrLf)
    Call DoRecPrint(oPrinter, "amount due as set out." & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, "Signed...................................(Cashier)" & vbCrLf & "       " & strUserName & vbCrLf & vbCrLf)
    If blnPrintCountersign = True Then
        Call DoRecPrint(oPrinter, "Countersigned....................................." & vbCrLf & vbCrLf)
        Call DoRecPrint(oPrinter, "Print name........................................" & vbCrLf & vbCrLf)
    End If
    Call DoRecPrint(oPrinter, "If the Refund is being made by way of a " & vbCrLf & "Cheque from Head Office. Please allow" & vbCrLf)
    Call DoRecPrint(oPrinter, "10 days for it to arrive." & vbCrLf & Chr$(27) & ESC_PRINT_NORMAL)
    Call PrintDivideLine(oPrinter)
                               
End Sub

'Public Sub PrintCreditCardDetails(ByRef oPrinter As OPOSPOSPrinter, _
'                                    ByVal strCardType As String, _
'                                    ByVal strCardNumber As String, _
'                                    ByVal strStartDate As String, _
'                                    ByVal strExpiryDate As String, _
'                                    ByVal dblAmount As Double, _
'                                    ByVal strEntryMethod As String, _
'                                    ByVal strAuthNum As String, _
'                                    ByVal strIssueNum As String, _
'                                    ByVal blnCustCopy As Boolean, _
'                                    ByVal strTerminalVerificationResult As String, _
'                                    ByVal strTransactionStatusInfo As String, _
'                                    ByRef strAuthResponceCode As String, _
'                                    ByRef strUnpredictableNumber As String, _
'                                    ByRef strApplicationIdentifier As String, _
'                                    ByVal strTerminalID As String, _
'                                    ByVal strMerchantNo As String, _
'                                    ByVal strCryptogram As String, _
'                                    ByVal strChequeType As String, ByVal strIACDefault As String, ByVal strIACDenial As String, _
'                                    ByVal strIACOnline As String, ByVal strEmvCvmResults As String)
Public Sub PrintCreditCardDetails(ByRef oPrinter As Object, _
                                    ByVal strCardType As String, _
                                    ByVal strCardNumber As String, _
                                    ByVal strStartDate As String, _
                                    ByVal strExpiryDate As String, _
                                    ByVal dblAmount As Double, _
                                    ByVal strEntryMethod As String, _
                                    ByVal strAuthNum As String, _
                                    ByVal strIssueNum As String, _
                                    ByVal blnCustCopy As Boolean, _
                                    ByVal strTerminalVerificationResult As String, _
                                    ByVal strTransactionStatusInfo As String, _
                                    ByRef strAuthResponceCode As String, _
                                    ByRef strUnpredictableNumber As String, _
                                    ByRef strApplicationIdentifier As String, _
                                    ByVal strTerminalID As String, _
                                    ByVal strMerchantNo As String, _
                                    ByVal strCryptogram As String, _
                                    ByVal strChequeType As String, ByVal strIACDefault As String, ByVal strIACDenial As String, _
                                    ByVal strIACOnline As String, ByVal strEmvCvmResults As String)
                                    
                                    
Dim strPrintStr     As String
Dim strPrintCardNum As String
Dim strReceiptCopy  As String
Dim strStage        As String

    On Error GoTo Err_Print_CCDetails
    
    'Determine the card verification method
    strStage = "Verify Method"
    If (Val(strIssueNum) = 0) Then strIssueNum = "  "
    
    Call DebugMsg("PrintCreditCardDetails", vbNullString, endlDebug, blnCustCopy)
                              
    If LenB(strExpiryDate) = 0 Then
        strExpiryDate = "0000"
    End If
    
    If LenB(strStartDate) = 0 Then
        strStartDate = "0000"
    End If
    
    strStage = "CustCopy=" & blnCustCopy
    If blnCustCopy = True Then

        strPrintStr = CUSTOMER_RECEIPT
        strStage = "Header-" & strChequeType
        strPrintStr = Space$((22 - Len(strPrintStr)) / 2) & strPrintStr
        strPrintStr = strPrintStr & Space$(22 - Len(strPrintStr))
        Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD & strPrintStr & vbCrLf)
        Call PrintDivideLine(oPrinter)
        Select Case strChequeType
            Case "Company"
                strCardType = Left$(strCardType, 26)
                strPrintStr = strCardType & Space$(26 - Len(strCardType)) & "["
                strPrintStr = strPrintStr & strChequeType & " " & strEntryMethod & "]" & Space$(11 - Len(strEntryMethod)) & vbCrLf
            Case "Personal"
                strCardType = Left$(strCardType, 28)
                strPrintStr = strCardType & Space$(28 - Len(strCardType)) & "["
                strPrintStr = strPrintStr & strEntryMethod & "]" & Space$(11 - Len(strEntryMethod)) & vbCrLf
                Call DoRecPrint(oPrinter, strPrintStr)
                strPrintCardNum = Replace(Space$(Len(strCardNumber) - 4), " ", "*")
                strPrintCardNum = strPrintCardNum & Right$(strCardNumber, 4)
                strPrintStr = strPrintCardNum & Space$(26 - Len(strPrintCardNum)) & " " & strIssueNum & " " & Left$(strStartDate, 2) & _
                    "/" & Right$(strStartDate, 2) & " " & Left$(strExpiryDate, 2) & _
                    "/" & Right$(strExpiryDate, 2) & vbCrLf
            Case Else
                strCardType = Left$(strCardType, 25)
                strPrintStr = Space$((40 - Len(strPrintStr)) / 2) & _
                    "[" & strEntryMethod & "]" & vbCrLf
                Call DoRecPrint(oPrinter, strPrintStr)

                strPrintStr = strCardType & Space$(25 - Len(strCardType))
                strPrintStr = strPrintStr & vbCrLf
                Call DoRecPrint(oPrinter, strPrintStr)
                strPrintCardNum = Replace(Space$(Len(strCardNumber) - 4), " ", "*")
                strPrintCardNum = strPrintCardNum & Right$(strCardNumber, 4)
                strPrintStr = strPrintCardNum & Space$(26 - Len(strPrintCardNum)) & " " & strIssueNum & " " & Left$(strStartDate, 2) & _
                    "/" & Right$(strStartDate, 2) & " " & Left$(strExpiryDate, 2) & _
                    "/" & Right$(strExpiryDate, 2) & vbCrLf
        End Select
        strStage = "Auth-" & strChequeType
        If strEntryMethod <> "Cheque" Then
            strPrintStr = strPrintStr & " Auth Num: " & strAuthNum & vbCrLf & vbCrLf
        End If
    Else
        strStage = "Auth(STORE)-" & strEntryMethod
        If ((Left$(strEntryMethod, 3) = "ICC") Or (Left$(UCase(strEntryMethod), 5) = "KEYED")) And (mblnPRM_SHOW_EFT_DATA = True) Then
            strPrintStr = "Term Veri Result: " & strTerminalVerificationResult
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "Trans Status Info: " & strTransactionStatusInfo
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "Auth Response Code: " & strAuthResponceCode
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "Unpredicatable No: " & strUnpredictableNumber
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "Application ID: " & strApplicationIdentifier
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "MerchantNo: " & strMerchantNo
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "TerminalID: " & strTerminalID
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            'strPrintStr = "Cryptogram: " & strCryptogram
            'Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            strPrintStr = "CVM Results: " & strEmvCvmResults
            Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            If LenB(strIACDefault) <> 0 Then
                strPrintStr = "IACDefault: " & strIACDefault
                Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            End If
            If LenB(strIACDenial) <> 0 Then
                strPrintStr = "IACDenial: " & strIACDenial
                Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            End If
            If LenB(strIACOnline) <> 0 Then
                strPrintStr = "IACOnline: " & strIACOnline
                Call DoRecPrint(oPrinter, strPrintStr & vbCrLf)
            End If
            Call PrintDivideLine(oPrinter)
        End If
        strStage = "Auth-" & strChequeType
        strPrintStr = STORE_RECEIPT
        strPrintStr = Space$((22 - Len(strPrintStr)) / 2) & strPrintStr
        strPrintStr = strPrintStr & Space$(22 - Len(strPrintStr))
        Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BIG & Chr$(27) & ESC_PRINT_BOLD & strPrintStr & vbCrLf)
        Call PrintDivideLine(oPrinter)
        
        strCardType = Left$(strCardType, 25)
        
        strPrintStr = Space$((40 - Len(strPrintStr)) / 2) & _
            "[" & strEntryMethod & "]" & vbCrLf
        Call DoRecPrint(oPrinter, strPrintStr)
        
        strPrintStr = strCardType & Space$(25 - Len(strCardType))
        strPrintStr = strPrintStr & vbCrLf
        Call DoRecPrint(oPrinter, strPrintStr)

        strPrintStr = strCardNumber & Space$(26 - Len(strCardNumber)) & " " & strIssueNum & " " & Left$(strStartDate, 2) & _
            "/" & Right$(strStartDate, 2) & " " & Left$(strExpiryDate, 2) & _
            "/" & Right$(strExpiryDate, 2) & vbCrLf
        If strEntryMethod <> "Cheque" Then
            strPrintStr = strPrintStr & " Auth Num: " & strAuthNum & vbCrLf & vbCrLf
        End If
    End If
    strStage = "Finishing"
    Call DoRecPrint(oPrinter, strPrintStr)
    Exit Sub
                               
Err_Print_CCDetails:

    Call MsgBoxEx("WARNING : An error has occurred when printing Credit Card Details - system will attempt to continue to complete transaction", vbOKOnly, "Print Error")
    Call Err.Report(MODULE_NAME, "PrintCreditCardDetails-" & strStage, 1, False)
    Call Err.Clear

End Sub 'PrintCreditCardDetails

'Public Sub PrintPaidInReasons(ByRef oPrinter As OPOSPOSPrinter, strPaymentDesc, dblAmount, strComment)
Public Sub PrintPaidInReasons(ByRef oPrinter As Object, strPaymentDesc, dblAmount, strComment)

Dim strAmount As String

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    strAmount = Format$(dblAmount, "0.00")
    strAmount = Space$(40 - (Len(strAmount) + Len(strPaymentDesc)))
    Call DoRecPrint(oPrinter, strPaymentDesc & strAmount & vbCrLf)
    Call DoRecPrint(oPrinter, "Reason : " & vbCrLf & strComment & vbCrLf)

End Sub

'Public Sub PrintTransDiscountHeader(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintTransDiscountHeader(ByRef oPrinter As Object)

    Call PrintDivideLine(oPrinter)
    Call DoRecPrint(oPrinter, "Discount Summary" & vbCrLf)
    
End Sub 'PrintTransDiscountSummary

'Public Sub PrintTransDiscountSummary(ByRef oPrinter As OPOSPOSPrinter, _
'                                ByRef dblTransDiscountAmount As Double, _
'                                ByRef strTransDiscountDesc As String)
Public Sub PrintTransDiscountSummary(ByRef oPrinter As Object, _
                                ByRef dblTransDiscountAmount As Double, _
                                ByRef strTransDiscountDesc As String)

Dim strAmount    As String
Dim strDiscDesc  As String
Dim strDescSplit As String
Dim strPrint     As String
Dim lngPos       As Long

    strAmount = Format$(Abs(dblTransDiscountAmount), "0.00")
    strAmount = Space$(10 - Len(strAmount)) & strAmount
    
    strDiscDesc = Trim$(strTransDiscountDesc)
    'split offer and promotion name to 2 lines if over 20 chars long
    If Len(strDiscDesc) > 20 Then
        strDescSplit = Left$(strDiscDesc, 20)
        lngPos = Len(strDescSplit)
        Do While (lngPos > 0)
            Select Case (Mid$(strDescSplit, lngPos, 1))
                Case " ", ".", ",", "/", "\": strDescSplit = Left$(strDescSplit, lngPos - 1)
                            Exit Do
                Case Else:  lngPos = lngPos - 1
            End Select
        Loop
        strTransDiscountDesc = strDescSplit & vbCrLf & "  " & Left$(LTrim$(Mid$(strTransDiscountDesc, lngPos)) & Space$(22), 22)
    Else
        strTransDiscountDesc = Left$(strTransDiscountDesc & Space$(22), 22)
    End If
    Call DoRecPrint(oPrinter, "**" & strTransDiscountDesc & "( " & strAmount & "-) " & vbCrLf)
    
End Sub 'PrintTransDiscountSummary

'Public Sub PrintTotalDiscounts(ByRef oPrinter As OPOSPOSPrinter, _
'                                ByRef dblTransDiscountAmount As Double)
Public Sub PrintTotalDiscounts(ByRef oPrinter As Object, _
                                ByRef dblTransDiscountAmount As Double)

Dim strAmount As String
Dim strPrint As String

    Call PrintDivideLine(oPrinter)
    strAmount = Format$(dblTransDiscountAmount, "0.00") & "-"
    strPrint = Space$(37 - (Len(strAmount) + Len("Total Discounts")))
    Call DoRecPrint(oPrinter, "Total Discounts" & strPrint & strAmount & vbCrLf)
    
End Sub 'PrintTotalDiscounts

'Public Sub PrintTotalBeforeDiscounts(ByRef oPrinter As OPOSPOSPrinter, _
'                                ByRef dblTransAmount As Double)
Public Sub PrintTotalBeforeDiscounts(ByRef oPrinter As Object, _
                                ByRef dblTransAmount As Double)

Dim strAmount As String
Dim strPrint As String

    Call PrintDivideLine(oPrinter)
    strAmount = Format$(dblTransAmount, "0.00") & "-"
    strPrint = Space$(37 - (Len(strAmount) + Len("Total Before Discounts")))
    Call DoRecPrint(oPrinter, "Total Before Discounts" & strPrint & strAmount & vbCrLf)
    
End Sub 'PrintTotalBeforeDiscounts

'Public Sub PrintAccountDepartmentAddress(ByRef oPrinter As OPOSPOSPrinter, _
'                                        ByVal strHeadofficename As String, _
'                                        ByVal strHeadOfficeAddress As String, _
'                                        ByVal strHeadOfficePhoneNo As String, _
'                                        ByVal strHeadOfficeFaxNumber As String, _
'                                        ByVal strVATNumber As String)
Public Sub PrintAccountDepartmentAddress(ByRef oPrinter As Object, _
                                        ByVal strHeadofficename As String, _
                                        ByVal strHeadOfficeAddress As String, _
                                        ByVal strHeadOfficePhoneNo As String, _
                                        ByVal strHeadOfficeFaxNumber As String, _
                                        ByVal strVATNumber As String)
Dim strPrint As String
    
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
       
   'strTranType = Space$((40 - Len(strTranType)) / 2) & strTranType
    
    strPrint = "Accounts Department"
    strPrint = Space$((40 - Len(strPrint)) / 2) & strPrint
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BOLD & strPrint & vbCrLf)
    strPrint = Space$((40 - Len(strHeadofficename)) / 2) & strHeadofficename
    Call DoRecPrint(oPrinter, strPrint & vbCrLf)
    Call DoRecPrint(oPrinter, strHeadOfficeAddress & vbCrLf)
    strPrint = "Phone Number: " & strHeadOfficePhoneNo
    strPrint = Space$((40 - Len(strPrint)) / 2) & strPrint
    Call DoRecPrint(oPrinter, strPrint & vbCrLf)
    strPrint = "Fax Number: " & strHeadOfficeFaxNumber
    strPrint = Space$((40 - Len(strPrint)) / 2) & strPrint
    Call DoRecPrint(oPrinter, strPrint & vbCrLf)
    strPrint = "VAT Number: " & strVATNumber
    strPrint = Space$((40 - Len(strPrint)) / 2) & strPrint
    Call DoRecPrint(oPrinter, strPrint & vbCrLf & vbCrLf)
    
    Call PrintDivideLine(oPrinter)
    
        
End Sub 'PrintAccountDepartmentAddress

'Public Sub PrintInvoiceHeader(ByRef oPrinter As OPOSPOSPrinter, _
'                            ByVal strAccountNum As String, _
'                            ByVal strAccountName As String, _
'                            ByVal strActive As String, _
'                            ByVal strCardExpiry As String, _
'                            ByVal strAddressLine1 As String, _
'                            ByVal strAddressLine2 As String, _
'                            ByVal strAddressLine3 As String, _
'                            ByVal strPostCode As String, _
'                            ByVal strHomeTelNumber As String, _
'                            ByVal strInvoiceNumber As String, _
'                            ByVal blnCustomerCopy As Boolean)
Public Sub PrintInvoiceHeader(ByRef oPrinter As Object, _
                            ByVal strAccountNum As String, _
                            ByVal strAccountName As String, _
                            ByVal strActive As String, _
                            ByVal strCardExpiry As String, _
                            ByVal strAddressLine1 As String, _
                            ByVal strAddressLine2 As String, _
                            ByVal strAddressLine3 As String, _
                            ByVal strPostCode As String, _
                            ByVal strHomeTelNumber As String, _
                            ByVal strInvoiceNumber As String, _
                            ByVal blnCustomerCopy As Boolean)

                                        
Dim strPrint As String
    
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    mblnCustomerCopy = blnCustomerCopy
    If blnCustomerCopy = False Then
        strPrint = "INVOICE - STORE COPY"
    Else
        strPrint = "INVOICE - CUSTOMER COPY"
    End If
    strPrint = Space$((40 - Len(strPrint)) / 2) & strPrint
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BOLD & strPrint & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_UNDERLINE & "Invoice To" & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, strAccountName & vbCrLf)
    Call DoRecPrint(oPrinter, strAddressLine1 & vbCrLf)
    Call DoRecPrint(oPrinter, strAddressLine2 & vbCrLf)
    Call DoRecPrint(oPrinter, strAddressLine3 & vbCrLf)
    Call DoRecPrint(oPrinter, strPostCode & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, "Invoice Number: " & vbTab & strInvoiceNumber & vbCrLf)
    Call DoRecPrint(oPrinter, "Account Number: " & vbTab & strAccountNum & vbCrLf)
    
    Call PrintDivideLine(oPrinter)
        
End Sub 'PrintInvoiceHeader

'Public Sub PrintInvoiceTrailer1(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintInvoiceTrailer1(ByRef oPrinter As Object)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    If mblnCustomerCopy = True Then
    
        If LenB(mstrDivLine) = 0 Then
            mstrDivLine = String$(40, Chr$(205))
        End If
        Call DoRecPrint(oPrinter, mstrDivLine & vbCrLf)
    
    End If
    
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_COMPRESSED & mstrInvTrailer1)
    

End Sub 'PrintInvoiceTrailer1

'Public Sub PrintInvoiceTrailer2(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintInvoiceTrailer2(ByRef oPrinter As Object)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call DoRecPrint(oPrinter, mstrInvTrailer2)


End Sub 'PrintInvoiceTrailer2

'Public Sub PrintInvoiceTrailer3(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintInvoiceTrailer3(ByRef oPrinter As Object)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call DoRecPrint(oPrinter, mstrInvTrailer3)


End Sub 'PrintInvoiceTrailer3

'Public Sub PrintCreditNoteHeader(ByRef oPrinter As OPOSPOSPrinter, _
'                                ByVal strAccountNum As String, _
'                                ByVal strAccountName As String, _
'                                ByVal strActive As String, _
'                                ByVal strCardExpiry As String, _
'                                ByVal strAddressLine1 As String, _
'                                ByVal strAddressLine2 As String, _
'                                ByVal strAddressLine3 As String, _
'                                ByVal strPostCode As String, _
'                                ByVal strHomeTelNumber As String, _
'                                ByVal strInvoiceNumber As String, _
'                                ByVal blnCustomerCopy As Boolean)
Public Sub PrintCreditNoteHeader(ByRef oPrinter As Object, _
                                ByVal strAccountNum As String, _
                                ByVal strAccountName As String, _
                                ByVal strActive As String, _
                                ByVal strCardExpiry As String, _
                                ByVal strAddressLine1 As String, _
                                ByVal strAddressLine2 As String, _
                                ByVal strAddressLine3 As String, _
                                ByVal strPostCode As String, _
                                ByVal strHomeTelNumber As String, _
                                ByVal strInvoiceNumber As String, _
                                ByVal blnCustomerCopy As Boolean)

                                        
Dim strPrint As String
    
    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    mblnCustomerCopy = blnCustomerCopy
    If blnCustomerCopy = False Then
        strPrint = "CREDIT NOTE - STORE COPY"
    Else
        strPrint = "CREDIT NOTE - CUSTOMER COPY"
    End If
    strPrint = Space$((40 - Len(strPrint)) / 2) & strPrint
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_BOLD & strPrint & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_UNDERLINE & "Invoice To" & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, strAccountName & vbCrLf)
    Call DoRecPrint(oPrinter, strAddressLine1 & vbCrLf)
    Call DoRecPrint(oPrinter, strAddressLine2 & vbCrLf)
    Call DoRecPrint(oPrinter, strAddressLine3 & vbCrLf)
    Call DoRecPrint(oPrinter, strPostCode & vbCrLf & vbCrLf)
    Call DoRecPrint(oPrinter, "Credit Note Number: " & vbTab & strInvoiceNumber & vbCrLf)
    Call DoRecPrint(oPrinter, "Account Number: " & vbTab & strAccountNum & vbCrLf)
    
    Call PrintDivideLine(oPrinter)
        
End Sub 'PrintInvoiceHeader

'Public Sub PrintInvoiceTrailer1Refund(ByRef oPrinter As OPOSPOSPrinter)
Public Sub PrintInvoiceTrailer1Refund(ByRef oPrinter As Object)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    If mblnCustomerCopy = True Then
    
        If LenB(mstrDivLine) = 0 Then
            mstrDivLine = String$(40, Chr$(205))
        End If
        Call DoRecPrint(oPrinter, mstrDivLine & vbCrLf)
    
    End If
    
    Call DoRecPrint(oPrinter, Chr$(27) & ESC_PRINT_COMPRESSED & mstrInvTrailer1Refund)
    

End Sub 'PrintInvoiceTrailer1Refund

'Public Sub PrintTeleOrderNum(ByRef oPrinter As OPOSPOSPrinter, _
'                             ByRef strTeleOrderNum As String)
Public Sub PrintTeleOrderNum(ByRef oPrinter As Object, _
                             ByRef strTeleOrderNum As String)

    'If printing switched off then do not print, just exit
    If mblnIgnorePrint = True Then Exit Sub
    
    Call DoRecPrint(oPrinter, "Tele Order Number: " & vbTab & strTeleOrderNum & vbCrLf)
    Call PrintDivideLine(oPrinter)

End Sub 'PrintTeleOrderNum

'Private Sub DoRecPrint(ByVal oPrinter As OPOSPOSPrinter, strOutput As String)
Private Sub DoRecPrint(ByVal oPrinter As Object, strOutput As String)
    
Dim lngResultCode As Long
Dim lngExtended   As Long
Dim blnOK         As Boolean
'Exit Sub
    Do
        lngResultCode = oPrinter.PrintNormal(FptrSReceipt, strOutput)
    
        If lngResultCode <> 0 Then
            If oPrinter.RecEmpty Then
                Call MsgBoxEx("Printer out of paper" & vbNewLine & "Re-load and press [OK]", vbExclamation, "Printer Paper Out", , , , , RGBMsgBox_WarnColour)
            Else
                Call MsgBoxEx("Re-initialise printer and press [OK]" & lngResultCode, vbExclamation, "Printer Output Error", , , , , RGBMsgBox_WarnColour)
                oPrinter.DeviceEnabled = False
                Call oPrinter.ReleaseDevice
                Call oPrinter.ClaimDevice(10)
                oPrinter.DeviceEnabled = True
                lngResultCode = oPrinter.ResultCode
            End If
        End If
    Loop While lngResultCode <> 0
    DoEvents
    
End Sub
