VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmPrintSlip 
   Caption         =   "Read Card"
   ClientHeight    =   10020
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12015
   LinkTopic       =   "Form1"
   ScaleHeight     =   10020
   ScaleWidth      =   12015
   StartUpPosition =   2  'CenterScreen
   Begin FPSpreadADO.fpSpread sprdSlip 
      Height          =   9795
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   11595
      _Version        =   458752
      _ExtentX        =   20452
      _ExtentY        =   17277
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   10
      MaxRows         =   32
      SpreadDesigner  =   "frmPrintSlip.frx":0000
   End
End
Attribute VB_Name = "frmPrintSlip"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmReadCard"


Private Sub Form_Activate()
    
    Call DebugMsg(MODULE_NAME, "Form_Activate", endlTraceIn)
    sprdSlip.PrintRowHeaders = False
    sprdSlip.PrintColHeaders = False
    sprdSlip.PrintGrid = False
    sprdSlip.PrintSheet
    Me.Hide

End Sub

Private Sub Form_Load()

    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug)
    Me.Visible = False
    
End Sub 'Form_Load
Public Sub PrintSlip(IsSale As Boolean, CustomerPresent As Boolean, MerchantNumber As String, _
                TerminalID As String, TransactionNo As String, _
                CardName As String, CardReadMethod As String, TranDate As String, TranTime As String, _
                CardNumber As String, IssueNo As String, StartDate As String, ExpiryDate As String, _
                ApplicationID As String, AuthorisationCode As String, TotalAmount As Currency, _
                VerifyMessage As String, Fallbacks As String, OnlineAuthFailed As String, _
                 ICCDebugInfo As String, SignatureSlip As Boolean, StoreCopy As Boolean)
                
    sprdSlip.Col = 2
    sprdSlip.Row = 2
    sprdSlip.CellType = CellTypePicture
    sprdSlip.TypePictCenter = True
    sprdSlip.TypePictPicture = LoadPicture("c:\eftlogo.bmp")
    sprdSlip.Col = 8
    sprdSlip.TypeEditMultiLine = True
    sprdSlip.TypeMaxEditLen = 2000
    sprdSlip.Text = sprdSlip.Text & vbNewLine & "      " & mstrHeadOffice
    sprdSlip.Col = 5
    sprdSlip.Row = 2
    sprdSlip.Text = IIf(SignatureSlip, "Store Copy", "Customer Copy")
    sprdSlip.Row = 5
    If (IsSale = True) Then
        sprdSlip.Text = "Sale"
    Else
        sprdSlip.Text = "Refund"
    End If
    sprdSlip.Row = 6
    If (CustomerPresent = False) Then
        sprdSlip.Text = "Cardholder Not Present"
    Else
        sprdSlip.Text = ""
    End If
    
    sprdSlip.Row = 9
    sprdSlip.Col = 2
    sprdSlip.Text = sprdSlip.Text & MerchantNumber
    sprdSlip.Col = 3
    sprdSlip.Text = mstrStoreName
    sprdSlip.Col = 6
    sprdSlip.Text = sprdSlip.Text & TerminalID
    sprdSlip.Col = 8
    sprdSlip.Text = sprdSlip.Text & TransactionNo
    
    sprdSlip.Row = 11
    sprdSlip.Col = 2
    sprdSlip.Text = CardName
    sprdSlip.Col = 3
    sprdSlip.Text = CardReadMethod
    If (TranDate = "") Then TranDate = Format(Now(), "DDMMYY")
    sprdSlip.Col = 6
    sprdSlip.Text = Left$(TranDate, 2) & "/" & Mid$(TranDate, 3, 2) & "/" & Mid$(TranDate, 5, 2)
    sprdSlip.Col = 9
    If (TranTime = "") Then TranTime = Format(Now(), "HHNNSS")
    sprdSlip.Text = TranTime
    
    sprdSlip.Row = 13
    sprdSlip.Col = 3
    sprdSlip.Text = IIf(StoreCopy, CardNumber, MaskCreditCardNo(CardNumber))
    sprdSlip.Col = 9
    sprdSlip.Text = IssueNo
    
    sprdSlip.Row = 14
    sprdSlip.Col = 3
    If (Len(StartDate) = 4) Then
        sprdSlip.Text = Left$(StartDate, 2) & "/" & Mid$(StartDate, 3)
    Else
        sprdSlip.Text = Mid$(StartDate, 3, 2) & "/" & Mid$(StartDate, 5, 2)
    End If
        
    sprdSlip.Col = 6
    sprdSlip.Text = ExpiryDate
    If (Len(StartDate) = 4) Then
        sprdSlip.Text = Left$(ExpiryDate, 2) & "/" & Mid$(ExpiryDate, 3)
    Else
        sprdSlip.Text = Mid$(ExpiryDate, 3, 2) & "/" & Mid$(ExpiryDate, 5, 2)
    End If
    
    sprdSlip.Col = 9
    sprdSlip.Text = ApplicationID
    
    sprdSlip.Row = 16
    sprdSlip.Col = 3
    sprdSlip.Text = AuthorisationCode
    sprdSlip.Col = 6
    sprdSlip.Text = sprdSlip.Text & mCurrChar & Format(TotalAmount, "0.00")
    
    Dim reversed As Boolean
    sprdSlip.Row = 18
    sprdSlip.Col = 3
    If (SignatureSlip = True) Then
        If ((IsSale = True) And (reversed = False)) Or ((IsSale = False) And (reversed = True)) Then
            sprdSlip.Text = "Please debit my account with the amount specified"
        Else
            sprdSlip.Text = "Please credit my account with the amount specified"
        End If
    Else
        sprdSlip.Row = 18
        sprdSlip.RowHidden = True
        sprdSlip.Row = 19
        sprdSlip.RowHidden = True
    End If
    
    sprdSlip.Row = 20
    sprdSlip.Col = 3
    If (StoreCopy = True) Then
        sprdSlip.Text = IIf(CustomerPresent, "Cardholder's Signature  X ............................................................", "Cardholder Not Present")
    Else
        sprdSlip.Text = IIf(CustomerPresent, "", "Cardholder Not Present")
        If (SignatureSlip = True) Then sprdSlip.Text = "Cardholder Signature verified."
            'if {Tender.PIN Bypassed} = true then
            '    msg :=  msg + "(PIN Bypass)";
            'if {Tender.MSR Fallback} = true then
            '    msg :=  msg + "(ICC Fall-back)";
            'if {Tender.MSR Fallback} = true then
            '    msg :=  msg + "(Technical Fall-back)";
    End If
    sprdSlip.Row = 22
    sprdSlip.Col = 3
    sprdSlip.Text = Fallbacks
    sprdSlip.Row = 26
    sprdSlip.RowHidden = True
    sprdSlip.Row = 27
    sprdSlip.RowHidden = True
'    sprdSlip.TypeEditMultiLine = True
'    If ((IsSale = True) And (reversed = False)) Or ((IsSale = False) And (reversed = True)) Then
'        sprdSlip.Text = "2.5% of this is paid by me to Wickes Retail Services Ltd for handling this transaction for me." & vbNewLine & "The total amount paid is the same however I pay."
''    Else
'        sprdSlip.Text = "The amount above includes a refund of 2.5% card handling charge made by Wickes Retail Services."
'    End If
'    sprdSlip.RowHeight(26) = sprdSlip.MaxTextCellHeight
    
    If (ICCDebugInfo <> "") Then
        sprdSlip.Col = 3
        sprdSlip.Row = 30
        sprdSlip.Text = "ICC Debug Information"
        sprdSlip.Row = 31
        sprdSlip.Text = ICCDebugInfo
        sprdSlip.RowHeight(31) = sprdSlip.MaxTextRowHeight(31)
    End If

    If (StoreCopy = False) Then
        sprdSlip.Row = 32
        sprdSlip.Text = "PLEASE RETAIN THIS COPY FOR YOUR RECORDS"
    End If

    Call DebugMsg(MODULE_NAME, "PrintSlip", endlDebug, "Ready to Show and Print")
    Me.Show vbModal
    Call DebugMsg(MODULE_NAME, "PrintSlip", endlDebug, "Printing Completed")


End Sub

