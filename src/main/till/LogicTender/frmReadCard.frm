VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmReadCard 
   Caption         =   "Read Card"
   ClientHeight    =   6390
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9930
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   6390
   ScaleWidth      =   9930
   StartUpPosition =   2  'CenterScreen
   Begin FPSpreadADO.fpSpread sprdSlip 
      Height          =   5835
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9675
      _Version        =   458752
      _ExtentX        =   17066
      _ExtentY        =   10292
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GridShowHoriz   =   0   'False
      GridShowVert    =   0   'False
      MaxCols         =   10
      MaxRows         =   22
      SpreadDesigner  =   "frmReadCard.frx":0000
   End
End
Attribute VB_Name = "frmReadCard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmReadCard"


Private Sub Form_Load()

    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug)
    
End Sub 'Form_Load
Public Sub PrintSlip(IsSale As Boolean, CustomerPresent As Boolean, MerchantNumber As String, _
                StoreName As String, TerminalID As String, TransactionNo As String, _
                CardName As String, CardReadMethod As String, TranDate As String, TranTime As String, _
                CardNumber As String, IssueNo As String, Startdate As String, ExpiryDate As String, _
                AuthorisationCode As String, TotalAmount As Currency, DebitCreditClause As String, _
                VerifyMessage As String, Fallbacks As String, CCFeeMessage As String, OnlineAuthFailed As String, _
                ICCDebugHdr As String, ICCDebugInfo As String)


End Sub

