VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{8956B51A-CAE0-4FC0-988F-BA3E8FDE7565}#1.1#0"; "KeyPadUC.ocx"
Begin VB.Form frmTender 
   BackColor       =   &H80000009&
   Caption         =   "Record tender details"
   ClientHeight    =   7125
   ClientLeft      =   2715
   ClientTop       =   1275
   ClientWidth     =   7395
   ControlBox      =   0   'False
   Icon            =   "frmTender.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   7395
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   45
      Top             =   6750
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmTender.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4577
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   635
            MinWidth        =   635
            Picture         =   "frmTender.frx":1644
            Key             =   "NumPad"
            Object.ToolTipText     =   "Show/Hide Num Pad"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "13:09"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin prjKeyPadUC.ucKeyPad ucKeyPad1 
      Height          =   4110
      Left            =   780
      Top             =   5820
      Visible         =   0   'False
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   7250
   End
   Begin VB.Frame fraEntry 
      Caption         =   "Frame1"
      Height          =   6255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7095
      Begin VB.PictureBox fraBorder 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   4455
         Left            =   0
         ScaleHeight     =   4425
         ScaleWidth      =   7065
         TabIndex        =   6
         Top             =   1200
         Width           =   7095
         Begin VB.PictureBox fraAuthNum 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   735
            Left            =   120
            ScaleHeight     =   735
            ScaleWidth      =   6855
            TabIndex        =   7
            Top             =   3600
            Width           =   6855
            Begin VB.TextBox txtAuthNum 
               BackColor       =   &H00C0FFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   6
               TabIndex        =   23
               Top             =   180
               Width           =   1815
            End
            Begin VB.Label lblAuthNum 
               BackStyle       =   0  'Transparent
               Caption         =   "Authorisation No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   240
               TabIndex        =   22
               Top             =   180
               Width           =   3015
            End
         End
         Begin VB.PictureBox fraChequeCard 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   3675
            Left            =   120
            ScaleHeight     =   3675
            ScaleWidth      =   6855
            TabIndex        =   24
            Top             =   -120
            Visible         =   0   'False
            Width           =   6855
            Begin VB.TextBox txtEndYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   15
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtEndMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   14
               Top             =   1680
               Width           =   735
            End
            Begin VB.TextBox txtStartYear 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   4320
               MaxLength       =   2
               TabIndex        =   12
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtCardNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   2280
               MaxLength       =   19
               TabIndex        =   9
               Top             =   240
               Width           =   4575
            End
            Begin VB.TextBox txtStartMonth 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   3240
               MaxLength       =   2
               TabIndex        =   11
               Top             =   960
               Width           =   735
            End
            Begin VB.TextBox txtIssueNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   17
               Top             =   2400
               Width           =   975
            End
            Begin VB.TextBox txtCustPresent 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   5760
               MaxLength       =   2
               TabIndex        =   19
               Text            =   "Y"
               Top             =   2400
               Width           =   495
            End
            Begin VB.TextBox txtCVV 
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   2280
               MaxLength       =   3
               TabIndex        =   21
               Top             =   3120
               Width           =   975
            End
            Begin VB.Label Label8 
               BackStyle       =   0  'Transparent
               Caption         =   "End Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   13
               Top             =   1680
               Width           =   2895
            End
            Begin VB.Label Label7 
               BackStyle       =   0  'Transparent
               Caption         =   "Card No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   8
               Top             =   240
               Width           =   1935
            End
            Begin VB.Label Label6 
               BackStyle       =   0  'Transparent
               Caption         =   "Start Month / Year"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   10
               Top             =   960
               Width           =   3015
            End
            Begin VB.Label Label5 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   16
               Top             =   2400
               Width           =   1935
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Cust Present"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   435
               Left            =   3540
               TabIndex        =   18
               Top             =   2400
               Width           =   2055
            End
            Begin VB.Label Label9 
               BackStyle       =   0  'Transparent
               Caption         =   "CVV"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   240
               TabIndex        =   20
               Top             =   3120
               Width           =   1935
            End
         End
      End
      Begin VB.PictureBox fraChequeType 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   0
         ScaleHeight     =   1215
         ScaleWidth      =   7095
         TabIndex        =   1
         Top             =   660
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdNoTransaxChq 
            Caption         =   "N-No Transax"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   4800
            TabIndex        =   4
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdCompanyChq 
            Caption         =   "C-Company"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   2400
            TabIndex        =   3
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdPersonalChq 
            Caption         =   "P-Personal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   0
            TabIndex        =   2
            Top             =   0
            Width           =   2300
         End
         Begin VB.CommandButton cmdEscCheque 
            Caption         =   "Esc-Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   5040
            TabIndex        =   5
            Top             =   600
            Width           =   2055
         End
      End
      Begin VB.PictureBox fraAccountNo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2415
         Left            =   0
         ScaleHeight     =   2385
         ScaleWidth      =   7065
         TabIndex        =   25
         Top             =   1320
         Visible         =   0   'False
         Width           =   7095
         Begin VB.TextBox txtChequeNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   27
            Top             =   240
            Width           =   1455
         End
         Begin VB.TextBox txtSortCode 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   6
            TabIndex        =   29
            Top             =   960
            Width           =   1455
         End
         Begin VB.TextBox txtAccountNo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   2280
            MaxLength       =   10
            TabIndex        =   31
            Top             =   1680
            Width           =   3015
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Cheque No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   26
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Sort Code"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   28
            Top             =   960
            Width           =   1695
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Account No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   240
            TabIndex        =   30
            Top             =   1680
            Width           =   1935
         End
      End
      Begin VB.PictureBox fraComplete 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2295
         Left            =   0
         ScaleHeight     =   2265
         ScaleWidth      =   7065
         TabIndex        =   32
         Top             =   1200
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdConfirm 
            Caption         =   "&Yes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   120
            TabIndex        =   35
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "&No"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   5160
            TabIndex        =   34
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CommandButton cmdExit 
            Caption         =   "EXIT"
            Height          =   495
            Left            =   2760
            TabIndex        =   33
            Top             =   1200
            Width           =   975
         End
         Begin VB.Label lblPrintPrompt 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Insert Cheque - Esc Cancel"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   36
            Top             =   240
            Visible         =   0   'False
            Width           =   6855
         End
         Begin VB.Label lblPrintChq 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Print front of cheque?"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   37
            Top             =   2160
            Visible         =   0   'False
            Width           =   6975
         End
      End
      Begin VB.PictureBox fraInsertCard 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   2535
         Left            =   0
         ScaleHeight     =   2505
         ScaleWidth      =   7065
         TabIndex        =   38
         Top             =   1200
         Visible         =   0   'False
         Width           =   7095
         Begin VB.Timer tmrESocket 
            Left            =   0
            Top             =   0
         End
         Begin VB.Label lblInsert 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1695
            Left            =   240
            TabIndex        =   39
            Top             =   120
            Width           =   6615
         End
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   44
         Top             =   5760
         Width           =   7095
      End
      Begin VB.Label lblActionRequired 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Type of Cheque"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   0
         TabIndex        =   43
         Top             =   0
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblChequeType 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   0
         TabIndex        =   42
         Top             =   600
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAuthTranMod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Authorisation Server Mode: "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   0
         TabIndex        =   41
         Top             =   720
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.Label lblAction 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter Credit Card"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   615
         Left            =   0
         TabIndex        =   40
         Top             =   60
         Visible         =   0   'False
         Width           =   7095
      End
   End
End
Attribute VB_Name = "frmTender"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "frmTender"


' Types of Sale
Const TT_SALE       As String = "SA"
Const TT_REFUND     As String = "RF"
Const TT_SIGNON     As String = "CO"
Const TT_COLLECT    As String = "IC"
Const TT_SIGNOFF    As String = "CC"
Const TT_OPENDRAWER As String = "OD"
Const TT_LOGO       As String = "RL"
Const TT_XREAD      As String = "XR"
Const TT_ZREAD      As String = "ZR"
Const TT_MISCIN     As String = "M+"
Const TT_MISCOUT    As String = "M-"
Const TT_RECALL     As String = "RP"
Const TT_QUOTE      As String = "QD"
Const TT_EXT_SALE   As String = "ES"
Const TT_TELESALES  As String = "TS"
Const TT_TELEREFUND As String = "TR"
Const TT_REPRINTREC As String = "RR"
Const TT_LOOKUP     As String = "LU"
Const TT_CORRIN     As String = "C+"
Const TT_CORROUT    As String = "C-"
Const TT_EAN_CHECK  As String = "EC"

Const RGB_LTYELLOW As Long = 12648447

Enum enCaptureMode
    encmCheque = 1
    encmCCard = 2
    encmAuthCard = 3
    encmKeyed = 4
End Enum

Enum enConfirmMode
    encmReprintSlip = 1
    encmSignatureCon = 2
    encmNon = 3
End Enum

Const VERSION_NUM                   As String = "1.0.1"

'Constants to hold credit card entry modes
Const ENTRY_ICCP                    As String = "ICC"
Const ENTRY_KEY                     As String = "Keyed"
Const ENTRY_SWIPE                   As String = "Swipe"
'Responces returned from modPayware.InitiliseTransaction
Const READ_ICC                      As Integer = 0
Const READ_EMV                      As Integer = 30
Const MANUAL_ENTRY                  As String = "Manual Entry"
'Cheque types
Const CHEQUE_PERSONAL               As String = "Personal"
Const CHEQUE_COMP                   As String = "Company"
Const CHEQUE_NOTRANSAX              As String = "No Transax"

'Constants refering to the authorisation server
Const AUTH_ONLINE                   As String = "Online"
Const AUTH_OFFLINE                  As String = "Offline"
Const AUTHORISED_BY_PED             As String = "PED"
Const AUTH_SERVER_FOUND             As String = "0"
Const AUTH_SERVER_NOT_FOUND         As String = "1"
Const PRM_AUTHTMODE                 As Long = "827"
Const PRM_CARREG                    As Long = "808"

Const SELECT_CHEQUE_MSG             As String = "Press P for personal cheque" & vbCrLf & _
        "Press C for company cheque" & vbCrLf & _
        "Press N for No Transax" & vbCrLf & "Or press escape to cancel"

Private mStartedTransaction         As Boolean

Private enEntryMode                 As enCaptureMode
Private enConfirmationMode          As enConfirmMode
Private moOPOSPrinter               As Object   ' As OPOSPOSPrinter

Private moTranHeader     As Object

Dim mblnRefund                      As Boolean
Dim mblnCNPTill                     As Boolean
Dim mblnIsCheque                    As Boolean
Dim mCustomerPresent                As Boolean
Dim mblnCheckCNP                    As Boolean

Dim mlngUseEFTPOS                   As Long
Dim mblnAuthEntered                 As Boolean
Dim mstrTransactionAmount           As String
Dim mdteTransactionDate             As Date
Dim mstrTransactionNo               As String
Dim mstrCreditCardNum               As String
Dim mstrExpiryDate                  As String
Dim mstrStartDate                   As String
Dim mstrIssueNum                    As String
Dim mstrAuthNum                     As String
Dim mstrTrack2                      As String
Dim mstrTmode                       As String
Dim mstrAuthResponse                As String
Dim mstrAuthType                    As String
Dim mstrTerminalVerificationResult  As String
Dim mstrTransactionStatusInfo       As String
Dim mstrAuthResponceCode            As String
Dim mstrUnpredictableNumber         As String
Dim mstrApplicationIdentifier       As String
Dim mstrCashierName                 As String
Dim mstrEntryMethod                 As String
Dim mstrVerification                As String
Dim mstrAuthSuccess                 As String
Dim mstrAuthServerFound             As String
Dim mstrTransactionType             As String
Dim mstrCaptureCarReg               As String
Dim mstrCarReg                      As String
Dim mstrChequeGuaranteeValue        As String
Dim mstrChequeNo                    As String
Dim mstrSortCode                    As String
Dim mstrAccountNo                   As String
Dim mstrChequeType                  As String
Dim mstrEFTTransID                  As String
Dim mlngAttemptNum                  As Long
Dim mstrFallBackType                As String
Dim mstrEFTTranID                   As String
Dim mstrTransactionSource           As String

Dim mstrDebugInfo   As String

Dim mblnInitiateCheque     As Boolean


' eSocket variables
Const ERR_CONNECTION As Long = 10061
 


Dim mstrResponseType                        As String
Dim mblnSendComplete                        As Boolean
Dim mblnCardDetailsEntered                  As Boolean
Dim mstrData                                As String
Dim mstrCardProductName                     As String
Dim mstrSignatureRequired                   As String
Dim mstrPanEntryMode                        As String
Dim mstrEmvAmount                           As String
Dim mstrEmvAmountOther                      As String
Dim mstrEmvApplicationIdentifier            As String
Dim mstrEmvApplicationInterchangeProfile    As String
Dim mstrEmvApplicationLabel                 As String
Dim mstrEmvApplicationTransactionCounter    As String
Dim mstrEmvApplicationUsageControl          As String
Dim mstrEmvApplicationVersionNumber         As String
Dim mstrEmvAuthorizationResponseCode        As String
Dim mstrEmvCryptogram                       As String
Dim mstrEmvCryptogramInformationData        As String
Dim mstrEmvCvmResults                       As String
Dim mstrEmvIssuerApplicationData            As String
Dim mstrEmvTerminalCapabilities             As String
Dim mstrEmvTerminalCountryCode              As String
Dim mstrEmvTerminalType                     As String
Dim mstrEmvTerminalVerificationResult       As String
Dim mstrEmvTransactionCurrencyCode          As String
Dim mstrEmvTransactionDate                  As String
Dim mstrEmvTransactionStatusInformation     As String
Dim mstrEmvTransactionType                  As String
Dim mstrEmvUnpredictableNumber              As String
Dim mstrIACDefault                          As String
Dim mstrCardSequenceNum                     As String
Dim mstrIACDenial                           As String
Dim mstrIACOnline                           As String
Dim mstrTID                                 As String
Dim mstrMerchantID                          As String
Dim mblnCustPresent                         As Boolean

Dim mblnShowNumPad   As Boolean
Dim mblnUsingNumPad  As Boolean 'flag if using Number pad for screen resizing
Dim mblnResizedKeyPad As Boolean
Dim mblnProcessCheque As Boolean

Dim mTranResult     As TranResult
Dim mPrintFormat    As String
Dim mPrintLayout    As String
Dim mTranAmount     As Long
Dim mIsSale         As Boolean
Dim mAllowCashback  As Boolean

Public AuthoriseEFTPaymentResult As Boolean
Public DebugEFTPrint As String

Public Event Duress()

Const RI_STATUS_CODE As String = "S"
Const RI_OK_CODE As String = "0"
Const RI_AUTH_ONLINE_CODE As String = "1"
Const RI_AUTH_TERMINAL_CODE As String = "2"
Const RI_AUTH_MANUAL_CODE As String = "3"
Const RI_DECLINE_CODE As String = "4"
Const RI_CANCEL_CODE As String = "5"
Const RI_REFERRAL_CODE As String = "6"
Const RI_MANAUTH_CODE As String = "7"
Const RI_SIGNATURE_CODE As String = "8"
Const RI_PHONE_CODE As String = "9"

Public Function TransactionResult() As TranResult
    TransactionResult = mTranResult
End Function

'Public Sub LoadEFTPayment(TranHeader As Object, _
'                            POSPrinter As OPOSPOSPrinter, _
'                            IsSale As Boolean, _
'                            TransactionAmount As Long, _
'                            CustomerPresent As Boolean, _
'                            AllowCashback As Boolean, _
'                            IsCheque As Boolean, _
'                            PrintFormat As String, _
'                            PrintLayout As String, _
'                            ByRef blnCheckCNP As Boolean, _
'                            CashierName As String)

Public Sub LoadEFTPayment(TranHeader As Object, _
                            POSPrinter As Object, _
                            IsSale As Boolean, _
                            TransactionAmount As Long, _
                            CustomerPresent As Boolean, _
                            AllowCashback As Boolean, _
                            IsCheque As Boolean, _
                            PrintFormat As String, _
                            PrintLayout As String, _
                            ByRef blnCheckCNP As Boolean, _
                            CashierName As String)

        
    Call DebugMsg(MODULE_NAME, "LoadEFTPayment", endlTraceIn)
    Set moOPOSPrinter = POSPrinter
    Set moTranHeader = TranHeader
    
    mPrintFormat = PrintFormat
    mPrintLayout = PrintLayout
    mTranAmount = TransactionAmount
    mCustomerPresent = CustomerPresent
    mIsSale = IsSale
    mAllowCashback = AllowCashback
    mblnIsCheque = IsCheque
    mstrCashierName = CashierName
    mblnCheckCNP = blnCheckCNP
    mstrTransactionAmount = TransactionAmount
    
End Sub

Public Function AuthoriseEFTPayment()

Dim AuthString As String
Dim NotValidResponse As Boolean

    On Error GoTo AuthoriseEFTError
        
    Call DebugMsg(MODULE_NAME, "AuthoriseEFTPayment", endlTraceIn)
        
    mStartedTransaction = True
    txtCardNo.Text = ""
    txtStartMonth = ""
    txtStartYear = ""
    txtEndMonth = ""
    txtEndYear = ""
    txtIssueNo.Text = ""
    txtCVV.Text = ""
    txtAuthNum.Text = ""
    
    fraBorder.Visible = False
    fraInsertCard.Visible = True
    lblStatus.Visible = True
    lblAction.Visible = True
    txtAuthNum.BackColor = RGBEdit_Colour
    txtIssueNo.BackColor = RGBEdit_Colour
    mstrCreditCardNum = vbNullString
    mstrExpiryDate = ""
    mstrStartDate = ""
    
    Call DebugMsg(MODULE_NAME, "AuthoriseEFTPayment", endlDebug, "Starting Authorise:IsCheque-" & mblnIsCheque)
    If (mblnIsCheque = True) Then
    
        Call CaptureCheque
        'TransactionResult = mTranResult
    Else
        
        mblnCNPTill = Not mCustomerPresent 'if Customer not present then switch on Keying
        If (mblnCheckCNP = True) Then
            '***********
            'For a CNP Till, prompt if user present and if not, switch off CVV, until need CC auth required
            If (mblnCNPTill = True) Then ' And (IsSale = True) Then 'WIX1378 - allow CnP Refunds
                If (MsgBoxEx("Customer Present Y/N", vbQuestion + vbYesNo, "Process Payment", , , , , RGBMsgBox_PromptColour) = vbYes) Then
                    mblnCNPTill = False
                    mCustomerPresent = True
                End If
            End If
            
        End If
            
        'WIX1378 - allow CnP Refunds so do not override flag
        'If (IsSale = False) Then mblnCNPTill = False 'only allow CNP if sales
        
        'SWitch into CNP Till mode if required
        If (mblnCNPTill = True) Then
            mstrEntryMethod = ENTRY_KEY
            Call TypeCCDetails
            txtCVV.Enabled = True
            txtCustPresent.Text = "N"
            mCustomerPresent = False
            txtCustPresent.Enabled = False
            ShowKeyPad
            mblnCheckCNP = True
            txtCardNo.SetFocus
            mblnCardDetailsEntered = False
            Do
                DoEvents
            Loop Until mblnCardDetailsEntered = True
            DebugEFTPrint = mstrDebugInfo
            DoEvents
            Exit Function
        End If
            
        
        Call PerformAuth(mTranAmount, txtCVV.Text <> "", mIsSale, mCustomerPresent, mAllowCashback, False, mTranResult)
    End If
    
    DoEvents
    Exit Function
    
AuthoriseEFTError:

    Call MsgBoxEx("Error has occurred in EFT system" & vbNewLine & Err.Number & ":" & Err.Description, vbExclamation, "Error Detected", , , , , RGBMsgBox_WarnColour)
    Call Err.Clear
         
End Function

'************************************************************************
'* Procedure to perform the comms to the PinPad and obtain authorisation
'************************************************************************
Private Sub PerformAuth(TranAmount As Long, Keyed As Boolean, IsSale As Boolean, CustomerPresent As Boolean, AllowCashback As Boolean, ChequeAuth As Boolean, ByRef tRes As TranResult)

Dim PayObject As COMLink.Pay
Dim NotValidResponse As Boolean
Dim EFTPrintSlip  As String
Dim PrintedOK     As Boolean
Dim UseFallback   As Boolean
Dim lngErrNo      As Long
Dim strEntryType  As String
Dim strErrorCode  As String
Dim strTranType   As String
Dim strTranSource As String
Dim strAmount     As String
    
On Error GoTo PerformAuth_Error
    
    Call DebugMsg(MODULE_NAME, "PerformAuth", endlTraceIn)
    Set PayObject = New Pay
    Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Log to : " & mstrLoggingPath & "(" & mblnLogEFT & ")")
    PayObject.Connection.DebugFilePath = mstrLoggingPath
    DoEvents
    PayObject.Connection.DebugLoggingEnabled = mblnLogEFT
    
    
    PayObject.Connection.RemoteHost = mstrEFTHost
    PayObject.Connection.RemotePort = mstrEFTHostPort
    PayObject.Connection.timeOut = mlngEFTTimeout
    PayObject.Connection.InterPacketDelay = 1000
    
    PayObject.Connection.CommsMethod = CommsXML
    PayObject.Trans.TransactionNumber = Format(Time(), "HHNNSS")
    PayObject.Trans.SourceTerminalIdentifier = "1234"
    PayObject.Trans.Wait = True
    
    Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Cancel Previous Tran ")
    Call PayObject.StatusResponse(CancelResponse) 'Added this to send cancel previous Transaction
    
    Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Setting Up Validation Request")
    PayObject.Trans.TransactionAmount = TranAmount
    PayObject.Trans.SourceTerminalIdentifier = "8099"
    If (IsSale = True) Then
        If (AllowCashback = True) Then
            PayObject.Trans.TransactionType = "3"
        Else
            PayObject.Trans.TransactionType = "0"
        End If
    Else
        PayObject.Trans.TransactionType = "5"
    End If
    
    PayObject.Trans.ChequeAccountNumber = txtAccountNo.Text
    PayObject.Trans.ChequeNumber = txtChequeNo.Text
    PayObject.Trans.BankSortCode = txtSortCode.Text
    PayObject.Connection.SecureMode = 0
    If (ChequeAuth = True) And (mstrChequeType = CHEQUE_COMP) Then
        PayObject.Trans.TransactionType = "1"
        PayObject.Trans.TransactionSource = "1"
        strEntryType = "Keyed-Cust Present"
        UseFallback = False
    Else
        If (txtCVV.Text <> "") Then  'Keyed entry
            PayObject.Trans.Card.CV2Data = txtCVV.Text

            If (txtCustPresent.Text = "Y") Then
                PayObject.Trans.TransactionSource = "1" 'Keyed Cust Present
                strEntryType = "Keyed-Cust Present"
            Else
                PayObject.Trans.TransactionSource = "3" 'Keyed Cust Not Present 'M.Milne 6/3/09 changed from 7 to 3
                strEntryType = "Keyed-Cust Not Present"
            End If
            UseFallback = False
        Else
            UseFallback = True
            PayObject.Trans.TransactionSource = "i" 'ICC entry - Cust present
            strEntryType = "ICC Entry"
        End If

        If (ChequeAuth = True) Then PayObject.Trans.TransactionType = "0" 'Personal Cheque
        
        NotValidResponse = True
        
        If (Keyed = False) Then
            If (ChequeAuth = True) Then
                lblAction.Caption = "Insert Cheque Guarantee Card"
            Else
                lblAction.Caption = "Insert Credit Card"
            End If
            lblInsert.Caption = ("Please insert \ swipe the card in Pin Pad")
'            PayObject.Trans.TransactionType = "1"
'            PayObject.Trans.TransactionSource = "1"
        Else
            lblStatus.Caption = "Validate Card"
            If (Val(txtStartMonth.Text) > 0) Then PayObject.Trans.Card.StartDate = Format(Val(txtStartMonth.Text), "00") & Format(Val(txtStartYear.Text), "00")
            PayObject.Trans.Card.ExpiryDate = Format(Val(txtEndMonth.Text), "00") & Format(Val(txtEndYear.Text), "00")
            PayObject.Trans.Card.Number = txtCardNo.Text
            PayObject.Trans.Card.IssueNumber = txtIssueNo.Text
        End If
        PayObject.Trans.FollowOnMsg = True
        DoEvents
        
        '***************************************************************************
        '* First step - perform validation of card to see what can be done with it *
        '***************************************************************************
        While NotValidResponse
            PayObject.Trans.Fallback = UseFallback
            Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Validation Request")
            PayObject.Validate
            'For Non-ICC Cards then force to get Signature Auth
            If (PayObject.Trans.TransactionSource = "6") Or (PayObject.Trans.TransactionSource = "6") Then
                PayObject.Trans.TransactionSource = "0"
                strEntryType = "Swipe-Cust Present"
            End If
            
            DoEvents
            While (PayObject.Trans.ResponseIndicator = "S")
                lblStatus.Caption = PayObject.Trans.StatusMessage
                Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Validation Request Status:" & PayObject.Trans.StatusMessage)
                DoEvents
                PayObject.StatusResponse (ConfirmedResponse)
            Wend
            NotValidResponse = False
            Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Validation Request Response:" & PayObject.Trans.ResponseIndicator)
            Select Case (PayObject.Trans.ResponseIndicator)
                Case (0):
                            If (PayObject.Trans.Card.ExpiryDate < Date) Then
                            End If
                            
                Case (4):
                          Select Case (PayObject.Trans.ErrorCode)
                            Case ("013018"):
                                Call MsgBoxEx("Card detected in device." & vbNewLine & "Remove Card and retry", , "Validate Failed", , , , , RGBMsgBox_PromptColour)
                                NotValidResponse = True
                            Case ("015000"):
                                Call MsgBoxEx("Terminal Disconnected. Check connections", , "Validate Failed", , , , , RGBMsgBox_WarnColour)
                                NotValidResponse = True
                            Case ("051000"):
                                Call MsgBoxEx("Comms failure, could not connect to authorisation server.", , "Validate Failed", , , , , RGBMsgBox_WarnColour)
                                lblAction.Caption = "Obtain Manual Authorisation"
                                fraInsertCard.Visible = False
                                fraBorder.Visible = True
                                fraChequeCard.Visible = True
                                If (Keyed = False) Then Call DisplayCard(PayObject)
                                fraAuthNum.Visible = True
                                Call DisableKeyedEntry(False)
                                txtAuthNum.Text = ""
                                fraAuthNum.Enabled = True
                                mblnAuthEntered = False
                                txtAuthNum.SetFocus
                                lblStatus.Caption = "Enter Manual Authorisation Code"
                                While mblnAuthEntered = False
                                  DoEvents
                                Wend
                                          
                                PayObject.Trans.Fallback = UseFallback
                                'Send Confirmation or Cancel
                                PayObject.CompleteTrans (txtAuthNum.Text)
                            Case Else
                                lblAction.Caption = "Declined"
                                If (Val(PayObject.Trans.ErrorCode) = "0") Then
                                    MsgBoxEx ("Declined:" & PayObject.Trans.APACSResponse)
                                Else
                                    MsgBoxEx ("Declined:" & PayObject.Trans.ErrorCode & " - " & ExtractError(PayObject.Connection.RawMessageFromHost))
                                End If
                        End Select
                
                Case (5): Call MsgBoxEx("Cancelled", , "Validate", , , , , RGBMsgBox_WarnColour)
                Case Else:
                    MsgBox PayObject.Trans.AuthCode & "-" & _
                           PayObject.Trans.ResponseIndicator & "-" & _
                           PayObject.Trans.ErrorCode, , "Validate"
                
            End Select
            DoEvents
        Wend
    End If
            
    '******************************************************
    '* After Validation then create authorisation request *
    '******************************************************
    If (PayObject.Trans.ResponseIndicator = RI_OK_CODE) Or ((ChequeAuth = True) And (mstrChequeType = CHEQUE_COMP)) Then
        UseFallback = PayObject.Trans.Fallback
        If (PayObject.Trans.TransactionSource = "i") Then UseFallback = True
        Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Performing Auth Command")
        fraInsertCard.Visible = False
        fraBorder.Visible = True
        fraChequeCard.Visible = True
        fraAuthNum.Visible = False
        If (Keyed = False) Then
            Call DisplayCard(PayObject)
        End If 'keyed entry, as values already displayed
        lblStatus.Caption = "Obtain Authorisation"
        lblAction.Caption = "Processing Authorisation"
        DoEvents
        PayObject.Trans.TransactionAmount = TranAmount
        If (ChequeAuth = True) Then
            If (mstrChequeType = CHEQUE_COMP) Then
                PayObject.Trans.TransactionType = "1"
            Else
                PayObject.Trans.TransactionType = "0"
            End If
            PayObject.Trans.TransactionSource = "1"
            strEntryType = "Keyed-Cust Present"
            If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
            PayObject.AuthoriseTransax
        Else
            strTranType = PayObject.Trans.TransactionType
            strTranSource = PayObject.Trans.TransactionSource
            If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
            If (PayObject.Trans.TransactionSource = "i") Then
                UseFallback = True
                PayObject.Trans.Fallback = UseFallback
            End If
            PayObject.Authorise (True)
        End If
        
        'Process and display any status messages
        lblStatus.Caption = ""
        While (PayObject.Trans.ResponseIndicator = "S")
            lblStatus.Caption = PayObject.Trans.StatusMessage
            DoEvents
            PayObject.StatusResponse (ConfirmedResponse)
        Wend

        If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
        If (PayObject.Trans.TransactionSource = "i") Then UseFallback = True
        NotValidResponse = False
        Select Case (PayObject.Trans.ResponseIndicator)
            Case (0), (1), (2), (3):
                        fraAuthNum.Visible = True
                        txtAuthNum.Text = PayObject.Trans.AuthCode
                        If (PayObject.Trans.AuthCode = "") Then
                            txtAuthNum.Text = "TERMAUTH"
                            PayObject.Trans.AuthCode = "TERMAUTH"
                        End If
                        lblAction.Caption = "Authorised"
                        Call ExtractResponse(PayObject, PayObject.Trans.AuthCode = "")
                        Call SetEntryMethod
                        DoEvents
                        Select Case (mPrintFormat)
                          Case ("A4"):
                            Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Printing A4 Slip")
                            Load frmPrintSlip
                            With PayObject.Trans
                                Call frmPrintSlip.PrintSlip(IsSale, mCustomerPresent, .MerchantNumber, mstrTID, _
                                    .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                    .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject), False, True)
                            End With
                            
                            Unload frmPrintSlip
                            Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Printed A4 Slip")
                          Case Else
                            With PayObject.Trans
                                EFTPrintSlip = POSPrintSlip(IsSale, CustomerPresent, .MerchantNumber, mstrTID, _
                                    .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                    .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject))
                                mstrPOSPrintSlip = EFTPrintSlip
                            End With
                        End Select
            Case (4):
                      If (PayObject.Trans.ErrorCode = "013018") Then
                          Call MsgBox("Remove Card")
                          NotValidResponse = True
                      Else
                          lblAction.Caption = "Declined"
                          If (Val(PayObject.Trans.ErrorCode) = "0") Then
                              MsgBoxEx ("Declined:" & PayObject.Trans.APACSResponse)
                          Else
                              MsgBoxEx ("Declined:" & PayObject.Trans.ErrorCode & " - " & ExtractError(PayObject.Connection.RawMessageFromHost))
                          End If
                      End If
            
            Case (5): lblAction.Caption = "Cancelled"
                        Call MsgBoxEx("Customer has cancelled", vbOKOnly + vbInformation, "Obtain Authorisation", , , , , RGBMsgBox_WarnColour)
            Case (7):   lblAction.Caption = "Obtain Manual Authorisation"
                        fraAuthNum.Visible = True
                        Call MsgBoxEx("Manual authorisation requested.  Remove Card from PED", vbOKOnly, "Manual Authorisation", , , , , RGBMsgBox_PromptColour)
                        Call DisableKeyedEntry(False)
                        txtAuthNum.Text = ""
                        mblnAuthEntered = False
                        fraAuthNum.Enabled = True
                        txtAuthNum.Enabled = True
                        txtAuthNum.SetFocus
                        lblStatus.Caption = "Enter Manual Authorisation Code"
                        While mblnAuthEntered = False
                          DoEvents
                        Wend
                        
                        If (txtAuthNum.Text = "") Then
                            lblAction.Caption = "Declined"
                            PayObject.Trans.AuthCode = ""
                            PayObject.CancelTrans (True)
                        Else
                            Call ExtractResponse(PayObject, PayObject.Trans.AuthCode = "")
                            Call SetEntryMethod
                            DoEvents
                            Do
                                Select Case (mPrintFormat)
                                Case ("A4"):
                                    Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Printing A4 Slip")
                                    Load frmPrintSlip
                                    With PayObject.Trans
                                        Call frmPrintSlip.PrintSlip(IsSale, CustomerPresent, .MerchantNumber, mstrTID, _
                                              .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                              .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject), True, True)
                                    End With
                                    Unload frmPrintSlip
                                    Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Printed A4 Slip")
                                Case Else
                                    PrintPOSSignatureSlip
                                End Select
                          
                                If MsgBoxEx("Did the Signature Slip print ok?", vbYesNo, "Signature Slip", , , , , RGBMsgBox_PromptColour) = vbYes Then
                                    PrintedOK = True
                                Else
                                    PrintedOK = False
                                End If
                            Loop While (PrintedOK = False)
                            If MsgBoxEx("Is the Signature ok?", vbYesNo, "Check Signature", , , , , RGBMsgBox_PromptColour) = vbYes Then
                                If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
                                PayObject.CompleteTrans (txtAuthNum.Text)
                                If Val(mstrTransactionAmount) > (50 * 100) Then
                                    If (txtCVV.Text <> "") And (txtCustPresent.Text = "Y") And (IsSale = False) Then
                                        Call MsgBoxEx("An imprint of the card must be obtained on a verification voucher for this tender." & _
                                              vbCrLf & "Confirm verification completed and signed by customer", vbOKOnly, _
                                              "Manual Imprint Required", "Confirm", , , , RGBMsgBox_WarnColour)
                                    End If
                                End If
                            Else
                                lblAction.Caption = "Declined"
                                PayObject.Trans.AuthCode = ""
                                PayObject.CancelTrans (True)
                            End If
                        End If
                      
            Case (8): 'Signature if Cust Present or CVV Validation if CNP
                    If (txtCVV.Text <> "") Then  'Keyed entry
                        If (InStr(mstrCV2Success, PayObject.Trans.CV2ValidationResponse) > 0) Then
                            fraAuthNum.Visible = True
                            txtAuthNum.Text = PayObject.Trans.AuthCode
                            If (PayObject.Trans.AuthCode = "") Then txtAuthNum.Text = "TERMAUTH"
                            lblAction.Caption = "Authorised"
                            Call ExtractResponse(PayObject, PayObject.Trans.AuthCode = "")
                            Call SetEntryMethod
                            If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
                            PayObject.CompleteTrans
                            If (PayObject.Trans.AuthCode = "") Then PayObject.Trans.AuthCode = "TERMAUTH"
                            DoEvents
                            Select Case (mPrintFormat)
                              Case ("A4"):
                                Load frmPrintSlip
                                With PayObject.Trans
                                    Call frmPrintSlip.PrintSlip(IsSale, CustomerPresent, .MerchantNumber, mstrTID, _
                                        .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                        .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject), False, True)
                                End With
                                Unload frmPrintSlip
                              Case Else
                                With PayObject.Trans
                                    EFTPrintSlip = POSPrintSlip(IsSale, CustomerPresent, .MerchantNumber, mstrTID, _
                                        .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                        .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject))
                                    mstrPOSPrintSlip = EFTPrintSlip
                                End With
                            End Select
                        Else 'valid CV check performed
                            lblAction.Caption = "Declined"
                            PayObject.CancelTrans (True)
                            PayObject.Trans.AuthCode = ""
                        End If 'CV check failed
                        If (PayObject.Trans.ResponseIndicator = "4") Or (PayObject.Trans.ResponseIndicator = "5") Then
                            lblAction.Caption = "Declined"
                            MsgBoxEx ("Authorisation Declined:CVV Check " & PayObject.Trans.ErrorCode & "(" & PayObject.Trans.CV2ValidationResponse & ")")
                        End If
                    Else
                        lblAction.Caption = "Get Signature"
                        mstrTransactionAmount = TranAmount
                        If (PayObject.Trans.AuthCode = "") Then txtAuthNum.Text = "TERMAUTH"
                        Call ExtractResponse(PayObject, PayObject.Trans.AuthCode = "")
                        Do
                            Select Case (mPrintFormat)
                                Case ("A4"):
                                    Load frmPrintSlip
                                    With PayObject.Trans
                                        Call frmPrintSlip.PrintSlip(IsSale, CustomerPresent, .MerchantNumber, mstrTID, _
                                            .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                            .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject), True, True)
                                    End With
                                    Unload frmPrintSlip
                                Case Else
                                    PrintPOSSignatureSlip
                            End Select
                        
                            If MsgBoxEx("Did the Signature Slip print ok?", vbYesNo, "Signature Slip", , , , , RGBMsgBox_PromptColour) = vbYes Then
                                PrintedOK = True
                            Else
                                PrintedOK = False
                            End If
                        Loop While (PrintedOK = False)
                        'For Customer not present do not confirm Signature, just complete
                        If (mblnCNPTill = True) Then
                            If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
                            PayObject.CompleteTrans
                        Else
                            If MsgBoxEx("Is the Signature ok?", vbYesNo, "Check Signature", , , , , RGBMsgBox_PromptColour) = vbYes Then
                                If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
                                PayObject.CompleteTrans
                                If Val(mstrTransactionAmount) > (50 * 100) Then
                                    If (txtCVV.Text <> "") And (txtCustPresent.Text = "Y") And (IsSale = False) Then
                                        Call MsgBoxEx("An imprint of the card must be obtained on a verification voucher for this tender." & _
                                            vbCrLf & "Confirm verification completed and signed by customer", vbOKOnly, _
                                            "Manual Imprint Required", "Confirm", , , , RGBMsgBox_WarnColour)
                                    End If
                                End If
                            Else
                                PayObject.CancelTrans (True)
                                lblAction.Caption = "Declined"
                                PayObject.Trans.AuthCode = ""
                            End If
                        End If
                        If (PayObject.Trans.ResponseIndicator = "4") Or (PayObject.Trans.ResponseIndicator = "5") Then
                            PayObject.Trans.AuthCode = ""
                            lblAction.Caption = "Declined"
                            If (PayObject.Trans.ResponseIndicator <> "5") Then
                                If (Val(PayObject.Trans.ErrorCode) = "0") Then
                                    MsgBoxEx ("Declined:" & PayObject.Trans.APACSResponse)
                                Else
                                    MsgBoxEx ("Declined:" & PayObject.Trans.ErrorCode & " - " & ExtractError(PayObject.Connection.RawMessageFromHost))
                                End If
                            End If
                        Else
                            If (PayObject.Trans.AuthCode = "") Then PayObject.Trans.AuthCode = "TERMAUTH"
                        End If
                    End If
            Case ("R"):
                      lblAction.Caption = "Referred"
                      fraAuthNum.Visible = True
                      lblStatus.Caption = "Enter Manual Authorisation Code"
                      Call MsgBoxEx("Manual authorisation requested.  Remove Card from PED" & vbCrLf & vbCrLf & IIf(PayObject.Trans.APACSResponse <> "", "    " & PayObject.Trans.APACSResponse & vbCrLf & "    " & PayObject.Trans.ContactPhoneNumber, ""), vbOKOnly, "Manual Authorisation", , , , , RGBMsgBox_PromptColour)
                      Call DisableKeyedEntry(False)
                      txtAuthNum.Text = ""
                      mblnAuthEntered = False
                      Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Referral-Enter Auth Number")
                      fraAuthNum.Enabled = True
                      txtAuthNum.Enabled = True
                      txtAuthNum.SetFocus
                      lblStatus.Caption = "Enter Manual Authorisation Code"
                      Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Referral-Wait for Enter Auth Number")
                      While mblnAuthEntered = False
                        DoEvents
                      Wend
                      
                      Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Referral-Wait for Enter Auth Number")
                      If (PayObject.Trans.ActualverificationMethod = "signature") Or _
                            (PayObject.Trans.ActualverificationMethod = "pin_and_signature") Or _
                            (PayObject.Trans.ActualverificationMethod = "not_performed") Or _
                            (PayObject.Trans.ActualverificationMethod = "unknown") Then
                            'Print Signature, if OK then get manual auth and Settle else just exit routine
                            Do
                                Select Case (mPrintFormat)
                                    Case ("A4"):
                                        Load frmPrintSlip
                                        With PayObject.Trans
                                            Call frmPrintSlip.PrintSlip(IsSale, CustomerPresent, .MerchantNumber, mstrTID, _
                                                .TransactionNumber, .Card.ApplicationName, Replace(.ActualverificationMethod, "_", " ") & " verified", .TransDate, .TransTime, .Card.Number, IIf(.Card.seqNum = "", .Card.IssueNumber, .Card.seqNum), _
                                                .Card.StartDate, "    ", .Card.ApplicationID, .AuthCode, .TransactionAmount / 100, "", "", "", GetDebugInfo(PayObject), True, True)
                                        End With
                                        Unload frmPrintSlip
                                    Case Else
                                        PrintPOSSignatureSlip
                                End Select
                            
                                If MsgBoxEx("Did the Signature Slip print ok?", vbYesNo, "Signature Slip", , , , , RGBMsgBox_PromptColour) = vbYes Then
                                    PrintedOK = True
                                Else
                                    PrintedOK = False
                                End If
                            Loop While (PrintedOK = False)
                            If MsgBoxEx("Is the Signature ok?", vbYesNo, "Check Signature", , , , , RGBMsgBox_PromptColour) = vbYes Then
                                If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
                                PayObject.CompleteTrans
                                If Val(mstrTransactionAmount) > (50 * 100) Then
                                    If (txtCVV.Text <> "") And (txtCustPresent.Text = "Y") And (IsSale = False) Then
                                        Call MsgBoxEx("An imprint of the card must be obtained on a verification voucher for this tender." & _
                                            vbCrLf & "Confirm verification completed and signed by customer", vbOKOnly, _
                                            "Manual Imprint Required", "Confirm", , , , RGBMsgBox_WarnColour)
                                    End If
                                End If
                            Else
                                'Signature failed so Cancel and Clear down Auth Code
                                PayObject.CancelTrans (True)
                                txtAuthNum.Text = ""
                                DoEvents
                            End If
                      End If
                      If (txtAuthNum.Text <> "") Then
                          'Send Settle to manually submit request
                          PayObject.Trans.MethodOfAuth = "2" 'required for Settle
                          If (PayObject.Trans.Card.Number <> "") And (PayObject.Trans.Card.Track2Details <> "") And _
                            (UCase(Left$(PayObject.Trans.ActualverificationMethod, 3)) <> "PIN") Then PayObject.Trans.Card.Number = ""
                          If (PayObject.Trans.TransactionType = "") Then PayObject.Trans.TransactionType = strTranType
                          If (PayObject.Trans.TransactionSource = "") Then PayObject.Trans.TransactionSource = strTranSource
                          If (Val(PayObject.Trans.TransactionAmount) = 0) Then PayObject.Trans.TransactionAmount = TranAmount
                          If (PayObject.Trans.TransactionSource <> "i") Then PayObject.Trans.Fallback = UseFallback
                          PayObject.Trans.AuthCode = txtAuthNum.Text
                          PayObject.Settle
                      End If
                      If (PayObject.Trans.ResponseIndicator = "4") Or (PayObject.Trans.ResponseIndicator = "5") Then
                          lblAction.Caption = "Declined"
                          If (PayObject.Trans.ResponseIndicator <> "5") Then
                              If (Val(PayObject.Trans.ErrorCode) = "0") Then
                                  MsgBoxEx ("Declined:" & PayObject.Trans.APACSResponse)
                              Else
                                  MsgBoxEx ("Declined:" & PayObject.Trans.ErrorCode & " - " & ExtractError(PayObject.Connection.RawMessageFromHost))
                              End If
                          End If
                      Else
                          'Settle wipes Auth Code, so reset to value to inform calling routines
                          PayObject.Trans.AuthCode = txtAuthNum.Text
                      End If
                        
            
            Case Else:
                MsgBox PayObject.Trans.AuthCode & "-" & _
                       PayObject.Trans.ResponseIndicator & "-" & _
                       PayObject.Trans.ErrorCode
        
        End Select
    End If
    DebugEFTPrint = GetDebugInfo(PayObject)
    mstrDebugInfo = DebugEFTPrint
    PayObject.Connection.DebugLoggingEnabled = False
    PayObject.Trans.Target = "terminal"
    PayObject.RemoveCard
    
    Call SetEntryMethod
    With PayObject.Trans.Card
        tRes.AcquirerIdentifier = .AcquirerIdentifier
        tRes.AcquirerName = .AcquirerName
        tRes.ActionCodeDefault = .ActionCodeDefault
        tRes.AuthCode = PayObject.Trans.AuthCode
        tRes.ActualverificationMethod = Replace(PayObject.Trans.ActualverificationMethod, "_", " ") & " verified"
        tRes.ApplicationID = .ApplicationID
        tRes.ApplicationLabel = .ApplicationLabel
        tRes.ApplicationName = .ApplicationName
        tRes.AuthorisedAmount = PayObject.Trans.AuthorisedAmount
        tRes.AuthorisingEntity = PayObject.Trans.AuthorisingEntity
        tRes.ExpiryDate = .ExpiryDate
        tRes.StartDate = .StartDate
        Call DebugMsg(MODULE_NAME, "PerformAuth", endlDebug, "Read back Start Date of : " & .StartDate)
        tRes.TransDate = PayObject.Trans.TransDate
        tRes.TransTime = PayObject.Trans.TransTime
        tRes.Number = .Number
        tRes.IssueNumber = PayObject.Trans.Card.IssueNumber
        tRes.seqNum = PayObject.Trans.Card.seqNum
        tRes.AuthRespCode = PayObject.Trans.AuthRespCode
        tRes.EntryMethod = mstrEntryMethod
        tRes.MerchantNumber = PayObject.Trans.MerchantNumber
        tRes.TermIFD = Mid$(PayObject.Trans.APACSRequestHeader, 2)
        If (Len(tRes.TermIFD) > 0) Then tRes.TermIFD = Left$(tRes.TermIFD, 8) & " " & Mid$(tRes.TermIFD, 9, 4)
'        tRes
        txtAuthNum.Text = tRes.AuthCode
        
        tRes.ApplicationName = PayObject.Trans.Card.ApplicationLabel
        If (tRes.ApplicationName = "") Then tRes.ApplicationName = PayObject.Trans.Card.ApplicationName
        If (tRes.ApplicationName = "") Then tRes.ApplicationName = PayObject.Trans.Card.AcquirerName
    
    End With
    
    Set PayObject = Nothing
    Exit Sub
    
PerformAuth_Error:
    lngErrNo = Err.Number
    Select Case (lngErrNo)
        Case (-2147219989):
            Call MsgBox("Error has occurred when Performing Auth" & vbNewLine & _
            "Check SmartSwitch service is running." & vbNewLine & _
                Err.Number & ":" & Err.Description, vbCritical, "Error Occurred")
        Case (-2147219983):
            Call MsgBox("Error has occurred when Performing Auth" & vbNewLine & _
                "Device has timed out" & vbNewLine & _
                Err.Number & ":" & Err.Description, vbCritical, "Error Occurred")
        Case Else:
            Call MsgBox("Error has occurred when Performing Auth" & vbNewLine & _
                Err.Number & ":" & Err.Description, vbCritical, "Error Occurred")
    End Select
    Call Err.Clear
    Resume Next
    
End Sub

Private Sub DisplayCard(PayTran As Pay)
    
    If (Len(PayTran.Trans.Card.StartDate) > 4) Then
        txtStartMonth = Mid$(PayTran.Trans.Card.StartDate, 3, 2)
        txtStartYear = Mid$(PayTran.Trans.Card.StartDate, 5, 2)
    Else
        txtStartMonth = Mid$(PayTran.Trans.Card.StartDate, 3, 2)
        txtStartYear = Mid$(PayTran.Trans.Card.StartDate, 5, 2)
    End If
    If (Len(PayTran.Trans.Card.ExpiryDate) > 4) Then
        txtEndMonth = Mid$(PayTran.Trans.Card.ExpiryDate, 3, 2)
        txtEndYear = Mid$(PayTran.Trans.Card.ExpiryDate, 5, 2)
    Else
        txtEndMonth = Left$(PayTran.Trans.Card.ExpiryDate, 2)
        txtEndYear = Mid$(PayTran.Trans.Card.ExpiryDate, 3, 2)
    End If
    txtCardNo.Text = String$(Len(PayTran.Trans.Card.Number) - 4, "X") & Right$(PayTran.Trans.Card.Number, 4)
    txtIssueNo.Text = PayTran.Trans.Card.IssueNumber

End Sub
'************************************************************************
'* Using PayObject, Create string for printing of all Debug Info (usually for bank testing)
'************************************************************************
Private Function GetDebugInfo(PayTran As Pay) As String

Dim DebugInfo As String

    Call DebugMsg(MODULE_NAME, "GetDeugInfo", endlTraceIn, mblnDebugPrint)
    If (mblnDebugPrint = False) Then Exit Function
    With PayTran
        DebugInfo = _
            "Cryptogram:" & .Trans.CryptoTransType & vbCrLf & _
            "Cryptogram Information Data:" & .Trans.CryptoInfoData & vbCrLf & _
            "Transaction Date:" & .Trans.TransDate & vbCrLf & _
            "Transaction Type:" & .Trans.TransactionType & vbCrLf & _
            "Transaction Currency Code:" & .Trans.Curr & vbCrLf & _
            "Terminal Country Code:" & .Trans.Country & vbCrLf & _
            "CVMR:" & .Trans.CardholderResult & vbCrLf & _
            "TSI:" & .Trans.StatusInfo & vbCrLf & _
            "TVR:" & .Trans.TerminalResult & vbCrLf & _
            "ARC:" & .Trans.ApacsResponseCode & vbCrLf & _
            "IAC Default:" & .Trans.Card.ActionCodeDefault & vbCrLf & _
            "IAC Denial:" & .Trans.Card.ActionCodeDenial & vbCrLf & _
            "IAC Online:" & .Trans.Card.ActionCodeOnline & vbCrLf
    End With
    Call DebugMsg(MODULE_NAME, "GetDeugInfo", endlTraceOut, "GetDebugInfo:" & Replace(DebugInfo, vbCrLf, "|"))
    GetDebugInfo = DebugInfo
        
End Function

Private Function ExtractError(ByVal strRawXML As String) As String

    On Error Resume Next
    strRawXML = Mid$(strRawXML, InStr(strRawXML, "<ERROR code=") + 25)
    If (InStr(strRawXML, "![CDATA") > 0) Then
        strRawXML = Mid$(strRawXML, InStr(strRawXML, "![CDATA") + 8)
        ExtractError = Left$(strRawXML, InStr(strRawXML, "]]") - 1)
    Else
        strRawXML = Mid$(strRawXML, InStr(strRawXML, ">") + 1)
        ExtractError = Left$(strRawXML, InStr(strRawXML, "</MESSAGE") - 1)
    End If
    Err.Clear
    
End Function

'************************************************************************
'* For Till based printing with items, creates string for printing of EFT info
'************************************************************************
Private Function POSPrintSlip(IsSale As Boolean, CustomerPresent As Boolean, MerchantNumber As String, _
                TerminalID As String, TransactionNo As String, _
                CardName As String, CardReadMethod As String, TranDate As String, TranTime As String, _
                CardNumber As String, IssueNo As String, StartDate As String, ExpiryDate As String, _
                ApplicationID As String, AuthorisationCode As String, TotalAmount As Currency, _
                VerifyMessage As String, Fallbacks As String, OnlineAuthFailed As String, _
                 ICCDebugInfo As String)
                
Dim sprdSlip As String

    sprdSlip = sprdSlip & "Customer Copy"
    sprdSlip = sprdSlip & IIf(mIsSale, "Sale", "Refund")
    If (CustomerPresent = False) Then
        sprdSlip = sprdSlip & "Cardholder Not Present"
    Else
        sprdSlip = sprdSlip & ""
    End If
    
    sprdSlip = sprdSlip & "Merchant:" & MerchantNumber
    sprdSlip = sprdSlip & mstrStoreName
    sprdSlip = sprdSlip & "TID:" & TerminalID
    sprdSlip = sprdSlip & "Tran No:" & TransactionNo
    
    sprdSlip = sprdSlip & CardName
    sprdSlip = sprdSlip & CardReadMethod
    sprdSlip = sprdSlip & Left$(TranDate, 2) & "/" & Mid$(TranDate, 3, 2) & "/" & Mid$(TranDate, 5, 2)
    sprdSlip = sprdSlip & TranTime
    
    sprdSlip = sprdSlip & "XXXX XXXX XXXX " & Right$(CardNumber, 4)
    sprdSlip = sprdSlip & IssueNo
    
    If (Len(StartDate) = 4) Then
        sprdSlip = sprdSlip & Left$(StartDate, 2) & "/" & Mid$(StartDate, 3)
    Else
        sprdSlip = sprdSlip & Mid$(StartDate, 3, 2) & "/" & Mid$(StartDate, 5, 2)
    End If
        
'    sprdSlip = sprdSlip & ExpiryDate
'    If (Len(StartDate) = 4) Then
'        sprdSlip = sprdSlip & Left$(ExpiryDate, 2) & "/" & Mid$(ExpiryDate, 3)
'    Else
'        sprdSlip = sprdSlip & Mid$(ExpiryDate, 3, 2) & "/" & Mid$(ExpiryDate, 5, 2)
'    End If
    
    sprdSlip = sprdSlip & ApplicationID
    
    sprdSlip = sprdSlip & AuthorisationCode
    sprdSlip = sprdSlip & "Amount:" & Format(TotalAmount, "currency")
    
    Dim reversed As Boolean
    If ((IsSale = True) And (reversed = False)) Or ((IsSale = False) And (reversed = True)) Then
        sprdSlip = sprdSlip & "Please debit my account with the amount specified"
    Else
        sprdSlip = sprdSlip & "Please credit my account with the amount specified"
    End If
    
    sprdSlip = sprdSlip & VerifyMessage
    sprdSlip = sprdSlip & Fallbacks
'    If ((IsSale = True) And (reversed = False)) Or ((IsSale = False) And (reversed = True)) Then
'        sprdSlip = sprdSlip & "2.5% of this is paid by me to Wickes Retail Services Ltd for handling this transaction for me." & vbNewLine & "The total amount paid is the same however I pay."
'    Else
'        sprdSlip = sprdSlip & "The amount above includes a refund of 2.5% card handling charge made by Wickes Retail Services."
'    End If
    
    If (ICCDebugInfo <> "") Then
        sprdSlip = sprdSlip & "ICC Debug Information"
        sprdSlip = sprdSlip & ICCDebugInfo
    End If

    POSPrintSlip = sprdSlip
    
End Function



'************************************************************************
'* Routine to extract values from PayObject into local variables for displaying and passing out
'************************************************************************
Private Sub ExtractResponse(PayTran As Pay, TermAuth As Boolean)

    mstrTransactionSource = PayTran.Trans.TransactionSource
    mstrCardProductName = PayTran.Trans.Card.ApplicationLabel
    If (mstrCardProductName = "") Then mstrCardProductName = PayTran.Trans.Card.ApplicationName
    If (mstrCardProductName = "") Then mstrCardProductName = PayTran.Trans.Card.AcquirerName
    mstrCreditCardNum = PayTran.Trans.Card.Number
    mstrStartDate = PayTran.Trans.Card.StartDate
    If (Len(mstrStartDate) > 4) Then mstrStartDate = Mid$(mstrStartDate, 3)
    mstrExpiryDate = PayTran.Trans.Card.ExpiryDate
    If (Len(mstrExpiryDate) > 4) Then mstrExpiryDate = Mid$(mstrExpiryDate, 3)
    mstrEntryMethod = Replace(PayTran.Trans.ActualverificationMethod, "_", " ") & " verified"
    mstrAuthNum = PayTran.Trans.AuthCode
    If (TermAuth = True) Then mstrAuthNum = "TERMAUTH"
    mstrCardSequenceNum = PayTran.Trans.Card.IssueNumber
    mstrEmvTerminalVerificationResult = PayTran.Trans.Passed
    mstrEmvTransactionStatusInformation = PayTran.Trans.StatusInfo
    mstrEmvAuthorizationResponseCode = PayTran.Trans.ApacsResponseCode
    mstrEmvUnpredictableNumber = PayTran.Trans.UnpredictableNum
    mstrEmvApplicationIdentifier = PayTran.Trans.Card.ApplicationID
'    mstrTID ,
    mstrMerchantID = PayTran.Trans.MerchantNumber
    mstrTID = Mid$(PayTran.Trans.APACSRequestHeader, 2)
    If (Len(mstrTID) > 0) Then mstrTID = Left$(mstrTID, 8) & " " & Mid$(mstrTID, 9, 4)
    mstrEmvCryptogram = PayTran.Trans.CryptoInfoData
    mstrIACDefault = PayTran.Trans.Card.ActionCodeDefault
    mstrIACDenial = PayTran.Trans.Card.ActionCodeDenial
    mstrIACOnline = PayTran.Trans.Card.ActionCodeOnline
    mstrEmvCvmResults = PayTran.Trans.Card.CVMList
    mstrAuthType = Replace(PayTran.Trans.ActualverificationMethod, "_", " ") & " verified"
    
End Sub

'************************************************************************
'* Routine to display card details and enable Auth Box for entry of Authorisation Code,
'* wait for entry and if valid, print slip.
'************************************************************************
Private Sub GetManualAuthorisation()

    lblAction.Caption = "Card Details"
    txtCardNo.Text = String$(Len(mstrCreditCardNum) - 4, "X") & Right$(mstrCreditCardNum, 4)
    txtEndMonth.Text = Mid$(mstrExpiryDate, 3, 2)
    txtEndYear.Text = Left$(mstrExpiryDate, 2)
    txtIssueNo.Text = mstrCardSequenceNum
    mblnAuthEntered = False
    lblAction.Caption = "Enter Authorisation Number"
    lblStatus.Caption = "Please enter a manual authorisation number"
    fraChequeCard.Enabled = False
    fraChequeType.Enabled = False
    fraBorder.Enabled = True
    fraAuthNum.Enabled = True
    DoEvents
    Call MsgBoxEx("Please phone the card issuer for a manual authorisation number", vbOKOnly, "Authorisation Required", _
        , , , , RGBMsgBox_WarnColour)
    txtAuthNum.SetFocus

    Do
        DoEvents
    Loop Until mblnAuthEntered = True
    Call DebugMsg(MODULE_NAME, "Transaction", endlDebug, "Auth Num Entered:" & txtAuthNum.Text)
    While Len(txtAuthNum.Text) < 6
        txtAuthNum.Text = "0" & txtAuthNum.Text
    Wend
    DoEvents
    If (txtAuthNum.Text) = "" Then
        lblStatus.Caption = "Transaction Cancelled"
        Exit Sub
    End If
    
    mstrAuthNum = txtAuthNum.Text

    Call DebugMsg(MODULE_NAME, "Transaction", endlDebug, "Successful Transaction - Printing Slip")
    lblAction.Caption = "Printing"
    lblStatus.Caption = "Printing"
    Call PrintPOSSignatureSlip

    lblStatus.Caption = "Transaction Successful"
    Call DebugMsg(Me.Name, "GetManualAuthorisation", endlDebug, "Completed OK")

End Sub

'Public Function CaptureCreditCard(ByRef oPrinter As OPOSPOSPrinter, _
'                                ByRef dteTransDate As Date, _
'                                ByVal dblAmount As Double, _
'                                ByRef lngLimit As Long, _
'                                ByRef strCardNumber As String, _
'                                ByRef strStartDate As String, _
'                                ByRef strEndDate As String, _
'                                ByRef strIssueNo As String, _
'                                ByRef strEntryMethod As String, _
'                                ByRef strAuthNum As String, _
'                                ByRef strCashierName As String, _
'                                ByVal strTransactionType As String, _
'                                ByRef strCardType As String, _
'                                ByRef strMerchantID As String, _
'                                ByRef blnUseNumPad As Boolean)

Public Function CaptureCreditCard(ByRef oPrinter As Object, _
                                ByRef dteTransDate As Date, _
                                ByVal dblAmount As Double, _
                                ByRef lngLimit As Long, _
                                ByRef strCardNumber As String, _
                                ByRef strStartDate As String, _
                                ByRef strEndDate As String, _
                                ByRef strIssueNo As String, _
                                ByRef strEntryMethod As String, _
                                ByRef strAuthNum As String, _
                                ByRef strCashierName As String, _
                                ByVal strTransactionType As String, _
                                ByRef strCardType As String, _
                                ByRef strMerchantID As String, _
                                ByRef blnUseNumPad As Boolean)

    'Confirm that all strings are empty before load frmTender
    If (blnUseNumPad = False) Then ucKeyPad1.Visible = False
    mblnShowNumPad = blnUseNumPad
    strAuthNum = vbNullString
    strCardNumber = vbNullString
    strStartDate = vbNullString
    strEndDate = vbNullString
    strIssueNo = vbNullString
    mstrAuthNum = vbNullString
    mstrCreditCardNum = vbNullString
    mstrStartDate = vbNullString
    mstrExpiryDate = vbNullString
    mstrIssueNum = vbNullString
    mstrTerminalVerificationResult = vbNullString
    mstrTransactionStatusInfo = vbNullString
    mstrAuthResponceCode = vbNullString
    mstrUnpredictableNumber = vbNullString
    mstrApplicationIdentifier = vbNullString
    mstrVerification = vbNullString
    mstrResponseType = vbNullString
    mstrCardSequenceNum = vbNullString
    strCardType = vbNullString
    'Populate moduale level variables with information in the variables passed through from frmTill
    mblnRefund = False
    If Left$(dblAmount, 1) = "-" Then mblnRefund = True
    dblAmount = Abs(dblAmount)
    mstrTransactionAmount = dblAmount * 100
    mdteTransactionDate = dteTransDate
    mstrCashierName = strCashierName
    
    mstrTransactionNo = Format(Now, "DDHHMMSS")
    mstrTransactionNo = Mid(mstrTransactionNo, 2, 6)
    If Left(mstrTransactionNo, 1) = "0" Then
        mstrTransactionNo = Replace(Left(mstrTransactionNo, 1), "0", "9") & _
        Mid(mstrTransactionNo, 2, 5)
    End If
    
    mstrTransactionType = strTransactionType
    Set moOPOSPrinter = oPrinter
    
    mstrEntryMethod = ENTRY_KEY
    Call TypeCCDetails
    
    Call Me.Show(vbModal)
    
    strMerchantID = mstrMerchantID
    strEntryMethod = mstrEntryMethod
    strAuthNum = mstrAuthNum
    strCardNumber = mstrCreditCardNum
    strStartDate = mstrStartDate
    strEndDate = mstrExpiryDate
    strIssueNo = Mid(mstrCardSequenceNum, 2, 2)
    strCardType = mstrCardProductName

End Function 'CaptureCreditCard


Public Function CaptureCheque()

    mblnResizedKeyPad = False
    mblnProcessCheque = True
    Call SetUpKeyPad(True)
    sbStatus.Visible = False
    'mblnShowNumPad = Not blnUseNumPad
    txtChequeNo.Text = vbNullString
    txtSortCode.Text = vbNullString
    txtAccountNo.Text = vbNullString
    txtAuthNum.Text = vbNullString
    txtEndMonth.Text = vbNullString
    txtStartMonth.Text = vbNullString
    txtStartYear.Text = vbNullString
    txtEndYear.Text = vbNullString
    txtCardNo.Text = vbNullString
    txtIssueNo.Text = vbNullString
    mblnCustPresent = False
    txtCustPresent.Text = "Y"

'    enEntryMode = encmCheque
    mblnRefund = False
    If Left$(mTranAmount, 1) < 0 Then mblnRefund = True
    Me.Caption = "Record cheque payment (v" & _
        VERSION_NUM & ")"
    
'    If mstrChequeType <> CHEQUE_COMP Then
    fraInsertCard.Visible = True
    lblInsert.Alignment = 0
    If (mblnShowKeypadIcon = False) Then
        cmdCompanyChq.Visible = False
        cmdPersonalChq.Visible = False
        cmdNoTransaxChq.Visible = False
        fraChequeType.Height = 0
    End If
    fraInsertCard.BackColor = RGBMsgBox_PromptColour
    lblInsert.Caption = SELECT_CHEQUE_MSG
    fraInsertCard.Top = lblActionRequired.Top + lblActionRequired.Height + 120
    fraChequeType.Visible = True
    fraChequeType.Top = fraInsertCard.Top + fraInsertCard.Height + 120
    
    Me.Height = fraInsertCard.Top + 120 + fraInsertCard.Height + 480 + fraChequeType.Height + 120
    lblAction.Caption = "Select cheque type"
    lblAction.Visible = True
    Call CentreForm(Me)
    
    Me.Hide 'as already shown - but must wait here until values entered
    Call Me.Show(vbModal)
    
    'lngLimit = 100
    mstrCreditCardNum = txtCardNo.Text
    mstrStartDate = txtStartMonth.Text & txtStartYear.Text
    mstrExpiryDate = txtEndMonth.Text & txtEndYear.Text
    mstrIssueNum = Mid(txtIssueNo.Text, 2, 2)
    mstrAuthNum = mstrAuthNum
    mstrEFTTranID = mstrEFTTranID
    Select Case mstrChequeType
        Case CHEQUE_COMP
            mstrMerchantID = mstrMerchantID
            mstrAccountNo = txtAccountNo.Text
            mstrChequeNo = txtChequeNo.Text
            mstrSortCode = txtSortCode.Text
            mstrEntryMethod = mstrEntryMethod
            mstrAuthNum = mstrAuthNum
        Case CHEQUE_PERSONAL
            If mstrAuthNum <> vbNullString Then
                mstrAccountNo = txtAccountNo.Text
                mstrChequeNo = txtChequeNo.Text
                mstrSortCode = txtSortCode.Text
                mstrEntryMethod = mstrEntryMethod
                mstrAuthNum = mstrAuthNum
            End If
        Case CHEQUE_NOTRANSAX
                mstrChequeNo = "000000"
                mstrEntryMethod = mstrEntryMethod
            
    End Select

End Function 'CaptureCheque


Private Sub cmdCompanyChq_Click()
    
    mstrChequeType = CHEQUE_COMP
    cmdPersonalChq.Value = True

End Sub 'cmdCompanyChq_Click

Private Function ConfirmPrintedOk() As Boolean
        
Dim dblTransactionAmount        As Double
Dim strTransaction              As String
Dim intLen                      As Integer
Dim strActionCode               As String
Dim strAuthorisationNumber      As String
Dim strDateTime                 As String
Dim mstrPosCondition             As String
Dim strResponseCode             As String
Dim strMessageReasonCode        As String
Dim strServiceRestrictionCode   As String
Dim strError                    As String
Dim strEventType                As String
Dim strMsgBoxResponse           As String

    Call DebugMsg(MODULE_NAME, "ConfirmPrintedOK", endlTraceIn, "ConfMode=" & enConfirmationMode & ",EntryMode=" & enEntryMode)
    dblTransactionAmount = mstrTransactionAmount

    Select Case enConfirmationMode
        
        'Confirming that the signature slip has printed ok
        Case encmReprintSlip
            enConfirmationMode = encmSignatureCon
            If Val(mstrTransactionAmount) > (50 * 100) Then
                If (mstrEntryMethod = ENTRY_KEY) And (txtCustPresent.Text = "Y") And (mblnRefund = False) Then
                    Call MsgBoxEx("An imprint of the card must be obtained on a verification voucher for this tender." & _
                        vbCrLf & "Confirm verification completed and signed by customer", vbOKOnly, _
                        "Manual Imprint Required", "Confirm", , , , RGBMsgBox_WarnColour)
                End If
            End If
            'For Customer not present do not confirm Signature, just complete
            If (mblnCNPTill = True) Then
                Call ConfirmPrintedOk
                Exit Function
            End If
            
            If MsgBoxEx("Is the Signature ok?", vbYesNo, "Check Signature", , , , , RGBMsgBox_PromptColour) = vbYes Then
                Call ConfirmPrintedOk
                Exit Function
            Else
'                Call ConfirmPrintedNotOK
            End If
        
        'Confirming that the signature is ok
        Case encmSignatureCon
            
            Select Case enEntryMode
            
                Case encmCheque
                    Dim o As New clsReceiptPrinting
                    Set o.Printer = moOPOSPrinter
                    Set o.goRoot = goRoot
                    Call o.Init(goSession, goDatabase)
                    Call o.PrintChequeFranking(moTranHeader.TranDate, moTranHeader.TransactionTime, goSession.CurrentEnterprise.IEnterprise_StoreNumber, moTranHeader.TillID, moTranHeader.TransactionNo, moTranHeader.CashierID, moTranHeader.TransactionCode, mstrTransactionAmount / 100, mstrCreditCardNum, mstrExpiryDate, , mstrAuthNum, mstrMerchantID)
                    
                    'Check the signature on the cheque
                    If MsgBoxEx("Is the Signature on the cheque ok?", vbYesNo, "Check Signature", , , , , RGBMsgBox_PromptColour) = vbYes Then
                        'mstrEntryMethod = "Cheque"
                        mstrEntryMethod = vbNullString
                        Exit Function
                    Else
                        Load frmVerifyPwd
                        Do
                        Loop Until frmVerifyPwd.VerifySupervisorPassword(ucKeyPad1.Visible) = True
                        Unload frmVerifyPwd
                        mstrAuthNum = vbNullString
                        mstrChequeType = vbNullString
                        txtChequeNo.Text = vbNullString
                        txtSortCode.Text = vbNullString
                        txtAccountNo.Text = vbNullString
                        Call MsgBoxEx("Not Authorised. Present customer with declination card", vbOKOnly _
                            , "Declination Card", , , , , RGBMsgBox_WarnColour)
                        Call Me.Hide
                        Exit Function
                    End If
                    
                Case encmAuthCard
                    DoEvents
                    fraInsertCard.Visible = False
                    lblStatus.Visible = True
                    fraChequeCard.Visible = True
                    fraBorder.Visible = True
                    fraAuthNum.Visible = True
                    txtAuthNum.BackColor = RGB_LTYELLOW
                    fraComplete.Visible = False
                    lblPrintChq.Visible = True
                    lblPrintPrompt.Visible = False
                    'Completes the transaction - online
                    lblStatus.Caption = "Completing Transaction Please Wait"
                    DoEvents
                    Call DebugMsg("frmTender", "cmdConfirm_Click", endlDebug, mstrAuthServerFound)

                    'Send Confirmation
                        
                       
                    DoEvents
                    lblAction.Caption = "Sending Confirmation"
                    lblStatus.Caption = "Sending Confirmation"
                    While mblnSendComplete = False
                        DoEvents
                    Wend
                    Wait (1)
                    mstrData = "BYPASS"
                    lblAction.Caption = "Printing Receipt"
                    lblStatus.Caption = "Printing Receipt"
                    DoEvents
                    'Call PrintCreditCardreceipt - taken out for WIX1180
                    lblStatus.Caption = "Please remove any credit cards from the Pin Pad"
                    DoEvents
                    Call Me.Hide
                    Exit Function
                    
                Case encmKeyed
                    fraInsertCard.Visible = False
                    lblStatus.Visible = True
                    fraChequeCard.Visible = True
                    fraBorder.Visible = True
                    fraAuthNum.Visible = True
                    fraAuthNum.Visible = False
                    txtAuthNum.BackColor = RGB_LTYELLOW
                    fraComplete.Visible = False
                    lblPrintChq.Visible = True
                    lblPrintPrompt.Visible = False
                    DoEvents
                    'Completes the transaction - online
                    lblStatus.Caption = "Completing Transaction Please Wait"
                    DoEvents
                    'Call PrintCreditCardreceipt - taken out for WIX1180

                    'Send Confirmation
                    If mstrTransactionType <> "REFUND" Then

                        mblnSendComplete = False
                        lblAction.Caption = "Sending Confirmation"
                        lblStatus.Caption = "Sending Confirmation"
                        While mblnSendComplete = False
                            DoEvents
                        Wend

                         'Loop until there is a response from the eSocket software
                    End If

                    lblStatus.Caption = "Transaction Complete"
                    Call Me.Hide
                    Exit Function
                
            End Select
            
        Case Else
    
    End Select 'enConfirmationMode
    
End Function 'ConfirmPrintedOk

Private Sub cmdExit_Click()

    cmdExit.SetFocus
    Call Me.Hide

End Sub 'cmdExit_Click

Private Sub cmdNoTransaxChq_Click()
    
    mstrChequeType = CHEQUE_NOTRANSAX
    fraInsertCard.Visible = False
    lblChequeType.Caption = "Cheque Type - No Transax"
    lblChequeType.Visible = True
    Call ChequeNoTransax

End Sub 'cmdNoTransaxChq_Click

Private Sub cmdPersonalChq_Click()

    If (mstrChequeType = CHEQUE_COMP) Then
        fraInsertCard.Visible = False
    Else
        mstrChequeType = CHEQUE_PERSONAL
    End If
    
    fraChequeType.Visible = False
    lblActionRequired.Visible = False
    lblAction.Visible = True
    lblInsert.Visible = False
    fraBorder.Visible = True
    fraComplete.Top = fraChequeCard.Top
    lblAction.Caption = "Enter Cheque Details"
    fraBorder.Visible = False
    fraAccountNo.Visible = True
    fraAccountNo.Top = lblAction.Height + 120
    sbStatus.Visible = True
    Me.Height = fraAccountNo.Top + fraAccountNo.Height + 700 + sbStatus.Height
    txtChequeNo.SetFocus
    
    Call ShowKeyPad
    Call CentreForm(Me)
        
End Sub 'cmdPersonalChq_Click

Private Sub SetUpKeyPad(blnShowNumPad As Boolean)

    mblnUsingNumPad = blnShowNumPad
    If (Me.Visible = False) Or (ucKeyPad1.Visible = False) Then Exit Sub
    If (blnShowNumPad = True) Then
        Call ucKeyPad1.ShowNumpad
        If (mblnProcessCheque = True) Then
            ucKeyPad1.Left = fraInsertCard.Left + fraInsertCard.Width + 120
        Else
            ucKeyPad1.Left = fraEntry.Left + fraEntry.Width + 120
        End If
        ucKeyPad1.Top = 120
    Else
        Call ucKeyPad1.ShowKeyboard
        ucKeyPad1.Left = 0
        ucKeyPad1.Top = fraEntry.Top + fraEntry.Height + sbStatus.Height + 120
    End If
    mblnResizedKeyPad = True
    
End Sub

Private Sub ShowKeyPad()
    
    mblnShowNumPad = Not mblnShowNumPad
    mblnShowNumPad = False
    If (mblnShowNumPad = False) Then
        ucKeyPad1.Visible = False
        If (mblnProcessCheque = True) Then
            Me.Height = 4225
            Me.Width = fraInsertCard.Width + 480
        Else
            Me.Height = fraEntry.Top + lblStatus.Top + lblStatus.Height + 720 + sbStatus.Height
            Me.Width = fraEntry.Width + 480
        End If
    Else
        ucKeyPad1.Visible = True
        If (mblnResizedKeyPad = False) Then Call SetUpKeyPad(mblnUsingNumPad)
        If (mblnUsingNumPad = True) Then
            If (mblnProcessCheque = True) Then
                Me.Width = fraInsertCard.Width + 480 + ucKeyPad1.Width
                Me.Height = ucKeyPad1.Height + sbStatus.Height + 720
            Else
                Me.Width = fraEntry.Width + 600 + ucKeyPad1.Width
                Me.Height = fraEntry.Top + lblStatus.Top + lblStatus.Height + 720 + sbStatus.Height
            End If
        Else
            Me.Width = ucKeyPad1.Width + 60
            Me.Height = ucKeyPad1.Height + sbStatus.Height + fraEntry.Top + fraEntry.Height + 480
        End If
    End If
    Call CentreForm(Me)
    
End Sub

Private Sub CaptureCardDetails()

    fraChequeType.Visible = False
    fraChequeCard.Visible = True
    fraBorder.Visible = True
    fraAuthNum.Visible = True
        
    If (enEntryMode = encmCCard) Or enEntryMode = encmAuthCard Then
        lblAction.Caption = "Card Details"
    Else
        lblAction.Caption = "Enter Cheque Guarantee Card Details"
        lblAction.Font.Size = "18"
    End If
    
    If mstrEntryMethod = ENTRY_KEY Then
        lblAction.Caption = "Enter Card Details"
    End If

End Sub 'CaptureCardDetails

Private Sub Form_Activate()

    Call DebugMsg(MODULE_NAME, "Form_Activate", endlTraceIn)
    If (ucKeyPad1.Visible = True) And (mblnResizedKeyPad = False) Then Call SetUpKeyPad(mblnUsingNumPad)
    If (txtCardNo.Visible = True) And (txtCardNo.Enabled = True) Then txtCardNo.SetFocus
    If (mStartedTransaction = False) Then AuthoriseEFTPayment
    Call DebugMsg(MODULE_NAME, "Form_Activate", endlTraceOut)
    DoEvents
    Me.Hide
    DoEvents

End Sub

Private Sub Form_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Form_Initialize", endlTraceIn)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If mintDuressKeyCode <> 0 And KeyCode = mintDuressKeyCode Then
        KeyCode = 0
        RaiseEvent Duress
    End If

End Sub 'Form_Keydown

Public Sub InitialiseStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    If (goSession Is Nothing = False) Then sbStatus.Panels(PANEL_WSID + 1).Text = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2) & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")
    If (mblnShowKeypadIcon = False) Then sbStatus.Panels("NumPad").Picture = Nothing

End Sub

Private Sub Form_KeyPress(KeyCode As Integer)

    KeyCode = Asc(UCase$(Chr$(KeyCode)))
    
    If (lblInsert.Caption = SELECT_CHEQUE_MSG) And (lblInsert.Visible = True) And (fraAuthNum.Visible = False) Then
        mstrChequeType = ""
        If KeyCode = vbKeyP Then cmdPersonalChq.Value = True
        If KeyCode = vbKeyC Then cmdCompanyChq.Value = True
        If KeyCode = vbKeyN Then cmdNoTransaxChq.Value = True
    End If

    If KeyCode = vbKeyEscape Then
        If ((enEntryMode = encmCheque) And (mblnInitiateCheque = False)) Or (fraAuthNum.Enabled = True) Then
            KeyCode = 0
            mstrEntryMethod = vbNullString
            If (Me.Visible = True) Then
                mlngAttemptNum = mlngAttemptNum + 1
                mstrAuthNum = vbNullString
                txtChequeNo.Text = vbNullString
                txtSortCode.Text = vbNullString
                txtAccountNo.Text = vbNullString
                txtAuthNum.Text = vbNullString
                txtEndMonth.Text = vbNullString
                txtStartMonth.Text = vbNullString
                txtStartYear.Text = vbNullString
                txtEndYear.Text = vbNullString
                txtCardNo.Text = vbNullString
                txtIssueNo.Text = vbNullString
                mblnCustPresent = False
                txtCustPresent.Text = "Y"
                mstrAuthNum = vbNullString
                mstrTrack2 = vbNullString
                mstrTransactionAmount = 0
                mstrChequeType = vbNullString
                Call Me.Hide
                mblnCardDetailsEntered = True
                mblnAuthEntered = True
            End If
        End If
        
    End If
    
End Sub 'form_keypress

Private Sub Form_Load()


'    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
'    RGBMsgBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
'    mlngUseEFTPOS = goSession.GetParameter(PRM_USE_EFTPOS)
'    mstrCaptureCarReg = goSession.GetParameter(PRM_CARREG)
    Call DebugMsg(MODULE_NAME, "Form_Load", endlTraceIn)
mlngBackColor = vbYellow
    RGBEdit_Colour = 16777152

    Me.BackColor = mlngBackColor
    fraEntry.BackColor = Me.BackColor
    fraBorder.Visible = False
    fraChequeType.BackColor = Me.BackColor
    Call InitialiseStatusBar
    Me.Visible = False
    Call DebugMsg(MODULE_NAME, "Form_Load", endlTraceOut)

End Sub 'Form_Load



Private Sub lblStatus_Change()

    Call DebugMsg(MODULE_NAME, "Status Change", endlDebug, "Now-" & lblStatus.Caption)

End Sub

Private Sub sbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)

Dim blnShow As Boolean

    Select Case (Panel.Key)
        Case ("NumPad"):
                    If ((Panel.Picture Is Nothing) = False) Then Call ShowKeyPad
    End Select

End Sub


Private Sub txtCVV_GotFocus()
    
    txtCVV.SelStart = 0
    txtCVV.SelLength = Len(txtCVV.Text)
    txtCVV.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCVV_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Call txtCustPresent_KeyPress(KeyAscii)
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
    End If

    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtCVV_LostFocus()
    
    txtCVV.BackColor = RGB_WHITE
    If (Trim$(txtCVV.Text) <> "") Then txtCVV.Text = Format$(Val(txtCVV.Text), "000")

End Sub

Private Sub ucKeyPad1_Resize()

    ucKeyPad1.Width = ucKeyPad1.Width

End Sub

Private Sub ISnitiate(ByVal blnStartTransaction As Boolean)

Dim strInt                  As String
Dim strTransaction          As String
Dim lngCounter              As Long
Dim intLen                  As Integer
Dim strResponseCode         As String
Dim strMessageReasonCode    As String
Dim strError                As String
Dim strEventType            As String
Dim strRead                 As String
Dim strTillID               As String

    fraInsertCard.Visible = False
    lblStatus.Visible = True
    fraChequeCard.Visible = True
    fraAuthNum.Visible = True
    fraBorder.Visible = True
    fraBorder.Enabled = False
    lblAction.Visible = True
    If (txtCVV.Text = "") Then mstrCreditCardNum = vbNullString
    Call DisableKeyedEntry(False)
    Call ResizeFormLiveMode
    DoEvents
    
    Call DebugMsg("Initiate", vbNullString, endlDebug, "Opening text file c:\eftauth.par")
    strTillID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    'Get terminal ids from eftauth.par
    lblStatus.Visible = True
    fraChequeCard.Visible = True
    fraBorder.Enabled = False
    lblAction.Visible = True
    Call DisableKeyedEntry(False)
    Call ResizeFormLiveMode
    
    fraInsertCard.Visible = True
    fraInsertCard.BackColor = RGBMsgBox_PromptColour
    lblInsert.Caption = ("Please insert \ swipe the card in Pin Pad")
    lblAction.Caption = "Insert Credit Card"
    fraBorder.Visible = False
    DoEvents
        
End Sub 'Initiate

Private Sub txtAccountNo_GotFocus()
    
    txtAccountNo.SelStart = 0
    txtAccountNo.SelLength = Len(txtAccountNo.Text)
    txtAccountNo.BackColor = RGBEdit_Colour

End Sub 'txtAccountNo_GotFocus

Private Sub txtAccountNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtAccountNo.Text) < 7 Then
            Call MsgBoxEx("Account number must be at least 7 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        If Len(txtChequeNo.Text) < 6 Then
            Call MsgBoxEx("Cheque number must be at least 6 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        If Len(txtSortCode.Text) < 6 Then
            Call MsgBoxEx("Sortcode number must be at least 6 digits" & vbCrLf & "Enter valid account number", vbOKOnly, "Invalid Details Entered", , , , , RGBMsgBox_WarnColour)
            Exit Sub
        End If
        If txtAccountNo.Text <> vbNullString Then
            fraAccountNo.Visible = False
            enConfirmationMode = encmSignatureCon
            mstrChequeNo = txtChequeNo.Text
            mstrSortCode = txtSortCode.Text
            mstrAccountNo = txtAccountNo.Text
            enEntryMode = encmCheque
            
            fraInsertCard.Visible = False
            lblStatus.Visible = True
            fraChequeCard.Visible = True
            fraAuthNum.Visible = True
            fraBorder.Visible = True
            fraBorder.Enabled = False
            fraAccountNo.Visible = False
            lblAction.Visible = True
            mstrCreditCardNum = vbNullString
            Call DisableKeyedEntry(False)
            Call ResizeFormLiveMode
            DoEvents
                    
            fraBorder.Enabled = False
            If mstrChequeType <> CHEQUE_COMP Then fraInsertCard.Visible = True
            lblInsert.Caption = ("Please insert \ swipe the Cheque Guarantee card in Pin Pad")
            lblAction.Caption = "Insert Cheque Guarantee Card"
            fraBorder.Visible = False
            
            Call AuthoriseCheque
            Me.Hide
        Else
            Call MsgBoxEx("Unable to continue a cheque transaction without an account number", vbOKOnly, _
                "Unable to contiune", , , , , RGBMsgBox_WarnColour)
            txtAccountNo.SetFocus
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub 'txtAccountNo_KeyPress

Private Sub txtAccountNo_LostFocus()

    txtAccountNo.BackColor = RGB_WHITE

End Sub 'txtAccountNo_LostFocus

Private Sub txtAuthNum_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        'This is what happens if a manual authorisation is accepted
        If Len(txtAuthNum.Text) > 0 Then
            If mstrChequeType <> vbNullString Then
                If Val(txtAuthNum.Text) <= 4 Then
                    Call MsgBoxEx("WARNING: Invalid authorisation number", vbOKOnly, "Invalid Auth Number", _
                        , , , , RGBMsgBox_WarnColour)
                    Exit Sub
                End If
            End If
            If (mstrChequeType <> vbNullString) And (txtCVV.Enabled = True) And (Val(Trim(txtCVV.Text)) = 0) Then
                Call MsgBoxEx("WARNING: Invalid CVV number (must be the 3 Digit Number from the back of the Card)", vbOKOnly, "Invalid CVV Number", _
                        , , , , RGBMsgBox_WarnColour)
                mblnAuthEntered = False
                Exit Sub
            End If
            '02/10/09 - Taken out as request new authorisation after Manual Auth - should just exit (M.Milne)
'            If (txtCVV.Enabled = True) And (Trim(txtCVV.Text) <> "") Then
'                Call txtCustPresent_KeyPress(vbKeyReturn)
'                If (fraBorder.Enabled = True) Then Exit Sub
'            End If
            
            mstrAuthNum = txtAuthNum.Text
            txtAuthNum.BackColor = RGB_LTYELLOW
            fraAuthNum.Enabled = False
            fraBorder.Enabled = False
            lblStatus.Caption = "Sending Confirmation"
            
            'Release control back to loop in tmrESocket
            mblnAuthEntered = True
            DoEvents
        End If
    End If
    
    If (KeyAscii = vbKeyEscape) Then
        'This is what happens if a manual authorisation is declined
        mlngAttemptNum = mlngAttemptNum + 1
        mstrAuthNum = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        txtAccountNo.Text = vbNullString
        txtAuthNum.Text = vbNullString
        txtEndMonth.Text = vbNullString
        txtStartMonth.Text = vbNullString
        txtStartYear.Text = vbNullString
        txtEndYear.Text = vbNullString
        txtCardNo.Text = vbNullString
        txtIssueNo.Text = vbNullString
        mblnCustPresent = False
        txtCustPresent.Text = "Y"
        mstrAuthNum = vbNullString
        mstrTrack2 = vbNullString
        Call Me.Hide
    End If

End Sub 'txtAuthNum_KeyPress
Private Sub txtAuthNum_GotFocus()

    txtAuthNum.BackColor = RGBEdit_Colour

End Sub 'txtAuthNum_GotFocus

Private Sub txtCardNo_GotFocus()

    txtCardNo.SelStart = 0
    txtCardNo.SelLength = Len(txtCardNo.Text)
    txtCardNo.BackColor = RGBEdit_Colour

End Sub 'txtCardNo_GotFocus

Private Sub txtCardNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        mblnCardDetailsEntered = True
        mstrAuthNum = vbNullString
        txtAccountNo.Text = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        Unload Me
'        Call Me.Hide
    End If
    Debug.Print Time$, KeyAscii, Chr$(KeyAscii)
    
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
    
End Sub 'txtCardNo_KeyPress

Private Sub txtCardNo_LostFocus()

        txtCardNo.BackColor = RGB_WHITE
    
End Sub 'txtCardNo_LostFocus

Private Sub txtChequeNo_GotFocus()
    
    txtChequeNo.SelStart = 0
    txtChequeNo.SelLength = Len(txtChequeNo.Text)
    txtChequeNo.BackColor = RGBEdit_Colour

End Sub 'txtChequeNo_GotFocus

Private Sub txtChequeNo_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Val(txtChequeNo.Text) > 0 Then
            While Len(txtChequeNo.Text) < 6
                txtChequeNo.Text = "0" & txtChequeNo.Text
            Wend
            Call SendKeys(vbTab)
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then
        mstrAuthNum = vbNullString
        txtAccountNo.Text = vbNullString
        txtChequeNo.Text = vbNullString
        txtSortCode.Text = vbNullString
        Call Me.Hide
        Me.Height = Me.Height - fraAccountNo.Height
    End If
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0
    
End Sub 'txtChequeNo_KeyPress

Private Sub txtChequeNo_LostFocus()

    txtChequeNo.BackColor = RGB_WHITE

End Sub 'txtChequeNo_LostFocus

Private Sub txtEndMonth_GotFocus()
    
    txtEndMonth.SelStart = 0
    txtEndMonth.SelLength = Len(txtEndMonth.Text)
    txtEndMonth.BackColor = RGBEdit_Colour

End Sub 'txtEndMonth_GotFocus

Private Sub txtEndMonth_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub 'txtEndMonth_KeyPress

Private Sub txtEndMonth_LostFocus()

    txtEndMonth.BackColor = RGB_WHITE
    txtEndMonth.Text = Format$(Val(txtEndMonth.Text), "00")
    If (Val(txtEndMonth.Text) <= 0) Or (Val(txtEndMonth.Text) > 12) Then txtEndMonth.Text = "--"

End Sub 'txtEndMonth_LostFocus

Private Sub txtEndYear_GotFocus()
    
    txtEndYear.SelStart = 0
    txtEndYear.SelLength = Len(txtEndYear.Text)
    txtEndYear.BackColor = RGBEdit_Colour

End Sub 'txtEndYear_GotFocus

Private Sub txtEndYear_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub 'txtEndYear_KeyPress

Private Sub txtEndYear_LostFocus()

    txtEndYear.BackColor = RGB_WHITE
    txtEndYear.Text = Format$(Val(txtEndYear.Text), "00")
    If (Val(txtEndYear.Text) <= 0) Or (Val(txtEndYear.Text) > 99) Then txtEndYear.Text = "--"

End Sub 'txtEndYear_LostFocus

Private Sub txtCustPresent_GotFocus()

    txtCustPresent.SelStart = 0
    txtCustPresent.SelLength = Len(txtCustPresent.Text)
    txtCustPresent.BackColor = RGBEdit_Colour
 
End Sub 'txtCustPresent_GotFocus

Private Sub txtIssueNo_GotFocus()
    
    txtIssueNo.SelStart = 0
    txtIssueNo.SelLength = Len(txtIssueNo.Text)
    txtIssueNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtIssueNo_LostFocus()

'    If LenB(txtIssueNo.Text) = 0 Then txtIssueNo.Text = "00"
'    txtIssueNo.Text = Format$(txtIssueNo.Text, "00")

    txtIssueNo.BackColor = RGB_WHITE

End Sub

Private Sub txtCustPresent_KeyPress(KeyAscii As Integer)
    
Dim strSQL          As String
Dim strDSN          As String

'    strDSN = goDatabase.ConnectionString

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        mblnCardDetailsEntered = False
        DoEvents
        mstrExpiryDate = txtEndMonth.Text & txtEndYear.Text
        If ValidateExpiryDate(mstrExpiryDate) = False Then
            Call MsgBoxEx("Please enter a valid expiry date", vbOKOnly, "Invalid Expiry Date", , , , , RGBMsgBox_WarnColour)
            txtCardNo.SetFocus
            Exit Sub
        End If
        If (Trim$(txtCVV.Text) = "") And (txtCVV.Enabled = True) Then
            Call MsgBoxEx("Please enter a valid CVV number from the Card", vbOKOnly, "Invalid CVV", , , , , RGBMsgBox_WarnColour)
            txtCVV.SetFocus
            Exit Sub
        End If

        KeyAscii = 0
        mblnCardDetailsEntered = True
        fraBorder.Enabled = False
        
        mstrStartDate = txtStartMonth.Text & txtStartYear.Text
        mstrExpiryDate = txtEndMonth.Text & txtEndYear.Text
        mstrCreditCardNum = txtCardNo.Text
'        mstrCardSequenceNum = IIf(Val(txtIssueNo.Text) = 0, "", txtIssueNo.Text)
        mstrCardSequenceNum = txtIssueNo.Text
        If txtCustPresent = "Y" Then
            mblnCustPresent = True
        Else
            mblnCustPresent = False
        End If

        If (Trim$(txtCVV.Text) <> "") Then
            mstrEntryMethod = ENTRY_KEY
            Call PerformAuth(mTranAmount, True, mIsSale, mblnCustPresent, mAllowCashback, False, mTranResult)
            mblnCardDetailsEntered = True
        End If

        'Release control back to loop in tmrESocket
        DoEvents
    End If

    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtIssueNo_KeyPress(KeyAscii As Integer)

    If (mblnCNPTill = False) Then fraAuthNum.Enabled = False

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub


Private Sub txtSortCode_GotFocus()
    
    txtSortCode.SelStart = 0
    txtSortCode.SelLength = Len(txtSortCode.Text)
    txtSortCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtSortCode_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        If Len(txtSortCode.Text) < 6 Then
            Call MsgBoxEx("The Sort Code must be six characters long.", vbOKOnly, "Incorrect SortCode", , , , , RGBMsgBox_WarnColour)
        Else
            Call SendKeys(vbTab)
        End If
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
    If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtSortCode_LostFocus()

    txtSortCode.BackColor = RGB_WHITE

End Sub

Private Sub txtStartMonth_GotFocus()
    
    txtStartMonth.SelStart = 0
    txtStartMonth.SelLength = Len(txtStartMonth.Text)
    txtStartMonth.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStartMonth_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")
If (KeyAscii < vbKey0 Or KeyAscii > vbKey9) And KeyAscii <> "8" Then KeyAscii = 0

End Sub

Private Sub txtStartMonth_LostFocus()

    txtStartMonth.BackColor = RGB_WHITE
    txtStartMonth.Text = Format$(Val(txtStartMonth.Text), "00")
    If (Val(txtStartMonth.Text) <= 0) Or (Val(txtStartMonth.Text) > 12) Then txtStartMonth.Text = "--"

End Sub

Private Sub txtStartYear_GotFocus()

    txtStartYear.SelStart = 0
    txtStartYear.SelLength = Len(txtStartYear.Text)
    txtStartYear.BackColor = RGBEdit_Colour

End Sub

Private Sub txtStartYear_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub txtStartYear_LostFocus()

    txtStartYear.BackColor = RGB_WHITE
    txtStartYear.Text = Format$(Val(txtStartYear.Text), "00")
    If (Val(txtStartYear.Text) <= 0) Or (Val(txtStartYear.Text) > 99) Then txtStartYear.Text = "--"

End Sub


Public Sub TypeCCDetails()

    'Stop the timer which calls modPayware
    fraInsertCard.Visible = False
    fraChequeCard.Visible = True
    fraBorder.Visible = True
    fraAuthNum.Visible = True
    fraAuthNum.Enabled = False
    enEntryMode = encmAuthCard
    txtIssueNo.BackColor = vbWhite

    lblAction.Visible = True
    'Displays a label if the authorisation server is in test mode
    If mstrTmode = "mTest" Then
        lblAuthTranMod.Visible = True
        lblAuthTranMod.Caption = "* * * TEST MODE * * *"
        Call ResizeFormTestMode 'Resizes form
    Else
        Call ResizeFormLiveMode 'Resizes form
    End If
    DoEvents
    Call CaptureCardDetails

End Sub 'TypeCCDetails

Public Sub PrintPOSSignatureSlip()

Dim dblAmount   As Double
Dim strVerified As String
    
    Call DebugMsg(Me.Name, "PrintSignatureSlip", endlTraceIn)
    dblAmount = mstrTransactionAmount / 100

mstrSignatureRequired = "TRUE"

    If mstrSignatureRequired = "TRUE" Then
        If Val(mstrFallBackType) = "1" Then mstrEntryMethod = "Swipe Fallback"
        Dim mstrPanEntryMode As String
        Call SetEntryMethod

        Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
        moTranHeader.TransactionNo, IIf(mIsSale, "Sale", "Refund"), _
        moTranHeader.CashierID, mstrCashierName, _
        vbNullString)

        Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
            mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, False, _
            mstrEmvTerminalVerificationResult, mstrEmvTransactionStatusInformation, mstrEmvAuthorizationResponseCode, _
            mstrEmvUnpredictableNumber, mstrEmvApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, _
            mstrChequeType, mstrIACDefault, mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)
        
        Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, True, mstrAuthNum, _
            mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, mblnCNPTill)
    Else

        Select Case mstrPanEntryMode
            Case "05"

                If mstrSignatureRequired <> "TRUE" Then 'Signature slip not required
                    If Val(mstrFallBackType) = "1" Then
                        mstrEntryMethod = "Swipe Fallback"
                    Else
                        SetEntryMethod
                    End If
                    If (mblnPRM_SHOW_EFT_DATA = True) Then
                        Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
                        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
                        moTranHeader.TransactionNo, IIf(mIsSale, "Sale", "Refund"), _
                        moTranHeader.CashierID, mstrCashierName, _
                        vbNullString)
    
                        Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
                            mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, False, _
                            mstrEmvTerminalVerificationResult, mstrEmvTransactionStatusInformation, mstrEmvAuthorizationResponseCode, _
                            mstrEmvUnpredictableNumber, mstrEmvApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, _
                            mstrChequeType, mstrIACDefault, mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)
    
                        Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, False, mstrAuthNum, _
                            mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, False)
                    End If 'print extra slip
        
                    enEntryMode = encmAuthCard
                    enConfirmationMode = encmSignatureCon
                    Call ConfirmPrintedOk
                    DoEvents
                    Call DebugMsg(MODULE_NAME, "PrintSignatureSlip", endlDebug, "Exiting Auth")
                    Exit Sub
                End If 'Signature slip not required

            Case Else

                    If Val(mstrFallBackType) = "1" Then
                        mstrEntryMethod = "Swipe Fallback"
                    Else
                        SetEntryMethod
                    End If
                    Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
                            moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
                            moTranHeader.TransactionNo, IIf(mIsSale, "Sale", "Refund"), _
                            moTranHeader.CashierID, mstrCashierName, _
                            vbNullString)

                    Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
                        mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, False, _
                        vbNullString, vbNullString, vbNullString, vbNullString, vbNullString, mstrTID, mstrMerchantID, vbNullString, _
                        mstrChequeType, mstrIACDefault, mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)

                    Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, True, mstrAuthNum, _
                        mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, mblnCNPTill)

        End Select
    End If

    
End Sub 'PrintSignatureSlip

Private Sub SetEntryMethod()
    
    Select Case mstrTransactionSource
        Case "1", "2", "3", "5", "7"
            mstrEntryMethod = ENTRY_KEY
        Case "0", "4"
            mstrEntryMethod = ENTRY_SWIPE
        Case "i"
            mstrEntryMethod = ENTRY_ICCP
        Case Else
            mstrEntryMethod = "ER(" & mstrTransactionSource & ")"
    End Select
    mstrEntryMethod = mstrEntryMethod & "|"
    'Extract the Verification method
    Select Case UCase(mstrAuthType)
        Case "SIGNATURE":   mstrEntryMethod = mstrEntryMethod & "Signature"
        Case "PIN":         mstrEntryMethod = mstrEntryMethod & "PIN"
        Case "NOT_PERFORMED": mstrEntryMethod = mstrEntryMethod & "Not"
        Case "PIN_AND_SIGNATURE": mstrEntryMethod = mstrEntryMethod & "PIN&Signature"
        Case "FAILED": mstrEntryMethod = mstrEntryMethod & "Failed"
        Case "UNKNOWN", "": mstrEntryMethod = mstrEntryMethod & "Unknown"
    End Select

End Sub

Private Sub PrintCreditCardreceipt()

Dim dblAmount As Double
Dim strStage  As String
    
    On Error GoTo Err_Print_CCReceipt
    
    strStage = "Starting"
    dblAmount = Val(mstrTransactionAmount) / 100
    
    strStage = "PrintHeader-" & mstrCashierName
    Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
        moTranHeader.TransactionNo, IIf(mIsSale, "Sale", "Refund"), _
        moTranHeader.CashierID, mstrCashierName, _
        vbNullString)

    lblStatus.Caption = "Printing Receipt-Details"
    DoEvents
    strStage = "PrintDetails-" & mstrCardProductName
    Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
            mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, True, _
            mstrTerminalVerificationResult, mstrTransactionStatusInfo, mstrAuthResponceCode, mstrUnpredictableNumber, _
            mstrApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, mstrChequeType, mstrIACDefault, _
            mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)
    
    lblStatus.Caption = "Printing Receipt-Slip"
    DoEvents
    strStage = "PrintingSlip-" & mstrCardProductName
    Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, False, mstrAuthNum, _
        mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, False)
        
    Exit Sub

Err_Print_CCReceipt:

    Call MsgBoxEx("WARNING : An error has occurred when printing Credit Card Receipt - system will attempt to continue to complete transaction", vbOKOnly, "Print Error")
    Call Err.Report(MODULE_NAME, "PrintCreditCardReceipt-" & strStage, 1, False)
    Call Err.Clear

End Sub 'PrintCreditCardreceipt

Private Sub DisableKeyedEntry(DisableAuth As Boolean)

    
    fraBorder.Enabled = Not DisableAuth
    DoEvents
    txtEndMonth.BackColor = RGB_LTYELLOW
    txtEndYear.BackColor = RGB_LTYELLOW
    txtStartMonth.BackColor = RGB_LTYELLOW
    txtStartYear.BackColor = RGB_LTYELLOW
    txtCardNo.BackColor = RGB_LTYELLOW
    txtIssueNo.BackColor = RGB_LTYELLOW
    txtCustPresent.BackColor = RGB_LTYELLOW
    If (DisableAuth = False) Then
        txtEndMonth.Enabled = False
        txtEndYear.Enabled = False
        txtStartMonth.Enabled = False
        txtStartYear.Enabled = False
        txtCardNo.Enabled = False
        txtIssueNo.Enabled = False
        txtCustPresent.Enabled = False
    End If

End Sub 'DisableKeyedEntry

Private Sub EnableKeyedEntry()

    txtEndMonth.BackColor = RGB_WHITE
    txtEndYear.BackColor = RGB_WHITE
    txtStartMonth.BackColor = RGB_WHITE
    txtStartYear.BackColor = RGB_WHITE
    txtCardNo.BackColor = RGB_WHITE
    txtIssueNo.BackColor = RGB_WHITE
    txtCustPresent.BackColor = RGB_WHITE
    txtEndMonth.Enabled = True
    txtEndYear.Enabled = True
    txtStartMonth.Enabled = True
    txtStartYear.Enabled = True
    txtCardNo.Enabled = True
    txtIssueNo.Enabled = True
    txtCustPresent.Enabled = True
    fraBorder.Enabled = True
    DoEvents

End Sub 'EnableKeyedEntry

Public Function ConfirmCardRead() As Boolean

    txtEndMonth.Text = Format$(Val(txtEndMonth.Text), "00")
    If Val(txtStartMonth.Text) < 0 Then
        txtStartMonth.Text = Format$(Val(txtStartMonth.Text), "00")
        Exit Function
    End If
        
    If LenB(txtCardNo.Text) = 0 Then
        Call MsgBoxEx("Invalid card detected: No Credit card number" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: No Card number", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    If IsNumeric(txtCardNo.Text) = False Then
        Call MsgBoxEx("Invalid card detected: Invalid Credit card number" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Invalid Card number", , , , , RGBMsgBox_WarnColour)
        Exit Function
    End If
    
    If Val(txtEndYear.Text & txtEndMonth.Text) > 0 Then
        
        If Val(txtEndYear.Text & txtEndMonth.Text) < Format$(Date, "YYMM") Then
            Call MsgBoxEx("Invalid card detected: Card has expired" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Card Expired", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
        
        If (txtStartYear.Text & txtStartMonth.Text) > (txtEndYear & txtEndMonth) Then
            Call MsgBoxEx("Invalid card detected: Start date is after expiry date" & vbCrLf & vbCrLf & "Please select an alternative tender type ", vbOKOnly, "Warning: Bad start date", , , , , RGBMsgBox_WarnColour)
            Exit Function
        End If
    End If
    
    ConfirmCardRead = True
    
End Function 'ConfirmCardRead

Private Sub ResizeFormTestMode()

'Resizes the form accordingly when the authorisation server is set to test mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + lblAuthTranMod.Height + 220
    lblStatus.Top = fraBorder.Top + fraBorder.Height + 120
    Me.Height = lblAction.Top + lblAction.Height + lblAuthTranMod.Height + lblStatus.Height + fraBorder.Height + 850 + sbStatus.Height
    DoEvents

End Sub 'ResizeFormTestMode

Private Sub ResizeFormLiveMode()

'Resizes the form accordingly when the authorisation server is set to live mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + 120
    lblStatus.Top = fraBorder.Top + fraBorder.Height + 120
    Me.Height = lblAction.Top + lblAction.Height + lblStatus.Height + fraBorder.Height + 850 + sbStatus.Height
    DoEvents

End Sub 'ResizeFormLiveMode

Private Sub ResizeFormChequeMode()

'Resizes the form accordingly when the authorisation server is set to live mode

    fraBorder.Height = fraChequeCard.Height + fraAuthNum.Height + 12
    fraBorder.Top = lblAction.Top + lblAction.Height + 120
    fraChequeCard.Top = 120
    fraAuthNum.Top = fraChequeCard.Height - 12
    fraAuthNum.Visible = False
    Me.Height = lblAction.Top + lblAction.Height + lblStatus.Height + fraBorder.Height + 850 + sbStatus.Height
    DoEvents

End Sub 'ResizeFormLiveMode

Private Sub PrintChequeReceipt()

Dim dblAmount As Double
    
    dblAmount = mstrTransactionAmount / 100
    
    Call modReceipt.PrintTransactionHeader(moOPOSPrinter, _
        moTranHeader.TranDate, moTranHeader.TransactionTime, moTranHeader.TillID, _
        moTranHeader.TransactionNo, IIf(mIsSale, "Sale", "Refund"), _
        moTranHeader.CashierID, mstrCashierName, _
        vbNullString)

    Call PrintCreditCardDetails(moOPOSPrinter, mstrCardProductName, mstrCreditCardNum, _
        mstrStartDate, mstrExpiryDate, dblAmount, mstrEntryMethod, mstrAuthNum, mstrCardSequenceNum, True, _
        mstrTerminalVerificationResult, mstrTransactionStatusInfo, mstrAuthResponceCode, mstrUnpredictableNumber, _
        mstrApplicationIdentifier, mstrTID, mstrMerchantID, mstrEmvCryptogram, mstrChequeType, mstrIACDefault, _
        mstrIACDenial, mstrIACOnline, mstrEmvCvmResults)

    Call modReceipt.PrintCreditCardSlip(moOPOSPrinter, False, mstrAuthNum, _
        mstrCardProductName, mstrCreditCardNum, mstrExpiryDate, dblAmount, False)

End Sub 'PrintChequeReceipt

Private Sub AuthoriseCheque()

    Call CentreForm(Me)
    Call PerformAuth(mTranAmount, False, True, True, False, True, mTranResult)
    
End Sub 'AuthoriseCheque

Private Sub txtCustPresent_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyTab Then Call txtCustPresent_KeyDown(vbKeyReturn, 0)

End Sub

Private Function ValidateExpiryDate(ByVal strExpiryDate) As Boolean

    ValidateExpiryDate = True
    strExpiryDate = Replace(strExpiryDate, "-", "")
    If strExpiryDate = vbNullString Then ValidateExpiryDate = False
    If Len(strExpiryDate) <> 4 Then ValidateExpiryDate = False
    strExpiryDate = Right(strExpiryDate, 2) & Left$(strExpiryDate, 2)
    If (Val(strExpiryDate) <= Val(Format(Date, "YYMM"))) Then ValidateExpiryDate = False
    
End Function 'ValidateExpiryDate

Private Sub ChequeNoTransax()

'    Call MsgBoxEx("Warning transaction amount exceeds the account credit limit" & _
'        vbCrLf & "Please enter a supervisor password", vbOKOnly, "Transaction Exceeds Account Credit Limit", _
'        , , , , RGBMsgBox_WarnColour)
    If frmVerifyPwd.VerifyEFTPassword(ucKeyPad1.Visible) = True Then
        Call Me.Hide
        lblAction.Caption = "Printing"
        lblStatus.Caption = "Printing"
        enEntryMode = encmCheque
        enConfirmationMode = encmSignatureCon
        
        Call ConfirmPrintedOk
        mstrAuthNum = "NO-TRNSX"
        mTranResult.AuthCode = mstrAuthNum
        mTranResult.BankSortCode = "000000"
        mTranResult.ChequeAccountNumber = "00000000"
        mTranResult.ChequeNumber = "000000"
        lblStatus.Caption = "Cheque Complete Successful"
    Else
        fraInsertCard.Visible = True
        lblChequeType.Visible = False
    End If

End Sub 'ChequeNoTransax
Public Sub RetrieveTracks(Track1 As String, Track2 As String, Track3 As String)

Dim PayObject As COMLink.Pay
Dim NotValidResponse As Boolean

On Error GoTo RetrieveTracks_Error

    Set PayObject = New Pay
    PayObject.Connection.DebugFilePath = mstrLoggingPath
    DoEvents
    PayObject.Connection.DebugLoggingEnabled = mblnLogEFT
    
    PayObject.Connection.RemoteHost = mstrEFTHost
    PayObject.Connection.RemotePort = mstrEFTHostPort
    PayObject.Connection.timeOut = mlngEFTSwipeTimeout
    PayObject.Connection.InterPacketDelay = 1000
    
    PayObject.Connection.CommsMethod = CommsXML
    PayObject.Trans.TransactionNumber = Format(Time, "HHNNSS")
    PayObject.Trans.Wait = True
    
    PayObject.Trans.SourceTerminalIdentifier = "8099"
    PayObject.Connection.SecureMode = 0

    Call PayObject.StatusResponse(CancelResponse) 'Added this to send cancel previous Transaction
    
    NotValidResponse = True
    
    lblAction.Caption = "Insert Credit Card"
    lblInsert.Caption = ("Please insert \ swipe the card in Pin Pad")
    lblStatus.Caption = "Validate Card"
    PayObject.Trans.TransactionSource = "i"
    DoEvents
    
    PayObject.GetCardDetails
    
    Track1 = PayObject.Trans.Card.Track1Details
    Track2 = PayObject.Trans.Card.Track2Details
    Track3 = PayObject.Trans.Card.Track3Details
    
    PayObject.Connection.DebugLoggingEnabled = False
    
    Set PayObject = Nothing
    
    Exit Sub
    
RetrieveTracks_Error:
    If (Err.Number <> -2147219983) Then
        Call MsgBox("Error has occurred when Retrieving Tracks" & vbNewLine & _
            Err.Number & ":" & Err.Description, vbCritical, "Error Occurred")
    Else
        If MsgBoxEx("Card reader has timed out before reading card." & vbNewLine & "Do you want to retry?", vbYesNo, "Card Reader Timed Out.", , , , , RGBMsgBox_PromptColour) = vbYes Then
            PayObject.Connection.DebugLoggingEnabled = False
            Set PayObject = Nothing
            Call RetrieveTracks(Track1, Track2, Track3)
            Exit Sub
        End If
    End If
    Call Err.Clear
    Resume Next 'tidy up
    
End Sub




