Attribute VB_Name = "modLogicTender"
Option Explicit

Public Const MODULE_NAME As String = "LogicTender"

Public Const PRM_CARDNO_MASK      As Long = 926

Public Const PRM_EFT_LOGGING As Long = 970 'Y/N,path to log to
Public Const PRM_EFT_PRINT_DEBUG As Long = 971
Public Const PRM_EFT_HOST As Long = 972 'as IP address and Port 127.0.0.1:9800
Public Const PRM_EFT_TIMEOUT As Long = 973
Public Const PRM_EFT_CV2_SUCCESS As Long = 975
Public Const PRM_EFT_SRC_TERM_ID As Long = 977
Public Const PRM_EFT_SWIPE_TIMEOUT As Long = 978 'added WIX1373 to split timeout when reading discount card

Public Const PRM_COUNTRY_CODE As Integer = 148 'WIX1312 - add in Country Code parameter



Public Const PANEL_VERNO As Integer = 2
Public Const PANEL_INFO As Integer = 3
Public Const PANEL_WSID As Integer = 4
Public Const PANEL_DATE As Integer = 5


Public Const RGBMsgBox_WarnColour As Long = vbRed
Public Const RGBMsgBox_PromptColour As Long = vbYellow
Public Const RGB_WHITE As Long = vbWhite

Public mblnPRM_SHOW_EFT_DATA As Boolean

Public goSession As Object
Public goRoot As Object
Public goDatabase As Object


Public mstrLoggingPath As String
Public mblnLogEFT      As Boolean
Public mstrEFTHost     As String
Public mstrEFTHostPort As String
Public mlngEFTTimeout  As Long
Public mlngEFTSwipeTimeout  As Long 'WIX1373-Read Card Timeout
Public mblnDebugPrint  As Boolean
Public mstrHeadOffice  As String
Public mstrStoreName   As String
Public mstrCardMask    As String
Public mstrCV2Success  As String
Public mstrSourceTerminalID  As String

Public mblnCNPTill  As Boolean


Public mblnShowKeypadIcon  As Boolean

Public mlngBackColor As Long
Public mintDuressKeyCode As Integer

Public RGBEdit_Colour As Long
Public mstrMaxValidCashier As String

Public mstrDebugPrintInfo As String

Public mstrPOSPrintSlip As String
Public mCurrChar As String

Public Function CentreForm(ByRef oForm As Form)

    If oForm.WindowState <> vbMaximized Then
        oForm.Left = (Screen.Width - oForm.Width) / 2
        oForm.Top = (Screen.Height - oForm.Height) / 2
    End If

End Function

Public Function InitialiseSettings()

Dim oFSO    As New FileSystemObject
Dim tsConfig As TextStream
Dim strTag   As String
Dim strData  As String

    Call DebugMsg(MODULE_NAME, "InitialiseSettings", endlTraceIn, "Session is " & (goSession Is Nothing))
    If (goSession Is Nothing) Then
        'Use Config File
        On Error Resume Next
        Set tsConfig = oFSO.OpenTextFile(App.Path & "\CTSLogic.config", ForReading, False)
        If (Err.Number <> 0) Then
            Call MsgBoxEx("Error loading CTS Logic configuration file from " & App.Path & "\CTSLogic.config", vbOKOnly, "Configuration Error")
            Exit Function
        End If
        
        While (Not tsConfig.AtEndOfStream)
            strData = Trim(tsConfig.ReadLine)
            strTag = Mid$(strData, 2)
            strTag = Left$(strTag, InStr(strTag, ">") - 1)
            strData = Mid$(strData, Len(strTag) + 3)
            If (InStr(strData, "</") > 0) Then
                strData = Left$(strData, InStr(strData, "</") - 1)
            End If
            Select Case (strTag)
                Case ("DebugPath"): mstrLoggingPath = strData
                Case ("DebugEFT"): mblnLogEFT = (strData = "Y")
                Case ("PrintDebug"): mblnDebugPrint = (strData = "Y")
                Case ("RemoteHost"): mstrEFTHost = strData
                Case ("RemotePort"): mstrEFTHostPort = strData
                Case ("TimeOut"):   mlngEFTTimeout = Val(strData)
                                    mlngEFTSwipeTimeout = mlngEFTTimeout
                Case ("ReceiptAddress"): mstrHeadOffice = Replace(strData, "|", vbCrLf & "      ")
                Case ("StoreName"): mstrStoreName = strData
                Case ("CardMask"):  mstrCardMask = strData
                Case ("CV2Success"): mstrCV2Success = strData
                Case ("CountryCode"):
                    If (strData = "IE") Then
                        mCurrChar = Chr(128)
                    Else
                        mCurrChar = ChrW(163)
                    End If
                Case ("SourceTerminalID"): mstrSourceTerminalID = strData
            End Select
        Wend
        tsConfig.Close
    Else
        'Use Session parameters
        mstrLoggingPath = Mid$(goSession.GetParameter(PRM_EFT_LOGGING), 3)
        mblnLogEFT = Left$(goSession.GetParameter(PRM_EFT_LOGGING), 1) = "Y"
        mblnDebugPrint = goSession.GetParameter(PRM_EFT_PRINT_DEBUG)
        mstrEFTHost = goSession.GetParameter(PRM_EFT_HOST)
        mstrEFTHostPort = Mid$(mstrEFTHost, InStr(mstrEFTHost, ":") + 1)
        mstrEFTHost = Left$(mstrEFTHost, Len(mstrEFTHost) - Len(mstrEFTHostPort) - 1)
        Select Case (goSession.GetParameter(PRM_COUNTRY_CODE))
            Case ("IE"): mCurrChar = Chr(128)
            Case Else
                mCurrChar = ChrW(163)
        End Select
        mstrCV2Success = goSession.GetParameter(PRM_EFT_CV2_SUCCESS)
        mlngEFTTimeout = goSession.GetParameter(PRM_EFT_TIMEOUT)
        mlngEFTSwipeTimeout = goSession.GetParameter(PRM_EFT_SWIPE_TIMEOUT)
        mstrSourceTerminalID = goSession.GetParameter(PRM_EFT_SRC_TERM_ID)
        Dim oSysOpt As cEnterprise_Wickes.cSystemOptions
        Set oSysOpt = goSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
        Call oSysOpt.LoadMatches
        
        mstrHeadOffice = oSysOpt.HeadOfficeName & vbNewLine
        mstrHeadOffice = mstrHeadOffice & oSysOpt.HeadOfficeAddress1 & vbNewLine
        mstrHeadOffice = mstrHeadOffice & oSysOpt.HeadOfficeAddress2 & vbNewLine
        mstrHeadOffice = mstrHeadOffice & oSysOpt.HeadOfficeAddress3 & vbNewLine
        mstrHeadOffice = mstrHeadOffice & oSysOpt.HeadOfficeAddress4 & vbNewLine
        mstrHeadOffice = mstrHeadOffice & oSysOpt.HeadOfficeAddress5 & vbNewLine
        mstrHeadOffice = mstrHeadOffice & oSysOpt.HeadOfficePostCode
        mstrHeadOffice = Replace(mstrHeadOffice, vbNewLine & vbNewLine, vbNewLine)
        mstrHeadOffice = Replace(mstrHeadOffice, vbNewLine, vbNewLine & "      ")
        
        mstrStoreName = oSysOpt.StoreName
        mstrCardMask = goSession.GetParameter(PRM_CARDNO_MASK)
    End If
    Call DebugMsg(MODULE_NAME, "InitialiseSettings", endlTraceOut)

End Function


Public Function MaskCreditCardNo(ByVal strCardNo As String) As String

Dim lngCharPos  As Long
Dim strChar     As String
Dim lngFirstPos As Long
Dim lngLastPos  As Long

    If (mstrCardMask <> "") Then
        If (InStr(mstrCardMask, ":") = 0) Then
            'Apply mask directly
            For lngCharPos = 1 To Len(mstrCardMask) Step 1
                strChar = Mid$(mstrCardMask, lngCharPos, 1)
                If (strChar <> "0") Then strCardNo = Left$(strCardNo, lngCharPos - 1) & strChar & Mid$(strCardNo, lngCharPos + 1)
            Next lngCharPos
        Else
            'apply mask only to first and last characters only
            For lngCharPos = 1 To Len(mstrCardMask) Step 1
                strChar = Mid$(mstrCardMask, lngCharPos, 1)
                If (strChar = "0") And (lngFirstPos > 0) And (lngLastPos = 0) Then lngLastPos = lngCharPos
                If (strChar <> "0") And (lngFirstPos = 0) And (lngLastPos = 0) Then lngFirstPos = lngCharPos - 1
            Next lngCharPos
            lngLastPos = Len(mstrCardMask) - lngLastPos + 1
            If (Len(mstrCardMask) - 3 < Len(strCardNo)) Then
                strCardNo = Left$(strCardNo, lngFirstPos) & String(Len(strCardNo) - (Len(mstrCardMask) - 3), Mid$(mstrCardMask, lngFirstPos + 1, 1)) & Right$(strCardNo, lngLastPos)
            End If
        End If
    End If
    MaskCreditCardNo = Right$(String$(19, "0") & strCardNo, 19)

End Function


