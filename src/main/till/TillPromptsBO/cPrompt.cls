VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPrompts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : cPrompt
'* Date   : 07/5/04
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/cOPEventsBO/cPrompt.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/05/04 12:19 $ $Revision: 2 $
'* Versions:
'* 07/05/04    mauricem
'*             Initial Version
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME = "cPrompt"

Implements IBo
Implements ISysBo

Private mPromptID As String

Private mPromptType As String

Private mImagePath As String

Private mScreenText1 As String

Private mScreenText2 As String

Private mScreenText3 As String

Private mScreenText4 As String

Private mPrintText1 As String

Private mPrintText2 As String

Private mPrintText3 As String

Private mPrintText4 As String

Private mRejectedText1 As String

Private mRejectedText2 As String

Private mDataItem As String

Private mDataGroup As String

Private mDataCondition As String

Private mSupervisorAuth As Boolean

Private mManagerAuth As Boolean

Private mCollectInfo As String

Private mAllowSale As Boolean

Private mPromptGroup As Byte

Private mPromptValue As String

Private mResponseType As String

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

Public Property Get PromptGroup() As Byte
    Let PromptGroup = mPromptGroup
End Property

Public Property Let PromptGroup(ByVal Value As Byte)
    Let mPromptGroup = Value
End Property

Public Property Get PromptValue() As String
    Let PromptValue = mPromptValue
End Property

Public Property Let PromptValue(ByVal Value As String)
    Let mPromptValue = Value
End Property

Public Property Get AllowSale() As Boolean
    Let AllowSale = mAllowSale
End Property

'##ModelId=4098E35100B7
Public Property Let AllowSale(ByVal Value As Boolean)
    Let mAllowSale = Value
End Property

Public Property Get CollectInfo() As String
    Let CollectInfo = mCollectInfo
End Property

'##ModelId=4098E35100B7
Public Property Let CollectInfo(ByVal Value As String)
    Let mCollectInfo = Value
End Property

Public Property Get ManagerAuth() As Boolean
    Let ManagerAuth = mManagerAuth
End Property

'##ModelId=4098E35100B7
Public Property Let ManagerAuth(ByVal Value As Boolean)
    Let mManagerAuth = Value
End Property

Public Property Get SupervisorAuth() As Boolean
    Let SupervisorAuth = mSupervisorAuth
End Property

'##ModelId=4098E35100B7
Public Property Let SupervisorAuth(ByVal Value As Boolean)
    Let mSupervisorAuth = Value
End Property

Public Property Get DataCondition() As String
    Let DataCondition = mDataCondition
End Property

'##ModelId=4098E35100B7
Public Property Let DataCondition(ByVal Value As String)
    Let mDataCondition = Value
End Property

Public Property Get DataGroup() As String
    Let DataGroup = mDataGroup
End Property

'##ModelId=4098E35100B7
Public Property Let DataGroup(ByVal Value As String)
    Let mDataGroup = Value
End Property

'##ModelId=4098E3500369
Public Property Get DataItem() As String
    Let DataItem = mDataItem
End Property

'##ModelId=4098E35001B0
Public Property Let DataItem(ByVal Value As String)
    Let mDataItem = Value
End Property

'##ModelId=4098E350007A
Public Property Get PrintText4() As String
    Let PrintText4 = mPrintText4
End Property

'##ModelId=4098E34F02B3
Public Property Let PrintText4(ByVal Value As String)
    Let mPrintText4 = Value
End Property

'##ModelId=4098E34F017D
Public Property Get PrintText3() As String
    Let PrintText3 = mPrintText3
End Property

'##ModelId=4098E34E03C0
Public Property Let PrintText3(ByVal Value As String)
    Let mPrintText3 = Value
End Property

'##ModelId=4098E350007A
Public Property Get PrintText2() As String
    Let PrintText2 = mPrintText2
End Property

'##ModelId=4098E34F02B3
Public Property Let PrintText2(ByVal Value As String)
    Let mPrintText2 = Value
End Property

'##ModelId=4098E34F017D
Public Property Get PrintText1() As String
    Let PrintText1 = mPrintText1
End Property

'##ModelId=4098E34E03C0
Public Property Let PrintText1(ByVal Value As String)
    Let mPrintText1 = Value
End Property

'##ModelId=4098E34E0294
Public Property Get ScreenText4() As String
    Let ScreenText4 = mScreenText4
End Property

'##ModelId=4098E34E00F9
Public Property Let ScreenText4(ByVal Value As String)
    Let mScreenText4 = Value
End Property

'##ModelId=4098E34D03BF
Public Property Get ScreenText3() As String
    Let ScreenText3 = mScreenText3
End Property

'##ModelId=4098E34D0238
Public Property Let ScreenText3(ByVal Value As String)
    Let mScreenText3 = Value
End Property

'##ModelId=4098E34D0120
Public Property Get ScreenText2() As String
    Let ScreenText2 = mScreenText2
End Property

'##ModelId=4098E34C038B
Public Property Let ScreenText2(ByVal Value As String)
    Let mScreenText2 = Value
End Property

'##ModelId=4098E34C0273
Public Property Get ScreenText1() As String
    Let ScreenText1 = mScreenText1
End Property

'##ModelId=4098E34C0100
Public Property Let ScreenText1(ByVal Value As String)
    Let mScreenText1 = Value
End Property

Public Property Get RejectedText2() As String
    Let RejectedText2 = mRejectedText2
End Property

'##ModelId=4098E34F02B3
Public Property Let RejectedText2(ByVal Value As String)
    Let mRejectedText2 = Value
End Property

'##ModelId=4098E34F017D
Public Property Get RejectedText1() As String
    Let RejectedText1 = mRejectedText1
End Property

'##ModelId=4098E34E03C0
Public Property Let RejectedText1(ByVal Value As String)
    Let mRejectedText1 = Value
End Property

'##ModelId=4098E34B03DA
Public Property Get ImagePath() As String
    Let ImagePath = mImagePath
End Property

'##ModelId=4098E34B027B
Public Property Let ImagePath(ByVal Value As String)
    Let mImagePath = Value
End Property

'##ModelId=4098E34B0181
Public Property Get PromptType() As String
    Let PromptType = mPromptType
End Property

'##ModelId=4098E34B002C
Public Property Let PromptType(ByVal Value As String)
    Let mPromptType = Value
End Property

'##ModelId=4098E34A0324
Public Property Get PromptID() As String
    Let PromptID = mPromptID
End Property


'##ModelId=4098E34A01C6
Public Property Let PromptID(ByVal Value As String)
    Let mPromptID = Value
End Property

Public Property Get ResponseType() As String
    Let ResponseType = mResponseType
End Property

Public Property Let ResponseType(ByVal Value As String)
    Let mResponseType = Value
End Property


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_PROMPT * &H10000) + 1 To FID_PROMPT_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cPrompts

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As Object
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_PROMPT, FID_PROMPT_END_OF_STATIC, m_oSession)

End Function

Private Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    Select Case (lFieldID)
        Case (FID_PROMPT_PromptID):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPromptID
        Case (FID_PROMPT_PromptType):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPromptType
        Case (FID_PROMPT_ImagePath):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mImagePath
        Case (FID_PROMPT_ScreenText1):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mScreenText1
        Case (FID_PROMPT_ScreenText2):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mScreenText2
        Case (FID_PROMPT_ScreenText3):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mScreenText3
        Case (FID_PROMPT_ScreenText4):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mScreenText4
        Case (FID_PROMPT_PrintText1):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPrintText1
        Case (FID_PROMPT_PrintText2):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPrintText2
        Case (FID_PROMPT_PrintText3):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPrintText3
        Case (FID_PROMPT_PrintText4):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPrintText4
        Case (FID_PROMPT_RejectedText1):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mRejectedText1
        Case (FID_PROMPT_RejectedText2):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mRejectedText2
        Case (FID_PROMPT_DataItem):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mDataItem
        Case (FID_PROMPT_DataGroup):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mDataGroup
        Case (FID_PROMPT_DataCondition):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mDataCondition
        Case (FID_PROMPT_SupervisorAuth):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mSupervisorAuth
        Case (FID_PROMPT_ManagerAuth):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mManagerAuth
        Case (FID_PROMPT_CollectInfo):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mCollectInfo
        Case (FID_PROMPT_AllowSale):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mAllowSale
        Case (FID_PROMPT_PromptGroup):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldLong")
                GetField.ValueAsVariant = mPromptGroup
        Case (FID_PROMPT_PromptValue):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPromptValue
        Case (FID_PROMPT_ResponseType):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mResponseType
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cPrompts
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_PROMPT

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Prompt " & mPromptID

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True

    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_PROMPT_PromptID):         mPromptID = oField.ValueAsVariant
            Case (FID_PROMPT_PromptType):       mPromptType = oField.ValueAsVariant
            Case (FID_PROMPT_ImagePath):        mImagePath = oField.ValueAsVariant
            Case (FID_PROMPT_ScreenText1):      mScreenText1 = oField.ValueAsVariant
            Case (FID_PROMPT_ScreenText2):      mScreenText2 = oField.ValueAsVariant
            Case (FID_PROMPT_ScreenText3):      mScreenText3 = oField.ValueAsVariant
            Case (FID_PROMPT_ScreenText4):      mScreenText4 = oField.ValueAsVariant
            Case (FID_PROMPT_PrintText1):       mPrintText1 = oField.ValueAsVariant
            Case (FID_PROMPT_PrintText2):       mPrintText2 = oField.ValueAsVariant
            Case (FID_PROMPT_PrintText3):       mPrintText3 = oField.ValueAsVariant
            Case (FID_PROMPT_PrintText4):       mPrintText4 = oField.ValueAsVariant
            Case (FID_PROMPT_RejectedText1):       mRejectedText1 = oField.ValueAsVariant
            Case (FID_PROMPT_RejectedText2):       mRejectedText2 = oField.ValueAsVariant
            Case (FID_PROMPT_DataItem):         mDataItem = oField.ValueAsVariant
            Case (FID_PROMPT_DataGroup):        mDataGroup = oField.ValueAsVariant
            Case (FID_PROMPT_DataCondition):    mDataCondition = oField.ValueAsVariant
            Case (FID_PROMPT_SupervisorAuth):   mSupervisorAuth = oField.ValueAsVariant
            Case (FID_PROMPT_ManagerAuth):      mManagerAuth = oField.ValueAsVariant
            Case (FID_PROMPT_CollectInfo):      mCollectInfo = oField.ValueAsVariant
            Case (FID_PROMPT_AllowSale):        mAllowSale = oField.ValueAsVariant
            Case (FID_PROMPT_PromptGroup):      mPromptGroup = oField.ValueAsVariant
            Case (FID_PROMPT_PromptValue):      mPromptValue = oField.ValueAsVariant
            Case (FID_PROMPT_ResponseType):     mResponseType = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_PROMPT_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As Object

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


