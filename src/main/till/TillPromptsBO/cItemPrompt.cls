VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cItemGrpPrompts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : cItemGrpPrompts
'* Date   : 07/5/04
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/cOPEventsBO/cEventHeader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/05/04 12:19 $ $Revision: 2 $
'* Versions:
'* 07/05/04    mauricem
'*             Initial Version
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cItemGrpPrompts"

Implements IBo
Implements ISysBo


Private mPartCode As String

'##ModelId=4098D1730219
Private mPromptGroupID As String

Private mSequence As String

'##ModelId=4098D17B021A
Private mSentToStores As Boolean

'##ModelId=4098D18301CC
Private mDateDeleted As Date

Dim moCurrentItemGrpPrompt As cItemGrpPrompts

Dim mcolGroups As Collection

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=4098E356019B
Public Property Get SentToStores() As Boolean
    Let SentToStores = mSentToStores
End Property

'##ModelId=4098E3560014
Public Property Let SentToStores(ByVal Value As Boolean)
    Let mSentToStores = Value
End Property

'##ModelId=4098E35502EE
Public Property Get DateDeleted() As Date
    Let DateDeleted = mDateDeleted
End Property

'##ModelId=4098E3550171
Public Property Let DateDeleted(ByVal Value As Date)
    Let mDateDeleted = Value
End Property

'##ModelId=4098E35401CA
Public Property Get Sequence() As String
    Let Sequence = mSequence
End Property

'##ModelId=4098E3540061
Public Property Let Sequence(ByVal Value As String)
    Let mSequence = Value
End Property

'##ModelId=4098E353034F
Public Property Get PromptGroupID() As String
    Let PromptGroupID = mPromptGroupID
End Property

'##ModelId=4098E35301F1
Public Property Let PromptGroupID(ByVal Value As String)
    Let mPromptGroupID = Value
End Property

'##ModelId=4098E35300EC
Public Property Get PartCode() As String
    Let PartCode = mPartCode
End Property


'##ModelId=4098E352038A
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property


Public Function GetGroupsForSKU(strPartCode As String) As Boolean

Dim dteActiveDate As Date

    Call IBo_ClearLoadFilter
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_ITEM_GRPPROMPT_PartCode, strPartCode)
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_ITEM_GRPPROMPT_DateDeleted, dteActiveDate)
    Set mcolGroups = LoadMatches
    
    If (mcolGroups.Count > 0) Then GetGroupsForSKU = True

End Function

Public Function IsGroupForSKU(ByRef oItemBO As cInventory, ByRef oItemWickesBO As cInventoryWickes, ByRef colGroups As Collection) As Boolean

Dim oGrpPrompt  As cGroupPrompts
Dim blnResults  As Boolean

Dim oFirstPrompt As cPrompts
Dim lngItemNo    As Long

    blnResults = False
    'Load First Group and Prompts, if not already loaded
    If ((oFirstPrompt Is Nothing) = True) Then
        Set oGrpPrompt = m_oSession.Database.CreateBusinessObject(CLASSID_GROUP_PROMPT)
        Call oGrpPrompt.IBo_AddLoadFilter(CMP_EQUAL, FID_GROUP_PROMPT_GroupID, mPromptGroupID)
        Call oGrpPrompt.IBo_AddLoadFilter(CMP_EQUAL, FID_GROUP_PROMPT_Sequence, "000001")
        Call oGrpPrompt.LoadMatches
        
        Set oFirstPrompt = m_oSession.Database.CreateBusinessObject(CLASSID_PROMPT)
        Call oFirstPrompt.IBo_AddLoadFilter(CMP_EQUAL, FID_PROMPT_PromptID, oGrpPrompt.PromptID)
        Call oFirstPrompt.LoadMatches
        
    End If
    
Dim vntData
Dim vntCondition
Dim strField As String
Dim lngFieldID As Long
Dim strCond    As String
Dim strValue   As String
Dim oField     As IField

    IsGroupForSKU = True
    If (InStr(oFirstPrompt.DataItem, "};") > 0) Then
        vntData = Split(oFirstPrompt.DataItem, "};")
        vntCondition = Split(oFirstPrompt.DataCondition, "};")
        For lngItemNo = 0 To UBound(vntData) Step 1
            strField = Mid$(vntData(lngItemNo), 3)
            If (strField <> "") Then
                lngFieldID = ConvertToFieldID(strField)
                If (lngItemNo < UBound(vntCondition)) Then strCond = Trim$(Left(Mid$(vntCondition(lngItemNo), 2), 2))
                If (lngItemNo < UBound(vntCondition)) Then strValue = Trim$(Mid$(vntCondition(lngItemNo), 3))
                Set oField = oItemWickesBO.GetField(lngFieldID)
                Select Case (strCond)
                    Case ("="):
                        If (oField.ValueAsVariant <> strValue) Then
                            IsGroupForSKU = False
                            Exit Function
                        End If
                    Case (">="):
                        If (oItemBO.GetField(lngFieldID).ValueAsVariant < strValue) Then
                            IsGroupForSKU = False
                            Exit Function
                        End If
                    Case ("<="):
                        If (oItemBO.GetField(lngFieldID).ValueAsVariant > strValue) Then
                            IsGroupForSKU = False
                            Exit Function
                        End If
                    Case (">"):
                        If (oField.ValueAsVariant <= strValue) Then
                            IsGroupForSKU = False
                            Exit Function
                        End If
                    Case (">"):
                        If (oField.ValueAsVariant >= strValue) Then
                            IsGroupForSKU = False
                            Exit Function
                        End If
                    Case ("<>"):
                        If (oField.ValueAsVariant = strValue) Then
                            IsGroupForSKU = False
                            Exit Function
                        End If
                End Select
            End If
        Next lngItemNo
    End If
    
'    If oItemBO.GetField(1).ValueAsVariant = cnd Then
'    End If
    
'    If (mcolGroups.Count > 0) Then GetGroupsForSKU = True

End Function

Public Function ConvertToFieldID(strField As String) As Long

    Select Case (strField)
        Case ("SM:ISOL"): ConvertToFieldID = FID_INVENTORYWICKES_SolventAge
        Case ("SM:QUAR"): ConvertToFieldID = FID_INVENTORYWICKES_QuarantineFlag
        Case ("SM:IOFF"): ConvertToFieldID = FID_INVENTORYWICKES_OffensiveWeaponAge
        Case ("SM:IPSK"): ConvertToFieldID = FID_INVENTORYWICKES_PalletedSku
        Case ("SM:IPRD"): ConvertToFieldID = FID_INVENTORYWICKES_PricingDiscrepency
    End Select
        
End Function

Public Function GetGenericPromptGroups() As Collection

Dim dteBlankDate As Date
Dim colGroups    As Collection

    Call IBo_ClearLoadFilter
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_ITEM_GRPPROMPT_PartCode, "000000")
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_ITEM_GRPPROMPT_DateDeleted, dteBlankDate)
    Set colGroups = LoadMatches
    
    Set GetGenericPromptGroups = colGroups

End Function

Public Function FirstGroup() As cGroupPrompts

Dim oItemGrpPromptBO As cItemGrpPrompts
Dim oPassOutBO       As cItemGrpPrompts

    'Step through list of Prompts and find first in display sequence
    For Each oItemGrpPromptBO In mcolGroups
        If oPassOutBO Is Nothing Then Set oPassOutBO = oItemGrpPromptBO
        If (oPassOutBO.Sequence > oItemGrpPromptBO.Sequence) Then Set oPassOutBO = oItemGrpPromptBO
    Next
    Set moCurrentItemGrpPrompt = oPassOutBO
    mPromptGroupID = oPassOutBO.PromptGroupID
    mSequence = oPassOutBO.Sequence
    mDateDeleted = oPassOutBO.DateDeleted
    mSentToStores = oPassOutBO.SentToStores
    
End Function


Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    
    Delete = Save(SaveTypeDelete)


End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_ITEM_GRPPROMPT * &H10000) + 1 To FID_ITEM_GRPPROMPT_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Sequence: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Sequence: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cItemGrpPrompts

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_ITEM_GRPPROMPT, FID_ITEM_GRPPROMPT_END_OF_STATIC, m_oSession)

End Function

Private Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)

        Case (FID_ITEM_GRPPROMPT_PartCode):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPartCode
        Case (FID_ITEM_GRPPROMPT_PromptGroupID):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPromptGroupID
        Case (FID_ITEM_GRPPROMPT_Sequence):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mSequence
        Case (FID_ITEM_GRPPROMPT_DateDeleted):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldDate")
                GetField.ValueAsVariant = mDateDeleted
        Case (FID_ITEM_GRPPROMPT_SentToStores):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldBool")
                GetField.ValueAsVariant = mSentToStores
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Sequence)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cItemGrpPrompts
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_ITEM_GRPPROMPT

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Item Group " & mPartCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Sequence: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_ITEM_GRPPROMPT_PartCode):      mPartCode = oField.ValueAsVariant
            Case (FID_ITEM_GRPPROMPT_PromptGroupID): mPromptGroupID = oField.ValueAsVariant
            Case (FID_ITEM_GRPPROMPT_Sequence):      mSequence = oField.ValueAsVariant
            Case (FID_ITEM_GRPPROMPT_DateDeleted):   mDateDeleted = oField.ValueAsVariant
            Case (FID_ITEM_GRPPROMPT_SentToStores):  mSentToStores = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_ITEM_GRPPROMPT_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cItemGrpPrompts

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


