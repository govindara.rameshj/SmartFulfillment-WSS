VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGroupPrompts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : cGroupPrompts
'* Date   : 07/5/04
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/cOPEventsBO/cEventHeader.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/05/04 12:19 $ $Revision: 2 $
'* Versions:
'* 07/05/04    mauricem
'*             Initial Version
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cGroupPrompts"

Const SEQ_OK As String = "END"
Const SEQ_FAIL As String = "REJECT"

Implements IBo
Implements ISysBo


Private mGroupID As String

'##ModelId=4098D1730219
Private mPromptID As String

'##ModelId=4098D17B021A
Private mSequence As String

'##ModelId=4098D18301CC
Private mOK_Goto As String

'##ModelId=4098D186011C
Private mYes_Goto As String

'##ModelId=4098D1880381
Private mNo_Goto As String

Private mLogReject As Boolean

Private mGroupDesc As String

Dim moCurrentItemPrompt As cGroupPrompts

Dim mcolPrompts As Collection

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=4098E356019B
Public Property Get No_Goto() As String
    Let No_Goto = mNo_Goto
End Property

'##ModelId=4098E3560014
Public Property Let No_Goto(ByVal Value As String)
    Let mNo_Goto = Value
End Property

'##ModelId=4098E35502EE
Public Property Get Yes_Goto() As String
    Let Yes_Goto = mYes_Goto
End Property

'##ModelId=4098E3550171
Public Property Let Yes_Goto(ByVal Value As String)
    Let mYes_Goto = Value
End Property

'##ModelId=4098E3550063
Public Property Get OK_Goto() As String
    Let OK_Goto = mOK_Goto
End Property

'##ModelId=4098E35402D8
Public Property Let OK_Goto(ByVal Value As String)
    Let mOK_Goto = Value
End Property

'##ModelId=4098E35401CA
Public Property Get Sequence() As String
    Let Sequence = mSequence
End Property

'##ModelId=4098E3540061
Public Property Let Sequence(ByVal Value As String)
    Let mSequence = Value
End Property

'##ModelId=4098E353034F
Public Property Get PromptID() As String
    Let PromptID = mPromptID
End Property

'##ModelId=4098E35301F1
Public Property Let PromptID(ByVal Value As String)
    Let mPromptID = Value
End Property

'##ModelId=4098E35300EC
Public Property Get GroupID() As String
    Let GroupID = mGroupID
End Property


'##ModelId=4098E352038A
Public Property Let GroupID(ByVal Value As String)
    Let mGroupID = Value
End Property

Public Property Get LogReject() As Boolean
    Let LogReject = mLogReject
End Property

'##ModelId=4098E35301F1
Public Property Let LogReject(ByVal Value As Boolean)
    Let mLogReject = Value
End Property

Public Property Get GroupDesc() As String
    Let GroupDesc = mGroupDesc
End Property

Public Property Let GroupDesc(ByVal Value As String)
    Let mGroupDesc = Value
End Property


Public Function GetPromptsForGroupID(strGroupID As String) As Boolean

    Call IBo_ClearLoadFilter
    Call IBo_AddLoadFilter(CMP_EQUAL, FID_GROUP_PROMPT_GroupID, strGroupID)
    Call IBo_AddLoadFilter(CMP_GREATERTHAN, FID_GROUP_PROMPT_PromptID, "000000")
    Set mcolPrompts = LoadMatches
    
    If (mcolPrompts.Count > 0) Then GetPromptsForGroupID = True

End Function

Public Function FirstPrompt() As cPrompts

Dim oItemPromptBO   As cGroupPrompts
Dim oPassOutBO      As cGroupPrompts
Dim oPromptBO       As cPrompts

    'Step through list of Prompts and find first in display sequence
    For Each oItemPromptBO In mcolPrompts
        If oPassOutBO Is Nothing Then Set oPassOutBO = oItemPromptBO
        If (oPassOutBO.Sequence > oItemPromptBO.Sequence) Then Set oPassOutBO = oItemPromptBO
    Next
    Set moCurrentItemPrompt = oPassOutBO
    mPromptID = oPassOutBO.PromptID
    mSequence = oPassOutBO.Sequence
    mYes_Goto = oPassOutBO.Yes_Goto
    mNo_Goto = oPassOutBO.No_Goto
    mOK_Goto = oPassOutBO.OK_Goto
    mLogReject = oPassOutBO.LogReject
    mGroupDesc = oPassOutBO.GroupDesc
    
    'Pass the prompt out to be processed
    Set FirstPrompt = RetrievePrompt(moCurrentItemPrompt.PromptID)

End Function

Private Function RetrievePrompt(strPromptID As String) As cPrompts

Dim oPromptBO       As cPrompts
    
    'Use PromptID to retrieve actual prompt details
    Set oPromptBO = m_oSession.Database.CreateBusinessObject(CLASSID_PROMPT)
    Call oPromptBO.IBo_AddLoadFilter(CMP_EQUAL, FID_PROMPT_PromptID, strPromptID)
    Call oPromptBO.LoadMatches
    Set RetrievePrompt = oPromptBO

End Function

Public Function Next_Yes_Prompt(ByRef oPromptBO As cPrompts) As Boolean

    Next_Yes_Prompt = ProcessGotoResponse(moCurrentItemPrompt.Yes_Goto, oPromptBO)
    
End Function

Public Function Next_No_Prompt(ByRef oPromptBO As cPrompts) As Boolean

    Next_No_Prompt = ProcessGotoResponse(moCurrentItemPrompt.No_Goto, oPromptBO)

End Function

Public Function Next_OK_Prompt(ByRef oPromptBO As cPrompts) As Boolean

    Next_OK_Prompt = ProcessGotoResponse(moCurrentItemPrompt.OK_Goto, oPromptBO)

End Function

Private Function ProcessGotoResponse(strActionCode As String, ByRef cPromptBO As cPrompts) As Boolean

Dim oItemPromptBO   As cGroupPrompts
    
    Select Case (UCase(strActionCode))
        Case (SEQ_OK):
                        Set cPromptBO = Nothing
                        ProcessGotoResponse = True
                        Exit Function
        Case (SEQ_FAIL):
                        Set cPromptBO = Nothing
                        ProcessGotoResponse = False
                        Exit Function
        Case Else
                        'Step through list of Prompts and find next prompt in display sequence
                        For Each oItemPromptBO In mcolPrompts
                            If (oItemPromptBO.Sequence = strActionCode) Then
                                Set moCurrentItemPrompt = oItemPromptBO
                                Exit For
                                End If
                        Next
                        mPromptID = moCurrentItemPrompt.PromptID
                        mSequence = moCurrentItemPrompt.Sequence
                        mYes_Goto = moCurrentItemPrompt.Yes_Goto
                        mNo_Goto = moCurrentItemPrompt.No_Goto
                        mOK_Goto = moCurrentItemPrompt.OK_Goto
                        mLogReject = moCurrentItemPrompt.LogReject
                        mGroupDesc = moCurrentItemPrompt.GroupDesc
                        'Pass the prompt out to be processed
                        Set cPromptBO = RetrievePrompt(moCurrentItemPrompt.PromptID)
                        ProcessGotoResponse = True
    End Select
    
End Function

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    
    Delete = Save(SaveTypeDelete)


End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_GROUP_PROMPT * &H10000) + 1 To FID_GROUP_PROMPT_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Sequence: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    IBo_AddLoadFilter = True

End Function 'IBo_AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Sequence: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".IBo_AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'IBo_AddLoadField

Public Sub IBo_ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cGroupPrompts

End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_GROUP_PROMPT, FID_GROUP_PROMPT_END_OF_STATIC, m_oSession)

End Function

Private Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)

        Case (FID_GROUP_PROMPT_GroupID):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mGroupID
        Case (FID_GROUP_PROMPT_PromptID):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mPromptID
        Case (FID_GROUP_PROMPT_Sequence):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mSequence
        Case (FID_GROUP_PROMPT_OK_GOTO):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mOK_Goto
        Case (FID_GROUP_PROMPT_YES_GOTO):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mYes_Goto
        Case (FID_GROUP_PROMPT_NO_GOTO):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mNo_Goto
        Case (FID_GROUP_PROMPT_LogReject):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = IIf(mLogReject, "Y", "N")
        Case (FID_GROUP_PROMPT_GroupDesc):
                Set GetField = m_oSession.Root.CreateUtilityObject("CFieldString")
                GetField.ValueAsVariant = mGroupDesc
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Sequence)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cGroupPrompts
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_GROUP_PROMPT

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "Group Prompt " & mGroupID

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Sequence: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_GROUP_PROMPT_GroupID):     mGroupID = oField.ValueAsVariant
            Case (FID_GROUP_PROMPT_PromptID):    mPromptID = oField.ValueAsVariant
            Case (FID_GROUP_PROMPT_Sequence):    mSequence = oField.ValueAsVariant
            Case (FID_GROUP_PROMPT_OK_GOTO):     mOK_Goto = oField.ValueAsVariant
            Case (FID_GROUP_PROMPT_YES_GOTO):    mYes_Goto = oField.ValueAsVariant
            Case (FID_GROUP_PROMPT_NO_GOTO):     mNo_Goto = oField.ValueAsVariant
            Case (FID_GROUP_PROMPT_LogReject):   mLogReject = (oField.ValueAsVariant = "Y")
            Case (FID_GROUP_PROMPT_GroupDesc):   mGroupDesc = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_GROUP_PROMPT_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cGroupPrompts

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function


