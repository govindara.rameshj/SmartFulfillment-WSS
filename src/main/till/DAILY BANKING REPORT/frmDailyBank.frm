VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmDailyBankReport 
   Caption         =   "Daily Banking Report"
   ClientHeight    =   6885
   ClientLeft      =   795
   ClientTop       =   1605
   ClientWidth     =   10695
   Icon            =   "frmDailyBank.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6885
   ScaleWidth      =   10695
   WindowState     =   2  'Maximized
   Begin VB.PictureBox fraCriteria 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   120
      ScaleHeight     =   705
      ScaleWidth      =   4785
      TabIndex        =   5
      Top             =   180
      Width           =   4815
      Begin ucEditDate.ucDateText dtxtDate 
         Height          =   285
         Left            =   1740
         TabIndex        =   9
         Top             =   300
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "09/09/10"
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7-Search"
         Height          =   375
         Left            =   3540
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblfraCriteria 
         AutoSize        =   -1  'True
         Caption         =   "Selection Criteria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   15
         Width           =   1470
      End
      Begin VB.Label labStartDate 
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Enter date for report"
         Height          =   255
         Left            =   180
         TabIndex        =   7
         Top             =   345
         Width           =   1575
      End
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1860
      Left            =   2760
      TabIndex        =   4
      Top             =   2520
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   6000
      Width           =   1815
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9-Print"
      Height          =   375
      Left            =   9000
      TabIndex        =   2
      Top             =   6000
      Visible         =   0   'False
      Width           =   1335
   End
   Begin FPSpreadADO.fpSpread sprdData 
      Height          =   3855
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   10455
      _Version        =   458752
      _ExtentX        =   18441
      _ExtentY        =   6800
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      DisplayColHeaders=   0   'False
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   11
      MaxRows         =   3
      OperationMode   =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmDailyBank.frx":058A
      UserResize      =   0
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   6510
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11060
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:54"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDailyBankReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmDailyBankReport
'* Date   : 21/05/2003
'* Author : KeithB
'*$Archive: /Projects/OasysV2/VB/Daily Banking Report/frmDailyBank.frm $
'**********************************************************************************************
'* Summary: Display & print facility to produce Daily Banking Report
'           Can be called, passing in parameters to pre-select criteria & auto-print.
'
' Parameters: DTyyyymmdd  Date   yyyy=year mm=month dd=day  e.g. (/P='DT=20030329')
'          or DByyyymmdd  Date   yyyy=year mm=month dd=day  e.g. (/P='DB=20030329')
'             DCP         Destination Code Printer
'             DCS         Destination Code Preview
'SAMPLE=(/P='DB=20030515,DCS,STO,GRD,DL0' /u='044' -c='ffff')
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/06/04 10:06 $ $Revision: 11 $
'* Versions:
'* 21/05/2003  KeithB
'*             Header added.
'* 21/05/2003  KeithB set left margin before printing.
'* 30/05/03    KeithB allow date parameter of DB when called from Bank Deposits (Daily Banking)
'* 03/06/03    KeithB
'*         (a) add 'explain till error' and 'margin comments'
'*         (b) get tender type for account sale from param file
'*         (c) debug of 'A/C Sales' & 'A/C Payments' & Daily Sales Summary
'*         (d) cSale.dll was not working correctly - use new version
'*         (e) display order number for Deposits
'* 06/06/03    KeithB
'*             Remove 'Jasba Stand' as requested by John Coles - e-mail to Raj (06/06/03)
'*
'</CAMH>***************************************************************************************



Option Explicit

Const CONST_RETAIL_CODE    As String = "00000000"
Const TRANS_CODE_SALE      As String = "SA"
Const TRANS_CODE_REFUND    As String = "RF"
Const TRANS_CODE_PAIDIN    As String = "M+"
Const TRANS_CODE_PETTYCASH As String = "M-"
Const CONST_ACCNT_CUST     As String = "A"

Const HO_REFUND            As Integer = 19

Const COL_DEP_HEADER       As Long = 1
Const COL_DEP_CUST_NAME    As Long = 2
Const COL_DEP_NO           As Long = 3
Const COL_DEP_CODE         As Long = 4
Const COL_DEP_AMOUNT       As Long = 5
Const COL_DEP_SPARE        As Long = 6
Const COL_SMRY_SPARE1      As Long = 7
Const COL_SMRY_DESCRIPT    As Long = 8
Const COL_SMRY_AMOUNT      As Long = 9
Const COL_SMRY_COMMENT     As Long = 10
Const COL_SORT             As Long = 11

Const CASH_PARAMETER       As Integer = 650
Const ACCNTSALE_TENDERTYPE As Integer = 651
Const BORDER_WIDTH         As Long = 60

Dim mlngPayAmtDecPlaces    As Long   'hold number of decimal places to show for pay amount
Dim mstrValueFormat        As String

Private Enum endgDepartmentGroup
    endgGroupTileSales = 0
    endgGroupWoodProducts = 1
    endgGroupStoneProducts = 2 ' 06/06/03 KVB remove Jasba / 25/11/03-MM Added back in as Stone
    endgGroupDelivery = 3      ' 06/06/03 KVB remove Jasba
End Enum

Private mstrCashPmtCode         As String
Private mstrDestCode            As String
Private mlngAccntSaleTenderType As Long
Private mstrExitCaption         As String
Private mstrMaxValidCashier     As String
Dim madblMiscInc(10)            As Double
'Dim moMiscIncomeDesc            As cTillLookUps 'holds conversion from Code to the Description


Private Sub cmdApply_Click()
    
Dim dblDepositsReceived      As Double
Dim dblDepositsDeducted      As Double
Dim dblAccountSalesTot       As Double
Dim dblAccountPaymentsTot    As Double
Dim dblTotalTendered()       As Double
Dim dblTotalCash             As Double
Dim dblTotalSales            As Double
Dim dblTotalBanked           As Double
Dim dblPettyCashSection4Tot  As Double
Dim dblAcPaymentsSection4Tot As Double
Dim lngMaxRowLeft            As Long
Dim lngMaxRowRight           As Long
Dim lngFirstAccntSalesRow    As Long
Dim lngLastAccntSalesRow     As Long
Dim intCntTenderTypes        As Integer
Dim strComment1              As String
Dim strComment2              As String

    Screen.MousePointer = vbHourglass
    
    sbStatus.Panels(PANEL_INFO).Text = "Loading Data"
    cmdApply.Enabled = False
    cmdPrint.Visible = False
    
    Me.Refresh
    
    sprdData.MaxRows = 3
    sprdData.MaxCols = COL_SORT
    
    lngMaxRowLeft = 0
    lngMaxRowRight = 0
    dblDepositsReceived = 0
    dblDepositsDeducted = 0
    dblAccountSalesTot = 0
    dblAccountPaymentsTot = 0
    dblTotalCash = 0
    dblTotalSales = 0
    dblTotalBanked = 0
    dblPettyCashSection4Tot = 0
    dblAcPaymentsSection4Tot = 0
    
    lngFirstAccntSalesRow = 0
    lngLastAccntSalesRow = 0
    
    ' prepare an array to store tendered amounts (NOT cash)
    ' this array may be resized later
    
    intCntTenderTypes = 1
    ReDim dblTotalTendered(intCntTenderTypes)
 
    ' Hide the column headers  A, B, C ...
    sprdData.ColHeadersShow = False
    sprdData.Refresh
    sprdData.ReDraw = False
    
    Call FillInDeposits(dblDepositsReceived, dblDepositsDeducted, lngMaxRowLeft, lngFirstAccntSalesRow)
    
    ' Fill grid for Daily Sales Summary (right side of page)
    '      and A/C Sales & A/C Payments (left side)
    Call FillInSales(dblDepositsReceived, dblDepositsDeducted, dblAccountSalesTot, dblAccountPaymentsTot, _
                     lngMaxRowLeft, lngMaxRowRight, _
                     lngFirstAccntSalesRow, lngLastAccntSalesRow, _
                     dblTotalTendered(), dblTotalCash, dblTotalSales, _
                     dblPettyCashSection4Tot, dblAcPaymentsSection4Tot, _
                     intCntTenderTypes)
    
    Call FillInBanking(lngMaxRowRight, dblTotalBanked, strComment1, strComment2)
    
    Call SortAccountSales(dblAccountSalesTot, dblAccountPaymentsTot, lngMaxRowLeft, _
                          lngFirstAccntSalesRow, lngLastAccntSalesRow)
    
    Call FillInMiscIn
    
    Call FillInSummarySection4(lngMaxRowLeft, lngMaxRowRight, dblAccountSalesTot, dblAccountPaymentsTot, _
                               dblTotalTendered(), dblTotalCash, dblTotalSales, dblTotalBanked, _
                               dblPettyCashSection4Tot, dblAcPaymentsSection4Tot, _
                               strComment1, strComment2)
    
    Call FillInSignOff(lngMaxRowRight)
    
    On Error Resume Next

    sprdData.ReDraw = True
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    sprdData.MaxCols = COL_SMRY_COMMENT
    cmdApply.Enabled = True
    cmdPrint.Visible = True
    sprdData.SetFocus
    
    cmdApply.Caption = "F7-Refresh"
    Screen.MousePointer = vbDefault

End Sub

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()

Dim lngShadowColor        As Long
Dim blnGridShowHorizontal As Boolean
Dim blnGridShowVertical   As Boolean
Dim strFooter             As String
Dim strHeader             As String
Dim oPrintStore           As cStore

    ' get name and address for current store to put at top of document
    If oPrintStore Is Nothing Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.Load
    End If

    ' when printing change the header background to white
    '               & hide the grid lines.
    lngShadowColor = sprdData.ShadowColor
    blnGridShowHorizontal = sprdData.GridShowHoriz
    blnGridShowVertical = sprdData.GridShowVert
    
    sprdData.ShadowColor = vbWhite
    sprdData.GridShowHoriz = False
    sprdData.GridShowVert = False
    
    ' use PrintColor to ensure that horizontal lines (inverted color) are shown
    sprdData.PrintColor = True
    
    ' left margin (1440 twips = 1 inch)
    sprdData.PrintMarginLeft = 1080
    
    strHeader = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & _
                oPrintStore.AddressLine1 & "  -  Daily Banking Report for " & _
                dtxtDate.Text
    
    ' /c is Centre text       /fb1 is Font Bold on
    sprdData.PrintHeader = "/c/fb1" & strHeader
    
    sprdData.PrintJobName = "Daily Banking Report"
    
    ' Set up Footer - program version no date/time printed
    strFooter = "( V" & App.Major & "." & App.Minor & "." & App.Revision & " )" & _
                Space$(5) & "Printed - " & Format$(Now(), "DD/MM/YY HH:NN")
    ' /l is left align
    sprdData.PrintFooter = "/l" & strFooter & "/r Page /p of /pc"

    
    Load frmPreview
    Call frmPreview.SetPreview("Daily Banking Report", sprdData, mstrExitCaption)
    frmPreview.Show vbModal
    
    Unload frmPreview
    
    sprdData.ShadowColor = lngShadowColor
    sprdData.GridShowHoriz = blnGridShowHorizontal
    sprdData.GridShowVert = blnGridShowVertical
    
    sprdData.Refresh
    
    ' clean up
    On Error Resume Next
    Set oPrintStore = Nothing

End Sub

Private Sub dtxtDate_GotFocus()

    dtxtDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtDate_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    
End Sub

Private Sub dtxtDate_LostFocus()

    dtxtDate.BackColor = RGB_WHITE

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   
   Select Case (Shift)
    Case (0):
        Select Case (KeyCode)
        Case (vbKeyF7):  'if F7 then call search
          If cmdApply.Visible = True Then
              Call cmdApply_Click
          End If
        Case (vbKeyF9):  'if F9 then call print
          If cmdPrint.Visible = True Then
              Call cmdPrint_Click
          End If
        Case (vbKeyF10): 'if F10 then exit
            Call cmdExit_Click
        End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

Private Sub Form_Load()

Dim oLookUp As cRetailOptions

    Me.Show
    
    ucProgress.Max = 5
    ucProgress.Visible = True
    ucProgress.Value = 1
    
    sbStatus.Panels(PANEL_INFO).Text = "Accessing session"
    Me.Refresh

    Call GetRoot
    
    ucProgress.Value = 2
    ucProgress.Refresh
    
    'Display initial values in Status Bar
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    cmdExit.BackColor = Me.BackColor
    cmdPrint.BackColor = Me.BackColor
    ucProgress.FillColor = Me.BackColor
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    
    ucProgress.Value = 3
    Call InitialiseStatusBar(sbStatus)
    
   ' get display format for pounds & pence from param file
    mlngPayAmtDecPlaces = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    mstrValueFormat = Replace(Space$(mlngPayAmtDecPlaces), " ", "0")
    If LenB(mstrValueFormat) <> 0 Then
        mstrValueFormat = "0." & mstrValueFormat
    End If
    
    ' get pointer from param file to word 'Cash'
    mstrCashPmtCode = goSession.GetParameter(CASH_PARAMETER)
    If LenB(mstrCashPmtCode) = 0 Then
        Call MsgBox("No pointer set to description for 'Cash'", vbInformation, "WARNING - parameter not set.")
    End If
    
    ' get tender type for account sale from param file
    mlngAccntSaleTenderType = goSession.GetParameter(ACCNTSALE_TENDERTYPE)
    If mlngAccntSaleTenderType <= 0 Then
        Call MsgBox("No tender type for account sale", vbInformation, "WARNING - parameter not set.")
    End If
    
    mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    

    'Retrieve last Cashier ID, all cashiers above this are Training Codes
    Set oLookUp = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oLookUp.IBo_AddLoadField(FID_RETAILEROPTIONS_MaxValidCashierNumber)
    Call oLookUp.LoadMatches
    mstrMaxValidCashier = oLookUp.MaxValidCashierNumber
    Set oLookUp = Nothing
    
    ucProgress.Value = 4
    ucProgress.Refresh
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    
    ' default selection date to todays date
    ' Note: may be overwritten by parameters
    dtxtDate.Text = Format$(Now, mstrDateFmt)
    ucProgress.Value = ucProgress.Max
    ucProgress.Refresh
    
    ucProgress.Visible = False
    dtxtDate.SetFocus
    
    
    sbStatus.Panels(PANEL_INFO).Text = "Checking parameters"
    ' see if any parameters passed in
    ' NOTE:-
    '   (a) DecodeParameters may Call cmdApply_Click
    '   (b) example of ProgramParams -
    '       (/P='DT=20030326,DCS,STO,GRD,DL0' /u='044' -c='ffff')
    '       where DT is date for report in format yyyymmdd
    '             DCS is Destination Code Preview
    '       or    DCP is Destination Code Print
    
    Call DecodeParameters(goSession.ProgramParams)
    
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    
    Select Case mstrDestCode
        Case Is = DEST_REPORT_CODE_PREVIEW  ' S
          Call cmdPrint_Click
          End
        Case Is = DEST_REPORT_CODE_PRINTER  ' P
          Call cmdPrint_Click
          End
    End Select
   
End Sub

Private Sub DecodeParameters(strCommand As String)

Const SELECTION_DATE    As String = "DT"
Const SELECTION_DATE_DB As String = "DB" ' This program called from Daily Banking

'Const WEEK_END_DATE    As String = "WE"
'Const DUE_START_DATE   As String = "DS"
'Const DUE_END_DATE     As String = "DD"

Dim strTempDate As String
Dim dteEndDate  As Date
Dim strSection  As String
Dim vntSection  As Variant
Dim lngSectNo   As Long
Dim blnSetOpts  As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    
    If LenB(mstrDateFmt) = 0 Then
        mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    End If
    
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        
        Select Case (Left$(strSection, 2))
        Case SELECTION_DATE, SELECTION_DATE_DB
            Debug.Print Right$(strSection, 8)
            dteEndDate = GetParamDate(Right$(strSection, 8))
            dtxtDate.Text = Format$(dteEndDate, mstrDateFmt)
            dtxtDate.Refresh
            blnSetOpts = True
            If (Left$(strSection, 2) = SELECTION_DATE_DB) Then
'                    Me.MinButton = False
                cmdExit.Caption = "F10-Back to Banking"
                mstrExitCaption = "Back to Banking"
            End If
        
        Case DEST_CODE ' DC
            strTempDate = Mid$(strSection, 3)
            mstrDestCode = strTempDate
            blnSetOpts = True
        End Select
        
    Next lngSectNo
    
    If blnSetOpts = True Then Call cmdApply_Click

End Sub

Private Sub FillInSignOff(ByRef lngMaxRowRight As Long)

    sprdData.Col = COL_SMRY_DESCRIPT
    
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Text = "To be completed by Manager : - "
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Text = "Date banked :"
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Text = "Time banked :"
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Text = "Wallet No. :"
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Text = "Taken to Bank by :"
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Text = "Managers Signature :"
    Call SetGridRow(lngMaxRowRight, 3)
    Call UnderScoreGrid(lngMaxRowRight, COL_SMRY_SPARE1, COL_SMRY_AMOUNT)
    Call SetGridRow(lngMaxRowRight, 1)
    
End Sub

Private Sub FillInMiscIn()

Dim lngRowNo   As Long
Dim intMiscInc As Long

    'Start at bottom of form and move up until last item found
    sprdData.Col = COL_DEP_CUST_NAME
    For lngRowNo = sprdData.MaxRows To 1 Step -1
        sprdData.Row = lngRowNo
        If LenB(sprdData.Text) <> 0 Then
            sprdData.Row = sprdData.Row + 2
            Exit For
        End If
    Next lngRowNo
    
    'Now display any Miscellaneous Incomes accrued
    For intMiscInc = 1 To 10 Step 1
        If madblMiscInc(intMiscInc) > 0 Then
            sprdData.Row = sprdData.Row + 1
            sprdData.Col = COL_DEP_CUST_NAME
            'sprdData.Text = moMiscIncomeDesc.MiscIncomeAccountNo(intMiscInc)
            sprdData.Col = COL_DEP_AMOUNT
            sprdData.Text = madblMiscInc(intMiscInc)
        End If
    Next intMiscInc
    
End Sub

Private Sub FillInDeposits(ByRef dblDepositsReceived As Double, _
                           ByRef dblDepositsDeducted As Double, _
                           ByRef lngMaxRowLeft As Long, _
                           ByRef lngFirstAccntSalesRow As Long)
    MsgBox "FillInDeposits Removed"
'Dim oDeposit           As cDeposit
'Dim colDeposits        As Collection
'Dim lngDeposit         As Long
'Dim lngSalesLedger     As Long
'Dim dblThisAmount      As Double
'Dim dblErrorAmount     As Double
'Dim blnErrorDetected As Boolean
'
'    sbStatus.Panels(PANEL_INFO).Text = "Populating grid - Deposits"
'
'    ucProgress.Value = 0
'    ucProgress.Visible = True
'    Me.Refresh
'
'    dblDepositsReceived = 0
'    dblDepositsDeducted = 0
'    dblErrorAmount = 0
'
'    ' format the amount column as numeric
'    ' with mlngPayAmtDecPlaces decimal places
'    sprdData.MaxRows = 2
'    sprdData.Row = -1
'    sprdData.Col = COL_DEP_AMOUNT
'    sprdData.CellType = CellTypeNumber
'    sprdData.TypeNumberDecPlaces = mlngPayAmtDecPlaces
'
'    sprdData.Row = 1
'    sprdData.Col = -1
'    sprdData.FontBold = True
'
'    sprdData.Col = COL_SMRY_SPARE1
'    Call sprdData.AddCellSpan(COL_SMRY_SPARE1, sprdData.Row, 4, 1)
'
'    sprdData.Row = 2
'    sprdData.Col = -1
'    sprdData.FontBold = True
'
'    sprdData.Col = COL_DEP_AMOUNT
'    sprdData.CellType = CellTypeEdit
'    sprdData.Text = "  Amount"
'
'    ' divide the two halves of the report with a line down
'    sprdData.Col = COL_DEP_SPARE
'    sprdData.Row = -1
'    ' set the width of the row specified to .2 with background colour of black
'    sprdData.ColWidth(COL_DEP_SPARE) = 0.2
'    sprdData.BackColor = vbBlack
'    sprdData.ForeColor = vbWhite
'
'    Call DrawLineAcross(3)
'
'    ' select from table DEPMAS (CLASSID_DEPOSIT)
'    ' ======================== =================
'    Set oDeposit = goDatabase.CreateBusinessObject(CLASSID_DEPOSIT)
'
'    ' select for the date entered
'    Call oDeposit.AddLoadFilter(CMP_EQUAL, FID_DEPOSIT_DateCreated, GetDate(dtxtDate.Text))
'
'    'Select fields to load from table
'    Call oDeposit.AddLoadField(FID_DEPOSIT_CustomerName)
'    Call oDeposit.AddLoadField(FID_DEPOSIT_DepositNo)
'    Call oDeposit.AddLoadField(FID_DEPOSIT_TransactionMoveCode)
'    Call oDeposit.AddLoadField(FID_DEPOSIT_DepositAmount)
'    ' 02/06/03 display customer order no
'    Call oDeposit.AddLoadField(FID_DEPOSIT_OrderNo)
'
'    Set colDeposits = oDeposit.LoadMatches
'
'    sprdData.MaxRows = sprdData.MaxRows + 2
'    sprdData.Row = sprdData.MaxRows
'    sprdData.Col = COL_DEP_HEADER
'    sprdData.FontBold = True
'    sprdData.Text = "Deposits (Received/Deducted)"
'
'    If colDeposits.Count > 0 Then
'        ucProgress.Max = colDeposits.Count
'    Else
'        ucProgress.Max = 1
'    End If
'
'
'    For lngDeposit = 1 To colDeposits.Count Step 1
'
'        ucProgress.Value = ucProgress.Value + 1
'
'        blnErrorDetected = False
'
'        Select Case colDeposits(lngDeposit).TransactionMoveCode
'        Case "I" ' type I is 'Received' - display an "R"
'          dblThisAmount = colDeposits(lngDeposit).DepositAmount
'          dblDepositsReceived = dblDepositsReceived + dblThisAmount
'
'          Call DisplayThisDeposit(colDeposits(lngDeposit).CustomerName, _
'                                  colDeposits(lngDeposit).OrderNo, _
'                                  "R", dblThisAmount, blnErrorDetected)
'
'        Case "O" ' type O is 'Deducted' - dislay a "D" & reverse the sign
'          dblThisAmount = -colDeposits(lngDeposit).DepositAmount
'          dblDepositsDeducted = dblDepositsDeducted + dblThisAmount
'
'          Call DisplayThisDeposit(colDeposits(lngDeposit).CustomerName, _
'                                  colDeposits(lngDeposit).OrderNo, _
'                                  "D", dblThisAmount, blnErrorDetected)
'
'        Case Is = "T"
'          ' Deposit master record.
'          ' Ignored in this report
'
'        Case Else
'          ' an unrecognised code has been detected
'          ' display it with an * next to it
'          blnErrorDetected = True
'
'          dblThisAmount = colDeposits(lngDeposit).DepositAmount
'          dblErrorAmount = dblErrorAmount + dblThisAmount
'
'          Call DisplayThisDeposit(colDeposits(lngDeposit).CustomerName, _
'                                  colDeposits(lngDeposit).OrderNo, _
'                                  "* " & colDeposits(lngDeposit).TransactionMoveCode, dblThisAmount, blnErrorDetected)
'
'        End Select
'
'    Next lngDeposit
'
'
'    sprdData.MaxRows = sprdData.MaxRows + 1
'    sprdData.Row = sprdData.MaxRows
'    sprdData.Col = COL_DEP_CUST_NAME
'    sprdData.FontBold = True
'    sprdData.Text = "Deposits Total:-"
'
'    sprdData.Col = COL_DEP_AMOUNT
'    ' NB dblDepositsDeducted will be a negative amount
'    sprdData.Text = dblDepositsReceived + dblDepositsDeducted
'
'    If dblErrorAmount <> 0 Then
'        sprdData.MaxRows = sprdData.MaxRows + 1
'        sprdData.Row = sprdData.MaxRows
'
'        sprdData.Col = COL_DEP_CUST_NAME
'        sprdData.FontBold = True
'        sprdData.BackColor = vbRed
'        sprdData.Text = "* UNKNOWN CODES"
'
'        sprdData.Col = COL_DEP_AMOUNT
'        sprdData.Text = dblErrorAmount
'
'        sprdData.Refresh
'
'        Call MsgBox("Invalid Deposit Code(s) detected" & vbCrLf & vbCrLf & _
'                    "Amount " & Format$(dblErrorAmount, mstrValueFormat), _
'                    vbCritical, "Error - code(s) not recognised")
'
'    End If
'
'
'
'    sprdData.MaxRows = sprdData.MaxRows + 2
'    lngFirstAccntSalesRow = sprdData.MaxRows ' store the posotion ready for sorting
'    sprdData.Row = sprdData.MaxRows
'
'    sprdData.Col = COL_DEP_CUST_NAME
'    sprdData.FontBold = True
'    'sprdData.Text = "Credit Sales"
'    sprdData.Text = "A/C Payments Received"
'    sprdData.Col = COL_SORT
'    sprdData.Text = "1" & "  " & "00000"
'
'    sprdData.MaxRows = sprdData.MaxRows + 1
'    sprdData.Row = sprdData.MaxRows
'
'    sprdData.Col = COL_DEP_CUST_NAME
'    sprdData.FontBold = True
'    sprdData.Text = "Credit Sales"
'    sprdData.Col = COL_SORT
'    sprdData.Text = "2" & "  " & "00000"
'
'    On Error Resume Next
'
'    Set oDeposit = Nothing
'    Set colDeposits = Nothing
'
'    lngMaxRowLeft = sprdData.MaxRows
'
'    ucProgress.Visible = False
'    sbStatus.Panels(PANEL_INFO).Text = vbNullString
'
End Sub

Private Sub FillInSales(ByRef dblDepositsReceived As Double, _
                        ByRef dblDepositsDeducted As Double, _
                        ByRef dblAccountSalesTot As Double, _
                        ByRef dblAccountPaymentsTot As Double, _
                        ByRef lngMaxRowLeft As Long, _
                        ByRef lngMaxRowRight As Long, _
                        ByRef lngFirstAccntSalesRow As Long, _
                        ByRef lngLastAccntSalesRow As Long, _
                        ByRef dblTotalTendered() As Double, _
                        ByRef dblTotalCash As Double, _
                        ByRef dblTotalSales As Double, _
                        ByRef dblPettyCashSection4Tot As Double, _
                        ByRef dblAcPaymentsSection4Tot As Double, _
                        ByRef intCntTenderTypes As Integer)

Dim oEntry                      As cPOSHeader
Dim oTenderOptions              As cTenderOptions
Dim colEntries                  As Collection

Dim dblSales(endgGroupDelivery) As Double
Dim dblSalesSubTotal            As Double

Dim lngEntryNo                  As Long
Dim intDepPnt                   As Integer
Dim intTenderType               As Integer
Dim strDepGrp                   As String
    
    
    ucProgress.Value = 0
    ucProgress.Visible = True
    sbStatus.Panels(PANEL_INFO).Text = "Populating grid - Sales"
    Me.Refresh
    
    
    dblTotalCash = 0
    
    ' Create single element that List must be retrieved for
    ' Table is DLTOTS  (CLASSID_POSHEADER)
    ' ====================================
    Set oEntry = goDatabase.CreateBusinessObject(CLASSID_POSHEADER)
    
    Call oEntry.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TranDate, GetDate(dtxtDate.Text))
    
    'Set up Transaction selection criteria
    '  *** Known Problem [1] ***
    '  *** we want to select records which are either
    '  *** Sales (SA) or Refunds (RF) or Paid In (M+) or Petty Cash (M-)
    '  *** but the business object cannot cater with multiple = selection.
    '  *** so   - Select <> "CC" AND <> "CO" (not equal to 'sign off' or 'sign on')
    '  *** then - Call AccumulateForReport if it is "SA" or "RF" or "M+" or "M-"
    
    'Call oEntry.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionCode, "SA")
    'Call oEntry.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_TransactionCode, "RF")
    
    Call oEntry.AddLoadFilter(CMP_NOTEQUAL, FID_POSHEADER_TransactionCode, "CC")
    Call oEntry.AddLoadFilter(CMP_NOTEQUAL, FID_POSHEADER_TransactionCode, "CO")
    
    Call oEntry.AddLoadFilter(CMP_EQUAL, FID_POSHEADER_Voided, False)
    Call oEntry.AddLoadFilter(CMP_LESSEQUALTHAN, FID_POSHEADER_CashierID, mstrMaxValidCashier)
    
    'Select fields to load from table
    Call oEntry.AddLoadField(FID_POSHEADER_TranDate)
    Call oEntry.AddLoadField(FID_POSHEADER_TransactionTime)
    Call oEntry.AddLoadField(FID_POSHEADER_TransactionNo)
    Call oEntry.AddLoadField(FID_POSHEADER_TransactionCode)
    Call oEntry.AddLoadField(FID_POSHEADER_OrderNumber)
    Call oEntry.AddLoadField(FID_POSHEADER_TotalSaleAmount)
    Call oEntry.AddLoadField(FID_POSHEADER_TaxAmount)
    Call oEntry.AddLoadField(FID_POSHEADER_DiscountAmount)
    Call oEntry.AddLoadField(FID_POSHEADER_TillID)
    Call oEntry.AddLoadField(FID_POSHEADER_CashierID)
    Call oEntry.AddLoadField(FID_POSHEADER_ReasonCode)
    Call oEntry.AddLoadField(FID_POSHEADER_Voided)
    
    'Make call on entries to return collection of all entries
    Set colEntries = oEntry.LoadMatches
    
    If colEntries.Count > 0 Then
        ucProgress.Max = colEntries.Count
    Else
        ucProgress.Max = 1
    End If
    
    
    For lngEntryNo = 1 To colEntries.Count Step 1
    
        ucProgress.Value = ucProgress.Value + 1
        
        ' Debug.Print lngEntryNo, colEntries(lngEntryNo).TransactionCode
        
        Select Case colEntries(lngEntryNo).TransactionCode
        Case TRANS_CODE_SALE, TRANS_CODE_REFUND, _
             TRANS_CODE_PAIDIN, TRANS_CODE_PETTYCASH
            
            ' This is a Sale (SA) or a Refund   (RF)
            '     or Paid In (M+) or Petty Cash (M-)
            
            Call ProcessEntry(lngEntryNo, colEntries, lngMaxRowLeft, _
                              dblSales(), _
                              dblSalesSubTotal, _
                              dblTotalTendered(), _
                              intCntTenderTypes, _
                              dblTotalCash, _
                              dblAccountSalesTot, _
                              dblAccountPaymentsTot, _
                              dblPettyCashSection4Tot, _
                              dblAcPaymentsSection4Tot, _
                              lngFirstAccntSalesRow, _
                              lngLastAccntSalesRow)
        Case Else
            ' Ignore other codes
            Debug.Print "lngEntryNo ="; lngEntryNo; _
                        " Transaction Code "; colEntries(lngEntryNo).TransactionCode; _
                        " ignored."
        End Select

    Next lngEntryNo
    
    If sprdData.MaxRows < 40 Then
        sprdData.MaxRows = 40
    End If
    
    ' show Section 1 of Daily sales Summary
    ' showing breakdown for-
    ' Tile Sales, Wood Products, Stone and Delivery
    ' ===================================================
    
    ' format column as a number with correct number of decimal places
    sprdData.Row = -1
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.CellType = CellTypeNumber
    sprdData.TypeNumberDecPlaces = mlngPayAmtDecPlaces ' set no of decimal places

    
    lngMaxRowRight = 4
    
    
    For intDepPnt = endgGroupTileSales To endgGroupDelivery Step 1
    
        Call SetGridRow(lngMaxRowRight, 1)  ' increment row
        
        Call SelectDepartmentGroup(intDepPnt, strDepGrp)
        
        sprdData.Col = COL_SMRY_DESCRIPT
        sprdData.Text = strDepGrp
        
        sprdData.Col = COL_SMRY_AMOUNT
        sprdData.Text = dblSales(intDepPnt)
        
    Next intDepPnt
    
    ' now add Sub Total
    Call SetGridRow(lngMaxRowRight, 1)  ' increment row
    
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.FontBold = True
    sprdData.Text = "Sub Total :-"
    
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.Text = dblSalesSubTotal
    
    
    ' show Section 2 of Daily sales Summary
    '   (a)  deposits received & deducted
    '       (Note: these were accumulated in FillInDeposits)
    ' & (b) Total sales
    ' ======================================================
    
    Call SetGridRow(lngMaxRowRight, 2)
    
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.Text = "Deposits Received"
    
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.Text = dblDepositsReceived
    
    Call SetGridRow(lngMaxRowRight, 1)  ' increment row
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.Text = "Deposits Deducted"
    
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.Text = dblDepositsDeducted
    
    ' show total sales
    Call SetGridRow(lngMaxRowRight, 2)

    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.FontBold = True
    sprdData.Text = "Total Sales"
        
    sprdData.Col = COL_SMRY_AMOUNT
    dblTotalSales = dblSalesSubTotal + dblDepositsReceived + dblDepositsDeducted
    sprdData.Text = dblTotalSales
    
    Call SetGridRow(lngMaxRowRight, 1)  ' increment row

    Call UnderScoreGrid(sprdData.Row, COL_SMRY_SPARE1, COL_SMRY_AMOUNT)
       
    
    ' show Section 3 of Daily sales Summary
    '  Tenders / Banking
    ' =====================================
    
'    'Tender Types
'    Set oTenderOptions = goDatabase.CreateBusinessObject(CLASSID_TENDEROPTIONS)
'
'    Call SetGridRow(lngMaxRowRight, 2)
'
'    sprdData.Col = COL_SMRY_DESCRIPT
'    sprdData.FontBold = True
'    sprdData.Text = "Tenders/Banking"
'
'
'    ' first cash
'    Call SetGridRow(lngMaxRowRight, 1)  ' increment row
'    sprdData.Col = COL_SMRY_DESCRIPT
'    lenb(If mstrCashPmtCode) <> 0 Then
'        Call oTenderOptions.ClearLoadFilter
'        Call oTenderOptions.AddLoadFilter(CMP_EQUAL, FID_TENDEROPTIONS_TenderNo, mstrCashPmtCode)
'        Call oTenderOptions.LoadMatches
'
'        sprdData.Text = oTenderOptions.Description
'    Else
'        sprdData.Text = "Cash"
'    End If
'
'    sprdData.Col = COL_SMRY_AMOUNT
'    sprdData.Text = dblTotalCash
'
'    dblTotalBanked = dblTotalBanked + dblTotalCash
'
'    ' then non-cash from array dblTotalTendered()
'
'    For intTenderType = 0 To intCntTenderTypes Step 1
'
'       dblTotalBanked = dblTotalBanked + dblTotalTendered(intTenderType)
'
'       If dblTotalTendered(intTenderType) <> 0 Then
'
'            Call SetGridRow(lngMaxRowRight, 1)  ' increment row
'
'            sprdData.Col = COL_SMRY_DESCRIPT
'            Call oTenderOptions.ClearLoadFilter
'            Call oTenderOptions.AddLoadFilter(CMP_EQUAL, FID_TENDEROPTIONS_TenderNo, Format$(intTenderType, "00"))
'            Call oTenderOptions.LoadMatches
'
'            sprdData.Text = oTenderOptions.Description
'
'            sprdData.Col = COL_SMRY_AMOUNT
'            sprdData.Text = dblTotalTendered(intTenderType)
'
'        End If
'
'    Next intTenderType'
'    Call SetGridRow(lngMaxRowRight, 1)  ' increment row
'    sprdData.Col = COL_SMRY_DESCRIPT
'    sprdData.FontBold = True
'    sprdData.Text = "Total Banked :-"
'
'    sprdData.Col = COL_SMRY_AMOUNT
'    sprdData.Text = dblTotalBanked

    
    ' clean up
    On Error Resume Next
    
    sbStatus.Panels(PANEL_INFO).Text = vbNullString
    ucProgress.Visible = False
    
    Set oEntry = Nothing
    
    
    Set oTenderOptions = Nothing
    Set colEntries = Nothing
    

End Sub

Private Sub SelectDepartmentGroup(ByVal intDepPnt As Integer, _
                                  ByRef strDepGrp As String)
    
    ' INPUT: intDepPnt  - the department group number in the range 0 to 3
    ' OUTPUT: strDepGrp - the name to be used for this department number
    
    Select Case intDepPnt
    Case Is = endgGroupTileSales
      strDepGrp = "Tile Sales"
    Case Is = endgGroupWoodProducts
      strDepGrp = "Wood Products"
    Case Is = endgGroupStoneProducts ' 06/06/03 KVB remove Jasba
      strDepGrp = "Stone Products"
    Case Is = endgGroupDelivery
      strDepGrp = "Delivery"
    Case Else
      strDepGrp = "UNKNOWN" ' error condition
    End Select

End Sub

Private Sub Form_Resize()
Dim lSpacing As Long

    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdData.Left
    sbStatus.Top = Me.Height
    'Check form is not below minimum width
    If Me.Width < cmdPrint.Width + cmdExit.Width + (lSpacing * 4) Then
        Me.Width = cmdPrint.Width + cmdExit.Width + (lSpacing * 4)
        Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < 6480 Then
        Me.Height = 6480
        Exit Sub
    End If
    
    'relocate Progress bar
    If (Me.Width - ucProgress.Width) > 0 Then
        ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    Else
        ucProgress.Left = 0
    End If
    If (Me.Height - ucProgress.Height) > 0 Then
        ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    Else
        ucProgress.Top = 0
    End If
    
    'start resizing
    sprdData.Width = sbStatus.Width - sprdData.Left * 2
    
    sprdData.Height = sbStatus.Top - (cmdExit.Height + sprdData.Top + lSpacing)
    'sprdData.Height = Me.Height - (cmdExit.Height + sprdData.Top + (lSpacing * 3) + (BORDER_WIDTH * 4) + sbStatus.Height)
    'sprdData.Height = Me.Height - (sprdData.Top + (lSpacing * 8))
    
    cmdExit.Top = sprdData.Top + sprdData.Height + lSpacing
    cmdPrint.Top = cmdExit.Top
    
    cmdPrint.Left = sbStatus.Width - cmdPrint.Width - lSpacing
    
End Sub

Private Function DrawLineAcross(ByVal lngRow As Long)
    
    If lngRow > sprdData.MaxRows Then
        sprdData.MaxRows = lngRow
    End If
    
    sprdData.Row = lngRow
    
    ' set the height of the row specified to 1 with background colour of black
    sprdData.RowHeight(sprdData.Row) = 1
    sprdData.Col = -1
    sprdData.BackColor = vbBlack
    sprdData.ForeColor = vbWhite
    
End Function

Private Function UnderScoreGrid(ByVal lngRow As Long, _
                                ByVal lngFromCol As Long, _
                                ByVal lngToCol As Long)
    
    Dim lngNumCols As Long
    
    If lngRow > sprdData.MaxRows Then
        sprdData.MaxRows = lngRow
    End If
    
    sprdData.Row = lngRow
    
    For lngNumCols = lngFromCol To lngToCol Step 1
        sprdData.Col = lngNumCols
        sprdData.TypeEditMultiLine = False
        sprdData.CellType = CellTypeEdit
        sprdData.FontBold = True
        'sprdData.TypeHAlign = TypeHAlignRight
        sprdData.Text = Space$(50)
        sprdData.FontStrikethru = True
    Next lngNumCols
    
End Function

Private Sub DisplayThisDeposit(ByVal strCustName As String, _
                               ByVal strDepositNo As String, _
                               ByVal strDeductType As String, _
                               ByVal dblThisAmount As Double, _
                               ByVal blnErrorDetected As Boolean)

    ' this routine adds display information to the grid for one Deposit record
    
    sprdData.MaxRows = sprdData.MaxRows + 1
    sprdData.Row = sprdData.MaxRows
    
    sprdData.Col = COL_DEP_CUST_NAME
    sprdData.Text = strCustName
    
    sprdData.Col = COL_DEP_NO
    sprdData.Text = strDepositNo
    
    sprdData.Col = COL_DEP_CODE
    If blnErrorDetected = True Then
        sprdData.BackColor = vbRed
    End If
    sprdData.Text = strDeductType
    
    sprdData.Col = COL_DEP_AMOUNT
    sprdData.Text = dblThisAmount
    
End Sub

Private Sub ProcessEntry(ByVal lngEntryNo As Long, _
                         ByVal colEntries As Collection, _
                         ByRef lngMaxRowLeft As Long, _
                         ByRef dblSales() As Double, _
                         ByRef dblSalesSubTotal As Double, _
                         ByRef dblTotalTendered() As Double, _
                         ByRef intCntTenderTypes As Integer, _
                         ByRef dblTotalCash As Double, _
                         ByRef dblAccountSalesTot As Double, _
                         ByRef dblAccountPaymentsTot As Double, _
                         ByRef dblPettyCashSection4Tot As Double, _
                         ByRef dblAcPaymentsSection4Tot As Double, _
                         ByRef lngFirstAccntSalesRow As Long, _
                         ByRef lngLastAccntSalesRow As Long)
    
Dim oLine                As cPOSLine
Dim oPayment             As cPOSPayment
Dim colLines             As Collection
Dim colPayments          As Collection
Dim lngLineNo            As Long
Dim lngPaymentNo         As Long
Dim intPntSalesTot       As Integer
Dim dblThisSaleTendered  As Double
Dim dblThisSaleAccntSale As Double
Dim dblThisSaleCash      As Double
Dim dblTotalOtherTender  As Double
Dim strSort              As String

    ' Get Lines, if any for Sale
    ' from table DLLINE  (CLASSID_POSLINE)
    '======================================
    Set oLine = goDatabase.CreateBusinessObject(CLASSID_POSLINE)
    
    ' Filters are TillId, Transaction date & transaction number
    Call oLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TillID, colEntries(lngEntryNo).TillID)
    Call oLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TranDate, colEntries(lngEntryNo).TranDate)
    Call oLine.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TransactionNumber, colEntries(lngEntryNo).TransactionNo)

    ' Fields required are PartCode, Description etc
    Call oLine.AddLoadField(FID_POSLINE_PartCode)
    Call oLine.AddLoadField(FID_POSLINE_QuantitySold)
    Call oLine.AddLoadField(FID_POSLINE_ExtendedValue)
    Call oLine.AddLoadField(FID_POSLINE_LookUpPrice)
    Call oLine.AddLoadField(FID_POSLINE_ExtendedCost)
    Call oLine.AddLoadField(FID_POSLINE_DepartmentNumber)
    
    ' we can't use Actual Price Excluding VAT because it only has zeroes in it!
    'Call oLine.AddLoadField(FID_POSLINE_ActualPriceExVAT)
        
    'Make call on entries to return collection of all entries
    Set colLines = oLine.LoadMatches
    

    ' Go through all of the lines for this sale
    
    For lngLineNo = 1 To colLines.Count Step 1
            
        ' Debug.Print colLines(lngLineNo).DepartmentNumber
        
        ' set the pointer to the department group
        ' NB this is NOT the same as DepartmentNumber
        '    3 Department Numbers belong to 3 specific groups
        '    all others           belong to 'Tile Sales'
        ' ===================================================
        Select Case colLines(lngLineNo).DepartmentNumber
        Case Is = 38            ' Wood Products
          intPntSalesTot = endgGroupWoodProducts
        Case Is = 4             ' Jasba Stand  ' 06/06/03 KVB remove Jasba
          intPntSalesTot = endgGroupStoneProducts ' 06/06/03 KVB remove Jasba
        Case Is = 97            ' Delivery
          intPntSalesTot = endgGroupDelivery
        Case Else               ' everything else goes into Tile Sales
          intPntSalesTot = endgGroupTileSales
        End Select
    
        ' NOTE 1: we want to accumulate  Sale Price INCLUDING VAT
        ' NOTE 2: Sale Price Inc VAT is ExtendedValue
        ' NOTE 3: IF we need Sale Price EXCLUDING VAT have to use hard coded formula

        ' dblSalePriceExcVat = Val(Format$(colLines(lngLineNo).ExtendedValue / 1.175, "0.000"))
        
        ' Sale Price INCLUDING VAT
        dblSales(intPntSalesTot) = dblSales(intPntSalesTot) + CDbl(colLines(lngLineNo).ExtendedValue)
        dblSalesSubTotal = dblSalesSubTotal + CDbl(colLines(lngLineNo).ExtendedValue)
            
        '' Sale Price EXCLUDING VAT
        'dblSales(intPntSalesTot) = dblSales(intPntSalesTot) + dblSalePriceExcVat
        'dblSalesSubTotal = dblSalesSubTotal + dblSalePriceExcVat
        
    Next lngLineNo
    
    ' *************** Payment Lines ****************
    ' Get Payment Lines, if any for Sale
    ' from table DLPAID (CLASSID_POSPAYMENT)
    ' so we can accumulate payments info for this entry
    ' =================================================
    Set oPayment = goDatabase.CreateBusinessObject(CLASSID_POSPAYMENT)
    
    Call oPayment.AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TillID, colEntries(lngEntryNo).TillID)
    Call oPayment.AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionDate, colEntries(lngEntryNo).TranDate)
    Call oPayment.AddLoadFilter(CMP_EQUAL, FID_POSPAYMENT_TransactionNumber, colEntries(lngEntryNo).TransactionNo)
    
    ' we need to retrieve TenderType and TenderAmount
    Call oPayment.AddLoadField(FID_POSPAYMENT_TenderType)
    Call oPayment.AddLoadField(FID_POSPAYMENT_TenderAmount)
    
    'Make call on entries to return collection of all entries
    Set colPayments = oPayment.LoadMatches
    
    ' accumulate total cheque / mastercard payments etc for this Sale
    ' because any remainder is Cash.
    
    dblThisSaleTendered = 0
    dblThisSaleAccntSale = 0
    dblThisSaleCash = 0
    
    For lngPaymentNo = 1 To colPayments.Count Step 1
        
        ' nb 100 paid is -100 in colPayments(lngPaymentNo).TenderAmount
        '     so here we reverse the sign.
        dblThisSaleTendered = dblThisSaleTendered + (-1 * colPayments(lngPaymentNo).TenderAmount)
        
        ' if tender type is equal to mlngAccntSaleTenderType
        ' then this is an account sale payment
        If colPayments(lngPaymentNo).TenderType = mlngAccntSaleTenderType Then
            dblThisSaleAccntSale = dblThisSaleAccntSale + (-1 * colPayments(lngPaymentNo).TenderAmount)
        End If
        
        'Debug.Print "TenderType = "; colPayments(lngPaymentNo).TenderType
        'If colPayments(lngPaymentNo).TenderType = 10 Then
        '    Debug.Print "Equals HO_REFUND"  ' <<<<<<<
        'End If
        
        ' if the tender type is larger than allowed for in array dblTotalTendered()
        ' make the array bigger.
        If colPayments(lngPaymentNo).TenderType > UBound(dblTotalTendered) Then
            ReDim Preserve dblTotalTendered(colPayments(lngPaymentNo).TenderType)
            intCntTenderTypes = colPayments(lngPaymentNo).TenderType
        End If
        
        ' accumulate in array of tendered (NB This is NOT cash)
        dblTotalTendered(colPayments(lngPaymentNo).TenderType) = dblTotalTendered(colPayments(lngPaymentNo).TenderType) - colPayments(lngPaymentNo).TenderAmount
                    
        Debug.Print "** lngEntryNo="; lngEntryNo;
        Debug.Print " TenderType="; colPayments(lngPaymentNo).TenderType;
        Debug.Print " Amount="; colPayments(lngPaymentNo).TenderAmount; " ***"
                    
    Next lngPaymentNo


    ' now subtract this from the Total Sale Amount
    dblThisSaleCash = colEntries(lngEntryNo).TotalSaleAmount - dblThisSaleTendered
                
    dblTotalCash = dblTotalCash + dblThisSaleCash
    dblTotalOtherTender = dblTotalOtherTender + dblThisSaleTendered
    
    
    Debug.Print "** lngEntryNo="; lngEntryNo;
    Debug.Print " TotalSaleAmount="; colEntries(lngEntryNo).TotalSaleAmount;
    Debug.Print " dblThisSaleCash="; dblThisSaleCash;
    Debug.Print " dblTotalOtherTender="; dblTotalOtherTender;
    Debug.Print " dblTotalCash="; dblTotalCash; " ***"
    
    ' is this (a) a trader  - TradersCard contains e.g. 00026296
    ' & also  (b) an account customer - AccountType contains "A"
    ' if it is we need to add to the left hand column
    ' for either - A/C Sales
    '         or - A/C Payments
    
    If (colEntries(lngEntryNo).ReasonCode > 0) Then
'        If (moMiscIncomeDesc Is Nothing) = True Then
'            Set moMiscIncomeDesc = goDatabase.CreateBusinessObject(CLASSID_TILLLOOKUPS)
'            Call moMiscIncomeDesc.AddLoadField(FID_TILLLOOKUPS_MiscIncomeAccountNo)
'            Call moMiscIncomeDesc.LoadMatches
'        End If
        madblMiscInc(colEntries(lngEntryNo).ReasonCode) = madblMiscInc(colEntries(lngEntryNo).ReasonCode) + colEntries(lngEntryNo).TotalSaleAmount
    End If

    MsgBox "MiscPaidOut Items Removed"
    ' is this a 'Petty Cash' record? (needed for Section4 totals)
    ' if it is (a) it will be of type "M-"
    '     and  (b) ReasonCode must point to a MiscPaidOutDeposit() value of False
    
'    If colEntries(lngEntryNo).TransactionCode = TRANS_CODE_PETTYCASH Then
'        ' ReasonCode points to  TilllookUps
'        If colEntries(lngEntryNo).ReasonCode > 0 Then
'            Set oTillLookUp = goDatabase.CreateBusinessObject(CLASSID_TILLLOOKUPS)
'            Call oTillLookUp.LoadMatches
'            'Debug.Print "XXX "; colEntries(lngEntryNo).TransactionCode,
'            'Debug.Print oTillLookUp.MiscPaidOutDeposit(colEntries(lngEntryNo).ReasonCode);
'            'Debug.Print " "; colEntries(lngEntryNo).TotalSaleAmount
'            ' if false is NOT a deposit but is Petty Cash !!!
'            If oTillLookUp.MiscPaidOutDeposit(colEntries(lngEntryNo).ReasonCode) = False Then
'                ' add in the cash amount
'                dblPettyCashSection4Tot = dblPettyCashSection4Tot - dblThisSaleCash
'            End If
'        End If
'    End If
'
'    ' is this a 'A/C Payments' record? (needed for Section4 totals)
'    ' if it is (a) it will be of type "M+"
'    '     and  (b) ReasonCode must point to a MiscPaidOutDeposit() value of False
'
'    If colEntries(lngEntryNo).TransactionCode = TRANS_CODE_PAIDIN Then
'        ' ReasonCode points to  TilllookUps
'        If colEntries(lngEntryNo).ReasonCode > 0 Then
'            Set oTillLookUp = goDatabase.CreateBusinessObject(CLASSID_TILLLOOKUPS)
'            Call oTillLookUp.LoadMatches
'            'Debug.Print "XXX "; colEntries(lngEntryNo).TransactionCode,
'            'Debug.Print oTillLookUp.MiscPaidOutDeposit(colEntries(lngEntryNo).ReasonCode);
'            ' if false is NOT a deposit but is Petty Cash !!!
'            If oTillLookUp.MiscPaidOutDeposit(colEntries(lngEntryNo).ReasonCode) = False Then
'                ' add in the total amount
'                dblAcPaymentsSection4Tot = dblAcPaymentsSection4Tot - colEntries(lngEntryNo).TotalSaleAmount
'                'Debug.Print " "; colEntries(lngEntryNo).TotalSaleAmount
'            End If
'        End If
'    End If
    
    On Error Resume Next
    Set oLine = Nothing
    Set colLines = Nothing
    Set oPayment = Nothing
    Set colPayments = Nothing
'    Set oTillLookUp = Nothing
    
End Sub

Private Sub SetGridRow(ByRef lngRowNo As Long, ByVal lngAddToRow As Long)
    
    lngRowNo = lngRowNo + lngAddToRow
    
    If lngRowNo > sprdData.MaxRows Then
        sprdData.MaxRows = lngRowNo
    End If
    
    sprdData.Row = lngRowNo
    
End Sub

Private Sub SortAccountSales(ByRef dblAccountSalesTot As Double, _
                             ByRef dblAccountPaymentsTot As Double, _
                             ByRef lngMaxRowLeft As Long, _
                             ByVal lngFirstAccntSalesRow As Long, _
                             ByVal lngLastAccntSalesRow As Long)
    
    If lngLastAccntSalesRow = 0 Then
        Exit Sub
    End If
    
    Call SetGridRow(lngLastAccntSalesRow, 1)
            
    ' display the A/C PaymentsReceived Total
    ' ======================================
    sprdData.Col = COL_DEP_CUST_NAME
    sprdData.FontBold = True
    sprdData.Text = "A/C Payments Total"
    sprdData.Col = COL_SORT
    sprdData.Text = "1" & "ZZ" & "99998"
    sprdData.Col = COL_DEP_AMOUNT
    sprdData.Text = dblAccountPaymentsTot
    
    ' add a blank line
    Call SetGridRow(lngLastAccntSalesRow, 1)
    sprdData.Col = COL_SORT
    sprdData.Text = "1" & "ZZ" & "99999"
    
    
    ' display the Credit Sales Total
    ' ===============================
    Call SetGridRow(lngLastAccntSalesRow, 1)
    
    ' the line we are on needs storing for sort
    lngMaxRowLeft = lngLastAccntSalesRow
    
    sprdData.Col = COL_DEP_CUST_NAME
    sprdData.FontBold = True
    sprdData.Text = "Credit Sales Total"
    sprdData.Col = COL_SORT
    sprdData.Text = "2" & "ZZ" & "99999"
    
    sprdData.Col = COL_DEP_AMOUNT
    sprdData.Text = dblAccountSalesTot
    
    ' Sort A/C Payments Received & Credit Sales using the grid's own sort facility
    Call sprdData.Sort(COL_DEP_HEADER, _
                       lngFirstAccntSalesRow, COL_DEP_AMOUNT, _
                       lngLastAccntSalesRow, _
                       SortByRow, COL_SORT)
    
End Sub

Private Sub FillInSummarySection4(ByRef lngMaxRowLeft As Long, _
                                  ByRef lngMaxRowRight As Long, _
                                  ByVal dblAccountSalesTot As Double, _
                                  ByVal dblAccountPaymentsTot As Double, _
                                  ByRef dblTotalTendered() As Double, _
                                  ByVal dblTotalCash As Double, _
                                  ByRef dblTotalSales As Double, _
                                  ByRef dblTotalBanked As Double, _
                                  ByRef dblPettyCashSection4Tot As Double, _
                                  ByRef dblAcPaymentsSection4Tot As Double, _
                                  ByVal strComment1 As String, _
                                  ByVal strComment2 As String)

Dim dblTotalAccounted As Double
Dim dblTillError      As Double
Dim intTenderType     As Integer
Dim intHoRefund       As Integer
Dim oTenderOptions    As cTenderOptions

    ' show Section 4 of Daily Sales Summary
    '  A/C Sales, A/C Payments, H/O Refunds, Petty Cash etc
    ' =====================================================
    
    Call SetGridRow(lngMaxRowRight, 2)

    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.Text = "A/C Sales"
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.Text = dblAccountSalesTot
    
    Call SetGridRow(lngMaxRowRight, 1)
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.Text = "A/C Payments"
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.Text = dblAcPaymentsSection4Tot
    
    ' find where H/O Refunds are!
    
    'Tender Types
    MsgBox "HO_REFUND search removed"
'    Set oTenderTypes = goDatabase.CreateBusinessObject(CLASSID_TENDERTYPES)
'    Call oTenderTypes.Load
'
'    For intTenderType = 1 To 20 Step 1
'        ' Debug.Print intTenderType, oTenderTypes.TenderType(intTenderType)
'        If oTenderTypes.TenderType(intTenderType) = HO_REFUND Then
'            intHoRefund = intTenderType
'            Exit For
'        End If
'    Next intTenderType
    
    Call SetGridRow(lngMaxRowRight, 1)
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.Text = "H/O Refunds"
    sprdData.Col = COL_SMRY_AMOUNT
    
    If UBound(dblTotalTendered) < intHoRefund Then
        ReDim Preserve dblTotalTendered(intHoRefund)
    End If
    sprdData.Text = dblTotalTendered(intHoRefund)
    
    
    Call SetGridRow(lngMaxRowRight, 1)
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.Text = "Petty Cash"
    sprdData.Col = COL_SMRY_AMOUNT
    sprdData.Text = dblPettyCashSection4Tot
    
    ' Total Accounted
    Call SetGridRow(lngMaxRowRight, 1)
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.FontBold = True
    sprdData.Text = "Total Accounted"
    sprdData.Col = COL_SMRY_AMOUNT
    dblTotalAccounted = dblTotalBanked + dblAccountSalesTot + _
                        dblAcPaymentsSection4Tot + dblTotalTendered(intHoRefund) + _
                        dblPettyCashSection4Tot
    sprdData.Text = dblTotalAccounted
    
    ' Till Error
    dblTillError = dblTotalAccounted - dblTotalSales
    Call SetGridRow(lngMaxRowRight, 2)
    sprdData.Col = COL_SMRY_DESCRIPT
    sprdData.FontBold = True
    sprdData.Text = "Till Error"
    sprdData.Col = COL_SMRY_AMOUNT
    'Leave conversion in as values < 0.01 are rounded to 0??
    sprdData.Text = Format$(dblTillError, "0.000")
    
    sprdData.Col = COL_SMRY_COMMENT
    Select Case dblTillError
    Case Is > 0
      sprdData.Text = "Over"
    Case Is < 0
      sprdData.Text = "Under"
    End Select
    
    ' 02/06/03 add comments (a) Explain till error &
    '                       (b) Margin comments
    
    If LenB(strComment1) <> 0 Then
        Call SetGridRow(lngMaxRowRight, 2)
        
        If lngMaxRowRight < lngMaxRowLeft Then
            ' have to equalise the rows
            ' as using multiline in right hand side.
            lngMaxRowRight = lngMaxRowLeft
            Call SetGridRow(lngMaxRowRight, 1)
        End If
        sprdData.Col = COL_SMRY_DESCRIPT
        sprdData.Text = "Explain Till Error"
        sprdData.FontBold = True
        Call SetGridRow(lngMaxRowRight, 1)
        sprdData.Col = COL_SMRY_DESCRIPT
        Call sprdData.AddCellSpan(COL_SMRY_DESCRIPT, sprdData.Row, 3, 1)
        sprdData.TypeEditMultiLine = True
        sprdData.Text = strComment1
        sprdData.RowHeight(lngMaxRowRight) = sprdData.MaxTextRowHeight(lngMaxRowRight)
    End If
    
    If LenB(strComment2) <> 0 Then
        Call SetGridRow(lngMaxRowRight, 2)
        If lngMaxRowRight < lngMaxRowLeft Then
            ' have to equalise the rows
            ' as using multiline in right hand side.
            lngMaxRowRight = lngMaxRowLeft
            Call SetGridRow(lngMaxRowRight, 1)
        End If
        sprdData.Col = COL_SMRY_DESCRIPT
        sprdData.Text = "Margin Comments"
        sprdData.FontBold = True
        Call SetGridRow(lngMaxRowRight, 1)
        sprdData.Col = COL_SMRY_DESCRIPT
        Call sprdData.AddCellSpan(COL_SMRY_DESCRIPT, sprdData.Row, 3, 1)
        sprdData.TypeEditMultiLine = True
        sprdData.Text = strComment2
        sprdData.RowHeight(lngMaxRowRight) = sprdData.MaxTextRowHeight(lngMaxRowRight)
    End If

    Call SetGridRow(lngMaxRowRight, 2)
    Call UnderScoreGrid(sprdData.Row, COL_SMRY_SPARE1, COL_SMRY_AMOUNT)

    Set oTenderOptions = Nothing
    
End Sub

Private Sub FillInBanking(ByRef lngMaxRowRight As Long, _
                          ByRef dblTotalBanked As Double, _
                          ByRef strComment1 As String, _
                          ByRef strComment2 As String)

    MsgBox "FillInBanking Removed"

'Dim oCashHeader   As cCashBalanceHeader
'Dim oCashControl  As cCashBalanceControl
'Dim oTenderType   As cTenderTypes
'Dim dteDate       As Date
'Dim intTenderNo   As Integer
'Dim adblTotals()  As Double
'Dim astrTotDesc() As String
'
'    ReDim adblTotals(3)
'    ReDim astrTotDesc(3)
'    astrTotDesc(1) = "Cash/Cheque"
'    astrTotDesc(2) = "MasterCard/Visa"
'    astrTotDesc(3) = "Switch/Solo"
'    ' dtxtDate.Text = "26/03/03" ' <<< test only
'
'    dteDate = GetDate(dtxtDate.Text)
'
'    dblTotalBanked = 0
'    strComment1 = vbNullString
'    strComment2 = vbNullString
'
'    Call SetGridRow(lngMaxRowRight, 2)
'
'    sprdData.Col = COL_SMRY_DESCRIPT
'    sprdData.FontBold = True
'    sprdData.Text = "Tenders/Banking"
'
'    ' there should be just one record in CBSHDR
'    Set oCashHeader = goDatabase.CreateBusinessObject(CLASSID_CASHBALANCEHDR)
'    Call oCashHeader.LoadMatches
'    Debug.Print oCashHeader.CurrentDate
'
'    'get tender types to convert description to System Tender ID
'    Set oTenderType = goDatabase.CreateBusinessObject(CLASSID_TENDERTYPES)
'    Call oTenderType.Load
'
'
''    If DisplayDate(oCashHeader.CurrentDate, False) <> dtxtDate.Text Then
''        Call SetGridRow(lngMaxRowRight, 1)
''        sprdData.Col = COL_SMRY_DESCRIPT
''        sprdData.Text = "(Not current record)"
''        sprdData.Col = COL_SMRY_AMOUNT
''        sprdData.Text = 0
''    End If
'
'
'    ' read CBSCTL records for dteDate
'    ' Cash balance Control - CLASSID_CASHBALANCECONTROL
'
'    Set oCashControl = goDatabase.CreateBusinessObject(CLASSID_CASHBALANCECONTROL)
'    Call oCashControl.AddLoadFilter(CMP_EQUAL, FID_CASHBALANCECONTROL_ControlDate, dteDate)
'    Call oCashControl.LoadMatches
'
'    ' oCashControl.ControlDate will contain -
'    '    a valid date e.g. 21/03/03 if records exist
'    '    00:00:00 if there are NO records for this date.
'
'    If Year(oCashControl.ControlDate) > 1899 Then
'
'        strComment1 = oCashControl.Comment1
'        strComment2 = oCashControl.Comment2
'
'        For intTenderNo = 1 To 40 Step 1
'
'            Debug.Print intTenderNo, oCashControl.BankingAmount(intTenderNo);
'            Debug.Print oCashHeader.TenderType(intTenderNo)
'
'            If (lenb(oCashHeader.TenderType(intTenderNo)) <> 0) Or (oCashControl.BankingAmount(intTenderNo) <> 0) Then
'                dblTotalBanked = dblTotalBanked + oCashControl.BankingAmount(intTenderNo)
''                Call SetGridRow(lngMaxRowRight, 1)
''                sprdData.Col = COL_SMRY_DESCRIPT
''                sprdData.Text = oCashHeader.tendertype(intTenderNo)
''                sprdData.Col = COL_SMRY_AMOUNT
''                sprdData.Text = oCashControl.BankingAmount(intTenderNo)
'                Select Case (oTenderType.TenderType(intTenderNo))
'                    Case (1): 'Cash
'                                adblTotals(1) = adblTotals(1) + oCashControl.BankingAmount(intTenderNo)
'                    Case (2): 'Cheque
'                                adblTotals(1) = adblTotals(1) + oCashControl.BankingAmount(intTenderNo)
'                    Case (4): 'Mastercard
'                                adblTotals(2) = adblTotals(2) + oCashControl.BankingAmount(intTenderNo)
'                    Case (5): 'Visa
'                                adblTotals(2) = adblTotals(2) + oCashControl.BankingAmount(intTenderNo)
'                    Case (7): 'Switch
'                                adblTotals(3) = adblTotals(3) + oCashControl.BankingAmount(intTenderNo)
'                    Case (9): 'Solo
'                                adblTotals(3) = adblTotals(3) + oCashControl.BankingAmount(intTenderNo)
'                End Select
'            End If
'
'        Next intTenderNo
'    End If
'
'    For intTenderNo = 1 To 3 Step 1
'        Call SetGridRow(lngMaxRowRight, 1)
'        sprdData.Col = COL_SMRY_DESCRIPT
'        sprdData.Text = astrTotDesc(intTenderNo)
'        sprdData.Col = COL_SMRY_AMOUNT
'        sprdData.Text = adblTotals(intTenderNo)
'    Next intTenderNo
'
'
'    Call SetGridRow(lngMaxRowRight, 1)  ' increment row
'    sprdData.Col = COL_SMRY_DESCRIPT
'    sprdData.FontBold = True
'    sprdData.Text = "Total Banked :-"
'
'    sprdData.Col = COL_SMRY_AMOUNT
'    sprdData.Text = dblTotalBanked
'
End Sub

