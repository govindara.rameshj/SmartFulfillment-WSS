VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmRequestAudit 
   Caption         =   "Label Request Audit"
   ClientHeight    =   7800
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14910
   Icon            =   "frmPrintLabels.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7800
   ScaleWidth      =   14910
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraSortBy 
      Caption         =   "Display Order"
      Height          =   1575
      Left            =   7500
      TabIndex        =   7
      Top             =   120
      Width           =   2595
      Begin VB.CommandButton cmdSortBy 
         Caption         =   "Sort"
         Height          =   375
         Left            =   1500
         TabIndex        =   10
         Top             =   780
         Width           =   915
      End
      Begin VB.ComboBox cmbSortBy 
         Height          =   315
         ItemData        =   "frmPrintLabels.frx":0A96
         Left            =   780
         List            =   "frmPrintLabels.frx":0AA3
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   360
         Width           =   1635
      End
      Begin VB.Label Label1 
         Caption         =   "Sort By"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   420
         Width           =   735
      End
   End
   Begin VB.Frame fraHHTHeaders 
      Caption         =   "HHT Price Labels Requests Summary"
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7275
      Begin FPSpreadADO.fpSpread sprdHHT 
         Height          =   1215
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   6855
         _Version        =   458752
         _ExtentX        =   12091
         _ExtentY        =   2143
         _StockProps     =   64
         DisplayColHeaders=   0   'False
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   7
         MaxRows         =   5
         ScrollBars      =   0
         SpreadDesigner  =   "frmPrintLabels.frx":0ABE
         UserResize      =   1
      End
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   4680
      TabIndex        =   3
      Top             =   2280
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9 - Print"
      Height          =   435
      Left            =   8280
      TabIndex        =   5
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   435
      Left            =   120
      TabIndex        =   4
      Top             =   6660
      Width           =   1155
   End
   Begin FPSpreadADO.fpSpread sprdSKUs 
      Height          =   4635
      Left            =   120
      TabIndex        =   2
      Top             =   1800
      Visible         =   0   'False
      Width           =   14655
      _Version        =   458752
      _ExtentX        =   25850
      _ExtentY        =   8176
      _StockProps     =   64
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   19
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      RowsFrozen      =   1
      SpreadDesigner  =   "frmPrintLabels.frx":1574
      UserResize      =   2
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   7425
      Width           =   14910
      _ExtentX        =   26300
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmPrintLabels.frx":1F37
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19103
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "18:51"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmRequestAudit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/11/06
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Labels Request/frmRequestAudit.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*      TS  Start Date followed by one of the following =  >  <
'*                           e.g. (/P='TS=20030326,  or  (/P>'TS>20030429,
'*      DB  Dated - when called from Daily Banking
'*                           e.g. (/P='DB=20030326
'*      TD  End date         e.g. TD20030329
'*      WE  Week Ending      e.g. WE=20030329     (note date should be a Saturday)
'*      SB  Show Button      if parameter is present Cash & Other Totals CAN be revealed
'*                           if parameter is absent  Cash & Other Totals cannot be revealed
'*      DC  Destination Code DCP Printer or DCS Preview
'*      CF  Called From      If CFC then the form is called in
'*                           in a automatic (Close) fashion.
'*
'**********************************************************************************************
'* SAMPLE (/P='DB=20031020,SB,STO,DCS,GRD,DL0' /u='044' -c='ffff')
'</CAMH>***************************************************************************************
Option Explicit

Const COL_SKU       As Long = 1
Const COL_DESC      As Long = 2
Const COL_PRICE     As Long = 3
Const COL_SM_REQ    As Long = 5
Const COL_SM_DONE    As Long = 6
Const COL_MED_REQ    As Long = 8
Const COL_MED_DONE    As Long = 9
Const COL_LRG_REQ    As Long = 11
Const COL_LRG_DONE    As Long = 12
Const COL_PLANOGRAM    As Long = 14
Const COL_REQ_DATE  As Long = 15
Const COL_OTHER_PLANS As Long = 16
Const COL_PRIME_PLAN As Long = 17
Const COL_SORT_LOC  As Long = 18
Const COL_HHT_ID  As Long = 19


Const PRM_TRANSNO   As Long = 911
Const PRM_LABELS_PATH As Long = 500
Const PRM_LABELS_PAGE As Long = 501
Const PRM_REF_834_SWITCHABLE As Long = 980834

Private mdteSystemDate  As Date
Private mblnAutoApply   As Boolean

Private mblnFromNightlyClose As Boolean
Private mstrDestCode         As Boolean

Dim moTempFilBO      As cTempFil
Dim mcolHHTLabels    As Collection
Dim mlngNoSmlLblPage As Long
Dim mlngNoMedLblPage As Long
Dim mlngNoLrgLblPage As Long

Private Sub cmdExit_Click()

    End

End Sub

Private Sub cmdPrint_Click()

Dim strHeader   As String
Dim oSysOptBO   As cSystemOptions
Dim lngLineNo   As Long

    Screen.MousePointer = vbHourglass
    'If printed in PlanoGram order then hide all non Primary Planogram Locations
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 1) Then
        sprdSKUs.Redraw = False
        For lngLineNo = 2 To sprdSKUs.MaxRows Step 1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_PRIME_PLAN
            If (sprdSKUs.Text <> "") Then
                sprdSKUs.Col = COL_SKU
                lngLineNo = lngLineNo + 1
                sprdSKUs.Row = lngLineNo
                While (lngLineNo <= sprdSKUs.MaxRows) And (sprdSKUs.Text = "")
                    sprdSKUs.Col = COL_SKU
                    lngLineNo = lngLineNo + 1
                    sprdSKUs.Row = lngLineNo
                Wend
                lngLineNo = lngLineNo - 1
            Else
                sprdSKUs.RowHidden = True
            End If
        Next lngLineNo
        sprdSKUs.Redraw = True
    End If
    
    Set oSysOptBO = goSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreNumber)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreName)
    Call oSysOptBO.LoadMatches

    ' 02/05/03  KVB  remove Printed dd/mm/yy from strHeader
    strHeader = "Store No: " & oSysOptBO.StoreNumber & " " & _
                oSysOptBO.StoreName & "/n/cLabels Request Audit on " & Format(Date, "DD/MM/YY")
    
    ' /c is Centre text      /fb1 is Font bold on
    sprdSKUs.PrintHeader = "/c/fb1" & strHeader
    
    sprdSKUs.PrintJobName = "Labels Request Audit Report"
       
    sprdSKUs.PrintFooter = GetFooter

    sprdSKUs.PrintOrientation = PrintOrientationLandscape
    
    sprdSKUs.Refresh
    
    sprdSKUs.PrintFooter = GetFooter
    sprdSKUs.PrintRowHeaders = False
    sprdSKUs.Refresh
    sprdSKUs.PrintScalingMethod = PrintScalingMethodSmartPrint
    sprdSKUs.PrintSheet
    Screen.MousePointer = vbNormal
    
    'If printed in PlanoGram order then show all non Primary Planogram Locations
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 1) Then
        sprdSKUs.Redraw = False
        For lngLineNo = 2 To sprdSKUs.MaxRows Step 1
            sprdSKUs.Row = lngLineNo
            If (sprdSKUs.RowHidden = True) Then sprdSKUs.RowHidden = False
        Next lngLineNo
        sprdSKUs.Redraw = True
    End If

End Sub

Private Sub cmdReset_Click()
        
    If (sprdSKUs.Visible = True) And (sprdSKUs.MaxRows > 0) Then
        If (MsgBoxEx("Ignore SKU's selected for printing?", vbInformation + vbYesNo, "Reset Criteria", , , , , RGBMSGBox_PromptColour) = vbNo) Then Exit Sub
    End If
    sprdSKUs.MaxRows = 0
    cmdPrint.Visible = False

End Sub

Private Sub cmdSortBy_Click()

Dim lngLineNo As Long
Dim vntPGLocs As Variant
Dim lngLocNo  As Integer
Dim strPlan   As String
Dim lngStart  As Long

    sprdSKUs.Redraw = False
    Screen.MousePointer = vbHourglass
    For lngLineNo = 1 To sprdSKUs.MaxRows Step 1
        Call sprdSKUs.RemoveCellSpan(COL_SKU, lngLineNo)
    Next lngLineNo
    Call sprdSKUs.RemoveCellSpan(COL_SM_REQ, 1)
    Call sprdSKUs.RemoveCellSpan(COL_MED_REQ, 1)
    Call sprdSKUs.RemoveCellSpan(COL_LRG_REQ, 1)
    'Group all info lines together
    sprdSKUs.SortKey(1) = COL_PRIME_PLAN
    sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdSKUs.Sort(-1, 2, -1, sprdSKUs.MaxRows, SortByRow)
    'delete info lines
    lngLineNo = sprdSKUs.SearchCol(COL_PRIME_PLAN, 2, sprdSKUs.MaxRows, "", SearchFlagsGreaterOrEqual)
    If lngLineNo > 0 Then
        Call sprdSKUs.DeleteRows(lngLineNo, (sprdSKUs.MaxRows - lngLineNo) + 1)
        sprdSKUs.MaxRows = lngLineNo - 1
    End If
    'sort by selected criteria (SKU/Plangram
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 0) Or (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 2) Then
        If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 0) Then
            sprdSKUs.SortKey(1) = COL_SKU
        Else
            sprdSKUs.SortKey(1) = COL_HHT_ID
        End If
        sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdSKUs.Sort(-1, 2, -1, sprdSKUs.MaxRows, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdSKUs.MaxRows To 2 Step -1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        If (lngStart = -1) Then lngStart = sprdSKUs.Row
                    Else
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdSKUs.AddCellSpan(COL_SKU, lngStart, COL_PLANOGRAM - 1, sprdSKUs.Row - lngStart + 1)
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
            End If

        Next lngLineNo
    End If
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 1) Then
        sprdSKUs.SortKey(1) = COL_SKU
        sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdSKUs.Sort(-1, 2, -1, sprdSKUs.MaxRows, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdSKUs.MaxRows To 2 Step -1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        Call sprdSKUs.CopyRowRange(lngLineNo, lngLineNo, sprdSKUs.Row)
                        sprdSKUs.Col = COL_PRIME_PLAN
                        sprdSKUs.Text = ""
                    End If
                    sprdSKUs.Col = COL_SORT_LOC
                    sprdSKUs.Text = vntPGLocs(lngLocNo)
                    sprdSKUs.Col = COL_PLANOGRAM
                    sprdSKUs.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                End If
            Next lngLocNo
        Next lngLineNo
        'Sort by Plan Gram Locations
        sprdSKUs.SortKey(1) = COL_SORT_LOC
        sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdSKUs.Sort(-1, 2, -1, sprdSKUs.MaxRows, SortByRow)
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdSKUs.MaxRows To 2 Step -1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            sprdSKUs.Col = COL_SORT_LOC
            strPlan = sprdSKUs.Text
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (strPlan <> vntPGLocs(lngLocNo)) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        If (lngStart = -1) Then lngStart = sprdSKUs.Row
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdSKUs.AddCellSpan(COL_SKU, lngStart, COL_PLANOGRAM - 1, sprdSKUs.Row - lngStart + 1)
'                Call sprdSKUs.AddCellSpan(COL_NO_LABELS, lngStart, COL_MANAGER - COL_PLANOGRAM, sprdSKUs.Row - lngStart + 1)
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
            End If
        Next lngLineNo
    End If
    Call sprdSKUs.AddCellSpan(COL_SM_REQ, 1, 2, 1)
    Call sprdSKUs.AddCellSpan(COL_MED_REQ, 1, 2, 1)
    Call sprdSKUs.AddCellSpan(COL_LRG_REQ, 1, 2, 1)
    Screen.MousePointer = vbNormal
    sprdSKUs.Redraw = True
    
End Sub

Private Sub Form_Load()

Dim oRetOpt As cRetailOptions
Dim oSysDat As cSystemDates
Dim strPage As String
Dim vntPage As Variant
Dim lngOptNo As Long

    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    Call sprdSKUs.SetOddEvenRowColor(RGB(224, 224, 224), vbBlack, vbWhite, vbBlack)
    
    Call InitialiseStatusBar(sbStatus)
    
    cmbSortBy.ListIndex = 0
    Call Me.Show
    DoEvents
    
    strPage = goSession.GetParameter(PRM_LABELS_PAGE)
    vntPage = Split(strPage, ",")
    For lngOptNo = LBound(vntPage) To UBound(vntPage) Step 1
        Select Case (Left$(vntPage(lngOptNo), 1))
            Case ("S"): mlngNoSmlLblPage = Val(Mid(vntPage(lngOptNo), 2))
            Case ("M"): mlngNoMedLblPage = Val(Mid(vntPage(lngOptNo), 2))
            Case ("L"): mlngNoLrgLblPage = Val(Mid(vntPage(lngOptNo), 2))
        End Select
    Next lngOptNo
    
    Set oRetOpt = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oRetOpt.LoadMatches
    
    Set oRetOpt = Nothing
    
    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.LoadMatches
    
    mdteSystemDate = oSysDat.TodaysDate
    Set oSysDat = Nothing
    
    sprdSKUs.Row = 1
    Call sprdSKUs.AddCellSpan(COL_SM_REQ, 1, 2, 1)
    sprdSKUs.Col = COL_SM_REQ
    sprdSKUs.CellType = CellTypeStaticText
    sprdSKUs.Text = "Peg"
    sprdSKUs.TypeHAlign = TypeHAlignCenter
    
    Call sprdSKUs.AddCellSpan(COL_MED_REQ, 1, 2, 1)
    sprdSKUs.Col = COL_MED_REQ
    sprdSKUs.CellType = CellTypeStaticText
    sprdSKUs.Text = "Small"
    sprdSKUs.TypeHAlign = TypeHAlignCenter
    
    Call sprdSKUs.AddCellSpan(COL_LRG_REQ, 1, 2, 1)
    sprdSKUs.Col = COL_LRG_REQ
    sprdSKUs.CellType = CellTypeStaticText
    sprdSKUs.Text = "Medium"
    sprdSKUs.TypeHAlign = TypeHAlignCenter
    sprdSKUs.Col = -1
    sprdSKUs.Row = 1
    sprdSKUs.BackColor = RGB_LTGREY
 '   Call sprdSKUs.AddCellSpan(COL_SKU, 1, 4, 1)
    
    Call LoadHHTHeader
    
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 4500) Then
        Me.Width = 4500
        Exit Sub
    End If
    
    sprdSKUs.Width = Me.Width - sprdSKUs.Left * 4
    sprdSKUs.Height = Me.Height - sprdSKUs.Top - cmdPrint.Height - 720 - sbStatus.Height
    cmdPrint.Top = sprdSKUs.Top + sprdSKUs.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdPrint.Left = sprdSKUs.Left + sprdSKUs.Width - cmdPrint.Width
    ucpbProgress.Left = (Me.Width - ucpbProgress.Width) / 2
    ucpbProgress.Top = (Me.Height - 480 - ucpbProgress.Height) / 2

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF9): 'if F10 then exit
                    If (cmdPrint.Visible = True) Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

    
Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        'Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case (CALLED_FROM):
                If Mid(strSection, 3) = CF_CLOSE Then
                    mblnFromNightlyClose = True
                    blnSetOpts = True
                End If
        End Select
    Next lngSectNo
    

End Sub



Private Sub LoadHHTHeader()
    Const ROW_SMALL = 3
    Const ROW_MEDIUM = 4
    Const ROW_LARGE = 5
    
    Dim lngNoSmlReq     As Long
    Dim lngNoSmlDone    As Long
    Dim lngNoMedReq     As Long
    Dim lngNoMedDone    As Long
    Dim lngNoLrgReq     As Long
    Dim lngNoLrgDone    As Long
    
    Dim oItemBO     As cInventory
    Dim oHHTLabel   As cTempFil
    Dim lngRowNo    As Long

    Dim HHTLabelImplementationFactory As HHTLabelCommon.HHTLabelFactory
    Dim HHTLabelImplementation As HHTLabelCommon.IHHTLabel
    
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving Items"
    If (moTempFilBO Is Nothing) Then
        Screen.MousePointer = vbHourglass
        Call LocalLoadLabelPrintHeader(lngNoSmlReq, lngNoMedReq, lngNoLrgReq, lngNoSmlDone, lngNoMedDone, lngNoLrgDone)
        sprdHHT.Col = 2
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = lngNoSmlReq
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = lngNoMedReq
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = lngNoLrgReq
        sprdHHT.Col = 3
        sprdHHT.Row = ROW_SMALL
        If (lngNoSmlReq > 0) Then sprdHHT.Text = ((lngNoSmlReq - 1) \ mlngNoSmlLblPage) + 1
        sprdHHT.Row = ROW_MEDIUM
        If (lngNoMedReq > 0) Then sprdHHT.Text = ((lngNoMedReq - 1) \ mlngNoMedLblPage) + 1
        sprdHHT.Row = ROW_LARGE
        If (lngNoLrgReq > 0) Then sprdHHT.Text = ((lngNoLrgReq - 1) \ mlngNoLrgLblPage) + 1
        sprdHHT.Col = 4
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = lngNoSmlDone
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = lngNoMedDone
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = lngNoLrgDone
        sprdHHT.Col = 5
        sprdHHT.Row = ROW_SMALL
        If (lngNoSmlDone > 0) Then sprdHHT.Text = ((lngNoSmlDone - 1) \ mlngNoSmlLblPage) + 1
        sprdHHT.Row = ROW_MEDIUM
        If (lngNoMedDone > 0) Then sprdHHT.Text = ((lngNoMedDone - 1) \ mlngNoMedLblPage) + 1
        sprdHHT.Row = ROW_LARGE
        If (lngNoLrgDone > 0) Then sprdHHT.Text = ((lngNoLrgDone - 1) \ mlngNoLrgLblPage) + 1
        sprdHHT.Col = 6
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = lngNoSmlReq - lngNoSmlDone
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = lngNoMedReq - lngNoMedDone
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = lngNoLrgReq - lngNoLrgDone
        sprdHHT.Col = 7
        sprdHHT.Row = ROW_SMALL
        If (lngNoSmlReq - lngNoSmlDone > 0) Then sprdHHT.Text = ((lngNoSmlReq - lngNoSmlDone - 1) \ mlngNoSmlLblPage) + 1
        sprdHHT.Row = ROW_MEDIUM
        If (lngNoMedReq - lngNoMedDone > 0) Then sprdHHT.Text = ((lngNoMedReq - lngNoMedDone - 1) \ mlngNoMedLblPage) + 1
        sprdHHT.Row = ROW_LARGE
        If (lngNoLrgReq - lngNoLrgDone > 0) Then sprdHHT.Text = ((lngNoLrgReq - lngNoLrgDone - 1) \ mlngNoLrgLblPage) + 1
    End If
        
    If (mcolHHTLabels.Count > 0) Then ucpbProgress.Max = mcolHHTLabels.Count
    ucpbProgress.Caption1 = "Displaying Items"
    sprdSKUs.MaxRows = 1
    
    Set HHTLabelImplementationFactory = New HHTLabelCommon.HHTLabelFactory
    Call HHTLabelImplementationFactory.Initialise(goSession)
    Set HHTLabelImplementation = HHTLabelImplementationFactory.GetImplementation
    
    For lngRowNo = 1 To mcolHHTLabels.Count Step 1
        ucpbProgress.Value = lngRowNo
        Set oHHTLabel = mcolHHTLabels(lngRowNo)
        Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, HHTLabelImplementation.GetSkuFromKeyData(oHHTLabel.KeyData))
        Call oItemBO.AddLoadField(FID_INVENTORY_Description)
        Call oItemBO.AddLoadField(FID_INVENTORY_NormalSellPrice)
        Call oItemBO.AddLoadField(FID_INVENTORY_PriorSellPrice)
        Call oItemBO.AddLoadField(FID_INVENTORY_SupplierNo)
        Call oItemBO.AddLoadField(FID_INVENTORY_QuantityAtHand)
        Call oItemBO.AddLoadField(FID_INVENTORY_NonStockItem)
        Call oItemBO.AddLoadField(FID_INVENTORY_EAN)
        Call oItemBO.AddLoadField(FID_INVENTORY_ItemObsolete)
        Call oItemBO.LoadMatches
                
        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
        sprdSKUs.Row = sprdSKUs.MaxRows
        sprdSKUs.Col = COL_SKU
        sprdSKUs.Text = oItemBO.PartCode
        sprdSKUs.Col = COL_DESC
        sprdSKUs.Text = IIf(oItemBO.Description = "", "STKMAS Entry Missing", oItemBO.Description)
        sprdSKUs.Col = COL_PRICE
        sprdSKUs.Text = oItemBO.NormalSellPrice
        sprdSKUs.Col = COL_REQ_DATE
        sprdSKUs.Text = Format(oHHTLabel.KeyDate, goSession.GetParameter(PRM_DATE_FORMAT))
        sprdSKUs.Col = COL_HHT_ID
        sprdSKUs.Text = oHHTLabel.Id
        sprdSKUs.Col = COL_SM_REQ
        sprdSKUs.Text = oHHTLabel.NoLabels("S", True)
        sprdSKUs.Col = COL_SM_DONE
        sprdSKUs.Text = oHHTLabel.NoLabels("S", False)
        sprdSKUs.Col = COL_MED_REQ
        sprdSKUs.Text = oHHTLabel.NoLabels("M", True)
        sprdSKUs.Col = COL_MED_DONE
        sprdSKUs.Text = oHHTLabel.NoLabels("M", False)
        sprdSKUs.Col = COL_LRG_REQ
        sprdSKUs.Text = oHHTLabel.NoLabels("L", True)
        sprdSKUs.Col = COL_LRG_DONE
        sprdSKUs.Text = oHHTLabel.NoLabels("L", False)
        Call DisplayPlanogram(oItemBO.PartCode)
    Next lngRowNo
    
    ucpbProgress.Visible = False
    If (sprdSKUs.MaxRows > 0) Then
        sprdSKUs.Visible = True
        cmdPrint.Visible = True
        sprdSKUs.SetFocus
    End If
    
    cmbSortBy.ListIndex = 2
    Call cmdSortBy_Click
    sprdSKUs.ColWidth(COL_DESC) = sprdSKUs.MaxTextColWidth(COL_DESC) + 1
    sprdSKUs.ColWidth(COL_PLANOGRAM) = sprdSKUs.MaxTextColWidth(COL_PLANOGRAM) + 3
    Screen.MousePointer = vbNormal

End Sub

Private Sub DisplayPlanogram(strSKU As String)

Dim oPlanGramBO As cStock_Wickes.cPlangram
Dim strPGLocs   As String
Dim strLocation As String
Dim colPGLocs   As Collection
Dim strFirstLoc As String
Dim lngFirstLoc As Long
Dim lngLocNo    As Long
        
    'TCH1230 - Added Plangram sequencing
    Set oPlanGramBO = goDatabase.CreateBusinessObject(CLASSID_PLANGRAM)
    Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_PartCode, strSKU)
    Set colPGLocs = oPlanGramBO.LoadMatches
    While (colPGLocs.Count > 0)
        strFirstLoc = "ZZZZZZZZZZZZZ"
        For lngLocNo = 1 To colPGLocs.Count Step 1
            Set oPlanGramBO = colPGLocs(lngLocNo)
            strLocation = String(10 - Len(Str(oPlanGramBO.PlanNumber)), "0") & oPlanGramBO.PlanNumber & "/" & String(10 - Len(oPlanGramBO.PlanSegment), "0") & oPlanGramBO.PlanSegment & "/" & String(10 - Len(oPlanGramBO.ShelfNumber), "0") & oPlanGramBO.ShelfNumber
            If (strFirstLoc > strLocation) Then
                lngFirstLoc = lngLocNo
                strFirstLoc = strLocation
            End If
        Next
        Set oPlanGramBO = colPGLocs(lngFirstLoc)
'        strPGLocs = strPGLocs & oPlanGramBO.PlanNumber & "/" & oPlanGramBO.PlanSegment & "/" & oPlanGramBO.ShelfNumber & "-" & oPlanGramBO.PlanName & "##"  '& oPlanGramBO.ShelfNumber & "-" & oPlanGramBO.PlanName & "##"
        strPGLocs = strPGLocs & strFirstLoc & "-" & oPlanGramBO.PlanName & "##"
        Set oPlanGramBO = Nothing
        Call colPGLocs.Remove(lngFirstLoc)
    Wend
    sprdSKUs.Col = COL_OTHER_PLANS
    sprdSKUs.Text = strPGLocs
    If (strPGLocs = "") Then sprdSKUs.Text = "N/A" 'flag with something to show as a valid line
    sprdSKUs.Col = COL_PRIME_PLAN
    sprdSKUs.Text = 1

End Sub

Private Function DecodeLocation(strLocation As String) As String

Dim strLoc  As String

    strLoc = "/" & strLocation
    While (InStr(strLoc, "/0") > 0)
        strLoc = Replace(strLoc, "/0", "/")
    Wend
    DecodeLocation = Mid$(strLoc, 2)

End Function


Private Sub LocalLoadLabelPrintHeader(ByRef lngNoSmlReq As Long, ByRef lngNoMedReq As Long, ByRef lngNoLrgReq As Long, ByRef lngNoSmlDone As Long, ByRef lngNoMedDone As Long, ByRef lngNoLrgDone As Long)
    Dim rsTmpFil    As New ADODB.Recordset
    Dim rsStock     As ADODB.Recordset
    Dim oBO         As cTempFil
    Dim sData       As String
    Dim lFieldNo    As Long

    Dim HHTLabelImplementationFactory As HHTLabelCommon.HHTLabelFactory
    Dim HHTLabelImplementation As HHTLabelCommon.IHHTLabel
    Dim SummaryRecordFKeyIdentifier As String
        
    Set HHTLabelImplementationFactory = New HHTLabelCommon.HHTLabelFactory
    Call HHTLabelImplementationFactory.Initialise(goSession)
    Set HHTLabelImplementation = HHTLabelImplementationFactory.GetImplementation
    SummaryRecordFKeyIdentifier = HHTLabelImplementation.LabelRequestSummaryRecordFKeyIdentifier

    ' Create new or clear previous entries
    If mcolHHTLabels Is Nothing Then
        Set mcolHHTLabels = New Collection
    Else
        On Error GoTo SkipSwitch
        If goSession.GetParameter(PRM_REF_834_SWITCHABLE) = True Then
            Call ClearLabelCollection
        Else
SkipSwitch:
            On Error GoTo 0
            Err.Clear
            With mcolHHTLabels
                Do While .Count > 0
                    Call .Remove(0)
                Loop
            End With
        End If
    End If
    With rsTmpFil
        On Error GoTo SkipAgain
        If goSession.GetParameter(PRM_REF_834_SWITCHABLE) = True Then
            Call .Open("select * from TMPFIL where FKEY like 'PCPLRQ%' and FKEY <> '" & SummaryRecordFKeyIdentifier & "'", goDatabase.Connection)
        Else
SkipAgain:
            On Error GoTo 0
            Err.Clear
            Call .Open("select * from TMPFIL where FKEY like 'PCPLRQ%'", goDatabase.Connection)
        End If
        If Not .EOF = True Then
            .MoveFirst
            Do While Not .EOF
                Set oBO = goSession.Database.CreateBusinessObject(CLASSID_TEMPFIL)
                For lFieldNo = 0 To .Fields.Count - 1 Step 1
                    With .Fields.Item(lFieldNo)
                        If Not IsNull(.Value) Then
                            Select Case .Name
                                Case "TKEY"
                                    oBO.Id = .Value
                                Case "WSID"
                                    oBO.WorkstationID = .Value
                                Case "FKEY"
                                    oBO.KeyData = .Value
                                Case "DATE1"
                                    oBO.KeyDate = .Value
                                Case "DATA"
                                    oBO.Value = .Value
                                Case "DAT1"
                                    oBO.Data1 = .Value
                                Case "DAT2"
                                    oBO.Data2 = .Value
                                Case "DAT3"
                                    oBO.Data3 = .Value
                                Case "DAT4"
                                    oBO.Data4 = .Value
                            End Select
                        End If
                    End With
                Next lFieldNo
                Call mcolHHTLabels.Add(oBO)
                .MoveNext
            Loop
        End If
        Call .Close
        
        Call .Open("select DATA from TMPFIL where FKEY = '" & SummaryRecordFKeyIdentifier & "'", goDatabase.Connection)
        If Not .EOF = True Then
            .MoveFirst
            sData = .Fields.Item(0).Value
            lngNoSmlReq = 0
            lngNoMedReq = 0
            lngNoLrgReq = 0
            lngNoSmlDone = 0
            lngNoMedDone = 0
            lngNoLrgDone = 0
            lngNoSmlReq = Val(Mid(sData, 14, 6))
            lngNoMedReq = Val(Mid(sData, 20, 6))
            lngNoLrgReq = Val(Mid(sData, 26, 6))
            lngNoSmlDone = Val(Mid(sData, 32, 6))
            lngNoMedDone = Val(Mid(sData, 38, 6))
            lngNoLrgDone = Val(Mid(sData, 44, 6))
        End If
    End With
End Sub

'Referral 834
Private Sub ClearLabelCollection()
    'Clear the collection
    With mcolHHTLabels
        Do While .Count > 0
            Call .Remove(.Count)
        Loop
    End With
End Sub

