VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cGrossMargins"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "RVB_UniqueId" ,"408FC85D03D7"
'<CAMH>****************************************************************************************
'* Module : cGrossMargins
'* Date   : 29/04/04
'* Author : damians
'*$Archive: /Projects/OasysV2/VB/Summaries BO/cGrossMargins.cls $
'**********************************************************************************************
'* Summary: Business Object to wrap the GMRSKU table
'**********************************************************************************************
'* $Author: Damians $
'* $Date: 29/04/04 14:28 $
'* $Revision: 2 $
'* Versions:
'* 29/04/04    damians
'*             Header added.
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Const MODULE_NAME As String = "cGrossMargins"

Implements IBo
Implements ISysBo

'##ModelId=4090A879038A
Private mPartCode As String

'##ModelId=4090A89201F4
Private mDepartmentCode As String

'##ModelId=4090A9F4037A
Private mGroup As String

'##ModelId=4090AA340261
Private mSalesUnits(6) As Long

'##ModelId=4090AAA70222
Private mSalesValue(6) As Double

'##ModelId=4090AC120290
Private mSalesCost(6) As Double

'##ModelId=4090AAF70213
Private mStockHoldingAtCost(6) As Double

'##ModelId=4090AB1102BF
Private mDaysStoreOpen(6) As Long

'##ModelId=4090AB550399
Private mDaysStockHeld(6) As Long

Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'##ModelId=4090AE000222
Public Property Get DaysStockHeld(Index As Variant) As Long
    Let DaysStockHeld = mDaysStockHeld(Index)
End Property

'##ModelId=4090AE0000CB
Public Property Let DaysStockHeld(Index As Variant, ByVal Value As Long)
    Let mDaysStockHeld(Index) = Value
End Property

'##ModelId=4090ADFF03C8
Public Property Get DaysStoreOpen(Index As Variant) As Long
    Let DaysStoreOpen = mDaysStoreOpen(Index)
End Property

'##ModelId=4090ADFF0280
Public Property Let DaysStoreOpen(Index As Variant, ByVal Value As Long)
    Let mDaysStoreOpen(Index) = Value
End Property

'##ModelId=4090ADFF01B5
Public Property Get StockHoldingAtCost(Index As Variant) As Double
    Let StockHoldingAtCost = mStockHoldingAtCost(Index)
End Property

'##ModelId=4090ADFF008C
Public Property Let StockHoldingAtCost(Index As Variant, ByVal Value As Double)
    Let mStockHoldingAtCost(Index) = Value
End Property

'##ModelId=4090ADFE038A
Public Property Get SalesCost(Index As Variant) As Double
    Let SalesCost = mSalesCost(Index)
End Property

'##ModelId=4090ADFE0271
Public Property Let SalesCost(Index As Variant, ByVal Value As Double)
    Let mSalesCost(Index) = Value
End Property

'##ModelId=4090ADFE01A5
Public Property Get SalesValue(Index As Variant) As Double
    Let SalesValue = mSalesValue(Index)
End Property

'##ModelId=4090ADFE008C
Public Property Let SalesValue(Index As Variant, ByVal Value As Double)
    Let mSalesValue(Index) = Value
End Property

'##ModelId=4090ADFD03A9
Public Property Get SalesUnits(Index As Variant) As Long
    Let SalesUnits = mSalesUnits(Index)
End Property

'##ModelId=4090ADFD029F
Public Property Let SalesUnits(Index As Variant, ByVal Value As Long)
    Let mSalesUnits(Index) = Value
End Property

'##ModelId=4090ADFD0242
Public Property Get Group() As String
   Let Group = mGroup
End Property

'##ModelId=4090ADFD0196
Public Property Let Group(ByVal Value As String)
    Let mGroup = Value
End Property

'##ModelId=4090ADFD0138
Public Property Get DepartmentCode() As String
   Let DepartmentCode = mDepartmentCode
End Property

'##ModelId=4090ADFD008C
Public Property Let DepartmentCode(ByVal Value As String)
    Let mDepartmentCode = Value
End Property

'##ModelId=4090ADFD002E
Public Property Get PartCode() As String
   Let PartCode = mPartCode
End Property


'##ModelId=4090ADFC036B
Public Property Let PartCode(ByVal Value As String)
    Let mPartCode = Value
End Property

Private Function Load() As Boolean
    
' Load up all properties from the database
Dim oView       As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean

    IBo_Save = Save(SaveTypeAllCases)

End Function

Public Function IBo_SaveIfNew() As Boolean

    IBo_SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean

    IBo_SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean

    IBo_Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean

    IBo_IsValid = IsValid()

End Function


'##ModelId=3D3D19F30316
Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Private Function Delete() As Boolean
    Debug.Assert False
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function


Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow As IRow
Dim nFid As Long
    
    Set oRow = m_oSession.Root.CreateUtilityObject("CRow")
    For nFid = (CLASSID_GROSSMARGINS * &H10000) + 1 To FID_GROSSMARGINS_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter




'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField
Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()

    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = m_oSession.Root.CreateUtilityObject("CRowSelector")
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(lBONo)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cGrossMargins
End Function


Public Function IBo_GetField(nFid As Long) As IField
    
    Set IBo_GetField = GetField(nFid)

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    
    Set IBo_GetSelectAllRow = GetSelectAllRow

End Function

Public Function IBo_Load() As Boolean

    IBo_Load = Load

End Function


Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_GROSSMARGINS, FID_GROSSMARGINS_END_OF_STATIC, m_oSession)

End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_GROSSMARGINS_PartCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPartCode
        Case (FID_GROSSMARGINS_DepartmentCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDepartmentCode
        Case (FID_GROSSMARGINS_Group):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mGroup
        Case (FID_GROSSMARGINS_SalesUnits):             Set GetField = SaveLongArray(mSalesUnits, m_oSession)
        Case (FID_GROSSMARGINS_SalesValue):             Set GetField = SaveDoubleArray(mSalesValue, m_oSession)
        Case (FID_GROSSMARGINS_SalesCost):              Set GetField = SaveDoubleArray(mSalesCost, m_oSession)
        Case (FID_GROSSMARGINS_StockHoldingAtCost):     Set GetField = SaveDoubleArray(mStockHoldingAtCost, m_oSession)
        Case (FID_GROSSMARGINS_DaysStoreOpen):          Set GetField = SaveLongArray(mDaysStoreOpen, m_oSession)
        Case (FID_GROSSMARGINS_DaysStockHeld):          Set GetField = SaveLongArray(mDaysStockHeld, m_oSession)
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function Initialise(oSession As ISession) As cGrossMargins
    Set m_oSession = oSession
    Set Initialise = Me
End Function



Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_GROSSMARGINS

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = "GrossMargins" & mPartCode

End Property

Private Function IBo_Initialise(oSession As ISession) As IBo
    
    Set IBo_Initialise = Initialise(oSession)

End Function

Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    
    Call LoadFromRow(oRow)

End Function


'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_GROSSMARGINS_PartCode):           mPartCode = oField.ValueAsVariant
            Case (FID_GROSSMARGINS_DepartmentCode):     mDepartmentCode = oField.ValueAsVariant
            Case (FID_GROSSMARGINS_Group):              mGroup = oField.ValueAsVariant
            Case (FID_GROSSMARGINS_SalesUnits):         Call LoadLongArray(mSalesUnits, oField, m_oSession)
            Case (FID_GROSSMARGINS_SalesValue):         Call LoadDoubleArray(mSalesValue, oField, m_oSession)
            Case (FID_GROSSMARGINS_SalesCost):          Call LoadDoubleArray(mSalesCost, oField, m_oSession)
            Case (FID_GROSSMARGINS_StockHoldingAtCost): Call LoadDoubleArray(mStockHoldingAtCost, oField, m_oSession)
            Case (FID_GROSSMARGINS_DaysStoreOpen):      Call LoadLongArray(mDaysStoreOpen, oField, m_oSession)
            Case (FID_GROSSMARGINS_DaysStockHeld):      Call LoadLongArray(mDaysStockHeld, oField, m_oSession)
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property



Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
        
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()

End Function

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_GROSSMARGINS_END_OF_STATIC

End Function

Public Function Interface(Optional eInterfaceType As Long) As cGrossMargins

Dim oBO As IBo

    Select Case (eInterfaceType)
        Case BO_INTERFACE_IBO:
            Set oBO = Me
            Set IBo_Interface = oBO
        Case Else:
            Set IBo_Interface = Me
    End Select

End Function



