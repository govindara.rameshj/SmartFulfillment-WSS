VERSION 5.00
Begin VB.Form frmRemoveCommideaTill 
   Caption         =   "Remove Till from Commidea Tills Parameter"
   ClientHeight    =   165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5265
   LinkTopic       =   "Form1"
   ScaleHeight     =   165
   ScaleWidth      =   5265
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "frmRemoveCommideaTill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Parameters
Private Const PRM_COMMIDEA_TILLS As Long = 970001

Private moRoot As OasysRoot
Private moConnection As ADODB.Connection

Private Sub Form_Load()
    Dim astrTills() As String
    Dim intTill As Integer
    Dim strTillNo As String
    Dim strTillsUsingCommidea As String

    Set moRoot = GetRoot
    Set moConnection = New ADODB.Connection
    moConnection.Open OfflineDSN
    strTillNo = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    strTillsUsingCommidea = goSession.GetParameter(PRM_COMMIDEA_TILLS)
    astrTills = Split(strTillsUsingCommidea, ",")
    For intTill = 0 To UBound(astrTills)
        If Val(astrTills(intTill)) = Val(strTillNo) Then
            Exit For
        End If
    Next intTill
    If intTill <= UBound(astrTills) Then
        For intTill = intTill To UBound(astrTills) - 1
            astrTills(intTill) = astrTills(intTill + 1)
        Next intTill
        ReDim Preserve astrTills(UBound(astrTills) - 1) As String
        strTillsUsingCommidea = Join(astrTills, ",")
        Call goDatabase.ExecuteCommand("UPDATE " & Chr(34) & "Parameters" & Chr(34) & " SET StringValue='" & strTillsUsingCommidea & "' WHERE ParameterID=" & PRM_COMMIDEA_TILLS)
        Call moConnection.Execute("UPDATE " & Chr(34) & "Parameters" & Chr(34) & " SET StringValue='" & strTillsUsingCommidea & "' WHERE ParameterID=" & PRM_COMMIDEA_TILLS)
    End If
    Call moConnection.Close
    Set moConnection = Nothing
    Set moRoot = Nothing
    End
End Sub

Private Property Get OfflineDSN() As String
    Dim strKey, strValue As String
    Dim nResult As Long
    Dim oRegistry As OASYSCBASECOMLib.Registry
    
    Set oRegistry = moRoot.IOasysRoot_CreateUtilityObject("Registry")
    If Not (oRegistry Is Nothing) Then
        strKey = RelativeRegistryKey
        oRegistry.GetCTSStringValue strKey, "OfflineDSN", strValue, nResult
        If nResult = 0 Then
            OfflineDSN = strValue
            Debug.Print "Database for Enterprise " & goSession.CurrentEnterpriseId & " Offline DSN from registry: " & OfflineDSN
        End If
    End If
    If LenB(OfflineDSN) = 0 Then
        ' Mnemonic not set so use a default
        OfflineDSN = "OasysOffline"
        Debug.Print "Database (default) for Enterprise " & goSession.CurrentEnterpriseId & " Offline DSN from registry: " & OfflineDSN
    End If
    Set oRegistry = Nothing
End Property

Private Property Get RelativeRegistryKey() As String

    RelativeRegistryKey = goSession.CurrentEnterprise.IEnterprise_RelativeRegistryKey & "\Database"
End Property
