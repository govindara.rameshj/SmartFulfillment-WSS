VERSION 5.00
Begin VB.Form frmCommideaWorkstation 
   Caption         =   "Update Workstation to Commidea"
   ClientHeight    =   2340
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5265
   LinkTopic       =   "Form1"
   ScaleHeight     =   2340
   ScaleWidth      =   5265
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdQuit 
      Cancel          =   -1  'True
      Caption         =   "&Quit"
      Height          =   435
      Left            =   3990
      TabIndex        =   6
      Top             =   1785
      Width           =   1170
   End
   Begin VB.OptionButton optNoTills 
      Caption         =   "&No Tills"
      Height          =   435
      Left            =   210
      TabIndex        =   4
      Top             =   1575
      Width           =   1170
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "&Update"
      Default         =   -1  'True
      Height          =   435
      Left            =   2625
      TabIndex        =   5
      Top             =   1785
      Width           =   1170
   End
   Begin VB.TextBox txtTheseTills 
      Height          =   330
      Left            =   1995
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   1100
      Width           =   3165
   End
   Begin VB.OptionButton optMultiTills 
      Caption         =   "&These Tills..."
      Height          =   435
      Left            =   210
      TabIndex        =   2
      Top             =   1050
      Width           =   1695
   End
   Begin VB.OptionButton optThisTill 
      Caption         =   "This Till (00)  &Only"
      Height          =   435
      Left            =   210
      TabIndex        =   1
      Top             =   525
      Width           =   1590
   End
   Begin VB.Label lblCurrentTills 
      Caption         =   "Current Commidea Tills: "
      Height          =   330
      Left            =   210
      TabIndex        =   0
      Top             =   210
      Width           =   4950
   End
End
Attribute VB_Name = "frmCommideaWorkstation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Parameters
Private Const PRM_COMMIDEA_TILLS As Long = 970001

Private mstrTillNo As String

Private Sub cmdQuit_Click()

    Call Unload(Me)
End Sub

Private Sub cmdUpdate_Click()
    Dim strNewTillsUsingCommideaParamValue As String
    Dim astrTills() As String
    Dim intTill As Integer
    Dim astrTillsUsingCommidea() As String

    If optThisTill.Value Then
        strNewTillsUsingCommideaParamValue = CStr(Val(mstrTillNo))
    ElseIf optMultiTills.Value Then
        If txtTheseTills.Text <> "" Then
            astrTills = Split(txtTheseTills.Text, ",")
            For intTill = 0 To UBound(astrTills)
                strNewTillsUsingCommideaParamValue = strNewTillsUsingCommideaParamValue _
                                                   & CStr(Val(astrTills(intTill)))
                If intTill < UBound(astrTills) Then
                    strNewTillsUsingCommideaParamValue = strNewTillsUsingCommideaParamValue _
                                                       & ","
                End If
            Next intTill
        End If
'    ElseIf optAllTills.Value Then
'        strNewTillsUsingCommideaParamValue = "*"
    ElseIf optNoTills.Value Then
        strNewTillsUsingCommideaParamValue = ""
    End If
    goDatabase.ExecuteCommand ("UPDATE " & Chr(34) & "Parameters" & Chr(34) & " SET StringValue='" & strNewTillsUsingCommideaParamValue & "' WHERE ParameterID=" & PRM_COMMIDEA_TILLS)
    With lblCurrentTills
        .Caption = "Current Commidea Tills: "
        If strNewTillsUsingCommideaParamValue = "*" Then
            .Caption = .Caption & "All"
            txtTheseTills.Text = ""
        ElseIf strNewTillsUsingCommideaParamValue = "" Then
            .Caption = .Caption & "None"
            txtTheseTills.Text = ""
        Else
            astrTillsUsingCommidea = Split(strNewTillsUsingCommideaParamValue, ",")
            For intTill = 0 To UBound(astrTillsUsingCommidea)
                .Caption = .Caption & Right$("00" & astrTillsUsingCommidea(intTill), 2)
                If intTill < UBound(astrTillsUsingCommidea) Then
                    .Caption = .Caption & ","
                End If
            Next intTill
            txtTheseTills.Text = Trim(Mid(.Caption, InStr(1, .Caption, ":", vbTextCompare) + 1))
        End If
    End With
End Sub

Private Sub Form_Load()

    Call GetRoot
    If goSession.ISession_IsOnline Then
        mstrTillNo = Right$("00" & goSession.CurrentEnterprise.IEnterprise_WorkstationID, 2)
        With optThisTill
            .Caption = Replace(.Caption, "00", mstrTillNo)
        End With
    End If
    LoadExistingTills
    optMultiTills.Value = True
End Sub

Private Sub optMultiTills_Click()

    With txtTheseTills
        .Locked = Not optMultiTills.Value
        .Text = .Tag
    End With
End Sub

Private Sub optNoTills_Click()

    With txtTheseTills
        .Locked = Not optMultiTills.Value
        .Tag = .Text
        .Text = ""
    End With
End Sub

Private Sub optThisTill_Click()

    With txtTheseTills
        .Locked = Not optMultiTills.Value
        .Tag = .Text
        .Text = ""
    End With
End Sub

Private Sub LoadExistingTills()
    Dim strTillsUsingCommidea As String
    Dim astrTillsUsingCommidea() As String
    Dim strFormattedTills As String
    Dim intTill As Integer

    If goSession.ISession_IsOnline Then
        strTillsUsingCommidea = goSession.GetParameter(PRM_COMMIDEA_TILLS)
        With lblCurrentTills
            If strTillsUsingCommidea = "" Then
                .Caption = .Caption & "None"
            ElseIf strTillsUsingCommidea = "*" Then
                .Caption = .Caption & "All"
            Else
                .Caption = "Current Commidea Tills: "
                astrTillsUsingCommidea = Split(strTillsUsingCommidea, ",")
                For intTill = 0 To UBound(astrTillsUsingCommidea)
                    .Caption = .Caption & Right$("00" & astrTillsUsingCommidea(intTill), 2)
                    If intTill < UBound(astrTillsUsingCommidea) Then
                        .Caption = .Caption & ","
                    End If
                Next intTill
                txtTheseTills.Text = Trim(Mid(.Caption, InStr(1, .Caption, ":", vbTextCompare) + 1))
            End If
        End With
    End If
End Sub
