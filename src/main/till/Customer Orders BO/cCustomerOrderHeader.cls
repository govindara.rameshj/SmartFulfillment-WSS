VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCustOrderHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : HierarchyCategory
'* Date   : 21/10/04
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/InventoryBO/HierarchyCategory.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: johng $
'* $Date: 20/05/04 9:24 $
'* $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cCustOrderHeader"

' Private Storage for Properties
Private mOrderNumber             As String
Private mCustomerNumber          As String
Private mOrderDate               As Date
Private mCancelled               As String
Private mIsForDelivery           As Boolean
Private mDateRequired            As Date
Private mDeliveryConfirmed       As Boolean
Private mNoTimesAmended          As String
Private mDeliveryAddress1        As String
Private mDeliveryAddress2        As String
Private mDeliveryAddress3        As String
Private mDeliveryAddress4        As String
Private mPhoneNumber             As String
Private mPrinted                 As Boolean
Private mNoOfReprints            As Long
Private mRevisionNumber          As Long
Private mMerchandiseValue        As Currency
Private mDeliveryCharge          As Currency
Private mUnitsOrdered            As Long
Private mUnitsDelivered          As Long
Private mUnitsRefunded           As Long
Private mOrderWeight             As Currency
Private mOrderVolume             As Currency
Private mTranDate                As Date
Private mTillID                  As String
Private mTransactionNumber       As String
Private mRefundTranDate          As Date
Private mRefundTillID            As String
Private mRefundTransactionNumber As String

' Standard IBo private storage
Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'Properties
Public Property Get OrderNumber() As String
    OrderNumber = mOrderNumber
End Property

Public Property Let OrderNumber(ByVal Value As String)
    mOrderNumber = Value
End Property

Public Property Get CustomerNumber() As String
    CustomerNumber = mCustomerNumber
End Property

Public Property Let CustomerNumber(ByVal Value As String)
    mCustomerNumber = Value
End Property

Public Property Get OrderDate() As Date
    OrderDate = mOrderDate
End Property

Public Property Let OrderDate(ByVal Value As Date)
    mOrderDate = Value
End Property

Public Property Get Cancelled() As String
    Cancelled = mCancelled
End Property

Public Property Let Cancelled(ByVal Value As String)
    mCancelled = Value
End Property

Public Property Get IsForDelivery() As Boolean
    IsForDelivery = mIsForDelivery
End Property

Public Property Let IsForDelivery(ByVal Value As Boolean)
    mIsForDelivery = Value
End Property

Public Property Get DateRequired() As Date
    DateRequired = mDateRequired
End Property

Public Property Let DateRequired(ByVal Value As Date)
    mDateRequired = Value
End Property

Public Property Get DeliveryConfirmed() As Boolean
    DeliveryConfirmed = mDeliveryConfirmed
End Property

Public Property Let DeliveryConfirmed(ByVal Value As Boolean)
    mDeliveryConfirmed = Value
End Property

Public Property Get NoTimesAmended() As String
    NoTimesAmended = mNoTimesAmended
End Property

Public Property Let NoTimesAmended(ByVal Value As String)
    mNoTimesAmended = Value
End Property

Public Property Get DeliveryAddress1() As String
    DeliveryAddress1 = mDeliveryAddress1
End Property

Public Property Let DeliveryAddress1(ByVal Value As String)
    mDeliveryAddress1 = Value
End Property

Public Property Get DeliveryAddress2() As String
    DeliveryAddress2 = mDeliveryAddress2
End Property

Public Property Let DeliveryAddress2(ByVal Value As String)
    mDeliveryAddress2 = Value
End Property

Public Property Get DeliveryAddress3() As String
    DeliveryAddress3 = mDeliveryAddress3
End Property

Public Property Let DeliveryAddress3(ByVal Value As String)
    mDeliveryAddress3 = Value
End Property

Public Property Get DeliveryAddress4() As String
    DeliveryAddress4 = mDeliveryAddress4
End Property

Public Property Let DeliveryAddress4(ByVal Value As String)
    mDeliveryAddress4 = Value
End Property

Public Property Get PhoneNumber() As String
    PhoneNumber = mPhoneNumber
End Property

Public Property Let PhoneNumber(ByVal Value As String)
    mPhoneNumber = Value
End Property

Public Property Get Printed() As Boolean
    Printed = mPrinted
End Property

Public Property Let Printed(ByVal Value As Boolean)
    mPrinted = Value
End Property

Public Property Get NoOfReprints() As Long
    NoOfReprints = mNoOfReprints
End Property

Public Property Let NoOfReprints(ByVal Value As Long)
    mNoOfReprints = Value
End Property

Public Property Get RevisionNumber() As Long
    RevisionNumber = mRevisionNumber
End Property

Public Property Let RevisionNumber(ByVal Value As Long)
    mRevisionNumber = Value
End Property

Public Property Get MerchandiseValue() As Currency
    MerchandiseValue = mMerchandiseValue
End Property

Public Property Let MerchandiseValue(ByVal Value As Currency)
    mMerchandiseValue = Value
End Property

Public Property Get DeliveryCharge() As Currency
    DeliveryCharge = mDeliveryCharge
End Property

Public Property Let DeliveryCharge(ByVal Value As Currency)
    mDeliveryCharge = Value
End Property

Public Property Get UnitsOrdered() As Long
    UnitsOrdered = mUnitsOrdered
End Property

Public Property Let UnitsOrdered(ByVal Value As Long)
    mUnitsOrdered = Value
End Property

Public Property Get UnitsDelivered() As Long
    UnitsDelivered = mUnitsDelivered
End Property

Public Property Let UnitsDelivered(ByVal Value As Long)
    mUnitsDelivered = Value
End Property

Public Property Get UnitsRefunded() As Long
    UnitsRefunded = mUnitsRefunded
End Property

Public Property Let UnitsRefunded(ByVal Value As Long)
    mUnitsRefunded = Value
End Property

Public Property Get OrderWeight() As Currency
    OrderWeight = mOrderWeight
End Property

Public Property Let OrderWeight(ByVal Value As Currency)
    mOrderWeight = Value
End Property

Public Property Get OrderVolume() As Currency
    OrderVolume = mOrderVolume
End Property

Public Property Let OrderVolume(ByVal Value As Currency)
    mOrderVolume = Value
End Property

Public Property Get TranDate() As Date
    TranDate = mTranDate
End Property

Public Property Let TranDate(ByVal Value As Date)
    mTranDate = Value
End Property

Public Property Get TillID() As String
    TillID = mTillID
End Property

Public Property Let TillID(ByVal Value As String)
    mTillID = Value
End Property

Public Property Get TransactionNumber() As String
    TransactionNumber = mTransactionNumber
End Property

Public Property Let TransactionNumber(ByVal Value As String)
    mTransactionNumber = Value
End Property

Public Property Get RefundTranDate() As Date
    RefundTranDate = mRefundTranDate
End Property

Public Property Let RefundTranDate(ByVal Value As Date)
    mRefundTranDate = Value
End Property

Public Property Get RefundTillID() As String
    RefundTillID = mRefundTillID
End Property

Public Property Let RefundTillID(ByVal Value As String)
    mRefundTillID = Value
End Property

Public Property Get RefundTransactionNumber() As String
    RefundTransactionNumber = mRefundTransactionNumber
End Property

Public Property Let RefundTransactionNumber(ByVal Value As String)
    mRefundTransactionNumber = Value
End Property

Public Function IBo_Load() As Boolean
    IBo_Load = Load
End Function

Public Function Load() As Boolean
' Load up all properties from the database

Dim oView As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean
    IBo_Save = Save(SaveTypeAllCases)
End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean
    IBo_IsValid = IsValid()
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As IBo
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_cCustOrderHeader * &H10000) + 1 To FID_cCustOrderHeader_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()
    
    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCustOrderHeader

End Function


Public Function IBo_GetField(nFid As Long) As IField
    Set IBo_GetField = GetField(nFid)
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CUSTORDERHEADER_OrderNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrderNumber
        Case (FID_CUSTORDERHEADER_CustomerNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerNumber
        Case (FID_CUSTORDERHEADER_OrderDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mOrderDate
        Case (FID_CUSTORDERHEADER_Cancelled):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCancelled
        Case (FID_CUSTORDERHEADER_IsForDelivery):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mIsForDelivery
        Case (FID_CUSTORDERHEADER_DateRequired):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateRequired
        Case (FID_CUSTORDERHEADER_DeliveryConfirmed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mDeliveryConfirmed
        Case (FID_CUSTORDERHEADER_NoTimesAmended):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mNoTimesAmended
        Case (FID_CUSTORDERHEADER_DeliveryAddress1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddress1
        Case (FID_CUSTORDERHEADER_DeliveryAddress2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddress2
        Case (FID_CUSTORDERHEADER_DeliveryAddress3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddress3
        Case (FID_CUSTORDERHEADER_DeliveryAddress4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryAddress4
        Case (FID_CUSTORDERHEADER_PhoneNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPhoneNumber
        Case (FID_CUSTORDERHEADER_Printed):
                Set GetField = New CFieldBool
                GetField.ValueAsVariant = mPrinted
        Case (FID_CUSTORDERHEADER_NoOfReprints):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mNoOfReprints
        Case (FID_CUSTORDERHEADER_RevisionNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mRevisionNumber
        Case (FID_CUSTORDERHEADER_MerchandiseValue):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mMerchandiseValue
        Case (FID_CUSTORDERHEADER_DeliveryCharge):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mDeliveryCharge
        Case (FID_CUSTORDERHEADER_UnitsOrdered):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mUnitsOrdered
        Case (FID_CUSTORDERHEADER_UnitsDelivered):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mUnitsDelivered
        Case (FID_CUSTORDERHEADER_UnitsRefunded):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mUnitsRefunded
        Case (FID_CUSTORDERHEADER_OrderWeight):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrderWeight
        Case (FID_CUSTORDERHEADER_OrderVolume):
                Set GetField = New CFieldDouble
                GetField.ValueAsVariant = mOrderVolume
        Case (FID_CUSTORDERHEADER_TranDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mTranDate
        Case (FID_CUSTORDERHEADER_TillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTillID
        Case (FID_CUSTORDERHEADER_TransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTransactionNumber
        Case (FID_CUSTORDERHEADER_RefundTranDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mRefundTranDate
        Case (FID_CUSTORDERHEADER_RefundTillID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRefundTillID
        Case (FID_CUSTORDERHEADER_RefundTransactionNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRefundTransactionNumber
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    Set IBo_GetSelectAllRow = GetSelectAllRow
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_cCustOrderHeader, FID_cCustOrderHeader_END_OF_STATIC, m_oSession)

End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cCustOrderHeader
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CUSTORDERHEADER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = MODULE_NAME & ": " & mOrderNumber

End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    Call LoadFromRow(oRow)
End Function

Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CUSTORDERHEADER_OrderNumber):             mOrderNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_CustomerNumber):          mCustomerNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_OrderDate):               mOrderDate = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_Cancelled):               mCancelled = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_IsForDelivery):           mIsForDelivery = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DateRequired):            mDateRequired = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DeliveryConfirmed):       mDeliveryConfirmed = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_NoTimesAmended):          mNoTimesAmended = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DeliveryAddress1):        mDeliveryAddress1 = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DeliveryAddress2):        mDeliveryAddress2 = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DeliveryAddress3):        mDeliveryAddress3 = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DeliveryAddress4):        mDeliveryAddress4 = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_PhoneNumber):             mPhoneNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_Printed):                 mPrinted = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_NoOfReprints):            mNoOfReprints = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_RevisionNumber):          mRevisionNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_MerchandiseValue):        mMerchandiseValue = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_DeliveryCharge):          mDeliveryCharge = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_UnitsOrdered):            mUnitsOrdered = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_UnitsDelivered):          mUnitsDelivered = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_UnitsRefunded):           mUnitsRefunded = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_OrderWeight):             mOrderWeight = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_OrderVolume):             mOrderVolume = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_TranDate):                mTranDate = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_TillID):                  mTillID = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_TransactionNumber):       mTransactionNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_RefundTranDate):          mRefundTranDate = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_RefundTillID):            mRefundTillID = oField.ValueAsVariant
            Case (FID_CUSTORDERHEADER_RefundTransactionNumber): mRefundTransactionNumber = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_cCustOrderHeader_END_OF_STATIC

End Function



