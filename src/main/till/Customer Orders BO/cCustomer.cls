VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : HierarchyCategory
'* Date   : 27/06/05
'* Author : johng
'*$Archive: /Projects/OasysV2/VB/cCustomer Orders BP/cCustomer.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: johng $
'* $Date: 27/06/05 9:24 $
'* $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cCustomer"

' Private Storage for Properties
Private mCustomerNo           As String
Private mAlphaKey             As String
Private mTitle                As String
Private mInitials             As String
Private mSurname              As String
Private mAddressLine1         As String
Private mAddressLine2         As String
Private mAddressLine3         As String
Private mAddressLine4         As String
Private mPostCode             As String
Private mPhoneNo1             As String
Private mPhoneNo2             As String
Private mPhoneNo3             As String
Private mLifestyleAnswer1     As String
Private mLifestyleAnswer2     As String
Private mLifestyleAnswer3     As String
Private mLifestyleAnswer4     As String
Private mLifestyleAnswer5     As String
Private mLifestyleAnswer6     As String
Private mLifestyleAnswer7     As String
Private mLifestyleAnswer8     As String
Private mLifestyleAnswer9     As String
Private mLifestyleAnswer10    As String
Private mDeliveryInstructions As String

' Standard IBo private storage
Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'Properties
Public Property Get CustomerNo() As String
    CustomerNo = mCustomerNo
End Property

Public Property Let CustomerNo(ByVal Value As String)
    mCustomerNo = Value
End Property

Public Property Get AlphaKey() As String
    AlphaKey = mAlphaKey
End Property

Public Property Let AlphaKey(ByVal Value As String)
    mAlphaKey = Value
End Property

Public Property Get Title() As String
    Title = mTitle
End Property

Public Property Let Title(ByVal Value As String)
    mTitle = Value
End Property

Public Property Get Initials() As String
    Initials = mInitials
End Property

Public Property Let Initials(ByVal Value As String)
    mInitials = Value
End Property

Public Property Get Surname() As String
    Surname = mSurname
End Property

Public Property Let Surname(ByVal Value As String)
    mSurname = Value
End Property

Public Property Get AddressLine1() As String
    AddressLine1 = mAddressLine1
End Property

Public Property Let AddressLine1(ByVal Value As String)
    mAddressLine1 = Value
End Property

Public Property Get AddressLine2() As String
    AddressLine2 = mAddressLine2
End Property

Public Property Let AddressLine2(ByVal Value As String)
    mAddressLine2 = Value
End Property

Public Property Get AddressLine3() As String
    AddressLine3 = mAddressLine3
End Property

Public Property Let AddressLine3(ByVal Value As String)
    mAddressLine3 = Value
End Property

Public Property Get AddressLine4() As String
    AddressLine4 = mAddressLine4
End Property

Public Property Let AddressLine4(ByVal Value As String)
    mAddressLine4 = Value
End Property

Public Property Get PostCode() As String
    PostCode = mPostCode
End Property

Public Property Let PostCode(ByVal Value As String)
    mPostCode = Value
End Property

Public Property Get PhoneNo1() As String
    PhoneNo1 = mPhoneNo1
End Property

Public Property Let PhoneNo1(ByVal Value As String)
    mPhoneNo1 = Value
End Property

Public Property Get PhoneNo2() As String
    PhoneNo2 = mPhoneNo2
End Property

Public Property Let PhoneNo2(ByVal Value As String)
    mPhoneNo2 = Value
End Property

Public Property Get PhoneNo3() As String
    PhoneNo3 = mPhoneNo3
End Property

Public Property Let PhoneNo3(ByVal Value As String)
    mPhoneNo3 = Value
End Property

Public Property Get LifestyleAnswer1() As String
    LifestyleAnswer1 = mLifestyleAnswer1
End Property

Public Property Let LifestyleAnswer1(ByVal Value As String)
    mLifestyleAnswer1 = Value
End Property

Public Property Get LifestyleAnswer2() As String
    LifestyleAnswer2 = mLifestyleAnswer2
End Property

Public Property Let LifestyleAnswer2(ByVal Value As String)
    mLifestyleAnswer2 = Value
End Property

Public Property Get LifestyleAnswer3() As String
    LifestyleAnswer3 = mLifestyleAnswer3
End Property

Public Property Let LifestyleAnswer3(ByVal Value As String)
    mLifestyleAnswer3 = Value
End Property

Public Property Get LifestyleAnswer4() As String
    LifestyleAnswer4 = mLifestyleAnswer4
End Property

Public Property Let LifestyleAnswer4(ByVal Value As String)
    mLifestyleAnswer4 = Value
End Property

Public Property Get LifestyleAnswer5() As String
    LifestyleAnswer5 = mLifestyleAnswer5
End Property

Public Property Let LifestyleAnswer5(ByVal Value As String)
    mLifestyleAnswer5 = Value
End Property

Public Property Get LifestyleAnswer6() As String
    LifestyleAnswer6 = mLifestyleAnswer6
End Property

Public Property Let LifestyleAnswer6(ByVal Value As String)
    mLifestyleAnswer6 = Value
End Property

Public Property Get LifestyleAnswer7() As String
    LifestyleAnswer7 = mLifestyleAnswer7
End Property

Public Property Let LifestyleAnswer7(ByVal Value As String)
    mLifestyleAnswer7 = Value
End Property

Public Property Get LifestyleAnswer8() As String
    LifestyleAnswer8 = mLifestyleAnswer8
End Property

Public Property Let LifestyleAnswer8(ByVal Value As String)
    mLifestyleAnswer8 = Value
End Property

Public Property Get LifestyleAnswer9() As String
    LifestyleAnswer9 = mLifestyleAnswer9
End Property

Public Property Let LifestyleAnswer9(ByVal Value As String)
    mLifestyleAnswer9 = Value
End Property

Public Property Get LifestyleAnswer10() As String
    LifestyleAnswer10 = mLifestyleAnswer10
End Property

Public Property Let LifestyleAnswer10(ByVal Value As String)
    mLifestyleAnswer10 = Value
End Property

Public Property Get DeliveryInstructions() As String
    DeliveryInstructions = mDeliveryInstructions
End Property

Public Property Let DeliveryInstructions(ByVal Value As String)
    mDeliveryInstructions = Value
End Property


Public Function IBo_Load() As Boolean
    IBo_Load = Load
End Function

Public Function Load() As Boolean
' Load up all properties from the database

Dim oView As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean
    IBo_Save = Save(SaveTypeAllCases)
End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean
    IBo_IsValid = IsValid()
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As IBo
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_CUSTOMER * &H10000) + 1 To FID_CUSTOMER_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()
    
    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCustomer

End Function


Public Function IBo_GetField(nFid As Long) As IField
    Set IBo_GetField = GetField(nFid)
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CUSTOMER_CustomerNo):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerNo
        Case (FID_CUSTOMER_AlphaKey):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAlphaKey
        Case (FID_CUSTOMER_Title):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mTitle
        Case (FID_CUSTOMER_Initials):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mInitials
        Case (FID_CUSTOMER_Surname):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSurname
        Case (FID_CUSTOMER_AddressLine1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine1
        Case (FID_CUSTOMER_AddressLine2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine2
        Case (FID_CUSTOMER_AddressLine3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine3
        Case (FID_CUSTOMER_AddressLine4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mAddressLine4
        Case (FID_CUSTOMER_PostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPostCode
        Case (FID_CUSTOMER_PhoneNo1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPhoneNo1
        Case (FID_CUSTOMER_PhoneNo2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPhoneNo2
        Case (FID_CUSTOMER_PhoneNo3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mPhoneNo3
        Case (FID_CUSTOMER_LifestyleAnswer1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer1
        Case (FID_CUSTOMER_LifestyleAnswer2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer2
        Case (FID_CUSTOMER_LifestyleAnswer3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer3
        Case (FID_CUSTOMER_LifestyleAnswer4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer4
        Case (FID_CUSTOMER_LifestyleAnswer5):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer5
        Case (FID_CUSTOMER_LifestyleAnswer6):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer6
        Case (FID_CUSTOMER_LifestyleAnswer7):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer7
        Case (FID_CUSTOMER_LifestyleAnswer8):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer8
        Case (FID_CUSTOMER_LifestyleAnswer9):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer9
        Case (FID_CUSTOMER_LifestyleAnswer10):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLifestyleAnswer10
        Case (FID_CUSTOMER_DeliveryInstructions):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mDeliveryInstructions
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    Set IBo_GetSelectAllRow = GetSelectAllRow
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CUSTOMER, FID_CUSTOMER_END_OF_STATIC, m_oSession)

End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cCustomer
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CUSTOMER

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = MODULE_NAME & ": " & mCustomerNo

End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    Call LoadFromRow(oRow)
End Function

Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CUSTOMER_CustomerNo):           mCustomerNo = oField.ValueAsVariant
            Case (FID_CUSTOMER_AlphaKey):             mAlphaKey = oField.ValueAsVariant
            Case (FID_CUSTOMER_Title):                mTitle = oField.ValueAsVariant
            Case (FID_CUSTOMER_Initials):             mInitials = oField.ValueAsVariant
            Case (FID_CUSTOMER_Surname):              mSurname = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine1):         mAddressLine1 = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine2):         mAddressLine2 = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine3):         mAddressLine3 = oField.ValueAsVariant
            Case (FID_CUSTOMER_AddressLine4):         mAddressLine4 = oField.ValueAsVariant
            Case (FID_CUSTOMER_PostCode):             mPostCode = oField.ValueAsVariant
            Case (FID_CUSTOMER_PhoneNo1):             mPhoneNo1 = oField.ValueAsVariant
            Case (FID_CUSTOMER_PhoneNo2):             mPhoneNo2 = oField.ValueAsVariant
            Case (FID_CUSTOMER_PhoneNo3):             mPhoneNo3 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer1):     mLifestyleAnswer1 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer2):     mLifestyleAnswer2 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer3):     mLifestyleAnswer3 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer4):     mLifestyleAnswer4 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer5):     mLifestyleAnswer5 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer6):     mLifestyleAnswer6 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer7):     mLifestyleAnswer7 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer8):     mLifestyleAnswer8 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer9):     mLifestyleAnswer9 = oField.ValueAsVariant
            Case (FID_CUSTOMER_LifestyleAnswer10):    mLifestyleAnswer10 = oField.ValueAsVariant
            Case (FID_CUSTOMER_DeliveryInstructions): mDeliveryInstructions = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CUSTOMER_END_OF_STATIC

End Function


