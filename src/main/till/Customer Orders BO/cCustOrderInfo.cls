VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCustOrderInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : CustOrderInfo
'* Date   : 20/80/07
'* Author : maurice
'*$Archive: /Projects/OasysV2/VB/InventoryBO/HierarchyCategory.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: johng $
'* $Date: 20/05/06 9:24 $
'* $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cCustOrderInfo"

Private mOrderNumber            As String
Private mOrderDate              As Date
Private mDateRequired           As Date
Private mCustomerName           As String
Private mCustomerNumber         As String
Private mDespatchDate           As Date
Private mUserId                 As String
Private mFreeDelID              As String
Private mDeliveryRequestStatus  As Long
Private mSellingStoreId         As Long
Private mSellingStoreOrderId    As Long
Private mCustomerAddress1       As String
Private mCustomerAddress2       As String
Private mCustomerAddress3       As String
Private mCustomerAddress4       As String
Private mCustomerPostCode       As String
Private mOmOrderNumber          As Long
Private mEmail                  As String
Private mWorkTelNumber          As String
Private mRefundStatus           As Long

' Standard IBo private storage
Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'Properties
Public Property Get OrderNumber() As String
    OrderNumber = mOrderNumber
End Property

Public Property Let OrderNumber(ByVal Value As String)
    mOrderNumber = Value
End Property

Public Property Get CustomerNumber() As String
    CustomerNumber = mCustomerNumber
End Property

Public Property Let CustomerNumber(ByVal Value As String)
    mCustomerNumber = Value
End Property

Public Property Get OrderDate() As Date
    OrderDate = mOrderDate
End Property

Public Property Let OrderDate(ByVal Value As Date)
    mOrderDate = Value
End Property

Public Property Get DateRequired() As Date
    DateRequired = mDateRequired
End Property

Public Property Let DateRequired(ByVal Value As Date)
    mDateRequired = Value
End Property

Public Property Get CustomerName() As String
    CustomerName = mCustomerName
End Property

Public Property Let CustomerName(ByVal Value As String)
    mCustomerName = Value
End Property

Public Property Get UserId() As String
    UserId = mUserId
End Property

Public Property Let UserId(ByVal Value As String)
    mUserId = Value
End Property

Public Property Get FreeDelID() As String
    FreeDelID = mFreeDelID
End Property

Public Property Let FreeDelID(ByVal Value As String)
    mFreeDelID = Value
End Property

Public Property Get DespatchDate() As Date
    DespatchDate = mDespatchDate
End Property

Public Property Let DespatchDate(ByVal Value As Date)
    mDespatchDate = Value
End Property

Public Property Get DeliveryRequestStatus() As Long
    DeliveryRequestStatus = mDeliveryRequestStatus
End Property

Public Property Let DeliveryRequestStatus(ByVal Value As Long)
    mDeliveryRequestStatus = Value
End Property

Public Property Get SellingStoreId() As Long
    SellingStoreId = mSellingStoreId
End Property

Public Property Let SellingStoreId(ByVal Value As Long)
    mSellingStoreId = Value
End Property

Public Property Get SellingStoreOrderId() As Long
    SellingStoreOrderId = mSellingStoreOrderId
End Property

Public Property Let SellingStoreOrderId(ByVal Value As Long)
    mSellingStoreOrderId = Value
End Property

Public Property Get CustomerAddress1() As String
    CustomerAddress1 = mCustomerAddress1
End Property

Public Property Let CustomerAddress1(ByVal Value As String)
    mCustomerAddress1 = Value
End Property

Public Property Get CustomerAddress2() As String
    CustomerAddress2 = mCustomerAddress2
End Property

Public Property Let CustomerAddress2(ByVal Value As String)
    mCustomerAddress2 = Value
End Property

Public Property Get CustomerAddress3() As String
    CustomerAddress3 = mCustomerAddress3
End Property

Public Property Let CustomerAddress3(ByVal Value As String)
    mCustomerAddress3 = Value
End Property

Public Property Get CustomerAddress4() As String
    CustomerAddress4 = mCustomerAddress4
End Property

Public Property Let CustomerAddress4(ByVal Value As String)
    mCustomerAddress4 = Value
End Property

Public Property Get CustomerPostCode() As String
    CustomerPostCode = mCustomerPostCode
End Property

Public Property Let CustomerPostCode(ByVal Value As String)
    mCustomerPostCode = Value
End Property

Public Property Get OmOrderNumber() As Long
    OmOrderNumber = mOmOrderNumber
End Property

Public Property Let OmOrderNumber(ByVal Value As Long)
    mOmOrderNumber = Value
End Property

Public Property Get Email() As String
    Email = mEmail
End Property

Public Property Let Email(ByVal Value As String)
    mEmail = Value
End Property

Public Property Get WorkTelNumber() As String
    WorkTelNumber = mWorkTelNumber
End Property

Public Property Let WorkTelNumber(ByVal Value As String)
    mWorkTelNumber = Value
End Property

Public Property Get RefundStatus() As Long
    RefundStatus = mRefundStatus
End Property

Public Property Let RefundStatus(ByVal Value As Long)
    mRefundStatus = Value
End Property

Public Function IBo_Load() As Boolean
    IBo_Load = Load
End Function

Public Function Load() As Boolean
' Load up all properties from the database

Dim oView As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean
    IBo_Save = Save(SaveTypeAllCases)
End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
Dim lngLineNo   As Long
Dim oOrdLine    As cCustOrderLine
Dim oNotes      As cCustOrderText
Dim vntNotes    As Variant

    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean
    IBo_IsValid = IsValid()
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As IBo
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_CUSTORDERINFO * &H10000) + 1 To FID_CUSTORDERINFO_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()
    
    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCustOrderInfo

End Function


Public Function IBo_GetField(nFid As Long) As IField
    Set IBo_GetField = GetField(nFid)
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CUSTORDERINFO_OrderNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrderNumber
        Case (FID_CUSTORDERINFO_OrderDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mOrderDate
        Case (FID_CUSTORDERINFO_DateRequired):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDateRequired
        Case (FID_CUSTORDERINFO_CustomerName):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerName
        Case (FID_CUSTORDERINFO_CustomerNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerNumber
        Case (FID_CUSTORDERINFO_DespatchDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mDespatchDate
        Case (FID_CUSTORDERINFO_UserID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mUserId
        Case (FID_CUSTORDERINFO_FreeDelID):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFreeDelID
        Case (FID_CUSTORDERINFO_DeliveryRequestStatus):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mDeliveryRequestStatus
        Case (FID_CUSTORDERINFO_SellingStoreId):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSellingStoreId
        Case (FID_CUSTORDERINFO_SellingStoreOrderId):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mSellingStoreOrderId
        Case (FID_CUSTORDERINFO_CustomerAddress1):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddress1
        Case (FID_CUSTORDERINFO_CustomerAddress2):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddress2
        Case (FID_CUSTORDERINFO_CustomerAddress3):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddress3
        Case (FID_CUSTORDERINFO_CustomerAddress4):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerAddress4
        Case (FID_CUSTORDERINFO_CustomerPostCode):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mCustomerPostCode
        Case (FID_CUSTORDERINFO_OMOrderNumber):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mOmOrderNumber
        Case (FID_CUSTORDERINFO_Email):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mEmail
        Case (FID_CUSTORDERINFO_WorkTelNumber):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mWorkTelNumber
        Case (FID_CUSTORDERINFO_RefundStatus):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mRefundStatus
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    Set IBo_GetSelectAllRow = GetSelectAllRow
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_CUSTORDERINFO, FID_CUSTORDERINFO_END_OF_STATIC, m_oSession)

End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cCustOrderInfo
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CUSTORDERINFO

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = MODULE_NAME & ": " & mOrderNumber

End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    Call LoadFromRow(oRow)
End Function

Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
            Case (FID_CUSTORDERINFO_OrderNumber):               mOrderNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_OrderDate):                 mOrderDate = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_DateRequired):              mDateRequired = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerName):              mCustomerName = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerNumber):            mCustomerNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_DespatchDate):              mDespatchDate = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_UserID):                    mUserId = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_FreeDelID):                 mFreeDelID = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_DeliveryRequestStatus):     mDeliveryRequestStatus = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_SellingStoreId):            mSellingStoreId = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_SellingStoreOrderId):       mSellingStoreOrderId = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerAddress1):          mCustomerAddress1 = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerAddress2):          mCustomerAddress2 = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerAddress3):          mCustomerAddress3 = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerAddress4):          mCustomerAddress4 = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_CustomerPostCode):          mCustomerPostCode = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_OMOrderNumber):             mOmOrderNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_Email):                     mEmail = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_WorkTelNumber):             mWorkTelNumber = oField.ValueAsVariant
            Case (FID_CUSTORDERINFO_RefundStatus):              mRefundStatus = oField.ValueAsVariant
            Case Else: 'invalid field ID passed in
                LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CUSTORDERINFO_END_OF_STATIC

End Function



