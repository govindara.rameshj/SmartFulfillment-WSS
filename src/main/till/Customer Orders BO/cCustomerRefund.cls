VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cCustomerRefund"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : cCustomerRefund
'* Date   : 04/03/10
'* Author : MikeO
'*$Archive: /Projects/Wix/VB/cCustomerOrdersBO/cCustOrderText.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: MikeO $
'* $Date: 04/03/10 9:24 $
'* $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Base 0
Option Explicit

Implements IBo
Implements ISysBo

Const MODULE_NAME As String = "cCustomerRefund"

' Private Storage for Properties
Private mOrderNumber            As String
Private mLineNo                 As String
Private mPartCode               As String
Private mRefundedStoreId        As String
Private mRefundDate             As Date
Private mRefundTill             As String
Private mRefundTran             As String
Private mQtyReturned            As Integer
Private mQtyCancelled           As Integer
Private mRefundStatus           As Integer
Private mSellingStoreIBTOut     As String
Private mFulfillingStoreIbtIn   As String

' Standard IBo private storage
Private moRowSel As IRowSelector

Private moLoadRow As IRowSelector

Private m_oSession As Session

'Properties
Public Property Get OrderNumber() As String
    OrderNumber = mOrderNumber
End Property

Public Property Let OrderNumber(ByVal Value As String)
    mOrderNumber = Value
End Property

Public Property Get LineNo() As String
    LineNo = mLineNo
End Property

Public Property Let LineNo(ByVal Value As String)
    mLineNo = Value
End Property

Public Property Get PartCode() As String
    PartCode = mPartCode
End Property

Public Property Let PartCode(ByVal Value As String)
    mPartCode = Value
End Property

Public Property Get RefundedStoreId() As String
    RefundedStoreId = mRefundedStoreId
End Property

Public Property Let RefundedStoreId(ByVal Value As String)
    mRefundedStoreId = Value
End Property

Public Property Get RefundDate() As Date
    RefundDate = mRefundDate
End Property

Public Property Let RefundDate(ByVal Value As Date)
    mRefundDate = Value
End Property

Public Property Get RefundTill() As String
    RefundTill = mRefundTill
End Property

Public Property Let RefundTill(ByVal Value As String)
    mRefundTill = Value
End Property

Public Property Get RefundTran() As String
    RefundTran = mRefundTran
End Property

Public Property Let RefundTran(ByVal Value As String)
    mRefundTran = Value
End Property

Public Property Get QtyReturned() As Integer
    QtyReturned = mQtyReturned
End Property

Public Property Let QtyReturned(ByVal Value As Integer)
    mQtyReturned = Value
End Property

Public Property Get QtyCancelled() As Integer
    QtyCancelled = mQtyCancelled
End Property

Public Property Let QtyCancelled(ByVal Value As Integer)
    mQtyCancelled = Value
End Property

Public Property Get RefundStatus() As Integer
    RefundStatus = mRefundStatus
End Property

Public Property Let RefundStatus(ByVal Value As Integer)
    mRefundStatus = Value
End Property

Public Property Get SellingStoreIBTOut() As String
    SellingStoreIBTOut = mSellingStoreIBTOut
End Property

Public Property Let SellingStoreIBTOut(ByVal Value As String)
    mSellingStoreIBTOut = Value
End Property

Public Property Get FulfillingStoreIbtIn() As String
    FulfillingStoreIbtIn = mFulfillingStoreIbtIn
End Property

Public Property Let FulfillingStoreIbtIn(ByVal Value As String)
    mFulfillingStoreIbtIn = Value
End Property


Public Function IBo_Load() As Boolean
    IBo_Load = Load
End Function

Public Function Load() As Boolean
' Load up all properties from the database

Dim oView As IView
    
    Load = False
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.Merge(GetSelectAllRow)
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(moRowSel)
    If Not (oView Is Nothing) Then
        If oView.Count > 0 Then Load = LoadFromRow(oView.Row(1))
    End If

End Function

Public Function IBo_Save() As Boolean
    IBo_Save = Save(SaveTypeAllCases)
End Function

Private Function Save(eSave As enSaveType) As Boolean
' Save all properties to the database
    
    Save = False
    
    If IsValid() Then
        ' Get a row and pass it to the database to insert/update/delete
        Save = m_oSession.Database.SaveBo(eSave, Me)
    End If

End Function

Public Function IBo_SaveIfNew() As Boolean
    IBo_SaveIfNew = SaveIfNew
End Function

Public Function SaveIfNew() As Boolean
    
    SaveIfNew = Save(SaveTypeIfNew)

End Function

Public Function IBo_SaveIfExists() As Boolean
    IBo_SaveIfExists = SaveIfExists
End Function

Public Function SaveIfExists() As Boolean

    SaveIfExists = Save(SaveTypeIfExists)

End Function

Public Function IBo_Delete() As Boolean
    IBo_Delete = Delete
End Function

Public Function Delete() As Boolean

    Delete = Save(SaveTypeDelete)

End Function

Public Function IBo_IsValid() As Boolean
    IBo_IsValid = IsValid()
End Function

Private Function IsValid() As Boolean
'    Debug.Assert False
    IsValid = True
End Function

Public Function IBo_Interface(Optional eInterfaceType As Long) As IBo
    Set IBo_Interface = Interface(eInterfaceType)
End Function

Public Function Interface(Optional eInterfaceType As Long) As IBo
    Select Case (eInterfaceType)
    Case BO_INTERFACE_IBO:
        Dim oBO As IBo
        Set oBO = Me
        Set Interface = oBO
    Case Else:
        Set Interface = Me
    End Select
End Function

Public Function IBo_GetAllFieldsAsRow() As IRow
    Set IBo_GetAllFieldsAsRow = GetAllFieldsAsRow()
End Function

Private Function GetAllFieldsAsRow() As IRow
' Returns a CRow object containing one field object for each property.
    
Dim oRow    As IRow
Dim nFid    As Long
    
    Set oRow = New CRow
    For nFid = (CLASSID_CUST_REFUND * &H10000) + 1 To FID_CUST_REFUND_END_OF_STATIC Step 1
        oRow.Add GetField(nFid)
    Next nFid
    Set GetAllFieldsAsRow = oRow

End Function

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadFilter()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up any selection criteria before
'*              calling the IBo_Load method.  This appends the selection criteria to the
'*              oRowSel object which is used by the IBo_Load method.
'**********************************************************************************************
'* Parameters:
'*In/Out:Comparator Long. - Comparison Type - see modBO.bas
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'*In/Out:Value  Variant. - Value that Field will be compared to
'**********************************************************************************************
'* Returns:  Boolean - True if Valid FieldID and successful
'**********************************************************************************************
'* History:
'* 08/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Function IBo_AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean
    IBo_AddLoadFilter = AddLoadFilter(Comparator, FieldID, Value)
End Function
Public Function AddLoadFilter(Comparator As Long, FieldID As Long, Value As Variant) As Boolean

Dim oField As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadFilter"

    'Create field for storing Key into
    Set oField = GetField(FieldID)
    If oField Is Nothing Then Exit Function
    oField.ValueAsVariant = Value
    
    'Add field to Row selector
    If moRowSel Is Nothing Then Set moRowSel = New CRowSelector
    Call moRowSel.AddSelection(Comparator, oField)
    AddLoadFilter = True

End Function 'AddLoadFilter

'<CACH>****************************************************************************************
'* Function:  Boolean IBo_AddLoadField()
'**********************************************************************************************
'* Description: Part of the IBo Interface.  Used to set-up all fields that must be returned
'*              when using the LoadMatches method.  This appends the field to the oFieldRow
'*              object which is used by the IBo_LoadMatches method, if it is populated.
'**********************************************************************************************
'* Parameters:
'*In/Out:FieldID    Long. - Unique Field ID against which field to compare against
'**********************************************************************************************
'* History:
'* 09/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub IBo_AddLoadField(FieldID As Long)
    AddLoadField FieldID
End Sub

Public Sub AddLoadField(FieldID As Long)

Const PROCEDURE_NAME As String = MODULE_NAME & ".AddLoadField"
    
    'Add field to Row selector
    If moLoadRow Is Nothing Then Set moLoadRow = m_oSession.Root.CreateUtilityObject("CRowSelector")
    Call moLoadRow.AddSelectAll(FieldID)

End Sub      'AddLoadField

Public Sub IBo_ClearLoadFilter()
    ClearLoadFilter
End Sub

Public Sub ClearLoadFilter()
    
    Set moRowSel = Nothing

End Sub

Public Sub IBo_ClearLoadField()
    ClearLoadField
End Sub

Public Sub ClearLoadField()

    Set moLoadRow = Nothing

End Sub

Public Function IBo_LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection
    Set IBo_LoadMatches = LoadMatches(oReturnRow)
End Function

Public Function LoadMatches(Optional oReturnRow As IRow = Nothing) As Collection

' Load up all properties from the database
Dim oView       As IView
Dim colBO       As Collection
Dim oBO         As ISysBo
Dim lBONo       As Long
Dim oLoadFields As IRowSelector
    
    Set colBO = New Collection
    
    Call CheckIsUnInitialised(m_oSession, IBo_ClassName)
    
    Set oLoadFields = moRowSel
    If oLoadFields Is Nothing Then Set oLoadFields = New CRowSelector
    
    If oReturnRow Is Nothing Then
        If moLoadRow Is Nothing Then
            Call oLoadFields.Merge(GetSelectAllRow)
        Else
            Call oLoadFields.Merge(moLoadRow) ' add in the specified field
        End If
    Else
        Call oLoadFields.Merge(oReturnRow) ' add in the specified field
    End If
    
    ' Pass the selector to the database to get the data view
    Set oView = m_oSession.Database.GetView(oLoadFields)
    If Not (oView Is Nothing) Then
        For lBONo = 1 To oView.Count Step 1
            Set oBO = m_oSession.Database.CreateBusinessObject(IBo_ClassId)
            Call oBO.LoadFromRow(oView.Row(CLng(lBONo)))
            If lBONo = 1 Then Call LoadFromRow(oView.Row(CLng(1)))
            Call colBO.Add(oBO)
        Next lBONo
    End If
    
    Set LoadMatches = colBO

End Function


Private Function IBo_CreateNew() As IBo
    
    Set IBo_CreateNew = New cCustomerRefund
End Function


Public Function IBo_GetField(nFid As Long) As IField
    Set IBo_GetField = GetField(nFid)
End Function

Public Function GetField(lFieldID As Long) As IField

    If CheckIsUnInitialised(m_oSession, IBo_ClassName) Then
        lFieldID = 0
        Exit Function
    End If
    On Error GoTo Error_Handler
    
    Select Case (lFieldID)
        Case (FID_CUST_REFUND_Number):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mOrderNumber
        Case (FID_CUST_REFUND_Line):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mLineNo
        Case (FID_CUST_REFUND_RefundStoreId):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRefundedStoreId
        Case (FID_CUST_REFUND_RefundDate):
                Set GetField = New CFieldDate
                GetField.ValueAsVariant = mRefundDate
        Case (FID_CUST_REFUND_RefundTill):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRefundTill
        Case (FID_CUST_REFUND_RefundTran):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mRefundTran
        Case (FID_CUST_REFUND_QtyReturned):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQtyReturned
        Case (FID_CUST_REFUND_QtyCancelled):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mQtyCancelled
        Case (FID_CUST_REFUND_RefundStatus):
                Set GetField = New CFieldLong
                GetField.ValueAsVariant = mRefundStatus
        Case (FID_CUST_REFUND_FulfillingStoreIbtIn):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mFulfillingStoreIbtIn
        Case (FID_CUST_REFUND_SellingStoreIBTOut):
                Set GetField = New CFieldString
                GetField.ValueAsVariant = mSellingStoreIBTOut
        Case Else
                Call Err.Raise(OASYS_ERR_GETFIELD_INVALID_FID, MODULE_NAME, OASYS_ERR_GETFIELD_INVALID_FID_MSG & lFieldID)
    End Select
    
    If Not GetField Is Nothing Then GetField.Id = lFieldID
    
    Exit Function
    
Error_Handler:

    Call MsgBox(MODULE_NAME & " " & "GetField-" & lFieldID & vbCrLf & "Err:" & Err.Number & "-" & Err.Description)
    Resume Next

End Function

Private Function IBo_GetSelectAllRow() As IRowSelector
    Set IBo_GetSelectAllRow = GetSelectAllRow
End Function

Private Function GetSelectAllRow() As IRowSelector
' Returns a CRow object containing one SelectAll field selector for each property.
    
    Set GetSelectAllRow = GetSelectAllRows(CLASSID_ORDERTEXT, FID_ORDERTEXT_END_OF_STATIC, m_oSession)

End Function

Private Function IBo_Initialise(oSession As ISession) As IBo
    Set IBo_Initialise = Initialise(oSession)
End Function

Private Function Initialise(oSession As ISession) As cCustomerRefund
    Set m_oSession = oSession
    Set Initialise = Me
End Function

Public Property Get IBo_ClassId() As Integer

    IBo_ClassId = CLASSID_CUST_REFUND

End Property

Public Property Get IBo_ClassName() As String

    IBo_ClassName = MODULE_NAME

End Property

Private Property Get IBo_DebugString() As String
    
    IBo_DebugString = MODULE_NAME & ": " & mOrderNumber & " " & mLineNo

End Property

'<CACH>****************************************************************************************
'* Function:  Boolean LoadFromRow()
'**********************************************************************************************
'* Description: Part of the IBo interface called by the DB object to pass in a row of field and
'*              for this object to extract the data and populate the properties of the object.
'**********************************************************************************************
'* Parameters:
'*In/Out:oRow   Object. - collection of fields
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 13/08/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function ISysBo_LoadFromRow(oRow As IRow) As Boolean
    Call LoadFromRow(oRow)
End Function

Public Function LoadFromRow(oRow As IRow) As Boolean

Dim lFieldNo As Integer
Dim oField   As IField

Const PROCEDURE_NAME As String = MODULE_NAME & ".LoadFromRow"

    LoadFromRow = True
    For lFieldNo = 1 To oRow.Count Step 1
        Set oField = oRow.Field(lFieldNo)
        Select Case (oField.Id)
        Case (FID_CUST_REFUND_Number):                  mOrderNumber = oField.ValueAsVariant
        Case (FID_CUST_REFUND_Line):                    mLineNo = oField.ValueAsVariant
        Case (FID_CUST_REFUND_RefundStoreId):           mRefundedStoreId = oField.ValueAsVariant
        Case (FID_CUST_REFUND_RefundDate):              mRefundDate = oField.ValueAsVariant
        Case (FID_CUST_REFUND_RefundTill):              mRefundTill = oField.ValueAsVariant
        Case (FID_CUST_REFUND_RefundTran):              mRefundTran = oField.ValueAsVariant
        Case (FID_CUST_REFUND_QtyReturned):             mQtyReturned = oField.ValueAsVariant
        Case (FID_CUST_REFUND_QtyCancelled):            mQtyCancelled = oField.ValueAsVariant
        Case (FID_CUST_REFUND_RefundStatus):            mRefundStatus = oField.ValueAsVariant
        Case (FID_CUST_REFUND_FulfillingStoreIbtIn):    mFulfillingStoreIbtIn = oField.ValueAsVariant
        Case (FID_CUST_REFUND_SellingStoreIBTOut):      mSellingStoreIBTOut = oField.ValueAsVariant
        Case Else: 'invalid field ID passed in
            LoadFromRow = False
        End Select
    Next lFieldNo

End Function 'LoadFromRow

Private Property Get IBo_Version() As String

    IBo_Version = App.Major & "." & App.Minor & "." & App.Revision

End Property

Public Function IBo_EndOfStaticFieldId() As Long

    IBo_EndOfStaticFieldId = FID_CUST_REFUND_END_OF_STATIC

End Function

