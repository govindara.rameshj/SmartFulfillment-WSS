VERSION 5.00
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Begin VB.UserControl ucPCEntry 
   ClientHeight    =   1710
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2940
   DefaultCancel   =   -1  'True
   KeyPreview      =   -1  'True
   ScaleHeight     =   1710
   ScaleWidth      =   2940
   Begin VB.TextBox txtPostCode 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   240
      MaxLength       =   8
      TabIndex        =   0
      Top             =   540
      Width           =   2415
   End
   Begin VB.PictureBox fraControl 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   0
      ScaleHeight     =   1665
      ScaleWidth      =   2895
      TabIndex        =   4
      Top             =   0
      Width           =   2925
      Begin LpLib.fpCombo cmbRegion 
         Height          =   480
         Left            =   60
         TabIndex        =   1
         Top             =   540
         Visible         =   0   'False
         Width           =   2715
         _Version        =   196608
         _ExtentX        =   4789
         _ExtentY        =   847
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   0
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   0
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "ucPostCodeEntry.ctx":0000
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "ENTER"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   660
         TabIndex        =   2
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label lblMessage 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Enter Postcode"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   180
         TabIndex        =   3
         Top             =   60
         Width           =   2475
      End
   End
End
Attribute VB_Name = "ucPCEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Event OK(strPostCode As String)
Public Event Cancel()
Public RGBEdit_Colour As Long
Public PartialOK As Boolean

Private mCountryCode As String
Private mRegionPath  As String

Private Sub cmbRegion_KeyPress(KeyAscii As Integer)
    
    cmbRegion.ListDown = True

End Sub

Private Sub cmdOK_Click()
    
    cmdOK.SetFocus
    DoEvents
    If (txtPostCode.Visible = True) Then
        If (txtPostCode.Text = "") Then
            Call MsgBoxEx("No value has been entered", vbCritical + vbOKOnly, "Invalid entry detected", , , , , vbRed)
            txtPostCode.SetFocus
            Exit Sub
        End If
        If PostCodeIsOK(txtPostCode.Text) Then
            RaiseEvent OK(txtPostCode.Text)
        Else
            MsgBoxEx "Invalid Postcode Format"
            txtPostCode.SetFocus
        End If
    Else
        If (cmbRegion.Text = "") Then
            Call MsgBoxEx("No value has been entered", vbCritical + vbOKOnly, "Invalid entry detected", , , , , vbRed)
            cmbRegion.SetFocus
            Exit Sub
        End If
        RaiseEvent OK(cmbRegion.Text)
    End If

End Sub

Private Sub txtPostCode_GotFocus()
    
    txtPostCode.SelStart = 0
    txtPostCode.SelLength = Len(txtPostCode.Text)
    txtPostCode.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPostCode_LostFocus()
    txtPostCode.BackColor = vbWindowBackground
End Sub

Private Sub cmbRegion_GotFocus()
    
Const KEY_DROPDOWN As String = "%{Down}"
    
    cmbRegion.SelStart = 0
    cmbRegion.SelLength = Len(cmbRegion.Text)
    cmbRegion.BackColor = RGBEdit_Colour
    cmbRegion.ListDown = True

End Sub

Private Sub cmbRegion_LostFocus()
    cmbRegion.BackColor = vbWindowBackground
End Sub

Private Sub UserControl_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyEscape Then
        RaiseEvent Cancel
    End If

End Sub

Public Property Let BackColor(ByVal Value As Long)
    fraControl.BackColor = Value
    cmdOK.BackColor = Value
End Property

Public Property Get BackColor() As Long
    BackColor = fraControl.BackColor
End Property

Public Property Let InputColor(ByVal Value As Long)
    txtPostCode.BackColor = Value
End Property

Public Property Get InputColor() As Long
    InputColor = txtPostCode.BackColor
End Property

Public Property Let RegionPath(ByVal Value As String)
    mRegionPath = Value
End Property

Public Property Let CountryCode(Value As String)

Dim oFSO As New FileSystemObject
Dim tsRegion As TextStream
Dim strRegions As String
Dim arrRegion() As String
Dim lngRegion As Integer

    mCountryCode = Value
    If (Value = "IE") Then
        lblMessage.Caption = "Enter Region"
        If (oFSO.FileExists(mRegionPath & "Region.txt") = True) Then
            Set tsRegion = oFSO.OpenTextFile(mRegionPath & "Region.txt", ForReading, False)
            strRegions = tsRegion.ReadAll
            arrRegion = Split(strRegions, vbCrLf)
            For lngRegion = LBound(arrRegion) To UBound(arrRegion) Step 1
                cmbRegion.AddItem (arrRegion(lngRegion))
            Next lngRegion
            tsRegion.Close
            cmbRegion.Visible = True
            txtPostCode.Visible = False
        End If
    End If

End Property

Private Sub txtPostCode_KeyPress(KeyAscii As Integer)

    If (mCountryCode <> "IE") Then
        If Chr$(KeyAscii) Like "[a-z]" Then KeyAscii = Asc(UCase$(Chr$(KeyAscii)))
    
        If (Not Chr$(KeyAscii) Like "[0-9A-Z ]") And (KeyAscii >= 32) Then KeyAscii = 0
    End If
    
End Sub

Public Function PostCodeIsOK(ByVal strCode As String, Optional ByVal blnAllowPartial As Boolean = False) As Boolean

    PostCodeIsOK = False
    
    If (mCountryCode <> "IE") Then
        strCode = Trim$(strCode)
        
        If strCode Like "[A-Z][1-9A-Z]*[0-9 ][0-9][A-Z][A-Z]" Then
            PostCodeIsOK = True
            Exit Function
        End If
        
        ' Check for valid Partial Postcode
        If Not blnAllowPartial Then Exit Function
    Else
        PostCodeIsOK = True
        Exit Function
    End If
    
End Function

