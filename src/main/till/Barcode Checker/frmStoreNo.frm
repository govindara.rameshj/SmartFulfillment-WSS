VERSION 5.00
Object = "{9A526B11-3010-11D5-99E4-000102897E9C}#2.2#0"; "NumTextControl.ocx"
Begin VB.Form frmStoreNo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Store \ Warehouse Number Entry"
   ClientHeight    =   2760
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   Icon            =   "frmStoreNo.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2760
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Caption         =   "F5 - OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   656
      TabIndex        =   1
      Top             =   1800
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "F10 - Exit"
      Height          =   375
      Left            =   2809
      TabIndex        =   2
      Top             =   1800
      Width           =   1215
   End
   Begin NumTextControl.NumText ntxtStoreNo 
      Height          =   375
      Left            =   2033
      TabIndex        =   0
      Top             =   1200
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   661
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxLength       =   2
      SelStart        =   1
      Text            =   "0"
      DecimalPlaces   =   0
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   6  'Inside Solid
      BorderWidth     =   2
      DrawMode        =   1  'Blackness
      Height          =   2415
      Left            =   240
      Top             =   173
      Width           =   4215
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      Caption         =   "Please enter the Store or Warehouse number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   533
      TabIndex        =   3
      Top             =   420
      Width           =   3615
   End
End
Attribute VB_Name = "frmStoreNo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MODULE_NAME  As String = "frmStoreNo"

Private mstrStoreNo As String

Private Sub cmdExit_Click()

    ' Warn the user that the system will exit if they don't enter the number.
    If (MsgBox("You did not enter a Store \ Warehouse Number." & vbCrLf & vbCrLf & _
                    " You are about to exit the system, do you wish to continue.", vbExclamation + vbYesNo, _
                    "Number Not Entered") = vbNo) Then
        Exit Sub
    End If
    
    mstrStoreNo = ""
    
    ' Exit without saving.
    Me.Visible = False
    
End Sub

Private Sub cmdOK_Click()

Const PROCEDURE_NAME As String = "cmdOK_Click"

    On Error GoTo cmdSort_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdOK Click")

    ' Test if the user entered anything.
    If Not (Me.ntxtStoreNo.Value > 0) Then
        Call MsgBox("You did not enter a Store \ Warehouse Number." & vbCrLf & vbCrLf & _
                        " Please enter a number to continue.", vbExclamation + vbOKOnly, _
                        "Number Not Entered")
        Exit Sub
    End If
    
    mstrStoreNo = Format(Me.ntxtStoreNo.Text, "000")
    
    ' Exit.
    Me.Visible = False
    
    Exit Sub
    
cmdSort_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
'   Setup the form.
    Me.BackColor = frmBarcodeChecker.BackColor
    Me.lblMessage.BackColor = Me.BackColor
    
'   Disable the forms close button and system menu option.
    Call DisableCloseButton(Me)

    Exit Sub
    
FormLoad_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyF5
                KeyCode = 0
                cmdOK_Click     ' Exit form with save of store number.
                
            Case vbKeyF10
                KeyCode = 0
                cmdExit_Click   ' Exit form without saving.
        End Select
    End If

End Sub

' Allow retrieval of the entered Store or Warehouse number.
Public Property Get StoreNo() As String

    StoreNo = mstrStoreNo
    
End Property

