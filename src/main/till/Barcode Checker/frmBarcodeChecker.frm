VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{027D61A0-4251-11D0-B7A4-80BBFFC00000}#1.50#0"; "scanner.ocx"
Begin VB.Form frmBarcodeChecker 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Barcode Checker"
   ClientHeight    =   7335
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11415
   Icon            =   "frmBarcodeChecker.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7335
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin SCANNERLib.Scanner comUSBScanner 
      Left            =   960
      Top             =   240
      _Version        =   65541
      _ExtentX        =   1005
      _ExtentY        =   1005
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdViewReport 
      Caption         =   "F7 - View Report"
      Height          =   495
      Left            =   6510
      TabIndex        =   11
      Top             =   6372
      Width           =   1455
   End
   Begin VB.CommandButton cmdAddLine 
      Caption         =   "F3 - Add Line"
      Height          =   495
      Left            =   3450
      TabIndex        =   2
      Top             =   6372
      Width           =   1455
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   495
      Left            =   9840
      TabIndex        =   0
      Top             =   6372
      Width           =   1455
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5 - Save"
      Height          =   495
      Left            =   120
      TabIndex        =   5
      Top             =   6372
      Width           =   1455
   End
   Begin VB.CommandButton cmdClearList 
      Caption         =   "F4 - Clear List"
      Height          =   495
      Left            =   4980
      TabIndex        =   4
      Top             =   6372
      Width           =   1455
   End
   Begin VB.CommandButton cmdDeleteLine 
      Caption         =   "F2 - Delete Line"
      Height          =   495
      Left            =   1920
      TabIndex        =   3
      Top             =   6372
      Width           =   1455
   End
   Begin VB.CommandButton cmdSkuEnquiry 
      Caption         =   "F8 - Sku Enquiry"
      Height          =   495
      Left            =   8040
      TabIndex        =   1
      Top             =   6372
      Width           =   1455
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   6960
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmBarcodeChecker.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12779
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:56"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSCommLib.MSComm comBarScanner 
      Left            =   360
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      CommPort        =   4
      DTREnable       =   -1  'True
   End
   Begin FPSpreadADO.fpSpread fpsprdData 
      Height          =   5385
      Left            =   120
      TabIndex        =   12
      Top             =   900
      Width           =   11175
      _Version        =   458752
      _ExtentX        =   19711
      _ExtentY        =   9499
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   6
      SpreadDesigner  =   "frmBarcodeChecker.frx":2756
   End
   Begin VB.Label lblStockUpdateDate 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   8550
      TabIndex        =   10
      Top             =   360
      Width           =   1095
   End
   Begin VB.Label lblDate 
      Alignment       =   1  'Right Justify
      Caption         =   "Last Stock File Update"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   6270
      TabIndex        =   9
      Top             =   360
      Width           =   2175
   End
   Begin VB.Label lblStoreNo 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   4110
      TabIndex        =   8
      Top             =   360
      Width           =   1095
   End
   Begin VB.Label lblNumber 
      Alignment       =   1  'Right Justify
      Caption         =   "Store / Warehouse Number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   1470
      TabIndex        =   7
      Top             =   360
      Width           =   2535
   End
End
Attribute VB_Name = "frmBarcodeChecker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Barcode Checker/frmBarcodeChecker.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application that is designed to check the Barcodes in store or warehouse against
'*                      stock files and if required save the scan data to table EANAUDIT.
'*
'**********************************************************************************************
'* Versions:
'*
'* 03/11/05 DaveF   v1.0.0  Initial build. WIX1170.
'*
'* 29/11/05 DaveF   v1.0.3  WIX1170 Updated to enable the use of a USB scanner.
'*
'* 30/11/05 DaveF   v1.0.4  WIX1170 Updated form frmAuditReport to use LEFT joins in gathering
'*                              the data for the report.
'*
'* 31/01/06 DaveF   v1.0.5  WIX1170 Changed method InitialiseBarCodeScanner() to check if the
'*                              Comm port is already open before trying to open it.
'*
'* 01/02/06 DaveF   v1.0.7  WIX1170 Changed the frmAuditReport to include totals for date, store
'*                              and overall.
'*
'* 28/04/06 DaveF   v1.0.8  Changed to remove the scanning side of the application by not
'*                              setting up the scanner.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'*  (/P='X' /u='001' -c='ffff')
'*
'*  P = L or S. L = Laptop or standalone processing. S = Store.
'*
'*  u = User ID
'*
'*  c = Checksum (Not used at present)
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmBarcodeChecker"

Private Const PRM_WIXCOMMSPATH As Integer = 914
Private Const PRM_WIXDATAPATH As Integer = 910
Private Const PRM_WIXCOMPORT As Integer = 952

Private Const SUCCESS_FILE As String = "StockFileUpdate.txt"

Private mcnnDBConnection    As ADODB.Connection
Private mstrStoreNo         As String
Private mstrUserID          As String
Private mstrWSID            As String
Private mblnStandAlone      As Boolean
Private mstrWixData         As String
Private mblnProcBarcode     As Boolean
Private mstrComPortSettings As String

' Form load event.
Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

Dim strErrSource  As String

'    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
'   Get the system setup.
    Call GetRoot
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Me.lblNumber.BackColor = Me.BackColor
    Me.lblDate.BackColor = Me.BackColor
    Me.fpsprdData.MaxRows = 0
    
    ' Get the User ID, Workstation ID and if running as the StandAlone version or not.
    mstrUserID = goSession.UserID
    mstrWSID = goSession.CurrentEnterprise.IEnterprise_WorkstationID
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "ProgramParams - " & goSession.ProgramParams)
    If (goSession.ProgramParams = "L") Then
        mblnStandAlone = True
        Me.cmdSkuEnquiry.Visible = False
        Me.Caption = Me.Caption & " - Standalone"
    Else
        mblnStandAlone = False
        Me.Caption = Me.Caption & " - Store"
    End If
    
    ' Get the paths to use.
    mstrWixData = goSession.GetParameter(PRM_WIXDATAPATH)
    Call TestPathExists(mstrWixData)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Wix Data Path - " & mstrWixData)

'   Set up the database connection.
    Set mcnnDBConnection = goSession.Database.Connection
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    If (mcnnDBConnection Is Nothing) Then
        Call MsgBox("The Database connection to DSN - " & goSession.Database.Connection.DefaultDatabase & " - Failed")
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    End If
    
    ' Get the Store / Warehouse number from the user.
    Call GetStoreNumber
    
    ' Get the date of the last stock file update.
    Call GetLastStockFileUpdateDate
    
    ' Get the Com Port settings.
    mstrComPortSettings = goSession.GetParameter(PRM_WIXCOMPORT)
    
    ' Initialise the Barcode scanner.
'    Call InitialiseBarCodeScanner
    
    ' Initialise the USB Barcode scanner.
 '   Call InitialiseUSBBarCodeScanner

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load Successful")
    
    Exit Sub
    
FormLoad_Error:
   
    If (Err.Source <> "") Then
        strErrSource = PROCEDURE_NAME & " - " & Err.Source
    Else
        strErrSource = PROCEDURE_NAME
    End If
    Call MsgBox("Module: " & strErrSource & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, App.EXEName)
    Call DebugMsg(MODULE_NAME, strErrSource, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
    Call Err.Report(MODULE_NAME, strErrSource, 0, False)
    
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Unload Me
    
End Sub

' Unload the form and empty all the used objects.
Private Sub Form_Unload(Cancel As Integer)

Const PROCEDURE_NAME As String = "Form_Unload"
    
    On Error GoTo Form_Unload_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Unload")
    
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Set mcnnDBConnection = Nothing
    
    If (Me.comBarScanner.PortOpen = True) Then
        Me.comBarScanner.PortOpen = False
    End If
    
    End

    Exit Sub
    
Form_Unload_Error:

    End
    
End Sub

' Form Key Down event.
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyF2
                KeyCode = 0
                cmdDeleteLine_Click ' Delete line.
            
            Case vbKeyF3
                KeyCode = 0
                cmdAddLine_Click    ' Add line.
            
            Case vbKeyF4
                KeyCode = 0
                cmdClearList_Click  ' Clear the list.
                
            Case vbKeyF5
                KeyCode = 0
                cmdSave_Click       ' Save the list data.
                
            Case vbKeyF7
                KeyCode = 0
                cmdViewReport_Click ' View the Audit report.
            
            Case vbKeyF8
                KeyCode = 0
                cmdSkuEnquiry_Click ' SKU enquiry.
            
            Case vbKeyF10
                KeyCode = 0
                cmdExit_Click       ' Exit system.
                
        End Select
    End If
    
End Sub

' Manually add a new product to the list by the user entering the Barcode.
Private Sub cmdAddLine_Click()

Const PROCEDURE_NAME As String = "cmdAddLine_Click"

Dim strBarcode As String

    On Error GoTo cmdAddLine_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdAddLine Click")

    ' Load Form frmUserEntry to allow user entry of the Barcode.
    Me.Visible = False
    Load frmUserEntry
    frmUserEntry.BarcodeEntry = True
    frmUserEntry.Show vbModal
    Me.Visible = True
    strBarcode = frmUserEntry.EnteredNumber
    Unload frmUserEntry
    
    ' If the user entered a Barcode then add a line to list.
    If (strBarcode <> "") Then
        ' Process the Barcode just entered.
        Call ProcessBarcode(strBarcode, True)
    End If
    
    Exit Sub
    
cmdAddLine_Click_Error:
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Clear the List.
Private Sub cmdClearList_Click()

Const PROCEDURE_NAME As String = "cmdClearList_Click"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdClearList Click")
    
    Me.fpsprdData.MaxRows = 0
    
End Sub

' Delete the current line.
Private Sub cmdDeleteLine_Click()

Const PROCEDURE_NAME As String = "cmdDeleteLine_Click"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdDeleteLine Click")
    
    ' Delete the current line if required.
    If (Me.fpsprdData.MaxRows > 0) Then
        If (MsgBox("Do you wish to delete this line?", vbQuestion + vbYesNo, "Delete Line?") = vbYes) Then
            Call fpsprdData.DeleteRows(fpsprdData.ActiveRow, 1)
            Me.fpsprdData.MaxRows = Me.fpsprdData.MaxRows - 1
        End If
    Else
        Call MsgBox("There are no products in the" & vbCrLf & _
                        " list to delete.", vbInformation + vbOKOnly, "No Products In The List")
    End If

End Sub

'   Exit the system.
Private Sub cmdExit_Click()

Const PROCEDURE_NAME As String = "cmdExit_Click"
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdExit Click")
    
    Unload Me
    
End Sub

    ' Save the list data.
Private Sub cmdSave_Click()

Const PROCEDURE_NAME As String = "cmdSave_Click"

Dim recEANAUDIT As ADODB.Recordset
Dim intCounter  As Integer

    On Error GoTo cmdSave_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdSave Click")
    
    If (Me.fpsprdData.MaxRows = 0) Then
        Call MsgBox("There are no products in the" & vbCrLf & _
                        " list to save.", vbInformation + vbOKOnly, "No Products In The List")
        Exit Sub
    End If
    
    ' Open the recordset for adding the list data to table EANAUDIT.
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    Set recEANAUDIT = New ADODB.Recordset
    Call recEANAUDIT.Open("EANAUDIT", mcnnDBConnection, adOpenKeyset, adLockBatchOptimistic, adCmdTable)
    
    ' Loop over the list adding the details to the recordset.
    For intCounter = 1 To Me.fpsprdData.MaxRows
        Me.fpsprdData.Row = intCounter
        With recEANAUDIT
            .AddNew
            .Fields("DATE1").Value = Now()
            .Fields("STOR").Value = Me.lblStoreNo
            .Fields("EEID").Value = mstrUserID
            .Fields("WSID").Value = mstrWSID
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("EAN")
            .Fields("EANN").Value = Me.fpsprdData.Text
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("Keyed")
            If (Me.fpsprdData.Text = "Y") Then
                .Fields("KEYD").Value = 1
            Else
                .Fields("KEYD").Value = 0
            End If
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("Exists")
            If (Me.fpsprdData.Text = "Y") Then
                .Fields("EXIST").Value = 1
            Else
                .Fields("EXIST").Value = 0
            End If
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("SKU")
            .Fields("SKUN").Value = Me.fpsprdData.Text
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("AutoMatch")
            If (Me.fpsprdData.Text = "Y") Then
                .Fields("AUTO").Value = 1
            Else
                .Fields("AUTO").Value = 0
            End If
        End With
    Next intCounter
    
    ' Add the recordset data to the database table.
    recEANAUDIT.UpdateBatch
    
    Set recEANAUDIT = Nothing
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    
    ' Let the user know that the data has been saved successfully.
    Call MsgBox("The list data has been saved.", vbInformation + vbOKOnly, "List Data Saved")
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "List Data Saved")
    
    Exit Sub
    
cmdSave_Click_Error:
    
    Set recEANAUDIT = Nothing
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Sub

' Perform a SKU enquiry.
Private Sub cmdSkuEnquiry_Click()

Const PROCEDURE_NAME As String = "cmdSkuEnquiry_Click"

Dim strSKUN   As String
Dim strEnquiryEXE As String

    On Error GoTo cmdSkuEnquiry_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdSkuEnquiry Click")
    
    If (Me.fpsprdData.MaxRows = 0) Then
        Call MsgBox("There are no products in the list to" & vbCrLf & _
                        " enquire about.", vbInformation + vbOKOnly, "No Products In The List")
        Exit Sub
    End If
    
    ' Get the SKUN from the currently selected row.
    With Me.fpsprdData
        .Row = .ActiveRow
        .Col = .GetColFromID("SKU")
        strSKUN = .Text
    End With
    
    If (strSKUN = "") Then
        Call MsgBox("The product selected has no SKUN" & vbCrLf & _
                        " number to enquire about.", vbInformation + vbOKOnly, "Products Has No SKUN")
    End If
    
    ' Get the name of the Item Enquiry application.
    strEnquiryEXE = goSession.GetParameter(PRM_ENQUIRY_EXE)
    ' CreateCommandLine passes the set value to the /P, and creates the, /u and /c command line properties
    '   of the program called.
    strEnquiryEXE = strEnquiryEXE & " " & goSession.CreateCommandLine("SKU=" & strSKUN)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Shell - " & strEnquiryEXE)
    Call ShellWait(strEnquiryEXE, SW_MAX)
    
    Exit Sub
    
cmdSkuEnquiry_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Sub

' View the Audit report.
Private Sub cmdViewReport_Click()

Const PROCEDURE_NAME As String = "cmdViewReport_Click"
    
    On Error GoTo cmdViewReport_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdViewReport Click")

    ' Load Form frmAuditReport to allow viewing of the Audit report.
    Me.Visible = False
    Load frmAuditReport
    Set frmAuditReport.DBConnection = mcnnDBConnection
    frmAuditReport.Standalone = mblnStandAlone
    frmAuditReport.Show vbModal
    Me.Visible = True
    Unload frmAuditReport
    
    Exit Sub
    
cmdViewReport_Click_Error:
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Sub

Private Function InitialiseBarCodeScanner() As Boolean

Const PROCEDURE_NAME As String = "InitialiseBarCodeScanner"

    On Error GoTo InitialiseBarCodeScanner_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Initialise Barcode Scanner")
       
    With Me.comBarScanner
        .CommPort = Replace(UCase(Left(mstrComPortSettings, InStr(mstrComPortSettings, ",") - 1)), "COM", "")
        .Settings = Mid(mstrComPortSettings, InStr(mstrComPortSettings, ",") + 1)
        .DTREnable = True
        .RTSEnable = True
        .RThreshold = 1
        .SThreshold = 1
        If (.PortOpen = False) Then
            .PortOpen = True
        End If
    End With

    InitialiseBarCodeScanner = True
    
    Exit Function
    
InitialiseBarCodeScanner_Error:
    
    InitialiseBarCodeScanner = False
    
    ' Check if the error was that the selected com port was not connected and not running in Standalone mode.
    If (mblnStandAlone = False) And (Err.Number = 8002) Then
        Call MsgBox("The Scanner could not be setup on the Serial Com Port - " & _
                Replace(UCase(Left(mstrComPortSettings, InStr(mstrComPortSettings, ",") - 1)), "COM", "") & vbCrLf & vbCrLf & _
                "Please check into this problem if you are attempting to use a Serial Scanner." & vbCrLf & _
                "The Com Port number can be changed using the Parameter Editor application" & vbCrLf & _
                "adjusting parameter - " & PRM_WIXCOMPORT, vbInformation + vbOKOnly, _
                "Serial Com Port - " & _
                Replace(UCase(Left(mstrComPortSettings, InStr(mstrComPortSettings, ",") - 1)), "COM", "") & " Not Set Up")
        Err.Clear
        Exit Function
    ElseIf (mblnStandAlone = True) Then
        Err.Clear
        Exit Function
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function

Private Function InitialiseUSBBarCodeScanner() As Boolean

Const PROCEDURE_NAME As String = "InitialiseUSBBarCodeScanner"

    On Error GoTo InitialiseUSBBarCodeScanner_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Initialise USB Barcode Scanner")
       
    With Me.comUSBScanner
        .Open ("STI_USBSCANNER")
        .Claim (0)
        .DeviceEnabled = True
        .DataEventEnabled = True
        .AutoDisable = False
        .CheckHealth (0)
    End With

    InitialiseUSBBarCodeScanner = True
    
    Exit Function
    
InitialiseUSBBarCodeScanner_Error:
    
    InitialiseUSBBarCodeScanner = False
        
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function

' Check if the path exists, if not create it.
Private Sub TestPathExists(ByRef strPath As String)

Const PROCEDURE_NAME As String = "TestPathExists"

Dim objFSO As Scripting.FileSystemObject
Dim objFolder As Scripting.Folder

    On Error GoTo TestPathExists_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Test Path Exists")
    
    Set objFSO = New FileSystemObject
    
    ' Test if the folder exists.
    If (objFSO.FolderExists(strPath) = False) Then
        ' Create the folder.
        objFSO.CreateFolder (strPath)
    End If
    
    ' Make sure the path ends with '\'.
    If (Right(strPath, 1) <> "\") Then
        strPath = strPath & "\"
    End If

    Set objFSO = Nothing
    
    Exit Sub
    
TestPathExists_Error:

    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Get the store or Warehouse number.
Private Sub GetStoreNumber()

Const PROCEDURE_NAME As String = "GetStoreNumber"

    On Error GoTo GetStoreNumber_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Store Number")
    
    ' Load Form frmStoreNo to allow user entry of the Store \ Warehouse number.
    Me.Visible = False
    frmStoreNo.Show vbModal
    Me.Visible = True
    mstrStoreNo = frmStoreNo.StoreNo
    Unload frmStoreNo
        
    ' If the user did not enter a Store number then Exit.
    If (mstrStoreNo = "") Then
        Unload Me
    Else
        Me.lblStoreNo.Caption = mstrStoreNo
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Store No - " & mstrStoreNo)
    End If
    
    Exit Sub
    
GetStoreNumber_Error:
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Get the date of the last stock file update.
Private Sub GetLastStockFileUpdateDate()

Const PROCEDURE_NAME As String = "GetLastStockFileUpdateDate"

Dim objFSO        As Scripting.FileSystemObject
Dim objFile       As Scripting.File
Dim blnFileExists As Boolean

    On Error GoTo GetLastStockFileUpdateDate_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Last Stock File Update Date")
    
    ' Test if the files exist, if so get the last modified date.
    Set objFSO = New Scripting.FileSystemObject
    blnFileExists = objFSO.FileExists(mstrWixData & SUCCESS_FILE)
    If (blnFileExists = True) Then
        Set objFile = objFSO.GetFile(mstrWixData & SUCCESS_FILE)
        Me.lblStockUpdateDate.Caption = Format(objFile.DateLastModified, "dd/mm/yy")
        Select Case DateDiff("d", objFile.DateLastModified, Now)
            Case 0 To 4
                Me.lblStockUpdateDate.ForeColor = &HC000&
            Case 5 To 14
                Me.lblStockUpdateDate.ForeColor = &H80FF&
            Case Else
                Me.lblStockUpdateDate.ForeColor = vbRed
        End Select
    Else
        Me.lblStockUpdateDate.Caption = "Not Found"
        Me.lblStockUpdateDate.ForeColor = vbRed
    End If
    
    Set objFile = Nothing
    Set objFSO = Nothing
            
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Last Stock File Update Date - " & _
                        Me.lblStockUpdateDate.Caption)
    
    Exit Sub
    
GetLastStockFileUpdateDate_Error:
   
    Set objFile = Nothing
    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub comBarScanner_OnComm()
    
Const PROCEDURE_NAME As String = "comBarScanner_OnComm"

Static strInBuffer As String
    
    On Error GoTo comBarScanner_OnComm_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "comBarScanner OnComm")
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "COMM Event - " & comBarScanner.CommEvent)
    
    Select Case comBarScanner.CommEvent
   ' Handle each event or error by placing
   ' code below each case statement

   ' Errors
        Case comEventBreak   ' A Break was received.
            Exit Sub
        Case comEventFrame   ' Framing Error
            Exit Sub
        Case comEventOverrun   ' Data Lost.
            Exit Sub
        Case comEventRxOver   ' Receive buffer overflow.
            Exit Sub
        Case comEventRxParity   ' Parity Error.
            Exit Sub
        Case comEventTxFull   ' Transmit buffer full.
            Exit Sub
        Case comEventDCB   ' Unexpected error retrieving DCB]
            Exit Sub
    
       ' Events
        Case comEvCD   ' Change in the CD line.
            Exit Sub
        Case comEvCTS   ' Change in the CTS line.
            Exit Sub
        Case comEvDSR   ' Change in the DSR line.
            Exit Sub
        Case comEvRing   ' Change in the Ring Indicator.
            Exit Sub
        Case comEvReceive   ' Received RThreshold # of
                            ' chars.
        Case comEvSend   ' There are SThreshold number of
                         ' characters in the transmit
                         ' buffer.
            Exit Sub
        Case comEvEOF   ' An EOF charater was found in
                         ' the input stream
            Exit Sub
    End Select

    strInBuffer = strInBuffer & comBarScanner.Input
    If InStr(1, strInBuffer, vbCr) = 0 Then
        Exit Sub
    End If
    
    strInBuffer = Replace(strInBuffer, vbCr, vbNullString)
    strInBuffer = Replace(strInBuffer, vbLf, vbNullString)

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Barcode Number - " & strInBuffer)

    ' Process the Barcode just scanned.
    If (mblnProcBarcode = False) And (frmUserEntry.Visible = False) Then
        Call ProcessBarcode(strInBuffer, False)
    End If
    
    ' Clear out the buffer for the next scan.
    strInBuffer = ""
    
    Exit Sub
    
comBarScanner_OnComm_Error:
   
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Process the details for the Barcode.
Private Sub ProcessBarcode(strBarcode As String, blnKeyed As Boolean)

Const PROCEDURE_NAME As String = "ProcessBarcode"

Dim recSKU      As ADODB.Recordset
Dim strSQL      As String
Dim blnEnterSKU As Boolean
Dim strSKU      As String

    On Error GoTo ProcessBarcode_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Process Barcode")
    
    mblnProcBarcode = True
    
    ' Show the barcode as the EAN value on the list.
    With Me.fpsprdData
        .MaxRows = .MaxRows + 1
        ' Lock the row.
        .Row = .MaxRows
        .Row2 = .MaxRows
        .Col = 1
        .Col2 = .MaxCols
        .BlockMode = True
        .Lock = True
        .BlockMode = False
        .Row = .MaxRows
        ' Show the EAN.
        .Col = .GetColFromID("EAN")
        .Text = Format(strBarcode, "0000000000000000")
        ' Show if Barcode keyed in or not.
        .Col = .GetColFromID("Keyed")
        If (blnKeyed = True) Then
            .Text = "Y"
        Else
            .Text = "N"
        End If
    End With
    
    ' Try to match the barcode with fields EANN or EANW in table BCVSKU for Standalone version or field NUMB in
    '   table EANMAS for the Store version to get the SKU details.
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    If (mblnStandAlone = True) Then
        strSQL = "SELECT * FROM BCVSKU WHERE EANN = '" & strBarcode & "' OR EANW = '" & strBarcode & "'"
    Else
        strSQL = "SELECT * FROM EANMAS WHERE NUMB = '" & strBarcode & "'"
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQL)
    Set recSKU = mcnnDBConnection.Execute(strSQL, , adCmdText)
    ' If the Barcode was found then fill in the list values, else ask the user to
    '   enter the SKU details.
    blnEnterSKU = False
    If (recSKU.EOF = False) Then
        ' Show if Barcode exists or not.
        With Me.fpsprdData
            .Col = .GetColFromID("Exists")
            .Text = "Y"
        End With
        ' If running the Store version get the SKU details from the table STKMAS.
        If (mblnStandAlone = False) Then
            If Not IsNull(recSKU.Fields("SKUN")) Then
                strSQL = "SELECT SKUN, DESCR FROM STKMAS WHERE SKUN = '" & recSKU.Fields("SKUN") & "'"
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQL)
                Set recSKU = mcnnDBConnection.Execute(strSQL, , adCmdText)
                If (recSKU.EOF = False) Then
                    If IsNull(recSKU.Fields("SKUN")) Then
                        blnEnterSKU = True
                    End If
                Else
                    blnEnterSKU = True
                End If
            Else
                blnEnterSKU = True
            End If
        End If
    Else
        ' Show if Barcode exists or not.
        With Me.fpsprdData
            .Col = .GetColFromID("Exists")
            .Text = "N"
        End With
        blnEnterSKU = True
    End If
    
    ' If the SKU was not found then allow the user to enter it manually.
    If (blnEnterSKU = True) Then
        ' Show that SKU does not exist.
        With Me.fpsprdData
            .Col = .GetColFromID("AutoMatch")
            .Text = "N"
        End With
        ' Load Form frmUserEntry to allow user entry of the SKU number.
        Me.Visible = False
        Load frmUserEntry
        frmUserEntry.BarcodeEntry = False
        frmUserEntry.Show vbModal
        Me.Visible = True
        strSKU = frmUserEntry.EnteredNumber
        Unload frmUserEntry
        ' If the user entered a SKU number then check it against the Standalone or Store table.
        If (strSKU <> "") Then
            ' Store the entered SKU.
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("SKU")
            Me.fpsprdData.Text = Format(strSKU, "000000")
            If (mblnStandAlone = True) Then
                strSQL = "SELECT SKUN, DESCR FROM BCVSKU WHERE SKUN = '" & strSKU & "'"
            Else
                strSQL = "SELECT SKUN, DESCR FROM STKMAS WHERE SKUN = '" & strSKU & "'"
            End If
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "SQL - " & strSQL)
            Set recSKU = mcnnDBConnection.Execute(strSQL, , adCmdText)
            ' Store the found description.
            If (recSKU.EOF = False) Then
                If Not IsNull(recSKU.Fields("DESCR")) Then
                    Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("Description")
                    Me.fpsprdData.Text = recSKU.Fields("DESCR").Value
                End If
            End If
        End If
    Else
        ' Show that SKU exists.
        With Me.fpsprdData
            .Col = .GetColFromID("AutoMatch")
            .Text = "Y"
        End With
        ' Store the found SKU and description.
        With Me.fpsprdData
            Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("SKU")
            .Text = recSKU.Fields("SKUN").Value
            If Not IsNull(recSKU.Fields("DESCR")) Then
                Me.fpsprdData.Col = Me.fpsprdData.GetColFromID("Description")
                .Text = recSKU.Fields("DESCR").Value
            End If
        End With
    End If
    
    Set recSKU = Nothing
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    
    mblnProcBarcode = False
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Process Barcode Finished")
    
    Exit Sub
    
ProcessBarcode_Error:
   
    mblnProcBarcode = False
    
    Set recSKU = Nothing
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub comUSBScanner_DataEvent(ByVal lStatus As Long)

Const PROCEDURE_NAME As String = "comUSBScanner_DataEvent"

Dim strBarcode As String
    
    On Error GoTo comUSBScanner_DataEvent_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "comUSBScanner DataEvent")

    With Me.comUSBScanner
        strBarcode = .ScanData
        .DataEventEnabled = True
    End With
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Barcode Number - " & strBarcode)

    ' Process the Barcode just scanned.
    If (mblnProcBarcode = False) Then
        Call ProcessBarcode(strBarcode, False)
    End If
    
    Exit Sub
    
comUSBScanner_DataEvent_Error:
       
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub
