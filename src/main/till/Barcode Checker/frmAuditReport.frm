VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmAuditReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Audit Report"
   ClientHeight    =   7965
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11505
   ControlBox      =   0   'False
   Icon            =   "frmAuditReport.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   11505
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdResetFilters 
      Caption         =   "F9 - Reset Filters"
      Height          =   400
      Left            =   5940
      TabIndex        =   22
      Top             =   7130
      Width           =   1750
   End
   Begin VB.CommandButton cmdApplyFilter 
      Caption         =   "F8 - Apply Filter"
      Height          =   400
      Left            =   3815
      TabIndex        =   21
      Top             =   7130
      Width           =   1750
   End
   Begin VB.ComboBox cboID 
      Height          =   315
      Left            =   8805
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   770
      Width           =   1695
   End
   Begin VB.ComboBox cboSupplier 
      Height          =   315
      Left            =   1005
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   1370
      Width           =   1695
   End
   Begin VB.ComboBox cboKeyed 
      Height          =   315
      Left            =   3600
      Style           =   2  'Dropdown List
      TabIndex        =   18
      Top             =   1370
      Width           =   1695
   End
   Begin VB.ComboBox cboExists 
      Height          =   315
      Left            =   6203
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   1370
      Width           =   1695
   End
   Begin VB.ComboBox cboAuto 
      Height          =   315
      Left            =   8805
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   1370
      Width           =   1695
   End
   Begin VB.ComboBox cboStore 
      Height          =   315
      Left            =   6203
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   770
      Width           =   1695
   End
   Begin MSComCtl2.DTPicker dtpStartDate 
      Height          =   315
      Left            =   1005
      TabIndex        =   5
      Top             =   770
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   20578305
      CurrentDate     =   38666
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   400
      Left            =   8065
      TabIndex        =   3
      Top             =   7130
      Width           =   1750
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F7 - Print Report"
      Height          =   400
      Left            =   1690
      TabIndex        =   2
      Top             =   7130
      Width           =   1750
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   7590
      Width           =   11505
      _ExtentX        =   20294
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmAuditReport.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12938
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "10:04"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin FPSpreadADO.fpSpread fpsprdReport 
      Height          =   5055
      Left            =   45
      TabIndex        =   4
      Top             =   1920
      Width           =   11400
      _Version        =   458752
      _ExtentX        =   20108
      _ExtentY        =   8916
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   10
      RetainSelBlock  =   0   'False
      ScrollBarShowMax=   0   'False
      SpreadDesigner  =   "frmAuditReport.frx":2756
      VScrollSpecialType=   2
      ScrollBarTrack  =   3
   End
   Begin MSComCtl2.DTPicker dtpEndDate 
      Height          =   315
      Left            =   3600
      TabIndex        =   7
      Top             =   770
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   20578305
      CurrentDate     =   38666
   End
   Begin VB.Label lblAuto 
      Alignment       =   2  'Center
      Caption         =   "Auto Matched"
      Height          =   285
      Left            =   8805
      TabIndex        =   14
      Top             =   1160
      Width           =   1695
   End
   Begin VB.Label lblExists 
      Alignment       =   2  'Center
      Caption         =   "Exists"
      Height          =   285
      Left            =   6210
      TabIndex        =   13
      Top             =   1160
      Width           =   1695
   End
   Begin VB.Label lblKeyed 
      Alignment       =   2  'Center
      Caption         =   "Keyed"
      Height          =   285
      Left            =   3600
      TabIndex        =   12
      Top             =   1160
      Width           =   1695
   End
   Begin VB.Label lblSupplier 
      Alignment       =   2  'Center
      Caption         =   "Supplier"
      Height          =   285
      Left            =   1005
      TabIndex        =   11
      Top             =   1160
      Width           =   1695
   End
   Begin VB.Label lblID 
      Alignment       =   2  'Center
      Caption         =   "ID"
      Height          =   285
      Left            =   8805
      TabIndex        =   10
      Top             =   530
      Width           =   1695
   End
   Begin VB.Label lblStore 
      Alignment       =   2  'Center
      Caption         =   "Store"
      Height          =   285
      Left            =   6210
      TabIndex        =   9
      Top             =   530
      Width           =   1695
   End
   Begin VB.Label lblEndDate 
      Alignment       =   2  'Center
      Caption         =   "End Date"
      Height          =   285
      Left            =   3600
      TabIndex        =   8
      Top             =   530
      Width           =   1695
   End
   Begin VB.Label lblStartDate 
      Alignment       =   2  'Center
      Caption         =   "Start Date"
      Height          =   285
      Left            =   1005
      TabIndex        =   6
      Top             =   530
      Width           =   1695
   End
   Begin VB.Shape Shape1 
      Height          =   1695
      Left            =   195
      Top             =   135
      Width           =   11115
   End
   Begin VB.Label lblTitle 
      Alignment       =   2  'Center
      Caption         =   "Report Filter Settings"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3105
      TabIndex        =   1
      Top             =   240
      Width           =   5295
   End
End
Attribute VB_Name = "frmAuditReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Barcode Checker/frmAuditReport.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 02/11/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Designed to allow the user to view the Audit report on the screen and also print
'*              the report. Various filters to the data can be set.
'*
'**********************************************************************************************
'* Versions:
'*
'* 02/11/05 DaveF   v1.0.0  Initial build. WIX1170.
'*
'* 28/04/06 DaveF   v1.0.7  Added totals to the report.
'*
'* 19/10/06 DaveF   v1.0.9  Changed the report so that EAN Exists = Y is based only only the
'*                              the data value EANAUDIT.EXIST = 1.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmAuditReport"

Private mobjUser        As cEnterprise_Wickes.cUser
Private mcnnConnection  As ADODB.Connection
Private mrecData        As ADODB.Recordset
Private mblnStandAlone  As Boolean

' Form load event.
Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

Dim ctlGeneric As Control

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
    Call InitialiseStatusBar(sbStatus)
    
    ' Setup the forms display.
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    For Each ctlGeneric In Me.Controls
        If (TypeOf ctlGeneric Is Label) Then
            ctlGeneric.BackColor = Me.BackColor
        End If
    Next ctlGeneric
    
    Exit Sub
    
FormLoad_Error:
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Form Activate event.
Private Sub Form_Activate()

Const PROCEDURE_NAME As String = "Form_Activate"

    On Error GoTo Form_Activate_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Activate")
        
    ' Set up the various filters.
    Call SetupFilters
    
    ' Get the full data from the database.
    Call GetData
    
    ' Show the data on the grid.
    Call ShowData
    
    Exit Sub
    
Form_Activate_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case vbKeyF7
                    KeyCode = 0
                    Call cmdPrint_Click         ' Print the report.
            
            Case vbKeyF8
                    KeyCode = 0
                    Call cmdApplyFilter_Click   ' Apply the filter.
            
            Case vbKeyF9
                    KeyCode = 0
                    Call cmdResetFilters_Click  ' Reset the filters.
            
            Case vbKeyF10
                    KeyCode = 0
                    Call cmdExit_Click          ' Exit from the report.
         End Select
     End If

End Sub

' Apply the filter.
Private Sub cmdApplyFilter_Click()

    ' Show the data on the grid using the set filter.
    Call ShowData
    
End Sub

' Exit the report.
Private Sub cmdExit_Click()

Const PROCEDURE_NAME As String = "cmdExit_Click"

    On Error GoTo cmdExit_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdExit Click")
   
    Me.Hide

    Exit Sub
    
cmdExit_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Sub

' Print the report.
Private Sub cmdPrint_Click()
Dim strfont1, strfont2, StrFont3, strbuf  As String
Dim StrHeadings(1 To 8) As String * 20

Dim strCriteria(1 To 8) As String * 20

Const PROCEDURE_NAME As String = "cmdPrint_Click"

    On Error GoTo cmdPrint_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdPrint Click")
    
    ' Create a font as Arial, size 10, bold, no italics,
    '   underline, no strikethrough and save it as font #1
    strfont1 = "/fn""Tahoma""/fz""16""/fb1/fi0/fu1/fk0/fs1" ' Heading
    strfont2 = "/fn""Tahoma""/fz""10""/fb1/fi0/fu1/fk0/fs2"
    StrFont3 = "/fn""Tahoma""/fz""10""/fb1/fi0/fu0/fk0/fs2"
    ' Set The headings
    StrHeadings(1) = "Start Date"
    StrHeadings(2) = "  End Date "
    StrHeadings(3) = "Store No."
    StrHeadings(4) = "  I.D   "
    StrHeadings(5) = "Supplier "
    StrHeadings(6) = " Keyed   "
    StrHeadings(7) = " Exists  "
    StrHeadings(8) = "Auto Matched"
    
    strCriteria(1) = dtpStartDate.Value
    strCriteria(2) = dtpEndDate.Value
    strCriteria(3) = cboStore.Text
    strCriteria(4) = cboID.Text
    strCriteria(5) = cboSupplier.Text
    strCriteria(6) = cboKeyed.Text
    strCriteria(7) = cboExists.Text
    strCriteria(8) = cboAuto.Text
    
    ' Recall font configurations and set the header and footer text
    strbuf = strfont1 & strfont2 & StrFont3 & "/f2Date :/date/c/f1Barcode Audit Report/n/n/n/f2/fu1 " & _
        StrHeadings(1) & Space(30) & StrHeadings(2) & Space(30) & StrHeadings(3) & Space(30) & _
        StrHeadings(4) & "/n/f3/fu0" & strCriteria(1) & Space(30) & strCriteria(2) & Space(30) & _
        strCriteria(3) & Space(30) & strCriteria(4) & "/n/n/f2/fu1" & _
        StrHeadings(5) & Space(30) & StrHeadings(6) & Space(31) & StrHeadings(7) & Space(30) & _
        StrHeadings(8) & "/n/f3/fu0" & strCriteria(5) & Space(35) & strCriteria(6) & Space(30) & _
        strCriteria(7) & Space(30) & strCriteria(8) & "/n/n"

        
    fpsprdReport.PrintFooter = "/f2Page: /p"
    fpsprdReport.PrintHeader = strbuf

    fpsprdReport.PrintScalingMethod = PrintScalingMethodSmartPrint
    fpsprdReport.PrintBestFitPagesWide = True
    fpsprdReport.PrintSheet
    
    Exit Sub
    
cmdPrint_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Reset the filters.
Private Sub cmdResetFilters_Click()

    Me.dtpStartDate.Value = Me.dtpStartDate.MinDate
    Me.dtpEndDate.Value = Me.dtpEndDate.MaxDate
    Me.cboStore.ListIndex = 0
    Me.cboID.ListIndex = 0
    Me.cboSupplier.ListIndex = 0
    Me.cboKeyed.ListIndex = 0
    Me.cboExists.ListIndex = 0
    Me.cboAuto.ListIndex = 0
    
End Sub

' Store the Database connnection to use.
Public Property Set DBConnection(ByVal cnnConnection As ADODB.Connection)

    Set mcnnConnection = cnnConnection
    
End Property

' Store if running in standalone mode or Store based.
Public Property Let Standalone(ByVal blnStandalone As Boolean)

    mblnStandAlone = blnStandalone
    
End Property

' Get the data from the database.
Private Sub GetData()

Const PROCEDURE_NAME As String = "GetData"

Dim strSQL  As String

    On Error GoTo GetData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Data")
    
    If (mcnnConnection.State = adStateClosed) Then
        mcnnConnection.Open
    End If
    ' Test if running in standalone mode or store based.
    If (mblnStandAlone = True) Then
        ' Standalone.
        strSQL = "SELECT a.DATE1, a.STOR, a.EEID, a.EANN, a.KEYD, a.EXIST, a.SKUN, a.AUTO, b.DESCR, c.NAME " & _
                    "FROM EANAUDIT a LEFT JOIN BCVSKU b ON a.SKUN = b.SKUN " & _
                    "LEFT JOIN BCVSUP c ON b.SUPN = c.SUPN " & _
                    "ORDER BY a.STOR, a.DATE1, a.EEID, a.EANN"
    Else
        ' Store.
        strSQL = "SELECT a.DATE1, a.STOR, a.EEID, a.EANN, a.KEYD, a.EXIST, a.SKUN, a.AUTO, B.DESCR, c.NAME " & _
                    "FROM EANAUDIT a LEFT JOIN STKMAS b ON a.SKUN = b.SKUN " & _
                    "LEFT JOIN SUPMAS c ON b.SUPP = c.SUPN " & _
                    "ORDER BY a.STOR, a.DATE1, a.EEID, a.EANN"
    End If
    Set mrecData = New ADODB.Recordset
    Call mrecData.Open(strSQL, mcnnConnection, adOpenStatic, adLockOptimistic)
    
    Exit Sub
    
GetData_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Setup the various filters.
Private Sub SetupFilters()

Const PROCEDURE_NAME As String = "SetupFilters"

Dim recData As ADODB.Recordset
Dim strSQL  As String

    On Error GoTo SetupFilters_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setup Filters")

    If (mcnnConnection.State = adStateClosed) Then
        mcnnConnection.Open
    End If
    
    ' Setup the start and end dates.
    strSQL = "SELECT MIN(DATE1) as MinDate, MAX(DATE1) as MaxDate " & _
                "FROM EANAUDIT"
    Set recData = mcnnConnection.Execute(strSQL, , adCmdText)
    If (recData.EOF = False) Then
        If Not IsNull(recData.Fields("MinDate")) Then
            Me.dtpStartDate.MinDate = recData.Fields("MinDate")
            Me.dtpEndDate.MinDate = recData.Fields("MinDate")
            Me.dtpStartDate.Value = recData.Fields("MinDate")
        End If
        If Not IsNull(recData.Fields("MaxDate")) Then
            Me.dtpStartDate.MaxDate = recData.Fields("MaxDate")
            Me.dtpEndDate.MaxDate = recData.Fields("MaxDate")
            Me.dtpEndDate.Value = recData.Fields("MaxDate")
        End If
    End If
    Set recData = Nothing
    
    ' Setup the list of Stores.
    ' Add value 'All' to the list.
    Me.cboStore.AddItem ("All")
    strSQL = "SELECT STOR FROM EANAUDIT GROUP BY STOR ORDER BY STOR"
    Set recData = mcnnConnection.Execute(strSQL, , adCmdText)
    Do Until (recData.EOF = True)
        If Not IsNull(recData.Fields("STOR")) Then
            Me.cboStore.AddItem (recData.Fields("STOR"))
        End If
        recData.MoveNext
    Loop
    Set recData = Nothing
    Me.cboStore.ListIndex = 0
    
    ' Setup the list of IDs.
    ' Add value 'All' to the list.
    Me.cboID.AddItem ("All")
    strSQL = "SELECT EEID FROM EANAUDIT GROUP BY EEID ORDER BY EEID"
    Set recData = mcnnConnection.Execute(strSQL, , adCmdText)
    Do Until (recData.EOF = True)
        If Not IsNull(recData.Fields("EEID")) Then
            Me.cboID.AddItem (recData.Fields("EEID"))
        End If
        recData.MoveNext
    Loop
    Set recData = Nothing
    Me.cboID.ListIndex = 0
    
    ' Setup the list of Suppliers.
    ' Add value 'All' to the list.
    Me.cboSupplier.AddItem ("All")
    ' Test if running in standalone mode or store based.
    If (mblnStandAlone = True) Then
        ' Standalone.
        strSQL = "SELECT NAME FROM BCVSUP a INNER JOIN BCVSKU b ON a.SUPN = b.SUPN " & _
                    "INNER JOIN EANAUDIT c ON b.SKUN = c.SKUN " & _
                    "GROUP BY NAME ORDER BY NAME"
    Else
        ' Store.
        strSQL = "SELECT NAME FROM SUPMAS a INNER JOIN STKMAS b ON a.SUPN = b.SUPP " & _
                    "INNER JOIN EANAUDIT c ON b.SKUN = c.SKUN " & _
                    "GROUP BY NAME ORDER BY NAME"
    End If
    Set recData = mcnnConnection.Execute(strSQL, , adCmdText)
    Do Until (recData.EOF = True)
        If Not IsNull(recData.Fields("NAME")) Then
            Me.cboSupplier.AddItem (recData.Fields("NAME"))
        End If
        recData.MoveNext
    Loop
    Set recData = Nothing
    Me.cboSupplier.ListIndex = 0
    
    ' Setup the list of Keyed values.
    With Me.cboKeyed
        .AddItem ("Either")
        .AddItem ("Yes")
        .AddItem ("No")
        .ListIndex = 0
    End With
    
    ' Setup the list of EAN Exists values.
    With Me.cboExists
        .AddItem ("Either")
        .AddItem ("Yes")
        .AddItem ("No")
        .ListIndex = 0
    End With
    
    ' Setup the list of Auto Matched values.
    With Me.cboAuto
        .AddItem ("Either")
        .AddItem ("Yes")
        .AddItem ("No")
        .ListIndex = 0
    End With
    
    Set recData = Nothing
    If (mcnnConnection.State = adStateOpen) Then
        mcnnConnection.Close
    End If
    
    Exit Sub
    
SetupFilters_Error:

    Set recData = Nothing
    If (mcnnConnection.State = adStateOpen) Then
        mcnnConnection.Close
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Show the required data on the grid.
Private Sub ShowData()

Const PROCEDURE_NAME As String = "ShowData"

Dim strFilter           As String

Dim strLastStore        As String
Dim strLastDate         As String
Dim strNewStore         As String
Dim strNewDate          As String
Dim intMainKeyedTotal   As Integer
Dim intMainExistsTotal  As Integer
Dim intMainAutoTotal    As Integer
Dim intStoreKeyedTotal  As Integer
Dim intStoreExistsTotal As Integer
Dim intStoreAutoTotal   As Integer
Dim intDateKeyedTotal   As Integer
Dim intDateExistsTotal  As Integer
Dim intDateAutoTotal    As Integer
Dim blnFirstRow         As Boolean

Dim blnStoreChanged     As Boolean
Dim blnDateChanged      As Boolean

    On Error GoTo ShowData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Show Data")
    
    ' Filter the data.
    strFilter = BuildFilter
    
    ' Apply the filter.
    mrecData.Filter = strFilter

    ' Put the data on the grid.
    If (mrecData.EOF = True) Then
        ' Warn if no data was returned.
        Me.fpsprdReport.MaxRows = 0
        Call MsgBox("No data was found in the database for the report", vbInformation + vbOKOnly, _
                        "No Data For Report")
    Else
        ' Get the initial values.
        If Not IsNull(mrecData.Fields("STOR")) Then
            strLastStore = mrecData.Fields("STOR")
        End If
        If Not IsNull(mrecData.Fields("DATE1")) Then
            strLastDate = mrecData.Fields("DATE1")
        End If
        ' Add a new row for the data.
        With Me.fpsprdReport
            blnFirstRow = True
            .MaxRows = 1
            .Row = .MaxRows
            ' Add the Store or Date values.
            .Col = .GetColFromID("Store")
            .Text = strLastStore
            .Col = .GetColFromID("Date")
            .Text = strLastDate
        
            intMainKeyedTotal = 0
            intMainExistsTotal = 0
            intMainAutoTotal = 0
            
            ' Loop over the data in the recordset.
            Do Until (mrecData.EOF = True)
                
                blnStoreChanged = False
                blnDateChanged = False
                ' Test if the store has changed.
                If Not IsNull(mrecData.Fields("STOR")) Then
                    strNewStore = mrecData.Fields("STOR")
                End If
                If (strNewStore <> strLastStore) Then
                    blnStoreChanged = True
                    strLastStore = strNewStore
                End If
                ' Test if the date has changed.
                If Not IsNull(mrecData.Fields("DATE1")) Then
                    strNewDate = mrecData.Fields("DATE1")
                End If
                If (strNewDate <> strLastDate) Then
                    blnDateChanged = True
                    strLastDate = strNewDate
                End If
                
                ' Add the Date totals.
                If (blnStoreChanged = True) Or (blnDateChanged = True) Then
                    .MaxRows = .MaxRows + 1
                    .Row = .MaxRows
                    .Col = .GetColFromID("Supplier")
                    .Text = "Date Total"
                    .TypeHAlign = TypeHAlignRight
                    .FontBold = True
                    .Col = .GetColFromID("Keyed")
                    .Text = intDateKeyedTotal
                    .FontBold = True
                    .Col = .GetColFromID("Exists")
                    .Text = intDateExistsTotal
                    .FontBold = True
                    .Col = .GetColFromID("Auto")
                    .Text = intDateAutoTotal
                    .FontBold = True
                    intDateKeyedTotal = 0
                    intDateExistsTotal = 0
                    intDateAutoTotal = 0
                    .SetCellBorder .GetColFromID("Supplier"), .Row, .GetColFromID("Auto"), .Row, CellBorderIndexTop, _
                            -1, CellBorderStyleSolid
                    If (blnStoreChanged = False) Then
                        .SetCellBorder .GetColFromID("Supplier"), .Row, .GetColFromID("Auto"), .Row, CellBorderIndexBottom, _
                                -1, CellBorderStyleSolid
                    End If
                End If
                
                ' Add the Store totals.
                If (blnStoreChanged = True) Then
                    .MaxRows = .MaxRows + 1
                    .Row = .MaxRows
                    .Col = .GetColFromID("Supplier")
                    .Text = "Store Total"
                    .TypeHAlign = TypeHAlignRight
                    .FontBold = True
                    .Col = .GetColFromID("Keyed")
                    .Text = intStoreKeyedTotal
                    .FontBold = True
                    .Col = .GetColFromID("Exists")
                    .Text = intStoreExistsTotal
                    .FontBold = True
                    .Col = .GetColFromID("Auto")
                    .Text = intStoreAutoTotal
                    .FontBold = True
                    intStoreKeyedTotal = 0
                    intStoreExistsTotal = 0
                    intStoreAutoTotal = 0
                    .SetCellBorder .GetColFromID("Supplier"), .Row, .GetColFromID("Auto"), .Row, CellBorderIndexBottom, _
                            -1, CellBorderStyleSolid
                End If
                
                ' Add a new row for the data.
                If (blnFirstRow = False) Then
                    .MaxRows = .MaxRows + 1
                    .Row = .MaxRows
                Else
                    blnFirstRow = False
                End If
                
                ' If changed add the Store or Date values.
                If (blnStoreChanged = True) Then
                    .Col = .GetColFromID("Store")
                    .Text = strNewStore
                    .Col = .GetColFromID("Date")
                    .Text = strNewDate
                End If
                If (blnDateChanged = True) Then
                    .Col = .GetColFromID("Date")
                    .Text = strNewDate
                End If
                
                ' Add the rest of the data line.
                .Col = .GetColFromID("ID")
                If Not IsNull(mrecData.Fields("EEID")) Then
                    .Text = mrecData.Fields("EEID")
                End If
                .Col = .GetColFromID("EAN")
                If Not IsNull(mrecData.Fields("EANN")) Then
                    .Text = mrecData.Fields("EANN")
                End If
                .Col = .GetColFromID("SKU")
                If Not IsNull(mrecData.Fields("SKUN")) Then
                    .Text = mrecData.Fields("SKUN")
                End If
                .Col = .GetColFromID("Description")
                If Not IsNull(mrecData.Fields("DESCR")) Then
                    .Text = mrecData.Fields("DESCR")
                End If
                .Col = .GetColFromID("Supplier")
                If Not IsNull(mrecData.Fields("NAME")) Then
                    .Text = mrecData.Fields("NAME")
                End If
                .Col = .GetColFromID("Keyed")
                If Not IsNull(mrecData.Fields("KEYD")) Then
                    If (mrecData.Fields("KEYD") = True) Then
                        .Text = "Y"
                        intMainKeyedTotal = intMainKeyedTotal + 1
                        intStoreKeyedTotal = intStoreKeyedTotal + 1
                        intDateKeyedTotal = intDateKeyedTotal + 1
                    Else
                        .Text = "N"
                    End If
                End If
                .Col = .GetColFromID("Exists")
                If Not IsNull(mrecData.Fields("EXIST")) Then
                    If (mrecData.Fields("EXIST") = True) Then
                        .Text = "Y"
                        intMainExistsTotal = intMainExistsTotal + 1
                        intStoreExistsTotal = intStoreExistsTotal + 1
                        intDateExistsTotal = intDateExistsTotal + 1
                    Else
                        .Text = "N"
                    End If
                End If
                .Col = .GetColFromID("Auto")
                If Not IsNull(mrecData.Fields("AUTO")) Then
                    If (mrecData.Fields("AUTO") = True) Then
                        .Text = "Y"
                        intMainAutoTotal = intMainAutoTotal + 1
                        intStoreAutoTotal = intStoreAutoTotal + 1
                        intDateAutoTotal = intDateAutoTotal + 1
                    Else
                        .Text = "N"
                    End If
                End If
                mrecData.MoveNext
            Loop
            
            ' Add the Date totals.
            .MaxRows = .MaxRows + 1
            .Row = .MaxRows
            .Col = .GetColFromID("Supplier")
            .Text = "Date Total"
            .TypeHAlign = TypeHAlignRight
            .FontBold = True
            .Col = .GetColFromID("Keyed")
            .Text = intDateKeyedTotal
            .FontBold = True
            .Col = .GetColFromID("Exists")
            .Text = intDateExistsTotal
            .FontBold = True
            .Col = .GetColFromID("Auto")
            .Text = intDateAutoTotal
            .FontBold = True
            intDateKeyedTotal = 0
            intDateExistsTotal = 0
            intDateAutoTotal = 0
            .SetCellBorder .GetColFromID("Supplier"), .Row, .GetColFromID("Auto"), .Row, CellBorderIndexTop, _
                    -1, CellBorderStyleSolid
            ' Add the Store totals.
            .MaxRows = .MaxRows + 1
            .Row = .MaxRows
            .Col = .GetColFromID("Supplier")
            .Text = "Store Total"
            .TypeHAlign = TypeHAlignRight
            .FontBold = True
            .Col = .GetColFromID("Keyed")
            .Text = intStoreKeyedTotal
            .FontBold = True
            .Col = .GetColFromID("Exists")
            .Text = intStoreExistsTotal
            .FontBold = True
            .Col = .GetColFromID("Auto")
            .Text = intStoreAutoTotal
            .FontBold = True
            intStoreKeyedTotal = 0
            intStoreExistsTotal = 0
            intStoreAutoTotal = 0
            ' Add the main overall totals.
            .MaxRows = .MaxRows + 1
            .Row = .MaxRows
            .Col = .GetColFromID("Supplier")
            .Text = "Overall Total"
            .TypeHAlign = TypeHAlignRight
            .FontBold = True
            .Col = .GetColFromID("Keyed")
            .Text = intMainKeyedTotal
            .FontBold = True
            .Col = .GetColFromID("Exists")
            .Text = intMainExistsTotal
            .FontBold = True
            .Col = .GetColFromID("Auto")
            .Text = intMainAutoTotal
            .FontBold = True

            ' Lock the entire grid.
            .Col = 1
            .Col2 = .MaxCols
            .Row = 1
            .Row2 = .MaxRows
            .BlockMode = True
            ' Lock cells
            .Lock = True
            ' Protect the cells from being edited
            .Protect = True
            ' Turn block mode off
            .BlockMode = False
        
        End With
    End If
    
    Exit Sub
    
ShowData_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Build the filter used against the recordset based upon the users selected filter options.
Private Function BuildFilter() As String

Const PROCEDURE_NAME As String = "BuildFilters"

Dim strFilter As String

    On Error GoTo BuildFilters_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Build Filters")

'   Build the filter.
    ' Start Date.
    If (Me.dtpStartDate.Value <> "") Then
        strFilter = "DATE1 >= " & Me.dtpStartDate.Value & " AND "
    End If
    ' End Date.
    If (Me.dtpEndDate.Value <> "") Then
        strFilter = strFilter & "DATE1 <= " & Me.dtpEndDate.Value & " AND "
    End If
    ' Store.
    If (Me.cboStore.Text <> "All") Then
        strFilter = strFilter & "STOR = " & Me.cboStore.Text & " AND "
    End If
    ' ID.
    If (Me.cboID.Text <> "All") Then
        strFilter = strFilter & "EEID = " & Me.cboID.Text & " AND "
    End If
    ' Supplier.
    If (Me.cboSupplier.Text <> "All") Then
        strFilter = strFilter & "SUPN = " & Me.cboSupplier.Text & " AND "
    End If
    ' Keyed.
    If (Me.cboKeyed.Text = "Yes") Then
        strFilter = strFilter & "KEYD = 1 AND "
    ElseIf (Me.cboKeyed.Text = "No") Then
        strFilter = strFilter & "KEYD = 0 AND "
    End If
    ' EAN Exists.
    If (Me.cboExists.Text = "Yes") Then
        strFilter = strFilter & "EXIST = 1 AND "
    ElseIf (Me.cboExists.Text = "No") Then
        strFilter = strFilter & "EXIST = 0 AND "
    End If
    ' Auto Matched.
    If (Me.cboAuto.Text = "Yes") Then
        strFilter = strFilter & "AUTO = 1 AND "
    ElseIf (Me.cboAuto.Text = "No") Then
        strFilter = strFilter & "AUTO = 0 AND "
    End If
    
'   Remove the extra " AND " if it exists.
    If (Right(strFilter, 5) = " AND ") Then
        strFilter = Left(strFilter, Len(strFilter) - 5)
    End If
    
    BuildFilter = strFilter
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Filter - " & strFilter)

    Exit Function
    
BuildFilters_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function
