VERSION 5.00
Begin VB.Form frmUserEntry 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Barcode Number Entry"
   ClientHeight    =   2760
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   Icon            =   "frmBarcode.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2760
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtBarcode 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   1163
      TabIndex        =   0
      Top             =   1200
      Width           =   2355
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "F5 - OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   593
      TabIndex        =   1
      Top             =   1920
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "F10 - Exit"
      Height          =   375
      Left            =   2873
      TabIndex        =   2
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   6  'Inside Solid
      BorderWidth     =   2
      DrawMode        =   1  'Blackness
      Height          =   2415
      Left            =   240
      Top             =   173
      Width           =   4215
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      Caption         =   "Please enter the Barcode number"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   533
      TabIndex        =   3
      Top             =   420
      Width           =   3615
   End
End
Attribute VB_Name = "frmUserEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MODULE_NAME  As String = "frmUserEntry"

Private mstrEnteredNumber As String
Private mblnBarcodeEntry  As Boolean

Private Sub cmdExit_Click()

    mstrEnteredNumber = ""
    
    ' Exit without saving.
    Me.Visible = False
    
End Sub

Private Sub cmdOK_Click()

Const PROCEDURE_NAME As String = "cmdOK_Click"

    On Error GoTo cmdSort_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmdOK Click")

    ' Test if the user entered anything and if they did that it was numeric.
    If (Me.txtBarcode.Text = "") Then
        Call MsgBox("You did not enter a Number." & vbCrLf & vbCrLf & _
                        " Please enter a number to continue.", vbExclamation + vbOKOnly, _
                        "Number Not Entered")
        Exit Sub
    ElseIf Not IsNumeric(Me.txtBarcode.Text) Then
        Call MsgBox("The Number you entered was not numeric." & vbCrLf & vbCrLf & _
                        " Please enter a numeric value to continue.", vbExclamation + vbOKOnly, _
                        "Entered Value Not Numeric")
        Exit Sub
    End If
    
    mstrEnteredNumber = Me.txtBarcode.Text
    
    ' Exit.
    Me.Visible = False
    
    Exit Sub
    
cmdSort_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

' Customise the display of the form.
Private Sub Form_Activate()

'   Setup the form for Barcode or SKU entry.
    If (mblnBarcodeEntry = True) Then
        Me.Caption = "Barcode Number Entry"
        lblMessage.Caption = "Please enter the Barcode number"
        Me.txtBarcode.MaxLength = 13
    Else
        Me.Caption = "SKU Number Entry"
        lblMessage.Caption = "Please enter the SKU number"
        Me.txtBarcode.MaxLength = 6
    End If

End Sub

Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
'   Setup the form.
    Me.BackColor = frmBarcodeChecker.BackColor
    Me.lblMessage.BackColor = Me.BackColor
    
'   Disable the forms close button and system menu option.
    Call DisableCloseButton(Me)
    
    Exit Sub
    
FormLoad_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyF5
                KeyCode = 0
                cmdOK_Click     ' Exit form with save of store number.
                
            Case vbKeyF10
                KeyCode = 0
                cmdExit_Click   ' Exit form without saving.
        End Select
    End If

End Sub

' Allow retrieval of the entered number.
Public Property Get EnteredNumber() As String

    EnteredNumber = mstrEnteredNumber
    
End Property

' Store if used to get the Barcode or the SKU from the User.
Public Property Let BarcodeEntry(ByVal blnBarcodeEntry As Boolean)

    mblnBarcodeEntry = blnBarcodeEntry
    
End Property
