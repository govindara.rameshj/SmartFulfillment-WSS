Attribute VB_Name = "modFTPUtil"
Option Explicit

Const MODULE_NAME As String = "modFTPUtil"

Const FTP_ERR_NOT_OPEN As Long = 1
Const FTP_ERR_DIR_EMPTY As Long = 18

Dim mlngINet     As Long
Dim mlngINetConn As Long
Dim mlngHINet    As Long

Private Type FILETIME
        dwLowDateTime  As Long
        dwHighDateTime As Long
End Type

Private Type WIN32_FIND_DATA
        dwFileAttributes As Long
        ftCreationTime   As FILETIME
        ftLastAccessTime As FILETIME
        ftLastWriteTime  As FILETIME
        nFileSizeHigh    As Long
        nFileSizeLow     As Long
        dwReserved0      As Long
        dwReserved1      As Long
        cFileName        As String * 260
        cAlternate       As String * 14
End Type

Private Declare Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" _
    (ByVal sAgent As String, ByVal lAccessType As Long, ByVal sProxyName As String, _
    ByVal sProxyBypass As String, ByVal lFlags As Long) As Long

Private Declare Function InternetConnect Lib "wininet.dll" Alias "InternetConnectA" _
    (ByVal hInternetSession As Long, ByVal sServerName As String, _
    ByVal nServerPort As Integer, ByVal sUsername As String, _
    ByVal sPassword As String, ByVal lService As Long, _
    ByVal lFlags As Long, ByVal lContext As Long) As Long

Private Declare Function FtpGetFile Lib "wininet.dll" Alias "FtpGetFileA" _
    (ByVal hFTPSession As Long, ByVal lpszRemoteFile As String, _
    ByVal lpszNewFile As String, ByVal fFailIfExists As Boolean, _
    ByVal dwFlagsAndAttributes As Long, ByVal dwFlags As Long, _
    ByVal dwContext As Long) As Boolean

Private Declare Function InternetCloseHandle Lib "wininet.dll" (ByVal hInet As Long) As Integer

Private Declare Function FtpFindFirstFile Lib "wininet.dll" Alias "FtpFindFirstFileA" _
    (ByVal hFTPSession As Long, ByVal lpszSearchFile As String, _
    lpFindFileData As WIN32_FIND_DATA, ByVal dwFlags As Long, _
    ByVal dwContent As Long) As Long
    
Private Declare Function InternetFindNextFile Lib "wininet.dll" Alias "InternetFindNextFileA" _
    (ByVal hFind As Long, lpvFindData As WIN32_FIND_DATA) As Long
    
Private Declare Function FtpDeleteFile Lib "wininet.dll" Alias "FtpDeleteFileA" _
    (ByVal hFTPSession As Long, ByVal lpszFileName As String) As Boolean

Private Declare Function FtpPutFile Lib "wininet.dll" Alias "FtpPutFileA" _
    (ByVal hFTPSession As Long, ByVal lpszLocalFile As String, _
    ByVal lpszRemoteFile As String, ByVal dwFlags As Long, _
    ByVal dwContext As Long) As Boolean
    
Private Declare Function FtpRenameFile Lib "wininet.dll" Alias "FtpRenameFileA" _
    (ByVal hFTPSession As Long, ByVal lpszExisting As String, _
    ByVal lpszNew As String) As Boolean

    

Public Function OpenFTPConnection(strAddress As String, strUsername As String, strPassword As String) As Boolean
   
    mlngINet = InternetOpen(strAddress, 0, vbNullString, vbNullString, 0)

    mlngINetConn = InternetConnect(mlngINet, strAddress, 0, strUsername, strPassword, 1, 0, 0)
    
    If mlngINetConn <> 0 Then OpenFTPConnection = True
    DoEvents

End Function

Public Function CloseFTPDirectory() As Boolean

    If mlngHINet > 0 Then Call InternetCloseHandle(mlngHINet)

End Function

Public Function CloseFTPConnection() As Boolean

    Call DebugMsg(MODULE_NAME, "ClosingFTPConn", endlDebug)
    If mlngHINet > 0 Then
        Call InternetCloseHandle(mlngHINet)
        mlngHINet = 0
    End If
    If mlngINetConn > 0 Then
        Call InternetCloseHandle(mlngINetConn)
        mlngINetConn = 0
    End If
    If mlngINet > 0 Then
        Call InternetCloseHandle(mlngINet)
        mlngINet = 0
    End If

End Function

Public Function GetFTPFile(strSourceFile As String, strDestinationFile As String)

Dim blnFileOK As Boolean

    blnFileOK = FtpGetFile(mlngINetConn, strSourceFile, strDestinationFile, 0, 0, 1, 0)
    GetFTPFile = blnFileOK

End Function

Public Function RenameFTPFile(strSourceFile As String, strDestinationFile As String)

Dim blnFileOK As Boolean

    Call DebugMsg(MODULE_NAME, "RenameFTPFile", endlDebug, "Renaming-" & strSourceFile & " to " & strDestinationFile)
    blnFileOK = FtpRenameFile(mlngINetConn, strSourceFile, strDestinationFile)
    RenameFTPFile = blnFileOK

End Function

Public Function PutFTPFile(strSourceFile As String, strDestinationFile As String)

    Call DebugMsg(MODULE_NAME, "PutFTPFile", endlDebug, "Putting-" & strSourceFile & " to " & strDestinationFile)
    PutFTPFile = FtpPutFile(mlngINetConn, strSourceFile, strDestinationFile, 1, 0)
    DoEvents

End Function

Public Function DeleteFTPFile(strFileName As String)

    Call DebugMsg(MODULE_NAME, "DeleteFTPFile", endlDebug, "Opening-" & strFileName)
    DeleteFTPFile = FtpDeleteFile(mlngINetConn, strFileName)

End Function

Public Function OpenFTPDirectory(strFileSpec As String) As String

Dim pData    As WIN32_FIND_DATA
Dim intError As Integer

    Call DebugMsg(MODULE_NAME, "OpenFTPDirectory", endlDebug, "Opening-" & strFileSpec)
    If mlngINetConn = 0 Then Call Err.Raise(FTP_ERR_NOT_OPEN, "Open FTP Directory")

    'init the filename buffer
    pData.cFileName = String(260, 0)
    'get the first file in the directory...
    mlngHINet = FtpFindFirstFile(mlngINetConn, strFileSpec, pData, 0, 0)
    
    'Check error code
    If mlngHINet = 0 Then
        'get the error from the findfirst call
        intError = Err.LastDllError
        Call DebugMsg(MODULE_NAME, "OpenFTPDirectory", endlDebug, "LastDLLError-" & intError)
        
        'is the directory empty?
        If intError <> FTP_ERR_DIR_EMPTY Then
            Call Err.Raise(Err.LastDllError, "Open FTP Directory")
        End If
    Else
        OpenFTPDirectory = Left(pData.cFileName, InStr(1, pData.cFileName, String(1, 0), vbBinaryCompare) - 1)
    End If
    
End Function


Public Function NextFTPDirEntry() As String

Dim blnAnyData As Boolean
Dim intError   As Integer
Dim pData      As WIN32_FIND_DATA

    Call DebugMsg(MODULE_NAME, "NextFTPDirEntry", endlDebug)
    If mlngINetConn = 0 Then Call Err.Raise(FTP_ERR_NOT_OPEN, "Next FTP Directory Entry")

    'init the filename buffer
    pData.cFileName = String(260, 0)
    'get the first file in the directory...
    blnAnyData = InternetFindNextFile(mlngHINet, pData)
    
    'Check error code
    If blnAnyData = False Then
        'get the error from the findfirst call
        intError = Err.LastDllError
        
        'is the directory empty?
        If intError <> FTP_ERR_DIR_EMPTY Then
            Call Err.Raise(Err.LastDllError, "Open FTP Directory")
        End If
    Else
        NextFTPDirEntry = Left(pData.cFileName, InStr(1, pData.cFileName, String(1, 0), vbBinaryCompare) - 1)
    End If
    
End Function



