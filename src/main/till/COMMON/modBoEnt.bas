Attribute VB_Name = "modBo"
'<CAMH>****************************************************************************************
'* Module : modBo
'* Date   : 11/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Common/modBoEnt.bas $
'**********************************************************************************************
'* Summary:
'* We list the business object Ids here in a module that belongs
'* to the OasysRootCom but may be built into other applications
'* in order to use a common set of Business Object class Id numbers.
'* We don't use the enumeration, we simply pass the values around as IDs.
'**********************************************************************************************
'* $Author: Mauricem $Date: 1/23/03 11:50a $ $Revision: 1 $
'* Versions:
'* 11/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modBo"

'* Define all Class IDs for each Business Object used within the system
Public Const CLASSID_SUPPLIER = 1
Public Const CLASSID_PURCHASEORDER = 2
Public Const CLASSID_MENU = 3
Public Const CLASSID_PURCHASEORDERLINE = 4
Public Const CLASSID_CUSTOMER = 5
Public Const CLASSID_POSHEADER = 6
Public Const CLASSID_POSLINE = 7
Public Const CLASSID_POSPAYMENT = 8
Public Const CLASSID_DISCOUNTRATE = 9
Public Const CLASSID_CASHIER = 10
Public Const CLASSID_PAYHOURS = 11
Public Const CLASSID_INVENTORY = 12
Public Const CLASSID_AUDITMASTER = 13
Public Const CLASSID_STOCKMOVE = 14
Public Const CLASSID_DELNOTEHEADER = 15
Public Const CLASSID_DELNOTELINE = 16
Public Const CLASSID_DEPARTMENT = 17
Public Const CLASSID_STORE = 18
Public Const CLASSID_RETURNCUST = 19
Public Const CLASSID_SYSTEMNO = 20
Public Const CLASSID_NIGHTLOG = 21
Public Const CLASSID_NIGHTMASTER = 22
Public Const CLASSID_SOHEADER = 23
Public Const CLASSID_SOLINE = 24
Public Const CLASSID_SOORDERTYPE = 25
Public Const CLASSID_SORANGEDISC = 26
Public Const CLASSID_SORANGE = 27
Public Const CLASSID_SORANGEACC = 28
Public Const CLASSID_RETURNHDR = 29
Public Const CLASSID_RETURNLINE = 30
Public Const CLASSID_CONSHEADER = 31
Public Const CLASSID_TRADER = 32
Public Const CLASSID_DEPOSIT = 33
Public Const CLASSID_ISTOUTHEADER = 34
Public Const CLASSID_ISTLINE = 35
Public Const CLASSID_STOCKWOF = 36
Public Const CLASSID_STOCKCOUNTHEADER = 37
Public Const CLASSID_STOCKCOUNTLINE = 38
Public Const CLASSID_STOCKADJUST = 39
Public Const CLASSID_STOCKADJPEND = 40
Public Const CLASSID_STOCKLOCATION = 41
Public Const CLASSID_STOCKLOCATION2 = 42
Public Const CLASSID_LOCATIONTYPE = 43
Public Const CLASSID_RELATEDITEM = 44
Public Const CLASSID_PRICECHANGE = 45
Public Const CLASSID_EAN = 46
Public Const CLASSID_POSACTION = 47
Public Const CLASSID_POSMESSAGE = 48
Public Const CLASSID_STOCKADJCODE = 49
Public Const CLASSID_UNITOFMEASURE = 50
Public Const CLASSID_PRODUCTGROUP = 51
Public Const CLASSID_ITEMGROUP = 52
Public Const CLASSID_DEPTGROUP = 53
Public Const CLASSID_CASHBALANCEHDR = 54
Public Const CLASSID_SUPPLIERS = 55
Public Const CLASSID_INVENTORYS = 56
Public Const CLASSID_DESPATCHMETHOD = 57
Public Const CLASSID_PURHDRNARRATIVE = 58
Public Const CLASSID_CUSTOMERS = 59
Public Const CLASSID_TYPESOFSALEOPTION = 60
Public Const CLASSID_TRADERS = 61
Public Const CLASSID_LINECOMMENTS = 62
Public Const CLASSID_DEPTANALYSIS = 63
Public Const CLASSID_VATRATES = 64
Public Const CLASSID_SYSTEMDATES = 65
Public Const CLASSID_RETURNCODE = 66
Public Const CLASSID_PRICEOVERRIDE = 67
Public Const CLASSID_STOCKLOG = 68
Public Const CLASSID_ISTINHEADER = 69
Public Const CLASSID_STOCKTAKE = 70
Public Const CLASSID_RETNOTEHEADER = 71
Public Const CLASSID_PARAMETER = 72
Public Const CLASSID_WORKSTATIONCONFIG = 73
Public Const CLASSID_USER = 74
Public Const CLASSID_TILLLOOKUPS = 75
Public Const CLASSID_RETAILEROPTIONS = 76
Public Const CLASSID_SYSTEMOPTIONS = 77
Public Const CLASSID_DELNOTENARRATIVE = 78
Public Const CLASSID_COUNTEDSKU = 79
Public Const CLASSID_RETURNNOTE = 80
Public Const CLASSID_RETURNNOTELINE = 81
Public Const CLASSID_SALESTOTAL = 82
Public Const CLASSID_TILLTOTALS = 83
Public Const CLASSID_CASHIERTOTALS = 84
Public Const CLASSID_ISTINEDIHEADER = 85
Public Const CLASSID_ISTINEDILINE = 86
Public Const CLASSID_PURCHASECONSLINE = 87
Public Const CLASSID_INVENTORYINFO = 88
Public Const CLASSID_TENDERTYPES = 89
Public Const CLASSID_STOCKADJREASON = 90
Public Const CLASSID_ACTIVITYLOG = 91
Public Const CLASSID_STORESTATUS = 92
Public Const CLASSID_EDIFILECONTROL = 93
Public Const CLASSID_SALESLEDGER = 94
Public Const CLASSID_TCFMASTER = 95
Public Const CLASSID_CASHBALANCECONTROL = 96
Public Const CLASSID_CASHBALANCESUMMARY = 97
'---------- Insert new class IDs immediately before here, incrementing the
'---------- number and setting CLASSID_END_OF_STATIC to the same name
Public Const CLASSID_END_OF_STATIC = CLASSID_CASHBALANCESUMMARY


' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_SUPPLIER_SupplierNo = (CLASSID_SUPPLIER * &H10000) + 1
Public Const FID_SUPPLIER_AlphaKey = (CLASSID_SUPPLIER * &H10000) + 2
Public Const FID_SUPPLIER_Active = (CLASSID_SUPPLIER * &H10000) + 3
Public Const FID_SUPPLIER_SubsetSupplier = (CLASSID_SUPPLIER * &H10000) + 4
Public Const FID_SUPPLIER_BackOrdersAllowed = (CLASSID_SUPPLIER * &H10000) + 5
Public Const FID_SUPPLIER_NAME = (CLASSID_SUPPLIER * &H10000) + 6
Public Const FID_SUPPLIER_HOAddressLine1 = (CLASSID_SUPPLIER * &H10000) + 7
Public Const FID_SUPPLIER_HOAddressLine2 = (CLASSID_SUPPLIER * &H10000) + 8
Public Const FID_SUPPLIER_HOAddressLine3 = (CLASSID_SUPPLIER * &H10000) + 9
Public Const FID_SUPPLIER_StoreAddressLine1 = (CLASSID_SUPPLIER * &H10000) + 10
Public Const FID_SUPPLIER_StoreAddressLine2 = (CLASSID_SUPPLIER * &H10000) + 11
Public Const FID_SUPPLIER_StoreAddressLine3 = (CLASSID_SUPPLIER * &H10000) + 12
Public Const FID_SUPPLIER_TelNumber = (CLASSID_SUPPLIER * &H10000) + 13
Public Const FID_SUPPLIER_OtherTelNumber = (CLASSID_SUPPLIER * &H10000) + 14
Public Const FID_SUPPLIER_ContactName = (CLASSID_SUPPLIER * &H10000) + 15
Public Const FID_SUPPLIER_OtherContactName = (CLASSID_SUPPLIER * &H10000) + 16
Public Const FID_SUPPLIER_HeadOfficeBuyerCode = (CLASSID_SUPPLIER * &H10000) + 17
Public Const FID_SUPPLIER_MCPType = (CLASSID_SUPPLIER * &H10000) + 18
Public Const FID_SUPPLIER_MCPValue = (CLASSID_SUPPLIER * &H10000) + 19
Public Const FID_SUPPLIER_MCPQuantity = (CLASSID_SUPPLIER * &H10000) + 20
Public Const FID_SUPPLIER_LeadTimeDays = (CLASSID_SUPPLIER * &H10000) + 21
Public Const FID_SUPPLIER_SOQFrequencyCode = (CLASSID_SUPPLIER * &H10000) + 22
Public Const FID_SUPPLIER_SOQFrequencyCode2 = (CLASSID_SUPPLIER * &H10000) + 23
Public Const FID_SUPPLIER_CalculatedLeadTime = (CLASSID_SUPPLIER * &H10000) + 24
Public Const FID_SUPPLIER_SOQControlNumber = (CLASSID_SUPPLIER * &H10000) + 25
Public Const FID_SUPPLIER_SOQAllowed = (CLASSID_SUPPLIER * &H10000) + 26
Public Const FID_SUPPLIER_DateLastPurchasedFrom = (CLASSID_SUPPLIER * &H10000) + 27
Public Const FID_SUPPLIER_DateLastReceipt = (CLASSID_SUPPLIER * &H10000) + 28
Public Const FID_SUPPLIER_NoOfOSOrders = (CLASSID_SUPPLIER * &H10000) + 29
Public Const FID_SUPPLIER_ValueOfOSOrders = (CLASSID_SUPPLIER * &H10000) + 30
Public Const FID_SUPPLIER_SumOfQtyOrdered = (CLASSID_SUPPLIER * &H10000) + 31
Public Const FID_SUPPLIER_SumOfQtyReceived = (CLASSID_SUPPLIER * &H10000) + 32
Public Const FID_SUPPLIER_SumOfLinePORecvd = (CLASSID_SUPPLIER * &H10000) + 33
Public Const FID_SUPPLIER_SumOfLinePONotRecvd = (CLASSID_SUPPLIER * &H10000) + 34
Public Const FID_SUPPLIER_SumOfLinesOverRecvd = (CLASSID_SUPPLIER * &H10000) + 35
Public Const FID_SUPPLIER_SumOfLinesUnderRecvd = (CLASSID_SUPPLIER * &H10000) + 36
Public Const FID_SUPPLIER_NumberOfPORecvd = (CLASSID_SUPPLIER * &H10000) + 37
Public Const FID_SUPPLIER_ValueOfPORecvd = (CLASSID_SUPPLIER * &H10000) + 38
Public Const FID_SUPPLIER_DaysToRecvdOrders = (CLASSID_SUPPLIER * &H10000) + 39
Public Const FID_SUPPLIER_ShortestNoDays = (CLASSID_SUPPLIER * &H10000) + 40
Public Const FID_SUPPLIER_LongestNoDays = (CLASSID_SUPPLIER * &H10000) + 41
Public Const FID_SUPPLIER_NoDaysToReportOpenBackOrder = (CLASSID_SUPPLIER * &H10000) + 42
Public Const FID_SUPPLIER_DayNoInCycle = (CLASSID_SUPPLIER * &H10000) + 43
Public Const FID_SUPPLIER_SOQNextDue = (CLASSID_SUPPLIER * &H10000) + 44
Public Const FID_SUPPLIER_SOQFrequency = (CLASSID_SUPPLIER * &H10000) + 45
Public Const FID_SUPPLIER_Category = (CLASSID_SUPPLIER * &H10000) + 46
Public Const FID_SUPPLIER_ElectronicOrders = (CLASSID_SUPPLIER * &H10000) + 47
Public Const FID_SUPPLIER_PostCode = (CLASSID_SUPPLIER * &H10000) + 48
Public Const FID_SUPPLIER_FaxNumber = (CLASSID_SUPPLIER * &H10000) + 49
Public Const FID_SUPPLIER_EMail = (CLASSID_SUPPLIER * &H10000) + 50
Public Const FID_SUPPLIER_EDICode = (CLASSID_SUPPLIER * &H10000) + 51
Public Const FID_SUPPLIER_RestockRate = (CLASSID_SUPPLIER * &H10000) + 52
Public Const FID_SUPPLIER_RestockCharge = (CLASSID_SUPPLIER * &H10000) + 53
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SUPPLIER_END_OF_STATIC = FID_SUPPLIER_RestockCharge

Public Const FID_PURCHASEORDER_Key = (CLASSID_PURCHASEORDER * &H10000) + 1
Public Const FID_PURCHASEORDER_OrderNumber = (CLASSID_PURCHASEORDER * &H10000) + 2
Public Const FID_PURCHASEORDER_SupplierNo = (CLASSID_PURCHASEORDER * &H10000) + 3
Public Const FID_PURCHASEORDER_OrderDate = (CLASSID_PURCHASEORDER * &H10000) + 4
Public Const FID_PURCHASEORDER_DueDate = (CLASSID_PURCHASEORDER * &H10000) + 5
Public Const FID_PURCHASEORDER_ReleaseNumber = (CLASSID_PURCHASEORDER * &H10000) + 6
Public Const FID_PURCHASEORDER_RaisedBy = (CLASSID_PURCHASEORDER * &H10000) + 7
Public Const FID_PURCHASEORDER_Source = (CLASSID_PURCHASEORDER * &H10000) + 8
Public Const FID_PURCHASEORDER_HOOrderNumber = (CLASSID_PURCHASEORDER * &H10000) + 9
Public Const FID_PURCHASEORDER_SOQNumber = (CLASSID_PURCHASEORDER * &H10000) + 10
Public Const FID_PURCHASEORDER_NoOfCartons = (CLASSID_PURCHASEORDER * &H10000) + 11
Public Const FID_PURCHASEORDER_QuantityOnOrder = (CLASSID_PURCHASEORDER * &H10000) + 12
Public Const FID_PURCHASEORDER_Value = (CLASSID_PURCHASEORDER * &H10000) + 13
Public Const FID_PURCHASEORDER_PrintFlag = (CLASSID_PURCHASEORDER * &H10000) + 14
Public Const FID_PURCHASEORDER_NoOfRePrints = (CLASSID_PURCHASEORDER * &H10000) + 15
Public Const FID_PURCHASEORDER_ReceiptNumber = (CLASSID_PURCHASEORDER * &H10000) + 16
Public Const FID_PURCHASEORDER_PricingNumber = (CLASSID_PURCHASEORDER * &H10000) + 17
Public Const FID_PURCHASEORDER_ReceivedAll = (CLASSID_PURCHASEORDER * &H10000) + 18
Public Const FID_PURCHASEORDER_ReceivedPart = (CLASSID_PURCHASEORDER * &H10000) + 19
Public Const FID_PURCHASEORDER_DeletedByMaint = (CLASSID_PURCHASEORDER * &H10000) + 20
Public Const FID_PURCHASEORDER_Commed = (CLASSID_PURCHASEORDER * &H10000) + 21
Public Const FID_PURCHASEORDER_CustomerOrderNo = (CLASSID_PURCHASEORDER * &H10000) + 22
Public Const FID_PURCHASEORDER_CommNumber = (CLASSID_PURCHASEORDER * &H10000) + 23
Public Const FID_PURCHASEORDER_Weight = (CLASSID_PURCHASEORDER * &H10000) + 24
Public Const FID_PURCHASEORDER_Status = (CLASSID_PURCHASEORDER * &H10000) + 25
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_PURCHASEORDER_END_OF_STATIC = FID_PURCHASEORDER_Weight


Public Const FID_MENU_ID = (CLASSID_MENU * &H10000) + 1
Public Const FID_MENU_TEXT = (CLASSID_MENU * &H10000) + 2
Public Const FID_MENU_EXECPATH = (CLASSID_MENU * &H10000) + 3
Public Const FID_MENU_ASPPAGE = (CLASSID_MENU * &H10000) + 4
Public Const FID_MENU_PARENTID = (CLASSID_MENU * &H10000) + 5
Public Const FID_MENU_SECURITY = (CLASSID_MENU * &H10000) + 6
Public Const FID_MENU_MENUTYPE = (CLASSID_MENU * &H10000) + 7
Public Const FID_MENU_SHORTCUTKEY = (CLASSID_MENU * &H10000) + 8
Public Const FID_MENU_ICONID = (CLASSID_MENU * &H10000) + 9
Public Const FID_MENU_WAITEXE = (CLASSID_MENU * &H10000) + 10
Public Const FID_MENU_ADDPATH = (CLASSID_MENU * &H10000) + 11
Public Const FID_MENU_GROUPID = (CLASSID_MENU * &H10000) + 12
Public Const FID_MENU_SENDSESSION = (CLASSID_MENU * &H10000) + 13
Public Const FID_MENU_PARAMETERS = (CLASSID_MENU * &H10000) + 14
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_MENU_END_OF_STATIC = FID_MENU_PARAMETERS

Public Const FID_PURCHASEORDERLINE_PurchaseOrderNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 1
Public Const FID_PURCHASEORDERLINE_LineNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 2
Public Const FID_PURCHASEORDERLINE_PartCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 3
Public Const FID_PURCHASEORDERLINE_PartAlphaKey = (CLASSID_PURCHASEORDERLINE * &H10000) + 4
Public Const FID_PURCHASEORDERLINE_ManufacturerCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 5
Public Const FID_PURCHASEORDERLINE_SupplierPartCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 6
Public Const FID_PURCHASEORDERLINE_OrderQuantity = (CLASSID_PURCHASEORDERLINE * &H10000) + 7
Public Const FID_PURCHASEORDERLINE_OrderPrice = (CLASSID_PURCHASEORDERLINE * &H10000) + 8
Public Const FID_PURCHASEORDERLINE_CostPrice = (CLASSID_PURCHASEORDERLINE * &H10000) + 9
Public Const FID_PURCHASEORDERLINE_LastOrderDate = (CLASSID_PURCHASEORDERLINE * &H10000) + 10
Public Const FID_PURCHASEORDERLINE_QuantityReceived = (CLASSID_PURCHASEORDERLINE * &H10000) + 11
Public Const FID_PURCHASEORDERLINE_QuantityBackordered = (CLASSID_PURCHASEORDERLINE * &H10000) + 12
Public Const FID_PURCHASEORDERLINE_Complete = (CLASSID_PURCHASEORDERLINE * &H10000) + 13
Public Const FID_PURCHASEORDERLINE_DelNoteNumber = (CLASSID_PURCHASEORDERLINE * &H10000) + 14
Public Const FID_PURCHASEORDERLINE_DelNoteLineNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 15
Public Const FID_PURCHASEORDERLINE_DateLastReceipt = (CLASSID_PURCHASEORDERLINE * &H10000) + 16
Public Const FID_PURCHASEORDERLINE_Message = (CLASSID_PURCHASEORDERLINE * &H10000) + 17
Public Const FID_PURCHASEORDERLINE_QuantityPerUnit = (CLASSID_PURCHASEORDERLINE * &H10000) + 18
Public Const FID_PURCHASEORDERLINE_CustomerOrderNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 19
Public Const FID_PURCHASEORDERLINE_CustomerOrderLineNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 20
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_PURCHASEORDERLINE_END_OF_STATIC = FID_PURCHASEORDERLINE_CustomerOrderLineNo

Public Const FID_CUSTOMER_CardNumber = (CLASSID_CUSTOMER * &H10000) + 1
Public Const FID_CUSTOMER_Active = (CLASSID_CUSTOMER * &H10000) + 2
Public Const FID_CUSTOMER_CardExpiry = (CLASSID_CUSTOMER * &H10000) + 3
Public Const FID_CUSTOMER_CustomerName = (CLASSID_CUSTOMER * &H10000) + 4
Public Const FID_CUSTOMER_AddressLine1 = (CLASSID_CUSTOMER * &H10000) + 5
Public Const FID_CUSTOMER_AddressLine2 = (CLASSID_CUSTOMER * &H10000) + 6
Public Const FID_CUSTOMER_AddressLine3 = (CLASSID_CUSTOMER * &H10000) + 7
Public Const FID_CUSTOMER_AddressLine4 = (CLASSID_CUSTOMER * &H10000) + 8
Public Const FID_CUSTOMER_PostCode = (CLASSID_CUSTOMER * &H10000) + 9
Public Const FID_CUSTOMER_WorkTelNumber = (CLASSID_CUSTOMER * &H10000) + 10
Public Const FID_CUSTOMER_HomeTelNumber = (CLASSID_CUSTOMER * &H10000) + 11
Public Const FID_CUSTOMER_DeliveryName = (CLASSID_CUSTOMER * &H10000) + 12
Public Const FID_CUSTOMER_DeliveryAddressLine1 = (CLASSID_CUSTOMER * &H10000) + 13
Public Const FID_CUSTOMER_DeliveryAddressLine2 = (CLASSID_CUSTOMER * &H10000) + 14
Public Const FID_CUSTOMER_DeliveryAddressLine3 = (CLASSID_CUSTOMER * &H10000) + 15
Public Const FID_CUSTOMER_DeliveryPostCode = (CLASSID_CUSTOMER * &H10000) + 16
Public Const FID_CUSTOMER_DeliveryWorkTelNumber = (CLASSID_CUSTOMER * &H10000) + 17
Public Const FID_CUSTOMER_DeliveryHomeTelNumber = (CLASSID_CUSTOMER * &H10000) + 18
Public Const FID_CUSTOMER_AccountOpened = (CLASSID_CUSTOMER * &H10000) + 19
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_CUSTOMER_END_OF_STATIC = FID_CUSTOMER_AccountOpened

Public Const FID_POSHEADER_TranDate = (CLASSID_POSHEADER * &H10000) + 1
Public Const FID_POSHEADER_TillID = (CLASSID_POSHEADER * &H10000) + 2
Public Const FID_POSHEADER_TransactionNo = (CLASSID_POSHEADER * &H10000) + 3
Public Const FID_POSHEADER_CashierID = (CLASSID_POSHEADER * &H10000) + 4
Public Const FID_POSHEADER_TransactionTime = (CLASSID_POSHEADER * &H10000) + 5
Public Const FID_POSHEADER_TradersCard = (CLASSID_POSHEADER * &H10000) + 6
Public Const FID_POSHEADER_Comm = (CLASSID_POSHEADER * &H10000) + 7
Public Const FID_POSHEADER_TransactionCode = (CLASSID_POSHEADER * &H10000) + 8
Public Const FID_POSHEADER_OpenDrawerCode = (CLASSID_POSHEADER * &H10000) + 9
Public Const FID_POSHEADER_ReasonCode = (CLASSID_POSHEADER * &H10000) + 10
Public Const FID_POSHEADER_OrderNumber = (CLASSID_POSHEADER * &H10000) + 11
Public Const FID_POSHEADER_Voided = (CLASSID_POSHEADER * &H10000) + 12
Public Const FID_POSHEADER_TrainingMode = (CLASSID_POSHEADER * &H10000) + 13
Public Const FID_POSHEADER_DailyUpdateProc = (CLASSID_POSHEADER * &H10000) + 14
Public Const FID_POSHEADER_PostReversed = (CLASSID_POSHEADER * &H10000) + 15
Public Const FID_POSHEADER_TranParked = (CLASSID_POSHEADER * &H10000) + 16
Public Const FID_POSHEADER_SupervisorNo = (CLASSID_POSHEADER * &H10000) + 17
Public Const FID_POSHEADER_SupervisorUsed = (CLASSID_POSHEADER * &H10000) + 18
Public Const FID_POSHEADER_VoidSupervisor = (CLASSID_POSHEADER * &H10000) + 19
Public Const FID_POSHEADER_DiscountSupervisor = (CLASSID_POSHEADER * &H10000) + 20
Public Const FID_POSHEADER_MerchandiseAmount = (CLASSID_POSHEADER * &H10000) + 21
Public Const FID_POSHEADER_NonMerchandiseAmount = (CLASSID_POSHEADER * &H10000) + 22
Public Const FID_POSHEADER_TaxAmount = (CLASSID_POSHEADER * &H10000) + 23
Public Const FID_POSHEADER_DiscountAmount = (CLASSID_POSHEADER * &H10000) + 24
Public Const FID_POSHEADER_TotalSaleAmount = (CLASSID_POSHEADER * &H10000) + 25
Public Const FID_POSHEADER_VATRates = (CLASSID_POSHEADER * &H10000) + 26
Public Const FID_POSHEADER_VATSymbol = (CLASSID_POSHEADER * &H10000) + 27
Public Const FID_POSHEADER_ExVATValue = (CLASSID_POSHEADER * &H10000) + 28
Public Const FID_POSHEADER_VATValue = (CLASSID_POSHEADER * &H10000) + 29
Public Const FID_POSHEADER_CreditSupervisor = (CLASSID_POSHEADER * &H10000) + 30
Public Const FID_POSHEADER_CashBackItem = (CLASSID_POSHEADER * &H10000) + 31
Public Const FID_POSHEADER_AreaCode = (CLASSID_POSHEADER * &H10000) + 32
Public Const FID_POSHEADER_AccountSale = (CLASSID_POSHEADER * &H10000) + 33
Public Const FID_POSHEADER_AccountNo = (CLASSID_POSHEADER * &H10000) + 34
Public Const FID_POSHEADER_CustomerRef = (CLASSID_POSHEADER * &H10000) + 35
Public Const FID_POSHEADER_CustomerResponseCard = (CLASSID_POSHEADER * &H10000) + 36
Public Const FID_POSHEADER_TransactionIncomplete = (CLASSID_POSHEADER * &H10000) + 37
Public Const FID_POSHEADER_GiroStatementNo = (CLASSID_POSHEADER * &H10000) + 38
Public Const FID_POSHEADER_PayDateDue = (CLASSID_POSHEADER * &H10000) + 39
Public Const FID_POSHEADER_GiroPrintedDate = (CLASSID_POSHEADER * &H10000) + 40
Public Const FID_POSHEADER_GiroPrinted = (CLASSID_POSHEADER * &H10000) + 41
Public Const FID_POSHEADER_SuperQuinnCardNo = (CLASSID_POSHEADER * &H10000) + 42
Public Const FID_POSHEADER_SuperQuinnPoints = (CLASSID_POSHEADER * &H10000) + 43
Public Const FID_POSHEADER_SuperQuinnBonus = (CLASSID_POSHEADER * &H10000) + 44
Public Const FID_POSHEADER_SalesmanCode = (CLASSID_POSHEADER * &H10000) + 45
Public Const FID_POSHEADER_CustomerOrderNo = (CLASSID_POSHEADER * &H10000) + 46
Public Const FID_POSHEADER_RefundReasonCode = (CLASSID_POSHEADER * &H10000) + 47
Public Const FID_POSHEADER_NoOfReceipts = (CLASSID_POSHEADER * &H10000) + 48
Public Const FID_POSHEADER_LoanTile = (CLASSID_POSHEADER * &H10000) + 49
Public Const FID_POSHEADER_RefundOrigTranDate = (CLASSID_POSHEADER * &H10000) + 50
Public Const FID_POSHEADER_RefundOrigTranNo = (CLASSID_POSHEADER * &H10000) + 51
Public Const FID_POSHEADER_DiscountReasonCode = (CLASSID_POSHEADER * &H10000) + 52
Public Const FID_POSHEADER_LineItems = (CLASSID_POSHEADER * &H10000) + 53
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_POSHEADER_END_OF_STATIC = FID_POSHEADER_DiscountReasonCode

Public Const FID_POSLINE_TranDate = (CLASSID_POSLINE * &H10000) + 1
Public Const FID_POSLINE_TillID = (CLASSID_POSLINE * &H10000) + 2
Public Const FID_POSLINE_TransactionNumber = (CLASSID_POSLINE * &H10000) + 3
Public Const FID_POSLINE_SequenceNo = (CLASSID_POSLINE * &H10000) + 4
Public Const FID_POSLINE_PartCode = (CLASSID_POSLINE * &H10000) + 5
Public Const FID_POSLINE_DepartmentNumber = (CLASSID_POSLINE * &H10000) + 6
Public Const FID_POSLINE_BarcodeEntry = (CLASSID_POSLINE * &H10000) + 7
Public Const FID_POSLINE_QuantitySold = (CLASSID_POSLINE * &H10000) + 8
Public Const FID_POSLINE_ActualSellPrice = (CLASSID_POSLINE * &H10000) + 9
Public Const FID_POSLINE_QuantityPerUnit = (CLASSID_POSLINE * &H10000) + 10
Public Const FID_POSLINE_ExtendedValue = (CLASSID_POSLINE * &H10000) + 11
Public Const FID_POSLINE_ExtendedCost = (CLASSID_POSLINE * &H10000) + 12
Public Const FID_POSLINE_LookUpPrice = (CLASSID_POSLINE * &H10000) + 13
Public Const FID_POSLINE_BasePrice = (CLASSID_POSLINE * &H10000) + 14
Public Const FID_POSLINE_PriceBand = (CLASSID_POSLINE * &H10000) + 15
Public Const FID_POSLINE_VATCode = (CLASSID_POSLINE * &H10000) + 16
Public Const FID_POSLINE_RelatedItems = (CLASSID_POSLINE * &H10000) + 17
Public Const FID_POSLINE_CatchAllItem = (CLASSID_POSLINE * &H10000) + 18
Public Const FID_POSLINE_LineReversed = (CLASSID_POSLINE * &H10000) + 19
Public Const FID_POSLINE_PriceOverrideReason = (CLASSID_POSLINE * &H10000) + 20
Public Const FID_POSLINE_SalesRefundReason = (CLASSID_POSLINE * &H10000) + 21
Public Const FID_POSLINE_SupervisorNumber = (CLASSID_POSLINE * &H10000) + 22
Public Const FID_POSLINE_ActualPriceExVAT = (CLASSID_POSLINE * &H10000) + 23
Public Const FID_POSLINE_ActualQuantity = (CLASSID_POSLINE * &H10000) + 24
Public Const FID_POSLINE_DiscountedAmount = (CLASSID_POSLINE * &H10000) + 25
Public Const FID_POSLINE_LookUpDiscountedAmount = (CLASSID_POSLINE * &H10000) + 26
Public Const FID_POSLINE_OverrideMarginCode = (CLASSID_POSLINE * &H10000) + 27
Public Const FID_POSLINE_DISMASMarginCode = (CLASSID_POSLINE * &H10000) + 28
Public Const FID_POSLINE_UnitOfMeasure = (CLASSID_POSLINE * &H10000) + 29
Public Const FID_POSLINE_QuantityOfMeasure = (CLASSID_POSLINE * &H10000) + 30
Public Const FID_POSLINE_ItemStatus = (CLASSID_POSLINE * &H10000) + 31
Public Const FID_POSLINE_DiscountAllowed = (CLASSID_POSLINE * &H10000) + 32
Public Const FID_POSLINE_LineDiscount = (CLASSID_POSLINE * &H10000) + 33
Public Const FID_POSLINE_RoundingOnTender = (CLASSID_POSLINE * &H10000) + 34
Public Const FID_POSLINE_Description = (CLASSID_POSLINE * &H10000) + 35
Public Const FID_POSLINE_CustomerOrderNumber = (CLASSID_POSLINE * &H10000) + 36
Public Const FID_POSLINE_OrderLineNumber = (CLASSID_POSLINE * &H10000) + 37
Public Const FID_POSLINE_OnHand = (CLASSID_POSLINE * &H10000) + 38
Public Const FID_POSLINE_AlreadyRefunded = (CLASSID_POSLINE * &H10000) + 39
Public Const FID_POSLINE_DiscountReasonCode = (CLASSID_POSLINE * &H10000) + 40
Public Const FID_POSLINE_NonReturnable = (CLASSID_POSLINE * &H10000) + 41
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_POSLINE_END_OF_STATIC = FID_POSLINE_NonReturnable

Public Const FID_POSPAYMENT_TransactionDate = (CLASSID_POSPAYMENT * &H10000) + 1
Public Const FID_POSPAYMENT_TillID = (CLASSID_POSPAYMENT * &H10000) + 2
Public Const FID_POSPAYMENT_TransactionNumber = (CLASSID_POSPAYMENT * &H10000) + 3
Public Const FID_POSPAYMENT_SequenceNumber = (CLASSID_POSPAYMENT * &H10000) + 4
Public Const FID_POSPAYMENT_TenderType = (CLASSID_POSPAYMENT * &H10000) + 5
Public Const FID_POSPAYMENT_TenderAmount = (CLASSID_POSPAYMENT * &H10000) + 6
Public Const FID_POSPAYMENT_CreditCardNumber = (CLASSID_POSPAYMENT * &H10000) + 7
Public Const FID_POSPAYMENT_CreditCardStartDate = (CLASSID_POSPAYMENT * &H10000) + 8
Public Const FID_POSPAYMENT_CreditCardExpiryDate = (CLASSID_POSPAYMENT * &H10000) + 9
Public Const FID_POSPAYMENT_IssuerNumber = (CLASSID_POSPAYMENT * &H10000) + 10
Public Const FID_POSPAYMENT_AuthorisationCode = (CLASSID_POSPAYMENT * &H10000) + 11
Public Const FID_POSPAYMENT_CCNumberKeyed = (CLASSID_POSPAYMENT * &H10000) + 12
Public Const FID_POSPAYMENT_ChequeAccountNumber = (CLASSID_POSPAYMENT * &H10000) + 13
Public Const FID_POSPAYMENT_ChequeSortCode = (CLASSID_POSPAYMENT * &H10000) + 14
Public Const FID_POSPAYMENT_ChequeNumber = (CLASSID_POSPAYMENT * &H10000) + 15
Public Const FID_POSPAYMENT_SupervisorCode = (CLASSID_POSPAYMENT * &H10000) + 16
Public Const FID_POSPAYMENT_EFTPOSVoucherNo = (CLASSID_POSPAYMENT * &H10000) + 17
Public Const FID_POSPAYMENT_EFTPOSCommNo = (CLASSID_POSPAYMENT * &H10000) + 18
Public Const FID_POSPAYMENT_CashBalanceProcessed = (CLASSID_POSPAYMENT * &H10000) + 19
Public Const FID_POSPAYMENT_DepartmentNumber = (CLASSID_POSPAYMENT * &H10000) + 20
Public Const FID_POSPAYMENT_AuthorisationType = (CLASSID_POSPAYMENT * &H10000) + 21
Public Const FID_POSPAYMENT_CashBackAmount = (CLASSID_POSPAYMENT * &H10000) + 22
Public Const FID_POSPAYMENT_SpecialTender = (CLASSID_POSPAYMENT * &H10000) + 23
Public Const FID_POSPAYMENT_CurrencyTendered = (CLASSID_POSPAYMENT * &H10000) + 24
Public Const FID_POSPAYMENT_ExchangeRateUsed = (CLASSID_POSPAYMENT * &H10000) + 25
Public Const FID_POSPAYMENT_ForeignAmountTendered = (CLASSID_POSPAYMENT * &H10000) + 26
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_POSPAYMENT_END_OF_STATIC = FID_POSPAYMENT_ForeignAmountTendered

Public Const FID_DISCOUNTRATE_DepartmentCode = (CLASSID_DISCOUNTRATE * &H10000) + 1
Public Const FID_DISCOUNTRATE_GroupCode = (CLASSID_DISCOUNTRATE * &H10000) + 2
Public Const FID_DISCOUNTRATE_PartCode = (CLASSID_DISCOUNTRATE * &H10000) + 3
Public Const FID_DISCOUNTRATE_DiscountRate = (CLASSID_DISCOUNTRATE * &H10000) + 4
Public Const FID_DISCOUNTRATE_StartDate = (CLASSID_DISCOUNTRATE * &H10000) + 5
Public Const FID_DISCOUNTRATE_EndDate = (CLASSID_DISCOUNTRATE * &H10000) + 6
Public Const FID_DISCOUNTRATE_MarginErosion = (CLASSID_DISCOUNTRATE * &H10000) + 7
Public Const FID_DISCOUNTRATE_END_OF_STATIC = FID_DISCOUNTRATE_MarginErosion

Public Const FID_CASHIER_CashierNumber = (CLASSID_CASHIER * &H10000) + 1
Public Const FID_CASHIER_Active = (CLASSID_CASHIER * &H10000) + 2
Public Const FID_CASHIER_Name = (CLASSID_CASHIER * &H10000) + 3
Public Const FID_CASHIER_Position = (CLASSID_CASHIER * &H10000) + 4
Public Const FID_CASHIER_SignOnCode = (CLASSID_CASHIER * &H10000) + 5
Public Const FID_CASHIER_Supervisor = (CLASSID_CASHIER * &H10000) + 6
Public Const FID_CASHIER_DefaultAmount = (CLASSID_CASHIER * &H10000) + 7
Public Const FID_CASHIER_NoOfSKULines = (CLASSID_CASHIER * &H10000) + 8
Public Const FID_CASHIER_NoOfDeptLookUps = (CLASSID_CASHIER * &H10000) + 9
Public Const FID_CASHIER_SKURetailSales = (CLASSID_CASHIER * &H10000) + 10
Public Const FID_CASHIER_DeptRetailSales = (CLASSID_CASHIER * &H10000) + 11
Public Const FID_CASHIER_SKUCostOfSales = (CLASSID_CASHIER * &H10000) + 12
Public Const FID_CASHIER_DeptCOSFactor = (CLASSID_CASHIER * &H10000) + 13
Public Const FID_CASHIER_NoOfPriceViolations = (CLASSID_CASHIER * &H10000) + 14
Public Const FID_CASHIER_MarkUpValueViolations = (CLASSID_CASHIER * &H10000) + 15
Public Const FID_CASHIER_NoOfLineReversals = (CLASSID_CASHIER * &H10000) + 16
Public Const FID_CASHIER_CurrentPeriod = (CLASSID_CASHIER * &H10000) + 17
Public Const FID_CASHIER_LastPeriod = (CLASSID_CASHIER * &H10000) + 18
Public Const FID_CASHIER_PayrollNumber = (CLASSID_CASHIER * &H10000) + 19
Public Const FID_CASHIER_WeeklyBasicHours = (CLASSID_CASHIER * &H10000) + 20
Public Const FID_CASHIER_DailyBasicHours = (CLASSID_CASHIER * &H10000) + 21
Public Const FID_CASHIER_OvertimeAllowed = (CLASSID_CASHIER * &H10000) + 22
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_CASHIER_END_OF_STATIC = FID_CASHIER_OvertimeAllowed

Public Const FID_PAYHOURS_StoreNumber = (CLASSID_PAYHOURS * &H10000) + 1
Public Const FID_PAYHOURS_CashierNo = (CLASSID_PAYHOURS * &H10000) + 2
Public Const FID_PAYHOURS_CashierPayrollNo = (CLASSID_PAYHOURS * &H10000) + 3
Public Const FID_PAYHOURS_DateHoursApplicable = (CLASSID_PAYHOURS * &H10000) + 4
Public Const FID_PAYHOURS_WeekEndingDate = (CLASSID_PAYHOURS * &H10000) + 5
Public Const FID_PAYHOURS_CommNo = (CLASSID_PAYHOURS * &H10000) + 6
Public Const FID_PAYHOURS_HoursType = (CLASSID_PAYHOURS * &H10000) + 7
Public Const FID_PAYHOURS_HoursWorked = (CLASSID_PAYHOURS * &H10000) + 8
Public Const FID_PAYHOURS_OvertimeHours = (CLASSID_PAYHOURS * &H10000) + 9
Public Const FID_PAYHOURS_ClockInTime = (CLASSID_PAYHOURS * &H10000) + 10
Public Const FID_PAYHOURS_ClockOutTime = (CLASSID_PAYHOURS * &H10000) + 11
Public Const FID_PAYHOURS_OvertimeOverriden = (CLASSID_PAYHOURS * &H10000) + 12
Public Const FID_PAYHOURS_HoursAuthorised = (CLASSID_PAYHOURS * &H10000) + 13
Public Const FID_PAYHOURS_AuthorisedID = (CLASSID_PAYHOURS * &H10000) + 14
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_PAYHOURS_END_OF_STATIC = FID_PAYHOURS_AuthorisedID

Public Const FID_INVENTORY_PartCode = (CLASSID_INVENTORY * &H10000) + 1
Public Const FID_INVENTORY_Description = (CLASSID_INVENTORY * &H10000) + 2
Public Const FID_INVENTORY_AlphaCode = (CLASSID_INVENTORY * &H10000) + 3
Public Const FID_INVENTORY_DepartmentCode = (CLASSID_INVENTORY * &H10000) + 4
Public Const FID_INVENTORY_Group = (CLASSID_INVENTORY * &H10000) + 5
Public Const FID_INVENTORY_Location = (CLASSID_INVENTORY * &H10000) + 6
Public Const FID_INVENTORY_Shelf = (CLASSID_INVENTORY * &H10000) + 7
Public Const FID_INVENTORY_VATRate = (CLASSID_INVENTORY * &H10000) + 8
Public Const FID_INVENTORY_BuyInPacksOf = (CLASSID_INVENTORY * &H10000) + 9
Public Const FID_INVENTORY_SellInPacksOf = (CLASSID_INVENTORY * &H10000) + 10
Public Const FID_INVENTORY_SupplierNo = (CLASSID_INVENTORY * &H10000) + 11
Public Const FID_INVENTORY_SupplierPartCode = (CLASSID_INVENTORY * &H10000) + 12
Public Const FID_INVENTORY_ManufProductCode = (CLASSID_INVENTORY * &H10000) + 13
Public Const FID_INVENTORY_QtyBuyIn = (CLASSID_INVENTORY * &H10000) + 14
Public Const FID_INVENTORY_DistrubSupplierNo = (CLASSID_INVENTORY * &H10000) + 15
Public Const FID_INVENTORY_SupplierSubsetNo = (CLASSID_INVENTORY * &H10000) + 16
Public Const FID_INVENTORY_PriceBand = (CLASSID_INVENTORY * &H10000) + 17
Public Const FID_INVENTORY_BaseSellPrice = (CLASSID_INVENTORY * &H10000) + 18
Public Const FID_INVENTORY_NormalSellPrice = (CLASSID_INVENTORY * &H10000) + 19
Public Const FID_INVENTORY_PriorSellPrice = (CLASSID_INVENTORY * &H10000) + 20
Public Const FID_INVENTORY_OtherSellPrice = (CLASSID_INVENTORY * &H10000) + 21
Public Const FID_INVENTORY_PriorOtherPrice = (CLASSID_INVENTORY * &H10000) + 22
Public Const FID_INVENTORY_SpecialPrice = (CLASSID_INVENTORY * &H10000) + 23
Public Const FID_INVENTORY_PriorSpecialPrice = (CLASSID_INVENTORY * &H10000) + 24
Public Const FID_INVENTORY_SellInPacksOfAlt = (CLASSID_INVENTORY * &H10000) + 25
Public Const FID_INVENTORY_QuantityToUOM = (CLASSID_INVENTORY * &H10000) + 26
Public Const FID_INVENTORY_BuyInUnitOccNo = (CLASSID_INVENTORY * &H10000) + 27
Public Const FID_INVENTORY_SellInUnitOccNo = (CLASSID_INVENTORY * &H10000) + 28
Public Const FID_INVENTORY_UserIndicator = (CLASSID_INVENTORY * &H10000) + 29
Public Const FID_INVENTORY_SalesStatsRequired = (CLASSID_INVENTORY * &H10000) + 30
Public Const FID_INVENTORY_CostPrice = (CLASSID_INVENTORY * &H10000) + 31
Public Const FID_INVENTORY_LastSold = (CLASSID_INVENTORY * &H10000) + 32
Public Const FID_INVENTORY_LastReceived = (CLASSID_INVENTORY * &H10000) + 33
Public Const FID_INVENTORY_LastOrdered = (CLASSID_INVENTORY * &H10000) + 34
Public Const FID_INVENTORY_PriceEffectiveDate = (CLASSID_INVENTORY * &H10000) + 35
Public Const FID_INVENTORY_OtherPriceDateEffective = (CLASSID_INVENTORY * &H10000) + 36
Public Const FID_INVENTORY_SpecialPriceDateEffective = (CLASSID_INVENTORY * &H10000) + 37
Public Const FID_INVENTORY_DateCreated = (CLASSID_INVENTORY * &H10000) + 38
Public Const FID_INVENTORY_DateObsolete = (CLASSID_INVENTORY * &H10000) + 39
Public Const FID_INVENTORY_DateDeleted = (CLASSID_INVENTORY * &H10000) + 40
Public Const FID_INVENTORY_QuantityAtHand = (CLASSID_INVENTORY * &H10000) + 41
Public Const FID_INVENTORY_QuantityOnOrder = (CLASSID_INVENTORY * &H10000) + 42
Public Const FID_INVENTORY_QuantityOnReturn = (CLASSID_INVENTORY * &H10000) + 43
Public Const FID_INVENTORY_QuantityOnDisplay = (CLASSID_INVENTORY * &H10000) + 44
Public Const FID_INVENTORY_QuantityCustOrder = (CLASSID_INVENTORY * &H10000) + 45
Public Const FID_INVENTORY_QuantityForBranches = (CLASSID_INVENTORY * &H10000) + 46
Public Const FID_INVENTORY_MinQuantity = (CLASSID_INVENTORY * &H10000) + 47
Public Const FID_INVENTORY_MaxQuantity = (CLASSID_INVENTORY * &H10000) + 48
Public Const FID_INVENTORY_QuantitySoldToday = (CLASSID_INVENTORY * &H10000) + 49
Public Const FID_INVENTORY_Status = (CLASSID_INVENTORY * &H10000) + 50
Public Const FID_INVENTORY_RelatedItemSingle = (CLASSID_INVENTORY * &H10000) + 51
Public Const FID_INVENTORY_RelatedNoItems = (CLASSID_INVENTORY * &H10000) + 52
Public Const FID_INVENTORY_PrePriced = (CLASSID_INVENTORY * &H10000) + 53
Public Const FID_INVENTORY_CatchAll = (CLASSID_INVENTORY * &H10000) + 54
Public Const FID_INVENTORY_ItemObsolete = (CLASSID_INVENTORY * &H10000) + 55
Public Const FID_INVENTORY_ItemDeleted = (CLASSID_INVENTORY * &H10000) + 56
Public Const FID_INVENTORY_DeleteNextTime = (CLASSID_INVENTORY * &H10000) + 57
Public Const FID_INVENTORY_NeedPrintChange = (CLASSID_INVENTORY * &H10000) + 58
Public Const FID_INVENTORY_ItemTagged = (CLASSID_INVENTORY * &H10000) + 59
Public Const FID_INVENTORY_Warranty = (CLASSID_INVENTORY * &H10000) + 60
Public Const FID_INVENTORY_BinJob = (CLASSID_INVENTORY * &H10000) + 61
Public Const FID_INVENTORY_NoAlternateLoc = (CLASSID_INVENTORY * &H10000) + 62
Public Const FID_INVENTORY_EAN = (CLASSID_INVENTORY * &H10000) + 63
Public Const FID_INVENTORY_NoOfPurchaseRecords = (CLASSID_INVENTORY * &H10000) + 64
Public Const FID_INVENTORY_ValueSoldToday = (CLASSID_INVENTORY * &H10000) + 65
Public Const FID_INVENTORY_UnitsSoldToday = (CLASSID_INVENTORY * &H10000) + 66
Public Const FID_INVENTORY_ValueSoldPeriod = (CLASSID_INVENTORY * &H10000) + 67
Public Const FID_INVENTORY_UnitsSoldPeriod = (CLASSID_INVENTORY * &H10000) + 68
Public Const FID_INVENTORY_ValueSoldYear = (CLASSID_INVENTORY * &H10000) + 69
Public Const FID_INVENTORY_UnitsSoldYear = (CLASSID_INVENTORY * &H10000) + 70
Public Const FID_INVENTORY_ValueSoldPriorYear = (CLASSID_INVENTORY * &H10000) + 71
Public Const FID_INVENTORY_UnitsSoldPriorYear = (CLASSID_INVENTORY * &H10000) + 72
Public Const FID_INVENTORY_LabPrintQty = (CLASSID_INVENTORY * &H10000) + 73
Public Const FID_INVENTORY_DaysOutStockPeriod = (CLASSID_INVENTORY * &H10000) + 74
Public Const FID_INVENTORY_DaysOutStockLastPeriod = (CLASSID_INVENTORY * &H10000) + 75
Public Const FID_INVENTORY_DaysOutStockPriorPeriod = (CLASSID_INVENTORY * &H10000) + 76
Public Const FID_INVENTORY_UnitsSoldLastPeriod = (CLASSID_INVENTORY * &H10000) + 77
Public Const FID_INVENTORY_UnitsSoldPriorPeriod = (CLASSID_INVENTORY * &H10000) + 78
Public Const FID_INVENTORY_AverageWeeklySales = (CLASSID_INVENTORY * &H10000) + 79
Public Const FID_INVENTORY_DateFirstStock = (CLASSID_INVENTORY * &H10000) + 80
Public Const FID_INVENTORY_NoWeeksUpdated = (CLASSID_INVENTORY * &H10000) + 81
Public Const FID_INVENTORY_WeeklyUpdate = (CLASSID_INVENTORY * &H10000) + 82
Public Const FID_INVENTORY_ConfidentWeeklyAverage = (CLASSID_INVENTORY * &H10000) + 83
Public Const FID_INVENTORY_UnitsSoldWeek = (CLASSID_INVENTORY * &H10000) + 84
Public Const FID_INVENTORY_CalcDay = (CLASSID_INVENTORY * &H10000) + 85
Public Const FID_INVENTORY_SOQMinLevel = (CLASSID_INVENTORY * &H10000) + 86
Public Const FID_INVENTORY_SOQQtyOnOrder = (CLASSID_INVENTORY * &H10000) + 87
Public Const FID_INVENTORY_SOQProcessed = (CLASSID_INVENTORY * &H10000) + 88
Public Const FID_INVENTORY_PreAuditQty = (CLASSID_INVENTORY * &H10000) + 89
Public Const FID_INVENTORY_PreAuditPrice = (CLASSID_INVENTORY * &H10000) + 90
Public Const FID_INVENTORY_PreAuditCost = (CLASSID_INVENTORY * &H10000) + 91
Public Const FID_INVENTORY_AuditAdjustment = (CLASSID_INVENTORY * &H10000) + 92
Public Const FID_INVENTORY_AuditCount = (CLASSID_INVENTORY * &H10000) + 93
Public Const FID_INVENTORY_AuditValue = (CLASSID_INVENTORY * &H10000) + 94
Public Const FID_INVENTORY_CountedDuringPIC = (CLASSID_INVENTORY * &H10000) + 95
Public Const FID_INVENTORY_POSMaterial = (CLASSID_INVENTORY * &H10000) + 96
Public Const FID_INVENTORY_SavePercentage = (CLASSID_INVENTORY * &H10000) + 97
Public Const FID_INVENTORY_LocalPromoStart = (CLASSID_INVENTORY * &H10000) + 98
Public Const FID_INVENTORY_LocalPromoEnd = (CLASSID_INVENTORY * &H10000) + 99
Public Const FID_INVENTORY_LocalPromoPrice = (CLASSID_INVENTORY * &H10000) + 100
Public Const FID_INVENTORY_LocalPromoLocal = (CLASSID_INVENTORY * &H10000) + 101
Public Const FID_INVENTORY_LastStockOnHandComm = (CLASSID_INVENTORY * &H10000) + 102
Public Const FID_INVENTORY_CustomerOrderNo = (CLASSID_INVENTORY * &H10000) + 103
Public Const FID_INVENTORY_OldPartCode = (CLASSID_INVENTORY * &H10000) + 104
Public Const FID_INVENTORY_POSMaterialPrint = (CLASSID_INVENTORY * &H10000) + 105
Public Const FID_INVENTORY_RetainedCostPerc = (CLASSID_INVENTORY * &H10000) + 106
Public Const FID_INVENTORY_ToppsRecord = (CLASSID_INVENTORY * &H10000) + 107
Public Const FID_INVENTORY_ExcludeFromDisc = (CLASSID_INVENTORY * &H10000) + 108
Public Const FID_INVENTORY_ActualCost = (CLASSID_INVENTORY * &H10000) + 109
Public Const FID_INVENTORY_SizeDescription = (CLASSID_INVENTORY * &H10000) + 110
Public Const FID_INVENTORY_Flag1 = (CLASSID_INVENTORY * &H10000) + 111
Public Const FID_INVENTORY_Flag2 = (CLASSID_INVENTORY * &H10000) + 112
Public Const FID_INVENTORY_Flag3 = (CLASSID_INVENTORY * &H10000) + 113
Public Const FID_INVENTORY_Flag4 = (CLASSID_INVENTORY * &H10000) + 114
Public Const FID_INVENTORY_Flag5 = (CLASSID_INVENTORY * &H10000) + 115
Public Const FID_INVENTORY_CatalogueNo = (CLASSID_INVENTORY * &H10000) + 116
Public Const FID_INVENTORY_StarRating = (CLASSID_INVENTORY * &H10000) + 117
Public Const FID_INVENTORY_DisplayStockCounted = (CLASSID_INVENTORY * &H10000) + 118
Public Const FID_INVENTORY_DisplayLocation = (CLASSID_INVENTORY * &H10000) + 119
Public Const FID_INVENTORY_Warehouse = (CLASSID_INVENTORY * &H10000) + 120
Public Const FID_INVENTORY_LastOrderQuantity = (CLASSID_INVENTORY * &H10000) + 121
Public Const FID_INVENTORY_TileHeightCM = (CLASSID_INVENTORY * &H10000) + 122
Public Const FID_INVENTORY_TileWidthCM = (CLASSID_INVENTORY * &H10000) + 123
Public Const FID_INVENTORY_DecorItem = (CLASSID_INVENTORY * &H10000) + 124
Public Const FID_INVENTORY_ReturnsAccepted = (CLASSID_INVENTORY * &H10000) + 125
Public Const FID_INVENTORY_EuroSellingPrice = (CLASSID_INVENTORY * &H10000) + 126
Public Const FID_INVENTORY_Weight = (CLASSID_INVENTORY * &H10000) + 127
Public Const FID_INVENTORY_END_OF_STATIC = FID_INVENTORY_Weight

Public Const FID_AUDITMASTER_PartCode = (CLASSID_AUDITMASTER * &H10000) + 1
Public Const FID_AUDITMASTER_OriginalOnHandQty = (CLASSID_AUDITMASTER * &H10000) + 2
Public Const FID_AUDITMASTER_EnteredOnHandQty = (CLASSID_AUDITMASTER * &H10000) + 3
Public Const FID_AUDITMASTER_Captured = (CLASSID_AUDITMASTER * &H10000) + 4
Public Const FID_AUDITMASTER_END_OF_STATIC = FID_AUDITMASTER_PartCode

Public Const FID_STOCKMOVE_PartCode = (CLASSID_STOCKMOVE * &H10000) + 1
Public Const FID_STOCKMOVE_StartQty = (CLASSID_STOCKMOVE * &H10000) + 2
Public Const FID_STOCKMOVE_StartPrice = (CLASSID_STOCKMOVE * &H10000) + 3
Public Const FID_STOCKMOVE_SoldQty = (CLASSID_STOCKMOVE * &H10000) + 4
Public Const FID_STOCKMOVE_SoldValueRetail = (CLASSID_STOCKMOVE * &H10000) + 5
Public Const FID_STOCKMOVE_DRLReceiptsQty = (CLASSID_STOCKMOVE * &H10000) + 6
Public Const FID_STOCKMOVE_DRLReceiptsValue = (CLASSID_STOCKMOVE * &H10000) + 7
Public Const FID_STOCKMOVE_AdjustmentsQty = (CLASSID_STOCKMOVE * &H10000) + 8
Public Const FID_STOCKMOVE_AdjustmentsValue = (CLASSID_STOCKMOVE * &H10000) + 9
Public Const FID_STOCKMOVE_PriceViolations = (CLASSID_STOCKMOVE * &H10000) + 10
Public Const FID_STOCKMOVE_ISTNETQuantity = (CLASSID_STOCKMOVE * &H10000) + 11
Public Const FID_STOCKMOVE_ISTNetValue = (CLASSID_STOCKMOVE * &H10000) + 12
Public Const FID_STOCKMOVE_ReturnsQuantity = (CLASSID_STOCKMOVE * &H10000) + 13
Public Const FID_STOCKMOVE_ReturnsValue = (CLASSID_STOCKMOVE * &H10000) + 14
Public Const FID_STOCKMOVE_BulkToSingleQty = (CLASSID_STOCKMOVE * &H10000) + 15
Public Const FID_STOCKMOVE_BulkToSingleValue = (CLASSID_STOCKMOVE * &H10000) + 16
Public Const FID_STOCKMOVE_ISTInQuantity = (CLASSID_STOCKMOVE * &H10000) + 17
Public Const FID_STOCKMOVE_ISTInValue = (CLASSID_STOCKMOVE * &H10000) + 18
Public Const FID_STOCKMOVE_ISTOutQuantity = (CLASSID_STOCKMOVE * &H10000) + 19
Public Const FID_STOCKMOVE_ISTOutValue = (CLASSID_STOCKMOVE * &H10000) + 20
Public Const FID_STOCKMOVE_PosStockAdjQuantity = (CLASSID_STOCKMOVE * &H10000) + 21
Public Const FID_STOCKMOVE_PosStockAdjValue = (CLASSID_STOCKMOVE * &H10000) + 22
Public Const FID_STOCKMOVE_NegStockAdjQuantity = (CLASSID_STOCKMOVE * &H10000) + 23
Public Const FID_STOCKMOVE_NegStockAdjValue = (CLASSID_STOCKMOVE * &H10000) + 24
Public Const FID_STOCKMOVE_WeeklyStartQty = (CLASSID_STOCKMOVE * &H10000) + 25
Public Const FID_STOCKMOVE_WeeklySalesQty = (CLASSID_STOCKMOVE * &H10000) + 26
Public Const FID_STOCKMOVE_WeeklyReceiptQty = (CLASSID_STOCKMOVE * &H10000) + 27
Public Const FID_STOCKMOVE_WeeklyISTInQty = (CLASSID_STOCKMOVE * &H10000) + 28
Public Const FID_STOCKMOVE_WeeklyISTOutQty = (CLASSID_STOCKMOVE * &H10000) + 29
Public Const FID_STOCKMOVE_WeeklyReturnQty = (CLASSID_STOCKMOVE * &H10000) + 30
Public Const FID_STOCKMOVE_WeeklyPosAdjQty = (CLASSID_STOCKMOVE * &H10000) + 31
Public Const FID_STOCKMOVE_WeeklyNegAdjQty = (CLASSID_STOCKMOVE * &H10000) + 32
Public Const FID_STOCKMOVE_WeeklyBulkQty = (CLASSID_STOCKMOVE * &H10000) + 33
Public Const FID_STOCKMOVE_END_OF_STATIC = FID_STOCKMOVE_WeeklyBulkQty

Public Const FID_DELNOTEHEADER_DocNo = (CLASSID_DELNOTEHEADER * &H10000) + 1
Public Const FID_DELNOTEHEADER_DocType = (CLASSID_DELNOTEHEADER * &H10000) + 2
Public Const FID_DELNOTEHEADER_Value = (CLASSID_DELNOTEHEADER * &H10000) + 3
Public Const FID_DELNOTEHEADER_Cost = (CLASSID_DELNOTEHEADER * &H10000) + 4
Public Const FID_DELNOTEHEADER_DelNoteDate = (CLASSID_DELNOTEHEADER * &H10000) + 5
Public Const FID_DELNOTEHEADER_Comm = (CLASSID_DELNOTEHEADER * &H10000) + 6
Public Const FID_DELNOTEHEADER_OrigDocKey = (CLASSID_DELNOTEHEADER * &H10000) + 7
Public Const FID_DELNOTEHEADER_EnteredBy = (CLASSID_DELNOTEHEADER * &H10000) + 8
Public Const FID_DELNOTEHEADER_Comment = (CLASSID_DELNOTEHEADER * &H10000) + 9
Public Const FID_DELNOTEHEADER_SupplierONumber = (CLASSID_DELNOTEHEADER * &H10000) + 10
Public Const FID_DELNOTEHEADER_ConsignmentRef = (CLASSID_DELNOTEHEADER * &H10000) + 11
Public Const FID_DELNOTEHEADER_PONumber = (CLASSID_DELNOTEHEADER * &H10000) + 12
Public Const FID_DELNOTEHEADER_PODate = (CLASSID_DELNOTEHEADER * &H10000) + 13
Public Const FID_DELNOTEHEADER_AssignedPORelease = (CLASSID_DELNOTEHEADER * &H10000) + 14
Public Const FID_DELNOTEHEADER_SOQControlNumber = (CLASSID_DELNOTEHEADER * &H10000) + 15
Public Const FID_DELNOTEHEADER_SupplierDelNote = (CLASSID_DELNOTEHEADER * &H10000) + 16
Public Const FID_DELNOTEHEADER_Source = (CLASSID_DELNOTEHEADER * &H10000) + 17
Public Const FID_DELNOTEHEADER_StoreNo = (CLASSID_DELNOTEHEADER * &H10000) + 18
Public Const FID_DELNOTEHEADER_IBT = (CLASSID_DELNOTEHEADER * &H10000) + 19
Public Const FID_DELNOTEHEADER_Printed = (CLASSID_DELNOTEHEADER * &H10000) + 20
Public Const FID_DELNOTEHEADER_ConsignmentKey = (CLASSID_DELNOTEHEADER * &H10000) + 21
Public Const FID_DELNOTEHEADER_SupplierNumber = (CLASSID_DELNOTEHEADER * &H10000) + 22
Public Const FID_DELNOTEHEADER_ReturnDate = (CLASSID_DELNOTEHEADER * &H10000) + 23
Public Const FID_DELNOTEHEADER_OrigPONumber = (CLASSID_DELNOTEHEADER * &H10000) + 24
Public Const FID_DELNOTEHEADER_SupplierReturnNumber = (CLASSID_DELNOTEHEADER * &H10000) + 25
Public Const FID_DELNOTEHEADER_InvCrnMatched = (CLASSID_DELNOTEHEADER * &H10000) + 26
Public Const FID_DELNOTEHEADER_InvCrnDate = (CLASSID_DELNOTEHEADER * &H10000) + 27
Public Const FID_DELNOTEHEADER_InvCrnNumber = (CLASSID_DELNOTEHEADER * &H10000) + 28
Public Const FID_DELNOTEHEADER_VarianceValue = (CLASSID_DELNOTEHEADER * &H10000) + 29
Public Const FID_DELNOTEHEADER_InvoiceValue = (CLASSID_DELNOTEHEADER * &H10000) + 30
Public Const FID_DELNOTEHEADER_InvoiceVAT = (CLASSID_DELNOTEHEADER * &H10000) + 31
Public Const FID_DELNOTEHEADER_MatchingComment = (CLASSID_DELNOTEHEADER * &H10000) + 32
Public Const FID_DELNOTEHEADER_MatchingDate = (CLASSID_DELNOTEHEADER * &H10000) + 33
Public Const FID_DELNOTEHEADER_ReceiptDate = (CLASSID_DELNOTEHEADER * &H10000) + 34
Public Const FID_DELNOTEHEADER_CarraigeValue = (CLASSID_DELNOTEHEADER * &H10000) + 35
Public Const FID_DELNOTEHEADER_Commit = (CLASSID_DELNOTEHEADER * &H10000) + 36
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_DELNOTEHEADER_END_OF_STATIC = FID_DELNOTEHEADER_Commit

Public Const FID_DELNOTELINE_DRLKey = (CLASSID_DELNOTELINE * &H10000) + 1
Public Const FID_DELNOTELINE_LineNo = (CLASSID_DELNOTELINE * &H10000) + 2
Public Const FID_DELNOTELINE_PartCode = (CLASSID_DELNOTELINE * &H10000) + 3
Public Const FID_DELNOTELINE_PartCodeAlphaCode = (CLASSID_DELNOTELINE * &H10000) + 4
Public Const FID_DELNOTELINE_OrderQty = (CLASSID_DELNOTELINE * &H10000) + 5
Public Const FID_DELNOTELINE_ReceivedQty = (CLASSID_DELNOTELINE * &H10000) + 6
Public Const FID_DELNOTELINE_OrderPrice = (CLASSID_DELNOTELINE * &H10000) + 7
Public Const FID_DELNOTELINE_BlanketDiscDesc = (CLASSID_DELNOTELINE * &H10000) + 8
Public Const FID_DELNOTELINE_BlanketPartCode = (CLASSID_DELNOTELINE * &H10000) + 9
Public Const FID_DELNOTELINE_CurrentPrice = (CLASSID_DELNOTELINE * &H10000) + 10
Public Const FID_DELNOTELINE_CurrentCost = (CLASSID_DELNOTELINE * &H10000) + 11
Public Const FID_DELNOTELINE_IBTQuantity = (CLASSID_DELNOTELINE * &H10000) + 12
Public Const FID_DELNOTELINE_ReturnQty = (CLASSID_DELNOTELINE * &H10000) + 13
Public Const FID_DELNOTELINE_ToFollowQty = (CLASSID_DELNOTELINE * &H10000) + 14
Public Const FID_DELNOTELINE_ReturnReasonCode = (CLASSID_DELNOTELINE * &H10000) + 15
Public Const FID_DELNOTELINE_POLineNumber = (CLASSID_DELNOTELINE * &H10000) + 16
Public Const FID_DELNOTELINE_Checked = (CLASSID_DELNOTELINE * &H10000) + 17
Public Const FID_DELNOTELINE_SinglesSellingUnit = (CLASSID_DELNOTELINE * &H10000) + 18
Public Const FID_DELNOTELINE_Narrative = (CLASSID_DELNOTELINE * &H10000) + 19
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_DELNOTELINE_END_OF_STATIC = FID_DELNOTELINE_SinglesSellingUnit

Public Const FID_DEPARTMENT_DepartmentCode = (CLASSID_DEPARTMENT * &H10000) + 1
Public Const FID_DEPARTMENT_Description = (CLASSID_DEPARTMENT * &H10000) + 2
Public Const FID_DEPARTMENT_Size = (CLASSID_DEPARTMENT * &H10000) + 3
Public Const FID_DEPARTMENT_VatCode = (CLASSID_DEPARTMENT * &H10000) + 4
Public Const FID_DEPARTMENT_CostFactor = (CLASSID_DEPARTMENT * &H10000) + 5
Public Const FID_DEPARTMENT_NoPartLines = (CLASSID_DEPARTMENT * &H10000) + 6
Public Const FID_DEPARTMENT_NoDeptLines = (CLASSID_DEPARTMENT * &H10000) + 7
Public Const FID_DEPARTMENT_PartRetailSales = (CLASSID_DEPARTMENT * &H10000) + 8
Public Const FID_DEPARTMENT_DeptRetailSales = (CLASSID_DEPARTMENT * &H10000) + 9
Public Const FID_DEPARTMENT_PartCostOfSales = (CLASSID_DEPARTMENT * &H10000) + 10
Public Const FID_DEPARTMENT_DeptCostOfSales = (CLASSID_DEPARTMENT * &H10000) + 11
Public Const FID_DEPARTMENT_NoPriceViolations = (CLASSID_DEPARTMENT * &H10000) + 12
Public Const FID_DEPARTMENT_ValueOfViolations = (CLASSID_DEPARTMENT * &H10000) + 13
Public Const FID_DEPARTMENT_WeeklySalesValue = (CLASSID_DEPARTMENT * &H10000) + 14
Public Const FID_DEPARTMENT_WeeklyItemLookups = (CLASSID_DEPARTMENT * &H10000) + 15
Public Const FID_DEPARTMENT_WeeklyOutOfStock = (CLASSID_DEPARTMENT * &H10000) + 16
Public Const FID_DEPARTMENT_AllowZeroPrices = (CLASSID_DEPARTMENT * &H10000) + 17
Public Const FID_DEPARTMENT_END_OF_STATIC = FID_DEPARTMENT_AllowZeroPrices

Public Const FID_STORE_StoreNumber = (CLASSID_STORE * &H10000) + 1
Public Const FID_STORE_AddressLine1 = (CLASSID_STORE * &H10000) + 2
Public Const FID_STORE_AddressLine2 = (CLASSID_STORE * &H10000) + 3
Public Const FID_STORE_AddressLine3 = (CLASSID_STORE * &H10000) + 4
Public Const FID_STORE_AddressLine4 = (CLASSID_STORE * &H10000) + 5
Public Const FID_STORE_PostCode = (CLASSID_STORE * &H10000) + 6
Public Const FID_STORE_PhoneNo = (CLASSID_STORE * &H10000) + 7
Public Const FID_STORE_FaxNo = (CLASSID_STORE * &H10000) + 8
Public Const FID_STORE_SOQDay = (CLASSID_STORE * &H10000) + 9
Public Const FID_STORE_PriceBand = (CLASSID_STORE * &H10000) + 10
Public Const FID_STORE_TOPPS = (CLASSID_STORE * &H10000) + 11
Public Const FID_STORE_Warehouse = (CLASSID_STORE * &H10000) + 12
Public Const FID_STORE_StoreCategory = (CLASSID_STORE * &H10000) + 13
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_STORE_END_OF_STATIC = FID_STORE_Warehouse

Public Const FID_RETURNCUST_StoreNumber = (CLASSID_RETURNCUST * &H10000) + 1
Public Const FID_RETURNCUST_TransactionDate = (CLASSID_RETURNCUST * &H10000) + 2
Public Const FID_RETURNCUST_PCTillID = (CLASSID_RETURNCUST * &H10000) + 3
Public Const FID_RETURNCUST_TransactionNumber = (CLASSID_RETURNCUST * &H10000) + 4
Public Const FID_RETURNCUST_CardNumber = (CLASSID_RETURNCUST * &H10000) + 5
Public Const FID_RETURNCUST_ContactName = (CLASSID_RETURNCUST * &H10000) + 6
Public Const FID_RETURNCUST_ContactCompany = (CLASSID_RETURNCUST * &H10000) + 7
Public Const FID_RETURNCUST_AddressLine1 = (CLASSID_RETURNCUST * &H10000) + 8
Public Const FID_RETURNCUST_AddressLine2 = (CLASSID_RETURNCUST * &H10000) + 9
Public Const FID_RETURNCUST_AddressLine3 = (CLASSID_RETURNCUST * &H10000) + 10
Public Const FID_RETURNCUST_PostCode = (CLASSID_RETURNCUST * &H10000) + 11
Public Const FID_RETURNCUST_PhoneNo = (CLASSID_RETURNCUST * &H10000) + 12
Public Const FID_RETURNCUST_StoreSoldFrom = (CLASSID_RETURNCUST * &H10000) + 13
Public Const FID_RETURNCUST_OriginalTranDate = (CLASSID_RETURNCUST * &H10000) + 14
Public Const FID_RETURNCUST_OriginalTillID = (CLASSID_RETURNCUST * &H10000) + 15
Public Const FID_RETURNCUST_OriginalTranID = (CLASSID_RETURNCUST * &H10000) + 16
Public Const FID_RETURNCUST_RefundReason = (CLASSID_RETURNCUST * &H10000) + 17
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_RETURNCUST_END_OF_STATIC = FID_RETURNCUST_RefundReason

Public Const FID_SYSTEMNO_RecID = (CLASSID_SYSTEMNO * &H10000) + 1
Public Const FID_SYSTEMNO_Value = (CLASSID_SYSTEMNO * &H10000) + 2
Public Const FID_SYSTEMNO_Width = (CLASSID_SYSTEMNO * &H10000) + 3
Public Const FID_SYSTEMNO_ID = (CLASSID_SYSTEMNO * &H10000) + 4 'TBA
Public Const FID_SYSTEMNO_Prefix = (CLASSID_SYSTEMNO * &H10000) + 5 'TBA
Public Const FID_SYSTEMNO_Suffix = (CLASSID_SYSTEMNO * &H10000) + 6 'TBA
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SYSTEMNO_END_OF_STATIC = FID_SYSTEMNO_Width

Public Const FID_NIGHTLOG_LogDate = (CLASSID_NIGHTLOG * &H10000) + 1
Public Const FID_NIGHTLOG_SetNumber = (CLASSID_NIGHTLOG * &H10000) + 2
Public Const FID_NIGHTLOG_TaskNumber = (CLASSID_NIGHTLOG * &H10000) + 3
Public Const FID_NIGHTLOG_Description = (CLASSID_NIGHTLOG * &H10000) + 4
Public Const FID_NIGHTLOG_ProgramPath = (CLASSID_NIGHTLOG * &H10000) + 5
Public Const FID_NIGHTLOG_DateStarted = (CLASSID_NIGHTLOG * &H10000) + 6
Public Const FID_NIGHTLOG_TimeStarted = (CLASSID_NIGHTLOG * &H10000) + 7
Public Const FID_NIGHTLOG_DateEnded = (CLASSID_NIGHTLOG * &H10000) + 8
Public Const FID_NIGHTLOG_TimeEnded = (CLASSID_NIGHTLOG * &H10000) + 9
Public Const FID_NIGHTLOG_DateAborted = (CLASSID_NIGHTLOG * &H10000) + 10
Public Const FID_NIGHTLOG_TimeAborted = (CLASSID_NIGHTLOG * &H10000) + 11
Public Const FID_NIGHTLOG_JobError = (CLASSID_NIGHTLOG * &H10000) + 12
Public Const FID_NIGHTLOG_ErrorMessage = (CLASSID_NIGHTLOG * &H10000) + 13
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_NIGHTLOG_END_OF_STATIC = FID_NIGHTLOG_ErrorMessage

Public Const FID_NIGHTMASTER_SetNumber = (CLASSID_NIGHTMASTER * &H10000) + 1
Public Const FID_NIGHTMASTER_TaskNumber = (CLASSID_NIGHTMASTER * &H10000) + 2
Public Const FID_NIGHTMASTER_Description = (CLASSID_NIGHTMASTER * &H10000) + 3
Public Const FID_NIGHTMASTER_ProgramPath = (CLASSID_NIGHTMASTER * &H10000) + 4
Public Const FID_NIGHTMASTER_RunNightly = (CLASSID_NIGHTMASTER * &H10000) + 5
Public Const FID_NIGHTMASTER_RunInRetry = (CLASSID_NIGHTMASTER * &H10000) + 6
Public Const FID_NIGHTMASTER_RunAtWeekEnd = (CLASSID_NIGHTMASTER * &H10000) + 7
Public Const FID_NIGHTMASTER_RunAtPeriodEnd = (CLASSID_NIGHTMASTER * &H10000) + 8
Public Const FID_NIGHTMASTER_RunBeforeStoreLive = (CLASSID_NIGHTMASTER * &H10000) + 9
Public Const FID_NIGHTMASTER_OptionNumber = (CLASSID_NIGHTMASTER * &H10000) + 10
Public Const FID_NIGHTMASTER_AbortOnError = (CLASSID_NIGHTMASTER * &H10000) + 11
Public Const FID_NIGHTMASTER_ErrorHandler = (CLASSID_NIGHTMASTER * &H10000) + 12
Public Const FID_NIGHTMASTER_RunOnDayWeek = (CLASSID_NIGHTMASTER * &H10000) + 13
Public Const FID_NIGHTMASTER_StartDate = (CLASSID_NIGHTMASTER * &H10000) + 14
Public Const FID_NIGHTMASTER_EndDate = (CLASSID_NIGHTMASTER * &H10000) + 15
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_NIGHTMASTER_END_OF_STATIC = FID_NIGHTMASTER_EndDate

Public Const FID_SOHEADER_OrderNo = (CLASSID_SOHEADER * &H10000) + 1
Public Const FID_SOHEADER_CustomerNo = (CLASSID_SOHEADER * &H10000) + 2
Public Const FID_SOHEADER_Salesman = (CLASSID_SOHEADER * &H10000) + 3
Public Const FID_SOHEADER_TillID = (CLASSID_SOHEADER * &H10000) + 4
Public Const FID_SOHEADER_CustomerPresent = (CLASSID_SOHEADER * &H10000) + 5
Public Const FID_SOHEADER_Name = (CLASSID_SOHEADER * &H10000) + 6
Public Const FID_SOHEADER_AddressLine1 = (CLASSID_SOHEADER * &H10000) + 7
Public Const FID_SOHEADER_AddressLine2 = (CLASSID_SOHEADER * &H10000) + 8
Public Const FID_SOHEADER_AddressLine3 = (CLASSID_SOHEADER * &H10000) + 9
Public Const FID_SOHEADER_PostCode = (CLASSID_SOHEADER * &H10000) + 10
Public Const FID_SOHEADER_TelephoneNo = (CLASSID_SOHEADER * &H10000) + 11
Public Const FID_SOHEADER_SecondTelNo = (CLASSID_SOHEADER * &H10000) + 12
Public Const FID_SOHEADER_DepositNumber = (CLASSID_SOHEADER * &H10000) + 13
Public Const FID_SOHEADER_DepositValue = (CLASSID_SOHEADER * &H10000) + 14
Public Const FID_SOHEADER_OrderProcessed = (CLASSID_SOHEADER * &H10000) + 15
Public Const FID_SOHEADER_Printed = (CLASSID_SOHEADER * &H10000) + 16
Public Const FID_SOHEADER_Status = (CLASSID_SOHEADER * &H10000) + 17
Public Const FID_SOHEADER_QuoteDate = (CLASSID_SOHEADER * &H10000) + 18
Public Const FID_SOHEADER_QuoteExpiry = (CLASSID_SOHEADER * &H10000) + 19
Public Const FID_SOHEADER_OrderDate = (CLASSID_SOHEADER * &H10000) + 20
Public Const FID_SOHEADER_ETADate = (CLASSID_SOHEADER * &H10000) + 21
Public Const FID_SOHEADER_NoDeliveries = (CLASSID_SOHEADER * &H10000) + 22
Public Const FID_SOHEADER_Complete = (CLASSID_SOHEADER * &H10000) + 23
Public Const FID_SOHEADER_LastDespatchDate = (CLASSID_SOHEADER * &H10000) + 24
Public Const FID_SOHEADER_Cancelled = (CLASSID_SOHEADER * &H10000) + 25
Public Const FID_SOHEADER_CancelledDate = (CLASSID_SOHEADER * &H10000) + 26
Public Const FID_SOHEADER_CancelledCashierID = (CLASSID_SOHEADER * &H10000) + 27
Public Const FID_SOHEADER_OrderValue = (CLASSID_SOHEADER * &H10000) + 28
Public Const FID_SOHEADER_OutStandingValue = (CLASSID_SOHEADER * &H10000) + 29
Public Const FID_SOHEADER_Remarks = (CLASSID_SOHEADER * &H10000) + 30
Public Const FID_SOHEADER_NameAlphaKey = (CLASSID_SOHEADER * &H10000) + 31
Public Const FID_SOHEADER_DiscountReasonCode = (CLASSID_SOHEADER * &H10000) + 32
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SOHEADER_END_OF_STATIC = FID_SOHEADER_DiscountReasonCode

Public Const FID_SOLINE_OrderNo = (CLASSID_SOLINE * &H10000) + 1
Public Const FID_SOLINE_LineNo = (CLASSID_SOLINE * &H10000) + 2
Public Const FID_SOLINE_PartCode = (CLASSID_SOLINE * &H10000) + 3
Public Const FID_SOLINE_Description = (CLASSID_SOLINE * &H10000) + 4
Public Const FID_SOLINE_SupplierNo = (CLASSID_SOLINE * &H10000) + 5
Public Const FID_SOLINE_OrderQuantity = (CLASSID_SOLINE * &H10000) + 6
Public Const FID_SOLINE_LookUpPrice = (CLASSID_SOLINE * &H10000) + 7
Public Const FID_SOLINE_OrderPrice = (CLASSID_SOLINE * &H10000) + 8
Public Const FID_SOLINE_ExtendedValue = (CLASSID_SOLINE * &H10000) + 9
Public Const FID_SOLINE_ExtendedCost = (CLASSID_SOLINE * &H10000) + 10
Public Const FID_SOLINE_LineDiscount = (CLASSID_SOLINE * &H10000) + 11
Public Const FID_SOLINE_QtyPerUnit = (CLASSID_SOLINE * &H10000) + 12
Public Const FID_SOLINE_RaiseOrder = (CLASSID_SOLINE * &H10000) + 13
Public Const FID_SOLINE_PurchaseOrderNo = (CLASSID_SOLINE * &H10000) + 14
Public Const FID_SOLINE_LastDelNoteNo = (CLASSID_SOLINE * &H10000) + 15
Public Const FID_SOLINE_QuantitySold = (CLASSID_SOLINE * &H10000) + 16
Public Const FID_SOLINE_Cancelled = (CLASSID_SOLINE * &H10000) + 17
Public Const FID_SOLINE_Complete = (CLASSID_SOLINE * &H10000) + 18
Public Const FID_SOLINE_CancellationReason = (CLASSID_SOLINE * &H10000) + 19
Public Const FID_SOLINE_DateCreated = (CLASSID_SOLINE * &H10000) + 20
Public Const FID_SOLINE_DiscountReasonCode = (CLASSID_SOLINE * &H10000) + 21
Public Const FID_SOLINE_RefundAllowed = (CLASSID_SOLINE * &H10000) + 22
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SOLINE_END_OF_STATIC = FID_SOLINE_RefundAllowed

Public Const FID_SOORDERTYPE_OrderType = (CLASSID_SOORDERTYPE * &H10000) + 1
Public Const FID_SOORDERTYPE_Description = (CLASSID_SOORDERTYPE * &H10000) + 2
Public Const FID_SOORDERTYPE_Deleted = (CLASSID_SOORDERTYPE * &H10000) + 3
Public Const FID_SOORDERTYPE_CreatePurchaseOrder = (CLASSID_SOORDERTYPE * &H10000) + 4
Public Const FID_SOORDERTYPE_QuoteValidFor = (CLASSID_SOORDERTYPE * &H10000) + 5
Public Const FID_SOORDERTYPE_DayOfWeek = (CLASSID_SOORDERTYPE * &H10000) + 6
Public Const FID_SOORDERTYPE_NoDaysWithoutInstallation = (CLASSID_SOORDERTYPE * &H10000) + 7
Public Const FID_SOORDERTYPE_ExtraDaysInstallation = (CLASSID_SOORDERTYPE * &H10000) + 8
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SOORDERTYPE_END_OF_STATIC = FID_SOORDERTYPE_ExtraDaysInstallation


Public Const FID_SORANGEDISC_Percentage = (CLASSID_SORANGEDISC * &H10000) + 1
Public Const FID_SORANGEDISC_StartDate = (CLASSID_SORANGEDISC * &H10000) + 2
Public Const FID_SORANGEDISC_EndDate = (CLASSID_SORANGEDISC * &H10000) + 3
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SORANGEDISC_END_OF_STATIC = FID_SORANGEDISC_EndDate


Public Const FID_SORANGE_OrderType = (CLASSID_SORANGE * &H10000) + 1
Public Const FID_SORANGE_DepartmentNo = (CLASSID_SORANGE * &H10000) + 2
Public Const FID_SORANGE_GroupNo = (CLASSID_SORANGE * &H10000) + 3
Public Const FID_SORANGE_Deleted = (CLASSID_SORANGE * &H10000) + 4
Public Const FID_SORANGE_AccessoriesAllowed = (CLASSID_SORANGE * &H10000) + 5
Public Const FID_SORANGE_InstallationPartCode = (CLASSID_SORANGE * &H10000) + 6
Public Const FID_SORANGE_Discount = (CLASSID_SORANGE * &H10000) + 7
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SORANGE_END_OF_STATIC = FID_SORANGE_Discount

Public Const FID_SORANGEACC_RangeOrderType = (CLASSID_SORANGEACC * &H10000) + 1
Public Const FID_SORANGEACC_RangeDepartmentNo = (CLASSID_SORANGEACC * &H10000) + 2
Public Const FID_SORANGEACC_RangeGroupNo = (CLASSID_SORANGEACC * &H10000) + 3
Public Const FID_SORANGEACC_AccessoryDepartment = (CLASSID_SORANGEACC * &H10000) + 4
Public Const FID_SORANGEACC_AccessoryGroup = (CLASSID_SORANGEACC * &H10000) + 5
Public Const FID_SORANGEACC_Deleted = (CLASSID_SORANGEACC * &H10000) + 6
Public Const FID_SORANGEACC_Discount = (CLASSID_SORANGEACC * &H10000) + 7
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SORANGEACC_END_OF_STATIC = FID_SORANGEACC_Discount


Public Const FID_RETURNHDR_ReturnID = (CLASSID_RETURNHDR * &H10000) + 1
Public Const FID_RETURNHDR_SupplierReturnNo = (CLASSID_RETURNHDR * &H10000) + 2
Public Const FID_RETURNHDR_SupplierNo = (CLASSID_RETURNHDR * &H10000) + 3
Public Const FID_RETURNHDR_DateCreated = (CLASSID_RETURNHDR * &H10000) + 4
Public Const FID_RETURNHDR_CollectionDate = (CLASSID_RETURNHDR * &H10000) + 5
Public Const FID_RETURNHDR_PurchaseOrderNo = (CLASSID_RETURNHDR * &H10000) + 6
Public Const FID_RETURNHDR_DRLNo = (CLASSID_RETURNHDR * &H10000) + 7
Public Const FID_RETURNHDR_ReturnValue = (CLASSID_RETURNHDR * &H10000) + 8
Public Const FID_RETURNHDR_Printed = (CLASSID_RETURNHDR * &H10000) + 9
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_RETURNHDR_END_OF_STATIC = FID_RETURNHDR_Printed

Public Const FID_RETURNLINE_ReturnID = (CLASSID_RETURNLINE * &H10000) + 1
Public Const FID_RETURNLINE_LineNo = (CLASSID_RETURNLINE * &H10000) + 2
Public Const FID_RETURNLINE_PartCode = (CLASSID_RETURNLINE * &H10000) + 3
Public Const FID_RETURNLINE_PartAlphaKey = (CLASSID_RETURNLINE * &H10000) + 4
Public Const FID_RETURNLINE_QuantityReturned = (CLASSID_RETURNLINE * &H10000) + 5
Public Const FID_RETURNLINE_Price = (CLASSID_RETURNLINE * &H10000) + 6
Public Const FID_RETURNLINE_Cost = (CLASSID_RETURNLINE * &H10000) + 7
Public Const FID_RETURNLINE_ReturnCode = (CLASSID_RETURNLINE * &H10000) + 8
Public Const FID_RETURNLINE_ReturnReason = (CLASSID_RETURNLINE * &H10000) + 9
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_RETURNLINE_END_OF_STATIC = FID_RETURNLINE_ReturnReason


Public Const FID_CONSHEADER_ConsignmentNo = (CLASSID_CONSHEADER * &H10000) + 1
Public Const FID_CONSHEADER_PurchaseOrderNo = (CLASSID_CONSHEADER * &H10000) + 2
Public Const FID_CONSHEADER_PurchaseOrderRelNo = (CLASSID_CONSHEADER * &H10000) + 3
Public Const FID_CONSHEADER_RecordedInSystem = (CLASSID_CONSHEADER * &H10000) + 4
Public Const FID_CONSHEADER_ConsignmentDate = (CLASSID_CONSHEADER * &H10000) + 5
Public Const FID_CONSHEADER_SupplierNo = (CLASSID_CONSHEADER * &H10000) + 6
Public Const FID_CONSHEADER_IBTConsignment = (CLASSID_CONSHEADER * &H10000) + 7
Public Const FID_CONSHEADER_IBTStoreNo = (CLASSID_CONSHEADER * &H10000) + 8
Public Const FID_CONSHEADER_DeliveryNoteNo = (CLASSID_CONSHEADER * &H10000) + 9
Public Const FID_CONSHEADER_PrintInitials1 = (CLASSID_CONSHEADER * &H10000) + 10
Public Const FID_CONSHEADER_PrintTime1 = (CLASSID_CONSHEADER * &H10000) + 11
Public Const FID_CONSHEADER_PrintInitials2 = (CLASSID_CONSHEADER * &H10000) + 12
Public Const FID_CONSHEADER_PrintTime2 = (CLASSID_CONSHEADER * &H10000) + 13
Public Const FID_CONSHEADER_PrintInitials3 = (CLASSID_CONSHEADER * &H10000) + 14
Public Const FID_CONSHEADER_PrintTime3 = (CLASSID_CONSHEADER * &H10000) + 15
Public Const FID_CONSHEADER_PrintInitials4 = (CLASSID_CONSHEADER * &H10000) + 16
Public Const FID_CONSHEADER_PrintTime4 = (CLASSID_CONSHEADER * &H10000) + 17
Public Const FID_CONSHEADER_PrintInitials5 = (CLASSID_CONSHEADER * &H10000) + 18
Public Const FID_CONSHEADER_PrintTime5 = (CLASSID_CONSHEADER * &H10000) + 19
Public Const FID_CONSHEADER_PrintInitials6 = (CLASSID_CONSHEADER * &H10000) + 20
Public Const FID_CONSHEADER_PrintTime6 = (CLASSID_CONSHEADER * &H10000) + 21
Public Const FID_CONSHEADER_PrintedCount = (CLASSID_CONSHEADER * &H10000) + 22
Public Const FID_CONSHEADER_END_OF_STATIC = FID_CONSHEADER_PrintedCount

Public Const FID_TRADER_TraderCardNo = (CLASSID_TRADER * &H10000) + 1
Public Const FID_TRADER_TraderName = (CLASSID_TRADER * &H10000) + 2
Public Const FID_TRADER_AddressLine1 = (CLASSID_TRADER * &H10000) + 3
Public Const FID_TRADER_AddressLine2 = (CLASSID_TRADER * &H10000) + 4
Public Const FID_TRADER_AddressLine3 = (CLASSID_TRADER * &H10000) + 5
Public Const FID_TRADER_PostCode = (CLASSID_TRADER * &H10000) + 6
Public Const FID_TRADER_TelephoneNo = (CLASSID_TRADER * &H10000) + 7
Public Const FID_TRADER_FaxNo = (CLASSID_TRADER * &H10000) + 8
Public Const FID_TRADER_DateOpened = (CLASSID_TRADER * &H10000) + 9
Public Const FID_TRADER_TradeDiscount = (CLASSID_TRADER * &H10000) + 10
Public Const FID_TRADER_TurnoverToDate = (CLASSID_TRADER * &H10000) + 11
Public Const FID_TRADER_LastTransactionDate = (CLASSID_TRADER * &H10000) + 12
Public Const FID_TRADER_Deleted = (CLASSID_TRADER * &H10000) + 13
Public Const FID_TRADER_TraderType = (CLASSID_TRADER * &H10000) + 14
Public Const FID_TRADER_AccountType = (CLASSID_TRADER * &H10000) + 15
Public Const FID_TRADER_CreditLimit = (CLASSID_TRADER * &H10000) + 16
Public Const FID_TRADER_AccountBalance = (CLASSID_TRADER * &H10000) + 17
Public Const FID_TRADER_OnHold = (CLASSID_TRADER * &H10000) + 18
Public Const FID_TRADER_LastPaymentDate = (CLASSID_TRADER * &H10000) + 19
Public Const FID_TRADER_OriginatingStoreNo = (CLASSID_TRADER * &H10000) + 20
Public Const FID_TRADER_END_OF_STATIC = FID_TRADER_OriginatingStoreNo

Public Const FID_DEPOSIT_DepositNo = (CLASSID_DEPOSIT * &H10000) + 1
Public Const FID_DEPOSIT_CustomerName = (CLASSID_DEPOSIT * &H10000) + 2
Public Const FID_DEPOSIT_DateCreated = (CLASSID_DEPOSIT * &H10000) + 3
Public Const FID_DEPOSIT_DepositAmount = (CLASSID_DEPOSIT * &H10000) + 4
Public Const FID_DEPOSIT_RefundAllowed = (CLASSID_DEPOSIT * &H10000) + 5
Public Const FID_DEPOSIT_DepositCode = (CLASSID_DEPOSIT * &H10000) + 6
Public Const FID_DEPOSIT_Refunded = (CLASSID_DEPOSIT * &H10000) + 7
Public Const FID_DEPOSIT_TillID = (CLASSID_DEPOSIT * &H10000) + 8
Public Const FID_DEPOSIT_TillTransactionNo = (CLASSID_DEPOSIT * &H10000) + 9
Public Const FID_DEPOSIT_CashierNo = (CLASSID_DEPOSIT * &H10000) + 10
Public Const FID_DEPOSIT_OrderNo = (CLASSID_DEPOSIT * &H10000) + 11
Public Const FID_DEPOSIT_DepositUsedAmount = (CLASSID_DEPOSIT * &H10000) + 12
Public Const FID_DEPOSIT_TransactionMoveType = (CLASSID_DEPOSIT * &H10000) + 13
Public Const FID_DEPOSIT_TransactionMoveCode = (CLASSID_DEPOSIT * &H10000) + 14
Public Const FID_DEPOSIT_TraderNo = (CLASSID_DEPOSIT * &H10000) + 15
Public Const FID_DEPOSIT_CommNo = (CLASSID_DEPOSIT * &H10000) + 16
Public Const FID_DEPOSIT_END_OF_STATIC = FID_DEPOSIT_CommNo

Public Const FID_ISTOUTHEADER_DocNo = (CLASSID_ISTOUTHEADER * &H10000) + 1
Public Const FID_ISTOUTHEADER_DocType = (CLASSID_ISTOUTHEADER * &H10000) + 2
Public Const FID_ISTOUTHEADER_Value = (CLASSID_ISTOUTHEADER * &H10000) + 3
Public Const FID_ISTOUTHEADER_Cost = (CLASSID_ISTOUTHEADER * &H10000) + 4
Public Const FID_ISTOUTHEADER_DelNoteDate = (CLASSID_ISTOUTHEADER * &H10000) + 5
Public Const FID_ISTOUTHEADER_Comm = (CLASSID_ISTOUTHEADER * &H10000) + 6
Public Const FID_ISTOUTHEADER_OrigDocKey = (CLASSID_ISTOUTHEADER * &H10000) + 7
Public Const FID_ISTOUTHEADER_EnteredBy = (CLASSID_ISTOUTHEADER * &H10000) + 8
Public Const FID_ISTOUTHEADER_Comment = (CLASSID_ISTOUTHEADER * &H10000) + 9
Public Const FID_ISTOUTHEADER_ConsignmentRef = (CLASSID_ISTOUTHEADER * &H10000) + 10
Public Const FID_ISTOUTHEADER_Source = (CLASSID_ISTOUTHEADER * &H10000) + 11
Public Const FID_ISTOUTHEADER_StoreNumber = (CLASSID_ISTOUTHEADER * &H10000) + 12
Public Const FID_ISTOUTHEADER_IBT = (CLASSID_ISTOUTHEADER * &H10000) + 13
Public Const FID_ISTOUTHEADER_Printed = (CLASSID_ISTOUTHEADER * &H10000) + 14
Public Const FID_ISTOUTHEADER_ConsignmentKey = (CLASSID_ISTOUTHEADER * &H10000) + 15
Public Const FID_ISTOUTHEADER_DespatchMethod = (CLASSID_ISTOUTHEADER * &H10000) + 16
Public Const FID_ISTOUTHEADER_InvCrnMatched = (CLASSID_ISTOUTHEADER * &H10000) + 17
Public Const FID_ISTOUTHEADER_InvCrnDate = (CLASSID_ISTOUTHEADER * &H10000) + 18
Public Const FID_ISTOUTHEADER_InvCrnNumber = (CLASSID_ISTOUTHEADER * &H10000) + 19
Public Const FID_ISTOUTHEADER_VarianceValue = (CLASSID_ISTOUTHEADER * &H10000) + 20
Public Const FID_ISTOUTHEADER_InvoiceValue = (CLASSID_ISTOUTHEADER * &H10000) + 21
Public Const FID_ISTOUTHEADER_InvoiceVAT = (CLASSID_ISTOUTHEADER * &H10000) + 22
Public Const FID_ISTOUTHEADER_MatchingComment = (CLASSID_ISTOUTHEADER * &H10000) + 23
Public Const FID_ISTOUTHEADER_MatchingDate = (CLASSID_ISTOUTHEADER * &H10000) + 24
Public Const FID_ISTOUTHEADER_ReceiptDate = (CLASSID_ISTOUTHEADER * &H10000) + 25
Public Const FID_ISTOUTHEADER_CarraigeValue = (CLASSID_ISTOUTHEADER * &H10000) + 26
Public Const FID_ISTOUTHEADER_Commit = (CLASSID_ISTOUTHEADER * &H10000) + 27
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_ISTOUTHEADER_END_OF_STATIC = FID_ISTOUTHEADER_Commit

Public Const FID_ISTLINE_DRLKey = (CLASSID_ISTLINE * &H10000) + 1
Public Const FID_ISTLINE_LineNo = (CLASSID_ISTLINE * &H10000) + 2
Public Const FID_ISTLINE_PartCode = (CLASSID_ISTLINE * &H10000) + 3
Public Const FID_ISTLINE_PartCodeAlphaCode = (CLASSID_ISTLINE * &H10000) + 4
Public Const FID_ISTLINE_OrderQty = (CLASSID_ISTLINE * &H10000) + 5
Public Const FID_ISTLINE_ReceivedQty = (CLASSID_ISTLINE * &H10000) + 6
Public Const FID_ISTLINE_OrderPrice = (CLASSID_ISTLINE * &H10000) + 7
Public Const FID_ISTLINE_BlanketDiscDesc = (CLASSID_ISTLINE * &H10000) + 8
Public Const FID_ISTLINE_BlanketPartCode = (CLASSID_ISTLINE * &H10000) + 9
Public Const FID_ISTLINE_CurrentPrice = (CLASSID_ISTLINE * &H10000) + 10
Public Const FID_ISTLINE_CurrentCost = (CLASSID_ISTLINE * &H10000) + 11
Public Const FID_ISTLINE_ISTQuantity = (CLASSID_ISTLINE * &H10000) + 12
Public Const FID_ISTLINE_ReturnQty = (CLASSID_ISTLINE * &H10000) + 13
Public Const FID_ISTLINE_ToFollowQty = (CLASSID_ISTLINE * &H10000) + 14
Public Const FID_ISTLINE_ReturnReasonCode = (CLASSID_ISTLINE * &H10000) + 15
Public Const FID_ISTLINE_POLineNumber = (CLASSID_ISTLINE * &H10000) + 16
Public Const FID_ISTLINE_Checked = (CLASSID_ISTLINE * &H10000) + 17
Public Const FID_ISTLINE_SinglesSellingUnit = (CLASSID_ISTLINE * &H10000) + 18
Public Const FID_ISTLINE_Narrative = (CLASSID_ISTLINE * &H10000) + 19
Public Const FID_ISTLINE_END_OF_STATIC = FID_ISTLINE_SinglesSellingUnit


Public Const FID_STOCKWOF_BatchNo = (CLASSID_STOCKWOF * &H10000) + 1
Public Const FID_STOCKWOF_LineNo = (CLASSID_STOCKWOF * &H10000) + 2
Public Const FID_STOCKWOF_AdjustmentCode = (CLASSID_STOCKWOF * &H10000) + 3
Public Const FID_STOCKWOF_PartCode = (CLASSID_STOCKWOF * &H10000) + 4
Public Const FID_STOCKWOF_DateCreated = (CLASSID_STOCKWOF * &H10000) + 5
Public Const FID_STOCKWOF_AuthorisedBy = (CLASSID_STOCKWOF * &H10000) + 6
Public Const FID_STOCKWOF_AuthorisedDate = (CLASSID_STOCKWOF * &H10000) + 7
Public Const FID_STOCKWOF_Quantity = (CLASSID_STOCKWOF * &H10000) + 8
Public Const FID_STOCKWOF_Price = (CLASSID_STOCKWOF * &H10000) + 9
Public Const FID_STOCKWOF_Cost = (CLASSID_STOCKWOF * &H10000) + 10
Public Const FID_STOCKWOF_AdjustedBy = (CLASSID_STOCKWOF * &H10000) + 11
Public Const FID_STOCKWOF_Comment = (CLASSID_STOCKWOF * &H10000) + 12
Public Const FID_STOCKWOF_END_OF_STATIC = FID_STOCKWOF_Comment

Public Const FID_STOCKCOUNTHEADER_LocationNumber = (CLASSID_STOCKCOUNTHEADER * &H10000) + 1
Public Const FID_STOCKCOUNTHEADER_ItemCount = (CLASSID_STOCKCOUNTHEADER * &H10000) + 2
Public Const FID_STOCKCOUNTHEADER_AppliedCount = (CLASSID_STOCKCOUNTHEADER * &H10000) + 3
Public Const FID_STOCKCOUNTHEADER_DateFrozen = (CLASSID_STOCKCOUNTHEADER * &H10000) + 4
Public Const FID_STOCKCOUNTHEADER_TimeFrozen = (CLASSID_STOCKCOUNTHEADER * &H10000) + 5
Public Const FID_STOCKCOUNTHEADER_END_OF_STATIC = FID_STOCKCOUNTHEADER_TimeFrozen

Public Const FID_STOCKCOUNTLINE_LocationNumber = (CLASSID_STOCKCOUNTLINE * &H10000) + 1
Public Const FID_STOCKCOUNTLINE_SequenceNumber = (CLASSID_STOCKCOUNTLINE * &H10000) + 2
Public Const FID_STOCKCOUNTLINE_PartCode = (CLASSID_STOCKCOUNTLINE * &H10000) + 3
Public Const FID_STOCKCOUNTLINE_DepartmentNumber = (CLASSID_STOCKCOUNTLINE * &H10000) + 4
Public Const FID_STOCKCOUNTLINE_QuantityCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 5
Public Const FID_STOCKCOUNTLINE_OldDate = (CLASSID_STOCKCOUNTLINE * &H10000) + 6
Public Const FID_STOCKCOUNTLINE_TimeCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 7
Public Const FID_STOCKCOUNTLINE_QuantitySold = (CLASSID_STOCKCOUNTLINE * &H10000) + 8
Public Const FID_STOCKCOUNTLINE_DisplayQuantityCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 9
Public Const FID_STOCKCOUNTLINE_DateCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 10
Public Const FID_STOCKCOUNTLINE_END_OF_STATIC = FID_STOCKCOUNTLINE_DateCounted

Public Const FID_STOCKADJUST_AdjustmentDate = (CLASSID_STOCKADJUST * &H10000) + 1
Public Const FID_STOCKADJUST_AdjustmentCode = (CLASSID_STOCKADJUST * &H10000) + 2
Public Const FID_STOCKADJUST_PartCode = (CLASSID_STOCKADJUST * &H10000) + 3
Public Const FID_STOCKADJUST_CommNo = (CLASSID_STOCKADJUST * &H10000) + 4
Public Const FID_STOCKADJUST_AdjustmentBy = (CLASSID_STOCKADJUST * &H10000) + 5
Public Const FID_STOCKADJUST_DepartmentNo = (CLASSID_STOCKADJUST * &H10000) + 6
Public Const FID_STOCKADJUST_OpeningQuantity = (CLASSID_STOCKADJUST * &H10000) + 7
Public Const FID_STOCKADJUST_AdjustmentQuantity = (CLASSID_STOCKADJUST * &H10000) + 8
Public Const FID_STOCKADJUST_Price = (CLASSID_STOCKADJUST * &H10000) + 9
Public Const FID_STOCKADJUST_Cost = (CLASSID_STOCKADJUST * &H10000) + 10
Public Const FID_STOCKADJUST_AdustmentType = (CLASSID_STOCKADJUST * &H10000) + 11
Public Const FID_STOCKADJUST_TransferPartCode = (CLASSID_STOCKADJUST * &H10000) + 12
Public Const FID_STOCKADJUST_TransferKeyPrice = (CLASSID_STOCKADJUST * &H10000) + 13
Public Const FID_STOCKADJUST_TransferMarkdownValue = (CLASSID_STOCKADJUST * &H10000) + 14
Public Const FID_STOCKADJUST_Comment = (CLASSID_STOCKADJUST * &H10000) + 15
Public Const FID_STOCKADJUST_DRLNo = (CLASSID_STOCKADJUST * &H10000) + 16
Public Const FID_STOCKADJUST_SequenceNo = (CLASSID_STOCKADJUST * &H10000) + 17
Public Const FID_STOCKADJUST_QuantityPerUnit = (CLASSID_STOCKADJUST * &H10000) + 18
Public Const FID_STOCKADJUST_Information2 = (CLASSID_STOCKADJUST * &H10000) + 19
Public Const FID_STOCKADJUST_AuthorisedBy = (CLASSID_STOCKADJUST * &H10000) + 20
Public Const FID_STOCKADJUST_END_OF_STATIC = FID_STOCKADJUST_AuthorisedBy

Public Const FID_STOCKADJPEND_PartCode = (CLASSID_STOCKADJPEND * &H10000) + 1
Public Const FID_STOCKADJPEND_TotalQuantity = (CLASSID_STOCKADJPEND * &H10000) + 2
Public Const FID_STOCKADJPEND_CountedQuantity = (CLASSID_STOCKADJPEND * &H10000) + 3
Public Const FID_STOCKADJPEND_OverStockQuantity = (CLASSID_STOCKADJPEND * &H10000) + 4
Public Const FID_STOCKADJPEND_Comment = (CLASSID_STOCKADJPEND * &H10000) + 5
Public Const FID_STOCKADJPEND_AppliedCode = (CLASSID_STOCKADJPEND * &H10000) + 6
Public Const FID_STOCKADJPEND_VarianceQuantity = (CLASSID_STOCKADJPEND * &H10000) + 7
Public Const FID_STOCKADJPEND_VarianceDescription = (CLASSID_STOCKADJPEND * &H10000) + 8
Public Const FID_STOCKADJPEND_END_OF_STATIC = FID_STOCKADJPEND_VarianceDescription

Public Const FID_STOCKLOCATION_PartCode = (CLASSID_STOCKLOCATION * &H10000) + 1
Public Const FID_STOCKLOCATION_LocationNo = (CLASSID_STOCKLOCATION * &H10000) + 2
Public Const FID_STOCKLOCATION_LastStockCountQuantity = (CLASSID_STOCKLOCATION * &H10000) + 3
Public Const FID_STOCKLOCATION_LastUpdated = (CLASSID_STOCKLOCATION * &H10000) + 4
Public Const FID_STOCKLOCATION_ChangedBy = (CLASSID_STOCKLOCATION * &H10000) + 5
Public Const FID_STOCKLOCATION_RFStockCheck = (CLASSID_STOCKLOCATION * &H10000) + 6
Public Const FID_STOCKLOCATION_SequenceInLocation = (CLASSID_STOCKLOCATION * &H10000) + 7
Public Const FID_STOCKLOCATION_END_OF_STATIC = FID_STOCKLOCATION_SequenceInLocation

Public Const FID_STOCKLOCATION2_Location = (CLASSID_STOCKLOCATION2 * &H10000) + 1
Public Const FID_STOCKLOCATION2_PartCode = (CLASSID_STOCKLOCATION2 * &H10000) + 2
Public Const FID_STOCKLOCATION2_END_OF_STATIC = FID_STOCKLOCATION2_PartCode

Public Const FID_LOCATIONTYPE_LocationType = (CLASSID_LOCATIONTYPE * &H10000) + 1
Public Const FID_LOCATIONTYPE_Description = (CLASSID_LOCATIONTYPE * &H10000) + 2
Public Const FID_LOCATIONTYPE_POSMaterialPrint = (CLASSID_LOCATIONTYPE * &H10000) + 3
Public Const FID_LOCATIONTYPE_NoLocationsLimit = (CLASSID_LOCATIONTYPE * &H10000) + 4
Public Const FID_LOCATIONTYPE_END_OF_STATIC = FID_LOCATIONTYPE_NoLocationsLimit

Public Const FID_RELATEDITEM_ItemNo = (CLASSID_RELATEDITEM * &H10000) + 1
Public Const FID_RELATEDITEM_BulkItemNo = (CLASSID_RELATEDITEM * &H10000) + 2
Public Const FID_RELATEDITEM_SinglesPerPack = (CLASSID_RELATEDITEM * &H10000) + 3
Public Const FID_RELATEDITEM_SinglesSoldNotUpdated = (CLASSID_RELATEDITEM * &H10000) + 4
Public Const FID_RELATEDITEM_Deleted = (CLASSID_RELATEDITEM * &H10000) + 5
Public Const FID_RELATEDITEM_SinglesSoldToDate = (CLASSID_RELATEDITEM * &H10000) + 6
Public Const FID_RELATEDITEM_SinglesSoldToYear = (CLASSID_RELATEDITEM * &H10000) + 7
Public Const FID_RELATEDITEM_SinglesSoldPriorYear = (CLASSID_RELATEDITEM * &H10000) + 8
Public Const FID_RELATEDITEM_MarkUpTransThisWeek = (CLASSID_RELATEDITEM * &H10000) + 9
Public Const FID_RELATEDITEM_MarkUpTransPriorWeek = (CLASSID_RELATEDITEM * &H10000) + 10
Public Const FID_RELATEDITEM_MarkUpThisWeek = (CLASSID_RELATEDITEM * &H10000) + 11
Public Const FID_RELATEDITEM_MarkUpPriorWeek = (CLASSID_RELATEDITEM * &H10000) + 12
Public Const FID_RELATEDITEM_MarkUpThisPeriod = (CLASSID_RELATEDITEM * &H10000) + 13
Public Const FID_RELATEDITEM_MarkUpPriorPeriod = (CLASSID_RELATEDITEM * &H10000) + 14
Public Const FID_RELATEDITEM_MarkUpThisYear = (CLASSID_RELATEDITEM * &H10000) + 15
Public Const FID_RELATEDITEM_MarkUpPriorYear = (CLASSID_RELATEDITEM * &H10000) + 16
Public Const FID_RELATEDITEM_END_OF_STATIC = FID_RELATEDITEM_MarkUpPriorYear

Public Const FID_PRICECHANGE_PartCode = (CLASSID_PRICECHANGE * &H10000) + 1
Public Const FID_PRICECHANGE_EffectiveDate = (CLASSID_PRICECHANGE * &H10000) + 2
Public Const FID_PRICECHANGE_SupplierNo = (CLASSID_PRICECHANGE * &H10000) + 3
Public Const FID_PRICECHANGE_Location = (CLASSID_PRICECHANGE * &H10000) + 4
Public Const FID_PRICECHANGE_SequenceNo = (CLASSID_PRICECHANGE * &H10000) + 5
Public Const FID_PRICECHANGE_NewPrice = (CLASSID_PRICECHANGE * &H10000) + 6
Public Const FID_PRICECHANGE_NewRetailPrice = (CLASSID_PRICECHANGE * &H10000) + 7
Public Const FID_PRICECHANGE_NewOtherPrice = (CLASSID_PRICECHANGE * &H10000) + 8
Public Const FID_PRICECHANGE_NewSpecialPrice = (CLASSID_PRICECHANGE * &H10000) + 9
Public Const FID_PRICECHANGE_ChangeStatus = (CLASSID_PRICECHANGE * &H10000) + 10
Public Const FID_PRICECHANGE_PriceBand = (CLASSID_PRICECHANGE * &H10000) + 11
Public Const FID_PRICECHANGE_LabelPrinted = (CLASSID_PRICECHANGE * &H10000) + 12
Public Const FID_PRICECHANGE_AutoApplyDate = (CLASSID_PRICECHANGE * &H10000) + 13
Public Const FID_PRICECHANGE_AutoAppliedDate = (CLASSID_PRICECHANGE * &H10000) + 14
Public Const FID_PRICECHANGE_MarkUpChange = (CLASSID_PRICECHANGE * &H10000) + 15
Public Const FID_PRICECHANGE_NewUnitOfMeasure = (CLASSID_PRICECHANGE * &H10000) + 16
Public Const FID_PRICECHANGE_NewQuantityPerUnit = (CLASSID_PRICECHANGE * &H10000) + 17
Public Const FID_PRICECHANGE_NewBuyingUnit = (CLASSID_PRICECHANGE * &H10000) + 18
Public Const FID_PRICECHANGE_NewSellingUnit = (CLASSID_PRICECHANGE * &H10000) + 19
Public Const FID_PRICECHANGE_Type1Print = (CLASSID_PRICECHANGE * &H10000) + 20
Public Const FID_PRICECHANGE_Type2Print = (CLASSID_PRICECHANGE * &H10000) + 21
Public Const FID_PRICECHANGE_Type3Print = (CLASSID_PRICECHANGE * &H10000) + 22
Public Const FID_PRICECHANGE_Type4Print = (CLASSID_PRICECHANGE * &H10000) + 23
Public Const FID_PRICECHANGE_Type5Print = (CLASSID_PRICECHANGE * &H10000) + 24
Public Const FID_PRICECHANGE_Type6Print = (CLASSID_PRICECHANGE * &H10000) + 25
Public Const FID_PRICECHANGE_CommNo = (CLASSID_PRICECHANGE * &H10000) + 26
Public Const FID_PRICECHANGE_END_OF_STATIC = FID_PRICECHANGE_CommNo

Public Const FID_EAN_EANNumber = (CLASSID_EAN * &H10000) + 1
Public Const FID_EAN_PartCode = (CLASSID_EAN * &H10000) + 2
Public Const FID_EAN_AddedAtStore = (CLASSID_EAN * &H10000) + 3
Public Const FID_EAN_DateAdded = (CLASSID_EAN * &H10000) + 4
Public Const FID_EAN_END_OF_STATIC = FID_EAN_DateAdded

Public Const FID_POSACTION_PartCode = (CLASSID_POSACTION * &H10000) + 1
Public Const FID_POSACTION_Location = (CLASSID_POSACTION * &H10000) + 2
Public Const FID_POSACTION_SelMissing = (CLASSID_POSACTION * &H10000) + 3
Public Const FID_POSACTION_RetailPrice = (CLASSID_POSACTION * &H10000) + 4
Public Const FID_POSACTION_SpecialPrice = (CLASSID_POSACTION * &H10000) + 5
Public Const FID_POSACTION_EANWrong = (CLASSID_POSACTION * &H10000) + 6
Public Const FID_POSACTION_OverShelfEANWrong = (CLASSID_POSACTION * &H10000) + 7
Public Const FID_POSACTION_POSMissing = (CLASSID_POSACTION * &H10000) + 8
Public Const FID_POSACTION_POSInfoIncorrect = (CLASSID_POSACTION * &H10000) + 9
Public Const FID_POSACTION_ItemNeedsTagging = (CLASSID_POSACTION * &H10000) + 10
Public Const FID_POSACTION_WrongLocation = (CLASSID_POSACTION * &H10000) + 11
Public Const FID_POSACTION_OverStockWrongLocation = (CLASSID_POSACTION * &H10000) + 12
Public Const FID_POSACTION_ShelfStockCount = (CLASSID_POSACTION * &H10000) + 13
Public Const FID_POSACTION_OverStockCount = (CLASSID_POSACTION * &H10000) + 14
Public Const FID_POSACTION_END_OF_STATIC = FID_POSACTION_OverStockCount

Public Const FID_POSMESSAGE_MessageNo = (CLASSID_POSMESSAGE * &H10000) + 1
Public Const FID_POSMESSAGE_MessageText = (CLASSID_POSMESSAGE * &H10000) + 2
Public Const FID_POSMESSAGE_MessageType = (CLASSID_POSMESSAGE * &H10000) + 3
Public Const FID_POSMESSAGE_END_OF_STATIC = FID_POSMESSAGE_MessageText

Public Const FID_STOCKADJCODE_AdjustmentNo = (CLASSID_STOCKADJCODE * &H10000) + 1
Public Const FID_STOCKADJCODE_Description = (CLASSID_STOCKADJCODE * &H10000) + 2
Public Const FID_STOCKADJCODE_AdjustmentType = (CLASSID_STOCKADJCODE * &H10000) + 3
Public Const FID_STOCKADJCODE_Sign = (CLASSID_STOCKADJCODE * &H10000) + 4
Public Const FID_STOCKADJCODE_SecurityLevel = (CLASSID_STOCKADJCODE * &H10000) + 5
Public Const FID_STOCKADJCODE_END_OF_STATIC = FID_STOCKADJCODE_SecurityLevel

Public Const FID_UNITOFMEASURE_UnitOfMeasureCode = (CLASSID_UNITOFMEASURE * &H10000) + 1
Public Const FID_UNITOFMEASURE_Description = (CLASSID_UNITOFMEASURE * &H10000) + 2
Public Const FID_UNITOFMEASURE_Deleted = (CLASSID_UNITOFMEASURE * &H10000) + 3
Public Const FID_UNITOFMEASURE_END_OF_STATIC = FID_UNITOFMEASURE_Deleted

Public Const FID_PRODUCTGROUP_DepartmentNo = (CLASSID_PRODUCTGROUP * &H10000) + 1
Public Const FID_PRODUCTGROUP_GroupNo = (CLASSID_PRODUCTGROUP * &H10000) + 2
Public Const FID_PRODUCTGROUP_SalesValue = (CLASSID_PRODUCTGROUP * &H10000) + 3
Public Const FID_PRODUCTGROUP_SalesCost = (CLASSID_PRODUCTGROUP * &H10000) + 4
Public Const FID_PRODUCTGROUP_END_OF_STATIC = FID_PRODUCTGROUP_SalesCost

Public Const FID_ITEMGROUP_PartCode = (CLASSID_ITEMGROUP * &H10000) + 1
Public Const FID_ITEMGROUP_DepartmentNo = (CLASSID_ITEMGROUP * &H10000) + 2
Public Const FID_ITEMGROUP_GroupNo = (CLASSID_ITEMGROUP * &H10000) + 3
Public Const FID_ITEMGROUP_WeekSaleQuantity = (CLASSID_ITEMGROUP * &H10000) + 4
Public Const FID_ITEMGROUP_WeekSaleValue = (CLASSID_ITEMGROUP * &H10000) + 5
Public Const FID_ITEMGROUP_WeekSaleCost = (CLASSID_ITEMGROUP * &H10000) + 6
Public Const FID_ITEMGROUP_WeekCummCost = (CLASSID_ITEMGROUP * &H10000) + 7
Public Const FID_ITEMGROUP_WeekDaysOpen = (CLASSID_ITEMGROUP * &H10000) + 8
Public Const FID_ITEMGROUP_WeekDaysStockToSell = (CLASSID_ITEMGROUP * &H10000) + 9
Public Const FID_ITEMGROUP_END_OF_STATIC = FID_ITEMGROUP_WeekDaysStockToSell

Public Const FID_DEPTGROUP_DepartmentNo = (CLASSID_DEPTGROUP * &H10000) + 1
Public Const FID_DEPTGROUP_GroupNo = (CLASSID_DEPTGROUP * &H10000) + 2
Public Const FID_DEPTGROUP_Description = (CLASSID_DEPTGROUP * &H10000) + 3
Public Const FID_DEPTGROUP_CycleDayNo = (CLASSID_DEPTGROUP * &H10000) + 4
Public Const FID_DEPTGROUP_WeekSalesValue = (CLASSID_DEPTGROUP * &H10000) + 5
Public Const FID_DEPTGROUP_WeekLookUps = (CLASSID_DEPTGROUP * &H10000) + 6
Public Const FID_DEPTGROUP_WeekOutOfStock = (CLASSID_DEPTGROUP * &H10000) + 7
Public Const FID_DEPTGROUP_END_OF_STATIC = FID_DEPTGROUP_WeekOutOfStock

Public Const FID_CASHBALANCEHDR_FileKey = (CLASSID_CASHBALANCEHDR * &H10000) + 1
Public Const FID_CASHBALANCEHDR_CurrentDate = (CLASSID_CASHBALANCEHDR * &H10000) + 2
Public Const FID_CASHBALANCEHDR_StoreBankTotal = (CLASSID_CASHBALANCEHDR * &H10000) + 3
Public Const FID_CASHBALANCEHDR_GiftVoucherStock = (CLASSID_CASHBALANCEHDR * &H10000) + 4
Public Const FID_CASHBALANCEHDR_CustomerDeposits = (CLASSID_CASHBALANCEHDR * &H10000) + 5
Public Const FID_CASHBALANCEHDR_StoreFloatAmount = (CLASSID_CASHBALANCEHDR * &H10000) + 6
Public Const FID_CASHBALANCEHDR_DepositInDeptNo = (CLASSID_CASHBALANCEHDR * &H10000) + 7
Public Const FID_CASHBALANCEHDR_DepositOutDeptNo = (CLASSID_CASHBALANCEHDR * &H10000) + 8
Public Const FID_CASHBALANCEHDR_NoSummaryKept = (CLASSID_CASHBALANCEHDR * &H10000) + 9
Public Const FID_CASHBALANCEHDR_NoSummaryNow = (CLASSID_CASHBALANCEHDR * &H10000) + 10
Public Const FID_CASHBALANCEHDR_SalesOfGiftVoucher = (CLASSID_CASHBALANCEHDR * &H10000) + 11
Public Const FID_CASHBALANCEHDR_TenderType = (CLASSID_CASHBALANCEHDR * &H10000) + 12
Public Const FID_CASHBALANCEHDR_NoReturns = (CLASSID_CASHBALANCEHDR * &H10000) + 13
Public Const FID_CASHBALANCEHDR_DepositInBank = (CLASSID_CASHBALANCEHDR * &H10000) + 14
Public Const FID_CASHBALANCEHDR_SequenceTendered = (CLASSID_CASHBALANCEHDR * &H10000) + 15
Public Const FID_CASHBALANCEHDR_RequestSlipForTender = (CLASSID_CASHBALANCEHDR * &H10000) + 16
Public Const FID_CASHBALANCEHDR_MaximumPerSlip = (CLASSID_CASHBALANCEHDR * &H10000) + 17
Public Const FID_CASHBALANCEHDR_NoSummaryInFile = (CLASSID_CASHBALANCEHDR * &H10000) + 18
Public Const FID_CASHBALANCEHDR_END_OF_STATIC = FID_CASHBALANCEHDR_NoSummaryInFile

Public Const FID_SUPPLIERS_SupplierNo = (CLASSID_SUPPLIERS * &H10000) + 1
Public Const FID_SUPPLIERS_NAME = (CLASSID_SUPPLIERS * &H10000) + 2
Public Const FID_SUPPLIERS_Deleted = (CLASSID_SUPPLIERS * &H10000) + 3
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_SUPPLIERS_END_OF_STATIC = FID_SUPPLIERS_Deleted

Public Const FID_INVENTORYS_PartCode = (CLASSID_INVENTORYS * &H10000) + 1
Public Const FID_INVENTORYS_Description = (CLASSID_INVENTORYS * &H10000) + 2
Public Const FID_INVENTORYS_Department = (CLASSID_INVENTORYS * &H10000) + 3
Public Const FID_INVENTORYS_SupplierNo = (CLASSID_INVENTORYS * &H10000) + 4
Public Const FID_INVENTORYS_Flag1 = (CLASSID_INVENTORYS * &H10000) + 5
Public Const FID_INVENTORYS_Flag2 = (CLASSID_INVENTORYS * &H10000) + 6
Public Const FID_INVENTORYS_Flag3 = (CLASSID_INVENTORYS * &H10000) + 7
Public Const FID_INVENTORYS_Flag4 = (CLASSID_INVENTORYS * &H10000) + 8
Public Const FID_INVENTORYS_Flag5 = (CLASSID_INVENTORYS * &H10000) + 9
Public Const FID_INVENTORYS_Warehouse = (CLASSID_INVENTORYS * &H10000) + 10
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_INVENTORYS_END_OF_STATIC = FID_INVENTORYS_Warehouse

Public Const FID_DESPATCHMETHOD_ID = (CLASSID_DESPATCHMETHOD * &H10000) + 1
Public Const FID_DESPATCHMETHOD_Description = (CLASSID_DESPATCHMETHOD * &H10000) + 2
Public Const FID_DESPATCHMETHOD_Active = (CLASSID_DESPATCHMETHOD * &H10000) + 3
Public Const FID_DESPATCHMETHOD_ReferenceRequired = (CLASSID_DESPATCHMETHOD * &H10000) + 4
Public Const FID_DESPATCHMETHOD_END_OF_STATIC = FID_DESPATCHMETHOD_ReferenceRequired

Public Const FID_PURHDRNARRATIVE_StoreNumber = (CLASSID_PURHDRNARRATIVE * &H10000) + 1
Public Const FID_PURHDRNARRATIVE_OrderID = (CLASSID_PURHDRNARRATIVE * &H10000) + 2
Public Const FID_PURHDRNARRATIVE_TextLine1 = (CLASSID_PURHDRNARRATIVE * &H10000) + 3
Public Const FID_PURHDRNARRATIVE_TextLine2 = (CLASSID_PURHDRNARRATIVE * &H10000) + 4
Public Const FID_PURHDRNARRATIVE_END_OF_STATIC = FID_PURHDRNARRATIVE_TextLine2

Public Const FID_CUSTOMERS_END_OF_STATIC = 59

Public Const FID_TYPESOFSALEOPTION_TypeOfSaleCode = (CLASSID_TYPESOFSALEOPTION * &H10000) + 1
Public Const FID_TYPESOFSALEOPTION_Description = (CLASSID_TYPESOFSALEOPTION * &H10000) + 2
Public Const FID_TYPESOFSALEOPTION_TenderTypesAllowed = (CLASSID_TYPESOFSALEOPTION * &H10000) + 3
Public Const FID_TYPESOFSALEOPTION_SupervisorToStart = (CLASSID_TYPESOFSALEOPTION * &H10000) + 4
Public Const FID_TYPESOFSALEOPTION_RequestExternalDoc = (CLASSID_TYPESOFSALEOPTION * &H10000) + 5
Public Const FID_TYPESOFSALEOPTION_SpecialPrint = (CLASSID_TYPESOFSALEOPTION * &H10000) + 6
Public Const FID_TYPESOFSALEOPTION_OpenOnZeroTran = (CLASSID_TYPESOFSALEOPTION * &H10000) + 7
Public Const FID_TYPESOFSALEOPTION_SignValue = (CLASSID_TYPESOFSALEOPTION * &H10000) + 8
Public Const FID_TYPESOFSALEOPTION_SpecialAccTenderAllow = (CLASSID_TYPESOFSALEOPTION * &H10000) + 9
Public Const FID_TYPESOFSALEOPTION_SpecialAccSupervisor = (CLASSID_TYPESOFSALEOPTION * &H10000) + 10
Public Const FID_TYPESOFSALEOPTION_RequestAreaCode = (CLASSID_TYPESOFSALEOPTION * &H10000) + 11
Public Const FID_TYPESOFSALEOPTION_LineReversalSupervisor = (CLASSID_TYPESOFSALEOPTION * &H10000) + 12
Public Const FID_TYPESOFSALEOPTION_AccountTenderValid = (CLASSID_TYPESOFSALEOPTION * &H10000) + 13
Public Const FID_TYPESOFSALEOPTION_END_OF_STATIC = FID_TYPESOFSALEOPTION_AccountTenderValid

Public Const FID_TRADERS_END_OF_STATIC = 61

Public Const FID_LINECOMMENTS_ID = (CLASSID_LINECOMMENTS * &H10000) + 1
Public Const FID_LINECOMMENTS_Description = (CLASSID_LINECOMMENTS * &H10000) + 2
Public Const FID_LINECOMMENTS_Active = (CLASSID_LINECOMMENTS * &H10000) + 3
Public Const FID_LINECOMMENTS_NoteType = (CLASSID_LINECOMMENTS * &H10000) + 4
Public Const FID_LINECOMMENTS_LineNote = (CLASSID_LINECOMMENTS * &H10000) + 5
Public Const FID_LINECOMMENTS_END_OF_STATIC = FID_LINECOMMENTS_LineNote

Public Const FID_DEPTANALYSIS_DeptCode = (CLASSID_DEPTANALYSIS * &H10000) + 1
Public Const FID_DEPTANALYSIS_TotalSales = (CLASSID_DEPTANALYSIS * &H10000) + 2
Public Const FID_DEPTANALYSIS_ValueSales = (CLASSID_DEPTANALYSIS * &H10000) + 3
Public Const FID_DEPTANALYSIS_NumberItems = (CLASSID_DEPTANALYSIS * &H10000) + 4
Public Const FID_DEPTANALYSIS_END_OF_STATIC = FID_DEPTANALYSIS_NumberItems

Public Const FID_VATRATES_ID = (CLASSID_VATRATES * &H10000) + 1
Public Const FID_VATRATES_VATRateCount = (CLASSID_VATRATES * &H10000) + 2
Public Const FID_VATRATES_VATRate = (CLASSID_VATRATES * &H10000) + 3
Public Const FID_VATRATES_END_OF_STATIC = FID_VATRATES_VATRate

Public Const FID_SYSTEMDATES_ID = (CLASSID_SYSTEMDATES * &H10000) + 1
Public Const FID_SYSTEMDATES_NoDaysOpen = (CLASSID_SYSTEMDATES * &H10000) + 2
Public Const FID_SYSTEMDATES_WeekEndingDay = (CLASSID_SYSTEMDATES * &H10000) + 3
Public Const FID_SYSTEMDATES_TodaysDate = (CLASSID_SYSTEMDATES * &H10000) + 4
Public Const FID_SYSTEMDATES_TodaysDayNo = (CLASSID_SYSTEMDATES * &H10000) + 5
Public Const FID_SYSTEMDATES_NextOpenDate = (CLASSID_SYSTEMDATES * &H10000) + 6
Public Const FID_SYSTEMDATES_NextOpenDayNo = (CLASSID_SYSTEMDATES * &H10000) + 7
Public Const FID_SYSTEMDATES_NoWeeksInCycle = (CLASSID_SYSTEMDATES * &H10000) + 8
Public Const FID_SYSTEMDATES_CurrentWeekNo = (CLASSID_SYSTEMDATES * &H10000) + 9
Public Const FID_SYSTEMDATES_CurrentPeriodEndDate = (CLASSID_SYSTEMDATES * &H10000) + 10
Public Const FID_SYSTEMDATES_PriorPeriodEndDate = (CLASSID_SYSTEMDATES * &H10000) + 11
Public Const FID_SYSTEMDATES_CurrentIsYearEnd = (CLASSID_SYSTEMDATES * &H10000) + 12
Public Const FID_SYSTEMDATES_PeriodEndNotProcessed = (CLASSID_SYSTEMDATES * &H10000) + 13
Public Const FID_SYSTEMDATES_WeekEndNotProcessed = (CLASSID_SYSTEMDATES * &H10000) + 14
Public Const FID_SYSTEMDATES_NightTaskSetNo = (CLASSID_SYSTEMDATES * &H10000) + 15
Public Const FID_SYSTEMDATES_CurrentTaskNo = (CLASSID_SYSTEMDATES * &H10000) + 16
Public Const FID_SYSTEMDATES_InRetryMode = (CLASSID_SYSTEMDATES * &H10000) + 17
Public Const FID_SYSTEMDATES_StoreLiveDate = (CLASSID_SYSTEMDATES * &H10000) + 18
Public Const FID_SYSTEMDATES_UsePeriodSetNo = (CLASSID_SYSTEMDATES * &H10000) + 19
Public Const FID_SYSTEMDATES_Period1EndDates = (CLASSID_SYSTEMDATES * &H10000) + 20
Public Const FID_SYSTEMDATES_Period1Ended = (CLASSID_SYSTEMDATES * &H10000) + 21
Public Const FID_SYSTEMDATES_Period1EndProc = (CLASSID_SYSTEMDATES * &H10000) + 22
Public Const FID_SYSTEMDATES_Period2EndDates = (CLASSID_SYSTEMDATES * &H10000) + 23
Public Const FID_SYSTEMDATES_Period2Ended = (CLASSID_SYSTEMDATES * &H10000) + 24
Public Const FID_SYSTEMDATES_Period2EndProc = (CLASSID_SYSTEMDATES * &H10000) + 25
Public Const FID_SYSTEMDATES_WeekEndDates = (CLASSID_SYSTEMDATES * &H10000) + 26
Public Const FID_SYSTEMDATES_SOQCycleLength = (CLASSID_SYSTEMDATES * &H10000) + 27
Public Const FID_SYSTEMDATES_CurrentSOQWeekNo = (CLASSID_SYSTEMDATES * &H10000) + 28
Public Const FID_SYSTEMDATES_LastStockDate = (CLASSID_SYSTEMDATES * &H10000) + 29
Public Const FID_SYSTEMDATES_END_OF_STATIC = FID_SYSTEMDATES_LastStockDate

Public Const FID_RETURNCODE_CodeType = (CLASSID_RETURNCODE * &H10000) + 1
Public Const FID_RETURNCODE_Code = (CLASSID_RETURNCODE * &H10000) + 2
Public Const FID_RETURNCODE_Description = (CLASSID_RETURNCODE * &H10000) + 3
Public Const FID_RETURNCODE_NormalSupplierReturn = (CLASSID_RETURNCODE * &H10000) + 4
Public Const FID_RETURNCODE_END_OF_STATIC = FID_RETURNCODE_NormalSupplierReturn

Public Const FID_PRICEOVERRIDE_CodeType = (CLASSID_PRICEOVERRIDE * &H10000) + 1
Public Const FID_PRICEOVERRIDE_Code = (CLASSID_PRICEOVERRIDE * &H10000) + 2
Public Const FID_PRICEOVERRIDE_Description = (CLASSID_PRICEOVERRIDE * &H10000) + 3
Public Const FID_PRICEOVERRIDE_PercentageDisc = (CLASSID_PRICEOVERRIDE * &H10000) + 4
Public Const FID_PRICEOVERRIDE_MaximumDiscAllowed = (CLASSID_PRICEOVERRIDE * &H10000) + 5
Public Const FID_PRICEOVERRIDE_MarginErosionCode = (CLASSID_PRICEOVERRIDE * &H10000) + 6
Public Const FID_PRICEOVERRIDE_END_OF_STATIC = FID_PRICEOVERRIDE_MarginErosionCode

Public Const FID_STOCKLOG_Key = (CLASSID_STOCKLOG * &H10000) + 1
Public Const FID_STOCKLOG_LogDate = (CLASSID_STOCKLOG * &H10000) + 2
Public Const FID_STOCKLOG_LogTime = (CLASSID_STOCKLOG * &H10000) + 3
Public Const FID_STOCKLOG_PartCode = (CLASSID_STOCKLOG * &H10000) + 4
Public Const FID_STOCKLOG_MovementType = (CLASSID_STOCKLOG * &H10000) + 5
Public Const FID_STOCKLOG_TransactionKey = (CLASSID_STOCKLOG * &H10000) + 6
Public Const FID_STOCKLOG_TillNumber = (CLASSID_STOCKLOG * &H10000) + 7
Public Const FID_STOCKLOG_TransactionNo = (CLASSID_STOCKLOG * &H10000) + 8
Public Const FID_STOCKLOG_QuantityAdjusted = (CLASSID_STOCKLOG * &H10000) + 9
Public Const FID_STOCKLOG_OnHandPrior = (CLASSID_STOCKLOG * &H10000) + 10
Public Const FID_STOCKLOG_ExtCost = (CLASSID_STOCKLOG * &H10000) + 11
Public Const FID_STOCKLOG_ExtRetailValue = (CLASSID_STOCKLOG * &H10000) + 12
Public Const FID_STOCKLOG_ExtItemValue = (CLASSID_STOCKLOG * &H10000) + 13
Public Const FID_STOCKLOG_QuantityPerSellUnit = (CLASSID_STOCKLOG * &H10000) + 14
Public Const FID_STOCKLOG_StoreNumber = (CLASSID_STOCKLOG * &H10000) + 15
Public Const FID_STOCKLOG_User = (CLASSID_STOCKLOG * &H10000) + 16
Public Const FID_STOCKLOG_CommNo = (CLASSID_STOCKLOG * &H10000) + 17
Public Const FID_STOCKLOG_TraderNumber = (CLASSID_STOCKLOG * &H10000) + 18
Public Const FID_STOCKLOG_CreditAccount = (CLASSID_STOCKLOG * &H10000) + 19
Public Const FID_STOCKLOG_Description = (CLASSID_STOCKLOG * &H10000) + 20
Public Const FID_STOCKLOG_ExtListValue = (CLASSID_STOCKLOG * &H10000) + 21
Public Const FID_STOCKLOG_CustomerOrder = (CLASSID_STOCKLOG * &H10000) + 22
Public Const FID_STOCKLOG_END_OF_STATIC = FID_STOCKLOG_CustomerOrder

Public Const FID_ISTINHEADER_DocNo = (CLASSID_ISTINHEADER * &H10000) + 1
Public Const FID_ISTINHEADER_DocType = (CLASSID_ISTINHEADER * &H10000) + 2
Public Const FID_ISTINHEADER_Value = (CLASSID_ISTINHEADER * &H10000) + 3
Public Const FID_ISTINHEADER_Cost = (CLASSID_ISTINHEADER * &H10000) + 4
Public Const FID_ISTINHEADER_DelNoteDate = (CLASSID_ISTINHEADER * &H10000) + 5
Public Const FID_ISTINHEADER_Comm = (CLASSID_ISTINHEADER * &H10000) + 6
Public Const FID_ISTINHEADER_OrigDocKey = (CLASSID_ISTINHEADER * &H10000) + 7
Public Const FID_ISTINHEADER_EnteredBy = (CLASSID_ISTINHEADER * &H10000) + 8
Public Const FID_ISTINHEADER_Comment = (CLASSID_ISTINHEADER * &H10000) + 9
Public Const FID_ISTINHEADER_ConsignmentRef = (CLASSID_ISTINHEADER * &H10000) + 10
Public Const FID_ISTINHEADER_Source = (CLASSID_ISTINHEADER * &H10000) + 11
Public Const FID_ISTINHEADER_StoreNumber = (CLASSID_ISTINHEADER * &H10000) + 12
Public Const FID_ISTINHEADER_IBT = (CLASSID_ISTINHEADER * &H10000) + 13
Public Const FID_ISTINHEADER_Printed = (CLASSID_ISTINHEADER * &H10000) + 14
Public Const FID_ISTINHEADER_ConsignmentKey = (CLASSID_ISTINHEADER * &H10000) + 15
Public Const FID_ISTINHEADER_InvCrnMatched = (CLASSID_ISTINHEADER * &H10000) + 16
Public Const FID_ISTINHEADER_InvCrnDate = (CLASSID_ISTINHEADER * &H10000) + 17
Public Const FID_ISTINHEADER_InvCrnNumber = (CLASSID_ISTINHEADER * &H10000) + 18
Public Const FID_ISTINHEADER_VarianceValue = (CLASSID_ISTINHEADER * &H10000) + 19
Public Const FID_ISTINHEADER_InvoiceValue = (CLASSID_ISTINHEADER * &H10000) + 20
Public Const FID_ISTINHEADER_InvoiceVAT = (CLASSID_ISTINHEADER * &H10000) + 21
Public Const FID_ISTINHEADER_MatchingComment = (CLASSID_ISTINHEADER * &H10000) + 22
Public Const FID_ISTINHEADER_MatchingDate = (CLASSID_ISTINHEADER * &H10000) + 23
Public Const FID_ISTINHEADER_ReceiptDate = (CLASSID_ISTINHEADER * &H10000) + 24
Public Const FID_ISTINHEADER_CarraigeValue = (CLASSID_ISTINHEADER * &H10000) + 25
Public Const FID_ISTINHEADER_Commit = (CLASSID_ISTINHEADER * &H10000) + 26
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_ISTINHEADER_END_OF_STATIC = FID_ISTINHEADER_Commit

Public Const FID_STOCKTAKE_ID = (CLASSID_STOCKTAKE * &H10000) + 1
Public Const FID_STOCKTAKE_State = (CLASSID_STOCKTAKE * &H10000) + 2
Public Const FID_STOCKTAKE_FreezeDate = (CLASSID_STOCKTAKE * &H10000) + 3
Public Const FID_STOCKTAKE_AdjustDate = (CLASSID_STOCKTAKE * &H10000) + 4
Public Const FID_STOCKTAKE_Password = (CLASSID_STOCKTAKE * &H10000) + 5
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_STOCKTAKE_END_OF_STATIC = FID_STOCKTAKE_Password

Public Const FID_RETNOTEHEADER_DocNo = (CLASSID_RETNOTEHEADER * &H10000) + 1
Public Const FID_RETNOTEHEADER_DocType = (CLASSID_RETNOTEHEADER * &H10000) + 2
Public Const FID_RETNOTEHEADER_Value = (CLASSID_RETNOTEHEADER * &H10000) + 3
Public Const FID_RETNOTEHEADER_Cost = (CLASSID_RETNOTEHEADER * &H10000) + 4
Public Const FID_RETNOTEHEADER_DelNoteDate = (CLASSID_RETNOTEHEADER * &H10000) + 5
Public Const FID_RETNOTEHEADER_Comm = (CLASSID_RETNOTEHEADER * &H10000) + 6
Public Const FID_RETNOTEHEADER_OrigDocKey = (CLASSID_RETNOTEHEADER * &H10000) + 7
Public Const FID_RETNOTEHEADER_EnteredBy = (CLASSID_RETNOTEHEADER * &H10000) + 8
Public Const FID_RETNOTEHEADER_Comment = (CLASSID_RETNOTEHEADER * &H10000) + 9
Public Const FID_RETNOTEHEADER_SupplierNumber = (CLASSID_RETNOTEHEADER * &H10000) + 10
Public Const FID_RETNOTEHEADER_ReturnDate = (CLASSID_RETNOTEHEADER * &H10000) + 11
Public Const FID_RETNOTEHEADER_OrigPONumber = (CLASSID_RETNOTEHEADER * &H10000) + 12
Public Const FID_RETNOTEHEADER_SupplierReturnNumber = (CLASSID_RETNOTEHEADER * &H10000) + 13
Public Const FID_RETNOTEHEADER_InvCrnMatched = (CLASSID_RETNOTEHEADER * &H10000) + 14
Public Const FID_RETNOTEHEADER_InvCrnDate = (CLASSID_RETNOTEHEADER * &H10000) + 15
Public Const FID_RETNOTEHEADER_InvCrnNumber = (CLASSID_RETNOTEHEADER * &H10000) + 16
Public Const FID_RETNOTEHEADER_VarianceValue = (CLASSID_RETNOTEHEADER * &H10000) + 17
Public Const FID_RETNOTEHEADER_InvoiceValue = (CLASSID_RETNOTEHEADER * &H10000) + 18
Public Const FID_RETNOTEHEADER_InvoiceVAT = (CLASSID_RETNOTEHEADER * &H10000) + 19
Public Const FID_RETNOTEHEADER_MatchingComment = (CLASSID_RETNOTEHEADER * &H10000) + 20
Public Const FID_RETNOTEHEADER_MatchingDate = (CLASSID_RETNOTEHEADER * &H10000) + 21
Public Const FID_RETNOTEHEADER_ReceiptDate = (CLASSID_RETNOTEHEADER * &H10000) + 22
Public Const FID_RETNOTEHEADER_RestockCharge = (CLASSID_RETNOTEHEADER * &H10000) + 23
Public Const FID_RETNOTEHEADER_Commit = (CLASSID_RETNOTEHEADER * &H10000) + 24
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_RETNOTEHEADER_END_OF_STATIC = FID_RETNOTEHEADER_Commit

Public Const FID_PARAMETER_ID = 0
Public Const FID_PARAMETER_Description = 1
Public Const FID_PARAMETER_String = 2
Public Const FID_PARAMETER_Long = 3
Public Const FID_PARAMETER_Boolean = 4
Public Const FID_PARAMETER_Decimal = 5
Public Const FID_PARAMETER_Type = 6
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_PARAMETER_END_OF_STATIC = FID_PARAMETER_Type

'Public Const FID_PARAMETER_ID = (CLASSID_PARAMETER * &H10000) + 1
'Public Const FID_PARAMETER_Description = (CLASSID_PARAMETER * &H10000) + 2
'Public Const FID_PARAMETER_String = (CLASSID_PARAMETER * &H10000) + 3
'Public Const FID_PARAMETER_Long = (CLASSID_PARAMETER * &H10000) + 4
'Public Const FID_PARAMETER_Boolean = (CLASSID_PARAMETER * &H10000) + 5
'Public Const FID_PARAMETER_Decimal = (CLASSID_PARAMETER * &H10000) + 6
'Public Const FID_PARAMETER_Type = (CLASSID_PARAMETER * &H10000) + 7
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
'Public Const FID_PARAMETER_END_OF_STATIC = FID_PARAMETER_Type

Public Const FID_WORKSTATIONCONFIG_id = (CLASSID_WORKSTATIONCONFIG * &H10000) + 1
Public Const FID_WORKSTATIONCONFIG_Description = (CLASSID_WORKSTATIONCONFIG * &H10000) + 2
Public Const FID_WORKSTATIONCONFIG_OutletFunction = (CLASSID_WORKSTATIONCONFIG * &H10000) + 3
Public Const FID_WORKSTATIONCONFIG_Active = (CLASSID_WORKSTATIONCONFIG * &H10000) + 4
Public Const FID_WORKSTATIONCONFIG_TenderGroups = (CLASSID_WORKSTATIONCONFIG * &H10000) + 5
Public Const FID_WORKSTATIONCONFIG_GroupLevels = (CLASSID_WORKSTATIONCONFIG * &H10000) + 6
Public Const FID_WORKSTATIONCONFIG_END_OF_STATIC = FID_WORKSTATIONCONFIG_GroupLevels

Public Const FID_USER_EmployeeID = (CLASSID_USER * &H10000) + 1
Public Const FID_USER_FullName = (CLASSID_USER * &H10000) + 2
Public Const FID_USER_Initials = (CLASSID_USER * &H10000) + 3
Public Const FID_USER_Position = (CLASSID_USER * &H10000) + 4
Public Const FID_USER_PayrollNumber = (CLASSID_USER * &H10000) + 5
Public Const FID_USER_Password = (CLASSID_USER * &H10000) + 6
Public Const FID_USER_PasswordValidTill = (CLASSID_USER * &H10000) + 7
Public Const FID_USER_Supervisor = (CLASSID_USER * &H10000) + 8
Public Const FID_USER_AuthorisationCode = (CLASSID_USER * &H10000) + 9
Public Const FID_USER_AuthorisationValidTill = (CLASSID_USER * &H10000) + 10
Public Const FID_USER_CurrentOutlet = (CLASSID_USER * &H10000) + 11
Public Const FID_USER_GroupLevels = (CLASSID_USER * &H10000) + 12
Public Const FID_USER_SecurityLevels = (CLASSID_USER * &H10000) + 13
Public Const FID_USER_Deleted = (CLASSID_USER * &H10000) + 14
Public Const FID_USER_DateDeleted = (CLASSID_USER * &H10000) + 15
Public Const FID_USER_TimeDeleted = (CLASSID_USER * &H10000) + 16
Public Const FID_USER_DeletedBy = (CLASSID_USER * &H10000) + 17
Public Const FID_USER_DeletedOutlet = (CLASSID_USER * &H10000) + 18
Public Const FID_USER_TillReceiptName = (CLASSID_USER * &H10000) + 19
Public Const FID_USER_FloatAmount = (CLASSID_USER * &H10000) + 20
Public Const FID_USER_LanguageCode = (CLASSID_USER * &H10000) + 21
Public Const FID_USER_END_OF_STATIC = FID_USER_LanguageCode

Public Const FID_TILLLOOKUPS_ID = (CLASSID_TILLLOOKUPS * &H10000) + 1
Public Const FID_TILLLOOKUPS_TenderTypeDescription = (CLASSID_TILLLOOKUPS * &H10000) + 2
Public Const FID_TILLLOOKUPS_TenderTypeCode = (CLASSID_TILLLOOKUPS * &H10000) + 3
Public Const FID_TILLLOOKUPS_TenderTypeDisplayOrder = (CLASSID_TILLLOOKUPS * &H10000) + 4
Public Const FID_TILLLOOKUPS_TenderTypeOverTenderAllowed = (CLASSID_TILLLOOKUPS * &H10000) + 5
Public Const FID_TILLLOOKUPS_TenderTypeCaptureCreditCard = (CLASSID_TILLLOOKUPS * &H10000) + 6
Public Const FID_TILLLOOKUPS_TenderTypeEFTPOSRequired = (CLASSID_TILLLOOKUPS * &H10000) + 7
Public Const FID_TILLLOOKUPS_TenderTypeOpenDrawer = (CLASSID_TILLLOOKUPS * &H10000) + 8
Public Const FID_TILLLOOKUPS_TenderTypeDefaultRemainAmount = (CLASSID_TILLLOOKUPS * &H10000) + 9
Public Const FID_TILLLOOKUPS_OpenDrawerReasons = (CLASSID_TILLLOOKUPS * &H10000) + 10
Public Const FID_TILLLOOKUPS_MiscIncomeAccountNo = (CLASSID_TILLLOOKUPS * &H10000) + 11
Public Const FID_TILLLOOKUPS_MiscIncomeVATRequired = (CLASSID_TILLLOOKUPS * &H10000) + 12
Public Const FID_TILLLOOKUPS_MiscIncomeDeposit = (CLASSID_TILLLOOKUPS * &H10000) + 13
Public Const FID_TILLLOOKUPS_MiscIncomeNumber = (CLASSID_TILLLOOKUPS * &H10000) + 14
Public Const FID_TILLLOOKUPS_MiscPaidOutCode = (CLASSID_TILLLOOKUPS * &H10000) + 15
Public Const FID_TILLLOOKUPS_MiscPaidOutVATRequired = (CLASSID_TILLLOOKUPS * &H10000) + 16
Public Const FID_TILLLOOKUPS_MiscPaidOutDeposit = (CLASSID_TILLLOOKUPS * &H10000) + 17
Public Const FID_TILLLOOKUPS_END_OF_STATIC = FID_TILLLOOKUPS_MiscPaidOutDeposit

Public Const FID_RETAILEROPTIONS_ID = (CLASSID_RETAILEROPTIONS * &H10000) + 1
Public Const FID_RETAILEROPTIONS_StoreNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 2
Public Const FID_RETAILEROPTIONS_StoreName = (CLASSID_RETAILEROPTIONS * &H10000) + 3
Public Const FID_RETAILEROPTIONS_AddressLine1 = (CLASSID_RETAILEROPTIONS * &H10000) + 4
Public Const FID_RETAILEROPTIONS_AddressLine2 = (CLASSID_RETAILEROPTIONS * &H10000) + 5
Public Const FID_RETAILEROPTIONS_AddressLine3 = (CLASSID_RETAILEROPTIONS * &H10000) + 6
Public Const FID_RETAILEROPTIONS_PostCode = (CLASSID_RETAILEROPTIONS * &H10000) + 7
Public Const FID_RETAILEROPTIONS_PhoneNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 8
Public Const FID_RETAILEROPTIONS_ContactName = (CLASSID_RETAILEROPTIONS * &H10000) + 9
Public Const FID_RETAILEROPTIONS_VatRatesInclusive = (CLASSID_RETAILEROPTIONS * &H10000) + 10
Public Const FID_RETAILEROPTIONS_VATExemptDept = (CLASSID_RETAILEROPTIONS * &H10000) + 11
Public Const FID_RETAILEROPTIONS_VATExemptGroup = (CLASSID_RETAILEROPTIONS * &H10000) + 12
Public Const FID_RETAILEROPTIONS_AccountabilityType = (CLASSID_RETAILEROPTIONS * &H10000) + 13
Public Const FID_RETAILEROPTIONS_MaxValidCashierNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 14
Public Const FID_RETAILEROPTIONS_KeepSalesPeriod = (CLASSID_RETAILEROPTIONS * &H10000) + 15
Public Const FID_RETAILEROPTIONS_EmployeeDiscount = (CLASSID_RETAILEROPTIONS * &H10000) + 16
Public Const FID_RETAILEROPTIONS_EmployeeDiscCode = (CLASSID_RETAILEROPTIONS * &H10000) + 17
Public Const FID_RETAILEROPTIONS_LocalDiscountCode = (CLASSID_RETAILEROPTIONS * &H10000) + 18
Public Const FID_RETAILEROPTIONS_MaximumDiscount = (CLASSID_RETAILEROPTIONS * &H10000) + 19
Public Const FID_RETAILEROPTIONS_DefaultFloatAmount = (CLASSID_RETAILEROPTIONS * &H10000) + 20
Public Const FID_RETAILEROPTIONS_MaximumCashGiven = (CLASSID_RETAILEROPTIONS * &H10000) + 21
Public Const FID_RETAILEROPTIONS_AccountCardFloorLimit = (CLASSID_RETAILEROPTIONS * &H10000) + 22
Public Const FID_RETAILEROPTIONS_CreditCardFloorLimit = (CLASSID_RETAILEROPTIONS * &H10000) + 23
Public Const FID_RETAILEROPTIONS_MaxBusinessCheque = (CLASSID_RETAILEROPTIONS * &H10000) + 24
Public Const FID_RETAILEROPTIONS_MaxPersonalCheque = (CLASSID_RETAILEROPTIONS * &H10000) + 25
Public Const FID_RETAILEROPTIONS_MaxTillDrawerFloat = (CLASSID_RETAILEROPTIONS * &H10000) + 26
Public Const FID_RETAILEROPTIONS_MaxTranBeforeVATReclaim = (CLASSID_RETAILEROPTIONS * &H10000) + 27
Public Const FID_RETAILEROPTIONS_SavingsFactor = (CLASSID_RETAILEROPTIONS * &H10000) + 28
Public Const FID_RETAILEROPTIONS_CreditCardOutletID = (CLASSID_RETAILEROPTIONS * &H10000) + 29
Public Const FID_RETAILEROPTIONS_EFTPOSDeviceNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 30
Public Const FID_RETAILEROPTIONS_AccountCardsUsed = (CLASSID_RETAILEROPTIONS * &H10000) + 31
Public Const FID_RETAILEROPTIONS_InvoiceFormat = (CLASSID_RETAILEROPTIONS * &H10000) + 32
Public Const FID_RETAILEROPTIONS_RRReportsDate = (CLASSID_RETAILEROPTIONS * &H10000) + 33
Public Const FID_RETAILEROPTIONS_RRDateLastRun = (CLASSID_RETAILEROPTIONS * &H10000) + 34
Public Const FID_RETAILEROPTIONS_PricePromiseDiffMultiplier = (CLASSID_RETAILEROPTIONS * &H10000) + 35
Public Const FID_RETAILEROPTIONS_PricePromiseMaxDeduction = (CLASSID_RETAILEROPTIONS * &H10000) + 36
Public Const FID_RETAILEROPTIONS_PriceMatchOverrideCode = (CLASSID_RETAILEROPTIONS * &H10000) + 37
Public Const FID_RETAILEROPTIONS_PricePromiseOverrideCode = (CLASSID_RETAILEROPTIONS * &H10000) + 38
Public Const FID_RETAILEROPTIONS_PayByDayNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 39
Public Const FID_RETAILEROPTIONS_BankAccountNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 40
Public Const FID_RETAILEROPTIONS_PrintChequeFront = (CLASSID_RETAILEROPTIONS * &H10000) + 41
Public Const FID_RETAILEROPTIONS_PrintChequeBack = (CLASSID_RETAILEROPTIONS * &H10000) + 42
Public Const FID_RETAILEROPTIONS_LowestCashDenomination = (CLASSID_RETAILEROPTIONS * &H10000) + 43
Public Const FID_RETAILEROPTIONS_LowestPriceDenomination = (CLASSID_RETAILEROPTIONS * &H10000) + 44
Public Const FID_RETAILEROPTIONS_TillTimeOut = (CLASSID_RETAILEROPTIONS * &H10000) + 45
Public Const FID_RETAILEROPTIONS_TypeOfSaleCode = (CLASSID_RETAILEROPTIONS * &H10000) + 46
Public Const FID_RETAILEROPTIONS_QuotesMarginCode = (CLASSID_RETAILEROPTIONS * &H10000) + 47
Public Const FID_RETAILEROPTIONS_DiscountMarginCode = (CLASSID_RETAILEROPTIONS * &H10000) + 48
Public Const FID_RETAILEROPTIONS_CustOrderDiscMarginCode = (CLASSID_RETAILEROPTIONS * &H10000) + 49
Public Const FID_RETAILEROPTIONS_MinimumSQMBeforeDiscount = (CLASSID_RETAILEROPTIONS * &H10000) + 50
Public Const FID_RETAILEROPTIONS_AutoSQMDiscount = (CLASSID_RETAILEROPTIONS * &H10000) + 51
Public Const FID_RETAILEROPTIONS_SuggestedMinimumDiscount = (CLASSID_RETAILEROPTIONS * &H10000) + 52
Public Const FID_RETAILEROPTIONS_MinimumOrderValueDeposit = (CLASSID_RETAILEROPTIONS * &H10000) + 53
Public Const FID_RETAILEROPTIONS_NominalCodes = (CLASSID_RETAILEROPTIONS * &H10000) + 54
Public Const FID_RETAILEROPTIONS_END_OF_STATIC = FID_RETAILEROPTIONS_NominalCodes

Public Const FID_SYSTEMOPTIONS_ID = (CLASSID_SYSTEMOPTIONS * &H10000) + 1
Public Const FID_SYSTEMOPTIONS_StoreNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 2
Public Const FID_SYSTEMOPTIONS_StoreName = (CLASSID_SYSTEMOPTIONS * &H10000) + 3
Public Const FID_SYSTEMOPTIONS_HeadOfficeName = (CLASSID_SYSTEMOPTIONS * &H10000) + 4
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress1 = (CLASSID_SYSTEMOPTIONS * &H10000) + 5
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress2 = (CLASSID_SYSTEMOPTIONS * &H10000) + 6
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress3 = (CLASSID_SYSTEMOPTIONS * &H10000) + 7
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress4 = (CLASSID_SYSTEMOPTIONS * &H10000) + 8
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress5 = (CLASSID_SYSTEMOPTIONS * &H10000) + 9
Public Const FID_SYSTEMOPTIONS_HeadOfficePostCode = (CLASSID_SYSTEMOPTIONS * &H10000) + 10
Public Const FID_SYSTEMOPTIONS_HeadOfficePhoneNo = (CLASSID_SYSTEMOPTIONS * &H10000) + 11
Public Const FID_SYSTEMOPTIONS_HeadOfficeFaxNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 12
Public Const FID_SYSTEMOPTIONS_VATNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 13
Public Const FID_SYSTEMOPTIONS_MasterOutletNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 14
Public Const FID_SYSTEMOPTIONS_HighestValidFunction = (CLASSID_SYSTEMOPTIONS * &H10000) + 15
Public Const FID_SYSTEMOPTIONS_MultipleSignOns = (CLASSID_SYSTEMOPTIONS * &H10000) + 16
Public Const FID_SYSTEMOPTIONS_PasswordValidFor = (CLASSID_SYSTEMOPTIONS * &H10000) + 17
Public Const FID_SYSTEMOPTIONS_AuthorisationValidFor = (CLASSID_SYSTEMOPTIONS * &H10000) + 18
Public Const FID_SYSTEMOPTIONS_TrackStockOnHand = (CLASSID_SYSTEMOPTIONS * &H10000) + 19
Public Const FID_SYSTEMOPTIONS_UseOtherInvoiceAddress = (CLASSID_SYSTEMOPTIONS * &H10000) + 20
Public Const FID_SYSTEMOPTIONS_CostFigureAccessLevel = (CLASSID_SYSTEMOPTIONS * &H10000) + 21
Public Const FID_SYSTEMOPTIONS_AddressStyle = (CLASSID_SYSTEMOPTIONS * &H10000) + 22
Public Const FID_SYSTEMOPTIONS_PICDayCode = (CLASSID_SYSTEMOPTIONS * &H10000) + 23
Public Const FID_SYSTEMOPTIONS_PICUseHandhelds = (CLASSID_SYSTEMOPTIONS * &H10000) + 24
Public Const FID_SYSTEMOPTIONS_PICLastNoOfItems = (CLASSID_SYSTEMOPTIONS * &H10000) + 25
Public Const FID_SYSTEMOPTIONS_PICSampleFrequency = (CLASSID_SYSTEMOPTIONS * &H10000) + 26
Public Const FID_SYSTEMOPTIONS_AutoApplyPriceChanges = (CLASSID_SYSTEMOPTIONS * &H10000) + 27
Public Const FID_SYSTEMOPTIONS_AutoApplyNoDays = (CLASSID_SYSTEMOPTIONS * &H10000) + 28
Public Const FID_SYSTEMOPTIONS_AutoPrintShelfLabels = (CLASSID_SYSTEMOPTIONS * &H10000) + 29
Public Const FID_SYSTEMOPTIONS_BulkUpdates = (CLASSID_SYSTEMOPTIONS * &H10000) + 30
Public Const FID_SYSTEMOPTIONS_LocalPriceingAllowed = (CLASSID_SYSTEMOPTIONS * &H10000) + 31
Public Const FID_SYSTEMOPTIONS_KundenPassCardType = (CLASSID_SYSTEMOPTIONS * &H10000) + 32
Public Const FID_SYSTEMOPTIONS_KundenPassCompanyGroup = (CLASSID_SYSTEMOPTIONS * &H10000) + 33
Public Const FID_SYSTEMOPTIONS_TillLocalDrive = (CLASSID_SYSTEMOPTIONS * &H10000) + 34
Public Const FID_SYSTEMOPTIONS_TillRemoteDrive = (CLASSID_SYSTEMOPTIONS * &H10000) + 35
Public Const FID_SYSTEMOPTIONS_TillPathName = (CLASSID_SYSTEMOPTIONS * &H10000) + 36
Public Const FID_SYSTEMOPTIONS_NoCopiesPricingDocs = (CLASSID_SYSTEMOPTIONS * &H10000) + 37
Public Const FID_SYSTEMOPTIONS_StoreMaintMinShelf = (CLASSID_SYSTEMOPTIONS * &H10000) + 38
Public Const FID_SYSTEMOPTIONS_StoreMaintMaxShelf = (CLASSID_SYSTEMOPTIONS * &H10000) + 39
Public Const FID_SYSTEMOPTIONS_MaximumShelfMultiplier = (CLASSID_SYSTEMOPTIONS * &H10000) + 40
Public Const FID_SYSTEMOPTIONS_AllowReceiptMaint = (CLASSID_SYSTEMOPTIONS * &H10000) + 41
Public Const FID_SYSTEMOPTIONS_ReceiptMaintGrace = (CLASSID_SYSTEMOPTIONS * &H10000) + 42
Public Const FID_SYSTEMOPTIONS_Division = (CLASSID_SYSTEMOPTIONS * &H10000) + 43
Public Const FID_SYSTEMOPTIONS_TimesheetAuthorisationCode = (CLASSID_SYSTEMOPTIONS * &H10000) + 44
Public Const FID_SYSTEMOPTIONS_LocalCurrencySymbol = (CLASSID_SYSTEMOPTIONS * &H10000) + 45
Public Const FID_SYSTEMOPTIONS_EuroCurrencySymbol = (CLASSID_SYSTEMOPTIONS * &H10000) + 46
Public Const FID_SYSTEMOPTIONS_EuroBased = (CLASSID_SYSTEMOPTIONS * &H10000) + 47
Public Const FID_SYSTEMOPTIONS_StoreManagerPassword = (CLASSID_SYSTEMOPTIONS * &H10000) + 48
Public Const FID_SYSTEMOPTIONS_END_OF_STATIC = FID_SYSTEMOPTIONS_StoreManagerPassword

Public Const FID_DELNOTENARRATIVE_StoreNumber = (CLASSID_DELNOTENARRATIVE * &H10000) + 1
Public Const FID_DELNOTENARRATIVE_DocumentNumber = (CLASSID_DELNOTENARRATIVE * &H10000) + 2
Public Const FID_DELNOTENARRATIVE_PartCode = (CLASSID_DELNOTENARRATIVE * &H10000) + 3
Public Const FID_DELNOTENARRATIVE_SequenceNumber = (CLASSID_DELNOTENARRATIVE * &H10000) + 4
Public Const FID_DELNOTENARRATIVE_NarrativeText1 = (CLASSID_DELNOTENARRATIVE * &H10000) + 5
Public Const FID_DELNOTENARRATIVE_NarrativeText2 = (CLASSID_DELNOTENARRATIVE * &H10000) + 6
Public Const FID_DELNOTENARRATIVE_END_OF_STATIC = FID_DELNOTENARRATIVE_NarrativeText2

Public Const FID_COUNTEDSKU_PartCode = (CLASSID_COUNTEDSKU * &H10000) + 1
Public Const FID_COUNTEDSKU_NumberCounted = (CLASSID_COUNTEDSKU * &H10000) + 2
Public Const FID_COUNTEDSKU_DisplayCounted = (CLASSID_COUNTEDSKU * &H10000) + 3
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_COUNTEDSKU_END_OF_STATIC = FID_COUNTEDSKU_DisplayCounted

Public Const FID_RETURNNOTE_DocNo = (CLASSID_RETURNNOTE * &H10000) + 1
Public Const FID_RETURNNOTE_DocType = (CLASSID_RETURNNOTE * &H10000) + 2
Public Const FID_RETURNNOTE_Value = (CLASSID_RETURNNOTE * &H10000) + 3
Public Const FID_RETURNNOTE_Cost = (CLASSID_RETURNNOTE * &H10000) + 4
Public Const FID_RETURNNOTE_DelNoteDate = (CLASSID_RETURNNOTE * &H10000) + 5
Public Const FID_RETURNNOTE_Comm = (CLASSID_RETURNNOTE * &H10000) + 6
Public Const FID_RETURNNOTE_OrigDocKey = (CLASSID_RETURNNOTE * &H10000) + 7
Public Const FID_RETURNNOTE_EnteredBy = (CLASSID_RETURNNOTE * &H10000) + 8
Public Const FID_RETURNNOTE_Comment = (CLASSID_RETURNNOTE * &H10000) + 9
Public Const FID_RETURNNOTE_SupplierNumber = (CLASSID_RETURNNOTE * &H10000) + 10
Public Const FID_RETURNNOTE_ReturnDate = (CLASSID_RETURNNOTE * &H10000) + 11
Public Const FID_RETURNNOTE_OrigPONumber = (CLASSID_RETURNNOTE * &H10000) + 12
Public Const FID_RETURNNOTE_SupplierReturnNumber = (CLASSID_RETURNNOTE * &H10000) + 13
Public Const FID_RETURNNOTE_InvCrnMatched = (CLASSID_RETURNNOTE * &H10000) + 14
Public Const FID_RETURNNOTE_InvCrnDate = (CLASSID_RETURNNOTE * &H10000) + 15
Public Const FID_RETURNNOTE_InvCrnNumber = (CLASSID_RETURNNOTE * &H10000) + 16
Public Const FID_RETURNNOTE_VarianceValue = (CLASSID_RETURNNOTE * &H10000) + 17
Public Const FID_RETURNNOTE_InvoiceValue = (CLASSID_RETURNNOTE * &H10000) + 18
Public Const FID_RETURNNOTE_InvoiceVAT = (CLASSID_RETURNNOTE * &H10000) + 19
Public Const FID_RETURNNOTE_MatchingComment = (CLASSID_RETURNNOTE * &H10000) + 20
Public Const FID_RETURNNOTE_MatchingDate = (CLASSID_RETURNNOTE * &H10000) + 21
Public Const FID_RETURNNOTE_ReceiptDate = (CLASSID_RETURNNOTE * &H10000) + 22
Public Const FID_RETURNNOTE_RestockCharge = (CLASSID_RETURNNOTE * &H10000) + 23
Public Const FID_RETURNNOTE_Commit = (CLASSID_RETURNNOTE * &H10000) + 24
Public Const FID_RETURNNOTE_CollectionNoteNumber = (CLASSID_RETURNNOTE * &H10000) + 25
Public Const FID_RETURNNOTE_OriginalDelNoteDRL = (CLASSID_RETURNNOTE * &H10000) + 26
Public Const FID_RETURNNOTE_SecondarySupplierNumber = (CLASSID_RETURNNOTE * &H10000) + 27
Public Const FID_RETURNNOTE_ShortageNote = (CLASSID_RETURNNOTE * &H10000) + 28
Public Const FID_RETURNNOTE_END_OF_STATIC = FID_RETURNNOTE_ShortageNote

Public Const FID_RETURNNOTELINE_RetNoteKey = (CLASSID_RETURNNOTELINE * &H10000) + 1
Public Const FID_RETURNNOTELINE_LineNo = (CLASSID_RETURNNOTELINE * &H10000) + 2
Public Const FID_RETURNNOTELINE_PartCode = (CLASSID_RETURNNOTELINE * &H10000) + 3
Public Const FID_RETURNNOTELINE_PartCodeAlphaCode = (CLASSID_RETURNNOTELINE * &H10000) + 4
Public Const FID_RETURNNOTELINE_OrderQty = (CLASSID_RETURNNOTELINE * &H10000) + 5
Public Const FID_RETURNNOTELINE_ReturnedQty = (CLASSID_RETURNNOTELINE * &H10000) + 6
Public Const FID_RETURNNOTELINE_OrderPrice = (CLASSID_RETURNNOTELINE * &H10000) + 7
Public Const FID_RETURNNOTELINE_BlanketDiscDesc = (CLASSID_RETURNNOTELINE * &H10000) + 8
Public Const FID_RETURNNOTELINE_BlanketPartCode = (CLASSID_RETURNNOTELINE * &H10000) + 9
Public Const FID_RETURNNOTELINE_CurrentPrice = (CLASSID_RETURNNOTELINE * &H10000) + 10
Public Const FID_RETURNNOTELINE_CurrentCost = (CLASSID_RETURNNOTELINE * &H10000) + 11
Public Const FID_RETURNNOTELINE_IBTQuantity = (CLASSID_RETURNNOTELINE * &H10000) + 12
Public Const FID_RETURNNOTELINE_ReturnQty = (CLASSID_RETURNNOTELINE * &H10000) + 13
Public Const FID_RETURNNOTELINE_ToFollowQty = (CLASSID_RETURNNOTELINE * &H10000) + 14
Public Const FID_RETURNNOTELINE_ReturnReasonCode = (CLASSID_RETURNNOTELINE * &H10000) + 15
Public Const FID_RETURNNOTELINE_POLineNumber = (CLASSID_RETURNNOTELINE * &H10000) + 16
Public Const FID_RETURNNOTELINE_Checked = (CLASSID_RETURNNOTELINE * &H10000) + 17
Public Const FID_RETURNNOTELINE_SinglesSellingUnit = (CLASSID_RETURNNOTELINE * &H10000) + 18
Public Const FID_RETURNNOTELINE_Narrative = (CLASSID_RETURNNOTELINE * &H10000) + 19
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_RETURNNOTELINE_END_OF_STATIC = FID_RETURNNOTELINE_SinglesSellingUnit

Public Const FID_SALESTOTAL_SaleTotalKey = (CLASSID_SALESTOTAL * &H10000) + 1
Public Const FID_SALESTOTAL_FloatAmount = (CLASSID_SALESTOTAL * &H10000) + 2
Public Const FID_SALESTOTAL_PickUpAmount = (CLASSID_SALESTOTAL * &H10000) + 3
Public Const FID_SALESTOTAL_TransactionCount = (CLASSID_SALESTOTAL * &H10000) + 4
Public Const FID_SALESTOTAL_TranLastTime = (CLASSID_SALESTOTAL * &H10000) + 5
Public Const FID_SALESTOTAL_TransactionValue = (CLASSID_SALESTOTAL * &H10000) + 6
Public Const FID_SALESTOTAL_TenderTypesCount = (CLASSID_SALESTOTAL * &H10000) + 7
Public Const FID_SALESTOTAL_TenderTypesValue = (CLASSID_SALESTOTAL * &H10000) + 8
Public Const FID_SALESTOTAL_END_OF_STATIC = FID_SALESTOTAL_TenderTypesValue

Public Const FID_TILLTOTALS_TillNumber = (CLASSID_TILLTOTALS * &H10000) + 1
Public Const FID_TILLTOTALS_SaleTotalKey = (CLASSID_TILLTOTALS * &H10000) + 2
Public Const FID_TILLTOTALS_CashierNumber = (CLASSID_TILLTOTALS * &H10000) + 3
Public Const FID_TILLTOTALS_PreviousCashierNumber = (CLASSID_TILLTOTALS * &H10000) + 4
Public Const FID_TILLTOTALS_TimeLastSignedOff = (CLASSID_TILLTOTALS * &H10000) + 5
Public Const FID_TILLTOTALS_TransactionCount = (CLASSID_TILLTOTALS * &H10000) + 6
Public Const FID_TILLTOTALS_TransactionValues = (CLASSID_TILLTOTALS * &H10000) + 7
Public Const FID_TILLTOTALS_CurrentTransactionNumber = (CLASSID_TILLTOTALS * &H10000) + 8
Public Const FID_TILLTOTALS_TillZReadDone = (CLASSID_TILLTOTALS * &H10000) + 9
Public Const FID_TILLTOTALS_END_OF_STATIC = FID_TILLTOTALS_TillZReadDone

Public Const FID_CASHIERTOTALS_CashierNumber = (CLASSID_CASHIERTOTALS * &H10000) + 1
Public Const FID_CASHIERTOTALS_SalesTotalKey = (CLASSID_CASHIERTOTALS * &H10000) + 2
Public Const FID_CASHIERTOTALS_CurrentTillNumber = (CLASSID_CASHIERTOTALS * &H10000) + 3
Public Const FID_CASHIERTOTALS_CashierName = (CLASSID_CASHIERTOTALS * &H10000) + 4
Public Const FID_CASHIERTOTALS_SecurityCode = (CLASSID_CASHIERTOTALS * &H10000) + 5
Public Const FID_CASHIERTOTALS_TransactionCount = (CLASSID_CASHIERTOTALS * &H10000) + 6
Public Const FID_CASHIERTOTALS_TransactionValue = (CLASSID_CASHIERTOTALS * &H10000) + 7
Public Const FID_CASHIERTOTALS_END_OF_STATIC = FID_CASHIERTOTALS_TransactionValue

Public Const FID_ISTINEDIHEADER_SourceStoreNumber = (CLASSID_ISTINEDIHEADER * &H10000) + 1
Public Const FID_ISTINEDIHEADER_ISTOutNumber = (CLASSID_ISTINEDIHEADER * &H10000) + 2
Public Const FID_ISTINEDIHEADER_SentDate = (CLASSID_ISTINEDIHEADER * &H10000) + 3
Public Const FID_ISTINEDIHEADER_ReceivedDate = (CLASSID_ISTINEDIHEADER * &H10000) + 4
Public Const FID_ISTINEDIHEADER_ReceiptDate = (CLASSID_ISTINEDIHEADER * &H10000) + 5
Public Const FID_ISTINEDIHEADER_TotalQuantity = (CLASSID_ISTINEDIHEADER * &H10000) + 6
Public Const FID_ISTINEDIHEADER_TotalValue = (CLASSID_ISTINEDIHEADER * &H10000) + 7
Public Const FID_ISTINEDIHEADER_TotalCost = (CLASSID_ISTINEDIHEADER * &H10000) + 8
Public Const FID_ISTINEDIHEADER_Received = (CLASSID_ISTINEDIHEADER * &H10000) + 9
Public Const FID_ISTINEDIHEADER_Cancelled = (CLASSID_ISTINEDIHEADER * &H10000) + 10
Public Const FID_ISTINEDIHEADER_ReceivedBy = (CLASSID_ISTINEDIHEADER * &H10000) + 11
Public Const FID_ISTINEDIHEADER_ISTInQuantity = (CLASSID_ISTINEDIHEADER * &H10000) + 12
Public Const FID_ISTINEDIHEADER_ISTOutCancelled = (CLASSID_ISTINEDIHEADER * &H10000) + 13
Public Const FID_ISTINEDIHEADER_ISTInNumber = (CLASSID_ISTINEDIHEADER * &H10000) + 14
Public Const FID_ISTINEDIHEADER_END_OF_STATIC = FID_ISTINEDIHEADER_ISTInNumber

Public Const FID_ISTINEDILINE_SourceStoreNumber = (CLASSID_ISTINEDILINE * &H10000) + 1
Public Const FID_ISTINEDILINE_ISTOutNumber = (CLASSID_ISTINEDILINE * &H10000) + 2
Public Const FID_ISTINEDILINE_PartCode = (CLASSID_ISTINEDILINE * &H10000) + 3
Public Const FID_ISTINEDILINE_LineNumber = (CLASSID_ISTINEDILINE * &H10000) + 4
Public Const FID_ISTINEDILINE_Quantity = (CLASSID_ISTINEDILINE * &H10000) + 5
Public Const FID_ISTINEDILINE_Price = (CLASSID_ISTINEDILINE * &H10000) + 6
Public Const FID_ISTINEDILINE_Cost = (CLASSID_ISTINEDILINE * &H10000) + 7
Public Const FID_ISTINEDILINE_QuantityPerSellingUnit = (CLASSID_ISTINEDILINE * &H10000) + 8
Public Const FID_ISTINEDILINE_QuantityReceived = (CLASSID_ISTINEDILINE * &H10000) + 9
Public Const FID_ISTINEDILINE_END_OF_STATIC = FID_ISTINEDILINE_QuantityReceived

Public Const FID_PURCHASECONSLINE_PurchaseOrderKey = (CLASSID_PURCHASECONSLINE * &H10000) + 1
Public Const FID_PURCHASECONSLINE_LineNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 2
Public Const FID_PURCHASECONSLINE_PurchaseOrderNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 3
Public Const FID_PURCHASECONSLINE_PurchaseOrderLineNo = (CLASSID_PURCHASECONSLINE * &H10000) + 4
Public Const FID_PURCHASECONSLINE_CustOrderNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 5
Public Const FID_PURCHASECONSLINE_CustOrderLineNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 6
Public Const FID_PURCHASECONSLINE_PartCode = (CLASSID_PURCHASECONSLINE * &H10000) + 7
Public Const FID_PURCHASECONSLINE_ProductCode = (CLASSID_PURCHASECONSLINE * &H10000) + 8
Public Const FID_PURCHASECONSLINE_Quantity = (CLASSID_PURCHASECONSLINE * &H10000) + 9
Public Const FID_PURCHASECONSLINE_Price = (CLASSID_PURCHASECONSLINE * &H10000) + 10
Public Const FID_PURCHASECONSLINE_Cost = (CLASSID_PURCHASECONSLINE * &H10000) + 11
Public Const FID_PURCHASECONSLINE_QuantityPerUnit = (CLASSID_PURCHASECONSLINE * &H10000) + 12
Public Const FID_PURCHASECONSLINE_Complete = (CLASSID_PURCHASECONSLINE * &H10000) + 13
Public Const FID_PURCHASECONSLINE_OrderDate = (CLASSID_PURCHASECONSLINE * &H10000) + 14
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_PURCHASECONSLINE_END_OF_STATIC = FID_PURCHASECONSLINE_OrderDate

Public Const FID_INVENTORYINFO_PartCode = (CLASSID_INVENTORYINFO * &H10000) + 1
Public Const FID_INVENTORYINFO_SequenceNumber = (CLASSID_INVENTORYINFO * &H10000) + 2
Public Const FID_INVENTORYINFO_TextBody = (CLASSID_INVENTORYINFO * &H10000) + 3
'---------- Insert new class IDs immediately before here,
'---------- incrementing the number and making CLASSID_END_OF_STATIC the same
Public Const FID_INVENTORYINFO_END_OF_STATIC = FID_INVENTORYINFO_TextBody

Public Const FID_TENDERTYPES_Key = (CLASSID_TENDERTYPES * &H10000) + 1
Public Const FID_TENDERTYPES_Description = (CLASSID_TENDERTYPES * &H10000) + 2
Public Const FID_TENDERTYPES_TenderType = (CLASSID_TENDERTYPES * &H10000) + 3
Public Const FID_TENDERTYPES_DisplayOrder = (CLASSID_TENDERTYPES * &H10000) + 4
Public Const FID_TENDERTYPES_OverTenderAllowed = (CLASSID_TENDERTYPES * &H10000) + 5
Public Const FID_TENDERTYPES_CaptureCreditCard = (CLASSID_TENDERTYPES * &H10000) + 6
Public Const FID_TENDERTYPES_UseEFTPOS = (CLASSID_TENDERTYPES * &H10000) + 7
Public Const FID_TENDERTYPES_OpenCashDrawer = (CLASSID_TENDERTYPES * &H10000) + 8
Public Const FID_TENDERTYPES_DefaultRemainingAmount = (CLASSID_TENDERTYPES * &H10000) + 9
Public Const FID_TENDERTYPES_END_OF_STATIC = FID_TENDERTYPES_DefaultRemainingAmount

Public Const FID_STOCKADJREASON_AdjustmentNo = (CLASSID_STOCKADJREASON * &H10000) + 1
Public Const FID_STOCKADJREASON_AdjustmentCode = (CLASSID_STOCKADJREASON * &H10000) + 2
Public Const FID_STOCKADJREASON_Description = (CLASSID_STOCKADJREASON * &H10000) + 3
Public Const FID_STOCKADJREASON_END_OF_STATIC = FID_STOCKADJREASON_Description

Public Const FID_ACTIVITYLOG_Key = (CLASSID_ACTIVITYLOG * &H10000) + 1
Public Const FID_ACTIVITYLOG_SystemDate = (CLASSID_ACTIVITYLOG * &H10000) + 2
Public Const FID_ACTIVITYLOG_EmployeeID = (CLASSID_ACTIVITYLOG * &H10000) + 3
Public Const FID_ACTIVITYLOG_WorkstationID = (CLASSID_ACTIVITYLOG * &H10000) + 4
Public Const FID_ACTIVITYLOG_GroupID = (CLASSID_ACTIVITYLOG * &H10000) + 5
Public Const FID_ACTIVITYLOG_ProgramID = (CLASSID_ACTIVITYLOG * &H10000) + 6
Public Const FID_ACTIVITYLOG_LoggingIn = (CLASSID_ACTIVITYLOG * &H10000) + 7
Public Const FID_ACTIVITYLOG_AutoLoggedOut = (CLASSID_ACTIVITYLOG * &H10000) + 8
Public Const FID_ACTIVITYLOG_StartTime = (CLASSID_ACTIVITYLOG * &H10000) + 9
Public Const FID_ACTIVITYLOG_EndTime = (CLASSID_ACTIVITYLOG * &H10000) + 10
Public Const FID_ACTIVITYLOG_EndDate = (CLASSID_ACTIVITYLOG * &H10000) + 11
Public Const FID_ACTIVITYLOG_END_OF_STATIC = FID_ACTIVITYLOG_EndDate

Public Const FID_STORESTATUS_Key = (CLASSID_STORESTATUS * &H10000) + 1
Public Const FID_STORESTATUS_MasterPCOpened = (CLASSID_STORESTATUS * &H10000) + 2
Public Const FID_STORESTATUS_DateOpened = (CLASSID_STORESTATUS * &H10000) + 3
Public Const FID_STORESTATUS_SlavePCOpened = (CLASSID_STORESTATUS * &H10000) + 4
Public Const FID_STORESTATUS_END_OF_STATIC = FID_STORESTATUS_SlavePCOpened

Public Const FID_EDIFILECONTROL_FileCode = (CLASSID_EDIFILECONTROL * &H10000) + 1
Public Const FID_EDIFILECONTROL_Description = (CLASSID_EDIFILECONTROL * &H10000) + 2
Public Const FID_EDIFILECONTROL_LastSequenceNo = (CLASSID_EDIFILECONTROL * &H10000) + 3
Public Const FID_EDIFILECONTROL_DoneSequenceNo = (CLASSID_EDIFILECONTROL * &H10000) + 4
Public Const FID_EDIFILECONTROL_ProcessingType = (CLASSID_EDIFILECONTROL * &H10000) + 5
Public Const FID_EDIFILECONTROL_EndToEndApplies = (CLASSID_EDIFILECONTROL * &H10000) + 6
Public Const FID_EDIFILECONTROL_END_OF_STATIC = FID_EDIFILECONTROL_EndToEndApplies

Public Const FID_SALESLEDGER_EntryID = (CLASSID_SALESLEDGER * &H10000) + 1
Public Const FID_SALESLEDGER_StoreNumber = (CLASSID_SALESLEDGER * &H10000) + 2
Public Const FID_SALESLEDGER_CustomerNumber = (CLASSID_SALESLEDGER * &H10000) + 3
Public Const FID_SALESLEDGER_TransactionDate = (CLASSID_SALESLEDGER * &H10000) + 4
Public Const FID_SALESLEDGER_DeliveryDate = (CLASSID_SALESLEDGER * &H10000) + 5
Public Const FID_SALESLEDGER_PostingDate = (CLASSID_SALESLEDGER * &H10000) + 6
Public Const FID_SALESLEDGER_PaymentDueDate = (CLASSID_SALESLEDGER * &H10000) + 7
Public Const FID_SALESLEDGER_PaidDate = (CLASSID_SALESLEDGER * &H10000) + 8
Public Const FID_SALESLEDGER_PeriodStart = (CLASSID_SALESLEDGER * &H10000) + 9
Public Const FID_SALESLEDGER_DocumentNumber = (CLASSID_SALESLEDGER * &H10000) + 10
Public Const FID_SALESLEDGER_OrderNumber = (CLASSID_SALESLEDGER * &H10000) + 11
Public Const FID_SALESLEDGER_TransactionType = (CLASSID_SALESLEDGER * &H10000) + 12
Public Const FID_SALESLEDGER_TotalValue = (CLASSID_SALESLEDGER * &H10000) + 13
Public Const FID_SALESLEDGER_TotalVAT = (CLASSID_SALESLEDGER * &H10000) + 14
Public Const FID_SALESLEDGER_OutstandingValue = (CLASSID_SALESLEDGER * &H10000) + 15
Public Const FID_SALESLEDGER_Disputed = (CLASSID_SALESLEDGER * &H10000) + 16
Public Const FID_SALESLEDGER_StatementPrinted = (CLASSID_SALESLEDGER * &H10000) + 17
Public Const FID_SALESLEDGER_UserInitials = (CLASSID_SALESLEDGER * &H10000) + 18
Public Const FID_SALESLEDGER_DateCommed = (CLASSID_SALESLEDGER * &H10000) + 19
Public Const FID_SALESLEDGER_Memo1 = (CLASSID_SALESLEDGER * &H10000) + 20
Public Const FID_SALESLEDGER_Memo2 = (CLASSID_SALESLEDGER * &H10000) + 21
Public Const FID_SALESLEDGER_END_OF_STATIC = FID_SALESLEDGER_Memo2

Public Const FID_TCFMASTER_FileCode = (CLASSID_TCFMASTER * &H10000) + 1
Public Const FID_TCFMASTER_SequenceNumber = (CLASSID_TCFMASTER * &H10000) + 2
Public Const FID_TCFMASTER_SourceSequenceNumber = (CLASSID_TCFMASTER * &H10000) + 3
Public Const FID_TCFMASTER_DateCreated = (CLASSID_TCFMASTER * &H10000) + 4
Public Const FID_TCFMASTER_TransmissionDate = (CLASSID_TCFMASTER * &H10000) + 5
Public Const FID_TCFMASTER_TransmissionTime = (CLASSID_TCFMASTER * &H10000) + 6
Public Const FID_TCFMASTER_SentRecordCount = (CLASSID_TCFMASTER * &H10000) + 7
Public Const FID_TCFMASTER_ActualRecordCount = (CLASSID_TCFMASTER * &H10000) + 8
Public Const FID_TCFMASTER_EndToEnd = (CLASSID_TCFMASTER * &H10000) + 9
Public Const FID_TCFMASTER_ReadyToProcess = (CLASSID_TCFMASTER * &H10000) + 10
Public Const FID_TCFMASTER_DateReady = (CLASSID_TCFMASTER * &H10000) + 11
Public Const FID_TCFMASTER_TimeReady = (CLASSID_TCFMASTER * &H10000) + 12
Public Const FID_TCFMASTER_Processed = (CLASSID_TCFMASTER * &H10000) + 13
Public Const FID_TCFMASTER_DateProcessed = (CLASSID_TCFMASTER * &H10000) + 14
Public Const FID_TCFMASTER_TimeProcessed = (CLASSID_TCFMASTER * &H10000) + 15
Public Const FID_TCFMASTER_ErrorCode = (CLASSID_TCFMASTER * &H10000) + 16
Public Const FID_TCFMASTER_END_OF_STATIC = FID_TCFMASTER_ErrorCode

Public Const FID_CASHBALANCECONTROL_ControlDate = (CLASSID_CASHBALANCECONTROL * &H10000) + 1
Public Const FID_CASHBALANCECONTROL_DayBalanced = (CLASSID_CASHBALANCECONTROL * &H10000) + 2
Public Const FID_CASHBALANCECONTROL_CommNumber = (CLASSID_CASHBALANCECONTROL * &H10000) + 3
Public Const FID_CASHBALANCECONTROL_OtherIncome = (CLASSID_CASHBALANCECONTROL * &H10000) + 4
Public Const FID_CASHBALANCECONTROL_MiscOutgoings = (CLASSID_CASHBALANCECONTROL * &H10000) + 5
Public Const FID_CASHBALANCECONTROL_TotalDeposits = (CLASSID_CASHBALANCECONTROL * &H10000) + 6
Public Const FID_CASHBALANCECONTROL_TotalVouchers = (CLASSID_CASHBALANCECONTROL * &H10000) + 7
Public Const FID_CASHBALANCECONTROL_TotalFloats = (CLASSID_CASHBALANCECONTROL * &H10000) + 8
Public Const FID_CASHBALANCECONTROL_BankingAmount = (CLASSID_CASHBALANCECONTROL * &H10000) + 9
Public Const FID_CASHBALANCECONTROL_BankSlipNumbers = (CLASSID_CASHBALANCECONTROL * &H10000) + 10
Public Const FID_CASHBALANCECONTROL_Comment1 = (CLASSID_CASHBALANCECONTROL * &H10000) + 11
Public Const FID_CASHBALANCECONTROL_Comment2 = (CLASSID_CASHBALANCECONTROL * &H10000) + 12
Public Const FID_CASHBALANCECONTROL_Comment3 = (CLASSID_CASHBALANCECONTROL * &H10000) + 13
Public Const FID_CASHBALANCECONTROL_END_OF_STATIC = FID_CASHBALANCECONTROL_Comment3


Public Const FID_CASHBALANCESUMMARY_Date = (CLASSID_CASHBALANCESUMMARY * &H10000) + 1
Public Const FID_CASHBALANCESUMMARY_END_OF_STATIC = FID_CASHBALANCESUMMARY_Date

' The enComparator lists the types of comparison supported by the IField interface.
Public Enum enComparator
    CMP_INVALID = 0     ' Initial value
    CMP_EQUAL = 1
    CMP_NOTEQUAL = 2
    CMP_LESSTHAN = 3
    CMP_GREATERTHAN = 4
    CMP_COMPARE = 5     ' !!!! For String this is a compare no case !!!!!!
    CMP_SELECTALL = 6   ' ie. *
    CMP_SELECTLIST = 7  ' ie. any of list of values
    CMP_LIKE = 8        ' ie. with leading or trailing % as wildcard
    CMP_LESSEQUALTHAN = 9
    CMP_GREATEREQUALTHAN = 10
    CMP_INRANGE = 11    ' ie. Not less than first value or greater than second
End Enum

' The enBoInterfaceType lists the types of interface supported by IBo.Interface()
Public Enum enBoInterfaceType
    BO_INTERFACE_NATIVE = 0     ' Default, native interface
    BO_INTERFACE_IBO = 1        ' The IBo interface
End Enum

' The enBoInterfaceType lists the types of interface supported by IBo.Interface()
Public Enum enStockTakeState
    STOCKTAKE_UNSTARTED = 0
    STOCKTAKE_PRECOUNT = 1
    STOCKTAKE_COUNTING = 2
    STOCKTAKE_POSTCOUNT = 3
    STOCKTAKE_COMPLETE = 4
End Enum

Public Const AGG_MAX As Long = 1
Public Const AGG_MIN As Long = 2
Public Const AGG_COUNT As Long = 3
Public Const AGG_AVG As Long = 4
Public Const AGG_SUM As Long = 5
Public Const AGG_DIST As Long = 6

Public Const PRM_TXOUT_FOLDER As Long = 150
Public Const PRM_TXOUT_BACKUP_FOLDER As Long = 151
Public Const PRM_TXIN_FOLDER As Long = 152
Public Const PRM_TXIN_BACKUP_FOLDER As Long = 153


