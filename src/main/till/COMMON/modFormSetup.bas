Attribute VB_Name = "modFormSetup"
Option Explicit

Private Const MODULE_NAME  As String = "modFormSetup"

'   Used to disable to forms close button.
Private Declare Function GetSystemMenu Lib "user32" _
        (ByVal hwnd As Long, _
        ByVal bRevert As Long) As Long
Private Declare Function RemoveMenu Lib "user32" _
        (ByVal hMenu As Long, _
        ByVal nPosition As Long, _
        ByVal wFlags As Long) As Long
Private Declare Function DrawMenuBar Lib "user32" _
        (ByVal hwnd As Long) As Long
Private Const MF_BYPOSITION = &H400&

'   Disable the forms close button and removes the system menu close option.
Public Function DisableCloseButton(frmForm As Form) As Boolean

Const PROCEDURE_NAME As String = "DisableCloseButton"

Dim lHndSysMenu As Long
Dim lAns1 As Long
Dim lAns2 As Long
Dim lAns3 As Long
    
    On Error GoTo DisableCloseButton_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Disable Close Button")
    
    lHndSysMenu = GetSystemMenu(frmForm.hwnd, 0)
    
'   Remove close button and system menu option.
    lAns1 = RemoveMenu(lHndSysMenu, 6, MF_BYPOSITION)
    
'   Remove seperator bar
    lAns2 = RemoveMenu(lHndSysMenu, 5, MF_BYPOSITION)
    
    lAns3 = DrawMenuBar(lHndSysMenu)
    
'   Return True if both calls were successful
    DisableCloseButton = ((lAns1 <> 0) And (lAns2 <> 0) And (lAns3 <> 0))

    Exit Function
    
DisableCloseButton_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Function
