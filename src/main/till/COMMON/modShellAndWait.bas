Attribute VB_Name = "ShellAndWait"
Option Explicit

Public Enum ShowStyle
    SW_HIDE = 0
    SW_SHOWNORMAL = 1
    SW_SHOWMINIMIZED = 2
    SW_SHOWMAXIMIZED = 3
    SW_SHOWNOACTIVATE = 4
    SW_SHOW = 5
    SW_MINIMIZE = 6
    SW_SHOWMINNOACTIVE = 7
    SW_SHOWNA = 8
    SW_RESTORE = 9
    SW_SHOWDEFAULT = 10
    SW_MAX = 10
End Enum

Private Type STARTUPINFO
   cb As Long
   lpReserved As String
   lpDesktop As String
   lpTitle As String
   dwX As Long
   dwY As Long
   dwXSize As Long
   dwYSize As Long
   dwXCountChars As Long
   dwYCountChars As Long
   dwFillAttribute As Long
   dwFlags As Long
   wShowWindow As Integer
   cbReserved2 As Integer
   lpReserved2 As Long
   hStdInput As Long
   hStdOutput As Long
   hStdError As Long
End Type

Private Type PROCESS_INFORMATION
   hProcess As Long
   hThread As Long
   dwProcessId As Long
   dwThreadID As Long
End Type

Private Const GW_HWNDFIRST = 0&
Private Const GW_HWNDLAST = 1&
Private Const GW_HWNDNEXT = 2&
Private Const GW_CHILD = 5&

Private Const GWL_EXSTYLE = -20&
Private Const GWL_HINSTANCE = -6&
Private Const GWL_HWNDPARENT = -8&
Private Const GWL_ID = -12&
Private Const GWL_STYLE = -16&
Private Const GWL_USERDATA = -21&
Private Const GWL_WNDPROC = -4&


Private Const PROCESS_ALL_ACCESS = &H1F0FFF


Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const Flags = SWP_NOACTIVATE Or SWP_SHOWWINDOW Or SWP_NOMOVE Or SWP_NOSIZE

Private Const WM_CLOSE = &H10

Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function CreateProcessA Lib "kernel32" (ByVal lpApplicationName As Long, ByVal lpCommandLine As String, ByVal lpProcessAttributes As Long, ByVal lpThreadAttributes As Long, ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, ByVal lpEnvironment As Long, ByVal lpCurrentDirectory As Long, lpStartupInfo As STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long

Private Const STARTF_USESHOWWINDOW = &H1
Private Const NORMAL_PRIORITY_CLASS = &H20&
Private Const INFINITE = -1&
Private Const WAIT_ABANDONED = &H80&
Private Const WAIT_OBJECT_0 = 0
Private Const WAIT_TIMEOUT = &H102&

Private Const TIMEOUTINTERVAL As Long = 5000    ' 5 Seconds

Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Private Declare Function GetWindow Lib "user32" (ByVal hWnd As Long, ByVal wCmd As Long) As Long
Private Declare Function GetDesktopWindow Lib "user32" () As Long
Private Declare Function DestroyWindow Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hWnd As Long, lpdwProcessId As Long) As Long


Private Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, lParam As Any) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

Sub CloseProcess(ByVal strCaption As String)
   
Dim lngPID          As Long
Dim lngRtn          As Long
Dim lngWndHwnd      As Long
Dim lngOpenProcHwnd As Long

    lngWndHwnd = GetWindowFromTitle(strCaption)
    lngRtn = GetWindowThreadProcessId(lngWndHwnd, lngPID)
    If (lngPID > 0) Then
        If GetWindowLong(lngWndHwnd, GWL_HWNDPARENT) = 0 Then
            Call PostMessage(lngWndHwnd, WM_CLOSE, 0&, ByVal 0&)
        End If
    End If
   
End Sub


'   Changed 06/06/2005 by Dave Formoy
'   Changed to allow an optional Timeout value being used when waiting for the process to complete.
Public Sub ShellWait(cmdline As String, StartFlags As ShowStyle, Optional lngTimeout As Long)
   
Dim proc  As PROCESS_INFORMATION
Dim start As STARTUPINFO
Dim ret   As Long
Dim lngCounter As Long
   
    ' Initialize the STARTUPINFO structure:
    start.dwFlags = STARTF_USESHOWWINDOW
    start.wShowWindow = StartFlags
    start.cb = Len(start)
    
    ' Start the shelled application:
    ret = CreateProcessA(0, cmdline, 0, 0, 1, NORMAL_PRIORITY_CLASS, 0, 0, start, proc)
    
    If (lngTimeout = 0) Then
'       Wait for the shelled application to finish:
        ret = WaitForSingleObject(proc.hProcess, INFINITE)
        ret = CloseHandle(proc.hProcess)
        ret = CloseHandle(proc.hThread)
    Else
'       Wait for the shelled application to finish, if timeout then close the file and exit.
        Do Until (lngCounter = lngTimeout)
            ret = WaitForSingleObject(proc.hProcess, TIMEOUTINTERVAL)
            If (ret = WAIT_OBJECT_0) Then
'               Finished processing so close file and exit.
                ret = CloseHandle(proc.hProcess)
                ret = CloseHandle(proc.hThread)

                Exit Sub
            End If
            lngCounter = lngCounter + 1
        Loop
        ret = CloseHandle(proc.hProcess)
        ret = CloseHandle(proc.hThread)
    End If
    
End Sub

Function ProcessRunning(ByVal strCaption As String) As Boolean

Dim lngWndHwnd      As Long

   lngWndHwnd = GetWindowFromTitle(strCaption)
   ProcessRunning = False
   If lngWndHwnd > 0 Then ProcessRunning = True

End Function

Sub KillProcess(ByVal strCaption As String)
   
Dim lngPID          As Long
Dim lngRtn          As Long
Dim lngWndHwnd      As Long
Dim lngOpenProcHwnd As Long

   lngWndHwnd = GetWindowFromTitle(strCaption)
   lngRtn = GetWindowThreadProcessId(lngWndHwnd, lngPID)
   If (lngPID > 0) Then
      lngOpenProcHwnd = OpenProcess(PROCESS_ALL_ACCESS, 0&, lngPID)
      
      If (lngOpenProcHwnd <> 0) Then
         Call KillWindow(lngWndHwnd, lngOpenProcHwnd)
         lngRtn = CloseHandle(lngOpenProcHwnd)
      End If
   End If
   
End Sub

' -------------------------------------------------
' Passed a partial or complete window title, return
'   the handle of the first title found matching
'   the passed string.
' NOTE: Case insensitive!
' -------------------------------------------------
Public Function GetWindowFromTitle(ByVal strPartialTitle As String) As Long
   
Dim strTitle        As String * 256
Dim lngHwnd         As Long
Dim lngRtn          As Long
Dim strMatch        As String
Dim strPassed       As String

   GetWindowFromTitle = 0

   strPassed = UCase$(strPartialTitle)
   lngHwnd = GetDesktopWindow()

   lngHwnd = GetWindow(lngHwnd, GW_CHILD)

   lngRtn = GetWindowText(lngHwnd, strTitle, Len(strTitle))
   On Error Resume Next
   If lngRtn = 0 Then
      strMatch = ""
   Else
      strMatch = UCase$(Mid$(strTitle, 1, lngRtn))
   End If

   ' Check to see if first child contains the passed string
   If Len(strMatch) > 0 Then
      If InStr(strPassed, strMatch) > 0 Then
         GetWindowFromTitle = lngHwnd
         Exit Function
      End If
   End If
   On Error GoTo 0

   ' Now check all the top level windows
   Do
      lngHwnd = GetWindow(lngHwnd, GW_HWNDNEXT)
      ' Get the text of the window
      strTitle = ""
      lngRtn = GetWindowText(lngHwnd, strTitle, Len(strTitle))

      On Error Resume Next
      If lngRtn = 0 Then
         strMatch = ""
      Else
         strMatch = UCase$(Mid$(strTitle, 1, lngRtn))
      End If


      ' Check to see if this window contains the passed string
      If Len(strMatch) > 0 Then
         If InStr(strMatch, strPassed) > 0 Then
            GetWindowFromTitle = lngHwnd
            Exit Function
         End If
      End If
      On Error GoTo 0

   Loop While lngHwnd <> 0

   GetWindowFromTitle = 0

End Function

Public Function KillWindow(ByVal lngInHwnd As Long, ByVal lngInProcessHwnd As Long) As Boolean
' -------------------------------------------------
' Passed a Windows handle and a Process handle, kill the process
' -------------------------------------------------
   
Dim lngRtn             As Long
Dim lngProcessHwnd     As Long
'Dim lngThreadHwnd      As Long
Dim lngExitCode        As Long
   
   lngExitCode = 0&
   
   If (lngInHwnd <> 0) Then
      If IsWindow(lngInHwnd) <> 0 Then
         lngRtn = TerminateProcess(lngInProcessHwnd, lngExitCode)
         
         If (lngRtn = 0) Then
            KillWindow = False
         Else
            KillWindow = True
         End If
      End If
   End If
   
End Function








