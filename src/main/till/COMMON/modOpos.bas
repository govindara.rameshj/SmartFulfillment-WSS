Attribute VB_Name = "modOpos"
Rem *///////////////////////////////////////////////////////////////////
Rem *///////////////////////////////////////////////////////////////////
Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OPOSALL.BAS
Rem *
Rem *   Includes all OPOS Device Classes.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 96-03-18 OPOS Release 1.01                                    CRM
Rem * 96-04-22 OPOS Release 1.1                                     CRM
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem * 98-03-06 OPOS Release 1.3                                     CRM
Rem * 99-06-18 OPOS Release 1.4 and 1.5                             CRM
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem * 01-07-15 OPOS Release 1.6                                 THH/BKS
Rem *
Rem *///////////////////////////////////////////////////////////////////
Rem *///////////////////////////////////////////////////////////////////
Rem *///////////////////////////////////////////////////////////////////



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * Opos.h
Rem *
Rem *   General header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem * 97-06-04 OPOS Release 1.2               MtD                      CRM
Rem *   Add OPOS_FOREVER.
Rem *   Add BinaryConversion values.
Rem * 98-03-06 OPOS Release 1.3                                     CRM
Rem *   Add CapPowerReporting, PowerState, and PowerNotify values.
Rem *   Add power reporting values for StatusUpdateEvent.
Rem * 00-09-24 OPOS Release 1.5                                     CRM
Rem *   Add OpenResult status values.
Rem * 00-07-18 OPOS Release 1.5                                     BKS
Rem *   Add many values and Point Card Reader Writer and
Rem *   POS Power sections.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * OPOS "State" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposSClosed& = 1
Public Const OposSIdle& = 2
Public Const OposSBusy& = 3
Public Const OposSError& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * OPOS "ResultCode" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposSuccess& = 0
Public Const OposEClosed& = 101
Public Const OposEClaimed& = 102
Public Const OposENotclaimed& = 103
Public Const OposENoservice& = 104
Public Const OposEDisabled& = 105
Public Const OposEIllegal& = 106
Public Const OposENohardware& = 107
Public Const OposEOffline& = 108
Public Const OposENoexist& = 109
Public Const OposEExists& = 110
Public Const OposEFailure& = 111
Public Const OposETimeout& = 112
Public Const OposEBusy& = 113
Public Const OposEExtended& = 114


Rem *///////////////////////////////////////////////////////////////////
Rem * OPOS "OpenResult" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const Oposopenerr& = 300

Public Const OposOrAlreadyopen& = 301
Public Const OposOrRegbadname& = 302
Public Const OposOrRegprogid& = 303
Public Const OposOrCreate& = 304
Public Const OposOrBadif& = 305
Public Const OposOrFailedopen& = 306
Public Const OposOrBadversion& = 307

Public Const Oposopenerrso& = 400

Public Const OposOrsNoport = 401
Public Const OposOrsNotsupported = 402
Public Const OposOrsConfig = 403
Public Const OposOrsSpecific = 450


Rem *///////////////////////////////////////////////////////////////////
Rem * OPOS "BinaryConversion" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposBcNone& = 0
Public Const OposBcNibble& = 1
Public Const OposBcDecimal& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "CheckHealth" Method: "Level" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposChInternal& = 1
Public Const OposChExternal& = 2
Public Const OposChInteractive& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * OPOS "CapPowerReporting", "PowerState", "PowerNotify" Property
Rem *   Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposPrNone& = 0
Public Const OposPrStandard& = 1
Public Const OposPrAdvanced& = 2

Public Const OposPnDisabled& = 0
Public Const OposPnEnabled& = 1

Public Const OposPsUnknown& = 2000
Public Const OposPsOnline& = 2001
Public Const OposPsOff& = 2002
Public Const OposPsOffline& = 2003
Public Const OposPsOffOffline& = 2004


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorEvent" Event: "ErrorLocus" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposElOutput& = 1
Public Const OposElInput& = 2
Public Const OposElInputData& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorEvent" Event: "ErrorResponse" Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposErRetry& = 11
Public Const OposErClear& = 12
Public Const OposErContinueinput& = 13


Rem *///////////////////////////////////////////////////////////////////
Rem * "StatusUpdateEvent" Event: Common "Status" Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposSuePowerOnline& = 2001
Public Const OposSuePowerOff& = 2002
Public Const OposSuePowerOffline& = 2003
Public Const OposSuePowerOffOffline& = 2004


Rem *///////////////////////////////////////////////////////////////////
Rem * General Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposForever& = -1



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposBb.h
Rem *
Rem *   Bump Bar header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 98-03-06 OPOS Release 1.3                                     BB
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "CurrentUnitID" and "UnitsOnline" Properties
Rem *  and "Units" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const BbUid1& = &H1
Public Const BbUid2& = &H2
Public Const BbUid3& = &H4
Public Const BbUid4& = &H8
Public Const BbUid5& = &H10
Public Const BbUid6& = &H20
Public Const BbUid7& = &H40
Public Const BbUid8& = &H80
Public Const BbUid9& = &H100
Public Const BbUid10& = &H200
Public Const BbUid11& = &H400
Public Const BbUid12& = &H800
Public Const BbUid13& = &H1000
Public Const BbUid14& = &H2000
Public Const BbUid15& = &H4000
Public Const BbUid16& = &H8000
Public Const BbUid17& = &H10000
Public Const BbUid18& = &H20000
Public Const BbUid19& = &H40000
Public Const BbUid20& = &H80000
Public Const BbUid21& = &H100000
Public Const BbUid22& = &H200000
Public Const BbUid23& = &H400000
Public Const BbUid24& = &H800000
Public Const BbUid25& = &H1000000
Public Const BbUid26& = &H2000000
Public Const BbUid27& = &H4000000
Public Const BbUid28& = &H8000000
Public Const BbUid29& = &H10000000
Public Const BbUid30& = &H20000000
Public Const BbUid31& = &H40000000
Public Const BbUid32& = &H80000000


Rem *///////////////////////////////////////////////////////////////////
Rem * "DataEvent" Event: "Status" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const BbDeKey& = &H1



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposCash.h
Rem *
Rem *   Cash Drawer header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem * 98-03-06 OPOS Release 1.3                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "StatusUpdateEvent" Event Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const CashSueDrawerclosed& = 0
Public Const CashSueDraweropen& = 1



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposCAT.h
Rem *
Rem *   CAT header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 98-09-23 OPOS Release 1.4                                 OPOS-J
Rem * 00-09-24 OPOS Release 1.5                                    BKS
Rem *   Add CAT_PAYMENT_DEBIT, CAT_MEDIA_UNSPECIFIED,
Rem *   CAT_MEDIA_NONDEFINE, CAT_MEDIA_CREDIT, CAT_MEDIA_DEBIT
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * Payment Condition Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const CatPaymentLump& = 10
Public Const CatPaymentBonus1& = 21
Public Const CatPaymentBonus2& = 22
Public Const CatPaymentBonus3& = 23
Public Const CatPaymentBonus4& = 24
Public Const CatPaymentBonus5& = 25
Public Const CatPaymentInstallment1& = 61
Public Const CatPaymentInstallment2& = 62
Public Const CatPaymentInstallment3& = 63
Public Const CatPaymentBonusCombination1& = 31
Public Const CatPaymentBonusCombination2& = 32
Public Const CatPaymentBonusCombination3& = 33
Public Const CatPaymentBonusCombination4& = 34
Public Const CatPaymentRevolving& = 80
Public Const CatPaymentDebit& = 110


Rem *///////////////////////////////////////////////////////////////////
Rem * Transaction Type Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const CatTransactionSales& = 10
Public Const CatTransactionVoid& = 20
Public Const CatTransactionRefund& = 21
Public Const CatTransactionVoidpresales& = 29
Public Const CatTransactionCompletion& = 30
Public Const CatTransactionPresales& = 40
Public Const CatTransactionCheckcard& = 41


Rem *///////////////////////////////////////////////////////////////////
Rem * "PaymentMedia" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const CatMediaUnspecified& = 0
Public Const CatMediaNondefine& = 0
Public Const CatMediaCredit& = 1
Public Const CatMediaDebit& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * ResultCodeExtended Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEcatCentererror& = 1
Public Const OposEcatCommanderror& = 90
Public Const OposEcatReset& = 91
Public Const OposEcatCommunicationerror& = 92
Public Const OposEcatDailylogoverflow& = 200


Rem *///////////////////////////////////////////////////////////////////
Rem * "Daily Log" Property  & Argument Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const CatDlNone& = 0                                 'None of them
Public Const CatDlReporting& = 1                            'Only Reporting
Public Const CatDlSettlement& = 2                           'Only Settlement
Public Const CatDlReportingSettlement& = 3                  'Both of them



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposChan.h
Rem *
Rem *   Cash Changer header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem * 00-09-24 OPOS Release 1.5                                  OPOS-J
Rem *   Add DepositStatus Constants.
Rem *   Add EndDeposit Constants.
Rem *   Add PauseDeposit Constants.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "DeviceStatus" and "FullStatus" Property Constants
Rem * "StatusUpdateEvent" Event Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const ChanStatusOk& = 0                         ' DeviceStatus, FullStatus

Public Const ChanStatusEmpty& = 11                     ' DeviceStatus, StatusUpdateEvent
Public Const ChanStatusNearempty& = 12                 ' DeviceStatus, StatusUpdateEvent
Public Const ChanStatusEmptyok& = 13                   ' StatusUpdateEvent

Public Const ChanStatusFull& = 21                      ' FullStatus, StatusUpdateEvent
Public Const ChanStatusNearfull& = 22                  ' FullStatus, StatusUpdateEvent
Public Const ChanStatusFullok& = 23                    ' StatusUpdateEvent

Public Const ChanStatusJam& = 31                       ' DeviceStatus, StatusUpdateEvent
Public Const ChanStatusJamok& = 32                     ' StatusUpdateEvent

Public Const ChanStatusAsync& = 91                     ' StatusUpdateEvent


Rem *///////////////////////////////////////////////////////////////////
Rem * "DepositStatus" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const ChanStatusDepositStart& = 1
Public Const ChanStatusDepositEnd& = 2
Public Const ChanStatusDepositNone& = 3
Public Const ChanStatusDepositCount& = 4
Public Const ChanStatusDepositJam& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "EndDeposit" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const ChanDepositChange& = 1
Public Const ChanDepositNochange& = 2
Public Const ChanDepositrepay& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * "PauseDeposit" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const ChanDepositPause& = 1
Public Const ChanDepositRestart& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for Cash Changer
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEchanOverdispense& = 201



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposCoin.h
Rem *
Rem *   Coin Dispenser header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "DispenserStatus" Property Constants
Rem * "StatusUpdateEvent" Event: "Data" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const CoinStatusOk& = 1
Public Const CoinStatusEmpty& = 2
Public Const CoinStatusNearempty& = 3
Public Const CoinStatusJam& = 4



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposDisp.h
Rem *
Rem *   Line Display header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem * 96-03-18 OPOS Release 1.01                                    CRM
Rem *   Add DISP_MT_INIT constant and MarqueeFormat constants.
Rem * 96-04-22 OPOS Release 1.1                                     CRM
Rem *   Add CapCharacterSet values for Kana and Kanji.
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem *   Add CapCharacterSet and CharacterSet constants for UNICODE
Rem * 01-07-15 OPOS Release 1.6                                     BKS
Rem *   Add CapCursorType, CapCustomGlyph, CapReadBack, CapReverse,
Rem *     CursorType property constants.
Rem *   Add DefineGlyph, DisplayText and DisplayTextAt parameter
Rem *     constants.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapBlink" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCbNoblink& = 0
Public Const DispCbBlinkall& = 1
Public Const DispCbBlinkeach& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapCharacterSet" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCcsNumeric& = 0
Public Const DispCcsAlpha& = 1
Public Const DispCcsAscii& = 998
Public Const DispCcsKana& = 10
Public Const DispCcsKanji& = 11
Public Const DispCcsUnicode& = 997


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapCursorType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCctNone& = &H0
Public Const DispCctFixed& = &H1
Public Const DispCctBlock& = &H2
Public Const DispCctHalfblock& = &H4
Public Const DispCctUnderline& = &H8
Public Const DispCctReverse& = &H10
Public Const DispCctOther& = &H20


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapReadBack" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCrbNone& = &H0
Public Const DispCrbSingle& = &H1


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapReverse" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCrNone& = &H0
Public Const DispCrReverseall& = &H1
Public Const DispCrReverseeach& = &H2


Rem *///////////////////////////////////////////////////////////////////
Rem * "CharacterSet" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCsUnicode& = 997
Public Const DispCsAscii& = 998
Public Const DispCsWindows& = 999


Rem *///////////////////////////////////////////////////////////////////
Rem * "CursorType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispCtNone& = 0
Public Const DispCtFixed& = 1
Public Const DispCtBlock& = 2
Public Const DispCtHalfblock& = 3
Public Const DispCtUnderline& = 4
Public Const DispCtReverse& = 5
Public Const DispCtOther& = 6


Rem *///////////////////////////////////////////////////////////////////
Rem * "MarqueeType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispMtNone& = 0
Public Const DispMtUp& = 1
Public Const DispMtDown& = 2
Public Const DispMtLeft& = 3
Public Const DispMtRight& = 4
Public Const DispMtInit& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "MarqueeFormat" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispMfWalk& = 0
Public Const DispMfPlace& = 1


Rem *///////////////////////////////////////////////////////////////////
Rem * "DefineGlyph" Method: "GlyphType" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispGtSingle& = 1


Rem *///////////////////////////////////////////////////////////////////
Rem * "DisplayText" Method: "Attribute" Property Constants
Rem * "DisplayTextAt" Method: "Attribute" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispDtNormal& = 0
Public Const DispDtBlink& = 1
Public Const DispDtReverse& = 2
Public Const DispDtBlinkReverse& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * "ScrollText" Method: "Direction" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispStUp& = 1
Public Const DispStDown& = 2
Public Const DispStLeft& = 3
Public Const DispStRight& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "SetDescriptor" Method: "Attribute" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const DispSdOff& = 0
Public Const DispSdOn& = 1
Public Const DispSdBlink& = 2



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposFptr.h
Rem *
Rem *   Fiscal Printer header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 98-03-06 OPOS Release 1.3                                     PDU
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem *   Change CountryCode constants and add code for Russia
Rem * 01-07-15 OPOS Release 1.6                                     THH
Rem *   Add values for all 1.6 added properties and method
Rem *   parameters
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "ActualCurrency" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrAcBrc& = 1
Public Const FptrAcBgl& = 2
Public Const FptrAcEur& = 3
Public Const FptrAcGrd& = 4
Public Const FptrAcHuf& = 5
Public Const FptrAcItl& = 6
Public Const FptrAcPlz& = 7
Public Const FptrAcRol& = 8
Public Const FptrAcRur& = 9
Public Const FptrAcTrl& = 10


Rem *///////////////////////////////////////////////////////////////////
Rem * "ContractorId" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrCidFirst& = 1
Public Const FptrCidSecond& = 2
Public Const FptrCidSingle& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * Fiscal Printer Station Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrSJournal& = 1
Public Const FptrSReceipt& = 2
Public Const FptrSSlip& = 4

Public Const FptrSJournalReceipt& = FptrSJournal Or FptrSReceipt


Rem *///////////////////////////////////////////////////////////////////
Rem * "CountryCode" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrCcBrazil& = 1
Public Const FptrCcGreece& = 2
Public Const FptrCcHungary& = 4
Public Const FptrCcItaly& = 8
Public Const FptrCcPoland& = 16
Public Const FptrCcTurkey& = 32
Public Const FptrCcRussia& = 64
Public Const FptrCcBulgaria& = 128
Public Const FptrCcRomania& = 256


Rem *///////////////////////////////////////////////////////////////////
Rem * "DateType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrDtConf& = 1
Public Const FptrDtEod& = 2
Public Const FptrDtReset& = 3
Public Const FptrDtRtc& = 4
Public Const FptrDtVat& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorLevel" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrElNone& = 1
Public Const FptrElRecoverable& = 2
Public Const FptrElFatal& = 3
Public Const FptrElBlocked& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorState", "PrinterState" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrPsMonitor& = 1
Public Const FptrPsFiscalReceipt& = 2
Public Const FptrPsFiscalReceiptTotal& = 3
Public Const FptrPsFiscalReceiptEnding& = 4
Public Const FptrPsFiscalDocument& = 5
Public Const FptrPsFixedOutput& = 6
Public Const FptrPsItemList& = 7
Public Const FptrPsLocked& = 8
Public Const FptrPsNonfiscal& = 9
Public Const FptrPsReport& = 10


Rem *///////////////////////////////////////////////////////////////////
Rem * "FiscalReceiptStation" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrRsReceipt& = 1
Public Const FptrRsSlip& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "FiscalReceiptType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrRtCashIn& = 1
Public Const FptrRtCashOut& = 2
Public Const FptrRtGeneric& = 3
Public Const FptrRtSales& = 4
Public Const FptrRtService& = 5
Public Const FptrRtSimpleInvoice& = 6


Rem *///////////////////////////////////////////////////////////////////
Rem * "MessageType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrMtAdvance& = 1
Public Const FptrMtAdvancePaid& = 2
Public Const FptrMtAmountToBePaid& = 3
Public Const FptrMtAmountToBePaidBack& = 4
Public Const FptrMtCard& = 5
Public Const FptrMtCardNumber& = 6
Public Const FptrMtCardType& = 7
Public Const FptrMtCash& = 8
Public Const FptrMtCashier& = 9
Public Const FptrMtCashRegisterNumber& = 10
Public Const FptrMtChange& = 11
Public Const FptrMtCheque& = 12
Public Const FptrMtClientNumber& = 13
Public Const FptrMtClientSignature& = 14
Public Const FptrMtCounterState& = 15
Public Const FptrMtCreditCard& = 16
Public Const FptrMtCurrency& = 17
Public Const FptrMtCurrencyValue& = 18
Public Const FptrMtDeposit& = 19
Public Const FptrMtDepositReturned& = 20
Public Const FptrMtDotLine& = 21
Public Const FptrMtDriverNumb& = 22
Public Const FptrMtEmptyLine& = 23
Public Const FptrMtFreeText& = 24
Public Const FptrMtFreeTextWithDayLimit& = 25
Public Const FptrMtGivenDiscount& = 26
Public Const FptrMtLocalCredit& = 27
Public Const FptrMtMileageKm& = 28
Public Const FptrMtNote& = 29
Public Const FptrMtPaid& = 30
Public Const FptrMtPayIn& = 31
Public Const FptrMtPointGranted& = 32
Public Const FptrMtPointsBonus& = 33
Public Const FptrMtPointsReceipt& = 34
Public Const FptrMtPointsTotal& = 35
Public Const FptrMtProfited& = 36
Public Const FptrMtRate& = 37
Public Const FptrMtRegisterNumb& = 38
Public Const FptrMtShiftNumber& = 39
Public Const FptrMtStateOfAnAccount& = 40
Public Const FptrMtSubscription& = 41
Public Const FptrMtTable& = 42
Public Const FptrMtThankYouForLoyalty& = 43
Public Const FptrMtTransactionNumb& = 44
Public Const FptrMtValidTo& = 45
Public Const FptrMtVoucher& = 46
Public Const FptrMtVoucherPaid& = 47
Public Const FptrMtVoucherValue& = 48
Public Const FptrMtWithDiscount& = 49
Public Const FptrMtWithoutUplift& = 50


Rem *///////////////////////////////////////////////////////////////////
Rem * "SlipSelection" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrSsFullLength& = 1
Public Const FptrSsValidation& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "TotalizerType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrTtDocument& = 1
Public Const FptrTtDay& = 2
Public Const FptrTtReceipt& = 3
Public Const FptrTtGrand& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "GetData" Method Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrGdCurrentTotal& = 1
Public Const FptrGdDailyTotal& = 2
Public Const FptrGdReceiptNumber& = 3
Public Const FptrGdRefund& = 4
Public Const FptrGdNotPaid& = 5
Public Const FptrGdMidVoid& = 6
Public Const FptrGdZReport& = 7
Public Const FptrGdGrandTotal& = 8
Public Const FptrGdPrinterId& = 9
Public Const FptrGdFirmware& = 10
Public Const FptrGdRestart& = 11
Public Const FptrGdRefundVoid& = 12
Public Const FptrGdNumbConfigBlock& = 13
Public Const FptrGdNumbCurrencyBlock& = 14
Public Const FptrGdNumbHdrBlock& = 15
Public Const FptrGdNumbResetBlock& = 16
Public Const FptrGdNumbVatBlock& = 17
Public Const FptrGdFiscalDoc& = 18
Public Const FptrGdFiscalDocVoid& = 19
Public Const FptrGdFiscalRec& = 20
Public Const FptrGdFiscalRecVoid& = 21
Public Const FptrGdNonfiscalDoc& = 22
Public Const FptrGdNonfiscalDocVoid& = 23
Public Const FptrGdNonfiscalRec& = 24
Public Const FptrGdSimpInvoice& = 25
Public Const FptrGdTender& = 26
Public Const FptrGdLinecount& = 27
Public Const FptrGdDescriptionLength& = 28

Public Const FptrPdlCash& = 1
Public Const FptrPdlCheque& = 2
Public Const FptrPdlChitty& = 3
Public Const FptrPdlCoupon& = 4
Public Const FptrPdlCurrency& = 5
Public Const FptrPdlDrivenOff& = 6
Public Const FptrPdlEftImprinter& = 7
Public Const FptrPdlEftTerminal& = 8
Public Const FptrPdlTerminalImprinter& = 9
Public Const FptrPdlFreeGift& = 10
Public Const FptrPdlGiro& = 11
Public Const FptrPdlHome& = 12
Public Const FptrPdlImprinterWithIssuer& = 13
Public Const FptrPdlLocalAccount& = 14
Public Const FptrPdlLocalAccountCard& = 15
Public Const FptrPdlPayCard& = 16
Public Const FptrPdlPayCardManual& = 17
Public Const FptrPdlPrepay& = 18
Public Const FptrPdlPumpTest& = 19
Public Const FptrPdlShortCredit& = 20
Public Const FptrPdlStaff& = 21
Public Const FptrPdlVoucher& = 22

Public Const FptrLcItem& = 1
Public Const FptrLcItemVoid& = 2
Public Const FptrLcDiscount& = 3
Public Const FptrLcDiscountVoid& = 4
Public Const FptrLcSurcharge& = 5
Public Const FptrLcSurchargeVoid& = 6
Public Const FptrLcRefund& = 7
Public Const FptrLcRefundVoid& = 8
Public Const FptrLcSubtotalDiscount& = 9
Public Const FptrLcSubtotalDiscountVoid& = 10
Public Const FptrLcSubtotalSurcharge& = 11
Public Const FptrLcSubtotalSurchargeVoid& = 12
Public Const FptrLcComment& = 13
Public Const FptrLcSubtotal& = 14
Public Const FptrLcTotal& = 15

Public Const FptrDlItem& = 1
Public Const FptrDlItemAdjustment& = 2
Public Const FptrDlItemFuel& = 3
Public Const FptrDlItemFuelVoid& = 4
Public Const FptrDlNotPaid& = 5
Public Const FptrDlPackageAdjustment& = 6
Public Const FptrDlRefund& = 7
Public Const FptrDlRefundVoid& = 8
Public Const FptrDlSubtotalAdjustment& = 9
Public Const FptrDlTotal& = 10
Public Const FptrDlVoid& = 11
Public Const FptrDlVoidItem& = 12


Rem *///////////////////////////////////////////////////////////////////
Rem * "GetTotalizer" Method Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrGtGross& = 1
Public Const FptrGtNet& = 2
Public Const FptrGtDiscount& = 3
Public Const FptrGtDiscountVoid& = 4
Public Const FptrGtItem& = 5
Public Const FptrGtItemVoid& = 6
Public Const FptrGtNotPaid& = 7
Public Const FptrGtRefund& = 8
Public Const FptrGtRefundVoid& = 9
Public Const FptrGtSubtotalDiscount& = 10
Public Const FptrGtSubtotalDiscountVoid& = 11
Public Const FptrGtSubtotalSurcharges& = 12
Public Const FptrGtSubtotalSurchargesVoid& = 13
Public Const FptrGtSurcharges& = 14
Public Const FptrGtSSurchargesVoid& = 15
Public Const FptrGtVat& = 16
Public Const FptrGtVatCategory& = 17


Rem *///////////////////////////////////////////////////////////////////
Rem * "AdjustmentType" arguments in diverse methods
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrAtAmountDiscount& = 1
Public Const FptrAtAmountSurcharge& = 2
Public Const FptrAtPercentageDiscount& = 3
Public Const FptrAtPercentageSurcharge& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "ReportType" argument in "PrintReport" method
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrRtOrdinal& = 1
Public Const FptrRtDate& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "NewCurrency" argument in "SetCurrency" method
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrScEuro& = 1


Rem *///////////////////////////////////////////////////////////////////
Rem * "StatusUpdateEvent" Event: "Data" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const FptrSueCoverOpen& = 11
Public Const FptrSueCoverOk& = 12

Public Const FptrSueJrnEmpty& = 21
Public Const FptrSueJrnNearempty& = 22
Public Const FptrSueJrnPaperok& = 23

Public Const FptrSueRecEmpty& = 24
Public Const FptrSueRecNearempty& = 25
Public Const FptrSueRecPaperok& = 26

Public Const FptrSueSlpEmpty& = 27
Public Const FptrSueSlpNearempty& = 28
Public Const FptrSueSlpPaperok& = 29

Public Const FptrSueIdle& = 1001


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for Fiscal Printer
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEfptrCoverOpen& = 201                ' (Several)
Public Const OposEfptrJrnEmpty& = 202                 ' (Several)
Public Const OposEfptrRecEmpty& = 203                 ' (Several)
Public Const OposEfptrSlpEmpty& = 204                 ' (Several)
Public Const OposEfptrSlpForm& = 205                  ' EndRemoval
Public Const OposEfptrMissingDevices& = 206           ' (Several)
Public Const OposEfptrWrongState& = 207               ' (Several)
Public Const OposEfptrTechnicalAssistance& = 208      ' (Several)
Public Const OposEfptrClockError& = 209               ' (Several)
Public Const OposEfptrFiscalMemoryFull& = 210         ' (Several)
Public Const OposEfptrFiscalMemoryDisconnected& = 211 ' (Several)
Public Const OposEfptrFiscalTotalsError& = 212        ' (Several)
Public Const OposEfptrBadItemQuantity& = 213          ' (Several)
Public Const OposEfptrBadItemAmount& = 214            ' (Several)
Public Const OposEfptrBadItemDescription& = 215       ' (Several)
Public Const OposEfptrReceiptTotalOverflow& = 216     ' (Several)
Public Const OposEfptrBadVat& = 217                   ' (Several)
Public Const OposEfptrBadPrice& = 218                 ' (Several)
Public Const OposEfptrBadDate& = 219                  ' (Several)
Public Const OposEfptrNegativeTotal& = 220            ' (Several)
Public Const OposEfptrWordNotAllowed& = 221           ' (Several)
Public Const OposEfptrBadLength& = 222                ' (Several)
Public Const OposEfptrMissingSetCurrency& = 223       ' (Several)




Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposKbd.h
Rem *
Rem *   POS Keyboard header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 96-04-22 OPOS Release 1.1                                     CRM
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem *   Add "EventTypes" and "POSKeyEventType" values.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "EventTypes" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const KbdEtDown& = 1
Public Const KbdEtDownUp& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "POSKeyEventType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const KbdKetKeydown& = 1
Public Const KbdKetKeyup& = 2



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposLock.h
Rem *
Rem *   Keylock header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "KeyPosition" Property Constants
Rem * "WaitForKeylockChange" Method: "KeyPosition" Parameter
Rem * "StatusUpdateEvent" Event: "Data" Parameter
Rem *///////////////////////////////////////////////////////////////////

Public Const LockKpAny& = 0                            ' WaitForKeylockChange Only
Public Const LockKpLock& = 1
Public Const LockKpNorm& = 2
Public Const LockKpSupr& = 3



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposMicr.h
Rem *
Rem *   MICR header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "CheckType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const MicrCtPersonal& = 1
Public Const MicrCtBusiness& = 2
Public Const MicrCtUnknown& = 99


Rem *///////////////////////////////////////////////////////////////////
Rem * "CountryCode" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const MicrCcUsa& = 1
Public Const MicrCcCanada& = 2
Public Const MicrCcMexico& = 3
Public Const MicrCcUnknown& = 99


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for MICR
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEmicrNocheck& = 201         ' EndInsertion
Public Const OposEmicrCheck& = 202           ' EndRemoval



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposMsr.h
Rem *
Rem *   Magnetic Stripe Reader header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem *   Add ErrorReportingType values.
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem *   Add constants relating to Track 4 Data
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "TracksToRead" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const MsrTr1& = 1
Public Const MsrTr2& = 2
Public Const MsrTr3& = 4
Public Const MsrTr4& = 8

Public Const MsrTr12& = MsrTr1 Or MsrTr2
Public Const MsrTr13& = MsrTr1 Or MsrTr3
Public Const MsrTr14& = MsrTr1 Or MsrTr4
Public Const MsrTr23& = MsrTr2 Or MsrTr3
Public Const MsrTr24& = MsrTr2 Or MsrTr4
Public Const MsrTr34& = MsrTr3 Or MsrTr4

Public Const MsrTr123& = MsrTr1 Or MsrTr2 Or MsrTr3
Public Const MsrTr124& = MsrTr1 Or MsrTr2 Or MsrTr4
Public Const MsrTr134& = MsrTr1 Or MsrTr3 Or MsrTr4
Public Const MsrTr234& = MsrTr2 Or MsrTr3 Or MsrTr4

Public Const MsrTr1234& = MsrTr1 Or MsrTr2 Or MsrTr3 Or MsrTr4


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorReportingType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const MsrErtCard& = 0
Public Const MsrErtTrack& = 1


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorEvent" Event: "ResultCodeExtended" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEmsrStart& = 201
Public Const OposEmsrEnd& = 202
Public Const OposEmsrParity& = 203
Public Const OposEmsrLrc& = 204


Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposPcrw.H
Rem *
Rem *   Point Card Reader Writer header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapCharacterSet" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwCcsAlpha& = 1
Public Const PcrwCcsAscii& = 998
Public Const PcrwCcsKana& = 10
Public Const PcrwCcsKanji& = 11
Public Const PcrwCcsUnicode& = 997


Rem *///////////////////////////////////////////////////////////////////
Rem * "CardState" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwStateNocard& = 1
Public Const PcrwStateRemaining& = 2
Public Const PcrwStateInrw& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * CapTrackToRead and TrackToWrite Property constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwTrack1& = &H1
Public Const PcrwTrack2& = &H2
Public Const PcrwTrack3& = &H4
Public Const PcrwTrack4& = &H8
Public Const PcrwTrack5& = &H10
Public Const PcrwTrack6& = &H20


Rem *///////////////////////////////////////////////////////////////////
Rem * "CharacterSet" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwCsUnicode& = 997
Public Const PcrwCsAscii& = 998
Public Const PcrwCsWindows& = 999


Rem *///////////////////////////////////////////////////////////////////
Rem * "MappingMode" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwMmDots& = 1
Public Const PcrwMmTwips& = 2
Public Const PcrwMmEnglish& = 3
Public Const PcrwMmMetric& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for PoinrCardR/W
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEpcrwRead& = 201
Public Const OposEpcrwWrite& = 202
Public Const OposEpcrwJam& = 203
Public Const OposEpcrwMotor& = 204
Public Const OposEpcrwCover& = 205
Public Const OposEpcrwPrinter& = 206
Public Const OposEpcrwRelease& = 207
Public Const OposEpcrwDisplay& = 208
Public Const OposEpcrwNocard& = 209


Rem *///////////////////////////////////////////////////////////////////
Rem * Magnetic read/write status Property Constants for PoinrCardR/W
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEpcrwStart& = 211
Public Const OposEpcrwEnd& = 212
Public Const OposEpcrwParity& = 213
Public Const OposEpcrwEncode& = 214
Public Const OposEpcrwLrc& = 215
Public Const OposEpcrwVerify& = 216


Rem *///////////////////////////////////////////////////////////////////
Rem * "RotatedPrint" Method: "Rotation" Parameter Constants
Rem * "RotateSpecial" Property Constants (PCRWRPNORMALASYNC not legal)
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwRpNormal& = &H1
Public Const PcrwRpNormalasync& = &H2

Public Const PcrwRpRight90& = &H101
Public Const PcrwRpLeft90& = &H102
Public Const PcrwRpRotate180& = &H103


Rem *///////////////////////////////////////////////////////////////////
Rem * "StatusUpdateEvent" "Status" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PcrwSueNocard& = 1
Public Const PcrwSueRemaining& = 2
Public Const PcrwSueInrw& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposPpad.h
Rem *
Rem *   PIN Pad header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 98-03-06 OPOS Release 1.3                                     JDB
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem *   Add PpadDispNone for devices with no display
Rem *   Add OposEppadBadKey extended result code
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapDisplay" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PpadDispUnrestricted& = 1
Public Const PpadDispPinrestricted& = 2
Public Const PpadDispRestrictedList& = 3
Public Const PpadDispRestrictedOrder& = 4
Public Const PpadDispNone& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "AvailablePromptsList" and "Prompt" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PpadMsgEnterpin& = 1
Public Const PpadMsgPleasewait& = 2
Public Const PpadMsgEntervalidpin& = 3
Public Const PpadMsgRetriesexceeded& = 4
Public Const PpadMsgApproved& = 5
Public Const PpadMsgDeclined& = 6
Public Const PpadMsgCanceled& = 7
Public Const PpadMsgAmountok& = 8
Public Const PpadMsgNotready& = 9
Public Const PpadMsgIdle& = 10
Public Const PpadMsgSlideCard& = 11
Public Const PpadMsgInsertcard& = 12
Public Const PpadMsgSelectcardtype& = 13


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapLanguage" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PpadLangNone& = 1
Public Const PpadLangOne& = 2
Public Const PpadLangPinrestricted& = 3
Public Const PpadLangUnrestricted& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "TransactionType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PpadTransDebit& = 1
Public Const PpadTransCredit& = 2
Public Const PpadTransInq& = 3
Public Const PpadTransReconcile& = 4
Public Const PpadTransAdmin& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "EndEFTTransaction" Method Completion Code Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PpadEftNormal& = 1
Public Const PpadEftAbnormal& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "DataEvent" Event Status Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PpadSuccess& = 1
Public Const PpadCancel& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEppadBadKey = 201


Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposPtr.h
Rem *
Rem *   POS Printer header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem * 96-04-22 OPOS Release 1.1                                     CRM
Rem *   Add CapCharacterSet values.
Rem *   Add ErrorLevel values.
Rem *   Add TransactionPrint Control values.
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem *   Remove PTR_RP_NORMAL_ASYNC.
Rem *   Add more barcode symbologies.
Rem * 98-03-06 OPOS Release 1.3                                     CRM
Rem *   Add more PrintTwoNormal constants.
Rem * 00-09-24 OPOS Release 1.5                                   EPSON
Rem *   Add CapRecMarkFeed values and MarkFeed constants.
Rem *   Add ChangePrintSide constants.
Rem *   Add StatusUpdateEvent constants.
Rem *   Add ResultCodeExtended values.
Rem *   Add CapXxxCartridgeSensor and XxxCartridgeState values.
Rem *   Add CartridgeNotify values.
Rem * 00-09-24 OPOS Release 1.5                                     BKS
Rem *   Add CapCharacterset and CharacterSet values for UNICODE.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * Printer Station Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrSJournal& = 1
Public Const PtrSReceipt& = 2
Public Const PtrSSlip& = 4

Public Const PtrSJournalReceipt& = PtrSJournal Or PtrSReceipt
Public Const PtrSJournalSlip& = PtrSJournal Or PtrSSlip
Public Const PtrSReceiptSlip& = PtrSReceipt Or PtrSSlip

Public Const PtrTwoReceiptJournal& = &H8000& + PtrSJournalReceipt
Public Const PtrTwoSlipJournal& = &H8000& + PtrSJournalSlip
Public Const PtrTwoSlipReceipt& = &H8000& + PtrSReceiptSlip


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapCharacterSet" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrCcsAlpha& = 1
Public Const PtrCcsAscii& = 998
Public Const PtrCcsKana& = 10
Public Const PtrCcsKanji& = 11
Public Const PtrCcsUnicode& = 997


Rem *///////////////////////////////////////////////////////////////////
Rem * "CharacterSet" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrCsUnicode& = 997
Public Const PtrCsAscii& = 998
Public Const PtrCsWindows& = 999


Rem *///////////////////////////////////////////////////////////////////
Rem * "ErrorLevel" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrElNone& = 1
Public Const PtrElRecoverable& = 2
Public Const PtrElFatal& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * "MapMode" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrMmDots& = 1
Public Const PtrMmTwips& = 2
Public Const PtrMmEnglish& = 3
Public Const PtrMmMetric& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "CapXxxColor" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrColorPrimary = &H1
Public Const PtrColorCustom1 = &H2
Public Const PtrColorCustom2 = &H4
Public Const PtrColorCustom3 = &H8
Public Const PtrColorCustom4 = &H10
Public Const PtrColorCustom5 = &H20
Public Const PtrColorCustom6 = &H40
Public Const PtrColorCyan = &H100
Public Const PtrColorMagenta = &H200
Public Const PtrColorYellow = &H400
Public Const PtrColorFull = &H80000000

Rem *///////////////////////////////////////////////////////////////////
Rem * "CapXxxCartridgeSensor" and  "XxxCartridgeState" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrCartUnknown = &H10000000
Public Const PtrCartOk = &H0
Public Const PtrCartRemoved = &H1
Public Const PtrCartEmpty = &H2
Public Const PtrCartNearend = &H4
Public Const PtrCartCleaning = &H8

Rem *///////////////////////////////////////////////////////////////////
Rem * "CartridgeNotify"  Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrCnDisabled = &H0
Public Const PtrCnEnabled = &H1


Rem *///////////////////////////////////////////////////////////////////
Rem * "CutPaper" Method Constant
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrCpFullcut& = 100


Rem *///////////////////////////////////////////////////////////////////
Rem * "PrintBarCode" Method Constants:
Rem *///////////////////////////////////////////////////////////////////

Rem *   "Alignment" Parameter
Rem *     Either the distance from the left-most print column to the start
Rem *     of the bar code, or one of the following:

Public Const PtrBcLeft& = -1
Public Const PtrBcCenter& = -2
Public Const PtrBcRight& = -3

Rem *   "TextPosition" Parameter

Public Const PtrBcTextNone& = -11
Public Const PtrBcTextAbove& = -12
Public Const PtrBcTextBelow& = -13

Rem *   "Symbology" Parameter:

Rem *     One dimensional symbologies
Public Const PtrBcsUpca& = 101                         ' Digits
Public Const PtrBcsUpce& = 102                         ' Digits
Public Const PtrBcsJan8& = 103                         ' = EAN 8
Public Const PtrBcsEan8& = 103                         ' = JAN 8 (added in 1.2)
Public Const PtrBcsJan13& = 104                        ' = EAN 13
Public Const PtrBcsEan13& = 104                        ' = JAN 13 (added in 1.2)
Public Const PtrBcsTf& = 105                           ' (Discrete 2 of 5) Digits
Public Const PtrBcsItf& = 106                          ' (Interleaved 2 of 5) Digits
Public Const PtrBcsCodabar& = 107                      ' Digits, -, $, :, /, ., +;
                                                       '   4 start/stop characters
                                                       '   (a, b, c, d)
Public Const PtrBcsCode39& = 108                       ' Alpha, Digits, Space, -, .,
                                                       '   $, /, +, %; start/stop (*)
                                                       ' Also has Full ASCII feature
Public Const PtrBcsCode93& = 109                       ' Same characters as Code 39
Public Const PtrBcsCode128& = 110                      ' 128 data characters
Rem *        (The following were added in Release 1.2)
Public Const PtrBcsUpcaS& = 111                        ' UPC-A with supplemental
                                                       '   barcode
Public Const PtrBcsUpceS& = 112                        ' UPC-E with supplemental
                                                       '   barcode
Public Const PtrBcsUpcd1& = 113                        ' UPC-D1
Public Const PtrBcsUpcd2& = 114                        ' UPC-D2
Public Const PtrBcsUpcd3& = 115                        ' UPC-D3
Public Const PtrBcsUpcd4& = 116                        ' UPC-D4
Public Const PtrBcsUpcd5& = 117                        ' UPC-D5
Public Const PtrBcsEan8S& = 118                        ' EAN 8 with supplemental
                                                       '   barcode
Public Const PtrBcsEan13S& = 119                       ' EAN 13 with supplemental
                                                       '   barcode
Public Const PtrBcsEan128& = 120                       ' EAN 128
Public Const PtrBcsOcra& = 121                         ' OCR "A"
Public Const PtrBcsOcrb& = 122                         ' OCR "B"


Rem *     Two dimensional symbologies
Public Const PtrBcsPdf417& = 201
Public Const PtrBcsMaxicode& = 202

Rem *     Start of Printer-Specific bar code symbologies
Public Const PtrBcsOther& = 501


Rem *///////////////////////////////////////////////////////////////////
Rem * "PrintBitmap" Method Constants:
Rem *///////////////////////////////////////////////////////////////////

Rem *   "Width" Parameter
Rem *     Either bitmap width or:

Public Const PtrBmAsis& = -11                          ' One pixel per printer dot

Rem *   "Alignment" Parameter
Rem *     Either the distance from the left-most print column to the start
Rem *     of the bitmap, or one of the following:

Public Const PtrBmLeft& = -1
Public Const PtrBmCenter& = -2
Public Const PtrBmRight& = -3


Rem *///////////////////////////////////////////////////////////////////
Rem * "RotatePrint" Method: "Rotation" Parameter Constants
Rem * "RotateSpecial" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrRpNormal& = &H1
Public Const PtrRpRight90& = &H101
Public Const PtrRpLeft90& = &H102
Public Const PtrRpRotate180& = &H103


Rem *///////////////////////////////////////////////////////////////////
Rem * "SetLogo" Method: "Location" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrLTop& = 1
Public Const PtrLBottom& = 2


Rem *///////////////////////////////////////////////////////////////////
Rem * "TransactionPrint" Method: "Control" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrTpTransaction& = 11
Public Const PtrTpNormal& = 12


Rem *///////////////////////////////////////////////////////////////////
Rem * "MarkFeed" Method: "Type" Parameter Constants
Rem * "CapRecMarkFeed" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrMfToTakeup = 1
Public Const PtrMfToCutter = 2
Public Const PtrMfToCurrentTof = 4
Public Const PtrMfToNextTof = 8

Rem *///////////////////////////////////////////////////////////////////
Rem * "ChangePrintSide" Method: "Side" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrPsUnknown = 0
Public Const PtrPsSide1 = 1
Public Const PtrPsSide2 = 2
Public Const PtrPsOpposite = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * "StatusUpdateEvent" Event: "Data" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PtrSueCoverOpen& = 11
Public Const PtrSueCoverOk& = 12

Public Const PtrSueJrnEmpty& = 21
Public Const PtrSueJrnNearempty& = 22
Public Const PtrSueJrnPaperok& = 23

Public Const PtrSueRecEmpty& = 24
Public Const PtrSueRecNearempty& = 25
Public Const PtrSueRecPaperok& = 26

Public Const PtrSueSlpEmpty& = 27
Public Const PtrSueSlpNearempty& = 28
Public Const PtrSueSlpPaperok& = 29

Public Const PtrSueJrnCartridgeEmpty = 41
Public Const PtrSueJrnCartridgeNearempty = 42
Public Const PtrSueJrnHeadCleaning = 43
Public Const PtrSueJrnCartridgeOk = 44

Public Const PtrSueRecCartridgeEmpty = 45
Public Const PtrSueRecCartridgeNearempty = 46
Public Const PtrSueRecHeadCleaning = 47
Public Const PtrSueRecCartridgeOk = 48

Public Const PtrSueSlpCartridgeEmpty = 49
Public Const PtrSueSlpCartridgeNearempty = 50
Public Const PtrSueSlpHeadCleaning = 51
Public Const PtrSueSlpCartridgeOk = 52

Public Const PtrSueIdle& = 1001


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for Printer
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEptrCoverOpen& = 201          ' (Several)
Public Const OposEptrJrnEmpty& = 202           ' (Several)
Public Const OposEptrRecEmpty& = 203           ' (Several)
Public Const OposEptrSlpEmpty& = 204           ' (Several)
Public Const OposEptrSlpForm& = 205            ' EndRemoval
Public Const OposEptrToobig& = 206             ' PrintBitmap
Public Const OposEptrBadformat& = 207          ' PrintBitmap
Public Const OposEptrJrnCartridgeRemoved = 208 ' (Several)
Public Const OposEptrJrnCartridgeEmpty = 209   ' (Several)
Public Const OposEptrJrnHeadCleaning = 210     ' (Several)
Public Const OposEptrRecCartridgeRemoved = 211 ' (Several)
Public Const OposEptrRecCartridgeEmpty = 212   ' (Several)
Public Const OposEptrRecHeadCleaning = 213     ' (Several)
Public Const OposEptrSlpCartridgeRemoved = 214 ' (Several)
Public Const OposEptrSlpCartridgeEmpty = 215   ' (Several)
Public Const OposEptrSlpHeadCleaning = 216     ' (Several)



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposPwr.h
Rem *
Rem *   POSPower header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 99-02-22 OPOS Release 1.x                                     AL
Rem * 99-09-13 OPOS Release 1.x                                     TH
Rem *            ACCU -> UPS, FAN_ALARM and HEAT_ALARM added
Rem * 99-12-06 OPOS Release 1.x                                     TH
Rem *            FAN_ALARM and HEAT_ALARM changed to FAN_STOPPED,
Rem *          FAN_RUNNING, TEMPERATURE_HIGH and TEMPERATURE_OK
Rem * 00-09-24 OPOS Release 1.5                                     TH
Rem *          SHUTDOWN added
Rem *
Rem *///////////////////////////////////////////////////////////////////

Rem *///////////////////////////////////////////////////////////////////
Rem * "UPSChargeState" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const PwrUpsFull = 1
Public Const PwrUpsWarning = 2
Public Const PwrUpsLow = 4
Public Const PwrUpsCritical = 8


Rem *///////////////////////////////////////////////////////////////////
Rem * "StatusUpdateEvent" Event: "Status" Parameter
Rem *///////////////////////////////////////////////////////////////////

Public Const PwrSueUpsFull = 11
Public Const PwrSueUpsWarning = 12
Public Const PwrSueUpsLow = 13
Public Const PwrSueUpsCritical = 14
Public Const PwrSueFanStopped = 15
Public Const PwrSueFanRunning = 16
Public Const PwrSueTemperatureHigh = 17
Public Const PwrSueTemperatureOk = 18
Public Const PwrSueShutdown = 19



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposRod.h
Rem *
Rem *   Remote Order Display header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 98-03-06 OPOS Release 1.3                                     BB
Rem * 00-09-24 OPOS Release 1.5                                    BKS
Rem *   Added CharacterSet value for UNICODE.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "CurrentUnitID" and "UnitsOnline" Properties
Rem *  and "Units" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodUid1& = &H1
Public Const RodUid2& = &H2
Public Const RodUid3& = &H4
Public Const RodUid4& = &H8
Public Const RodUid5& = &H10
Public Const RodUid6& = &H20
Public Const RodUid7& = &H40
Public Const RodUid8& = &H80
Public Const RodUid9& = &H100
Public Const RodUid10& = &H200
Public Const RodUid11& = &H400
Public Const RodUid12& = &H800
Public Const RodUid13& = &H1000
Public Const RodUid14& = &H2000
Public Const RodUid15& = &H4000
Public Const RodUid16& = &H8000
Public Const RodUid17& = &H10000
Public Const RodUid18& = &H20000
Public Const RodUid19& = &H40000
Public Const RodUid20& = &H80000
Public Const RodUid21& = &H100000
Public Const RodUid22& = &H200000
Public Const RodUid23& = &H400000
Public Const RodUid24& = &H800000
Public Const RodUid25& = &H1000000
Public Const RodUid26& = &H2000000
Public Const RodUid27& = &H4000000
Public Const RodUid28& = &H8000000
Public Const RodUid29& = &H10000000
Public Const RodUid30& = &H20000000
Public Const RodUid31& = &H40000000
Public Const RodUid32& = &H80000000


Rem *///////////////////////////////////////////////////////////////////
Rem * Broadcast Methods: "Attribute" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodAttrBlink& = &H80

Public Const RodAttrBgBlack& = &H0
Public Const RodAttrBgBlue& = &H10
Public Const RodAttrBgGreen& = &H20
Public Const RodAttrBgCyan& = &H30
Public Const RodAttrBgRed& = &H40
Public Const RodAttrBgMagenta& = &H50
Public Const RodAttrBgBrown& = &H60
Public Const RodAttrBgGray& = &H70

Public Const RodAttrIntensity& = &H8

Public Const RodAttrFgBlack& = &H0
Public Const RodAttrFgBlue& = &H1
Public Const RodAttrFgGreen& = &H2
Public Const RodAttrFgCyan& = &H3
Public Const RodAttrFgRed& = &H4
Public Const RodAttrFgMagenta& = &H5
Public Const RodAttrFgBrown& = &H6
Public Const RodAttrFgGray& = &H7


Rem *///////////////////////////////////////////////////////////////////
Rem * "DrawBox" Method: "BorderType" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodBdrSingle& = 1
Public Const RodBdrDouble& = 2
Public Const RodBdrSolid& = 3


Rem *///////////////////////////////////////////////////////////////////
Rem * "ControlClock" Method: "Function" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodClkStart& = 1
Public Const RodClkPause& = 2
Public Const RodClkResume& = 3
Public Const RodClkMove& = 4
Public Const RodClkStop& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "ControlCursor" Method: "Function" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodCrsLine& = 1
Public Const RodCrsLineBlink& = 2
Public Const RodCrsBlock& = 3
Public Const RodCrsBlockBlink& = 4
Public Const RodCrsOff& = 5


Rem *///////////////////////////////////////////////////////////////////
Rem * "SelectCharacterSet" Method: "CharacterSet" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodCsUnicode& = 997
Public Const RodCsAscii& = 998
Public Const RodCsWindows& = 999


Rem *///////////////////////////////////////////////////////////////////
Rem * "TransactionDisplay" Method: "Function" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodTdTransaction& = 11
Public Const RodTdNormal& = 12


Rem *///////////////////////////////////////////////////////////////////
Rem * "UpdateVideoRegionAttribute" Method: "Function" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodUaSet& = 1
Public Const RodUaIntensityOn& = 2
Public Const RodUaIntensityOff& = 3
Public Const RodUaReverseOn& = 4
Public Const RodUaReverseOff& = 5
Public Const RodUaBlinkOn& = 6
Public Const RodUaBlinkOff& = 7


Rem *///////////////////////////////////////////////////////////////////
Rem * "EventTypes" Property and "DataEvent" Event: "Status" Parameter Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const RodDeTouchUp& = &H1
Public Const RodDeTouchDown& = &H2
Public Const RodDeTouchMove& = &H4


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for Remote Order Display
Rem *///////////////////////////////////////////////////////////////////

Public Const OposErodBadclk& = 201           ' ControlClock
Public Const OposErodNoclocks& = 202         ' ControlClock
Public Const OposErodNoregion& = 203         ' RestoreVideoRegion
Public Const OposErodNobuffers& = 204        ' SaveVideoRegion
Public Const OposErodNoroom& = 205           ' SaveVideoRegion



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposScal.h
Rem *
Rem *   Scale header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "WeightUnit" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Public Const ScalWuGram& = 1
Public Const ScalWuKilogram& = 2
Public Const ScalWuOunce& = 3
Public Const ScalWuPound& = 4


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for Scale
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEscalOverweight& = 201      ' ReadWeight



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposScan.h
Rem *
Rem *   Scanner header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem *   Add "ScanDataType" values.
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "ScanDataType" Property Constants
Rem *///////////////////////////////////////////////////////////////////

Rem * One dimensional symbologies
Public Const ScanSdtUpca& = 101                        ' Digits
Public Const ScanSdtUpce& = 102                        ' Digits
Public Const ScanSdtJan8& = 103                        ' = EAN 8
Public Const ScanSdtEan8& = 103                        ' = JAN 8 (added in 1.2)
Public Const ScanSdtJan13& = 104                       ' = EAN 13
Public Const ScanSdtEan13& = 104                       ' = JAN 13 (added in 1.2)
Public Const ScanSdtTf& = 105                          ' (Discrete 2 of 5) Digits
Public Const ScanSdtItf& = 106                         ' (Interleaved 2 of 5) Digits
Public Const ScanSdtCodabar& = 107                     ' Digits, -, $, :, /, ., +;
                                                       '   4 start/stop characters
                                                       '   (a, b, c, d)
Public Const ScanSdtCode39& = 108                      ' Alpha, Digits, Space, -, .,
                                                       '   $, /, +, %; start/stop (*)
                                                       ' Also has Full ASCII feature
Public Const ScanSdtCode93& = 109                      ' Same characters as Code 39
Public Const ScanSdtCode128& = 110                     ' 128 data characters

Public Const ScanSdtUpcaS& = 111                       ' UPC-A with supplemental
                                                       '   barcode
Public Const ScanSdtUpceS& = 112                       ' UPC-E with supplemental
                                                       '   barcode
Public Const ScanSdtUpcd1& = 113                       ' UPC-D1
Public Const ScanSdtUpcd2& = 114                       ' UPC-D2
Public Const ScanSdtUpcd3& = 115                       ' UPC-D3
Public Const ScanSdtUpcd4& = 116                       ' UPC-D4
Public Const ScanSdtUpcd5& = 117                       ' UPC-D5
Public Const ScanSdtEan8S& = 118                       ' EAN 8 with supplemental
                                                       '   barcode
Public Const ScanSdtEan13S& = 119                      ' EAN 13 with supplemental
                                                       '   barcode
Public Const ScanSdtEan128& = 120                      ' EAN 128
Public Const ScanSdtOcra& = 121                        ' OCR "A"
Public Const ScanSdtOcrb& = 122                        ' OCR "B"

Rem * Two dimensional symbologies
Public Const ScanSdtPdf417& = 201
Public Const ScanSdtMaxicode& = 202

Rem * Special cases
Public Const ScanSdtOther& = 501                       ' Start of Scanner-Specific bar
                                                       '   code symbologies
Public Const ScanSdtUnknown& = 0                       ' Cannot determine the barcode
                                                       '   symbology.



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposSig.h
Rem *
Rem *   Signature Capture header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem * No definitions required for this version.



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposTone.h
Rem *
Rem *   Tone Indicator header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 97-06-04 OPOS Release 1.2                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem * No definitions required for this version.



Rem *///////////////////////////////////////////////////////////////////
Rem *
Rem * OposTot.h
Rem *
Rem *   Hard Totals header file for OPOS Applications.
Rem *
Rem * Modification history
Rem * ------------------------------------------------------------------
Rem * 95-12-08 OPOS Release 1.0                                     CRM
Rem *
Rem *///////////////////////////////////////////////////////////////////


Rem *///////////////////////////////////////////////////////////////////
Rem * "ResultCodeExtended" Property Constants for Hard Totals
Rem *///////////////////////////////////////////////////////////////////

Public Const OposEtotNoroom& = 201           ' Create, Write
Public Const OposEtotValidation& = 202       ' Read, Write



Rem *End of OPOSALL.BAS*
