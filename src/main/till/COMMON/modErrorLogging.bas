Attribute VB_Name = "modErrorLogging"
'<CAMH>****************************************************************************************
'* Module : modErrorLogging
'* Date   : 05/24/2004
'* Author : chrisr
' $Archive: /Projects/OasysV2/VB/Common/modErrorLogging.bas $
'**********************************************************************************************
'* Summary: Provides common routines used to output to the Windows Event Log
'**********************************************************************************************
'* $Revision: 1 $
'* $Date: 25/05/04 9:53 $
'* $Author: Mauricem $
'* Versions:
'* 05.24.04    chrisr
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modErrorLogging"

Declare Function RegisterEventSource Lib "advapi32.dll" Alias _
  "RegisterEventSourceA" (ByVal lpUNCServerName As String, _
  ByVal lpSourceName As String) As Long

Declare Function DeregisterEventSource Lib "advapi32.dll" ( _
  ByVal hEventLog As Long) As Long

Declare Function ReportEvent Lib "advapi32.dll" Alias _
"ReportEventA" ( _
  ByVal hEventLog As Long, ByVal wType As Integer, _
  ByVal wCategory As Integer, ByVal dwEventID As Long, _
  ByVal lpUserSid As Any, ByVal wNumStrings As Integer, _
  ByVal dwDataSize As Long, plpStrings As Long, _
  lpRawData As Any) As Boolean

Declare Function GetLastError Lib "kernel32" () As Long

Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" ( _
  hpvDest As Any, hpvSource As Any, _
  ByVal cbCopy As Long)

Declare Function GlobalAlloc Lib "kernel32" ( _
   ByVal wFlags As Long, _
   ByVal dwBytes As Long) As Long

Declare Function GlobalFree Lib "kernel32" ( _
   ByVal hMem As Long) As Long

Public Const EVENTLOG_SUCCESS = 0
Public Const EVENTLOG_ERROR_TYPE = 1
Public Const EVENTLOG_WARNING_TYPE = 2
Public Const EVENTLOG_INFORMATION_TYPE = 4
Public Const EVENTLOG_AUDIT_SUCCESS = 8
Public Const EVENTLOG_AUDIT_FAILURE = 10

Public Sub LogNTEvent(sString As String, mbCalledfromNightRoutine As Boolean)

Dim iNumStrings     As Integer
Dim hEventLog       As Long
Dim hMsgs           As Long
Dim cbStringSize    As Long
Dim iReportEvent    As Long

    hEventLog = RegisterEventSource(vbNullString, App.Title)
    
    cbStringSize = Len(sString) + 1
    
    hMsgs = GlobalAlloc(&H40, cbStringSize)
    
    Call CopyMemory(ByVal hMsgs, ByVal sString, cbStringSize)
    
    iNumStrings = 1
    
    iReportEvent = ReportEvent(hEventLog, EVENTLOG_ERROR_TYPE, 0, 1003, 0&, iNumStrings, _
        cbStringSize, hMsgs, hMsgs)
    
    If iReportEvent = 0 And mbCalledfromNightRoutine = True Then
        MsgBox GetLastError()
    End If
    
    Call GlobalFree(hMsgs)
    
    Call DeregisterEventSource(hEventLog)
    
End Sub
