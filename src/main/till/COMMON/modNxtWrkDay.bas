Attribute VB_Name = "modNxtWrkDay"
Option Explicit

'<CAMH>****************************************************************************************
'* Module : modNxtWrkDay
'* Date   : 27/05/03
'* Author : KeithB
' $Archive: /Projects/OasysV2/VB/Common/modNxtWrkDay.bas $
'**********************************************************************************************
'* Summary: Common routine to provide next working day a store is open
'**********************************************************************************************
'* $Revision: 1 $ $ Date: 5/27/03 5:21p $ $Author: Keithb $
'* Versions:
'* 27/05/03    KeithB
'*             Header added.
'*
'</CAMH>***************************************************************************************

' Parameter holding path & file name of date check file.
Private Const PRM_DATECHECK_FILE As Long = 143

'<CACH>****************************************************************************************
'* Function:  GetNextOpenDay
'**********************************************************************************************
'* Description: Provides next working day a store is open.
'*              Takes into account - normal closed days e.g. Sundays
'*                                 - holidays as indicated in the file date.Chk
'*
'*   NOTE:      This module requires a project reference to
'*              Microsoft Scripting Runtime - Scrrun.Dll
'*              (normally located in C:\Windows\System)
'*
'**********************************************************************************************
'* Parameters:
'* Input:  dteStartDate   - the date to be incremented until a working day is found
'**********************************************************************************************
'* History:
'* 27/05/03    KeithB
'*             Header added.
'</CACH>***************************************************************************************

Public Function GetNextOpenDay(ByVal dteStartDate As Date) As Date

Dim oSystemDates     As Object
Dim dteNextDateOpen  As Date
Dim strMessage       As String
Dim strDateCheckFile As String
Dim strDateCheckData As String
Dim strDateDdMmYy    As String
Dim intDayofWeek     As Integer
Dim blnStoreOpen     As Boolean
Dim tsDateCheck      As TextStream
Dim fsoDateCheck     As New FileSystemObject

    ' in case of errors set the date to the input date
    GetNextOpenDay = dteStartDate
    dteNextDateOpen = dteStartDate

    strDateCheckFile = goSession.GetParameter(PRM_DATECHECK_FILE)

    If LenB(strDateCheckFile) = 0 Then
        strMessage = "Path and name of the date check file missing -" & vbCrLf & _
                     "unable to increment date to the next working day" & vbCrLf & vbCrLf & _
                     "(parameter number " & PRM_DATECHECK_FILE & " is not set.)"
        Call MsgBox(strMessage, vbCritical, "Invalid Parameter")
        Exit Function
    End If
    
    
    On Error GoTo GetNextOpenDay_DateReadErr
    
    ' open the date check file
    Set tsDateCheck = fsoDateCheck.OpenTextFile(strDateCheckFile, ForReading)
    ' read the data from the date check file and create a string
    '   e.g.    24/12/02,25/12/02,26/12/02,01/01/03
    Do While Not tsDateCheck.AtEndOfStream
        strDateCheckData = strDateCheckData & "," & tsDateCheck.ReadLine
    Loop
    tsDateCheck.Close
    
    
    ' Read the SYSDAT file (CLASSID_SYSTEMDATES)
    ' to see how many days the store is open
    On Error Resume Next
    Set oSystemDates = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSystemDates.IBo_SetLoadField(CMP_EQUAL, FID_SYSTEMDATES_ID, "01")
    Call oSystemDates.IBo_Load

    If oSystemDates.NoDaysOpen <= 0 Then
        strMessage = "Error - number of open days not set in SysDat"
        Call MsgBox(strMessage, vbCritical, "Can't calculate next open day")
        Exit Function
    End If
    
    Do
    
        dteNextDateOpen = DateAdd("d", 1, dteNextDateOpen)
        ' first day of week is Monday (by definition)
        intDayofWeek = Weekday(dteNextDateOpen, vbMonday)
        If intDayofWeek > oSystemDates.NoDaysOpen Then
            blnStoreOpen = False
        Else
            ' check this is not a holiday
            strDateDdMmYy = Format$(dteNextDateOpen, "dd/mm/yy")
            If InStr(strDateCheckData, strDateDdMmYy) > 0 Then
                blnStoreOpen = False
            Else
                blnStoreOpen = True
            End If
        End If
        
    Loop Until blnStoreOpen = True
    
    GetNextOpenDay = dteNextDateOpen
      
GetNextOpenDay_Done:
    On Error Resume Next
    Set oSystemDates = Nothing
    
    Exit Function
    
GetNextOpenDay_DateReadErr:
    strMessage = "Error reading date check file " & strDateCheckFile & vbCrLf & _
                 "Err no " & Err.Number & vbCrLf & Err.Description
    Call MsgBox(strMessage, vbInformation, "Error setting next open day")
    Resume GetNextOpenDay_Done
    
End Function
