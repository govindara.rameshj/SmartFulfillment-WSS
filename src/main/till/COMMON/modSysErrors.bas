Attribute VB_Name = "modSysErrors"
'<CAMH>****************************************************************************************
'* Module : modSysErrors
'* Date   : 27/09/02
'* Author : MartinW
'*$Archive: /Projects/OasysV2/VB/Common/modSysErrors.bas $
'**********************************************************************************************
'* Summary: This module defines system error numbers as constants.
'*          It can be included in every project that might need to handle such an error.
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 7/06/04 10:03 $ $Revision: 19 $
'* Versions:
'* 27/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modSysErrors"

'Error Codes 1-100 reserved for Oasys System Errors
Public Const OASYS_ERR_CLASS_NOT_FOUND = vbObjectError + 1
Public Const OASYS_ERR_DUPLICATE_CLASS = vbObjectError + 2
Public Const OASYS_ERR_NULL_OBJECT_REFERENCE = vbObjectError + 3
Public Const OASYS_ERR_ROOT_NOT_INITIALISED = vbObjectError + 4
Public Const OASYS_ERR_INVALID_ENTERPRISE_ID = vbObjectError + 5
Public Const OASYS_ERR_INCONSISTENT_OASYS_SETUP = vbObjectError + 6
Public Const OASYS_ERR_INCONSISTENT_OBJECT = vbObjectError + 7
Public Const OASYS_ERR_NOT_IMPLEMENTED = vbObjectError + 8
Public Const OASYS_ERR_NOT_INSTALLED = vbObjectError + 9
Public Const OASYS_ERR_BO_CLASS_INCOMPLETE = vbObjectError + 10
Public Const OASYS_ERR_NO_DB_CONNECTION = vbObjectError + 11
Public Const OASYS_ERR_GETFIELD_INVALID_FID = vbObjectError + 12
Public Const OASYS_ERR_NON_UNIQUE_SELECTION = vbObjectError + 13
Public Const OASYS_ERR_BO_METHOD_NOT_IMPLEMENTED = vbObjectError + 14
Public Const OASYS_ERR_VIEWBO_METHOD_INVALID = vbObjectError + 15

'Error Codes 101-199 reserved for General Oasys Errors
Public Const OASYS_ERR_PARAMETER_NOT_KNOWN = vbObjectError + 101
Public Const OASYS_ERR_INVALID_WORKSTATIONID = vbObjectError + 102
Public Const OASYS_ERR_INVALID_USERID = vbObjectError + 103
Public Const OASYS_ERR_INVALID_EDI_PATH = vbObjectError + 104
Public Const OASYS_ERR_MISSING_SYSTEMOPTIONS = vbObjectError + 105
Public Const OASYS_ERR_MISSING_RETAILEROPTIONS = vbObjectError + 110
Public Const OASYS_ERR_UNABLE_To_LOG_TASK = vbObjectError + 111
Public Const OASYS_ERR_INVALID_PARAMETER_LINE = vbObjectError + 120
Public Const OASYS_ERR_MISSING_INVENTORY_RECORD = vbObjectError + 121
'Block reserved for use by PCI for NITMAS conversion 130-150
Public Const OASYS_ERR_NITMAS1 = vbObjectError + 130
Public Const OASYS_ERR_COMMS_PATH_NOT_FOUND = vbObjectError + 131
Public Const OASYS_ERR_INCOMPLETE_SYSTEM_UPDATE = vbObjectError + 132
Public Const OASYS_ERR_UPDATE_FAILED = vbObjectError + 133
Public Const OASYS_ERR_INVALID_PERIOD_END_SETUP = vbObjectError + 134
Public Const OASYS_ERR_INSERT_FAILED = vbObjectError + 135
Public Const OASYS_ERR_MISSING_EDI_FILE_CONTROL_RECORD = vbObjectError + 136
Public Const OASYS_ERR_EDI_CORRUPT_TRANSMISSION_FILE = vbObjectError + 137
Public Const OASYS_ERR_DELETE_FAILED = vbObjectError + 138

Public Const OASYS_ERR_NITMAS20 = vbObjectError + 150

'Error Codes 200-299 reserved for Oasys Transmission Export and Import Errors
Public Const OASYS_ERR_EDI_DETAIL_HEADER_MISSING = vbObjectError + 200
Public Const OASYS_ERR_EDI_HEADER_MISSING As Long = 201
Public Const OASYS_ERR_EDI_TRAILER_RECORDCOUNT_ERROR As Long = 202
Public Const OASYS_ERR_EDI_INVALID_LINETYPE As Long = 203

Public Const OASYS_ERR_GETFIELD_INVALID_FID_MSG As String = "Invalid Field ID passed into GetField - ID="
