VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F537A415-F16A-11D6-B070-0020AFC9A0C8}#1.0#0"; "HotkeyList.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Begin VB.Form frmPreview 
   Caption         =   "Report print-out preview"
   ClientHeight    =   6045
   ClientLeft      =   1590
   ClientTop       =   1545
   ClientWidth     =   10035
   Icon            =   "frmPreview.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MinButton       =   0   'False
   ScaleHeight     =   6045
   ScaleWidth      =   10035
   WindowState     =   2  'Maximized
   Begin prjHotkeyList.ucHotkeyList uchkHelpKeys 
      Height          =   2835
      Left            =   3960
      TabIndex        =   2
      Top             =   1920
      Visible         =   0   'False
      Width           =   3510
      _ExtentX        =   6191
      _ExtentY        =   5001
   End
   Begin FPSpreadADO.fpSpreadPreview spprvReport 
      Height          =   4755
      Left            =   60
      TabIndex        =   14
      Top             =   780
      Width           =   7035
      _Version        =   458752
      _ExtentX        =   12409
      _ExtentY        =   8387
      _StockProps     =   96
      BorderStyle     =   1
      AllowUserZoom   =   -1  'True
      GrayAreaColor   =   8421504
      GrayAreaMarginH =   720
      GrayAreaMarginType=   0
      GrayAreaMarginV =   720
      PageBorderColor =   8388608
      PageBorderWidth =   2
      PageShadowColor =   0
      PageShadowWidth =   2
      PageViewPercentage=   100
      PageViewType    =   0
      ScrollBarH      =   1
      ScrollBarV      =   1
      ScrollIncH      =   360
      ScrollIncV      =   360
      PageMultiCntH   =   1
      PageMultiCntV   =   1
      PageGutterH     =   -1
      PageGutterV     =   -1
      ScriptEnhanced  =   0   'False
   End
   Begin VB.PictureBox frOptions 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      ScaleHeight     =   615
      ScaleWidth      =   9975
      TabIndex        =   3
      Top             =   0
      Width           =   9975
      Begin VB.ComboBox cmbPageView 
         Height          =   315
         ItemData        =   "frmPreview.frx":058A
         Left            =   1920
         List            =   "frmPreview.frx":05A0
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   135
         Width           =   1335
      End
      Begin VB.TextBox txtCurrentPage 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3960
         TabIndex        =   8
         Text            =   "0"
         Top             =   165
         Width           =   375
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "F9-Print"
         Height          =   375
         Left            =   6600
         TabIndex        =   7
         Top             =   135
         Width           =   975
      End
      Begin VB.CheckBox chkSmartPrint 
         Alignment       =   1  'Right Justify
         Caption         =   "&Smart Print"
         Height          =   255
         Left            =   5280
         TabIndex        =   6
         Top             =   195
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdPrinter 
         Caption         =   "P&rinter"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   855
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "F10-Exit"
         Height          =   375
         Left            =   7680
         TabIndex        =   4
         Top             =   135
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Page &View"
         Height          =   255
         Left            =   1080
         TabIndex        =   13
         Top             =   195
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "&Page"
         Height          =   255
         Left            =   3360
         TabIndex        =   12
         Top             =   195
         Width           =   495
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "of "
         Height          =   195
         Left            =   4440
         TabIndex        =   11
         Top             =   195
         Width           =   180
      End
      Begin VB.Label lblNoPages 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4680
         TabIndex        =   10
         Top             =   195
         Width           =   375
      End
   End
   Begin MSComDlg.CommonDialog cdlgPrinter 
      Left            =   0
      Top             =   4920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   5670
      Width           =   10035
      _ExtentX        =   17701
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmPreview.frx":05E2
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9869
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "19:40"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblExitSize 
      AutoSize        =   -1  'True
      Caption         =   "Label4"
      Height          =   195
      Left            =   8400
      TabIndex        =   1
      Top             =   720
      Width           =   480
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmPreview
'* Date   : 29/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Common/frmPreview.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 19/05/04 9:15 $
'* $Revision: 6 $
'* Versions:
'* 29/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmPreview"

Private Const PRM_SELECT_PRINTER As Long = 107
Private mPreviewSpread As fpSpread
Private mlngOldColour  As Long

Private Sub chkSmartPrint_Click()

    mPreviewSpread.PrintSmartPrint = chkSmartPrint.Value
    Call RefreshPreview
    
End Sub

Private Sub cmbPageView_Click()

    spprvReport.PageViewType = cmbPageView.ItemData(cmbPageView.ListIndex)
    If cmbPageView.Text = "2 Pages" Then
        spprvReport.PageMultiCntH = 2
        spprvReport.PageMultiCntV = 1
    End If
    If cmbPageView.Text = "4 Pages" Then
        spprvReport.PageMultiCntH = 2
        spprvReport.PageMultiCntV = 2
    End If

End Sub

Private Sub cmbPageView_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub cmdExit_Click()

    Unload Me

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub cmdPrint_Click()

    mPreviewSpread.PrintSheet

End Sub

Private Sub cmdPrint_GotFocus()

    cmdPrint.FontBold = True

End Sub

Private Sub cmdPrint_LostFocus()
    
    cmdPrint.FontBold = False

End Sub

Private Sub cmdPrinter_Click()

    On Error Resume Next
    Err.Clear
    cdlgPrinter.CancelError = True
    Call cdlgPrinter.ShowPrinter
    If Err.Number <> 0 Then
        On Error GoTo 0
        Exit Sub
    End If
    Call RefreshPreview
    
End Sub

Private Sub RefreshPreview()

    spprvReport.hWndSpread = 0
    spprvReport.hWndSpread = mPreviewSpread.hwnd
    lblNoPages.Caption = mPreviewSpread.PrintPageCount
    If Val(txtCurrentPage.Text) > lblNoPages.Caption Then txtCurrentPage.Text = lblNoPages.Caption
    spprvReport.PageCurrent = Val(txtCurrentPage.Text)

End Sub


Private Sub cmdPrinter_GotFocus()
    
    cmdPrinter.FontBold = True

End Sub

Private Sub cmdPrinter_LostFocus()
    
    cmdPrinter.FontBold = False

End Sub

Private Sub Form_Activate()
    
    Call spprvReport.SetFocus

End Sub

Private Sub Form_Initialize()

    Call DebugMsg(MODULE_NAME, "Form_Initilize", endlTraceIn)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF1):  'if F1 then show Help buttons
                uchkHelpKeys.Visible = True
            Case (vbKeyF3):  'if F3 then reset screen to screen width
                cmbPageView.ListIndex = 3
                Call cmbPageView_Click
            Case (vbKeyF9):  'if F9 then call reprint document
                KeyCode = 0
                Call cmdPrint_Click
            Case (vbKeyF10): 'if F10 then exit
                Call cmdExit_Click
            Case (vbKeyPageDown):
                If Val(lblNoPages.Caption) > Val(txtCurrentPage.Text) Then
                    txtCurrentPage.Text = Val(txtCurrentPage.Text) + 1
                    Call txtCurrentPage_KeyPress(13)
                End If
            Case (vbKeyPageUp):
                If Val(txtCurrentPage.Text) > 1 Then
                    txtCurrentPage.Text = Val(txtCurrentPage.Text) - 1
                    Call txtCurrentPage_KeyPress(13)
                End If
        End Select 'Key pressed with no Shift/Alt combination
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    Select Case (KeyAscii)
        Case (Asc("-")):
            spprvReport.PageViewType = PageViewTypePercentage
            If spprvReport.PageViewPercentage > 50 Then spprvReport.PageViewPercentage = spprvReport.PageViewPercentage - 5
'            If Val(lblNoPages.Caption) > Val(txtCurrentPage.Text) Then
'                txtCurrentPage.Text = Val(txtCurrentPage.Text) + 1
'                Call txtCurrentPage_KeyPress(13)
'            End If
        Case (Asc("+")):
            spprvReport.PageViewType = PageViewTypePercentage
            If spprvReport.PageViewPercentage < 100 Then spprvReport.PageViewPercentage = spprvReport.PageViewPercentage + 5
    End Select 'Key pressed with no Shift/Alt combination


End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF1):
                Me.uchkHelpKeys.Visible = False
        End Select
    End If

End Sub

Private Sub Form_Load()
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    cmdPrinter.Visible = goSession.GetParameter(PRM_SELECT_PRINTER)
    Call InitialiseStatusBar(sbStatus)
    sbStatus.Panels(PANEL_INFO).Text = "F1-Help"
    cmbPageView.ListIndex = 3
    uchkHelpKeys.Caption = "Report preview keys"
    Call uchkHelpKeys.AddHotKey("Pg Up", "Change report to previous page")
    Call uchkHelpKeys.AddHotKey("Pg Dn", "Change report to next page")
    Call uchkHelpKeys.AddHotKey("-", "Enlarge Report (Zoom In)")
    Call uchkHelpKeys.AddHotKey("+", "Reduce Report (Zoom Out)")
    Call uchkHelpKeys.AddHotKey("F3", "Report to fit screen width")
    Call uchkHelpKeys.AddHotKey("F9", "Send report to printer")
    Call uchkHelpKeys.AddHotKey("F10", "Exit report preview")
        
End Sub

Private Sub Form_Resize()

    If Me.WindowState = vbMinimized Then Exit Sub
    If Me.Width < 2000 Then
        Me.Width = 2000
        Exit Sub
    End If
    If Me.Height < 2000 Then
        Me.Height = 2000
        Exit Sub
    End If
    DoEvents
    spprvReport.Width = Me.Width - spprvReport.Left * 3
    spprvReport.Height = Me.Height - spprvReport.Top - (sbStatus.Height * 3)
    frOptions.Width = Me.Width - 120

End Sub

Private Sub spprvReport_PageChange(ByVal Page As Long)

    txtCurrentPage.Text = Page

End Sub

Private Sub txtCurrentPage_GotFocus()

    mlngOldColour = txtCurrentPage.BackColor
    txtCurrentPage.BackColor = RGBEdit_Colour

End Sub

Private Sub txtCurrentPage_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = 13) Then
        txtCurrentPage.Text = Val(txtCurrentPage.Text)
        If txtCurrentPage.Text > mPreviewSpread.PrintPageCount Then txtCurrentPage.Text = mPreviewSpread.PrintPageCount
        If txtCurrentPage.Text <> spprvReport.PageCurrent Then spprvReport.PageCurrent = Val(txtCurrentPage.Text)
    End If

End Sub

Private Sub txtCurrentPage_LostFocus()

    Call txtCurrentPage_KeyPress(vbKeyReturn)
    txtCurrentPage.BackColor = mlngOldColour
        
End Sub

Public Sub SetPreview(strCaption As String, sprdPreview As fpSpreadPreview, Optional strExitCaption As String = vbNullString)

    Set mPreviewSpread = sprdPreview 'get local copy of spread to call print methods if required
    Me.Caption = strCaption
    If LenB(strExitCaption) <> 0 Then
        cmdExit.Caption = "F10-" & strExitCaption
        lblExitSize.Caption = cmdExit.Caption
        cmdExit.Width = lblExitSize.Width + 360
    End If
    spprvReport.hWndSpread = mPreviewSpread.hwnd
    lblNoPages.Caption = mPreviewSpread.PrintPageCount
    DoEvents
    txtCurrentPage.Text = 1

End Sub

