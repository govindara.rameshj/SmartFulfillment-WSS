Small Barcode	B		barcode1.sgn
Long Peg	L	S	PILOTLONGPEG.sgn
Small	L	M	PILOTSMALL.sgn
Medium	L	L	PILOTMEDIUM.sgn
SM Pilot Peg	L	S	KevanSMPilot_PEG.sgn
SM Pilot Medium	L	L	KevanSMPilot_MEDIUM.sgn
SM Pilot Small	L	M	KevanSMPilot_SMALL.sgn
NS SM Pilot Small	L	M	KevanNS_SMPilot_SMALL.sgn
AL SM Pilot Medium	L	L	KevanAL_SMPilot_MEDIUM.sgn
Kevan A4 L POS	L	L	KevanA4L.sgn
Kevan A4 P POS	L	L	KevanA4P.sgn
Kevan Barcode	B		KevanBarcode.sgn