VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmTenderControlsEditor 
   Caption         =   "Tender Controls Editor"
   ClientHeight    =   5730
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5985
   Icon            =   "frmTenderControlsEditor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5730
   ScaleWidth      =   5985
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAddRecord 
      Caption         =   "Add"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "F8-Delete"
      Height          =   375
      Left            =   1560
      TabIndex        =   2
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   4440
      TabIndex        =   4
      Top             =   4920
      Width           =   1335
   End
   Begin FPSpreadADO.fpSpread sprdEdit 
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5655
      _Version        =   458752
      _ExtentX        =   9975
      _ExtentY        =   8281
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GridShowHoriz   =   0   'False
      GridSolid       =   0   'False
      MaxCols         =   4
      ScrollBars      =   2
      SpreadDesigner  =   "frmTenderControlsEditor.frx":058A
      UserResize      =   0
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   5355
      Width           =   5985
      _ExtentX        =   10557
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmTenderControlsEditor.frx":594B
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   2725
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "2:11 PM"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmTenderControlsEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmTenderControlsEditor
'* Date   : 11/19/04
'* Author :
'**********************************************************************************************
'* Summary: Allows user edit of the tender controls (TENCTL) business object
'**********************************************************************************************
'* Versions:
'</CAMH>***************************************************************************************
Option Explicit

Private Const SPRD_COL_ID = 1
Private Const SPRD_COL_DESC = 2
Private Const SPRD_COL_INUSE = 3
Private Const SPRD_COL_ROW_EDITED_FLAG = 4

Private Const ROW_EDIT_TYPE_NONE = 0
Private Const ROW_EDIT_TYPE_CHANGE = 1
Private Const ROW_EDIT_TYPE_ADD = 2
Private Const ROW_EDIT_TYPE_DELETE = 3

Dim mEditedFlag As Boolean

Private Sub Form_Load()

Dim oTender     As cTenderControl
Dim colParams   As Collection
Dim lngIndex    As Long
Dim cCtrl       As Control

    GetRoot
    Call InitialiseStatusBar(sbStatus)
    
    KeyPreview = True '       F keys for buttons
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    sprdEdit.BackColor = goSession.GetParameter(PRM_QUERY_GRIDODDCOLOUR)
    
    For Each cCtrl In Me.Controls
        If TypeName(cCtrl) = "CommandButton" Then
            cCtrl.BackColor = Me.BackColor
        End If
    Next cCtrl

    mEditedFlag = False
    
    Set oTender = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
    
    oTender.AddLoadField FID_TENDERCONTROL_TendType
    oTender.AddLoadField FID_TENDERCONTROL_Description
    oTender.AddLoadField FID_TENDERCONTROL_ValidForUse
   ' oTender.AddLoadFilter CMP_GREATEREQUALTHAN, FID_PARAMETER_ID, 200
   ' oTender.AddLoadFilter CMP_LESSTHAN, FID_PARAMETER_ID, 300
    
    ' Build the spreadsheet
    sprdEdit.MaxRows = 0
    sprdEdit.EditModeReplace = True
    ' load the spreadsheet data
    Set colParams = oTender.LoadMatches
    'For lngIndex = 1 To colParams.Count
    For Each oTender In colParams
        lngIndex = lngIndex + 1
'        Set oTender = colParams(lngIndex)
        Call AddRowToSprd(lngIndex, oTender)
        SetRowEditedFlag(lngIndex) = ROW_EDIT_TYPE_NONE
    Next 'lngIndex
    ' Sort by ID
    Call SortSpreadSheet
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If mEditedFlag = True Then
        Select Case MsgBox("Save Changes before exit? (Y/N)", vbYesNoCancel, "Parameter Editor")
            Case vbCancel
                Cancel = 1
            Case vbYes
                Call SaveParamChanges
                Cancel = 0
            Case vbNo
                Cancel = 0
        End Select
    End If
    
End Sub

Private Sub SortSpreadSheet()
    
    sprdEdit.SortKey(1) = 1
    sprdEdit.SortKeyOrder(1) = SortKeyOrderAscending
    ' Sort data in first five columns and rows by column 1 and 3
    sprdEdit.Sort -1, -1, -1, -1, SortByRow
    sprdEdit.ReDraw = True
    
End Sub

' Sets a parameter's edit status in the spreadsheet
Private Property Let SetRowEditedFlag(ByVal row As Long, ByVal Value As Long)
    
    With sprdEdit
        .row = row
        ' Add the ID
        .Col = SPRD_COL_ROW_EDITED_FLAG
        .Value = Value
        If Value = ROW_EDIT_TYPE_NONE Then
            ' do nothing
        ElseIf Value = ROW_EDIT_TYPE_DELETE Then
           .row = row
           .RowHidden = True
            mEditedFlag = True  ' save is needed
        Else
            mEditedFlag = True  ' save is needed
        End If
    End With
    If mEditedFlag = True Then
        cmdSave.Enabled = True
    End If

End Property

Private Property Get GetRowEditedFlag(ByVal row As Long) As Long
    
    With sprdEdit
        .row = row
        ' Add the ID
        .Col = SPRD_COL_ROW_EDITED_FLAG
        GetRowEditedFlag = .Value
    End With
    
End Property

Private Sub AddRowToSprd(ByVal row As Long, ByVal TenderRec As cTenderControl)
    
    With sprdEdit
        If row > .MaxRows Then
            .MaxRows = row
        End If
        .row = row
        ' Add the ID
        .Col = SPRD_COL_ID
        .CellType = CellTypeNumber
        .TypeNumberMin = 1
        .TypeNumberMax = 99999
        .TypeNumberDecPlaces = 0
        .Value = Val(TenderRec.TendType)
        .TypeMaxEditLen = 5
            
        ' Add the description
        .Col = SPRD_COL_DESC
        .Value = TenderRec.Description
        .TypeMaxEditLen = 20
           
        ' Add the parameter type
        .Col = SPRD_COL_INUSE
        If TenderRec.ValidForUse = True Then
            .Value = 1
        Else
            .Value = 0
        End If
            
    End With
End Sub

Private Sub sprdEdit_Click(ByVal Col As Long, ByVal row As Long)
    
    If Col = SPRD_COL_INUSE Then
       SetRowEditedFlag(row) = ROW_EDIT_TYPE_CHANGE
    End If
    
End Sub

Sub sprdedit_EditMode(ByVal Col As Long, ByVal row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    
    If Col = SPRD_COL_ID Then sprdEdit.EditMode = False
    
    If Mode = 0 Then            ' exiting edit mode
       SetRowEditedFlag(row) = ROW_EDIT_TYPE_CHANGE
    End If
    
End Sub

Private Sub sprdEdit_KeyPress(KeyAscii As Integer)

    If sprdEdit.ActiveCol = SPRD_COL_ID Then
        If KeyAscii < 128 Then KeyAscii = 0
    End If

End Sub

Private Sub sprdEdit_LeaveCell(ByVal Col As Long, ByVal row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    
    If Col = SPRD_COL_ID Then
        sprdEdit.Col = SPRD_COL_ID
        sprdEdit.row = sprdEdit.ActiveRow
        
        If Val(sprdEdit.Value) <= 0 Then
            Cancel = True
        End If
    
    End If
    
End Sub

' Updates the Parameter table through IBo
Private Sub SaveParamChanges()

    Dim oTender     As cTenderControl
    Dim lIndex      As Long
    Dim lSaveType   As Long
    
    On Error GoTo LError
    
    If mEditedFlag = True Then
        With sprdEdit
            For lIndex = 1 To .MaxRows
                .row = lIndex
                .Col = SPRD_COL_ROW_EDITED_FLAG
                lSaveType = .Value
                .Value = ROW_EDIT_TYPE_NONE
                If lSaveType <> ROW_EDIT_TYPE_NONE Then
                    Set oTender = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
                    .Col = SPRD_COL_ID
                    oTender.TendType = .Value
                    .Col = SPRD_COL_DESC
                    oTender.Description = .Value
                    .Col = SPRD_COL_INUSE
                    oTender.ValidForUse = .Value
                    Select Case lSaveType
                        Case ROW_EDIT_TYPE_ADD
                            Call oTender.SaveIfNew
                        Case ROW_EDIT_TYPE_CHANGE
                            If oTender.SaveIfExists = False Then
                                Call oTender.SaveIfNew
                            End If
                        Case ROW_EDIT_TYPE_DELETE
                            Call oTender.Delete
                    End Select
                End If
            Next lIndex
        End With
    
    cmdSave.Enabled = False
    mEditedFlag = False
    End If
    Exit Sub
    
LError:
    MsgBox "Error saving Tenders, line=" & lIndex & ", ID=" & oTender.TendType
    
End Sub

Public Function TenderIDIsUnique(ByVal TenderID As String) As Boolean
    
    Dim lIndex As Long
    
    sprdEdit.Col = SPRD_COL_ID
    For lIndex = 1 To sprdEdit.MaxRows
        sprdEdit.row = lIndex
        If TenderID = sprdEdit.Value Then
            Exit Function
        End If
    Next

    TenderIDIsUnique = True
    
End Function
Private Sub cmdAddRecord_Click()

    Dim row As Long
    Dim oTenderCtl As cTenderControl
    Dim lIndex As Long
    
    Set oTenderCtl = goDatabase.CreateBusinessObject(CLASSID_TENDERCONTROL)
    Load frmAddTenderType
    If frmAddTenderType.UserEnterNewRecord(oTenderCtl) = True Then
        row = sprdEdit.MaxRows + 1
        Call AddRowToSprd(row, oTenderCtl)
        SetRowEditedFlag(row) = ROW_EDIT_TYPE_ADD
    End If
    Unload frmAddTenderType
    
End Sub

Private Sub cmdDelete_Click()
    
    Dim strDesc As String
    With sprdEdit
        .row = .ActiveRow
        .Col = SPRD_COL_DESC
        If MsgBox("Delete the entry -""" & .Value & """(Y/N)?", vbYesNoCancel) = vbYes Then
            SetRowEditedFlag(.ActiveRow) = ROW_EDIT_TYPE_DELETE
        End If
    End With
    
End Sub

Private Sub cmdSave_Click()
  
  If mEditedFlag = True Then Call SaveParamChanges
    
End Sub

Private Sub cmdExit_Click()
    
    Unload Me
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF8:
            cmdDelete_Click
        Case vbKeyF10:
            cmdExit_Click
    End Select
    
End Sub
Private Sub Form_Resize()

Dim lSpacing As Long
    
    Dim iBtnCnt     As Long
    Dim iBtnWidth   As Long
    Dim iBtnHeight  As Long
    Dim i           As Long
    Dim iBtnGap     As Long
    Dim iBtnCInc    As Long
    Const BORDER_WIDTH = 60
        
    iBtnCnt = 4
    iBtnWidth = cmdAddRecord.Width
    iBtnHeight = cmdAddRecord.Height
    iBtnGap = cmdAddRecord.Top - sprdEdit.Top + sprdEdit.Height
    If Me.WindowState = vbMinimized Then Exit Sub
    lSpacing = sprdEdit.Left
    
    'Check form is not below minimum width
    If Me.Width < iBtnWidth * iBtnCnt + (lSpacing * 4) Then
        Me.Width = iBtnWidth * iBtnCnt + (lSpacing * 4)
       ' Exit Sub
    End If
    'Check form is not below minimum height
    If Me.Height < sprdEdit.Top + iBtnHeight * 3 + sbStatus.Height Then
        Me.Height = sprdEdit.Top + iBtnHeight * 3 + sbStatus.Height
        Exit Sub
    End If
    
    'start resizing
    sprdEdit.Width = Me.Width - sprdEdit.Left * 2 - BORDER_WIDTH '* 2
    sprdEdit.Height = Me.Height - (iBtnHeight + sprdEdit.Top + (lSpacing * 3) + (BORDER_WIDTH * 6) + sbStatus.Height)
    
    iBtnCInc = (sprdEdit.Width - iBtnWidth) / 3
    i = sprdEdit.Left
    cmdAddRecord.Left = i
    cmdDelete.Left = i + iBtnCInc
    cmdSave.Left = i + iBtnCInc * 2
    cmdExit.Left = i + iBtnCInc * 3
    
    i = sprdEdit.Top + sprdEdit.Height + lSpacing
    cmdAddRecord.Top = i
    cmdDelete.Top = i
    cmdSave.Top = i
    cmdExit.Top = i
    
    cmdDelete.Width = iBtnWidth
    cmdDelete.Height = iBtnHeight
    cmdSave.Width = iBtnWidth
    cmdSave.Height = iBtnHeight
    cmdExit.Width = iBtnWidth
    cmdExit.Height = iBtnHeight

End Sub

