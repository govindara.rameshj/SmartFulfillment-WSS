Attribute VB_Name = "modCustOrderEnq"
Option Explicit

Private Const MODULE_NAME  As String = "modCustOrderEnq"

Public Enum enmSortOrder
    soSkuOrder = 1
    soSkuDelDateOrder = 2
    soDelDateSku = 3
End Enum
