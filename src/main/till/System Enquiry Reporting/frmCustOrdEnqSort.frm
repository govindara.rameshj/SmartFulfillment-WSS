VERSION 5.00
Begin VB.Form frmCustOrdEnqSort 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Customer Order Enquiry Sort Option"
   ClientHeight    =   2760
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   Icon            =   "frmCustOrdEnqSort.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2760
   ScaleWidth      =   4680
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSort 
      Caption         =   "F4 - Sort"
      Default         =   -1  'True
      Height          =   375
      Left            =   2640
      TabIndex        =   1
      Top             =   1800
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "F10 - Exit"
      Height          =   375
      Left            =   720
      TabIndex        =   2
      Top             =   1800
      Width           =   1215
   End
   Begin VB.ComboBox cboSortOption 
      Height          =   315
      Left            =   720
      TabIndex        =   0
      Top             =   1140
      Width           =   3255
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   6  'Inside Solid
      BorderWidth     =   2
      DrawMode        =   1  'Blackness
      Height          =   2415
      Left            =   240
      Top             =   173
      Width           =   4215
   End
   Begin VB.Label lblSortoption 
      Alignment       =   2  'Center
      Caption         =   "Sort Option"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1193
      TabIndex        =   3
      Top             =   420
      Width           =   2295
   End
End
Attribute VB_Name = "frmCustOrdEnqSort"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MODULE_NAME  As String = "frmCustOrdEnqSort"

Private Sub cmdExit_Click()

'   Exit without sorting.
    frmSystemEnqReporting.Visible = True
    Unload Me
    
End Sub

Private Sub cmdSort_Click()

Const PROCEDURE_NAME As String = "cmdSort_Click"

    On Error GoTo cmdSort_Click_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmd Sort Click")

'   Sort the grid and exit this form.
    Select Case Me.cboSortOption.Text
        Case "By Sku by Order"
            frmSystemEnqReporting.SortOrder = soSkuOrder
        Case "By Sku by Del/Col Date by Order"
            frmSystemEnqReporting.SortOrder = soSkuDelDateOrder
        Case "By Del/Col Date by Sku"
            frmSystemEnqReporting.SortOrder = soDelDateSku
    End Select
    
    Call frmSystemEnqReporting.SetupGrid(True)
    frmSystemEnqReporting.Visible = True
    Unload Me
    
    Exit Sub
    
cmdSort_Click_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
'   Setup the form.
    Me.BackColor = frmSystemEnqReporting.BackColor
    Me.lblSortoption.BackColor = Me.BackColor

'   Load the combo box with the sort options.
    With Me.cboSortOption
        .AddItem ("By Sku by Order")
        .AddItem ("By Sku by Del/Col Date by Order")
        .AddItem ("By Del/Col Date by Sku")
        .ListIndex = 0
    End With
    
'   Disable the forms close button and system menu option.
    Call DisableCloseButton(Me)

    Exit Sub
    
FormLoad_Error:

    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyF4
                cmdSort_Click
                
            Case vbKeyF10
                cmdExit_Click
        End Select
    End If

End Sub
