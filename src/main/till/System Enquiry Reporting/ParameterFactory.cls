VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ParameterFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private m_FactoryMember As IParameterInterface

Private m_IsOn As Boolean

Public Function BooleanParameterRead(ByRef DatabaseConnection As IBoDatabase, ByVal ParameterId As Integer) As Boolean

     BooleanParameterRead = GetBooleanParameter(DatabaseConnection, ParameterId)
End Function

Public Function LongParameterRead(ByRef DatabaseConnection As IBoDatabase, ByVal ParameterId As Integer) As Long

     LongParameterRead = GetLongParameter(DatabaseConnection, ParameterId)
End Function


Private Function GetBooleanParameter(ByRef DatabaseConnection As IBoDatabase, ByVal ParameterId As Integer) As Boolean
   
   Dim goDatabase As IBoDatabase
   Dim cmd As New ADODB.Command
   Dim rsParameter As New ADODB.Recordset
   
   DebugMsg mClassName, "GetBooleanParameter", endlDebug, "Select BooleanValue from Parameters Where ParameterId = ? " & CStr(ParameterId)
   
   With cmd
      .ActiveConnection = DatabaseConnection.Connection
      .CommandType = adCmdText
      .CommandText = "Select BooleanValue from Parameters Where ParameterId = ? "
      .Parameters.Append cmd.CreateParameter("ParameterId", adInteger, adParamInput, , ParameterId)
   End With
   Set rsParameter = cmd.Execute

   GetBooleanParameter = False
   If rsParameter.BOF = False And rsParameter.EOF = False Then
      'rsParameter.MoveFirst
      GetBooleanParameter = rsParameter.Fields("BooleanValue")
   End If
End Function

Private Function GetLongParameter(ByRef DatabaseConnection As IBoDatabase, ByVal ParameterId As Integer) As Long
   
   Dim goDatabase As IBoDatabase
   Dim cmd As New ADODB.Command
   Dim rsParameter As New ADODB.Recordset
   
   DebugMsg mClassName, "GetBooleanParameter", endlDebug, "Select LongValue from Parameters Where ParameterId = ? " & CStr(ParameterId)
   With cmd
      .ActiveConnection = DatabaseConnection.Connection
      .CommandType = adCmdText
      .CommandText = "Select LongValue from Parameters Where ParameterId = ? "
      .Parameters.Append cmd.CreateParameter("ParameterId", adInteger, adParamInput, , ParameterId)
   End With
   Set rsParameter = cmd.Execute

   GetLongParameter = 0
   If rsParameter.BOF = False And rsParameter.EOF = False Then
      'rsParameter.MoveFirst
      GetLongParameter = rsParameter.Fields("LongValue")
   End If
End Function
