VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmSystemEnqReporting 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Wickes System Enquiries And Reporting"
   ClientHeight    =   7800
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11385
   Icon            =   "frmSystemEnqReporting.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   11385
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrTimeout 
      Interval        =   60000
      Left            =   60
      Top             =   1620
   End
   Begin TabDlg.SSTab sstabSystems 
      Height          =   7215
      Left            =   105
      TabIndex        =   16
      Top             =   120
      Width           =   11205
      _ExtentX        =   19764
      _ExtentY        =   12726
      _Version        =   393216
      Tabs            =   1
      TabHeight       =   520
      TabCaption(0)   =   "Customer Order Enquiries"
      TabPicture(0)   =   "frmSystemEnqReporting.frx":0A96
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSelect"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame fraSelect 
         BorderStyle     =   0  'None
         Height          =   6780
         Left            =   40
         TabIndex        =   17
         Top             =   360
         Width           =   11000
         Begin VB.CommandButton cmdRefresh 
            Caption         =   "F6 - Refresh"
            Height          =   500
            Left            =   9825
            TabIndex        =   26
            Top             =   811
            Width           =   1050
         End
         Begin VB.OptionButton optOrder 
            Caption         =   "Select by Order / Item"
            Height          =   255
            Left            =   440
            TabIndex        =   0
            Top             =   60
            Value           =   -1  'True
            Width           =   1900
         End
         Begin VB.Frame fraOrder 
            Height          =   1520
            Left            =   240
            TabIndex        =   23
            Top             =   60
            Width           =   4600
            Begin VB.ComboBox cboOrderNo 
               Height          =   315
               Left            =   1400
               TabIndex        =   1
               Top             =   400
               Width           =   3000
            End
            Begin VB.ComboBox cboSKUNo 
               Height          =   315
               Left            =   1400
               TabIndex        =   2
               Top             =   900
               Width           =   3000
            End
            Begin VB.Label lblOrderNo 
               Caption         =   "Order Number"
               Height          =   255
               Left            =   200
               TabIndex        =   25
               Top             =   440
               Width           =   1455
            End
            Begin VB.Label lblSkuNo 
               Caption         =   "SKU Number"
               Height          =   255
               Left            =   200
               TabIndex        =   24
               Top             =   940
               Width           =   1455
            End
         End
         Begin VB.OptionButton optPIC 
            Caption         =   "Select by PIC"
            Height          =   255
            Left            =   5300
            TabIndex        =   3
            Top             =   60
            Width           =   1300
         End
         Begin VB.Frame fraFilter 
            Height          =   615
            Left            =   240
            TabIndex        =   18
            Top             =   1560
            Width           =   9460
            Begin VB.CheckBox chkNonStock 
               Caption         =   "Non - Stock Items"
               Height          =   255
               Left            =   200
               TabIndex        =   6
               Top             =   240
               Value           =   1  'Checked
               Width           =   1700
            End
            Begin VB.CheckBox chkDeleted 
               Caption         =   "Obsolete / Deleted Items"
               Height          =   255
               Left            =   2633
               TabIndex        =   7
               Top             =   240
               Value           =   1  'Checked
               Width           =   2100
            End
            Begin VB.CheckBox chkOutOfStock 
               Caption         =   "Out Of Stock Items"
               Height          =   255
               Left            =   5466
               TabIndex        =   8
               Top             =   240
               Value           =   1  'Checked
               Width           =   1700
            End
            Begin VB.CheckBox chkOther 
               Caption         =   "All Other Items"
               Height          =   255
               Left            =   7900
               TabIndex        =   9
               Top             =   240
               Value           =   1  'Checked
               Width           =   1400
            End
         End
         Begin VB.CommandButton cmdSort 
            Caption         =   "F4 - Sort"
            Height          =   500
            Left            =   9825
            TabIndex        =   12
            Top             =   150
            Width           =   1050
         End
         Begin VB.CommandButton cmdPrint 
            Caption         =   "F9 - Print"
            Height          =   500
            Left            =   9825
            TabIndex        =   13
            Top             =   1472
            Width           =   1050
         End
         Begin VB.CommandButton cmdExit 
            Caption         =   "F10 - Exit"
            Height          =   500
            Left            =   9825
            TabIndex        =   14
            Top             =   2135
            Width           =   1050
         End
         Begin VB.CommandButton cmdResetFilter 
            Caption         =   "F3 - Reset Filter"
            Enabled         =   0   'False
            Height          =   375
            Left            =   240
            TabIndex        =   11
            Top             =   2260
            Width           =   4335
         End
         Begin VB.CommandButton cmdApplyFilter 
            Caption         =   "F5 - Apply Filter"
            Enabled         =   0   'False
            Height          =   375
            Left            =   5365
            TabIndex        =   10
            Top             =   2260
            Width           =   4335
         End
         Begin FPSpreadADO.fpSpread fpspdData 
            Height          =   3900
            Left            =   240
            TabIndex        =   15
            Top             =   2805
            Width           =   10635
            _Version        =   458752
            _ExtentX        =   18759
            _ExtentY        =   6879
            _StockProps     =   64
            AllowDragDrop   =   -1  'True
            ColsFrozen      =   3
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   13
            MaxRows         =   15
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmSystemEnqReporting.frx":0AB2
            UserResize      =   1
         End
         Begin VB.Frame fraPIC 
            Height          =   1520
            Left            =   5100
            TabIndex        =   19
            Top             =   60
            Width           =   4600
            Begin VB.ComboBox cboCountType 
               Height          =   315
               ItemData        =   "frmSystemEnqReporting.frx":122C
               Left            =   1400
               List            =   "frmSystemEnqReporting.frx":122E
               TabIndex        =   4
               Top             =   400
               Width           =   3000
            End
            Begin MSComCtl2.DTPicker dtpPICDate 
               Height          =   315
               Left            =   1400
               TabIndex        =   5
               Top             =   900
               Width           =   3000
               _ExtentX        =   5292
               _ExtentY        =   556
               _Version        =   393216
               Format          =   16187393
               CurrentDate     =   38587
            End
            Begin VB.TextBox txtAuditDate 
               Height          =   315
               Left            =   1400
               Locked          =   -1  'True
               TabIndex        =   27
               Top             =   900
               Width           =   3000
            End
            Begin VB.Label lblCountType 
               Caption         =   "Count Type"
               Height          =   255
               Left            =   200
               TabIndex        =   21
               Top             =   440
               Width           =   1455
            End
            Begin VB.Label lblDate 
               Caption         =   "Date"
               Height          =   255
               Left            =   200
               TabIndex        =   20
               Top             =   940
               Width           =   1455
            End
         End
      End
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   22
      Top             =   7425
      Width           =   11385
      _ExtentX        =   20082
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmSystemEnqReporting.frx":1230
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13361
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "22-Aug-05"
            TextSave        =   "22-Aug-05"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "17:05"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSystemEnqReporting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/System Enquiry Reporting/frmSystemEnqReporting.frm $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 12/08/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application that runs at Store level to display various reports and enquiries in
'*              a tabbed screen format.
'*
'**********************************************************************************************
'* Versions:
'*
'* 25/08/05 DaveF   v1.0.0  Initial build. WIX1160. Designed to display enquiries based
'*                          upon customer order details.
'*
'* 25/08/05 DaveF   v1.0.1  Update to add more debugging around recordset operations.
'*
'* 25/08/05 DaveF   v1.0.2  Update to add more debugging around recordset operations.
'*
'* 25/08/05 DaveF   v1.0.3  Update to add more debugging around recordset operations.
'*
'* 26/08/05 DaveF   v1.0.4  Update to use more INNER JOINS on the recordset SQL.
'*
'* 26/08/05 DaveF   v1.0.5  Update to include Print and screen sizing functionality.
'*
'* 26/08/05 DaveF   v1.0.6  Update to check for NULLs in the data
'*
'* 13/09/05 DaveF   v1.0.7  Update to handle cycle weeks that range from 0 - 5 and not 1 - 6.
'*
'* 21/10/05 DaveF   v1.0.8  Changed to filter the list differently and to use table AUDMAS and
'*                          not table PICAUD. Also includes the SOLD, QTYT and QTYR columns.
'*
'* 25/10/05 DaveF   v1.0.9  Changed to add extra debugging to method SetupGrid().
'*
'* 25/10/05 DaveF   v1.0.10 Changed to alter the filtering method, and to use HHTDET for the
'*                          Primary PIC data SKUN matching rather than working with cycle
'*                          days in PICCTL.
'*
'* 31/10/05 DaveF   v1.0.11 Changed to include the new filtering method.
'*
'* 31/10/05 DaveF   v1.0.12 Changed to fix the problem when sorting an empty recordset.
'*
'* 01/10/05 DaveF   v1.0.13 Changed to fix the problem of duplicate field names in 'Primary'
'*                          data.
'*
'* 01/10/05 DaveF   v1.0.14 Changed to alter the text shown when no data has been returned due
'*                          to the criteria the user set.
'*
'* 04/01/06 DaveF   v1.0.15 Changed to force the Order / Item data to be excluded if both the
'*                          Date is less than Today and the item was not sold.
'*
'* 06/01/06 DaveF   v1.0.16 Changed to show the correct Order Date when viewing Primary PIC
'*                          Data.
'*
'* 01/03/11 DaveT   v.3.0.2 Removed DELC check.  Replaced with check on CORHDR4.DELIVERYSTATUS.
'*
'* 12/09/11 MikeO   v3.0.5  Fix for referral 871 TFS Task Id of 2293. SQL server was using a Null
'*                          Till Id for Fulfillment orders not sold by the current store Id.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Private Const MODULE_NAME  As String = "frmSystemEnqReporting"

Private mcnnDBConnection As ADODB.Connection
Private mrecTempData     As ADODB.Recordset
Private mdatAuditDate    As Date
Private msoSortOrder     As enmSortOrder

'Error handler variables
Private errorNo As Long
Private errorDesc As String

Private Enum enmDataType
    dtOrder = 1
    dtPrimary = 2
    dtAudit = 3
End Enum

'   Form load event.
Private Sub Form_Load()

Const PROCEDURE_NAME As String = "Form_Load"

Dim strErrSource As String

    On Error GoTo FormLoad_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Form Load")
    
'   Get the system setup.
    Call GetRoot
    Call InitialiseStatusBar(sbStatus)
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Me.sstabSystems.BackColor = Me.BackColor
    
'   Disable the forms close button and system menu option.
'    Call DisableCloseButton(Me)
    
'   Set up the database connection.
    Set mcnnDBConnection = goSession.Database.Connection
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    If (mcnnDBConnection Is Nothing) Then
        Call MsgBox("The Database connection to DSN - " & goSession.Database.Connection.DefaultDatabase & " - Failed")
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
        Call Err.Report(MODULE_NAME, PROCEDURE_NAME, 0, False)
    End If

    Set mrecTempData = New ADODB.Recordset
    
'   Setup the Customer Order Enquiry screen.
    Call CustOrderEnqSetup
    
'   Get the Audit Date.
    Call GetAuditDate

    Exit Sub
    
FormLoad_Error:

    errorNo = Err.Number
    errorDesc = Err.Description
    
    If (Err.Source <> "") Then
        strErrSource = PROCEDURE_NAME & " - " & Err.Source
    Else
        strErrSource = PROCEDURE_NAME
    End If
    Call MsgBox("Module: " & strErrSource & vbCrLf & "Err No: " & errorNo & vbCrLf & "Err Desc: " & errorDesc, vbCritical + vbOKOnly, App.EXEName)
    Call DebugMsg(MODULE_NAME, strErrSource, endlDebug, "Error No - " & errorNo & " - Error Desc - " & errorDesc)
    Call Err.Report(MODULE_NAME, strErrSource, 0, False)
    
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Unload Me
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    mlngPassed = 0
    If Shift = 0 Then ' No shift/ctrl/alt etc. combinations
        Select Case KeyCode
            Case vbKeyReturn
                If (Me.ActiveControl.Name = "optOrder") Then
                    Me.cboOrderNo.SetFocus
                ElseIf (Me.ActiveControl.Name = "optPIC") Then
                    Me.cboCountType.SetFocus
                End If
                
            Case vbKeyF3
                cmdResetFilter_Click
            
            Case vbKeyF4
                cmdSort_Click
                
            Case vbKeyF5
                cmdApplyFilter_Click
                
            Case vbKeyF9
                cmdPrint_Click
            
            Case vbKeyF10
                cmdExit_Click
                
        End Select
    End If

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    mlngPassed = 0

End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set mrecTempData = Nothing
    
    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If

    Set mcnnDBConnection = Nothing
    
    End
    
End Sub

Private Function CustOrderEnqSetup() As Boolean

Const PROCEDURE_NAME As String = "CustOrderEnqSetup"
    
Dim recData  As ADODB.Recordset
Dim recOrder As ADODB.Recordset
Dim strSQL   As String

    On Error GoTo CustOrderEnqSetup_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Cust Order Enq Setup")
    
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    
'   Get the unique complete lists of order numbers and SKUNs.
    Set recOrder = New ADODB.Recordset
    strSQL = "SELECT DISTINCT a.NUMB FROM CORHDR a INNER JOIN CORHDR4 b ON a.NUMB = b.NUMB WHERE a.CANC <> '1' AND b.DELIVERYSTATUS < '900' AND a.NUMB IS NOT NULL ORDER BY a.NUMB"
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Order Numbers - " & strSQL)
    Call recOrder.Open(strSQL, mcnnDBConnection, adOpenForwardOnly, adLockOptimistic)

'   Setup the form display.
    With Me
'       Fill the combo boxes.
        With .cboOrderNo
            .Clear
            .AddItem ("ALL")
            Do Until (recOrder.EOF = True)
                .AddItem (recOrder.Fields("NUMB").Value)
                recOrder.MoveNext
            Loop
            .ListIndex = 0
        End With
        With .cboCountType
            .Clear
            .AddItem ("None")
            .AddItem ("Primary")
            .AddItem ("Audit")
            .ListIndex = 0
        End With
        .optOrder = True

'       Fill the data grid.
        msoSortOrder = soSkuOrder
        Call SetupGrid(False)
        .cmdResetFilter.Enabled = False
        .cmdApplyFilter.Enabled = False
        .dtpPICDate.Value = Date
    End With
    
    Set recOrder = Nothing
    
'   Setup the display for the current screen resolution.
    Call SetupCustOrdEnqDisplay
    
    CustOrderEnqSetup = True
    
    Exit Function
    
CustOrderEnqSetup_Error:

    errorNo = Err.Number
    errorDesc = Err.Description
    
    CustOrderEnqSetup = False

    If (mcnnDBConnection.State = adStateOpen) Then
        mcnnDBConnection.Close
    End If
    Set recOrder = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Function

'   Populate the SKUN combo box.
Private Function SetupSKUNs() As Boolean

Const PROCEDURE_NAME As String = "SetupSKUNs"
    
Dim recSKUN    As ADODB.Recordset
Dim strSQL     As String
Dim strSKUN    As String
Dim intCounter As Integer

    On Error GoTo SetupSKUNs_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setup SKUNs")
    
'   If previously a SKUN had been selected and the user has just selected 'ALL' orders then return to that SKUN
'       after reading in all the Order numbers.
    If (Me.cboSKUNo.Text <> "ALL") And (Me.cboOrderNo.Text = "ALL") Then
        strSKUN = Me.cboSKUNo.Text
    End If
    
'   Read in the data from the database.
    Set recSKUN = New ADODB.Recordset
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If

    '   Get the unique complete lists of SKUNs relative to the selected Order Number.
    If (Me.cboOrderNo.Text = "ALL") Then
        strSQL = "SELECT DISTINCT SKUN FROM CORLIN a INNER JOIN CORHDR b ON a.NUMB = b.NUMB INNER JOIN CORHDR4 c ON a.NUMB = c.NUMB " & _
                    "WHERE b.CANC <> '1' AND c.DELIVERYSTATUS < '900' AND a.DELIVERYSOURCE = '8" & goSession.CurrentEnterprise.IEnterprise_StoreNumber & _
                    "' ORDER BY SKUN"
    Else
        strSQL = "SELECT DISTINCT SKUN FROM CORLIN WHERE NUMB = '" & Me.cboOrderNo.Text & "' ORDER BY SKUN"
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Sku SQL - " & strSQL)
    Call recSKUN.Open(strSQL, mcnnDBConnection, adOpenForwardOnly, adLockOptimistic)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Recordset State - " & recSKUN.State)
    
'   Fill the combo box.
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Fill Sku Combo Box")
    With Me.cboSKUNo
        .Clear
        .AddItem ("ALL")
        Do Until (recSKUN.BOF = True) Or (recSKUN.EOF = True)
            If Not IsNull(recSKUN.Fields("SKUN").Value) Then
                .AddItem (recSKUN.Fields("SKUN").Value)
            End If
            recSKUN.MoveNext
        Loop
        .ListIndex = 0
    End With
'   If required re-find the previously selected SKUN.
    If (strSKUN <> "") Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Re-Fill Sku Combo Box")
        For intCounter = 0 To Me.cboSKUNo.ListCount - 1
            If (Me.cboSKUNo.List(intCounter) = strSKUN) Then
                Me.cboSKUNo.ListIndex = intCounter
                Exit For
            End If
        Next intCounter
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Fill Sku Combo Box Finished")
    
    Set recSKUN = Nothing
    
    SetupSKUNs = True
    
    Exit Function
    
SetupSKUNs_Error:

    errorNo = Err.Number
    errorDesc = Err.Description
    
    SetupSKUNs = False
    
    Set recSKUN = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Function

Public Function SetupGrid(blnSort As Boolean) As Boolean

    Const PROCEDURE_NAME As String = "SetupGrid"
    
    Dim strSQL         As String
    Dim lngRowCounter  As Long
    Dim lngOrderQty    As Long
    Dim lngQtyTaken    As Long
    Dim lngQtyRefunded As Long
    Dim strPrevSKUN    As String
    Dim blnTotals      As Boolean
    Dim lngRecords     As Long
    Dim OrderDateFieldImplementationFactory As OrderDateFieldFctry
    Dim OrderDateFieldImplementation As IOrderDateField

    On Error GoTo SetupGrid_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setup Grid")
    
    ' All all the filter check boxes are off then don't return any data.
    If (Me.chkNonStock = vbUnchecked) And (Me.chkDeleted = vbUnchecked) And _
            (Me.chkOutOfStock = vbUnchecked) And (Me.chkOther = vbUnchecked) Then
        Me.fpspdData.MaxRows = 0
        Call MsgBox("No data was found that matched the criteria you set." & vbCrLf & vbCrLf & _
                        "At least one of the filter check boxes must be ticked" & vbCrLf & _
                        " to retrieve any data.", vbInformation + vbOKOnly, _
                        "No Data Found")
        Exit Function
    End If
    
    If (blnSort = False) Then
        ' Get the required data based upon the set criteria.
        If (Me.optOrder.Value = True) Then
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Recordset - Order Data")
            Set mrecTempData = GetData(dtOrder)
        Else
            If (Me.cboCountType = "None") Then
                Call MsgBox("You have not selected a PIC Count Type." & vbCrLf & vbCrLf & _
                                "Please select a Count Type to continue.", vbInformation + vbOKOnly, _
                                "PIC Count Type Not Selected")
                Exit Function
            End If
            If (Me.cboCountType = "Primary") Then
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Recordset - PIC Primary Data")
                Set mrecTempData = GetData(dtPrimary)
            Else
                Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Recordset - PIC Audit Data")
                Set mrecTempData = GetData(dtAudit)
            End If
        End If
    ElseIf (Me.fpspdData.MaxRows = 0) Then
        Call MsgBox("There is no available data to sort.", vbInformation + vbOKOnly, "No Data To Sort")
        Exit Function
    End If
    
    If (mrecTempData.State = adStateOpen) Then
        If (mrecTempData.BOF = True) And (mrecTempData.EOF = True) Then
            Me.fpspdData.MaxRows = 0
            Call MsgBox("No data was found that matched the criteria you set.", vbInformation + vbOKOnly, _
                            "No Data Found")
            Exit Function
        Else
            mrecTempData.MoveFirst
            lngRecords = mrecTempData.RecordCount
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Recordset Source - " & mrecTempData.Source)
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Record Count - " & lngRecords)
        End If
    End If

    DebugMsg MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setting up OrderDateFieldImplementation"
    Set OrderDateFieldImplementationFactory = New OrderDateFieldFctry
    Set OrderDateFieldImplementation = OrderDateFieldImplementationFactory.FactoryGetOrderDateField
    DebugMsg MODULE_NAME, PROCEDURE_NAME, endlDebug, "Set up OrderDateFieldImplementation"

'   Fill the data grid.
    lngRowCounter = 1
    blnTotals = True
    If (lngRecords > 0) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Sort Data - " & msoSortOrder)
        Select Case msoSortOrder
            Case soSkuOrder
                mrecTempData.Sort = "SKUN ASC, NUMB ASC"
                Me.sbStatus.Panels(3).Text = "SORT: By Sku by Order"
            Case soSkuDelDateOrder
                mrecTempData.Sort = "SKUN ASC, DELD ASC, NUMB ASC"
                Me.sbStatus.Panels(3).Text = "SORT: By Sku by Del/Col Date by Order"
            Case soDelDateSku
                mrecTempData.Sort = "DELD ASC, SKUN ASC"
                Me.sbStatus.Panels(3).Text = "SORT: By Del/Col Date by Sku"
                blnTotals = False
        End Select
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Sort Order - " & Me.sbStatus.Panels(3).Text)
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Fill Grid Start")
    With Me.fpspdData
        '.ClearRange 0, 0, .MaxCols, .MaxRows, True
        .MaxRows = 0
        lngOrderQty = 0
        lngQtyTaken = 0
        lngQtyRefunded = 0
        strPrevSKUN = ""
        If (lngRecords > 0) Then
            Do Until (mrecTempData.EOF = True)
                .MaxRows = lngRowCounter
                .Row = lngRowCounter
                ' SKUN (SKU Number).
                If (mrecTempData.Fields("SKUN") <> strPrevSKUN) Then
                    ' Add the quantity total value if not on the first row.
                    If (.Row <> 1) And (blnTotals = True) Then
                        .Col = .GetColFromID("DELD")
                        .Value = "Total"
                        .FontBold = True
                        .Col = .GetColFromID("QTYO")
                        .Value = CStr(lngOrderQty)
                        .FontBold = True
                        .Col = .GetColFromID("QTYT")
                        .Value = CStr(lngQtyTaken)
                        .FontBold = True
                        .Col = .GetColFromID("QTYR")
                        .Value = CStr(lngQtyRefunded)
                        .FontBold = True
                        lngRowCounter = lngRowCounter + 1
                        .MaxRows = lngRowCounter
                        .Row = lngRowCounter
                        lngOrderQty = 0
                        lngQtyTaken = 0
                        lngQtyRefunded = 0
                    End If
                    ' SKUN (Sku number).
                    If Not IsNull(mrecTempData.Fields("SKUN")) Then
                        strPrevSKUN = mrecTempData.Fields("SKUN")
                    Else
                        strPrevSKUN = ""
                    End If
                    .Col = .GetColFromID("SKUN")
                    .Value = strPrevSKUN
                    ' DESCR (Description).
                    If Not IsNull(mrecTempData.Fields("DESCR")) Then
                        .Col = .GetColFromID("DESCR")
                        .Value = mrecTempData.Fields("DESCR")
                    End If
                    
                    ' INON (Non-Stock Status).
                    .Col = .GetColFromID("INON")
                    If IsNull(mrecTempData.Fields("INON")) Then
                        .Value = ""
                    ElseIf (mrecTempData.Fields("INON") = True) Then
                        .Value = "Yes"
                    Else
                        .Value = "No"
                    End If
                    ' IDEL (Deletion Status).
                    .Col = .GetColFromID("IDEL")
                    If IsNull(mrecTempData.Fields("IDEL")) And IsNull(mrecTempData.Fields("IOBS")) Then
                        .Value = ""
                    ElseIf (mrecTempData.Fields("IDEL") = True) Then
                        .Value = "Del"
                    ElseIf (mrecTempData.Fields("IOBS") = True) Then
                        .Value = "Obs"
                    Else
                        .Value = "No"
                    End If
                    ' ONHA (On Hand Stock).
                    If Not IsNull(mrecTempData.Fields("ONHA")) Then
                        .Col = .GetColFromID("ONHA")
                        .Value = CStr(mrecTempData.Fields("ONHA"))
                    End If
                End If
                ' NUMB (Order Number). Referenced the NUMB field explicitly
                If Not IsNull(mrecTempData.Fields.Item(0).Value) Then
                    .Col = .GetColFromID("NUMB")
                    .Value = mrecTempData.Fields.Item(0).Value
                End If
'                If Not IsNull(mrecTempData.Fields.Item("NUMB").Value) Then
'                    .Col = .GetColFromID("NUMB")
'                    .Value = mrecTempData.Fields("NUMB")
'                End If
                ' DATE1 (Order Date).
                If Not IsNull(mrecTempData.Fields(OrderDateFieldImplementation.GetFieldNameForRecordSet)) Then
                    .Col = .GetColFromID("DATE1")
                    .Value = DisplayDate(mrecTempData.Fields(OrderDateFieldImplementation.GetFieldNameForRecordSet), False)
                End If
                ' DELD (Delivery / Collection Date).
                .Col = .GetColFromID("DELD")
                If mrecTempData.Fields("IS_CLICK_AND_COLLECT") Then
                    .Value = "-"
                Else
                    If Not IsNull(mrecTempData.Fields("DELD")) Then
                        .Value = DisplayDate(mrecTempData.Fields("DELD"), False)
                    End If
                End If
                ' QTYO (Order Quantity).
                If Not IsNull(mrecTempData.Fields("QTYO")) Then
                    .Col = .GetColFromID("QTYO")
                    .Value = CStr(mrecTempData.Fields("QTYO"))
                    lngOrderQty = lngOrderQty + .Value
                End If
                ' QTYT (Quantity taken).
                If Not IsNull(mrecTempData.Fields("QTYT")) Then
                    .Col = .GetColFromID("QTYT")
                    .Value = CStr(mrecTempData.Fields("QTYT"))
                    lngQtyTaken = lngQtyTaken + .Value
                End If
                ' QTYR (Quantity refunded).
                If Not IsNull(mrecTempData.Fields("QTYR")) Then
                    .Col = .GetColFromID("QTYR")
                    .Value = CStr(mrecTempData.Fields("QTYR"))
                    lngQtyRefunded = lngQtyRefunded + .Value
                End If
                ' DELI (Delivery Status).
                .Col = .GetColFromID("DELI")
                If mrecTempData.Fields("IS_CLICK_AND_COLLECT") Then
                    .Value = "-"
                Else
                    If Not IsNull(mrecTempData.Fields("DELI")) Then
                        If (mrecTempData.Fields("DELI") = True) Then
                            .Value = "Delivery"
                        Else
                            .Value = "Collection"
                        End If
                    End If
                End If
                ' SOLD (Sold Status).
                .Col = .GetColFromID("SOLD")
                If IsNull(mrecTempData.Fields("SOLD")) Then
                    .Value = ""
                ElseIf (mrecTempData.Fields("SOLD") = True) Then
                    .Value = "Yes"
                Else
                    .Value = "No"
                End If
            
                lngRowCounter = lngRowCounter + 1
                mrecTempData.MoveNext
            Loop
            ' Add the quantity total value to the last row.
            If (.MaxRows <> 0) And (blnTotals = True) Then
                .MaxRows = lngRowCounter
                .Row = lngRowCounter
                .Col = .GetColFromID("DELD")
                .Value = "Total"
                .FontBold = True
                .Col = .GetColFromID("QTYO")
                .Value = CStr(lngOrderQty)
                .FontBold = True
                .Col = .GetColFromID("QTYT")
                .Value = CStr(lngQtyTaken)
                .FontBold = True
                .Col = .GetColFromID("QTYR")
                .Value = CStr(lngQtyRefunded)
                .FontBold = True
                lngRowCounter = lngRowCounter + 1
                .MaxRows = lngRowCounter
            End If
        End If
        
        ' Lock the grid.
        .Col = 1
        .Col2 = .MaxCols
        .Row = 1
        .Row2 = .MaxRows
        .BlockMode = True
        .Lock = True
        .Protect = True
        .BlockMode = False
    End With
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Fill Grid Finish")
    
    SetupGrid = True
    
    Exit Function
    
SetupGrid_Error:

    errorNo = Err.Number
    errorDesc = Err.Description
    
    SetupGrid = False
      
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Function

' Builds the SQL for the required data and returns a Recordset of that found data.
Private Function GetData(dtDataType As enmDataType) As ADODB.Recordset

    Const PROCEDURE_NAME As String = "GetData"

    Dim recData     As ADODB.Recordset
    Dim strSQL      As String
    Dim strSQLWHERE As String
    Dim OrderDateFieldImplementationFactory As OrderDateFieldFctry
    Dim OrderDateFieldImplementation As IOrderDateField

    On Error GoTo GetData_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Data")

    ' Get the main section of the SQL for the chosen data type.
    Dim myimplementation As ICustOrderInterface
    Dim OrderSelect As ICustOrderInterface
    Dim myfactory As CustOrderRepFactory

    Set myfactory = New CustOrderRepFactory
    Set myimplementation = myfactory.FactoryGet
    
    DebugMsg MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setting up OrderDateFieldImplementation"
    Set OrderDateFieldImplementationFactory = New OrderDateFieldFctry
    Set OrderDateFieldImplementation = OrderDateFieldImplementationFactory.FactoryGetOrderDateField
    DebugMsg MODULE_NAME, PROCEDURE_NAME, endlDebug, "Set up OrderDateFieldImplementation"
    
    Select Case dtDataType
        Case dtOrder
            ' Order / Item data.
            strSQL = myimplementation.OrderSelectSql(OrderDateFieldImplementation.GetFieldNameForQuery)
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Order / Item SQL - " & strSQL)
        Case dtPrimary
            ' PIC Primary count data.
            strSQL = myimplementation.PickCountSql(OrderDateFieldImplementation.GetFieldNameForQuery)
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIC Primary SQL - " & strSQL)
        Case dtAudit
            ' PIC Audit count data.
            strSQL = myimplementation.PickAuditSql(OrderDateFieldImplementation.GetFieldNameForQuery)
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "PIC Audit SQL - " & strSQL)
    End Select
    
    ' Build the WHERE clause for the specified criteria.
    strSQLWHERE = BuildWHEREClause(dtDataType)
    
    ' Complete the SQL.
    strSQL = strSQL & strSQLWHERE
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Full SQL - " & strSQL)
    
    ' Open the recordset
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    Set recData = New ADODB.Recordset
    recData.CursorLocation = adUseClient
    Call recData.Open(strSQL, mcnnDBConnection, adOpenKeyset, adLockOptimistic)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Recordset State - " & recData.State)
    
    ' Return the recordset.
    Set GetData = recData
    
    Set recData = Nothing
    
    Exit Function
    
GetData_Error:
     
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Set GetData = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Function

'   Builds the SQL WHERE clause based upon user selection.
Private Function BuildWHEREClause(dtDataType As enmDataType) As String

Const PROCEDURE_NAME As String = "BuildWHEREClause"
    
Dim strWHEREClause As String

    On Error GoTo BuildWHEREClause_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Build WHERE Clause")

'   Build the WHERE Clause.
    strWHEREClause = " AND "
    Select Case dtDataType
        Case dtOrder
            ' Only selected Order number.
            If (Me.cboOrderNo.Text <> "ALL") Then
                strWHEREClause = strWHEREClause & "a.NUMB = '" & cboOrderNo.Text & "' AND "
            End If
            ' Only selected SKU number.
            If (Me.cboSKUNo.Text <> "ALL") Then
                strWHEREClause = strWHEREClause & "b.SKUN = '" & cboSKUNo.Text & "' AND "
            End If
        Case dtPrimary
            strWHEREClause = strWHEREClause & "d.DATE1 = '" & Format(Me.dtpPICDate.Value, "yyyy-mm-dd") & "' AND "
        Case dtAudit
            ' Just retrieves all the SKUNs from AUDMAS, so no additional WHERE clause needed at this point.
    End Select
    
    ' Test if 'All Other Items' is checked.
    If (Me.chkOther = vbChecked) Then
        ' Build the 'All Other Items' style WHERE clause.
        ' Check if all the check boxes are checked, if so then no additions to WHERE clause.
        If (Me.chkNonStock = vbChecked) And (Me.chkDeleted = vbChecked) And (Me.chkOutOfStock = vbChecked) Then
            'No addition.
        ElseIf (Me.chkNonStock = vbChecked) Or (Me.chkDeleted = vbChecked) Or (Me.chkOutOfStock = vbChecked) Then
            ' First part of clause based on selections of NonStock, Deleted and OutOfStock.
            strWHEREClause = strWHEREClause & "(("
            If (Me.chkNonStock = vbChecked) Then
                strWHEREClause = strWHEREClause & "c.INON = 1 OR "
            End If
            If (Me.chkDeleted = vbChecked) Then
                strWHEREClause = strWHEREClause & "c.IOBS = 1 OR "
            End If
            If (Me.chkOutOfStock = vbChecked) Then
                strWHEREClause = strWHEREClause & "c.ONHA <= 0 OR "
            End If
            ' Remove the extra " OR " if it exists.
            If (Right(strWHEREClause, 4) = " OR ") Then
                strWHEREClause = Left(strWHEREClause, Len(strWHEREClause) - 4)
                strWHEREClause = strWHEREClause & ") OR ("
            End If
            ' Second part of clause based on selections of NonStock, Deleted and OutOfStock but in reverse for
            '   'All Other Items'.
            If (Me.chkNonStock = vbUnchecked) Then
                strWHEREClause = strWHEREClause & "c.INON = 0 AND "
            End If
            If (Me.chkDeleted = vbUnchecked) Then
                strWHEREClause = strWHEREClause & "c.IOBS = 0 AND "
            End If
            If (Me.chkOutOfStock = vbUnchecked) Then
                strWHEREClause = strWHEREClause & "c.ONHA > 0 AND "
            End If
            ' Remove the extra " OR " if it exists.
            If (Right(strWHEREClause, 5) = " AND ") Then
                strWHEREClause = Left(strWHEREClause, Len(strWHEREClause) - 5)
                strWHEREClause = strWHEREClause & "))"
            ElseIf (Right(strWHEREClause, 5) = " OR (") Then
                strWHEREClause = Left(strWHEREClause, Len(strWHEREClause) - 5)
                strWHEREClause = strWHEREClause & ")"
            End If
        Else
            ' Only 'All Other Items' where NonStock = False and Deleted = False and OutOfStock = False.
            strWHEREClause = strWHEREClause & "(c.INON = 0 AND c.IOBS = 0 AND c.ONHA > 0)"
        End If
    
    Else
        ' Build the simple style WHERE clause.
        If (Me.chkNonStock = vbChecked) Or (Me.chkDeleted = vbChecked) Or (Me.chkOutOfStock = vbChecked) Then
            strWHEREClause = strWHEREClause & "("
            If (Me.chkNonStock = vbChecked) Then
                strWHEREClause = strWHEREClause & "c.INON = 1 OR "
            End If
            If (Me.chkDeleted = vbChecked) Then
                strWHEREClause = strWHEREClause & "c.IOBS = 1 OR "
            End If
            If (Me.chkOutOfStock = vbChecked) Then
                strWHEREClause = strWHEREClause & "c.ONHA <= 0 OR "
            End If
            ' Remove the extra " OR " if it exists.
            If (Right(strWHEREClause, 4) = " OR ") Then
                strWHEREClause = Left(strWHEREClause, Len(strWHEREClause) - 4)
                strWHEREClause = strWHEREClause & ")"
            End If
        End If
    End If
    
'   Remove the extra " AND " if it exists.
    If (Right(strWHEREClause, 5) = " AND ") Then
        strWHEREClause = Left(strWHEREClause, Len(strWHEREClause) - 5)
    End If
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "WHERE Clause - " & strWHEREClause)

    BuildWHEREClause = strWHEREClause
    
    Exit Function
    
BuildWHEREClause_Error:

    errorNo = Err.Number
    errorDesc = Err.Description
    
    BuildWHEREClause = ""
      
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Function

'   Setup up the Customer Order Enquiry tab display for the screen resolution.
Private Function SetupCustOrdEnqDisplay() As Boolean

Const PROCEDURE_NAME As String = "SetupCustOrdEnqDisplay"
    
    On Error GoTo SetupCustOrdEnqDisplay_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Setup Cust Ord Enq Display")

'   Set the screen for various resolutions.
    With Me
        If (Screen.Width < 15360) Then
            ' 800 upwards.
            .Width = 11475
            .sstabSystems.Left = 105
            .sstabSystems.Width = 11205
            .fraSelect.Width = 11000
            .fraOrder.Left = 240
            .fraPIC.Left = 5100
            .fraFilter.Left = 240
            .cmdSort.Left = 9900
            .cmdRefresh.Left = 9900
            .cmdPrint.Left = 9900
            .cmdExit.Left = 9900
            .cmdResetFilter.Left = 240
            .cmdApplyFilter.Left = 5365
            .fpspdData.Left = 240
            .fpspdData.Width = 10635
            .optOrder.Left = 440
            .optPIC.Left = 5300
        Else
            ' 1024 upwards.
            .Width = 15300
            .sstabSystems.Left = 105
            .sstabSystems.Width = 15020
            .fraSelect.Width = 14880
            .fraOrder.Left = 2280
            .fraPIC.Left = 7140
            .fraFilter.Left = 2280
            .cmdSort.Left = 11940
            .cmdRefresh.Left = 11940
            .cmdPrint.Left = 11940
            .cmdExit.Left = 11940
            .cmdResetFilter.Left = 2280
            .cmdApplyFilter.Left = 7410
            .fpspdData.Left = 240
            .fpspdData.Width = 14460
            .optOrder.Left = 2480
            .optPIC.Left = 7340
        End If
        If (Screen.Height < 11520) Then
            ' 600 Upwards.
            .Height = 8280
            .sstabSystems = 7215
            .fraSelect = 6780
            .fpspdData.Height = 3900
        Else
            ' 768 upwards.
            .Height = 10440
            .sstabSystems.Height = 9375
            .fraSelect.Height = 8940
            .fpspdData.Height = 6060
        End If
    End With
    
    Exit Function
    
SetupCustOrdEnqDisplay_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)
    
End Function

'   Get the Audit date.
Private Function GetAuditDate() As Date

Const PROCEDURE_NAME As String = "GetAuditDate"
    
Dim recAudit As ADODB.Recordset
Dim strSQL   As String
    
    On Error GoTo GetAuditDate_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Get Audit Date")

    ' Get the audit date.
    strSQL = "SELECT AUDT FROM SYSDAT"
    If (mcnnDBConnection.State = adStateClosed) Then
        mcnnDBConnection.Open
    End If
    Set recAudit = mcnnDBConnection.Execute(strSQL, , adCmdText)
    If (recAudit.EOF = False) Then
        If Not IsNull(recAudit.Fields("AUDT")) Then
            mdatAuditDate = CDate(recAudit.Fields("AUDT"))
        Else
            mdatAuditDate = Empty
        End If
    End If
    
    Set recAudit = Nothing
    
    ' If required display the found date.
    If (Me.optPIC = True) Then
        Call cboCountType_Click
    End If
    
    Exit Function
    
GetAuditDate_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Set recAudit = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Function

'   Exit the system.
Private Sub cmdExit_Click()

    Unload Me
    
End Sub

Private Sub cmdPrint_Click()

Const PROCEDURE_NAME As String = "cmdPrint_Click"
    
    On Error GoTo cmdPrintClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmd Print Click")
    
'   Print the grid.
    With Me.fpspdData
        .PrintCenterOnPageH = True
        .PrintHeader = "/cCustomer Order Enquiry Report/n"
        .PrintFooter = "/n/l/date /time/rPage /p of /pc"
        .PrintColHeaders = True
        .PrintRowHeaders = True
        .PrintOrientation = PrintOrientationLandscape
        .PrintType = PrintTypeAll
        .PrintJobName = "Customer Order Enquiry Report"
        .PrintSheet (1)
    End With

    Exit Sub
    
cmdPrintClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cmdSort_Click()

Const PROCEDURE_NAME As String = "cmdSort_Click"
    
    On Error GoTo cmdSortClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmd Sort Click")

'   Open the form to allow user selection of grid sort order.
    frmCustOrdEnqSort.Show
    Me.Visible = False
    
    Exit Sub
    
cmdSortClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cmdRefresh_Click()
   
'   Get the Audit Date.
    Call GetAuditDate
    
    Call SetupGrid(False)
    
End Sub

Private Sub cmdApplyFilter_Click()

Const PROCEDURE_NAME As String = "cmdApplyFilter_Click"
    
    On Error GoTo cmdApplyFilterClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmd Apply Filter Click")

'   Fill the data grid.
    Call SetupGrid(False)
        
    Exit Sub
    
cmdApplyFilterClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cmdResetFilter_Click()

Const PROCEDURE_NAME As String = "cmdResetFilter_Click"
    
    On Error GoTo cmdResetFilterClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cmd Reset Filter Click")
    
'   Reset the controls on the form.
    With Me
        .cboOrderNo.ListIndex = 0
        .cboSKUNo.ListIndex = 0
        .cboCountType.ListIndex = 0
        .optOrder = True
        .dtpPICDate.Value = Date
        .chkNonStock = 1
        .chkDeleted = 1
        .chkOutOfStock = 1
        .chkOther = 1
    End With

'   Fill the data grid.
    Call SetupGrid(False)
    
    Exit Sub
    
cmdResetFilterClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cboOrderNo_Click()

Const PROCEDURE_NAME As String = "cboOrderNo_Click"
    
    On Error GoTo cboOrderNoClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cbo Order No Click")
    
    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True
    Me.optOrder = True

'   Populate the SKUN combo box based upon the Order No selection.
    Call SetupSKUNs
    
    Exit Sub
    
cboOrderNoClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cboSKUNo_Click()

Const PROCEDURE_NAME As String = "cboSKUNo_Click"
    
    On Error GoTo cboSKUNoClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cbo SKU No Click")
    
    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True
    Me.optOrder = True

    Exit Sub
    
cboSKUNoClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cboCountType_Click()

Const PROCEDURE_NAME As String = "cboCountType_Click"
    
    On Error GoTo cboCountTypeClick_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "cbo Count Type Click")
    
    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True
    Me.optPIC = True

    ' Display the Audit date.
    If (Me.cboCountType.Text = "Audit") Then
        Me.txtAuditDate.Visible = True
        Me.dtpPICDate.Visible = False
        If (mdatAuditDate <> "00:00:00") Then
            Me.txtAuditDate.Text = mdatAuditDate
        Else
            Me.txtAuditDate.Text = ""
        End If
    Else
        Me.txtAuditDate.Visible = False
        Me.dtpPICDate.Visible = True
    End If
    
    Exit Sub
    
cboCountTypeClick_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub dtpPICDate_Change()

Const PROCEDURE_NAME As String = "dtpPICDate_Change"
    
    On Error GoTo dtpPICDateChange_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "dtp PIC Date Change")
    
    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True
    Me.optPIC = True

    Exit Sub
    
dtpPICDateChange_Error:
    
    errorNo = Err.Number
    errorDesc = Err.Description
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & errorNo & " - Desc - " & errorDesc)
    Call Err.Raise(errorNo, PROCEDURE_NAME, errorDesc)

End Sub

Private Sub cboCountType_GotFocus()

    Me.optPIC.Value = True

End Sub

Private Sub cboOrderNo_GotFocus()

    Me.optOrder.Value = True
    
End Sub

Private Sub cboSKUNo_GotFocus()

    Me.optOrder.Value = True

End Sub

Private Sub dtpPICDate_GotFocus()

    Me.optPIC.Value = True

End Sub

Private Sub chkDeleted_Click()

    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True

End Sub

Private Sub chkNonStock_Click()

    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True

End Sub

Private Sub chkOther_Click()

    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True

End Sub

Private Sub chkOutOfStock_Click()

    Me.cmdResetFilter.Enabled = True
    Me.cmdApplyFilter.Enabled = True

End Sub

Public Property Get SortOrder() As enmSortOrder

    SortOrder = msoSortOrder
    
End Property

Public Property Let SortOrder(ByVal soSortOrder As enmSortOrder)

    msoSortOrder = soSortOrder
    
End Property

Private Sub fpspdData_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    mlngPassed = 0

End Sub

Private Sub optOrder_Click()

    Me.cmdApplyFilter.Enabled = True
    Me.cmdResetFilter.Enabled = True
    Me.cboCountType.ListIndex = 0
    Me.optOrder.Value = True

End Sub

Private Sub optPIC_Click()

    Me.cmdApplyFilter.Enabled = True
    Me.cmdResetFilter.Enabled = True
    
End Sub

Private Sub sstabSystems_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    mlngPassed = 0

End Sub

Private Sub tmrTimeout_Timer()

    mlngPassed = mlngPassed + 1
    If (mlngPassed >= goSession.GetParameter(PRM_SYSTEM_TIMEOUT)) And (Format(Now(), "HHMM") > "2000") Then End
    
End Sub
