VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OrderDateFieldLive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Implements IOrderDateField

Private Function IOrderDateField_GetFieldNameForQuery() As String

    IOrderDateField_GetFieldNameForQuery = "DATE1"
End Function

Private Function IOrderDateField_GetFieldNameForRecordSet() As String

    IOrderDateField_GetFieldNameForRecordSet = "DATE1"
End Function
