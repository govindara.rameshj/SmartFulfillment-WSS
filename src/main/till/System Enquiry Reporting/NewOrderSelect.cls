VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "NewOrderSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements ICustOrderInterface

Private Function ICustOrderInterface_OrderSelectSql(ByVal OrderDateField As String) As String
    ICustOrderInterface_OrderSelectSql = "SELECT a.NUMB, a." & OrderDateField & ", a.DELD, a.DELI, b.SKUN, b.QTYO, b.QTYT, b.QTYR, " & _
                        "c.DESCR, c.INON, c.IDEL, c.ONHA, c.IOBS, c.ICAT, c.IRIS, c.IMDN, c.NOOR, c.FODT, " & _
                        "d.SOLD, a.IS_CLICK_AND_COLLECT " & _
                        "FROM vwCORHDRFull a INNER JOIN CORHDR4 c4 ON a.NUMB = c4.NUMB " & _
                        "INNER JOIN CORLIN b ON a.NUMB = b.NUMB " & _
                        "LEFT JOIN STKMAS c ON b.SKUN = c.SKUN " & _
                        "LEFT JOIN QUOHDR d ON a.NUMB = d.NUMB " & _
                        "WHERE a.CANC <> '1' AND c4.DELIVERYSTATUS < '900' AND " & TillNumberFilter & " AND " & _
                        "b.DELIVERYSOURCE = '8" & goSession.CurrentEnterprise.IEnterprise_StoreNumber & "'"
End Function

Private Function ICustOrderInterface_PickCountSql(ByVal OrderDateField As String) As String
    ICustOrderInterface_PickCountSql = "SELECT a.NUMB, a." & OrderDateField & ", a.DELD, a.DELI, b.SKUN, b.QTYO, b.QTYT, b.QTYR, " & _
                        "c.DESCR, c.INON, c.IDEL, c.ONHA, c.IOBS , c.ICAT, c.IRIS, c.IMDN, c.NOOR, c.FODT, " & _
                        "e.SOLD, a.IS_CLICK_AND_COLLECT " & _
                        "FROM vwCORHDRFull a INNER JOIN CORHDR4 c4 ON a.NUMB = c4.NUMB " & _
                        "INNER JOIN CORLIN b ON a.NUMB = b.NUMB " & _
                        "LEFT JOIN STKMAS c ON b.SKUN = c.SKUN " & _
                        "INNER JOIN HHTDET d ON b.SKUN = d.SKUN " & _
                        "LEFT JOIN QUOHDR e ON a.NUMB = e.NUMB " & _
                        "WHERE a.CANC <> '1' AND c4.DELIVERYSTATUS < '900' AND " & TillNumberFilter & " AND " & _
                        "b.DELIVERYSOURCE = '8" & goSession.CurrentEnterprise.IEnterprise_StoreNumber & "'"
End Function

Private Function ICustOrderInterface_PickAuditSql(ByVal OrderDateField As String) As String
    ICustOrderInterface_PickAuditSql = "SELECT a.NUMB, a." & OrderDateField & ", a.DELD, a.DELI, b.SKUN, b.QTYO, b.QTYT, b.QTYR, " & _
                        "c.DESCR, c.INON, c.IDEL, c.ONHA, c.IOBS, c.ICAT, c.IRIS, c.IMDN, c.NOOR, c.FODT, " & _
                        "d.SOLD, a.IS_CLICK_AND_COLLECT " & _
                        "FROM vwCORHDRFull a INNER JOIN CORHDR4 c4 ON a.NUMB = c4.NUMB " & _
                        "INNER JOIN CORLIN b ON a.NUMB = b.NUMB " & _
                        "LEFT JOIN STKMAS c ON b.SKUN = c.SKUN " & _
                        "LEFT JOIN QUOHDR d ON a.NUMB = d.NUMB " & _
                        "INNER JOIN AUDMAS e ON b.SKUN = e.SKUN " & _
                        "WHERE a.CANC <> '1' AND c4.DELIVERYSTATUS < '900' AND " & TillNumberFilter & " AND " & _
                        "b.DELIVERYSOURCE = '8" & goSession.CurrentEnterprise.IEnterprise_StoreNumber & "'"
End Function

Private Function TillNumberFilter() As String
    
Const PROCEDURE_NAME As String = "TillNumberFilter"
    
    Dim result As String
    result = ""
    
    Dim tillNumbers As String
    tillNumbers = LTrim(RTrim(goSession.GetParameter(4700)))
    
    If Len(tillNumbers) > 0 Then
        Dim fieldName As String
        fieldName = "ISNULL(a.STIL,0)"
    
        Dim tillNumberRanges() As String
        tillNumberRanges = Split(tillNumbers, ";")
        
        For Each range In tillNumberRanges
            Dim rangeBoundaries() As String
            rangeBoundaries = Split(range, ":")
            
            If UBound(rangeBoundaries) < 0 Or UBound(rangeBoundaries) > 1 Then
                Err.Raise 1, PROCEDURE_NAME, "Check Till Number parameter in DB: incorrect format"
            End If
            
            If Not (IsTillNumberValid(rangeBoundaries(0)) And IsTillNumberValid(rangeBoundaries(UBound(rangeBoundaries)))) Then
                Err.Raise 2, PROCEDURE_NAME, "Check Till Number parameter in DB: invalid value"
            End If
            
            If Len(result) > 0 Then
                result = result & " OR "
            End If
            
            If UBound(rangeBoundaries) = 0 Then
                result = result & fieldName & " = '" & rangeBoundaries(0) & "'"
            Else
                result = result & fieldName & " >= '" & rangeBoundaries(0) & "' AND " & _
                fieldName & " <= '" & rangeBoundaries(UBound(rangeBoundaries)) & "'"
            End If
        Next range
    End If
    
    TillNumberFilter = "(" & result & ")"
End Function

Private Function IsTillNumberValid(tillNumber As String) As Boolean
    On Error GoTo NonNumeric

    Dim i As Integer
    i = CInt(tillNumber)
    
    IsTillNumberValid = True
    Exit Function

NonNumeric:
    IsTillNumberValid = False

End Function

