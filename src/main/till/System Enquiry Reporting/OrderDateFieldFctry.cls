VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OrderDateFieldFctry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const m_OrderDateFieldParameterID As Long = 980986
Private m_InitialisedUseOrderDateFieldImplementation As Boolean
Private m_UseOrderDateFieldImplementation As Boolean

Friend Property Get UseOrderDateFieldImplementation() As Boolean
    
    UseOrderDateFieldImplementation = m_UseOrderDateFieldImplementation
End Property

Public Function FactoryGetOrderDateField() As IOrderDateField
    
    If UseOrderDateFieldImplementation Then
        'using implementation with additional alias
        Set FactoryGetOrderDateField = New OrderDateFieldAlias
    Else
        'using live implementation
        Set FactoryGetOrderDateField = New OrderDateFieldLive
    End If
End Function

Private Sub Class_Initialize()

    GetUseOrderDateFieldParameterValue
End Sub

Friend Sub GetUseOrderDateFieldParameterValue()

    If Not m_InitialisedUseOrderDateFieldImplementation Then
On Error Resume Next
        m_UseOrderDateFieldImplementation = goSession.GetParameter(m_OrderDateFieldParameterID)
        m_InitialisedUseOrderDateFieldImplementation = True
On Error GoTo 0
    End If
End Sub

