VERSION 5.00
Begin VB.UserControl ucTillOrderItem 
   BackStyle       =   0  'Transparent
   ClientHeight    =   6255
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11790
   KeyPreview      =   -1  'True
   ScaleHeight     =   6255
   ScaleWidth      =   11790
   Begin VB.PictureBox fraOrderItem 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   6015
      Left            =   180
      ScaleHeight     =   5985
      ScaleWidth      =   11505
      TabIndex        =   0
      Top             =   120
      Width           =   11535
      Begin VB.TextBox txtBuyIn 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6960
         MaxLength       =   1
         TabIndex        =   2
         Text            =   "?"
         Top             =   4680
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txtTakeNow 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6960
         MaxLength       =   1
         TabIndex        =   1
         Text            =   "?"
         Top             =   4080
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "SKU"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   240
         TabIndex        =   22
         Top             =   780
         Width           =   795
      End
      Begin VB.Label lblTakeItemPrompt 
         BackStyle       =   0  'Transparent
         Caption         =   "Is this item to be taken now (Y/N)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   21
         Top             =   4080
         Visible         =   0   'False
         Width           =   6375
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Description"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1920
         TabIndex        =   20
         Top             =   780
         Width           =   2010
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "On Order"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2760
         TabIndex        =   19
         Top             =   2940
         Width           =   1650
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Qty"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   6360
         TabIndex        =   18
         Top             =   780
         Width           =   600
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "On-Hand"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   240
         TabIndex        =   17
         Top             =   2940
         Width           =   1575
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Rate"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   8280
         TabIndex        =   16
         Top             =   780
         Width           =   825
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Product Code"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   240
         TabIndex        =   15
         Top             =   1980
         Width           =   2415
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   9960
         TabIndex        =   14
         Top             =   780
         Width           =   915
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Reserved"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5640
         TabIndex        =   13
         Top             =   2940
         Width           =   1695
      End
      Begin VB.Label lblSKU 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   240
         TabIndex        =   12
         Top             =   1260
         Width           =   1455
      End
      Begin VB.Label lblDescription 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   1920
         TabIndex        =   11
         Top             =   1260
         Width           =   4215
      End
      Begin VB.Label lblQtyOnOrder 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   2760
         TabIndex        =   10
         Top             =   3420
         Width           =   1935
      End
      Begin VB.Label lblOrderQty 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   6360
         TabIndex        =   9
         Top             =   1260
         Width           =   1695
      End
      Begin VB.Label lblQtyOnHand 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   3420
         Width           =   1935
      End
      Begin VB.Label lblUnitPrice 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   8280
         TabIndex        =   7
         Top             =   1260
         Width           =   1455
      End
      Begin VB.Label lblProductCode 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   2460
         Width           =   2535
      End
      Begin VB.Label lblTotalPrice 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   9960
         TabIndex        =   5
         Top             =   1260
         Width           =   1335
      End
      Begin VB.Label lblQtyReserved 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   5640
         TabIndex        =   4
         Top             =   3420
         Width           =   1935
      End
      Begin VB.Label lblBuyInPrompt 
         BackStyle       =   0  'Transparent
         Caption         =   "Item to be ordered from supplier (Y/N)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   3
         Top             =   4680
         Visible         =   0   'False
         Width           =   6855
      End
   End
End
Attribute VB_Name = "ucTillOrderItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const MODULE_NAME As String = "ucTillOrderItem"

Dim moParent As Form

Dim mblnPreview  As Boolean
Dim mblnInit     As Boolean
Dim mblnResponse As Boolean
Dim mblnEscaped  As Boolean

Dim mstrValueFmt As String
Dim mstrQtyFmt   As String

Dim mblnTakeItemNow As Boolean
Dim mblnBuyInItem   As Boolean

Private Sub txtBuyIn_KeyPress(KeyAscii As Integer)

Dim strKeyIn As String

    'ensure only Y and N keys are accepted
    strKeyIn = UCase(Chr(KeyAscii))
    If (strKeyIn <> "Y") And (strKeyIn <> "N") And (KeyAscii <> vbKeyReturn) And (KeyAscii <> vbKeyEscape) Then KeyAscii = 0
    
    If (strKeyIn = "Y") Or (strKeyIn = "N") Then
        txtBuyIn.Text = strKeyIn
        KeyAscii = 0
    End If
    
    'Process Enter key if valid Y/N has been pressed
    If (KeyAscii = vbKeyReturn) Then
        If (txtBuyIn.Text = "?") Then
            KeyAscii = 0
            Exit Sub
        Else
            If txtBuyIn.Text = "Y" Then
                mblnBuyInItem = True
            Else
                mblnBuyInItem = False
            End If
            mblnResponse = True
        End If
    End If
    
    'Process cancellation
    If (KeyAscii = vbKeyEscape) Then
        txtTakeNow.SetFocus
        lblBuyInPrompt.Visible = False
        txtBuyIn.Visible = False
    End If

    KeyAscii = 0

End Sub

Private Sub txtTakeNow_KeyPress(KeyAscii As Integer)

Dim strKeyIn As String

    'ensure only Y and N keys are accepted
    strKeyIn = UCase(Chr(KeyAscii))

    If (strKeyIn <> "Y") And (strKeyIn <> "N") And (KeyAscii <> vbKeyReturn) And (KeyAscii <> vbKeyEscape) Then KeyAscii = 0
    
    If (strKeyIn = "Y") Or (strKeyIn = "N") Then
        txtTakeNow.Text = strKeyIn
        KeyAscii = 0
    End If
    
    'Process Enter key if valid Y/N has been pressed
    If (KeyAscii = vbKeyReturn) Then
        If (txtTakeNow.Text = "?") Then
            KeyAscii = 0
            Exit Sub
        Else
            If (txtTakeNow.Text = "Y") Then
                mblnTakeItemNow = True
                mblnResponse = True
            Else
                mblnTakeItemNow = False
                lblBuyInPrompt.Visible = True
                txtBuyIn.Visible = True
                txtBuyIn.SetFocus
            End If
        End If
    End If 'processing Enter key
    
    'Process cancellation
    If (KeyAscii = vbKeyEscape) Then
    End If

    KeyAscii = 0

End Sub

Private Sub UserControl_Hide()
    
    Call DebugMsg(MODULE_NAME, "UC_Hide", endlDebug, "Resetting preview")
    If Not moParent Is Nothing Then moParent.KeyPreview = mblnPreview

End Sub

Private Sub UserControl_Initialize()
    
    Call DebugMsg(MODULE_NAME, "Initialize", endlDebug)

End Sub

Private Sub UserControl_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then
    End If

End Sub

Private Sub UserControl_Resize()
    
    UserControl.Width = 11790
    UserControl.Height = 6300

End Sub

'<CACH>****************************************************************************************
'* Function:  Boolean GetItem()
'**********************************************************************************************
'* Description: Used to retrieve an Item given the SKU and display in spread sheet.  Also used
'*              to initialise line values to 0 as each item is selected/ displayed.
'**********************************************************************************************
'* Parameters:
'*In/Out:strPartCode  String.-Part Code used to retrieve item - must be ready formatted
'**********************************************************************************************
'* Returns:  Boolean
'**********************************************************************************************
'* History:
'* 15/11/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Private Function GetItem(strPartCode As String) As Boolean

Dim oItem   As Object
Dim lRow    As Long
Dim sErrMsg As String

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetItem"

    On Error GoTo Catch
Try:
    'Main procedure code goes here
                
    Set oItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItem.IBo_AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_PartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_Description)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_SupplierPartCode)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityCustOrder)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityOnOrder)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityOnDisplay)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_NormalSellPrice)
    Call oItem.IBo_AddLoadField(FID_INVENTORY_QuantityToUOM)
    
    Call oItem.IBo_LoadMatches
    
    If oItem.Description = "" Then
        Exit Function
    End If

    lblSKU.Caption = oItem.PartCode
    lblDescription.Caption = oItem.Description
    
    lblQtyOnHand.Caption = Format(oItem.QuantityAtHand)
    lblQtyReserved.Caption = Format(oItem.QuantityCustOrder)
    lblProductCode.Caption = oItem.SupplierPartCode
'    lblQtyOnHand.Caption = Format(oItem.QuantityOnDisplay)
    lblQtyOnOrder.Caption = Format(oItem.QuantityOnOrder)
'    sprdLines.Col = COL_COST
'    sprdLines.Col = COL_ONHAND
'    sprdLines.Col = COL_BAND
'    sprdLines.Col = COL_SELLPRICE
'    sprdLines.Col = COL_LOOKEDUP
'    sprdLines.Col = COL_QTYSELLIN
'    sprdLines.Col = COL_INCTOTAL
'    sprdLines.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_SELLPRICE) & "# / " & ColToText(COL_QTYSELLIN) & "#"
'    sprdLines.Col = COL_TOTCOST
'    sprdLines.Formula = ColToText(COL_QTY) & "# * " & ColToText(COL_COST) & "# / " & ColToText(COL_QTYSELLIN) & "#"
'    sprdLines.Text = oItem.DepartmentCode
    Set oItem = Nothing
    GetItem = True

    'Tidy up
    GoSub Finally

    Exit Function

    '<CAEH>
Catch:
    Call Err.Push 'Preserve Error Code
    'Tidy up.
    GoSub Finally

    Call Err.Pop 'Retrieve Error Code
    'Re-raise error to next handler up the chain.
'    Call Err.Bubble(APP_NAME, PROCEDURE_NAME, Erl)
    Exit Function

Finally:
    'Perform tidy up here

    Return
    '</CAEH>

End Function 'GetItem

Public Function ConfirmOrderItem(strPartCode As String, _
                                 dblQuantity As Double, _
                                 dblUnitPrice As Double, _
                                 ByRef blnTakeItemNow As Boolean, _
                                 ByRef blnBuyInItem As Boolean)

    mblnEscaped = False
    mblnResponse = False
    Call GetItem(strPartCode)
    lblTakeItemPrompt.Visible = True
    txtTakeNow.Text = "?"
    txtTakeNow.Visible = True
    txtTakeNow.SetFocus
    txtBuyIn.Text = "?"
    txtBuyIn.Visible = False
    lblBuyInPrompt.Visible = False
    lblOrderQty.Caption = Format(dblQuantity, mstrQtyFmt)
    lblUnitPrice.Caption = Format(dblUnitPrice, mstrValueFmt)
    lblTotalPrice.Caption = Format(dblUnitPrice * dblQuantity, mstrValueFmt)

    While (mblnResponse = False) And (mblnEscaped = False)
        DoEvents
    Wend
    If mblnEscaped = True Then
        ConfirmOrderItem = False
    Else
        ConfirmOrderItem = True
    End If
    blnTakeItemNow = mblnTakeItemNow
    blnBuyInItem = mblnBuyInItem
    
End Function
Public Sub UserControl_Show()

Dim strMsg As String
    
    If moParent Is Nothing Then
        strMsg = "No Parent"
    Else
        strMsg = "Parent=" & moParent.Name & "-" & moParent.KeyPreview
    End If
    Call DebugMsg(MODULE_NAME, "SHOW", endlDebug, "Parent is " & strMsg)
    If Not moParent Is Nothing Then
        mblnPreview = moParent.KeyPreview
        moParent.KeyPreview = False
    End If
'    txtFilterOrderNo.SetFocus


End Sub

Public Sub Initialise(ByRef CurrentSession As Object, Optional ByRef ParentForm As Object = Nothing)

Dim lngQtyDecNum As Long
Dim lngValueDecNum As Long

    Call DebugMsg(MODULE_NAME, "UC_EnterFocus", endlTraceIn, "Initialise-" & mblnInit)
    If mblnInit Then Exit Sub
    ' Set up the object and field that we wish to select on
    DoEvents
    Set moParent = ParentForm
    
    Set goSession = CurrentSession
    Set goDatabase = goSession.Database
    
    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    lngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    If lngValueDecNum > 0 Then
        mstrValueFmt = "0." & Space$(lngValueDecNum)
        mstrValueFmt = Replace(mstrValueFmt, " ", "0")
    Else
        mstrValueFmt = "0"
    End If
    If lngQtyDecNum > 0 Then
        mstrQtyFmt = "0." & Space$(lngQtyDecNum)
        mstrQtyFmt = Replace(mstrQtyFmt, " ", "0")
    Else
        mstrQtyFmt = "0"
    End If
    
    mblnInit = True
    Call DebugMsg(MODULE_NAME, "UC_EnterFocus", endlTraceOut, "Initialise-" & mblnInit)

End Sub

