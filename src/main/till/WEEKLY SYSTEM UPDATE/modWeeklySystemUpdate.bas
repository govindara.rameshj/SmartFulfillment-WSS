Attribute VB_Name = "modWeeklySystemUpdate"
'<CAMH>****************************************************************************************
'* Module : modWeeklySystemUpdate
'* Date   : 04/05/04
'* Author :
'*$Archive: /Projects/OasysV2/VB/Weekly System Update/modWeeklySystemUpdate.bas $Author:  $ $Date: 25/05/04 10:10 $ $Revision: 1 $
'* Versions:
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME             As String = "modWeeklySystemUpdate"
Const MODULE_FUNCTION         As String = "Weekly System Update"
Const DISPLAY_UPDATE_INTERVAL As Long = 1

Const LOADING_MESSAGE         As String = "Loading Data..."
Const PROCESSING_MESSAGE      As String = "Processing Data..."
Const BLANK_MESSAGE           As String = vbNullString

Const DEPARTMENT              As String = "DEPARTMENT"
Const SUPPLIERCYCLE           As String = "SUPPLIERCYCLE"
Const STOCKMASTER             As String = "STOCKMASTER"
Const RELATEDITEM             As String = "RELATEDITEM"
Const GMROI                   As String = "GMROI"

Const STATUS_FILENAME         As String = "OASYSWSU.STA"

Dim mblnFromNightlyClose      As Boolean

' Fields used for ADO data access
Dim mcon                As ADODB.Connection
Dim mcmd                As ADODB.Command
Dim mrsSysDates         As ADODB.Recordset
Dim mstrStartingWith    As String
Dim mstrStatusFilePath  As String

Public Sub Main()
Dim strUpdateDay            As String
Dim strStartingWithMessage  As String
Dim intStatusFileNo         As Integer
    
Const PROCEDURE_NAME As String = MODULE_NAME & ".Main"
    
    'Initialise Oasys objects
    GetRoot
    
    Call DecodeParameters(goSession.ProgramParams)
    
    Set mcon = New ADODB.Connection
    mcon.Open goDatabase.ConnectionString
    
    Set mcmd = New ADODB.Command
    mcmd.ActiveConnection = mcon
    mcmd.CommandType = adCmdText
    
    mstrStartingWith = vbNullString
    strStartingWithMessage = vbNullString

    mstrStatusFilePath = App.Path & "\" & STATUS_FILENAME
    
    'Check for status file from failed update.
    If LenB(Dir(mstrStatusFilePath, vbNormal)) <> 0 Then
        intStatusFileNo = FreeFile()
        Open mstrStatusFilePath For Input As intStatusFileNo
        
        If EOF(intStatusFileNo) = False Then
            Line Input #intStatusFileNo, mstrStartingWith
            mstrStartingWith = Trim$(mstrStartingWith)
        End If
        
        Close intStatusFileNo
    End If
    
    If mblnFromNightlyClose = False Then
        Call ReadSysDates
    
        Select Case mstrStartingWith
            Case DEPARTMENT
                strStartingWithMessage = "STARTING WITH DEPARTMENT FILE UPDATE"
            Case SUPPLIERCYCLE
                strStartingWithMessage = "STARTING WITH SUPPLIER CYCLE COUNT UPDATE"
            Case STOCKMASTER
                strStartingWithMessage = "STARTING WITH STOCK MASTER FILE UPDATE"
            Case RELATEDITEM
                strStartingWithMessage = "STARTING WITH RELATED ITEM FILE UPDATE"
            Case GMROI
                strStartingWithMessage = "STARTING WITH GMROI FILE UPDATE"
        End Select
        
        Select Case mrsSysDates!Wend
            Case 1
                strUpdateDay = "Monday"
            Case 2
                strUpdateDay = "Tuesday"
            Case 3
                strUpdateDay = "Wednesday"
            Case 4
                strUpdateDay = "Thursday"
            Case 5
                strUpdateDay = "Friday"
            Case 6
                strUpdateDay = "Saturday"
            Case 7
                strUpdateDay = "Sunday"
        End Select
        
        If LenB(strStartingWithMessage) = 0 Then
            If MsgBox("Continue with Weekly Update for " & _
                strUpdateDay & ", Week Ending " & Format$(mrsSysDates!WK161, "Short Date") & "?", _
                vbYesNo + vbQuestion, MODULE_FUNCTION) = vbNo Then
                End
            End If
        Else
            If MsgBox("Continue with Weekly Update for " & _
                strUpdateDay & ", Week Ending " & _
                Format$(mrsSysDates!WK161, "Short Date") & "?" & vbCr & vbCr & _
                strStartingWithMessage, _
                vbYesNo + vbQuestion, MODULE_FUNCTION) = vbNo Then
                End
            End If
        End If
    ElseIf LenB(mstrStartingWith) <> 0 Then
' Put this in here because I believe that if this update is run from the nightly close
' it should not be starting in the middle somewhere. I could be wrong about that though.
        Err.Raise OASYS_ERR_INCOMPLETE_SYSTEM_UPDATE, PROCEDURE_NAME, _
            "Found Weekly System Update Status File."
    End If
    
    frmWeeklySystemUpdate.Show
    DoEvents
    frmWeeklySystemUpdate.lngUpdateInterval = DISPLAY_UPDATE_INTERVAL
    Call PerformWeeklySystemUpdate

    If mblnFromNightlyClose = False Then
        Call MsgBox("Weekly System File Update Complete.", vbInformation, MODULE_FUNCTION)
    End If
    End
End Sub

Private Sub DecodeParameters(strCommand As String)

Const CALLED_FROM As String = "CF"
Const CF_CLOSE    As String = "C"

Dim strTempDate As String
Dim dteEndDate  As Date
Dim strSection  As String
Dim vntSection  As Variant
Dim lngSectNo   As Long
Dim blnSetOpts  As Boolean

    vntSection = Split(strCommand, ",")
    
    mblnFromNightlyClose = False
    If UBound(vntSection) = 0 Then Exit Sub
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        
        Select Case (Left$(strSection, 2))
        Case CF_CLOSE
            Debug.Print Mid$(strSection, 4)
            If Mid$(strSection, 3) = CF_CLOSE Then
                mblnFromNightlyClose = True
            End If
        End Select
       
    Next lngSectNo
    
End Sub

Public Sub PerformWeeklySystemUpdate()

Const PROCEDURE_NAME As String = MODULE_NAME & ".PerformWeeklySystemUpdate"

    On Error GoTo errHandler

    Select Case mstrStartingWith
        Case DEPARTMENT
            GoTo DepartmentUpdate
        Case SUPPLIERCYCLE
            GoTo SupplierCycleUpdate
        Case STOCKMASTER
            GoTo StockMasterUpdate
        Case RELATEDITEM
            GoTo RelatedItemUpdate
        Case GMROI
            GoTo GMROIUpdate
    End Select

    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = "Updating Cashier File"
    End If
    Call UpdateCashierFile
    
    Call OutputStatusFile(DEPARTMENT)
    
DepartmentUpdate:
    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = "Updating Department File"
    End If
    Call UpdateDepartmentFile

    Call OutputStatusFile(SUPPLIERCYCLE)

SupplierCycleUpdate:
    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = "Rolling Supplier Cycle Count Week"
    End If
    Call RollSupplierCycleCountWeek

    Call OutputStatusFile(STOCKMASTER)

StockMasterUpdate:
    Call ReadSysDates
    
    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = "Updating Stock Master File"
    End If
    Call UpdateStockMasterFile

    Call OutputStatusFile(RELATEDITEM)

RelatedItemUpdate:
    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = "Updating Related Item File"
    End If
    Call UpdateRelatedItemFile

    Call OutputStatusFile(GMROI)

GMROIUpdate:
    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = "Updating GMROI Files"
    End If
    Call UpdateGMROIFiles

    Call OutputStatusFile(vbNullString)
    
    If frmWeeklySystemUpdate.Visible Then
        frmWeeklySystemUpdate.strStatus = vbNullString
        frmWeeklySystemUpdate.strStatusMessage = BLANK_MESSAGE
    End If
    
    Call Successful_End
    
    Exit Sub
    
errHandler:

    Call MsgBox("Module: " & PROCEDURE_NAME & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, PROCEDURE_NAME)
    End
End Sub

Private Sub Successful_End()
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Erase file so that close program will know that this program
' completed successfully
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If LenB(Dir("\TASKCOMP")) <> 0 Then
        Kill ("\TASKCOMP")
    End If
End Sub

Private Sub ReadSysDates()
    If mrsSysDates Is Nothing Then
        Set mrsSysDates = New ADODB.Recordset
    Else
        If mrsSysDates.State <> adStateClosed Then
            mrsSysDates.Close
        End If
    End If
    
    mrsSysDates.ActiveConnection = mcon
    mrsSysDates.CursorLocation = adUseClient
    mrsSysDates.CursorType = adOpenStatic
    
    mrsSysDates.Open "Select WEND, WK161, WK162, CYCW, CYNO, DAYS From SYSDAT Where FKEY = '01'"
    
    mrsSysDates.MoveFirst
End Sub

Private Sub UpdateCashierFile()
    
    On Error GoTo RollbackTrans
    
    mcon.BeginTrans
    
    mcmd.CommandText = "Update CASMAS Set SKUL4 = SKUL4 + SKUL2, DPTL4 = DPTL4 + DPTL2, " & _
        "SKUS4 = SKUS4 + SKUS2, DPTS4 = DPTS4 + DPTS2, SKUC4 = SKUC4 + SKUC2, " & _
        "DPTC4 = DPTC4 + DPTC2, PVNO4 = PVNO4 + PVNO2, PVVA4 = PVVA4 + PVVA2, " & _
        "LREV4 = LREV4 + LREV2, SKUL3 = SKUL2, DPTL3 = DPTL2, SKUS3 = SKUS2, " & _
        "DPTS3 = DPTS2, SKUC3 = SKUC2, DPTC3 = DPTC2, PVNO3 = PVNO2, " & _
        "PVVA3 = PVVA2, LREV3 = LREV2, SKUL2 = 0, DPTL2 = 0, SKUS2 = 0, " & _
        "DPTS2 = 0, SKUC2 = 0, DPTC2 = 0, PVNO2 = 0, PVVA2 = 0, LREV2 = 0 " & _
        "Where DELC = 0"
    
    mcmd.Execute

    mcmd.CommandText = "Delete From CASMAS Where DELC = 1"
    
    mcmd.Execute

    mcon.CommitTrans
    
    Exit Sub

RollbackTrans:
    mcon.RollbackTrans
    Call Err.Raise(Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext)
End Sub

Private Sub UpdateDepartmentFile()
    
    On Error GoTo RollbackTrans
    
    mcon.BeginTrans
    
    mcmd.CommandText = "Update DPTMAS Set SKUL4 = SKUL4 + SKUL2, DPTL4 = DPTL4 + DPTL2, " & _
        "SKUS4 = SKUS4 + SKUS2, DPTS4 = DPTS4 + DPTS2, SKUC4 = SKUC4 + SKUC2, " & _
        "DPTC4 = DPTC4 + DPTC2, PVNO4 = PVNO4 + PVNO2, PVVA4 = PVVA4 + PVVA2, " & _
        "SKUL3 = SKUL2, DPTL3 = DPTL2, SKUS3 = SKUS2, DPTS3 = DPTS2, SKUC3 = SKUC2, " & _
        "DPTC3 = DPTC2, PVNO3 = PVNO2, PVVA3 = PVVA2, SKUL2 = 0, DPTL2 = 0, SKUS2 = 0, " & _
        "DPTS2 = 0, SKUC2 = 0, DPTC2 = 0, PVNO2 = 0, PVVA2 = 0, WSVL6 = WSVL5, " & _
        "WSVL5 = WSVL4, WSVL4 = WSVL3, WSVL3 = WSVL2, WSVL2 = Truncate(WSVL1, 0), " & _
        "WSVL1 = 0, WLUP6 = WLUP5, WLUP5 = WLUP4, WLUP4 = WLUP3, WLUP3 = WLUP2, " & _
        "WLUP2 = WLUP1, WLUP1 = 0, WOOS6 = WOOS5, WOOS5 = WOOS4, WOOS4 = WOOS3, " & _
        "WOOS3 = WOOS2, WOOS2 = WOOS1, WOOS1 = 0"
    
    mcmd.Execute

    mcmd.CommandText = "Update DPTGRP Set WSVL6 = WSVL5, WSVL5 = WSVL4, WSVL4 = WSVL3, " & _
        "WSVL3 = WSVL2, WSVL2 = Truncate(WSVL1, 0), WSVL1 = 0, WLUP6 = WLUP5, " & _
        "WLUP5 = WLUP4, WLUP4 = WLUP3, WLUP3 = WLUP2, WLUP2 = WLUP1, WLUP1 = 0, " & _
        "WOOS6 = WOOS5, WOOS5 = WOOS4, WOOS4 = WOOS3, WOOS3 = WOOS2, WOOS2 = WOOS1, " & _
        "WOOS1 = 0"
    
    mcmd.Execute
    
    mcon.CommitTrans

    Exit Sub

RollbackTrans:
    mcon.RollbackTrans
    Call Err.Raise(Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext)
End Sub

Private Sub RollSupplierCycleCountWeek()
    
    On Error GoTo RollbackTrans
    
    mcon.BeginTrans
    
    mcmd.CommandText = "Update SYSDAT Set CYCW = If(CYNO = CYCW + 1, 0, CYCW + 1)"
    
    mcmd.Execute

    mcon.CommitTrans

    Exit Sub
    
RollbackTrans:
    mcon.RollbackTrans
    Call Err.Raise(Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext)
End Sub

Private Sub UpdateStockMasterFile()

Dim rsStockMaster       As ADODB.Recordset
Dim intI                As Integer
Dim intLastOccurance    As Integer
Dim lngTotalSold        As Long
Dim intTotalDays        As Integer
Dim intHalfTotalDays    As Integer
Dim intStockDays        As Integer
Dim lngDaysOut          As Long

    On Error GoTo RollbackTrans

    mcon.BeginTrans

    Call UpdateStockMovements
    
    Set rsStockMaster = New ADODB.Recordset
    
    rsStockMaster.ActiveConnection = mcon
    rsStockMaster.CursorLocation = adUseServer
    rsStockMaster.CursorType = adOpenDynamic
    rsStockMaster.LockType = adLockOptimistic
        
    rsStockMaster.Open "Select SKUN, US001, US002, US003, US004, US005, US006, US007, " & _
        "US008, US009, US0010, US0011, US0012, US0013, US0014, DO001, DO002, DO003, " & _
        "DO004, DO005, DO006, DO007, DO008, DO009, DO0010, DO0011, DO0012, DO0013, " & _
        "DO0014, UWEK, CFLG, AWSF, FLAG, PRIC, ONHA From STKMAS " & _
        "Where FLAG = 0 And DATS Is Not Null"

    On Error Resume Next
    rsStockMaster.MoveFirst
    On Error GoTo RollbackTrans

    Do While Not rsStockMaster.EOF
        
        frmWeeklySystemUpdate.strStatus = "Updating Stock Master File - " & rsStockMaster!SKUN
    
        rsStockMaster!UWEK = rsStockMaster!UWEK + 1
        If rsStockMaster!UWEK > 13 Then
            rsStockMaster!UWEK = 13
        End If

        For intI = 14 To 2 Step -1
            rsStockMaster.Fields("US00" & Format$(intI)).Value = _
                rsStockMaster.Fields("US00" & Format$(intI - 1)).Value
            rsStockMaster.Fields("DO00" & Format$(intI)).Value = _
                rsStockMaster.Fields("DO00" & Format$(intI - 1)).Value
        Next

        rsStockMaster.Fields("US001").Value = 0
        rsStockMaster.Fields("DO001").Value = 0

        lngTotalSold = 0
        intTotalDays = 0
        intStockDays = 0

        If (IIf(IsNull(rsStockMaster!UWEK), 0, rsStockMaster!UWEK) + 1) > 9 Then
            intLastOccurance = 9
        Else
            intLastOccurance = IIf(IsNull(rsStockMaster!UWEK), 0, rsStockMaster!UWEK) + 1
        End If

        For intI = 2 To intLastOccurance
            lngTotalSold = lngTotalSold + rsStockMaster.Fields("US00" & Format$(intI)).Value
            intTotalDays = intTotalDays + mrsSysDates!DAYS
            intStockDays = intStockDays + mrsSysDates!DAYS
            lngDaysOut = rsStockMaster.Fields("DO00" & Format$(intI)).Value

            If lngDaysOut <> 0 Then
                If lngDaysOut >= 64 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 64
                End If

                If lngDaysOut >= 32 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 32
                End If

                If lngDaysOut >= 16 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 16
                End If

                If lngDaysOut >= 8 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 8
                End If

                If lngDaysOut >= 4 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 4
                End If

                If lngDaysOut >= 2 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 2
                End If

                If lngDaysOut >= 1 Then
                    intStockDays = intStockDays - 1
                    lngDaysOut = lngDaysOut - 1
                End If
            End If
        Next

        rsStockMaster!CFLG = False

        intHalfTotalDays = intTotalDays / 2

        If intHalfTotalDays > intStockDays Or rsStockMaster!UWEK < 4 Then
            rsStockMaster!CFLG = True
        End If

        If intStockDays <> 0 Then
            rsStockMaster!AWSF = Format$((lngTotalSold * mrsSysDates!DAYS) / intStockDays, "#0.00")
        Else
            rsStockMaster!AWSF = 0
        End If
        
        rsStockMaster!FLAG = True

        rsStockMaster.Update
        
        rsStockMaster.MoveNext
    Loop
    
    rsStockMaster.Close
    
    frmWeeklySystemUpdate.strStatus = "Updating Stock Master File"
    
    If mrsSysDates!CYCW = 0 Then
        mcmd.CommandText = "Update STKMAS Set ICOU = 0"

        mcmd.Execute
    End If
    
    mcon.CommitTrans

    Exit Sub

RollbackTrans:
    mcon.RollbackTrans
    Call Err.Raise(Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext)
End Sub

Private Sub UpdateStockMovements()

Dim strSQLCommand As String

    'Update existing STKMOV records
    strSQLCommand = "Update STKMOV SV Set " & _
        "WMSQ16 = WMSQ15, WMSQ15 = WMSQ14, WMSQ14 = WMSQ13, WMSQ13 = WMSQ12, WMSQ12 = WMSQ11, WMSQ11 = WMSQ10, WMSQ10 = WMSQ9, WMSQ9 = WMSQ8, WMSQ8 = WMSQ7, WMSQ7 = WMSQ6, WMSQ6 = WMSQ5, WMSQ5 = WMSQ4, WMSQ4 = WMSQ3, WMSQ3 = WMSQ2, WMSQ2 = WMSQ1, " & _
        "WMSA16 = WMSA15, WMSA15 = WMSA14, WMSA14 = WMSA13, WMSA13 = WMSA12, WMSA12 = WMSA11, WMSA11 = WMSA10, WMSA10 = WMSA9, WMSA9 = WMSA8, WMSA8 = WMSA7, WMSA7 = WMSA6, WMSA6 = WMSA5, WMSA5 = WMSA4, WMSA4 = WMSA3, WMSA3 = WMSA2, WMSA2 = WMSA1, " & _
        "WMRE16 = WMRE15, WMRE15 = WMRE14, WMRE14 = WMRE13, WMRE13 = WMRE12, WMRE12 = WMRE11, WMRE11 = WMRE10, WMRE10 = WMRE9, WMRE9 = WMRE8, WMRE8 = WMRE7, WMRE7 = WMRE6, WMRE6 = WMRE5, WMRE5 = WMRE4, WMRE4 = WMRE3, WMRE3 = WMRE2, WMRE2 = WMRE1, " & _
        "WMII16 = WMII15, WMII15 = WMII14, WMII14 = WMII13, WMII13 = WMII12, WMII12 = WMII11, WMII11 = WMII10, WMII10 = WMII9, WMII9 = WMII8, WMII8 = WMII7, WMII7 = WMII6, WMII6 = WMII5, WMII5 = WMII4, WMII4 = WMII3, WMII3 = WMII2, WMII2 = WMII1, " & _
        "WMIO16 = WMIO15, WMIO15 = WMIO14, WMIO14 = WMIO13, WMIO13 = WMIO12, WMIO12 = WMIO11, WMIO11 = WMIO10, WMIO10 = WMIO9, WMIO9 = WMIO8, WMIO8 = WMIO7, WMIO7 = WMIO6, WMIO6 = WMIO5, WMIO5 = WMIO4, WMIO4 = WMIO3, WMIO3 = WMIO2, WMIO2 = WMIO1, " & _
        "WMRT16 = WMRT15, WMRT15 = WMRT14, WMRT14 = WMRT13, WMRT13 = WMRT12, WMRT12 = WMRT11, WMRT11 = WMRT10, WMRT10 = WMRT9, WMRT9 = WMRT8, WMRT8 = WMRT7, WMRT7 = WMRT6, WMRT6 = WMRT5, WMRT5 = WMRT4, WMRT4 = WMRT3, WMRT3 = WMRT2, WMRT2 = WMRT1, " & _
        "WMPA16 = WMPA15, WMPA15 = WMPA14, WMPA14 = WMPA13, WMPA13 = WMPA12, WMPA12 = WMPA11, WMPA11 = WMPA10, WMPA10 = WMPA9, WMPA9 = WMPA8, WMPA8 = WMPA7, WMPA7 = WMPA6, WMPA6 = WMPA5, WMPA5 = WMPA4, WMPA4 = WMPA3, WMPA3 = WMPA2, WMPA2 = WMPA1, " & _
        "WMNA16 = WMNA15, WMNA15 = WMNA14, WMNA14 = WMNA13, WMNA13 = WMNA12, WMNA12 = WMNA11, WMNA11 = WMNA10, WMNA10 = WMNA9, WMNA9 = WMNA8, WMNA8 = WMNA7, WMNA7 = WMNA6, WMNA6 = WMNA5, WMNA5 = WMNA4, WMNA4 = WMNA3, WMNA3 = WMNA2, WMNA2 = WMNA1, " & _
        "WMBS16 = WMBS15, WMBS15 = WMBS14, WMBS14 = WMBS13, WMBS13 = WMBS12, WMBS12 = WMBS11, WMBS11 = WMBS10, WMBS10 = WMBS9, WMBS9 = WMBS8, WMBS8 = WMBS7, WMBS7 = WMBS6, WMBS6 = WMBS5, WMBS5 = WMBS4, WMBS4 = WMBS3, WMBS3 = WMBS2, WMBS2 = WMBS1, " & _
        "WMSQ1 = MSTQ1, " & _
        "WMSA1 = MSAQ1, " & _
        "WMRE1 = MREQ1, " & _
        "WMII1 = If((MISQ1 > 0) And (MISI1 = 0) And (MISO1 = 0), MISI1 + MISQ1, MISI1), " & _
        "WMIO1 = If((MISQ1 < 0) And (MISI1 = 0) And (MISO1 = 0), MISO1 - MISQ1, MISO1), " & _
        "WMRT1 = MRTQ1, "
    strSQLCommand = strSQLCommand & _
        "WMPA1 = If((MADQ1 > 0) And (MPAQ1 = 0) And (MNAQ1 = 0), MPAQ1 + MADQ1, MPAQ1), " & _
        "WMNA1 = If((MADQ1 < 0) And (MPAQ1 = 0) And (MNAQ1 = 0), MNAQ1 - MADQ1, MNAQ1), " & _
        "WMBS1 = MBSQ1, " & _
        "MSTQ2 = MSTQ1, " & _
        "MSPR2 = MSPR1, " & _
        "MSAQ2 = MSAQ1, " & _
        "MSAV2 = MSAV1, " & _
        "MREQ2 = MREQ1, " & _
        "MREV2 = MREV1, " & _
        "MADQ2 = MADQ1, " & _
        "MADV2 = MADV1, " & _
        "MPVV2 = MPVV1, " & _
        "MISQ2 = MISQ1, " & _
        "MISV2 = MISV1, " & _
        "MRTQ2 = MRTQ1, " & _
        "MRTV2 = MRTV1, " & _
        "MBSQ2 = MBSQ1, " & _
        "MBSV2 = MBSV1, "
    strSQLCommand = strSQLCommand & _
        "MISI2 = If((MISQ1 > 0) And (MISI1 = 0) And (MISO1 = 0), MISI1 + MISQ1, MISI1), " & _
        "MIVI2 = If((MISQ1 > 0) And (MISI1 = 0) And (MISO1 = 0), MIVI1 + MISV1, MIVI1), " & _
        "MISO2 = If((MISQ1 < 0) And (MISI1 = 0) And (MISO1 = 0), MISO1 - MISQ1, MISO1), " & _
        "MIVO2 = If((MISQ1 < 0) And (MISI1 = 0) And (MISO1 = 0), MIVO1 - MISV1, MIVO1), " & _
        "MPAQ2 = If((MADQ1 > 0) And (MPAQ1 = 0) And (MNAQ1 = 0), MPAQ1 + MADQ1, MPAQ1), " & _
        "MPAV2 = If((MADQ1 > 0) And (MPAQ1 = 0) And (MNAQ1 = 0), MPAV1 + MADV1, MPAV1), " & _
        "MNAQ2 = If((MADQ1 < 0) And (MPAQ1 = 0) And (MNAQ1 = 0), MNAQ1 - MADQ1, MNAQ1), " & _
        "MNAV2 = If((MADQ1 < 0) And (MPAQ1 = 0) And (MNAQ1 = 0), MNAV1 - MADV1, MNAV1), " & _
        "MSTQ1 = (Select ONHA From STKMAS ST2 Where ST2.SKUN = SV.SKUN), " & _
        "MSPR1 = (Select PRIC From STKMAS ST2 Where ST2.SKUN = SV.SKUN), " & _
        "MSAQ1 = 0, " & _
        "MSAV1 = 0, " & _
        "MREQ1 = 0, " & _
        "MREV1 = 0, " & _
        "MADQ1 = 0, " & _
        "MADV1 = 0, " & _
        "MPVV1 = 0, "
    strSQLCommand = strSQLCommand & _
        "MISQ1 = 0, " & _
        "MISV1 = 0, " & _
        "MRTQ1 = 0, " & _
        "MRTV1 = 0, " & _
        "MBSQ1 = 0, " & _
        "MBSV1 = 0, " & _
        "MISI1 = 0, " & _
        "MIVI1 = 0, " & _
        "MISO1 = 0, " & _
        "MIVO1 = 0, " & _
        "MPAQ1 = 0, " & _
        "MPAV1 = 0, " & _
        "MNAQ1 = 0, " & _
        "MNAV1 = 0, " & _
        "MSAQ3 = MSAQ3 + MSAQ2, " & _
        "MSAV3 = MSAV3 + MSAV2, " & _
        "MREQ3 = MREQ3 + MREQ2, " & _
        "MREV3 = MREV3 + MREV2, " & _
        "MADQ3 = MADQ3 + MADQ2, " & _
        "MADV3 = MADV3 + MADV2, " & _
        "MPVV3 = MPVV3 + MPVV2, " & _
        "MISQ3 = MISQ3 + MISQ2, " & _
        "MISV3 = MISV3 + MISV2, "
    strSQLCommand = strSQLCommand & _
        "MRTQ3 = MRTQ3 + MRTQ2, " & _
        "MRTV3 = MRTV3 + MRTV2, " & _
        "MBSQ3 = MBSQ3 + MBSQ2, " & _
        "MBSV3 = MBSV3 + MBSV2, " & _
        "MISI3 = MISI3 + MISI2, " & _
        "MIVI3 = MIVI3 + MIVI2, " & _
        "MISO3 = MISO3 + MISO2, " & _
        "MIVO3 = MIVO3 + MIVO2, " & _
        "MPAQ3 = MPAQ3 + MPAQ2, " & _
        "MPAV3 = MPAV3 + MPAV2, " & _
        "MNAQ3 = MNAQ3 + MNAQ2, " & _
        "MNAV3 = MNAV3 + MNAV2 " & _
    "Where SV.SKUN In " & _
        "(Select SKUN From STKMAS ST1 " & _
        "Where FLAG = 0 And DATS Is Not Null)"
        
    mcmd.CommandText = strSQLCommand
    
    mcmd.Execute

    'Insert new STKMOV records for Updated STKMAS records
    strSQLCommand = "Insert Into STKMOV(SKUN, " & _
        "MSTQ1, MSTQ2, MSTQ3, MSTQ4, " & _
        "MSPR1, MSPR2, MSPR3, MSPR4, " & _
        "MSAQ1, MSAQ2, MSAQ3, MSAQ4, " & _
        "MSAV1, MSAV2, MSAV3, MSAV4, " & _
        "MREQ1, MREQ2, MREQ3, MREQ4, " & _
        "MREV1, MREV2, MREV3, MREV4, " & _
        "MADQ1, MADQ2, MADQ3, MADQ4, " & _
        "MADV1, MADV2, MADV3, MADV4, " & _
        "MPVV1, MPVV2, MPVV3, MPVV4, " & _
        "MISQ1, MISQ2, MISQ3, MISQ4, " & _
        "MISV1, MISV2, MISV3, MISV4, " & _
        "MRTQ1, MRTQ2, MRTQ3, MRTQ4, " & _
        "MRTV1, MRTV2, MRTV3, MRTV4, " & _
        "MBSQ1, MBSQ2, MBSQ3, MBSQ4, " & _
        "MBSV1, MBSV2, MBSV3, MBSV4, " & _
        "MISI1, MISI2, MISI3, MISI4, " & _
        "MIVI1, MIVI2, MIVI3, MIVI4, " & _
        "MISO1, MISO2, MISO3, MISO4, " & _
        "MIVO1, MIVO2, MIVO3, MIVO4, " & _
        "MPAQ1, MPAQ2, MPAQ3, MPAQ4, " & _
        "MPAV1, MPAV2, MPAV3, MPAV4, " & _
        "MNAQ1, MNAQ2, MNAQ3, MNAQ4, " & _
        "MNAV1, MNAV2, MNAV3, MNAV4, "
    strSQLCommand = strSQLCommand & _
        "WMSQ1, WMSQ2, WMSQ3, WMSQ4, WMSQ5, WMSQ6, WMSQ7, WMSQ8, WMSQ9, WMSQ10, WMSQ11, WMSQ12, WMSQ13, WMSQ14, WMSQ15, WMSQ16, " & _
        "WMSA1, WMSA2, WMSA3, WMSA4, WMSA5, WMSA6, WMSA7, WMSA8, WMSA9, WMSA10, WMSA11, WMSA12, WMSA13, WMSA14, WMSA15, WMSA16, " & _
        "WMRE1, WMRE2, WMRE3, WMRE4, WMRE5, WMRE6, WMRE7, WMRE8, WMRE9, WMRE10, WMRE11, WMRE12, WMRE13, WMRE14, WMRE15, WMRE16, " & _
        "WMII1, WMII2, WMII3, WMII4, WMII5, WMII6, WMII7, WMII8, WMII9, WMII10, WMII11, WMII12, WMII13, WMII14, WMII15, WMII16, " & _
        "WMIO1, WMIO2, WMIO3, WMIO4, WMIO5, WMIO6, WMIO7, WMIO8, WMIO9, WMIO10, WMIO11, WMIO12, WMIO13, WMIO14, WMIO15, WMIO16, " & _
        "WMRT1, WMRT2, WMRT3, WMRT4, WMRT5, WMRT6, WMRT7, WMRT8, WMRT9, WMRT10, WMRT11, WMRT12, WMRT13, WMRT14, WMRT15, WMRT16, " & _
        "WMPA1, WMPA2, WMPA3, WMPA4, WMPA5, WMPA6, WMPA7, WMPA8, WMPA9, WMPA10, WMPA11, WMPA12, WMPA13, WMPA14, WMPA15, WMPA16, " & _
        "WMNA1, WMNA2, WMNA3, WMNA4, WMNA5, WMNA6, WMNA7, WMNA8, WMNA9, WMNA10, WMNA11, WMNA12, WMNA13, WMNA14, WMNA15, WMNA16, " & _
        "WMBS1, WMBS2, WMBS3, WMBS4, WMBS5, WMBS6, WMBS7, WMBS8, WMBS9, WMBS10, WMBS11, WMBS12, WMBS13, WMBS14, WMBS15, WMBS16) " & _
    "Select ST.SKUN, ST.ONHA, 0, 0, 0, ST.PRIC, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, "
    strSQLCommand = strSQLCommand & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " & _
        "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 "
    strSQLCommand = strSQLCommand & _
        "From STKMAS ST " & _
        "Left Outer Join STKMOV SV On " & _
            "ST.SKUN = SV.SKUN " & _
        "Where FLAG = 0 And " & _
            "DATS Is Not Null And " & _
            "SV.SKUN Is Null"
            
    mcmd.CommandText = strSQLCommand
    
    mcmd.Execute
    
End Sub

Private Sub UpdateRelatedItemFile()
    
    On Error GoTo RollbackTrans
    
    mcon.BeginTrans
    
    mcmd.CommandText = "Update RELITM Set PWKS = WTDS, PWKM = WTDM, WTDS = 0, WTDM = 0"
    
    mcmd.Execute

    mcon.CommitTrans

    Exit Sub
    
RollbackTrans:
    mcon.RollbackTrans
    Call Err.Raise(Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext)
End Sub

Private Sub UpdateGMROIFiles()

    On Error GoTo RollbackTrans
    
    mcon.BeginTrans
    
    ' First, roll GMROI sku records
    mcmd.CommandText = "Update GMRSKU Set SVAL2 = SVAL1, SUNT2 = SUNT1, SCOS2 = SCOS1, " & _
        "HOLD2 = HOLD1, DOPN2 = DOPN1, DSTK2 = DSTK1, SVAL3 = SVAL3 + SVAL1, " & _
        "SUNT3 = SUNT3 + SUNT1, SCOS3 = SCOS3 + SCOS1, HOLD3 = HOLD3 + HOLD1, " & _
        "DOPN3 = DOPN3 + DOPN1, DSTK3 = DSTK3 + DSTK1, SVAL5 = SVAL5 + SVAL1, " & _
        "SUNT5 = SUNT5 + SUNT1, SCOS5 = SCOS5 + SCOS1, HOLD5 = HOLD5 + HOLD1, " & _
        "DOPN5 = DOPN5 + DOPN1, DSTK5 = DSTK5 + DSTK1, SVAL1 = 0, SUNT1 = 0, " & _
        "SCOS1 = 0, HOLD1 = 0, DOPN1 = 0, DSTK1 = 0"
    
    mcmd.Execute

    ' Next, Delete all GMRGRP records that have associated GMRSKU records, or
    ' don't have associated GMRSKU records, but have zero sales. Don't
    ' delete 999 records.
    mcmd.CommandText = "Delete From GMRGRP GG Where (Exists " & _
        "(Select * From GMRSKU GS Where GS.DEPT = GG.DEPT And GS.GROU = GG.GROU) Or " & _
        "(SVAL1 = 0 And SVAL2 = 0 And SVAL3 = 0 And SVAL4 = 0 And SVAL5 = 0)) And " & _
        "GROU <> '999'"
    
    mcmd.Execute

    ' Insert new GMRGRP Records
    mcmd.CommandText = "Insert Into GMRGRP " & _
        "Select DEPT, GROU, Sum(SVAL2) As SVAL1, Sum(SVAL3) As SVAL2, " & _
            "Sum(SVAL4) As SVAL3, Sum(SVAL5) As SVAL4, Sum(SVAL6) As SVAL5, " & _
            "Sum(SCOS2) As SCOS1, Sum(SCOS3) As SCOS2, Sum(SCOS4) As SCOS3, " & _
            "Sum(SCOS5) As SCOS4, Sum(SCOS6) As SCOS5, '' As SPARE From GMRSKU " & _
            "Group By DEPT, GROU"
    
    mcmd.Execute
    
    ' Add the department sales/cost to GMMGRP as DD999 records
    Call UpdateDeptSalesCost
    
    mcon.CommitTrans

    Exit Sub

RollbackTrans:
    mcon.RollbackTrans
    Call Err.Raise(Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext)
End Sub

Private Sub UpdateDeptSalesCost()

Dim rsDepartments   As ADODB.Recordset
Dim rsDeptSalesCost As ADODB.Recordset
Dim blnAdding       As Boolean
Dim intI            As Integer

    Set rsDepartments = New ADODB.Recordset
    Set rsDeptSalesCost = New ADODB.Recordset
    
    rsDepartments.ActiveConnection = mcon
    rsDepartments.CursorLocation = adUseClient
    rsDepartments.CursorType = adOpenStatic
    rsDepartments.LockType = adLockReadOnly
    
    rsDeptSalesCost.ActiveConnection = mcon
    rsDeptSalesCost.CursorLocation = adUseServer
    rsDeptSalesCost.CursorType = adOpenDynamic
    rsDeptSalesCost.LockType = adLockOptimistic
        
    rsDepartments.Open "Select DEPT, DPTS3, DPTS4, DPTS5, DPTS6, DPTS7, DPTC3, " & _
        "DPTC4, DPTC5, DPTC6, DPTC7 From DPTMAS"

    If rsDepartments.RecordCount <> 0 Then
        rsDepartments.MoveFirst
    
        Do While Not rsDepartments.EOF
            rsDeptSalesCost.Open "Select * from GMRGRP Where DEPT = '" & _
                rsDepartments!DEPT & "' And GROU = '999'"
        
            On Error Resume Next
            rsDeptSalesCost.MoveFirst
            If Err.Number = ADODB.ErrorValueEnum.adErrNoCurrentRecord Then
                On Error GoTo 0
                rsDeptSalesCost.AddNew
                
                For intI = 0 To rsDeptSalesCost.Fields.Count - 1
                    Select Case rsDeptSalesCost.Fields(intI).Type
                        Case adNumeric
                            rsDeptSalesCost.Fields(intI).Value = 0
                        Case adChar
                            rsDeptSalesCost.Fields(intI).Value = vbNullString
                    End Select
                Next
                
                rsDeptSalesCost!DEPT = rsDepartments!DEPT
                rsDeptSalesCost!GROU = "999"
            End If
            On Error GoTo 0
        
            rsDeptSalesCost!SVAL1 = rsDepartments!DPTS3
            rsDeptSalesCost!SVAL2 = rsDepartments!DPTS4
            rsDeptSalesCost!SVAL3 = rsDepartments!DPTS5
            rsDeptSalesCost!SVAL4 = rsDepartments!DPTS6
            rsDeptSalesCost!SVAL5 = rsDepartments!DPTS7
            
            rsDeptSalesCost!SCOS1 = rsDepartments!DPTC3
            rsDeptSalesCost!SCOS2 = rsDepartments!DPTC4
            rsDeptSalesCost!SCOS3 = rsDepartments!DPTC5
            rsDeptSalesCost!SCOS4 = rsDepartments!DPTC6
            rsDeptSalesCost!SCOS5 = rsDepartments!DPTC7
            
            rsDeptSalesCost!SVAL4 = rsDeptSalesCost!SVAL4 + rsDeptSalesCost!SVAL2
            rsDeptSalesCost!SCOS4 = rsDeptSalesCost!SCOS4 + rsDeptSalesCost!SCOS2
            
            rsDeptSalesCost.Update
            
            rsDeptSalesCost.Close
            
            rsDepartments.MoveNext
        Loop
    End If

    rsDepartments.Close

    Set rsDepartments = Nothing
    Set rsDeptSalesCost = Nothing

End Sub

Private Sub OutputStatusFile(strStatus As String)
Dim intStatusFileNo As Integer

    If Len(Dir(mstrStatusFilePath, vbNormal)) <> 0 Then
        Kill (mstrStatusFilePath)
    End If
    
    If Len(strStatus) <> 0 Then
        intStatusFileNo = FreeFile
        Open mstrStatusFilePath For Output As intStatusFileNo
        Print #intStatusFileNo, strStatus
        Close intStatusFileNo
    End If
    
End Sub
