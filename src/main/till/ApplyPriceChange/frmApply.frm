VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmApplyPrices 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Apply Price Changes"
   ClientHeight    =   4635
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8130
   Icon            =   "frmApply.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4635
   ScaleWidth      =   8130
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   6600
      TabIndex        =   7
      Top             =   360
      Width           =   1275
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   495
      Left            =   240
      TabIndex        =   4
      Top             =   3480
      Visible         =   0   'False
      Width           =   7695
      Begin VB.CommandButton cmdCancel 
         Caption         =   "F12 - Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   5820
         TabIndex        =   6
         Top             =   0
         Width           =   1875
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F5-Apply New Price"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   2295
      End
   End
   Begin VB.TextBox txtSKU 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   1860
      MaxLength       =   6
      TabIndex        =   1
      Top             =   300
      Width           =   2415
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   4140
      Width           =   8130
      _ExtentX        =   14340
      _ExtentY        =   873
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmApply.frx":0A96
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7620
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "00:10"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblStatus 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   240
      TabIndex        =   3
      Top             =   1020
      Width           =   7695
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Enter SKU"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   300
      TabIndex        =   0
      Top             =   360
      Width           =   1875
   End
End
Attribute VB_Name = "frmApplyPrices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const MODULE_NAME As String = "ApplyPriceChange"

Dim moItemBO      As cInventory
Dim moPriceChange As cPriceChange
Dim mstrStatus    As String
Dim mlngDaysPrior As Long
Dim mvToday       As Variant

Private Sub cmdApply_Click()
    
    Dim oSysDat      As cSystemDates

    ' Referral 368 - Get todays date for comparision with 'EffectiveDate'
    If IsEmpty(mvToday) Then
        Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
        Call oSysDat.LoadMatches
        mvToday = DateAdd("d", 1, CDate(oSysDat.NextOpenDate))
    End If
    
    ' Referral 368 - If effective date comes after today, then cannot apply the price change
    If DateDiff("d", CDate(mvToday), moPriceChange.EffectiveDate) > 0 Then
        Call MsgBoxEx("SKU price change is for future date - " & Format(moPriceChange.EffectiveDate, "dd/MM/yyyy") & ".", vbInformation, "Cannot apply price change.")
        Exit Sub
    End If

    If (moItemBO.AutoApplyPriceChanges = True) And (moPriceChange.ChangeStatus = "U") Then
        If (MsgBoxEx("WARNING - Will be system applied on " & Format(moPriceChange.AutoApplyDate, "dd/mm/yy") & "." & vbNewLine & "Do you wish to apply now?", vbInformation + vbYesNo, "Auto System Applied", , , , , RGBMsgBox_WarnColour) <> vbYes) Then
            Call cmdCancel_Click
            Exit Sub
        End If
        mstrStatus = "W"
    End If
    
    If (mstrStatus = "U") Then mstrStatus = "A"
    
    On Error GoTo Error_Apply
    Call moPriceChange.ApplyPriceChange(mstrStatus, moPriceChange.ChangeStatus = "S")
    txtSKU.Enabled = True
    txtSKU.Text = ""
    Call txtSKU.SetFocus
    fraButtons.Visible = False
    lblStatus.Caption = ""
    Exit Sub
    
Error_Apply:

    Call Err.Report(MODULE_NAME, "cmdApply_Click", 0, True, "Error Detected", "An error has occurred whilst applying the Price Change" & vbNewLine & "PRICE CHANGE NOT APPLIED" & _
        vbNewLine & vbNewLine & "Contact IT Support")
    
End Sub

Private Sub cmdCancel_Click()

    txtSKU.Enabled = True
    Call txtSKU.SetFocus
    fraButtons.Visible = False
    lblStatus.Caption = ""

End Sub

Private Sub cmdExit_Click()

    End

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case vbKeyF12 ' if F12 then call 'cancel apply
                    If cmdCancel.Visible = True Then cmdCancel_Click
                Case (vbKeyF5):  'if F7 then call search
                    If cmdApply.Visible = True Then Call cmdApply_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

Private Sub Form_Load()

Dim oSysDatesBO As cSystemDates

    Call GetRoot
    
    Call InitialiseStatusBar(sbStatus)
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    fraButtons.BackColor = Me.BackColor
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    
    Set oSysDatesBO = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDatesBO.AddLoadField(FID_SYSTEMDATES_PriceChangeLeadDays)
    Call oSysDatesBO.LoadMatches
    mlngDaysPrior = oSysDatesBO.PriceChangeLeadDays
    If (mlngDaysPrior = 0) Then mlngDaysPrior = 1

End Sub

Private Sub txtSKU_GotFocus()

    txtSKU.SelStart = 0
    txtSKU.SelLength = Len(txtSKU.Text)

End Sub

Private Sub txtSKU_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        Call RetrieveItem
    End If
    
    If ((KeyAscii < 48) Or (KeyAscii > 57)) And (KeyAscii <> 8) Then KeyAscii = 0
    

End Sub

Private Sub RetrieveItem()

Dim strLabelMsg  As String
Dim colPrices    As Collection
Dim oPriceBO     As cPriceChange

    Set moItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call moItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, txtSKU.Text)
    
    If (moItemBO.LoadMatches.Count = 0) Then
        Call MsgBoxEx("SKU-'" & txtSKU.Text & "' could not be located in the Stock Master File", vbInformation, "Invalid SKU entered", , , , , RGBMsgBox_WarnColour)
        Call txtSKU.SetFocus
        txtSKU.SelStart = 0
        txtSKU.SelLength = Len(txtSKU.Text)
        Exit Sub
    End If
    
    Set moPriceChange = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
    Call moPriceChange.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_PartCode, txtSKU.Text)
    
    
    Set colPrices = moPriceChange.LoadMatches
    If (colPrices.Count = 0) Then
        Call MsgBoxEx("Price Change entry for SKU-'" & txtSKU.Text & "' could not be located.", vbInformation, "No Price Change Found", , , , , RGBMsgBox_WarnColour)
        Call txtSKU.SetFocus
        txtSKU.SelStart = 0
        txtSKU.SelLength = Len(txtSKU.Text)
        Exit Sub
    End If
    
    
    Set oPriceBO = colPrices(1)
    'find first unapplied price change entry
    For Each moPriceChange In colPrices
        If ((moPriceChange.ChangeStatus = "U") And (oPriceBO.EffectiveDate >= moPriceChange.EffectiveDate)) Or ((oPriceBO.ChangeStatus <> "U") And (moPriceChange.ChangeStatus = "U")) Then
            Set oPriceBO = moPriceChange
        End If
    Next
    
    Set moPriceChange = oPriceBO
    
    If (Date < (DateAdd("d", mlngDaysPrior * -1, moPriceChange.EffectiveDate))) Then
        Call MsgBoxEx("Not allowed to action this Price Change until " & DisplayDate(DateAdd("d", mlngDaysPrior * -1, moPriceChange.EffectiveDate), False), vbInformation, "Unable to apply Price Change", , , , , RGBMsgBox_WarnColour)
        Call txtSKU.SetFocus
        txtSKU.SelStart = 0
        txtSKU.SelLength = Len(txtSKU.Text)
        Exit Sub
    End If
    
    mstrStatus = moPriceChange.ChangeStatus
    If (moPriceChange.ChangeStatus = "U") And (moItemBO.AutoApplyPriceChanges = False) Then
        strLabelMsg = ""
        If (moPriceChange.SmallLabel = True) Then strLabelMsg = strLabelMsg & "      Small" & vbCrLf
        If (moPriceChange.MediumLabel = True) Then strLabelMsg = strLabelMsg & "      Medium" & vbCrLf
        If (moPriceChange.LargeLabel = True) Then strLabelMsg = strLabelMsg & "      Large" & vbCrLf
        If (strLabelMsg <> "") Then
            mstrStatus = "L"
            lblStatus.Caption = "Note : the following shelf edge label(s) have not been printed yet." & vbCrLf & strLabelMsg & vbCrLf
        End If
    End If
    
    If (moItemBO.AutoApplyPriceChanges = True) Then
        If (moPriceChange.ChangeStatus <> "S") Then
            lblStatus.Caption = lblStatus.Caption & "WARNING - Will be system applied on " & Format(moPriceChange.AutoApplyDate, "dd/mm/yy")
        Else
            mstrStatus = "S"
            lblStatus.Caption = lblStatus.Caption & "NOTE: Price change was system applied on " & Format(moPriceChange.AutoAppliedDate, "dd/mm/yy")
        End If
    Else
        lblStatus.Caption = lblStatus.Caption & "Effective Date of Price change is " & Format(moPriceChange.EffectiveDate, "dd/mm/yy") & vbCrLf & "Current Price " & ChrW(163) & Format(moItemBO.NormalSellPrice, "0.00") & " to New Price " & ChrW(163) & Format(moPriceChange.NewPrice, "0.00")
    End If
        
    fraButtons.Visible = True
    If (moPriceChange.ChangeStatus <> "U") Then
        lblStatus.Caption = "NOTE : THE PRICE CHANGE HAS ALREADY BEEN APPLIED" & vbNewLine & "SELECT CANCEL TO ENTER ANOTHER SKU" & vbCrLf
        cmdApply.Visible = False
        cmdCancel.SetFocus
    Else
        cmdApply.Visible = True
    End If
        
    txtSKU.Enabled = False
            
End Sub
