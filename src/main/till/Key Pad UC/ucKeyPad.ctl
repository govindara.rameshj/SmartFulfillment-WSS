VERSION 5.00
Begin VB.UserControl ucKeyPad 
   BackColor       =   &H00004000&
   CanGetFocus     =   0   'False
   ClientHeight    =   4170
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11160
   ScaleHeight     =   4170
   ScaleWidth      =   11160
   Begin VB.Frame fraNumPad 
      BackColor       =   &H00004000&
      Height          =   4155
      Left            =   0
      TabIndex        =   53
      Top             =   0
      Visible         =   0   'False
      Width           =   4335
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   1
         Left            =   180
         TabIndex        =   65
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   2
         Left            =   1200
         TabIndex        =   64
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   0
         Left            =   180
         TabIndex        =   63
         Top             =   3120
         Width           =   1995
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   3
         Left            =   2220
         TabIndex        =   62
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   4
         Left            =   180
         TabIndex        =   61
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   5
         Left            =   1200
         TabIndex        =   60
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   6
         Left            =   2220
         TabIndex        =   59
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   7
         Left            =   180
         TabIndex        =   58
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   8
         Left            =   1200
         TabIndex        =   57
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Index           =   9
         Left            =   2220
         TabIndex        =   56
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblKeyNPEnter 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Left            =   3240
         TabIndex        =   55
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label lblKeyNPBackspace 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Back"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   3240
         TabIndex        =   54
         Top             =   240
         Width           =   975
      End
      Begin VB.Label lblKeyNPEscape 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "ESC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   915
         Left            =   2220
         TabIndex        =   66
         Top             =   3120
         Width           =   975
      End
   End
   Begin VB.Frame fraKeyPad 
      BackColor       =   &H00004000&
      Height          =   4155
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11130
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "R"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   30
         Left            =   2760
         TabIndex        =   28
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblShift 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Shift"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   120
         TabIndex        =   52
         Top             =   3300
         Width           =   1035
      End
      Begin VB.Label lblKeyKBBackspace 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Back"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   9480
         TabIndex        =   51
         Top             =   180
         Width           =   1575
      End
      Begin VB.Label lblKeyKBEnter 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Enter"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1515
         Left            =   10140
         TabIndex        =   50
         Top             =   960
         Width           =   915
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   10
         Left            =   4020
         TabIndex        =   49
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   11
         Left            =   3240
         TabIndex        =   48
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   12
         Left            =   2460
         TabIndex        =   47
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "E"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   13
         Left            =   1980
         TabIndex        =   46
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "W"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   14
         Left            =   1200
         TabIndex        =   45
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Q"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   15
         Left            =   420
         TabIndex        =   44
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "D"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   16
         Left            =   2340
         TabIndex        =   43
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   " "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   17
         Left            =   1200
         TabIndex        =   41
         Top             =   3300
         Width           =   8655
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "S"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   18
         Left            =   1560
         TabIndex        =   40
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   19
         Left            =   780
         TabIndex        =   39
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   20
         Left            =   6360
         TabIndex        =   38
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   21
         Left            =   5580
         TabIndex        =   37
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   22
         Left            =   4800
         TabIndex        =   36
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   23
         Left            =   1680
         TabIndex        =   35
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   24
         Left            =   900
         TabIndex        =   34
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   25
         Left            =   120
         TabIndex        =   33
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "="
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   26
         Left            =   8700
         TabIndex        =   32
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   27
         Left            =   7920
         TabIndex        =   31
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Y"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   28
         Left            =   4320
         TabIndex        =   30
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "T"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   29
         Left            =   3540
         TabIndex        =   29
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   31
         Left            =   6660
         TabIndex        =   27
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "I"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   32
         Left            =   5880
         TabIndex        =   26
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "U"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   33
         Left            =   5100
         TabIndex        =   25
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "P"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   34
         Left            =   7440
         TabIndex        =   24
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "H"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   35
         Left            =   4680
         TabIndex        =   23
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "G"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   36
         Left            =   3900
         TabIndex        =   22
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "F"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   37
         Left            =   3120
         TabIndex        =   21
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "L"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   38
         Left            =   7020
         TabIndex        =   20
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "K"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   39
         Left            =   6240
         TabIndex        =   19
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "J"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   40
         Left            =   5460
         TabIndex        =   18
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   ";"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   41
         Left            =   7800
         TabIndex        =   17
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "["
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   42
         Left            =   8220
         TabIndex        =   16
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "B"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   43
         Left            =   4380
         TabIndex        =   15
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "V"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   44
         Left            =   3600
         TabIndex        =   14
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "C"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   45
         Left            =   2820
         TabIndex        =   13
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   ","
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   46
         Left            =   6720
         TabIndex        =   12
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "M"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   47
         Left            =   5940
         TabIndex        =   11
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "N"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   48
         Left            =   5160
         TabIndex        =   10
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "X"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   49
         Left            =   2040
         TabIndex        =   9
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Z"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   50
         Left            =   1260
         TabIndex        =   8
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "\"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   51
         Left            =   480
         TabIndex        =   7
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "/"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   52
         Left            =   8280
         TabIndex        =   6
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   53
         Left            =   7500
         TabIndex        =   5
         Top             =   2520
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "]"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   54
         Left            =   9000
         TabIndex        =   4
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   55
         Left            =   7140
         TabIndex        =   3
         Top             =   180
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   56
         Left            =   9360
         TabIndex        =   2
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKey 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "'"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Index           =   57
         Left            =   8580
         TabIndex        =   1
         Top             =   1740
         Width           =   735
      End
      Begin VB.Label lblKeyKBEscape 
         Alignment       =   2  'Center
         BackColor       =   &H00FF8080&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "ESC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   9900
         TabIndex        =   42
         Top             =   3300
         Width           =   1095
      End
   End
End
Attribute VB_Name = "ucKeyPad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Dim blnShowNumPad As Boolean

Dim mblnShiftOn As Boolean

Public Event MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
Public Event MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
Public Event Resize()

Public Sub ShowNumpad()

    fraKeyPad.Visible = False
    fraNumPad.Visible = True
    blnShowNumPad = True
    Call UserControl_Resize

End Sub
Public Property Get KeyboardShown() As Boolean
    
    KeyboardShown = Not blnShowNumPad
    
End Property

Public Sub ShowKeyboard()

    fraKeyPad.Visible = True
    fraNumPad.Visible = False
    blnShowNumPad = False
    Call UserControl_Resize

End Sub

Public Property Get BackColor() As Long

    BackColor = fraKeyPad.BackColor

End Property

Public Property Let BackColor(Value As Long)

    fraKeyPad.BackColor = Value
    fraNumPad.BackColor = Value

End Property

Private Sub fraKeyPad_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)

    RaiseEvent MouseDown(Button, Shift, x, Y)

End Sub

Private Sub fraKeyPad_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)

    RaiseEvent MouseMove(Button, Shift, x, Y)

End Sub

Private Sub fraKeyPad_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)

    RaiseEvent MouseUp(Button, Shift, x, Y)

End Sub

Private Sub fraNumPad_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)

    RaiseEvent MouseDown(Button, Shift, x, Y)

End Sub

Private Sub fraNumPad_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)

    RaiseEvent MouseMove(Button, Shift, x, Y)

End Sub

Private Sub fraNumPad_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)

    RaiseEvent MouseUp(Button, Shift, x, Y)

End Sub

Private Sub Label7_Click()

End Sub

Private Sub Label1_Click()

End Sub

Private Sub lblKey_Click(Index As Integer)

Dim lngCol As Long
Dim bKey   As Byte
    
    lngCol = lblKey(Index).BackColor
    lblKey(Index).BackColor = vbWhite
    lblKey(Index).Refresh
    bKey = Asc("\")
    Select Case (lblKey(Index).Caption)
        Case ("\"): Call Module1.SendKeys("\")
        Case (","): Call Module1.SendKeys(",")
        Case ("."): Call Module1.SendKeys(".")
        Case ("|"): Call Module1.SendKeys("+(\)")
        Case ("<"): Call Module1.SendKeys("+(,)")
        Case (">"): Call Module1.SendKeys("+(.)")
        Case ("?"): Call Module1.SendKeys("+(/)")
        Case (":"): Call Module1.SendKeys("+(;)")
        Case ("@"): Call Module1.SendKeys("+(')")
        Case ("~"): Call Module1.SendKeys("+(#)")
        Case ("{"): Call Module1.SendKeys("+({{})")
        Case ("}"): Call Module1.SendKeys("+({}})")
        Case ("_"): Call Module1.SendKeys("+(-)")
        Case ("+"): Call Module1.SendKeys("+(=)")
        Case ("/"): Call Module1.SendKeys("/")
        Case (";"): Call Module1.SendKeys(";")
        Case ("'"): Call Module1.SendKeys("'")
        Case ("#"): Call Module1.SendKeys("#")
        Case ("["): Call Module1.SendKeys("[")
        Case ("]"): Call Module1.SendKeys("]")
        Case ("-"): Call Module1.SendKeys("-")
        Case ("="): Call Module1.SendKeys("=")
        
        Case ("!"): Call Module1.SendKeys("+(1)")
        Case (""""): Call Module1.SendKeys("+(2)")
        Case (ChrW(163)): Call Module1.SendKeys("+(3)")
        Case ("$"): Call Module1.SendKeys("+(4)")
        Case ("%"): Call Module1.SendKeys("+(5)")
        Case ("^"): Call Module1.SendKeys("+(6)")
        Case ("&&"): Call Module1.SendKeys("+(7)")
        Case ("*"): Call Module1.SendKeys("+(8)")
        Case ("("): Call Module1.SendKeys("+(9)")
        Case (")"): Call Module1.SendKeys("+(0)")
        Case Else:
                    
                    If (mblnShiftOn = True) Then
                        Call Module1.SendKeys("+(" & lblKey(Index).Caption & ")")
                    Else
                        Call Module1.SendKeys(lblKey(Index).Caption)
                    End If
    End Select
    Sleep (50)
    lblKey(Index).BackColor = lngCol

End Sub

Private Sub lblKeyKBEnter_Click()

Dim lngCol As Long
    
    lngCol = lblKeyKBEnter.BackColor
    lblKeyKBEnter.BackColor = vbWhite
    lblKeyKBEnter.Refresh
    Call Module1.SendKeys(Chr(13))
    Sleep (50)
    lblKeyKBEnter.BackColor = lngCol

End Sub

Private Sub lblKeyKBBackspace_Click()

Dim lngCol As Long
    
    lngCol = lblKeyKBBackspace.BackColor
    lblKeyKBBackspace.BackColor = vbWhite
    lblKeyKBBackspace.Refresh
    Call Module1.SendKeys(Chr(8))
    Sleep (50)
    lblKeyKBBackspace.BackColor = lngCol

End Sub

Private Sub lblKeyKBEscape_Click()

Dim lngCol As Long
    
    lngCol = lblKeyKBEscape.BackColor
    lblKeyKBEscape.BackColor = vbWhite
    lblKeyKBEscape.Refresh
    Call Module1.SendKeys(Chr(27))
    Sleep (50)
    lblKeyKBEscape.BackColor = lngCol

End Sub

Private Sub lblKeyNPEnter_Click()

Dim lngCol As Long
    
    lngCol = lblKeyNPEnter.BackColor
    lblKeyNPEnter.BackColor = vbWhite
    lblKeyNPEnter.Refresh
    Call Module1.SendKeys(Chr(13))
    Sleep (50)
    lblKeyNPEnter.BackColor = lngCol

End Sub

Private Sub lblKeyNPBackspace_Click()

Dim lngCol As Long
    
    lngCol = lblKeyNPBackspace.BackColor
    lblKeyNPBackspace.BackColor = vbWhite
    lblKeyNPBackspace.Refresh
    Call Module1.SendKeys(Chr(8))
    Sleep (50)
    lblKeyNPBackspace.BackColor = lngCol

End Sub


Private Sub lblKeyNPEscape_Click()

Dim lngCol As Long
        
    lngCol = lblKeyNPEscape.BackColor
    lblKeyNPEscape.BackColor = vbWhite
    lblKeyNPEscape.Refresh
    Call Module1.SendKeys(Chr(27))
    Sleep (50)
    lblKeyNPEscape.BackColor = lngCol

End Sub

Private Sub lblShift_Click()

    If (mblnShiftOn = False) Then
        lblShift.BackColor = vbRed
        lblShift.ForeColor = vbWhite
        lblShift.Refresh
        lblShift.Caption = "Shift" & vbNewLine & "ON"
        lblKey(51).Caption = "|"
        lblKey(46).Caption = "<"
        lblKey(53).Caption = ">"
        lblKey(52).Caption = "?"
        lblKey(41).Caption = ":"
        lblKey(57).Caption = "@"
        lblKey(56).Caption = "~"
        lblKey(42).Caption = "{"
        lblKey(54).Caption = "}"
        lblKey(27).Caption = "_"
        lblKey(26).Caption = "+"
    
        lblKey(25).Caption = "!"
        lblKey(24).Caption = """"
        lblKey(23).Caption = ChrW(163)
        lblKey(12).Caption = "$"
        lblKey(11).Caption = "%"
        lblKey(10).Caption = "^"
        lblKey(22).Caption = "&&"
        lblKey(21).Caption = "*"
        lblKey(20).Caption = "("
        lblKey(55).Caption = ")"
    Else
        lblShift.BackColor = lblKeyKBEnter.BackColor
        lblShift.ForeColor = vbBlack
        lblShift.Refresh
        lblShift.Caption = "Shift"
        lblKey(51).Caption = "\"
        lblKey(46).Caption = ","
        lblKey(53).Caption = "."
        lblKey(52).Caption = "/"
        lblKey(41).Caption = ";"
        lblKey(57).Caption = "'"
        lblKey(56).Caption = "#"
        lblKey(42).Caption = "["
        lblKey(54).Caption = "]"
        lblKey(27).Caption = "-"
        lblKey(26).Caption = "="
        
        lblKey(25).Caption = "1"
        lblKey(24).Caption = "2"
        lblKey(23).Caption = "3"
        lblKey(12).Caption = "4"
        lblKey(11).Caption = "5"
        lblKey(10).Caption = "6"
        lblKey(22).Caption = "7"
        lblKey(21).Caption = "8"
        lblKey(20).Caption = "9"
        lblKey(55).Caption = "0"
    End If
    mblnShiftOn = Not mblnShiftOn
    
End Sub

Private Sub UserControl_Resize()

    If (blnShowNumPad = False) Then
        UserControl.Width = 11130
        UserControl.Height = 4115
    Else
        UserControl.Width = 4335
        UserControl.Height = 4115
    End If
    RaiseEvent Resize

End Sub
