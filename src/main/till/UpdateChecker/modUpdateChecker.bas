Attribute VB_Name = "modUpdateChecker"
'<CAMH>****************************************************************************************
'*
'*$Archive: /Projects/OasysHW/VB/Update Checker/modUpdateChecker.bas $
'**********************************************************************************************
'* $Author: Davidf $ $Date: 12/08/05 10:08 $ $Revision: 0 $
'* $Versions:1.0.00$
'*
'**********************************************************************************************
'* Summary: Application that runs at Store level to check if the 'GO' file exists, if so call
'*              WixWorkstationUpdate, else run the EXE whose name is passed in to this program.
'*
'**********************************************************************************************
'* Versions:
'*
'* 31/10/05 DaveF   v1.0.0  Initial build. WIX1160.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*
'*      1) -    Name of EXE to run after processing any updates.
'*
'**********************************************************************************************
'</CAMH>***************************************************************************************

Option Explicit

Const MODULE_NAME As String = "modUpdateChecker"

Const PRM_WSUPDATE_PATH As Long = 180
Const PRM_WSNOTIFY_PATH As Long = 181

Private goSession       As Session
Private goRoot          As OasysRoot
Private goDatabase      As IBoDatabase

Private Err             As VbUtils_Wickes.ErrorObject

Private mstrWSID        As String
Private mstrReleaseRoot As String
Private mstrWSFolder    As String

Sub Main()

Const PROCEDURE_NAME As String = "Main"

Dim objFSO         As Scripting.FileSystemObject
Dim blnCheckUpdate As Boolean
Dim strUpdEXE      As String
Dim blnCheckEXE    As Boolean
Dim strEXEName     As String

Dim strErrSource   As String

    On Error GoTo Main_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Started Update Checker on " & Now())
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Main")
    
    ' Setup the system.
    Set goRoot = GetRoot
    mstrWSID = Format$(Val(goSession.CurrentEnterprise.IEnterprise_WorkstationID), "00")
    mstrReleaseRoot = goSession.GetParameter(PRM_WSNOTIFY_PATH)
    If (Right(mstrReleaseRoot, 1) <> "\") Then
        mstrReleaseRoot = mstrReleaseRoot & "\"
    End If
    mstrWSFolder = mstrReleaseRoot & "Workstations\WS" & mstrWSID & "\"
    
    ' Check that the release folder exists.
    Set objFSO = New FileSystemObject
    blnCheckUpdate = objFSO.FolderExists(mstrWSFolder)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Release System Check - " & blnCheckUpdate & " at " & mstrWSFolder)
    
    ' Get the name of the EXE to run.
    strEXEName = Command
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "EXE To Run - " & strEXEName)
    ' Check that the EXE file exists.
    blnCheckEXE = objFSO.FileExists(App.Path & "\" & strEXEName)
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "EXE File Check - " & blnCheckEXE & " at " & App.Path & "\")
    Set objFSO = Nothing
    If (blnCheckEXE = False) Then
        Call MsgBox("The EXE File - " & strEXEName & " - could" & vbCrLf & _
                        " not be found, please check this problem.", vbCritical + vbOKOnly, _
                        "EXE File - " & strEXEName & " - Not Found")
    End If
    
    ' Check for the GO file, if exists run WixWorkstationUpdate.EXE else run SystemEnqReporting.EXE.
    If (blnCheckUpdate = True) And (blnCheckEXE = True) Then
        If (CheckForGOFile = True) Then
            strUpdEXE = goSession.GetParameter(PRM_WSUPDATE_PATH)
            Call MsgBox("New version detected." & vbCrLf & "Exiting to run update system." & vbCrLf & vbCrLf & "Press OK to continue.", vbExclamation, "CTS Work-station update system")
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Update EXE - " & strUpdEXE)
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Shell Command - Shell(" & strUpdEXE & " EXIT " & App.Path & "\" & strEXEName & ", vbNormalFocus)")
            Call Shell(strUpdEXE & " EXIT " & App.Path & "\" & strEXEName, vbNormalFocus)
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Shell Command - Shell(" & App.Path & "\" & strEXEName & ", vbNormalFocus)")
            Call Shell(App.Path & "\" & strEXEName, vbNormalFocus)
        End If
    End If
    
    Set goSession = Nothing

    Exit Sub
    
Main_Error:

    Set objFSO = Nothing
    Set goSession = Nothing
    
    If (Err.Source <> "") Then
        strErrSource = PROCEDURE_NAME & " - " & Err.Source
    Else
        strErrSource = PROCEDURE_NAME
    End If
    Call MsgBox("Module: " & strErrSource & vbCrLf & "Err No: " & Err.Number & vbCrLf & "Err Desc: " & Err.Description, vbCritical + vbOKOnly, App.EXEName)
    Call DebugMsg(MODULE_NAME, strErrSource, endlDebug, "Error No - " & Err.Number & " - Error Desc - " & Err.Description)
    Call Err.Report(MODULE_NAME, strErrSource, 0, False)

End Sub

Public Function GetRoot() As IOasysRoot
    
Const PROCEDURE_NAME As String = "GetRoot"

Dim oStart     As OasysStartCom_Wickes.OasysStart
Dim oOasysRoot As OasysRoot
    
    On Error GoTo GetRoot_Error
    
    Call CreateErrorObject("UpdateChecker {" & App.Title & "}", VBA.Err, Err)
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Go Root")
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Starting Session - " & App.EXEName)
    Set oStart = New OasysStart
    Set oOasysRoot = oStart.GetRoot(Command)
    Set oStart = Nothing
    Set GetRoot = oOasysRoot
    Set oOasysRoot = Nothing
    
    If goSession Is Nothing Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Creating Session - " & App.EXEName)
        Set goSession = GetRoot.CreateSession(Command)
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Open Database - " & App.EXEName)
    Set goDatabase = goSession.Database
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Session Started - " & App.EXEName)

    Exit Function
    
GetRoot_Error:

    Set oStart = Nothing
    Set oOasysRoot = Nothing

    If Not (Err Is Nothing) Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    End If
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Function

Private Function CheckForGOFile() As Boolean

Const PROCEDURE_NAME As String = "CheckForGOFile"

Dim objFSO As Scripting.FileSystemObject

    On Error GoTo CheckForGOFile_Error
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Check For GO File")
    
    Set objFSO = New Scripting.FileSystemObject
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Checking for GO file - " & mstrWSFolder & mstrWSID & ".go")
    If objFSO.FileExists(mstrWSFolder & mstrWSID & ".go") Then
        CheckForGOFile = True
    Else
        CheckForGOFile = False
    End If
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Check for GO file - " & CheckForGOFile)
    
    Set objFSO = Nothing
    
    Exit Function

CheckForGOFile_Error:

    Set objFSO = Nothing
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, PROCEDURE_NAME & " Error - Error No - " & Err.Number & " - Desc - " & Err.Description)
    Call Err.Raise(Err.Number, PROCEDURE_NAME, Err.Description)
    
End Function 'CheckForGOFile

