VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmMenu 
   Caption         =   "Wizard"
   ClientHeight    =   8040
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11955
   ControlBox      =   0   'False
   Icon            =   "frmWizard.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   8040
   ScaleWidth      =   11955
   WindowState     =   2  'Maximized
   Begin VB.Timer tmrTimeOut 
      Left            =   8280
      Top             =   7320
   End
   Begin VB.Frame frMaint 
      Caption         =   "F8-Maintenance Options"
      Height          =   3495
      Left            =   9480
      TabIndex        =   5
      Top             =   3720
      Width           =   2415
      Begin VB.PictureBox picReportsFrame 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3120
         Left            =   120
         ScaleHeight     =   3120
         ScaleWidth      =   2235
         TabIndex        =   12
         Top             =   240
         Width           =   2235
         Begin VB.ListBox lstMaint 
            Height          =   2790
            ItemData        =   "frmWizard.frx":058A
            Left            =   0
            List            =   "frmWizard.frx":0597
            Sorted          =   -1  'True
            TabIndex        =   14
            Top             =   0
            Width           =   2175
         End
         Begin VB.CommandButton cmdMaint 
            Caption         =   "&Maintenance"
            Height          =   255
            Left            =   960
            TabIndex        =   13
            Top             =   2880
            Width           =   1215
         End
      End
   End
   Begin VB.Frame frReports 
      Caption         =   "F9-Reports Selection"
      Height          =   3495
      Left            =   9480
      TabIndex        =   4
      Top             =   120
      Width           =   2415
      Begin VB.PictureBox picMaintFrame 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3120
         Left            =   120
         ScaleHeight     =   3120
         ScaleWidth      =   2235
         TabIndex        =   9
         Top             =   240
         Width           =   2235
         Begin VB.ListBox lstReports 
            Height          =   2790
            ItemData        =   "frmWizard.frx":05B0
            Left            =   0
            List            =   "frmWizard.frx":05EA
            Sorted          =   -1  'True
            TabIndex        =   11
            Top             =   0
            Width           =   2175
         End
         Begin VB.CommandButton cmdReport 
            Caption         =   "Re&port"
            Height          =   255
            Left            =   960
            TabIndex        =   10
            Top             =   2880
            Width           =   1215
         End
      End
   End
   Begin MSComctlLib.Toolbar tbarMenu 
      Height          =   1830
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   3228
      ButtonWidth     =   3572
      ButtonHeight    =   3069
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "?. Access Top Level Menu"
            Key             =   "Top Level"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Logout"
      Height          =   375
      Left            =   10680
      TabIndex        =   0
      Top             =   7320
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   7665
      Width           =   11955
      _ExtentX        =   21087
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmWizard.frx":0678
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9684
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   3096
            MinWidth        =   3087
            Key             =   "Username"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   706
            MinWidth        =   706
            Key             =   "WID"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "15:16"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2760
      Top             =   6600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   128
      ImageHeight     =   96
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   30
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":2844
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":B89C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":148F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":1D94C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":269A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":2F9F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":38A44
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":41A96
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":4AAE8
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":53B3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":5CB8C
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":65BDE
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":6EC30
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":77C82
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":7B0D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":7E526
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":87578
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":905CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":93A1C
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":96E6E
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":9FEC0
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":A8F12
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":B1F64
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":BA9B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":C3408
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":CBE5A
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":D48AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":DD2FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":E5D50
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmWizard.frx":EE7A2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblProgram 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   8
      Top             =   2640
      Visible         =   0   'False
      Width           =   9255
   End
   Begin VB.Label lblRunning 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "RUNNING :"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   240
      TabIndex        =   7
      Top             =   2040
      Visible         =   0   'False
      Width           =   9135
   End
   Begin VB.Label lblWidth 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      Height          =   195
      Left            =   1440
      TabIndex        =   3
      Top             =   5760
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblSpaceWidth 
      AutoSize        =   -1  'True
      Caption         =   " "
      Height          =   195
      Left            =   360
      TabIndex        =   2
      Top             =   5760
      Width           =   45
   End
End
Attribute VB_Name = "frmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module: frmMenu
'* Date  : 30/07/02
'* Author: mauricem
'**********************************************************************************************
'* Summary: Form that displays the menu options on screen and handling selections
'**********************************************************************************************
'* Versions:
'* 30/07/02    mauricem
'*             Header added.
'*
'* 03/11/05 DaveF   v1.2.100 Changed to update the Menu Icon image list so it matches the list
'*                              in the MenuEditor.exe application.
'*
'* 09/11/05 DaveF   v1.2.101 Changed to update frmReport so that it allows the user to exit the
'*                              menu when there is only one top menu item.
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmMenu"

Const EXIT_CODE As Long = -1
Const RUN_EXE As Long = 0

'Timer Interval
Const TIMEOUT As Long = 60000
Const START_MENUID As Long = 1

Dim mintTimeout As Integer 'used to track the time that no events have occurred in the menu

Private moTopMenu  As cMenuItem
Private mlngMenuID As Long

'different modes that menu is in, where to pass in number keys pressed
Private Enum enMenuMode
    enmmButtons = 0
    enmmReports = 1
    enmmMaint = 2
End Enum

Private mlngMenuMode   As enMenuMode
Private mlngEditColour As Long

Const MIN_BTN_WIDTH As Long = 3025
Public Sub HideRunningMsg()

    lblRunning.Visible = False
    lblProgram.Visible = False

End Sub

Private Sub cmdExit_Click()

    Call frmReport.OptionSelected(mlngMenuID, EXIT_CODE, vbNullString, vbNullString, False, False, False)

End Sub

Private Sub cmdMaint_Click()

Dim lngMaintID     As Long
Dim strEXEPath     As String
Dim strParams      As String
Dim blnWaitForEXE  As Boolean
Dim blnAddPath     As Boolean
Dim blnSendSession As Boolean

    lngMaintID = 0
    If lstMaint.ListIndex <> -1 Then lngMaintID = lstMaint.ItemData(lstMaint.ListIndex)
    If lngMaintID = 0 Then
        MsgBox ("No maintenance option selected" & vbCrLf & "Select Maintenance option from list first"), vbInformation, "Error selecting maintenance option"
        Exit Sub
    End If
    'Extract extra parameters from selected menu option
    strEXEPath = moTopMenu.Maintenance(CStr(lngMaintID)).ExecPath
    strParams = moTopMenu.Maintenance(CStr(lngMaintID)).Parameters
    blnWaitForEXE = moTopMenu.Maintenance(CStr(lngMaintID)).WaitEXE
    blnAddPath = moTopMenu.Maintenance(CStr(lngMaintID)).AddPath
    blnSendSession = moTopMenu.Maintenance(CStr(lngMaintID)).SendSession
    'pass menu selection to calling program
    Call frmReport.OptionSelected(mlngMenuID, RUN_EXE, strEXEPath, strParams, blnWaitForEXE, blnAddPath, blnSendSession)

End Sub

Private Sub cmdReport_Click()

Dim lngReportID    As Long
Dim strEXEPath     As String
Dim strParams      As String
Dim blnWaitForEXE  As Boolean
Dim blnAddPath     As Boolean
Dim blnSendSession As Boolean

    lngReportID = 0
    If lstReports.ListIndex <> -1 Then lngReportID = lstReports.ItemData(lstReports.ListIndex)
    If lngReportID = 0 Then
        MsgBox ("No report selected for Printing" & vbCrLf & "Select Report from list first"), vbInformation, "Error Producing Report"
        Exit Sub
    End If
        
    'Extract extra parameters from selected menu option
    strEXEPath = moTopMenu.Reports(CStr(lngReportID)).ExecPath
    strParams = moTopMenu.Reports(CStr(lngReportID)).Parameters
    blnWaitForEXE = moTopMenu.Reports(CStr(lngReportID)).WaitEXE
    blnAddPath = moTopMenu.Reports(CStr(lngReportID)).AddPath
    blnSendSession = moTopMenu.Reports(CStr(lngReportID)).SendSession
    Call frmReport.OptionSelected(mlngMenuID, RUN_EXE, strEXEPath, strParams, blnWaitForEXE, blnAddPath, blnSendSession)

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
        
    mintTimeout = 0
        
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF8): 'if F8 then move to maint list
                KeyCode = 0
                If lstMaint.Enabled = True Then Call lstMaint.SetFocus
            Case (vbKeyF9): 'if F9 then move to reports list
                KeyCode = 0
                If lstReports.Enabled = True Then Call lstReports.SetFocus
            Case (vbKeyF10): 'if F10 then exit
                KeyCode = 0
                sbStatus.Panels(PANEL_INFO).Text = vbNullString
                Select Case (mlngMenuMode)
                    Case (enmmReports), (enmmMaint): 'mimic the Escape key
                            Call Form_KeyPress(vbKeyEscape)
                    Case (enmmButtons):
                            Call cmdExit_Click
                End Select
        End Select 'Key pressed with no Shift/Alt combination
    End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

Dim lngOptionNo   As Long
Dim strKeyCode    As String

    'if escape pressed, then cancel any sub list selections
    If (KeyAscii = vbKeyEscape) Then
        mlngMenuMode = enmmButtons
        lstMaint.BackColor = RGB_WHITE
        lstReports.BackColor = RGB_WHITE
        sbStatus.Panels(PANEL_INFO).Text = "Select Menu Option"
        Call cmdExit.SetFocus
        KeyAscii = 0 'clear key pressed to avoid it being processed else where
        Exit Sub
    End If
    
    If KeyAscii >= 97 Then KeyAscii = KeyAscii - 32 'convert lower to upper case
    strKeyCode = Chr$(KeyAscii)
    
    Select Case (mlngMenuMode)
        Case (enmmButtons):
            For lngOptionNo = 1 To tbarMenu.Buttons.Count Step 1
                If (Left$(Trim$(tbarMenu.Buttons(lngOptionNo).Caption), 1) = strKeyCode) And (tbarMenu.Buttons(lngOptionNo).Visible = True) Then
                    Call tbarMenu_ButtonClick(tbarMenu.Buttons(lngOptionNo))
                    Exit For
                End If
            Next lngOptionNo
        Case (enmmReports):
            For lngOptionNo = 0 To lstReports.ListCount - 1 Step 1
                If (Left$(Trim$(lstReports.List(lngOptionNo)), 1) = strKeyCode) Then
                    lstReports.ListIndex = lngOptionNo
                    Call cmdReport_Click
                    Exit For
                End If
            Next lngOptionNo
        Case (enmmMaint):
            For lngOptionNo = 0 To lstMaint.ListCount - 1 Step 1
                If (Left$(Trim$(lstMaint.List(lngOptionNo)), 1) = strKeyCode) Then
                    lstMaint.ListIndex = lngOptionNo
                    Call cmdMaint_Click
                    Exit For
                End If
            Next lngOptionNo
     End Select

End Sub 'Form_KeyPress

Private Sub Form_Load()

    tmrTimeOut.Interval = TIMEOUT
    mintTimeout = 0
    Call CentreForm(Me)
    
End Sub

Public Sub ShowMenu(ByVal MenuID As Long, ByRef oTopMenu As cMenuItem, oWorkstation As cWorkStation, oUser As cUser, blnExiting As Boolean)

Dim oBo         As cMenuItem
Dim colChildren As Collection
Dim lngIconID   As Long
Dim lngBtnPos   As Long
Dim lngLastKey  As Long
Dim strCaption  As String
Dim lngMaxIcon  As Long

    Me.Show
    lblRunning.Visible = True
    lblRunning.Caption = "LOADING MENU"
    DoEvents
    
    lngMaxIcon = ImageList1.ListImages.Count

    Call DebugMsg(MODULE_NAME, "show_menu", endlDebug, MenuID & " currently " & mlngMenuID)
    tbarMenu.Visible = True
    tbarMenu.Enabled = True
    lblProgram.Caption = vbNullString
    If mlngMenuID <> MenuID Then 'new menu being displayed so clear current menu and display
        Call lstReports.Clear
        Call lstMaint.Clear
        Call DebugMsg(MODULE_NAME, "show_menu", endlDebug, "Display New Menu ID = " & MenuID)
        lstReports.BackColor = RGB_WHITE
        lstMaint.BackColor = RGB_WHITE
        mlngMenuMode = enmmButtons
        sbStatus.Panels(PANEL_INFO).Text = "Select Menu Option"
        mlngMenuID = MenuID 'store menu ID away
        mlngEditColour = goSession.GetParameter(PRM_EDITCOLOUR)
        'set global instance to calling menu object - pass notifications, when option selected
'        Set moMenu = oMenuObj
        Set moTopMenu = oTopMenu
            
        Call InitialiseMenuStatusBar
        
        Me.Caption = oTopMenu.MenuText
        
        'Reset menu so that no buttons exist
        For lngIconID = 1 To tbarMenu.Buttons.Count Step 1
            Call tbarMenu.Buttons.Remove(1)
        Next lngIconID
        
        ' Process each Menu Item in the SubMenu collection
        Set colChildren = oTopMenu.SubMenus
        For Each oBo In colChildren
            Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, oBo.MenuID & "-" & oBo.GroupID & "-" & oUser.GroupLevels(oBo.GroupID))
            Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, oUser.GroupLevels(oBo.GroupID))
            If oUser.GroupLevels(oBo.GroupID) = True And oWorkstation.GroupLevels(oBo.GroupID) = True Then
                Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "Label1a" & oBo.ShortcutKey & "*")
                strCaption = oBo.ShortcutKey & ". " & oBo.MenuText
                Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "Label1a-" & strCaption & "*")
                lblWidth.Caption = strCaption
                lngIconID = oBo.IconID
                'Ensure Icon ID is within Image List bounds else force to last Icon
                If lngIconID > lngMaxIcon Then lngIconID = lngMaxIcon
                If Asc(oBo.ShortcutKey) < lngLastKey Then
                    Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "INSERT")
                    Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, lblWidth.Caption)
                    For lngBtnPos = 0 To tbarMenu.Buttons.Count - 1 Step 1
                        If Asc(Left$(Trim$(tbarMenu.Buttons(lngBtnPos + 1).Caption), 1)) > Asc(oBo.ShortcutKey) Then
                            Call tbarMenu.Buttons.Add(lngBtnPos + 1, "ID" & (oBo.MenuID) & "|Y", lblWidth.Caption, , lngIconID)
                            Exit For
                        End If
                    Next lngBtnPos
                Else
                    Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "APPEND")
                    Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, lblWidth.Caption)
                    Call tbarMenu.Buttons.Add(tbarMenu.Buttons.Count + 1, "ID" & (oBo.MenuID) & "|Y", lblWidth.Caption, , lngIconID)
                    lngLastKey = Asc(oBo.ShortcutKey)
                End If
            End If 'not a group level option
        Next
        
        ' Process each Menu Option in the SubMenu collection
        Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "Option Menu ")
        Set colChildren = oTopMenu.MenuOptions
        For Each oBo In colChildren
            lblWidth.Caption = oBo.ShortcutKey & ". " & oBo.MenuText
            lngIconID = oBo.IconID
            'Ensure Icon ID is within Image List bounds else force to last Icon
            If lngIconID > lngMaxIcon Then lngIconID = lngMaxIcon
            If Asc(oBo.ShortcutKey) < lngLastKey Then
                For lngBtnPos = 0 To tbarMenu.Buttons.Count - 1 Step 1
                    If Asc(Left$(Trim$(tbarMenu.Buttons(lngBtnPos + 1).Caption), 1)) > Asc(oBo.ShortcutKey) Then
                        Call tbarMenu.Buttons.Add(lngBtnPos + 1, "ID" & (oBo.MenuID) & "|N", lblWidth.Caption, , lngIconID)
                        Exit For
                    End If
                Next lngBtnPos
            Else
                Call tbarMenu.Buttons.Add(tbarMenu.Buttons.Count + 1, "ID" & (oBo.MenuID) & "|N", lblWidth.Caption, , lngIconID)
                lngLastKey = Asc(oBo.ShortcutKey)
            End If
        Next 'Menu option to display
        
        ' Process each Report Item in the Reports collection
        Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "Reports ")
        Set colChildren = oTopMenu.Reports
        
        For Each oBo In colChildren
            lstReports.AddItem (oBo.ShortcutKey & ". " & oBo.MenuText)
            lstReports.ItemData(lstReports.NewIndex) = oBo.MenuID
        Next
        If lstReports.ListCount = 0 Then
            frReports.Enabled = False
        Else
            frReports.Enabled = True
        End If
        
        ' Process each Report Item in the Maintenance collection
        Call DebugMsg(MODULE_NAME, "ShowMenu", endlDebug, "Maint")
        Set colChildren = oTopMenu.Maintenance

        For Each oBo In colChildren
            lstMaint.AddItem (oBo.ShortcutKey & ". " & oBo.MenuText)
            lstMaint.ItemData(lstMaint.NewIndex) = oBo.MenuID
        Next
        If lstMaint.ListCount = 0 Then
            frMaint.Enabled = False
        Else
            frMaint.Enabled = True
        End If
        
        
        If MenuID = 1 Then
            cmdExit.Caption = "F10-Logout"
        Else
            cmdExit.Caption = "F10-Exit Group"
        End If
        
        Call Assign_Numbers
        
        'If the sub menu only has one button then launch the Exe
        If (tbarMenu.Buttons.Count = 1) And (blnExiting = False) Then
            Call tbarMenu_ButtonClick(tbarMenu.Buttons(1))
        End If
    
    Else
        'If the menu only has one button it will return to the main menu upon exiting the program
        If tbarMenu.Buttons.Count = 1 And lstMaint.ListCount = 0 And lstReports.ListCount = 0 Then
            Call cmdExit_Click
        End If
    
    End If 'display new menu options
     
    If tbarMenu.Buttons.Count = 0 Then
        Call MsgBoxEx("There are no menu options setup for this Workstation \ User, unable to login. " & _
        "Please contact your system administrator", vbCritical, "Oasys Tile - Main Menu", , , , , RGB_RED)
        Call cmdExit_Click
    End If
    lblRunning.Visible = False
'    Me.Show
    Me.ZOrder (0)
    
    
        
End Sub 'ShowMenu

Private Sub Assign_Numbers()

Dim lngButtonNo As Long
  
    Call DebugMsg(MODULE_NAME, "AssignNumbers", endlTraceIn)
    For lngButtonNo = 1 To tbarMenu.Buttons.Count Step 1
        If InStr(tbarMenu.Buttons(lngButtonNo).Caption, "?") > 0 Then
            If lngButtonNo < 10 Then
                tbarMenu.Buttons(lngButtonNo).Caption = Left$(tbarMenu.Buttons(lngButtonNo).Caption, InStr(tbarMenu.Buttons(lngButtonNo).Caption, "?") - 1) & lngButtonNo & Mid$(tbarMenu.Buttons(lngButtonNo).Caption, InStr(tbarMenu.Buttons(lngButtonNo).Caption, "?") + 1)
            Else
                tbarMenu.Buttons(lngButtonNo).Caption = Left$(tbarMenu.Buttons(lngButtonNo).Caption, InStr(tbarMenu.Buttons(lngButtonNo).Caption, "?") - 1) & Chr$(55 + lngButtonNo) & Mid$(tbarMenu.Buttons(lngButtonNo).Caption, InStr(tbarMenu.Buttons(lngButtonNo).Caption, "?") + 1)
            End If
        End If
  Next lngButtonNo

End Sub 'Assign_Numbers


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    mlngMenuID = 0
    If UnloadMode = 0 Then 'clicked on Close form, so cancel and call F10 key instead
        Cancel = True
        Call cmdExit_Click 'pass exit to control form- to process
    End If

End Sub

Private Sub Form_Resize()

    If Me.WindowState = vbMinimized Then 'then control form minimise correctly
        Call frmReport.OptionSelected(mlngMenuID, -2, vbNullString, vbNullString, False, False, False)
    End If

End Sub

Private Sub lblWidth_Change()

Dim strPadStr As String
Dim lngSpaces As Long

On Error GoTo Bad_Format

    lngSpaces = MIN_BTN_WIDTH - (lblWidth.Width + (lblSpaceWidth.Width * 3))
    lngSpaces = (lngSpaces / lblSpaceWidth.Width) \ 2
    strPadStr = Space$(lngSpaces)
    
    lblWidth.Caption = strPadStr & lblWidth.Caption & strPadStr
    Exit Sub
    
Bad_Format:

    Call DebugMsg(MODULE_NAME, "lblWidth_Change", endlTraceOut, "lS=" & lngSpaces & "lW=" & lblWidth.Width & "LWC=" & lblWidth.Caption)
'    Call Err.Clear

End Sub

Private Sub lstMaint_GotFocus()

    sbStatus.Panels(PANEL_INFO).Text = "Select Maintenance Option"
    mlngMenuMode = enmmMaint
    lstMaint.BackColor = mlngEditColour
    lstReports.BackColor = RGB_WHITE

End Sub

Private Sub lstMaint_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) And (lstMaint.ListIndex <> -1) Then
        KeyAscii = 0
        Call cmdMaint_Click
    End If

End Sub

Private Sub lstReports_GotFocus()
    
    mlngMenuMode = enmmReports
    sbStatus.Panels(PANEL_INFO).Text = "Select Reports Option"
    lstReports.BackColor = mlngEditColour
    lstMaint.BackColor = RGB_WHITE

End Sub

Private Sub lstReports_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) And (lstReports.ListIndex <> -1) Then
        KeyAscii = 0
        Call cmdReport_Click
    End If

End Sub

Private Sub tbarMenu_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim lngID          As Long
Dim strKey         As String
Dim strSubMenu     As String
Dim strExec        As String
Dim strParams      As String
Dim blnSubMenu     As Boolean
Dim blnWaitForEXE  As Boolean
Dim blnAddPath     As Boolean
Dim blnSendSession As Boolean

    tbarMenu.Enabled = False
    tbarMenu.Visible = False
    lblProgram.Caption = Button.Caption
    lblProgram.Visible = True
    lblRunning.Visible = True
    Call tbarMenu.Refresh
    Call lblProgram.Refresh
    Call lblRunning.Refresh
    mlngMenuMode = enmmButtons
    lstReports.BackColor = RGB_WHITE
    lstMaint.BackColor = RGB_WHITE
    
    strSubMenu = Right$(Button.Key, 1)
    'Chop off ID prefix
    strKey = Mid$(Button.Key, 3)
    'Chop off SubMenu suffix
    strKey = Left$(strKey, Len(strKey) - 2)
    lngID = Val(strKey)
    'check if last character of Button.key states that menu is a Sub Menu or not
    If strSubMenu = "Y" Then
        blnSubMenu = True
    Else
        blnSubMenu = False
        If lngID > 0 Then
        
            strExec = moTopMenu.MenuOptions(strKey).ExecPath
            strParams = moTopMenu.MenuOptions(strKey).Parameters
            blnWaitForEXE = moTopMenu.MenuOptions(strKey).WaitEXE
            blnAddPath = moTopMenu.MenuOptions(strKey).AddPath
            blnSendSession = moTopMenu.MenuOptions(strKey).SendSession
            
            frmReport.mlngChildMenuID = lngID
            
            lngID = RUN_EXE
        End If
    End If
    Call frmReport.OptionSelected(mlngMenuID, lngID, strExec, strParams, blnWaitForEXE, blnAddPath, blnSendSession)
    
   
    tbarMenu.Enabled = True
    tbarMenu.Visible = True
    Call tbarMenu.Refresh
    
End Sub

Public Sub InitialiseMenuStatusBar()
    
    sbStatus.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbStatus.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbStatus.Panels(PANEL_INFO + 1).Text = goSession.UserID & "-" & goSession.UserFullName
    sbStatus.Panels(PANEL_WSID + 1).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbStatus.Panels(PANEL_WSID + 1).ToolTipText = "Current Work-Station Number"
    sbStatus.Panels(PANEL_DATE + 1).Text = Format$(Date, "DD-MMM-YY")

End Sub

Private Sub tmrTimeOut_Timer()

    mintTimeout = mintTimeout + 1
    
    Call DebugMsg("FrmWizard", "Timer", endlDebug, "The timer counter = " & mintTimeout & "Minute(s)")
  
    If mintTimeout >= 5 Then
        Call DebugMsg("FrmWizard", "Timer", endlDebug, "The timer counter = " & mintTimeout & "Minute(s). Logging Out.")
        Call frmReport.OptionSelected(1, EXIT_CODE, vbNullString, vbNullString, False, False, False)
    End If

End Sub 'tmrTimeOut_Timer
