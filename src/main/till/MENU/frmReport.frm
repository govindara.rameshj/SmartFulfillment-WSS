VERSION 5.00
Begin VB.Form frmReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Oasys V2 Menu"
   ClientHeight    =   585
   ClientLeft      =   780
   ClientTop       =   1665
   ClientWidth     =   1725
   Icon            =   "frmReport.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   585
   ScaleWidth      =   1725
   ShowInTaskbar   =   0   'False
   WindowState     =   1  'Minimized
   Begin VB.Label lblMenuID 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "frmReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmReport
'* Date   : 24/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Menu/frmReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 20/05/04 11:33 $ $Revision: 14 $
'* Versions:
'* 24/10/02    mauricem
'*             Header added.
'*
'* 22/02/06 DaveF   v1.2.102 Changed to run HistoryImport.exe if running Online.
'*
'* 30/11/06 DaveF   v1.2.103 Changed to run ItemFilter.exe in the background on start-up.
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmReport"

Const START_MENUID As Long = 1

Const PRM_WSUPDATE_PATH As Long = 180
Const PRM_WSNOTIFY_PATH As Long = 181

Private Const HISTORY_IMPORT_NAME As String = "HistoryImport.exe"

Private Const ITEM_FILTER_EXE_NAME As String = "ItemFilter.exe"

Dim colMenus     As Collection
Dim moWStation   As cWorkStation
Dim moUser       As cUser

Dim moFSO   As FileSystemObject

Dim mblnCheckUpd    As Boolean 'upon load up - check if CTS Release System installed -False if not
Dim mblnUpdates     As Boolean 'if set then must prompt user to upgarde on each menu option call and exit
Dim mstrReleaseRoot As String 'where Release software holds updates
Dim mstrWSID        As String 'which Work-Station folder to check for updates

Public mlngChildMenuID  As Long

Public Sub CallMenuOption(ByVal strExecPath As String, _
                          ByVal strParams As String, _
                          ByVal blnWaitForEXE As Boolean, _
                          ByVal blnAddPath As Boolean, _
                          ByVal blnSendSession As Boolean)

On Error GoTo Bad_MenuOption

Dim lngProcessID As Long
        
    If InStr(strParams, "${D}") > 0 Then
        strParams = Left$(strParams, InStr(strParams, "${D}") - 1) & Format$(Date, "YYYYMMDD") & Mid$(strParams, InStr(strParams, "${D}") + 4)
    End If
    If blnAddPath = True Then strExecPath = App.Path & "\" & strExecPath
    If blnSendSession = True Then
        strExecPath = strExecPath & " " & goSession.CreateCommandLine(strParams)
    Else
        strExecPath = strExecPath & " " & strParams
    End If
    Call DebugMsg(MODULE_NAME, "callMenuOption", endlDebug, strExecPath)
    If blnWaitForEXE = True Then
        Call ShellWait(strExecPath, SW_MAX)
    Else
        lngProcessID = Shell(strExecPath, vbNormalFocus)
    End If
    Exit Sub
    
Bad_MenuOption:

    Call MsgBox("Invalid set-up for menu option" & vbCrLf & "  " & strExecPath & vbCrLf & Err.Number & "-" & Err.Description, vbExclamation, "Select Option")
    Call Err.Clear
    Resume Next
    
End Sub

Private Sub Form_Load()

    mblnCheckUpd = False
    mblnUpdates = False
    
    If App.PrevInstance = True Then
        Call MsgBox("Previous menu running" & vbCrLf & "Only one instance of this menu can be run at once", vbExclamation, "CTS Retail System")
        Call BringAppToFront
        End
    End If
    Screen.MousePointer = vbHourglass
    
    Set goRoot = GetRoot
    
    mstrWSID = Format$(Val(goSession.CurrentEnterprise.IEnterprise_WorkstationID), "00")
    
    'check if release software is installed
    mstrReleaseRoot = goSession.GetParameter(PRM_WSNOTIFY_PATH)
    Set moFSO = New FileSystemObject
    If LenB(mstrReleaseRoot) <> 0 Then
        If Right$(mstrReleaseRoot, 1) <> "\" Then mstrReleaseRoot = mstrReleaseRoot & "\"
        mblnCheckUpd = moFSO.FolderExists(mstrReleaseRoot & "Workstations\WS" & mstrWSID)
    End If
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Release System Check-" & mblnCheckUpd & " at " & mstrReleaseRoot & "Workstations\WS" & mstrWSID)
    
'    If LenB(goSession.CurrentEnterprise.IEnterprise_WorkstationID) = 0 Then
'        Call InputBox$("No work-station ID set up" & vbCrLf & "Enter valid workstation ID", "Invalid work-station ID detected")
'        End If
'    End If
    
    ' Check if the system is running online, if so run HistoryImport.exe.
    If (goSession.ISession_IsOnline = True) Then
        Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "System Online so Processing - " & App.Path & "\" & HISTORY_IMPORT_NAME)
        Call Shell(App.Path & "\" & HISTORY_IMPORT_NAME, vbNormalFocus)
    End If
    
    Set moWStation = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call moWStation.AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, Format$(goSession.CurrentEnterprise.IEnterprise_WorkstationID, "00"))
    If moWStation.Load = False Then
        Call MsgBoxEx("Invalid work-station ID detected.", vbCritical, "Oasys Tile - Main Menu", , , , , RGB_RED)
        End
        Exit Sub
    End If
    
    Screen.MousePointer = vbNormal
    If mblnCheckUpd = True Then Call CheckForNewUpdates
    Call UserLogon
    Screen.MousePointer = vbNormal

    Me.Hide

End Sub
Private Sub UserLogon()

Dim oLogon    As clsLogon
Dim oMenuItem As cMenuItem
Dim oActivity As cActivityLog
    
    Call moWStation.RecordWSAsActive(moWStation.Id)
    Set oLogon = New prjLogon_Wickes.clsLogon ' goSession.Root.CreateUtilityObject("clsLogon")
    Set oLogon.SysRoot = goRoot
    If oLogon.ValidateUser(goSession) = False Then End
    Set oLogon = Nothing

    Screen.MousePointer = vbHourglass
    Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call moUser.IBo_AddLoadFilter(CMP_EQUAL, FID_USER_EmployeeID, goSession.UserID)
    If moUser.IBo_Load = False Then
        Call Err.Raise(OASYS_ERR_INVALID_USERID, MODULE_NAME, "Invalid User ID detected")
        Exit Sub
    End If
    
    Set oActivity = goDatabase.CreateBusinessObject(CLASSID_ACTIVITYLOG)
    Call oActivity.Login
    
    Set colMenus = New Collection
    Load frmMenu
    
    Set oMenuItem = LoadMenu(START_MENUID) 'Get parent menu
    'Show top level menu
    Call colMenus.Add(oMenuItem, Str$(0))
    Call frmMenu.ShowMenu(START_MENUID, oMenuItem, moWStation, moUser, False)
    Screen.MousePointer = vbNormal
    
End Sub

Private Function LoadMenu(lngMenuID As Long) As cMenuItem

    Set LoadMenu = goDatabase.CreateBusinessObject(CLASSID_MENU)
    
    Call LoadMenu.Retrieve(lngMenuID, True)

End Function

'<CACH>****************************************************************************************
'* Sub:  OptionSelected()
'**********************************************************************************************
'* Description:
'**********************************************************************************************
'* Parameters:
'*In    :lSubMenuID Long.
'*In    :lMenuID    Long.
'*In    :sExecPath  String.
'*In    :blnWaitForEXE  Boolean.
'*In    :blnAddPath Boolean.
'*In    :blnSendSession Boolean.
'**********************************************************************************************
'* History:
'* 24/10/02    mauricem
'*             Header added.
'</CACH>***************************************************************************************
Public Sub OptionSelected(ByVal lSubMenuID As Long, _
                          ByVal lMenuID As Long, _
                          ByVal sExecPath As String, _
                          ByVal strParams As String, _
                          ByVal blnWaitForEXE As Boolean, _
                          ByVal blnAddPath As Boolean, _
                          ByVal blnSendSession As Boolean)

Dim oMenu       As cMenuItem
Dim lIndex      As Long
Dim oActivity   As cActivityLog
Dim oWStationBO As cWorkStation

Const PROCEDURE_NAME As String = MODULE_NAME & ".OptionSelected"

    Screen.MousePointer = vbHourglass
    
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Option Selected-" & lSubMenuID & "\" & lMenuID & "\" & sExecPath & "\Prm" & strParams & "\W" & blnWaitForEXE & "\Ses" & blnSendSession & "\Path" & blnAddPath)
    If lMenuID > 0 Then 'display sub menu
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Select Sub Menu-" & lSubMenuID & "\" & lMenuID)
        If mblnCheckUpd = True Then Call CheckForNewUpdates
        On Error Resume Next
        Set oMenu = colMenus(Str$(lMenuID))
        'point to menu if already loaded
        If oMenu Is Nothing Then
            On Error GoTo 0
            'as not already loaded, then load and show
            Set oMenu = LoadMenu(lMenuID)
            Call frmMenu.ShowMenu(lMenuID, oMenu, moWStation, moUser, False)
            On Error Resume Next 'Added 29/8/07 to catch if menu group already added
            Call colMenus.Add(oMenu, Str$(lMenuID))
            Call Err.Clear
            On Error GoTo 0
        Else
            Call frmMenu.ShowMenu(lMenuID, oMenu, moWStation, moUser, False)
        End If
        Screen.MousePointer = vbNormal
        Call frmMenu.HideRunningMsg
        Exit Sub
    End If 'Sub menu selected

    'If MenuID 1 selected then re-show main Menu form
    If lMenuID = 0 And LenB(sExecPath) = 0 Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Switch to Top Level")
        If mblnCheckUpd = True Then Call CheckForNewUpdates
        Set oMenu = colMenus(Str$(lMenuID))
        Call frmMenu.ShowMenu(1, oMenu, moWStation, moUser, False)
    End If

    'If Execution path passed in then execute
    If LenB(sExecPath) <> 0 Then
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Execute-'" & sExecPath & "'")
        Screen.MousePointer = vbNormal
        
        Set oActivity = goDatabase.CreateBusinessObject(CLASSID_ACTIVITYLOG)
        
        Call oActivity.StartMenuItem(lSubMenuID, mlngChildMenuID)
        
        Call CallMenuOption(sExecPath, strParams, blnWaitForEXE, blnAddPath, blnSendSession)
        
        Call oActivity.EndMenuItem
        
        Call frmMenu.HideRunningMsg
'        If blnWaitForEXE = True Then
'            Set oMenu = LoadMenu(lSubMenuID)
'            Call frmMenu.ShowMenu(lSubMenuID, oMenu, moWStation, moUser, False)
'            If mblnCheckUpd = True Then Call CheckForNewUpdates
'        End If
'        Exit Sub
        If blnWaitForEXE = True Then
            Set oMenu = Nothing
            On Error Resume Next
            Set oMenu = colMenus(Str(lSubMenuID))
            'point to menu if already loaded
            On Error GoTo 0
            Call Err.Clear
            If oMenu Is Nothing Then
                'as not already loaded, then load and show
                Set oMenu = LoadMenu(lSubMenuID)
                Call colMenus.Add(oMenu, Str(lSubMenuID))
            End If
            Call frmMenu.ShowMenu(lSubMenuID, oMenu, moWStation, moUser, False) 'mcolMenuAccess)
            If mblnCheckUpd = True Then Call CheckForNewUpdates
        End If
        Exit Sub
    End If
    
    If lMenuID = -1 Then 'Exit has been called
        If lSubMenuID = 1 Then
            'if exit called on Top Menu then unload all menus and exit
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Exit Menu-" & lSubMenuID & "\" & lMenuID)
            Unload frmMenu
            For lIndex = colMenus.Count To 1 Step -1
                Call colMenus.Remove(lIndex)
            Next lIndex
            Screen.MousePointer = vbNormal
            DoEvents
            If mblnCheckUpd = True Then Call CheckForNewUpdates
            
            'Add Logout Activity
            Set oActivity = goDatabase.CreateBusinessObject(CLASSID_ACTIVITYLOG)
            oActivity.LogOut
            Set oWStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
            Call oWStationBO.RecordWSAsLoggedOut(mstrWSID)
            Set oWStationBO = Nothing
            Call UserLogon
        Else
            'if exit sub menu, then destroy reference
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Exit Sub Menu-" & lSubMenuID & "\" & lMenuID)
            If (colMenus.Count = 0) Then Exit Sub
            Set oMenu = colMenus(Str$(0))
            ' Count the menu options, if 1 then recall OptionSelected() passing in the SubMenu ID of 1
            '   allowing the system to exit, else continue.
            If (oMenu.SubMenus.Count = 1) Then
                Call OptionSelected(1, lMenuID, sExecPath, strParams, blnWaitForEXE, blnAddPath, blnSendSession)
            Else
                If mblnCheckUpd = True Then Call CheckForNewUpdates
                Call frmMenu.ShowMenu(1, oMenu, moWStation, moUser, True)
                If (frmMenu.tbarMenu.Buttons.Count = 1) Then
                    Call OptionSelected(1, lMenuID, sExecPath, strParams, blnWaitForEXE, blnAddPath, blnSendSession)
                End If
            End If
        End If
    End If

    Screen.MousePointer = vbNormal

End Sub 'OptionSelected

Private Sub Form_Paint()

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    Call moWStation.RecordWSAsLoggedOut(moWStation.Id)

End Sub

Private Sub Form_Resize()

    Me.AutoRedraw = False
    If Me.WindowState <> vbMinimized Then Me.WindowState = vbMinimized

End Sub

Private Sub BringAppToFront()

Dim strAppTitle As String
Dim lngRetCode  As Long
Dim lngHwnd     As Long
    
    strAppTitle = "User Logon"
    'clear current app title and form caption to aviod reselecting myself
    lngHwnd = FindWindow(vbNullString, strAppTitle)
    lngRetCode = ShowWindow(lngHwnd, SW_RESTORE)
    lngRetCode = BringWindowToTop(lngHwnd)
    On Error Resume Next
    Call AppActivate(strAppTitle)
    End

End Sub

Private Sub CheckForNewUpdates()

Const VER_FILE_EXT  As String = ".GO"
Const STATUS_FILE  As String = "STATUS.SRF"

Dim oFile     As File
Dim oFiles    As Files
Dim strFName  As String
Dim strUpdEXE As String

    'get list of files in Workstation folder
    Set oFiles = moFSO.GetFolder(mstrReleaseRoot & "Workstations\WS" & mstrWSID).Files
    mblnUpdates = False
    
    Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Checked for files (" & oFiles.Count & " found)")
    
    For Each oFile In oFiles
        strFName = UCase$(oFile.Name)
        'check if any of the files have correct prefix and suffix
        If (InStr(strFName, mstrWSID & ".") = 1) And (InStr(strFName, VER_FILE_EXT) > 1) Then
            Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Checked for files (" & oFile.Name & " found)")
            mblnUpdates = True
            Exit For
        End If
        If (InStr(strFName, STATUS_FILE) = 1) Then
            Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Checked for Status file(" & oFile.Name & " found)")
            mblnUpdates = True
            Exit For
        End If
    Next
    
    If mblnUpdates = True Then
        strUpdEXE = goSession.GetParameter(PRM_WSUPDATE_PATH)
        Call MsgBoxEx("New version detected" & vbCrLf & "Exiting menu to run update system" & vbCrLf & vbCrLf & "Press OK to continue", vbExclamation, "CTS Work-station update system", , , , , RGB_RED)
        Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Upd EXE - " & strUpdEXE)
        On Error Resume Next
        Call Shell(strUpdEXE & " EXIT " & App.Path & "\" & App.EXEName, vbNormalFocus)
        If Err.Number = 0 Then End
    End If

End Sub

' Tests if the Item Filter exe is running, if not start it.
Private Function StartItemFilter() As Boolean

Const PROCEDURE_NAME As String = "StartItemFilter"
    
    Dim objFSO As Scripting.FileSystemObject
        
    Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "Start")
    
    ' See if the Item Filter exe is running, if not start it.
    If (GetProcesses(ITEM_FILTER_EXE_NAME) = 0) Then
        ' Test if the Item Filter exe exists.
        Set objFSO = New Scripting.FileSystemObject
        If (objFSO.FileExists(App.Path & "\" & ITEM_FILTER_EXE_NAME) = False) Then
            StartItemFilter = False
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, ITEM_FILTER_EXE_NAME & " NOT FOUND")
            Call MsgBoxEx("The Item Filter exe cannot be found." & vbNewLine & vbNewLine & _
                    "Path - " & App.Path & "\" & ITEM_FILTER_EXE_NAME, vbCritical + vbOKOnly, _
                    "Item Filter Not Found", , , , , RGBMsgBox_WarnColour)
            Exit Function
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, ITEM_FILTER_EXE_NAME & " not running so starting")
            Call Shell(App.Path & "\" & ITEM_FILTER_EXE_NAME, vbHide)
            StartItemFilter = True
        End If
        Set objFSO = Nothing
    Else
        StartItemFilter = True
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, ITEM_FILTER_EXE_NAME & " running")
    End If

End Function

