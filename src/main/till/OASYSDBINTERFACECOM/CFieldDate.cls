VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFieldDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IField
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CFieldDate.cls $
' $Revision: 14 $
' $Date: 30/01/03 9:13 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Public Type tDateProps
    nId       As Long
    Value     As Date
    IsEmpty   As Boolean
    SeqNo     As Long
End Type
Private m_Props As tDateProps

'----------------------------------------------------------------------------
' Default interface
'----------------------------------------------------------------------------
Public Property Let DateProps(Props As tDateProps)
    m_Props = Props
End Property
Public Property Let DateValue(nValue As Date)
    m_Props.Value = nValue
    m_Props.IsEmpty = False
    If Year(nValue) = 1899 Then m_Props.IsEmpty = True
End Property
Public Property Get DateValue() As Date
    DateValue = m_Props.Value
End Property
Public Property Let TimeValue(nValue As Date)
    m_Props.Value = nValue
    m_Props.IsEmpty = False
    If Year(nValue) = 1899 Then m_Props.IsEmpty = True
End Property
Public Property Get TimeValue() As Date
    TimeValue = m_Props.Value
End Property
Public Property Let IsEmpty(Value As Boolean)
    m_Props.IsEmpty = Value
End Property
Public Property Get IsEmpty() As Boolean
    IsEmpty = m_Props.IsEmpty
End Property
Public Property Let SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get SeqNo() As Long
    SeqNo = m_Props.SeqNo
End Property
Public Function DateMatches(nComparator As enComparator, dValue As Date) As Integer
    Select Case nComparator
    Case Is = CMP_SELECTALL
        DateMatches = True
    Case Is = CMP_EQUAL
        DateMatches = (dValue = m_Props.Value)
    Case Is = CMP_NOTEQUAL
        DateMatches = (dValue <> m_Props.Value)
    Case Is = CMP_LESSTHAN
        DateMatches = (dValue < m_Props.Value)
    Case Is = CMP_GREATERTHAN
        DateMatches = (dValue > m_Props.Value)
    Case Is = CMP_COMPARE
        ' same as equal for non-strings
        DateMatches = (dValue = m_Props.Value)
    Case Is = CMP_LESSEQUALTHAN
        DateMatches = (dValue <= m_Props.Value)
    Case Is = CMP_GREATEREQUALTHAN
        DateMatches = (dValue >= m_Props.Value)
'    Case Is = CMP_LIKE     ' not supported for non-strings
'    Case Is = CMP_SELECTLIST
    Case Else
        Debug.Assert False
        DateMatches = False
    End Select
End Function

Public Property Get SqlLiteralTimeAsChar6() As String
    If m_Props.IsEmpty = True Then
        SqlLiteralTimeAsChar6 = "NULL"
    Else
        SqlLiteralTimeAsChar6 = "'" & Format$(m_Props.Value, "HHNNSS") & "'"
    End If
End Property

'----------------------------------------------------------------------------
' IField interface
'----------------------------------------------------------------------------

Public Property Get IField_FieldType() As enFieldType
    IField_FieldType = FieldTypeDate
End Property
Public Property Let IField_Id(nId As Long)
    m_Props.nId = nId
End Property
Public Property Get IField_Id() As Long
    IField_Id = m_Props.nId
End Property
Private Property Let IField_IsEmpty(ByVal RHS As Boolean)
    m_Props.IsEmpty = RHS
End Property

Private Property Get IField_IsEmpty() As Boolean
    IField_IsEmpty = m_Props.IsEmpty
End Property
Public Property Let IField_SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get IField_SeqNo() As Long
    IField_SeqNo = m_Props.SeqNo
End Property


Public Property Get IField_DisplayValue() As String
    IField_DisplayValue = CStr(m_Props.Value)
End Property
Public Property Get IField_SqlLiteral() As String
    If m_Props.IsEmpty = True Then
        IField_SqlLiteral = "NULL"
    Else
        IField_SqlLiteral = "{ts '" & Format$(m_Props.Value, "YYYY-MM-DD HH:NN:SS") & "'}"
    End If
End Property
Public Property Let IField_ValueAsVariant(Value As Variant)
    ' Needs validation here
    If IsNull(Value) Then
        m_Props.IsEmpty = True
    ElseIf LenB(CStr(Value)) = 0 Then
        m_Props.IsEmpty = True
    ElseIf Year(Value) = 1899 Then
        m_Props.IsEmpty = True
    Else
        m_Props.IsEmpty = False
        m_Props.Value = Format(Value)
    End If
End Property
Public Property Get IField_ValueAsVariant() As Variant
    IField_ValueAsVariant = m_Props.Value
End Property
Public Function IField_CreateNew() As IField
    ' A faster way of creating an object
    Set IField_CreateNew = New CFieldDate
End Function
Public Function IField_Duplicate() As IField
    Dim oField As New CFieldDate
    oField.DateProps = m_Props
    Set IField_Duplicate = oField
End Function
Public Function IField_Matches(nComparator As enComparator, vtValue As Variant) As Integer
    On Error GoTo Err_IField_Matches
    IField_Matches = DateMatches(nComparator, CDate(vtValue))
    Exit Function
    
Err_IField_Matches:
    ' Kludge because we allow old database to have Time as a char(6) = "      "
    IField_Matches = (nComparator = CMP_SELECTALL)
End Function
Public Function Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Select Case (eInterfaceType)
    Case FieldInterfaceIField:
        Dim oField As IField
        Set oField = Me
        Set Interface = oField
    Case Else:
        Set Interface = Me
    End Select
End Function
Public Function IField_Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Set IField_Interface = Interface(eInterfaceType)
End Function



