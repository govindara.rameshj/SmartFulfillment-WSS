Attribute VB_Name = "Globals"
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/Globals.bas $
' $Revision: 5 $
' $Date: 4/11/02 10:27 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' Everything should be encapsulated and so there should
' be no globals.  However, constants can usefully be global.
'----------------------------------------------------------------------------


