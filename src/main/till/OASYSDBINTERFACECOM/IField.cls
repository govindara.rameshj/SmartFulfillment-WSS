VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/IField.cls $
' $Revision: 15 $
' $Date: 31/10/02 15:02 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------

Public Enum enFieldType
    FieldTypeInvalid = 0    ' Initial value
    FieldTypeString = 1
    FieldTypeLong = 2
    FieldTypeBool = 3
    FieldTypeDate = 4
    FieldTypeDouble = 5
    FieldTypeGroup = 6
End Enum
Public Enum enFieldInterfaceType
    FieldInterfaceNative = 0    ' Default
    FieldInterfaceIField = 1
End Enum

Public Type tFieldProps
    ' Id combines class id in top int and property id in bottom int
    nId As Long
End Type

Public Property Get FieldType() As enFieldType
End Property
Public Property Let Id(nId As Long)
End Property
Public Property Get Id() As Long
End Property
Public Property Let SeqNo(lSeqNo As Long)
End Property
Public Property Get SeqNo() As Long
End Property
Public Property Get DisplayValue() As String
End Property
Public Property Get SqlLiteral() As String
End Property
Public Property Let ValueAsVariant(Value As Variant)
End Property
Public Property Get ValueAsVariant() As Variant
End Property
Public Function Duplicate() As Object
End Function
Public Function CreateNew() As Object
End Function
#If ccEarlyBinding = 1 Then
Public Function Matches(nComparator As enComparator, vtValue As Variant) As Integer
#Else
Public Function Matches(nComparator As Long, vtValue As Variant) As Integer
#End If
End Function
Public Property Get IsEmpty() As Boolean
End Property
Public Property Let IsEmpty(ByVal Value As Boolean)
End Property
Public Function Interface(Optional eInterfaceType As enFieldInterfaceType) As Object
End Function

