VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IRow
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CRow.cls $
' $Revision: 6 $
' $Date: 10/11/02 9:23a $
' $Author: Mauricem $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Private m_colFields As Collection   ' each As IField

Private Sub Class_Initialize()
    Set m_colFields = New Collection
End Sub
Private Sub Class_Terminate()
    Set m_colFields = Nothing
End Sub
Public Sub Clear()
    Do While Count > 0
        m_colFields.Remove m_colFields.Count
    Loop
End Sub

Public Sub Add(oField As IField)
    Debug.Assert Not (oField Is Nothing)
    ' We don't own the selector, so create our own duplicate object
    Call m_colFields.Add(oField.Duplicate)
End Sub
Public Property Get Count() As Long
    Count = m_colFields.Count
End Property
Public Property Get Field(nInstance As Integer) As IField
    Debug.Assert Not nInstance > Count
    If nInstance > m_colFields.Count Then
        Debug.Assert False
        Set Field = Nothing
    Else
        Set Field = m_colFields.Item(nInstance)
    End If
End Property
Friend Function Duplicate() As CRow
'    Dim oField As IField
    Dim oField As IField
    Set Duplicate = New CRow
    For Each oField In m_colFields
        Duplicate.Add oField
    Next
End Function

'----------------------------------------------------------------------------
' IRow Implementation
'----------------------------------------------------------------------------
Public Sub IRow_Clear()
    Clear
End Sub

Public Sub IRow_Add(oField As IField)
    Add oField
End Sub

Public Property Get IRow_Count() As Long
    IRow_Count = Count
End Property

Public Property Get IRow_Field(nInstance As Integer) As IField
    Set IRow_Field = Field(nInstance)
End Property

Friend Function IRow_Duplicate() As IRow
    Set IRow_Duplicate = Duplicate
End Function


