VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRowSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IRowSelector
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CRowSelector.cls $
' $Revision: 12 $
' $Date: 18/11/02 13:04 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Private m_colFieldSelectors As Collection   ' each As CFieldSelector
Private m_oSysRoot          As ISysRoot       ' ISysRoot interface

Private Sub Class_Initialize()
    Set m_colFieldSelectors = New Collection
End Sub
Private Sub Class_Terminate()
    Set m_colFieldSelectors = Nothing
End Sub

Public Sub Clear()
    Do While Count > 0
        m_colFieldSelectors.Remove Count
    Loop
End Sub

Public Sub Add(oFieldSelector As CFieldSelector)
    Debug.Assert Not (oFieldSelector Is Nothing)
    ' We DON'T own the selector, so create our own duplicate object
    m_colFieldSelectors.Add oFieldSelector.Duplicate
End Sub
Public Sub AddSelection(Comparator As enComparator, oField As IField, Optional bIsSelectOnly As Boolean)
    ' Purely a short-cut to simplify use of the selector interfaces
    ' by removing the need to create a Field Selector first
    ' oField is underlying object i/f underneath IField
    Dim oFieldSelector As New CFieldSelector
    oFieldSelector.Initialise Comparator, oField, bIsSelectOnly
    ' We DO own the selector, so pass ownership on (don't need to Duplicate)
    m_colFieldSelectors.Add oFieldSelector
End Sub
Public Sub AddSelectAll(FieldID As Long)
    ' Purely a short-cut to simplify use of the selector interfaces
    ' by removing the need to create a Field Selector first
    Dim oFieldSelector As New CFieldSelector
    Set oFieldSelector.SysRoot = m_oSysRoot
    oFieldSelector.InitialiseAsSelectAll FieldID
    Debug.Assert Not (oFieldSelector Is Nothing)
    ' We DO own the selector, so pass ownership on (don't need to Duplicate)
    m_colFieldSelectors.Add oFieldSelector
End Sub
Public Sub AddSelectValue(Comparator As Long, FieldID As Long, Value As Variant)
    ' Purely a short-cut to simplify use of the selector interfaces
    ' by removing the need to create a Field Selector first
    Dim oFieldSelector As New CFieldSelector
    Dim oField As IField
    Set oField = m_oSysRoot.CreateFieldFromFid(FieldID)
    oField.ValueAsVariant = Value
    oFieldSelector.Initialise Comparator, oField
    Debug.Assert Not (oFieldSelector Is Nothing)
    ' We DO own the selector, so pass ownership on (don't need to Duplicate)
    m_colFieldSelectors.Add oFieldSelector
End Sub
Public Property Get Count() As Long
    Count = m_colFieldSelectors.Count
End Property
Public Sub Remove(nInstance As Integer)
    If nInstance > Count Then
        Debug.Assert False
    Else
        m_colFieldSelectors.Remove (nInstance)
    End If
End Sub
Public Property Get FieldSelector(nInstance As Integer) As CFieldSelector
    If nInstance > Count Or nInstance = 0 Then
        Debug.Assert False
    Else
        Set FieldSelector = m_colFieldSelectors.Item(nInstance)
    End If
End Property
Public Property Set FieldSelector(nInstance As Integer, oFSelector As CFieldSelector)
    ' We do a replace at the given instance number
    If nInstance > Count Or nInstance = 0 Then
        Debug.Assert False
    Else
        m_colFieldSelectors.Add oFSelector, , nInstance
        m_colFieldSelectors.Remove nInstance + 1
    End If
End Property
Public Function GetInstanceFromFid(nFid As Long) As Integer
    ' return the instance number for the given FID
    Dim i As Integer
    For i = 1 To m_colFieldSelectors.Count
        If m_colFieldSelectors.Item(i).Field.Id = nFid Then
            GetInstanceFromFid = i
            Exit For
        End If
    Next i
End Function
Public Property Set SysRoot(oSysRoot As ISysRoot)
    Set m_oSysRoot = oSysRoot
End Property
Public Function Merge(oRowSelector As CRowSelector) As Boolean
    ' For each item in the input row, add it to my collection as long
    ' as there is no item for the associated FID already there.  If so
    ' we simply don't add the item.
    Dim i           As Integer
    Dim nFid        As Long
    Dim colNew      As Collection
    Set colNew = New Collection
    
    For i = 1 To oRowSelector.Count
        nFid = oRowSelector.FieldSelector(i).Field.Id
        If GetInstanceFromFid(nFid) = 0 Then
            ' Not found, so we add
            If (nFid And &H7FFF) = 1 Then
                ' We have an Id selector which must be first, so Add now
                ' We DON'T own the selector, so create our own duplicate object
                If m_colFieldSelectors.Count = 0 Then
                    m_colFieldSelectors.Add oRowSelector.FieldSelector(i).Duplicate
                Else
                    m_colFieldSelectors.Add oRowSelector.FieldSelector(i).Duplicate, , 1
                End If
            Else
                ' For performance, we delay adding other selectors
                colNew.Add oRowSelector.FieldSelector(i)
            End If
        End If
    Next i
    For i = 1 To colNew.Count
        Add colNew.Item(i)
    Next i
    Set colNew = Nothing
    Merge = True
End Function
Public Function Override(oRowSelector As CRowSelector) As Boolean
    ' For each item in the input row, add it to my collection, replacing
    ' any item for the associated FID that is already there.
    Dim i           As Integer
    Dim j           As Integer
    Dim nFid        As Long
    
    For i = 1 To oRowSelector.Count
        nFid = oRowSelector.FieldSelector(i).Field.Id
        j = GetInstanceFromFid(nFid)
        If j > 0 Then
            ' Found, so replace
            Set FieldSelector(j) = oRowSelector.FieldSelector(i)
        End If
    Next i
    ' Now we can simply do a merge
    Override = Merge(oRowSelector)
End Function

Public Property Get DebugString() As String
    Dim i As Integer
    DebugString = "Row Selector: " & Count & " Items for short FIDs: "
    For i = 1 To Count
        Select Case i
        Case 1
        Case 6
            ' A practical limit
            DebugString = DebugString & ", ..."
            Exit For
        Case Else
            DebugString = DebugString & ", "
        End Select
        DebugString = DebugString & m_colFieldSelectors.Item(i).Field.Id
        If i = 1 Then
            DebugString = DebugString & "(" & m_colFieldSelectors.Item(i).Field.DisplayValue & ")"
        End If
    Next i
End Property

Public Function ExtractFieldRow() As CRow
    ' Create a CRow of field objects that correspond to the selector.
    ' If a field selector has more than one field object, we just take the default one.
    Dim i As Integer
    Set ExtractFieldRow = New CRow
    For i = 1 To Count
        ExtractFieldRow.Add FieldSelector(i).Field
    Next i
End Function

'----------------------------------------------------------------------------
' IRowSelector interface
'----------------------------------------------------------------------------
Public Sub IRowSelector_Clear()
    Clear
End Sub

Public Sub IRowSelector_Add(oFieldSelector As IFieldSelector)
    Add oFieldSelector
End Sub

Public Sub IRowSelector_AddSelection(Comparator As enComparator, oField As IField, Optional bIsSelectOnly As Boolean)
    AddSelection Comparator, oField, bIsSelectOnly
End Sub

Public Sub IRowSelector_AddSelectAll(FieldID As Long)
    AddSelectAll FieldID
End Sub

Public Sub IRowSelector_AddSelectValue(Comparator As Long, FieldID As Long, Value As Variant)
    AddSelectValue Comparator, FieldID, Value
End Sub

Public Property Get IRowSelector_Count() As Long
    IRowSelector_Count = Count
End Property

Public Sub IRowSelector_Remove(nInstance As Integer)
    Remove nInstance
End Sub

Public Property Get IRowSelector_FieldSelector(nInstance As Integer) As IFieldSelector
    Set IRowSelector_FieldSelector = FieldSelector(nInstance)
End Property

Public Property Set IRowSelector_FieldSelector(nInstance As Integer, oFSelector As IFieldSelector)
   Set FieldSelector(nInstance) = oFSelector
End Property

Public Function IRowSelector_GetInstanceFromFid(nFid As Long) As Integer
    IRowSelector_GetInstanceFromFid = GetInstanceFromFid(nFid)
End Function

Public Property Set IRowSelector_SysRoot(oSysRoot As ISysRoot)
    Set SysRoot = oSysRoot
End Property

Public Function IRowSelector_Merge(oRowSelector As IRowSelector) As Boolean
    IRowSelector_Merge = Merge(oRowSelector)
End Function

Public Function IRowSelector_Override(oRowSelector As IRowSelector) As Boolean
    IRowSelector_Override = Override(oRowSelector)
End Function

Public Property Get IRowSelector_DebugString() As String
    IRowSelector_DebugString = DebugString
End Property

Public Function IRowSelector_ExtractFieldRow() As IRow
    Set IRowSelector_ExtractFieldRow = ExtractFieldRow
End Function


