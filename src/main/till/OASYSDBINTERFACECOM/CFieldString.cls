VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFieldString"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IField
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CFieldString.cls $
' $Revision: 19 $
' $Date: 26/11/02 15:40 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Public Type tStringProps
    nId       As Long
    Value     As String
    IsEmpty   As Boolean
    SeqNo     As Long
End Type

Private m_Props As tStringProps

'----------------------------------------------------------------------------
' No Default interface, except for friends
'----------------------------------------------------------------------------
Public Property Let StringProps(Props As tStringProps)
    m_Props = Props
End Property
Public Property Let StringValue(strValue As String)
    ' Ensure we trim trailing spaces from the given value
    m_Props.Value = strValue
End Property
Public Property Get StringValue() As String
    StringValue = m_Props.Value
End Property
Public Property Let IsEmpty(Value As Boolean)
    m_Props.IsEmpty = Value
End Property
Public Property Get IsEmpty() As Boolean
    IsEmpty = m_Props.IsEmpty
End Property
Public Property Let SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get SeqNo() As Long
    SeqNo = m_Props.SeqNo
End Property
Public Function StringMatches(nComparator As enComparator, strValue As String) As Integer
    Select Case nComparator
    Case Is = CMP_SELECTALL
        ' Try the most common case first
        StringMatches = True
    Case Is = CMP_EQUAL
        ' Ensure we trim trailing spaces from the test value
        StringMatches = (RTrim$(strValue) = RTrim$(m_Props.Value))
    Case Is = CMP_NOTEQUAL
        StringMatches = (RTrim$(strValue) <> m_Props.Value)
    Case Is = CMP_LESSTHAN
        StringMatches = (RTrim$(strValue) < m_Props.Value)
    Case Is = CMP_GREATERTHAN
        StringMatches = (RTrim$(strValue) > m_Props.Value)
    Case Is = CMP_LESSEQUALTHAN
        StringMatches = (RTrim$(strValue) <= m_Props.Value)
    Case Is = CMP_GREATEREQUALTHAN
        StringMatches = (RTrim$(strValue) >= m_Props.Value)
    Case Is = CMP_COMPARE
        StringMatches = (UCase$(RTrim$(strValue)) = UCase$(RTrim$(m_Props.Value)))
    Case Is = CMP_LIKE
        ' may be of form "%xxx%", so remove % and look for xxx
        Dim s As String
        s = m_Props.Value
        If Right$(s, 1) = "%" Then
            s = Left$(s, Len(s) - 1)
        End If
        If Left$(s, 1) = "%" Then
            s = Right$(s, Len(s) - 1)
        End If
        ' is the substring within the string?
        StringMatches = (InStr(1, RTrim$(strValue), s, vbTextCompare) <> 0)
'    Case Is = CMP_SELECTLIST
    Case Else
        Debug.Assert False
        StringMatches = False
    End Select
End Function

'----------------------------------------------------------------------------
' IField interface
'----------------------------------------------------------------------------

Public Property Get IField_FieldType() As enFieldType
    IField_FieldType = FieldTypeString
End Property
Public Property Let IField_Id(nId As Long)
    m_Props.nId = nId
End Property
Public Property Get IField_Id() As Long
    IField_Id = m_Props.nId
End Property
Private Property Let IField_IsEmpty(ByVal RHS As Boolean)
    m_Props.IsEmpty = RHS
End Property

Private Property Get IField_IsEmpty() As Boolean
    IField_IsEmpty = m_Props.IsEmpty
End Property
Public Property Let IField_SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get IField_SeqNo() As Long
    IField_SeqNo = m_Props.SeqNo
End Property

Public Property Get IField_DisplayValue() As String
    IField_DisplayValue = CStr(m_Props.Value)
End Property
Public Property Get IField_SqlLiteral() As String
    'check if empty/NULL
    If m_Props.IsEmpty = True Then
        IField_SqlLiteral = "NULL"
    Else
        ' We must ensure the string contains no delimiters, else SQL will fail
        IField_SqlLiteral = "'" & Replace(IField_DisplayValue, "'", "''") & "'"
    End If
End Property
Public Property Let IField_ValueAsVariant(Value As Variant)
    If IsNull(Value) Then
        m_Props.IsEmpty = True
    Else
        ' Null value is held as empty string
        m_Props.IsEmpty = False
        m_Props.Value = Value
    End If
End Property
Public Property Get IField_ValueAsVariant() As Variant
    IField_ValueAsVariant = m_Props.Value
End Property
Public Function IField_CreateNew() As IField
    ' A faster way of creating an object
    Set IField_CreateNew = New CFieldString
End Function
Public Function IField_Duplicate() As IField
    Dim oField As New CFieldString
    oField.StringProps = m_Props
    Set IField_Duplicate = oField
End Function
Public Function IField_Matches(nComparator As enComparator, vtValue As Variant) As Integer
    IField_Matches = StringMatches(nComparator, CStr(vtValue))
End Function
Public Function Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Select Case (eInterfaceType)
    Case FieldInterfaceIField:
        Dim oField As IField
        Set oField = Me
        Set Interface = oField
    Case Else:
        Set Interface = Me
    End Select
End Function
Public Function IField_Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Set IField_Interface = Interface(eInterfaceType)
End Function

