VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IView
'<CAMH>****************************************************************************************
'* Module : CView
'* Date   : 11/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CView.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 10/11/02 4:42p $
'* $Revision: 3 $
'* Versions:
'* 11/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************

Const MODULE_NAME As String = "CView"

Private m_colRows As Collection   ' each As CRow

Private Sub Class_Initialize()
    Set m_colRows = New Collection
End Sub
Private Sub Class_Terminate()
    Set m_colRows = Nothing
End Sub

Public Sub Add(oRow As CRow)
    Debug.Assert Not (oRow Is Nothing)
    ' We don't own the selector, so create our own duplicate object
    m_colRows.Add oRow.Duplicate
End Sub
Public Property Get Count() As Long
    Count = m_colRows.Count
    'added M.Milne 11/9/02
    Call DebugMsg(MODULE_NAME, "Count", endlDebug, "Record Count is " & m_colRows.Count)
    'Debug.Assert (Count > 0)
End Property
Public Property Get Row(nInstance As Integer) As CRow
    Debug.Assert Not nInstance > Count
    If nInstance > m_colRows.Count Then
        Debug.Assert False
        Set Row = Nothing
    Else
        Set Row = m_colRows.Item(nInstance)
    End If
End Property

'----------------------------------------------------------------------------
' IView Implementation
'----------------------------------------------------------------------------
Public Sub IView_Add(oRow As IRow)
    Add oRow
End Sub

Public Property Get IView_Count() As Long
    IView_Count = Count
End Property

Public Property Get IView_Row(nInstance As Integer) As IRow
    Set IView_Row = Row(nInstance)
End Property


