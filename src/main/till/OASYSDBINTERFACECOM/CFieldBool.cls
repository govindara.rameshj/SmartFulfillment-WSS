VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFieldBool"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IField
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CFieldBool.cls $
' $Revision: 10 $
' $Date: 26/11/02 15:40 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Public Type tBoolProps
    nId       As Long
    Value     As Boolean
    IsEmpty   As Boolean
    SeqNo     As Long
End Type
Private m_Props As tBoolProps

'----------------------------------------------------------------------------
' Default interface
'----------------------------------------------------------------------------
Public Property Let BoolProps(Props As tBoolProps)
    m_Props = Props
End Property
Public Property Let BoolValue(nValue As Boolean)
    m_Props.Value = nValue
End Property
Public Property Get BoolValue() As Boolean
    BoolValue = m_Props.Value
End Property
Public Property Let IsEmpty(Value As Boolean)
    m_Props.IsEmpty = Value
End Property
Public Property Get IsEmpty() As Boolean
    IsEmpty = m_Props.IsEmpty
End Property
Public Property Let SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get SeqNo() As Long
    SeqNo = m_Props.SeqNo
End Property
Public Function BoolMatches(nComparator As enComparator, bValue As Boolean) As Integer
    Select Case nComparator
    Case Is = CMP_SELECTALL
        BoolMatches = True
    Case Is = CMP_EQUAL
        BoolMatches = (bValue = m_Props.Value)
    Case Is = CMP_NOTEQUAL
        BoolMatches = (bValue <> m_Props.Value)
    Case Is = CMP_LESSTHAN
        BoolMatches = (bValue < m_Props.Value)
    Case Is = CMP_GREATERTHAN
        BoolMatches = (bValue > m_Props.Value)
    Case Is = CMP_COMPARE
        ' same as equal for non-strings
        BoolMatches = (bValue = m_Props.Value)
'    Case Is = CMP_LESSEQUALTHAN
'    Case Is = CMP_GREATEREQUALTHAN
'    Case Is = CMP_LIKE     ' not supported for non-strings
'    Case Is = CMP_SELECTLIST
    Case Else
        Debug.Assert False
        BoolMatches = False
    End Select
End Function

'----------------------------------------------------------------------------
' IField interface
'----------------------------------------------------------------------------

Public Property Get IField_FieldType() As enFieldType
    IField_FieldType = FieldTypeBool
End Property
Public Property Let IField_Id(nId As Long)
    m_Props.nId = nId
End Property
Public Property Get IField_Id() As Long
    IField_Id = m_Props.nId
End Property
Public Property Let IField_SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get IField_SeqNo() As Long
    IField_SeqNo = m_Props.SeqNo
End Property

Public Property Get IField_DisplayValue() As String
    IField_DisplayValue = CStr(m_Props.Value)
End Property

Private Property Let IField_IsEmpty(ByVal RHS As Boolean)
    m_Props.IsEmpty = RHS
End Property

Private Property Get IField_IsEmpty() As Boolean
    IField_IsEmpty = m_Props.IsEmpty
End Property

Public Property Get IField_SqlLiteral() As String
    If m_Props.Value = True Then
        IField_SqlLiteral = 1
    Else
        IField_SqlLiteral = 0
    End If
End Property
Public Property Let IField_ValueAsVariant(Value As Variant)
    ' Needs validation here
    If IsNull(Value) = True Then
        m_Props.Value = False 'catch if null, so default to False
    Else
        m_Props.Value = Value
    End If
End Property
Public Property Get IField_ValueAsVariant() As Variant
    IField_ValueAsVariant = m_Props.Value
End Property
Public Function IField_CreateNew() As IField
    ' A faster way of creating an object
    Set IField_CreateNew = New CFieldBool
End Function
Public Function IField_Duplicate() As IField
    Dim oField As New CFieldBool
    oField.BoolProps = m_Props
    Set IField_Duplicate = oField
End Function
Public Function IField_Matches(nComparator As enComparator, vtValue As Variant) As Integer
    IField_Matches = BoolMatches(nComparator, CBool(vtValue))
End Function
Public Function Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Select Case (eInterfaceType)
    Case FieldInterfaceIField:
        Dim oField As IField
        Set oField = Me
        Set Interface = oField
    Case Else:
        Set Interface = Me
    End Select
End Function
Public Function IField_Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Set IField_Interface = Interface(eInterfaceType)
End Function



