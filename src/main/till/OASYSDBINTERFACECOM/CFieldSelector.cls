VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFieldSelector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IFieldSelector
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CFieldSelector.cls $
' $Revision: 15 $
' $Date: 25/02/03 11:07 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------

'Public Type tFSelectorProps
'    nComparator     As enComparator
'    oSingleField    As IField
'    ' When unset, a selection includes the item in the returned row.  When set, it is used to select the row and then discarded.
'    bIsSelectOnly As Boolean
'    ' When set, reverses the sense of the returned boolean value for all operations, except COMPARE which returns tri-state.
'    bNegateResult As Boolean
'End Type
Private m_Props             As tFSelectorProps
Private m_oSysRoot          As ISysRoot           ' ISysRoot interface

' Perhaps m_colFields would be more efficient as an array?
Private m_colFields     As Collection   ' each As IField
                                        ' > 1 for CMP_SELECTLIST
' When unset, a selection includes the item in the returned row.  When set, it is used to select the row and then discarded.
'Private m_bIsSelectOnly As Boolean
' When set, reverses the sense of the returned boolean value for all operations, except COMPARE which returns tri-state.
'Private m_bNegateResult As Boolean
' The dbParam is a long value that is set an used only by the database implementation.
Private m_nDbParam As Long

Private Sub Class_Terminate()
    Clear       ' Tidy up
End Sub

Public Function Initialise(Comparator As enComparator, oField As IField, Optional bIsSelectOnly As Boolean)
        
    Debug.Assert Not oField Is Nothing
    Debug.Assert Not oField.Id = 0  ' Id must be set to a valid property
    If oField Is Nothing Or oField.Id = 0 Then
        Debug.Assert False
        Exit Function
    End If
    ' May be a re-initialisation, so tidy up old stuff
    Set m_Props.oSingleField = Nothing
    Set m_colFields = Nothing
    
    m_Props.nComparator = Comparator
    m_Props.bIsSelectOnly = bIsSelectOnly
    AddField oField
End Function

Public Function InitialiseAsSelectAll(FieldId As Long)
    Dim oField As IField
    If m_oSysRoot Is Nothing Then
        Debug.Assert False
    Else
        Set oField = m_oSysRoot.CreateFieldFromFid(FieldId)
        If oField Is Nothing Then
            ' trap a null pointer
            Debug.Assert False
        Else
            oField.Id = FieldId
            Initialise CMP_SELECTALL, oField
        End If
    End If
End Function

Public Function AddField(oField As IField)
    ' We don't own the input field, so duplicate our own
    ' Ensure all added fields have the same FID, ignore otherwise.
    If m_Props.oSingleField Is Nothing Then
        Set m_Props.oSingleField = oField.Duplicate
    Else
        If m_Props.oSingleField.FieldType = oField.FieldType Then
            If m_colFields Is Nothing Then
                Set m_colFields = New Collection
            End If
            Select Case (m_Props.nComparator)
            Case CMP_INRANGE:
                ' Can only be two values in range and first must be lower than first
                If m_colFields.Count < 1 Then
                    If m_Props.oSingleField.Matches(CMP_GREATERTHAN, oField.ValueAsVariant) Then
                        m_colFields.Add oField.Duplicate
                    Else
                        ' Reverse fields to put lowest first
                        m_colFields.Add m_Props.oSingleField
                        Set m_Props.oSingleField = oField.Duplicate
                    End If
                Else
                    Debug.Assert False  ' More than 2 fields in a range!
                End If
            Case Else
                m_colFields.Add oField.Duplicate
            End Select
        Else
            ' We don't allow a field selector to contain different field types!
            Debug.Assert False
        End If
    End If
End Function

Public Function Duplicate() As CFieldSelector
    Dim oField  As IField
    Dim l_Props As tFSelectorProps
    ' Make a local copy of our properties
    l_Props = m_Props
    ' Copied a pointer to 'our' field object, so now we duplicate another for 'him'.
    Set l_Props.oSingleField = m_Props.oSingleField.Duplicate
    ' Now create 'him' and set 'his' properties
    Set Duplicate = New CFieldSelector
    Duplicate.FSelectorProps = l_Props
    If Not (m_colFields Is Nothing) Then
        For Each oField In m_colFields
            Duplicate.AddField oField
        Next
    End If
End Function
Public Property Let FSelectorProps(Props As tFSelectorProps)
    m_Props = Props
End Property

Public Property Get Comparator() As enComparator
    Comparator = m_Props.nComparator
End Property
Public Property Get IsSelectOnly() As Long
    IsSelectOnly = m_Props.bIsSelectOnly
End Property

'Private Property Get Fields() As Collection
'    Set Fields = m_colFields
'End Property

Public Property Get FieldCount() As Integer
    FieldCount = 1
    If Not (m_colFields Is Nothing) Then
        FieldCount = 1 + m_colFields.Count
    End If
End Property
Public Property Get Field(Optional nInstance As Integer) As IField
    If nInstance = 0 Then
        ' if we dont' say which, then get the first
        nInstance = 1
    End If
    If nInstance = 1 Then
        Set Field = m_Props.oSingleField
    Else
        If nInstance > FieldCount Then
            ' Beyond the end of the array!
            Debug.Assert False
            Set Field = Nothing
        Else
            Set Field = m_colFields.Item(nInstance - 1)
        End If
    End If
End Property

Public Property Set SysRoot(oSysRoot As ISysRoot)
    Set m_oSysRoot = oSysRoot
End Property
Public Function Clear()
    ' Simply clears out any existing fields, so we can re-use the object.
    Set m_Props.oSingleField = Nothing
    Set m_colFields = Nothing
End Function
Public Function Matches(vtValue As Variant) As Integer
    Dim i       As Integer
    Dim oField  As IField
    Select Case m_Props.nComparator
    Case Is = CMP_SELECTLIST
        ' We have a list of fields and if any one matches then we return true
        For i = 1 To FieldCount
            Set oField = Field(i)
            Matches = oField.Matches(CMP_EQUAL, vtValue)
            If Matches Then
                ' The value matches one from our list, so return true.
                Exit For
            End If
        Next i
        If m_Props.bNegateResult Then Matches = Not Matches
    Case Is = CMP_INRANGE
        ' False if less than first (lowest) or greater than second (highest)
        If Not m_Props.oSingleField.Matches(CMP_LESSTHAN, vtValue) Then
            ' Greater or equal to lower value, so check second value
            If FieldCount = 2 Then
                Set oField = Field(2)
                Matches = Not oField.Matches(CMP_GREATERTHAN, vtValue)
            Else
                Debug.Assert False  ' Not two fields defining the range
            End If
        End If
        If m_Props.bNegateResult Then Matches = Not Matches
    Case Else
        ' Delegate to the field
        Matches = m_Props.oSingleField.Matches(CInt(m_Props.nComparator), vtValue)
    End Select
    
End Function

Public Property Get NotComparator() As Boolean
    ' When true, reverses the sense of some comparators
    ' We simulate an interface that is only to be used by the database implementation.
    NotComparator = m_Props.bNegateResult
End Property
Public Property Let NotComparator(Value As Boolean)
    m_Props.bNegateResult = Value
End Property

Public Property Get IDb_DbParam() As Long
    ' We simulate an interface that is only to be used by the database implementation.
    IDb_DbParam = m_nDbParam
End Property
Public Property Let IDb_DbParam(nDbParam As Long)
    m_nDbParam = nDbParam
End Property

'----------------------------------------------------------------------------
' IFieldSelector interface
'----------------------------------------------------------------------------
Public Sub IFieldSelector_Initialise(Comparator As enComparator, oField As IField, Optional bIsSelectOnly As Boolean)
    Initialise Comparator, oField, bIsSelectOnly
End Sub

Public Sub IFieldSelector_InitialiseAsSelectAll(FieldId As Long)
    InitialiseAsSelectAll FieldId
End Sub

Public Sub IFieldSelector_AddField(oField As IField)
    AddField oField
End Sub

Public Function IFieldSelector_Duplicate() As IFieldSelector
    Set IFieldSelector_Duplicate = Duplicate
End Function

Public Property Let IFieldSelector_FSelectorProps(Props As tFSelectorProps)
    FSelectorProps = Props
End Property

Public Property Get IFieldSelector_Comparator() As enComparator
    IFieldSelector_Comparator = Comparator
End Property

Public Property Get IFieldSelector_IsSelectOnly() As Long
    IFieldSelector_IsSelectOnly = IsSelectOnly
End Property

Public Property Get IFieldSelector_FieldCount() As Integer
    IFieldSelector_FieldCount = FieldCount
End Property

Public Property Get IFieldSelector_Field(Optional nInstance As Integer) As IField
    Set IFieldSelector_Field = Field(nInstance)
End Property

Public Property Set IFieldSelector_SysRoot(oSysRoot As ISysRoot)
    Set SysRoot = oSysRoot
End Property

Public Sub IFieldSelector_Clear()
    Clear
End Sub

Public Function IFieldSelector_Matches(vtValue As Variant) As Integer
    IFieldSelector_Matches = Matches(vtValue)
End Function

Public Property Get IFieldSelector_NotComparator() As Boolean
    IFieldSelector_NotComparator = NotComparator
End Property

Public Property Let IFieldSelector_NotComparator(Value As Boolean)
    NotComparator = Value
End Property

Public Property Get IFieldSelector_IDbDbParam() As Long
    IFieldSelector_IDbDbParam = IDb_DbParam
End Property

Public Property Let IFieldSelector_IDbDbParam(nDbParam As Long)
    IDb_DbParam = nDbParam
End Property


