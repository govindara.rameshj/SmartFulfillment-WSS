VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFieldGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IField
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CFieldGroup.cls $
' $Revision: 6 $
' $Date: 26/11/02 15:40 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
'----------------------------------------------------------------------------
Public Type tGroupProps
    nId       As Long
    ' There will always be a CRow present as a value.
    ' When count is zero, IsEmpty is true.
    Value     As CRow
    SeqNo     As Long
    ' Group fields usually hold logical arrays of occurring fields; the whole array.
    ' However, if we only want one item in the array, we can set the subscript (non-zero).
    ' We then can discard all the fields in the array bar the one corresponding to subscript.
    Subscript As Integer
End Type

Private m_Props As tGroupProps


Private Sub Class_Initialize()
    ' Ensure there will always be a CRow present as a value.
    Set m_Props.Value = New CRow
End Sub
'----------------------------------------------------------------------------
' No Default interface, except for friends
'----------------------------------------------------------------------------
Public Property Let GroupProps(Props As tGroupProps)
    m_Props = Props
    ' We have an object, so create our own copy.
    GroupRow = Props.Value
End Property
Public Property Let GroupRow(oRow As CRow)
    If oRow Is Nothing Then
        Debug.Assert False
        m_Props.Value.Clear
    Else
        Set m_Props.Value = oRow.Duplicate
    End If
End Property
Public Property Get GroupRow() As CRow
    ' We don't duplicate, else the user cannot set the sub-fields in our row
    Set GroupRow = m_Props.Value
End Property
Public Property Let IsEmpty(bValue As Boolean)
    If bValue Then
        m_Props.Value.Clear
    Else
        Debug.Assert False
    End If
End Property
Public Property Get IsEmpty() As Boolean
    IsEmpty = (m_Props.Value.Count = 0)
End Property
Public Property Let SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get SeqNo() As Long
    SeqNo = m_Props.SeqNo
End Property
Public Function GroupMatches(nComparator As enComparator, oRow As CRow) As Integer

    ' Default is always false
    GroupMatches = False
    
    If oRow Is Nothing Then
        Debug.Assert False
        Exit Function
    End If
    
    Select Case nComparator
    Case Is = CMP_SELECTALL
        ' Try the most common case first
        GroupMatches = True
    Case Is = CMP_EQUAL
        GroupMatches = True
        If m_Props.Value.Count = oRow.Count Then
            ' Check each field matches
            Dim i As Integer
            Dim oField As IField
            For i = 1 To m_Props.Value.Count
                Set oField = m_Props.Value.Field(i)
                If Not (oField.Matches(CMP_EQUAL, oRow.Field(i).ValueAsVariant)) Then
                    GroupMatches = False
                    Exit For
                End If
            Next i
        Else
            GroupMatches = False
        End If
    Case Is = CMP_NOTEQUAL
        GroupMatches = Not GroupMatches(enComparator.CMP_EQUAL, oRow)
    Case Is = CMP_LESSTHAN
        ' Default is always false
    Case Is = CMP_GREATERTHAN
        ' Default is always false
    Case Is = CMP_LESSEQUALTHAN
    Case Is = CMP_GREATEREQUALTHAN
    Case Is = CMP_COMPARE
        ' Default is always false
    Case Is = CMP_LIKE
        ' Default is always false
'    Case Is = CMP_SELECTLIST
    Case Else
        Debug.Assert False
        ' Default is always false
    End Select
End Function
Public Property Get Subscript() As Integer
    ' Zero means subscript is not in use.
    Subscript = m_Props.Subscript
End Property
Public Property Let Subscript(Value As Integer)
    Dim oField As IField
    Debug.Assert (m_Props.Subscript = 0)
    If m_Props.Subscript = 0 Then
        ' Subscript has not been used yet, so we may use it now
        If Value < 1 Or Value > m_Props.Value.Count Then
            Debug.Assert False
        Else
            m_Props.Subscript = Value
            ' Get required subscript field
            Set oField = m_Props.Value.Field(m_Props.Subscript)
            ' Now remove all data occurrences except for our subscript
            m_Props.Value.Clear
            m_Props.Value.Add oField
        End If
    End If
End Property
Public Function Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Select Case (eInterfaceType)
    Case FieldInterfaceIField:
        Dim oField As IField
        Set oField = Me
        Set Interface = oField
    Case Else:
        Set Interface = Me
    End Select
End Function

'----------------------------------------------------------------------------
' IField interface
'----------------------------------------------------------------------------

Public Property Get IField_FieldType() As enFieldType
    IField_FieldType = FieldTypeGroup
End Property
Public Property Let IField_Id(nId As Long)
    m_Props.nId = nId
End Property
Public Property Get IField_Id() As Long
    IField_Id = m_Props.nId
End Property
Private Property Let IField_IsEmpty(ByVal RHS As Boolean)
    IsEmpty = RHS
End Property

Private Property Get IField_IsEmpty() As Boolean
    IField_IsEmpty = IsEmpty
End Property
Public Property Let IField_SeqNo(lSeqNo As Long)
    m_Props.SeqNo = lSeqNo
End Property
Public Property Get IField_SeqNo() As Long
    IField_SeqNo = m_Props.SeqNo
End Property

Public Property Get IField_DisplayValue() As String
    Dim i As Integer
    Dim oField As IField
    If Subscript = 0 Then
        IField_DisplayValue = "Group of " & m_Props.Value.Count & " fields:"
    Else
        IField_DisplayValue = "Group, subscripted by " & m_Props.Subscript & ":"
    End If
    For i = 1 To m_Props.Value.Count
        Set oField = m_Props.Value.Field(i)
        If i > 1 Then
            IField_DisplayValue = IField_DisplayValue & ","
        End If
        IField_DisplayValue = IField_DisplayValue & " " & oField.DisplayValue
        If i > 3 Then
            IField_DisplayValue = IField_DisplayValue & ", ..."
            Exit For
        End If
    Next i
End Property
Public Property Get IField_SqlLiteral() As String
    Dim i As Integer
    Dim oField As IField
    For i = 1 To m_Props.Value.Count
        Set oField = m_Props.Value.Field(i)
        If i > 1 Then
            IField_SqlLiteral = IField_SqlLiteral & ", "
        End If
        IField_SqlLiteral = IField_SqlLiteral & oField.SqlLiteral
    Next i
End Property
Public Property Let IField_ValueAsVariant(Value As Variant)
    ' Null value is held as empty string
    Set m_Props.Value = Value
End Property
Public Property Get IField_ValueAsVariant() As Variant
    Set IField_ValueAsVariant = m_Props.Value
End Property
Public Function IField_CreateNew() As IField
    ' A faster way of creating an object
    Set IField_CreateNew = New CFieldGroup
End Function
Public Function IField_Duplicate() As IField
    Dim oField As New CFieldGroup
    oField.GroupProps = m_Props
    Set IField_Duplicate = oField
End Function
Public Function IField_Matches(nComparator As enComparator, vtValue As Variant) As Integer
    Dim obj As CRow
    Set obj = vtValue
    If obj Is Nothing Then
        ' Not a CRow
        IField_Matches = False
    Else
        IField_Matches = GroupMatches(nComparator, obj)
    End If
End Function
Public Function IField_Interface(Optional eInterfaceType As enFieldInterfaceType) As IField
    Set IField_Interface = Interface(eInterfaceType)
End Function


