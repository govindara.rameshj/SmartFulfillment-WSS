VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBoSortKeys"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IBoSortKeys
'
' $Archive: /Projects/OasysV2/VB/OasysDbInterfaceCom/CBoSortKeys.cls $
' $Revision: 2 $
' $Date: 12/02/03 15:10 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' To sort a collection of BOs, we need as list of sort keys and for
' each key and indicator of ascending/descending.
'----------------------------------------------------------------------------
'Public Type typeSortKey
'    nFieldId        As Long
'    bIsDescending   As Boolean
'End Type
Private m_colKeys   As Collection   ' each As typeSortKey

Private Sub Class_Initialize()
    Set m_colKeys = New Collection
End Sub
Private Sub Class_Terminate()
    Set m_colKeys = Nothing
End Sub
Public Function Clear()
    Set m_colKeys = Nothing
End Function

Public Function Add(nFieldId As Long, Optional bIsDescending As Boolean)
    Dim tKey As typeSortKey
    Debug.Assert Not (nFieldId < 1)
    tKey.nFieldId = nFieldId
    tKey.bIsDescending = bIsDescending
    Call m_colKeys.Add(tKey)
End Function
Public Property Get Count() As Long
    Count = m_colKeys.Count
End Property
Private Property Get Field(nInstance As Integer) As typeSortKey
    Dim tKey As typeSortKey
    Debug.Assert Not nInstance > m_colKeys.Count
    If nInstance > m_colKeys.Count Then
        Debug.Assert False
        Field = tKey
    Else
        Field = m_colKeys.Item(nInstance)
    End If
End Property
Public Property Get FieldID(nInstance As Integer) As Long
    FieldID = Field(nInstance).nFieldId
End Property
Public Property Get IsDescending(nInstance As Integer) As Boolean
    IsDescending = Field(nInstance).bIsDescending
End Property
Friend Function Duplicate() As CBoSortKeys
    Dim tField  As typeSortKey
    Dim i       As Integer
    Set Duplicate = New CBoSortKeys
    For i = 1 To m_colKeys.Count
        tField = m_colKeys.Item(i)
        Duplicate.Add tField.nFieldId, tField.bIsDescending
    Next
End Function

'----------------------------------------------------------------------------
' IBoSortKeys Implementation
'----------------------------------------------------------------------------
Public Sub IBoSortKeys_Clear()
    Clear
End Sub

Public Sub IBoSortKeys_Add(nFieldId As Long, Optional bIsDescending As Boolean)
    Add nFieldId, bIsDescending
End Sub

Public Property Get IBoSortKeys_Count() As Long
    IBoSortKeys_Count = m_colKeys.Count
End Property

Private Property Get IBoSortKeys_Field(nInstance As Integer) As typeSortKey
    IBoSortKeys_Field = Field(nInstance)
End Property

Public Property Get IBoSortKeys_FieldID(nInstance As Integer) As Long
    IBoSortKeys_FieldID = FieldID(nInstance)
End Property

Public Property Get IBoSortKeys_IsDescending(nInstance As Integer) As Boolean
    IBoSortKeys_IsDescending = IsDescending(nInstance)
End Property

Friend Function IBoSortKeys_Duplicate() As IBoSortKeys
    Set IBoSortKeys_Duplicate = Duplicate
End Function

