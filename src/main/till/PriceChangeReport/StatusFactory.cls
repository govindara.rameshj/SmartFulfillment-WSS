VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StatusFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const m_PriceChangeReportMaxRecordsParameterID As Long = 980981
Private m_InitialisedUsePriceChangeReportMaxRecordsImplementation As Boolean
Private m_UsePriceChangeReportMaxRecordsImplementation As Boolean

Friend Property Get UsePriceChangeReportMaxRecordsImplementation() As Boolean
    
    UsePriceChangeReportMaxRecordsImplementation = m_UsePriceChangeReportMaxRecordsImplementation
End Property

Public Function FactoryGetPriceChangeReportMaxRecords() As IStatus
    
    If UsePriceChangeReportMaxRecordsImplementation Then
        'using implementation with 891 fix
        Set FactoryGetPriceChangeReportMaxRecords = New StatusNew
    Else
        'using live implementation
        Set FactoryGetPriceChangeReportMaxRecords = New StatusExisting
    End If
End Function

Private Sub Class_Initialize()

    GetUsePriceChangeReportMaxRecordsValue
End Sub

Friend Sub GetUsePriceChangeReportMaxRecordsValue()

    If Not m_InitialisedUsePriceChangeReportMaxRecordsImplementation Then
On Error Resume Next
        m_UsePriceChangeReportMaxRecordsImplementation = goSession.GetParameter(m_PriceChangeReportMaxRecordsParameterID)
        m_InitialisedUsePriceChangeReportMaxRecordsImplementation = True
On Error GoTo 0
    End If
End Sub



