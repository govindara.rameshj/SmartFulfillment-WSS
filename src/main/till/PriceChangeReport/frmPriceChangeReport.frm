VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmPriceChangeReport 
   Caption         =   "Outstanding Price Changes Report"
   ClientHeight    =   6135
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13200
   Icon            =   "frmPriceChangeReport.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   6135
   ScaleWidth      =   13200
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   4680
      TabIndex        =   17
      Top             =   2520
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9 - Print"
      Height          =   435
      Left            =   3900
      TabIndex        =   15
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   435
      Left            =   60
      TabIndex        =   14
      Top             =   4800
      Width           =   1155
   End
   Begin VB.Frame fraCriteria 
      Caption         =   "Select Report Criteria"
      Height          =   1515
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   11415
      Begin VB.Frame ctlPrintedLabels 
         Caption         =   "Printed Labels"
         Height          =   1215
         Left            =   6120
         TabIndex        =   18
         Top             =   180
         Width           =   2355
         Begin VB.OptionButton chkPrintedLabelsBoth 
            Caption         =   "Both"
            Height          =   195
            Left            =   240
            TabIndex        =   21
            Top             =   900
            Value           =   -1  'True
            Width           =   2055
         End
         Begin VB.OptionButton chkLabelsAlreadyPrinted 
            Caption         =   "Labels already printed"
            Height          =   315
            Left            =   240
            TabIndex        =   20
            Top             =   540
            Width           =   1875
         End
         Begin VB.OptionButton chkLabelsToBePrinted 
            Caption         =   "Labels to be printed"
            Height          =   255
            Left            =   240
            TabIndex        =   19
            Top             =   240
            Width           =   1875
         End
      End
      Begin ucEditDate.ucDateText ucdtDateFrom 
         Height          =   285
         Left            =   10200
         TabIndex        =   8
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DateFormat      =   "DD/MM/YY"
         Text            =   "08/12/06"
      End
      Begin LpADOLib.fpComboAdo cmbStatus 
         Height          =   315
         Left            =   780
         TabIndex        =   2
         Top             =   300
         Width           =   3315
         _Version        =   196608
         _ExtentX        =   5847
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   0
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         EnableClickEvent=   -1  'True
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         DataMemberList  =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmPriceChangeReport.frx":0A96
      End
      Begin LpADOLib.fpComboAdo cmbOrderBy 
         Height          =   315
         Left            =   9360
         TabIndex        =   10
         Top             =   600
         Width           =   1755
         _Version        =   196608
         _ExtentX        =   3096
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   0
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   0   'False
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         EnableClickEvent=   -1  'True
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         DataMemberList  =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmPriceChangeReport.frx":0D1D
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "F7 - Apply"
         Height          =   435
         Left            =   10080
         TabIndex        =   12
         Top             =   960
         Width           =   1035
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3 - Reset"
         Height          =   435
         Left            =   8760
         TabIndex        =   11
         Top             =   960
         Width           =   1155
      End
      Begin VB.Frame Frame2 
         Caption         =   "Price Change"
         Height          =   1215
         Left            =   4200
         TabIndex        =   3
         Top             =   180
         Width           =   1875
         Begin VB.OptionButton chkPriceIncrease 
            Caption         =   "Increases Only"
            Height          =   255
            Left            =   240
            TabIndex        =   4
            Top             =   240
            Width           =   1515
         End
         Begin VB.OptionButton chkPriceDecrease 
            Caption         =   "Decreases Only"
            Height          =   315
            Left            =   240
            TabIndex        =   5
            Top             =   540
            Width           =   1515
         End
         Begin VB.OptionButton chkAllPrices 
            Caption         =   "Both"
            Height          =   255
            Left            =   240
            TabIndex        =   6
            Top             =   900
            Value           =   -1  'True
            Width           =   1515
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Status"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   300
         Width           =   795
      End
      Begin VB.Label lblNoDayslbl 
         Caption         =   "Up To Date"
         Height          =   255
         Left            =   9240
         TabIndex        =   7
         Top             =   300
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Sort By"
         Height          =   255
         Left            =   8760
         TabIndex        =   9
         Top             =   660
         Width           =   615
      End
   End
   Begin FPSpreadADO.fpSpread sprdReport 
      Height          =   3075
      Left            =   60
      TabIndex        =   13
      Top             =   1620
      Width           =   13455
      _Version        =   458752
      _ExtentX        =   23733
      _ExtentY        =   5424
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   20
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmPriceChangeReport.frx":0FA4
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   16
      Top             =   5760
      Width           =   13200
      _ExtentX        =   23283
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmPriceChangeReport.frx":17A5
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16113
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "17:26"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPriceChangeReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Refunds Report/frmRefundReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*      TS  Start Date followed by one of the following =  >  <
'*                           e.g. (/P='TS=20030326,  or  (/P>'TS>20030429,
'*      DB  Dated - when called from Daily Banking
'*                           e.g. (/P='DB=20030326
'*      TD  End date         e.g. TD20030329
'*      WE  Week Ending      e.g. WE=20030329     (note date should be a Saturday)
'*      SB  Show Button      if parameter is present Cash & Other Totals CAN be revealed
'*                           if parameter is absent  Cash & Other Totals cannot be revealed
'*      DC  Destination Code DCP Printer or DCS Preview
'*      CF  Called From      If CFC then the form is called in
'*                           in a automatic (Close) fashion.
'*
'**********************************************************************************************
'* SAMPLE (/P='DB=20031020,SB,STO,DCS,GRD,DL0' /u='044' -c='ffff')
'</CAMH>***************************************************************************************
Option Explicit

Const COL_DESC As Long = 1
Const COL_EFF_DATE As Long = 2
Const COL_OLD_PRICE As Long = 3
Const COL_SKU As Long = 4
Const COL_STATUS As Long = 5
Const COL_NEW_PRICE As Long = 6
Const COL_SIGN As Long = 7
Const COL_SOH As Long = 8
Const COL_MARKDOWN As Long = 9
Const COL_PLANGRAM As Long = 10
Const COL_NO_LABELS As Long = 11
Const COL_ACT_BY As Long = 12
Const COL_SHELF_LABEL As Long = 13
Const COL_MANAGER As Long = 14
Const COL_DAYS As Long = 15
Const COL_SUPPLIER As Long = 16
Const COL_SORTDATE As Long = 17
Const COL_OTHER_PLANS As Long = 18
Const COL_PRIME_PLAN As Long = 19
Const COL_SORT_LOC As Long = 20

Const STATUS_PRICE_CHANGE_ACTION_REQUIRED As Long = 1
Const STATUS_OVERDUE As Long = 2
Const STATUS_NON_STANDART_PRODUCTS As Long = 3

Const SORTBY_SKU As Long = 1
Const SORTBY_SUPPLIER As Long = 2
Const SORTBY_DAYS As Long = 3
Const SORTBY_PLANGRAM As Long = 4

Private mdteSystemDate  As Date

Private mblnFromNightlyClose As Boolean
Private mstrDestCode         As Boolean

Private Sub chkAllPrices_Click()
    
    cmdPrint.Visible = False

End Sub

Private Sub chkAllPrices_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub chkPriceDecrease_Click()
    
    cmdPrint.Visible = False

End Sub

Private Sub chkPriceIncrease_Click()
    
    cmdPrint.Visible = False

End Sub

Private Sub chkPriceIncrease_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub chkPriceDecrease_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub cmbOrderBy_Click()

Dim lngLineNo As Long
Dim vntPGLocs As Variant
Dim lngLocNo  As Integer
Dim strPlan   As String
Dim lngStart  As Long
Dim strSupp   As String
Dim strDate   As String

    sprdReport.ReDraw = False
    Screen.MousePointer = vbHourglass
    For lngLineNo = 1 To sprdReport.MaxRows Step 1
        Call sprdReport.RemoveCellSpan(COL_DESC, lngLineNo)
        Call sprdReport.RemoveCellSpan(COL_NO_LABELS, lngLineNo)
    Next lngLineNo
    'Group all info lines together
    sprdReport.SortKey(1) = COL_PRIME_PLAN
    sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
    'delete info lines
    lngLineNo = sprdReport.SearchCol(COL_PRIME_PLAN, 1, sprdReport.MaxRows, "", SearchFlagsGreaterOrEqual)
    If lngLineNo > 0 Then
        Call sprdReport.DeleteRows(lngLineNo, (sprdReport.MaxRows - lngLineNo) + 1)
        sprdReport.MaxRows = lngLineNo - 1
    End If
    'sort by selected criteria (SKU/Plangram
    If (cmbOrderBy.ItemData(cmbOrderBy.ListIndex) = SORTBY_SKU) Or _
            (cmbOrderBy.ItemData(cmbOrderBy.ListIndex) = SORTBY_SUPPLIER) Or _
            (cmbOrderBy.ItemData(cmbOrderBy.ListIndex) = SORTBY_DAYS) Then
        Select Case (cmbOrderBy.ItemData(cmbOrderBy.ListIndex))
        Case (SORTBY_SKU):
                sprdReport.SortKey(1) = COL_SKU
                sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
                sprdReport.SortKey(2) = COL_SORTDATE
                sprdReport.SortKeyOrder(2) = SortKeyOrderAscending
            Case (SORTBY_SUPPLIER):
                sprdReport.SortKey(1) = COL_SUPPLIER
                sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
                sprdReport.SortKey(2) = COL_SKU
                sprdReport.SortKeyOrder(2) = SortKeyOrderAscending
                sprdReport.SortKey(3) = COL_SORTDATE
                sprdReport.SortKeyOrder(3) = SortKeyOrderAscending
            Case (SORTBY_DAYS):
                sprdReport.SortKey(1) = COL_SORTDATE
                sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
                sprdReport.SortKey(2) = COL_SKU
                sprdReport.SortKeyOrder(2) = SortKeyOrderAscending
        End Select
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdReport.MaxRows To 1 Step -1
            sprdReport.Row = lngLineNo
            sprdReport.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdReport.Text, "##")
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdReport.MaxRows = sprdReport.MaxRows + 1
                        Call sprdReport.InsertRows(sprdReport.Row + 1, 1)
                        sprdReport.Row = sprdReport.Row + 1
                        sprdReport.Col = COL_PLANGRAM
                        sprdReport.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        If (lngStart = -1) Then lngStart = sprdReport.Row
                    Else
                        sprdReport.Col = COL_PLANGRAM
                        sprdReport.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdReport.AddCellSpan(COL_DESC, lngStart, COL_PLANGRAM - 1, sprdReport.Row - lngStart + 1)
                Call sprdReport.AddCellSpan(COL_NO_LABELS, lngStart, COL_SUPPLIER - COL_PLANGRAM, sprdReport.Row - lngStart + 1)
                sprdReport.Col = COL_DESC
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
                sprdReport.Col = COL_NO_LABELS
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
            End If

        Next lngLineNo
    End If
    
    If (cmbOrderBy.ItemData(cmbOrderBy.ListIndex) = SORTBY_PLANGRAM) Then
        sprdReport.SortKey(1) = COL_SKU
        sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdReport.MaxRows To 1 Step -1
            sprdReport.Row = lngLineNo
            sprdReport.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdReport.Text, "##")
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdReport.MaxRows = sprdReport.MaxRows + 1
                        Call sprdReport.InsertRows(sprdReport.Row + 1, 1)
                        sprdReport.Row = sprdReport.Row + 1
                        Call sprdReport.CopyRowRange(lngLineNo, lngLineNo, sprdReport.Row)
                        sprdReport.Col = COL_PRIME_PLAN
                        sprdReport.Text = ""
                    End If
                    sprdReport.Col = COL_SORT_LOC
                    sprdReport.Text = vntPGLocs(lngLocNo)
                    sprdReport.Col = COL_PLANGRAM
                    sprdReport.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                End If
            Next lngLocNo
        Next lngLineNo
        'Sort by Plan Gram Locations
        sprdReport.SortKey(1) = COL_SORT_LOC
        sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdReport.MaxRows To 1 Step -1
            sprdReport.Row = lngLineNo
            sprdReport.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdReport.Text, "##")
            sprdReport.Col = COL_SORT_LOC
            strPlan = sprdReport.Text
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (strPlan <> vntPGLocs(lngLocNo)) Then
                        sprdReport.MaxRows = sprdReport.MaxRows + 1
                        Call sprdReport.InsertRows(sprdReport.Row + 1, 1)
                        sprdReport.Row = sprdReport.Row + 1
                        sprdReport.Col = COL_PLANGRAM
                        sprdReport.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        If (lngStart = -1) Then lngStart = sprdReport.Row
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdReport.AddCellSpan(COL_DESC, lngStart, COL_PLANGRAM - 1, sprdReport.Row - lngStart + 1)
                Call sprdReport.AddCellSpan(COL_NO_LABELS, lngStart, COL_SUPPLIER - COL_PLANGRAM, sprdReport.Row - lngStart + 1)
                sprdReport.Col = COL_DESC
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
                sprdReport.Col = COL_NO_LABELS
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
            End If
        Next lngLineNo
    End If
    sprdReport.ColWidth(COL_PLANGRAM) = sprdReport.MaxTextColWidth(COL_PLANGRAM) + 1
    Screen.MousePointer = vbNormal
    sprdReport.ReDraw = True
    
End Sub

Private Sub cmbStatus_Click()

Dim lngStatus   As Long
    
    cmdPrint.Visible = False
    lblNoDayslbl.Visible = False
    ucdtDateFrom.Visible = False
    lngStatus = cmbStatus.ItemData(cmbStatus.ListIndex)
    sprdReport.MaxRows = 0
    sprdReport.Col = COL_DAYS
    sprdReport.ColHidden = True
    chkLabelsAlreadyPrinted.Enabled = True
    chkPrintedLabelsBoth.Enabled = True
    chkPrintedLabelsBoth.Value = True
    
    Select Case (lngStatus)
        Case (STATUS_PRICE_CHANGE_ACTION_REQUIRED): lblNoDayslbl.Visible = True
                                ucdtDateFrom.Visible = True
                                ucdtDateFrom.Text = DisplayDate(DateAdd("d", 7, mdteSystemDate), False)
                                sprdReport.Col = COL_DAYS
                                sprdReport.ColHidden = False
        Case (STATUS_OVERDUE):
                                chkLabelsAlreadyPrinted.Enabled = False
                                chkPrintedLabelsBoth.Enabled = False
                                chkLabelsToBePrinted.Value = True
                                
    End Select

End Sub

Private Sub cmbStatus_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)

End Sub

Private Sub cmdApply_Click()

Dim cPriceBO    As cStock_Wickes.cPriceChange
Dim oItemBO     As cStock_Wickes.cInventory
Dim colPrices   As Collection
Dim maxEffectiveDates As Dictionary
Dim lngRowNo    As Long
Dim blnGoodSKU  As Boolean
Dim lngStatus   As Long
Dim effectiveDate As String

    On Error GoTo Error_Display
    
    Set cPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
    
    sprdReport.MaxRows = 0
    lngStatus = cmbStatus.ItemData(cmbStatus.ListIndex)
    
    Screen.MousePointer = vbHourglass
        ucpbProgress.Visible = True
        ucpbProgress.Caption1 = "Accessing List"
    
    If (lngStatus = STATUS_OVERDUE) Then
        Dim smallLabelsPriceChanges As Collection
        Dim mediumLabelsPriceChanges As Collection
        Dim largeLabelsPriceChanges As Collection
        
        Call cPriceBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_EffectiveDate, mdteSystemDate)
        Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_SmallLabel, True)
        Set smallLabelsPriceChanges = cPriceBO.LoadMatches
        
        Set cPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
        
        Call cPriceBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_EffectiveDate, mdteSystemDate)
        Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_MediumLabel, True)
        Set mediumLabelsPriceChanges = cPriceBO.LoadMatches
        
        Set cPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
        
        Call cPriceBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_EffectiveDate, mdteSystemDate)
        Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_LargeLabel, True)
        Set largeLabelsPriceChanges = cPriceBO.LoadMatches
        
        Set colPrices = smallLabelsPriceChanges
        
        Dim varTemp As cStock_Wickes.cPriceChange
        
        For Each varTemp In mediumLabelsPriceChanges
            If (Not CheckIfCollectionContains(colPrices, varTemp)) Then
                colPrices.Add varTemp
            End If
        Next varTemp
        
        For Each varTemp In largeLabelsPriceChanges
            If (Not CheckIfCollectionContains(colPrices, varTemp)) Then
                colPrices.Add varTemp
            End If
        Next varTemp
        
    Else
        Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_ChangeStatus, "U")
        
        If (lngStatus = STATUS_PRICE_CHANGE_ACTION_REQUIRED) Then
            Call cPriceBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_EffectiveDate, GetDate(ucdtDateFrom.Text))
        End If
        
        Set colPrices = cPriceBO.LoadMatches
    End If
    
    If (colPrices.Count > 0) Then ucpbProgress.Max = colPrices.Count
    ucpbProgress.Caption1 = "Displaying List"
    
    Set maxEffectiveDates = GetEffectivePriceDictionary(Format(mdteSystemDate, "yyyy-mm-dd"))
    
    For lngRowNo = 1 To colPrices.Count Step 1
        Set cPriceBO = colPrices(lngRowNo)
        ucpbProgress.Value = lngRowNo
        
        If (CheckLabelsPrintedCriteria(cPriceBO)) Then
            blnGoodSKU = True
            Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
            Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, cPriceBO.PartCode)
            Call oItemBO.AddLoadField(FID_INVENTORY_Description)
            Call oItemBO.AddLoadField(FID_INVENTORY_NormalSellPrice)
            Call oItemBO.AddLoadField(FID_INVENTORY_SupplierNo)
            Call oItemBO.AddLoadField(FID_INVENTORY_QuantityAtHand)
            Call oItemBO.AddLoadField(FID_INVENTORY_NonStockItem)
            Call oItemBO.AddLoadField(FID_INVENTORY_AutoApplyPriceChanges)
            Call oItemBO.AddLoadField(FID_INVENTORY_MarkDownQuantity)
            Call oItemBO.AddLoadField(FID_INVENTORY_ItemObsolete)
            Call oItemBO.AddLoadField(FID_INVENTORY_NoOfSmallLabels)
            Call oItemBO.AddLoadField(FID_INVENTORY_NoOfMediumLabels)
            Call oItemBO.AddLoadField(FID_INVENTORY_NoOfLargeLabels)
            Call oItemBO.AddLoadField(FID_INVENTORY_PriorSellPrice)
            
            Call oItemBO.LoadMatches
            
            If (oItemBO.PartCode <> "") Then
                If (CheckPriceDifferenceCriteria(oItemBO, cPriceBO)) Then
                    If (CheckDropDownStatusCriteria(oItemBO)) Then
                        If (lngStatus = STATUS_OVERDUE) Then
                            If (maxEffectiveDates.Exists(cPriceBO.PartCode) And maxEffectiveDates(cPriceBO.PartCode) = cPriceBO.effectiveDate) Then
                                Call DisplayPriceChanges(oItemBO, cPriceBO)
                            End If
                        Else
                            If ((cPriceBO.effectiveDate > mdteSystemDate) _
                                Or (maxEffectiveDates.Exists(cPriceBO.PartCode) And maxEffectiveDates(cPriceBO.PartCode) = cPriceBO.effectiveDate)) Then
                                    Call DisplayPriceChanges(oItemBO, cPriceBO)
                            End If
                        End If
                    End If
                End If
            End If
        End If
    Next lngRowNo
    
    sprdReport.ColWidth(COL_DESC) = sprdReport.MaxTextColWidth(COL_DESC)
    cmdPrint.Visible = True
    ucpbProgress.Visible = False
    Call cmbOrderBy_Click
    Screen.MousePointer = vbNormal
    
    If (sprdReport.MaxRows = 0) Then
        Call MsgBoxEx("No Matches found for selected criteria", vbInformation, "No Data Found", , , , , RGBMSGBox_PromptColour)
    Else
        Call sprdReport.SetFocus
    End If
    
    Exit Sub
    
Error_Display:

    Call MsgBoxEx("An error has occurred whilst display data" & vbNewLine & "Err No:" & Err.Number & " - " & Err.Description, vbInformation, "Error detected")
    Call Err.Clear
    ucpbProgress.Visible = False

End Sub

Private Function CheckIfCollectionContains(colPrices As Collection, cPriceBO As cStock_Wickes.cPriceChange) As Boolean
    CheckIfCollectionContains = False
    
    Dim varTemp As cStock_Wickes.cPriceChange
    
    For Each varTemp In colPrices
        If (varTemp.ShelfLabelPrinted = cPriceBO.ShelfLabelPrinted And varTemp.SmallLabel = cPriceBO.SmallLabel _
        And varTemp.MediumLabel = cPriceBO.MediumLabel And varTemp.LargeLabel = cPriceBO.LargeLabel _
        And varTemp.effectiveDate = cPriceBO.effectiveDate And varTemp.PartCode = cPriceBO.PartCode _
        And varTemp.ChangeStatus = cPriceBO.ChangeStatus And varTemp.EventNumber = cPriceBO.EventNumber _
        And varTemp.NewPrice = cPriceBO.NewPrice) Then
            CheckIfCollectionContains = True
        End If
    Next varTemp

End Function

Private Function GetEffectivePriceDictionary(strDate As String) As Dictionary
    Dim oConnection As Connection
    Dim objCommand As ADODB.Command
    Dim rsPrice As Recordset
    Dim dicPrice As New Dictionary
    
    Set oConnection = goDatabase.Connection
    Set objCommand = New ADODB.Command
    
    Dim status As String
    Dim lngStatus As String
    lngStatus = cmbStatus.ItemData(cmbStatus.ListIndex)
    
    If (lngStatus = STATUS_OVERDUE) Then
        status = ""
    Else
        status = "AND PSTA = 'U'"
    End If
    
    With objCommand
        .ActiveConnection = oConnection
        .CommandType = adCmdText
        .CommandText = "SELECT SKUN, MAX(PDAT) AS MPDAT FROM dbo.PRCCHG WHERE PDAT <= '" & _
                        Format(strDate, "yyyy-mm-dd") & "' " & status & " GROUP BY SKUN"
        .Prepared = True
    End With
   
    Set rsPrice = objCommand.Execute()
   
    Do While Not rsPrice.EOF
        dicPrice.Add rsPrice(0).Value, rsPrice(1).Value
        rsPrice.MoveNext
    Loop

    Set GetEffectivePriceDictionary = dicPrice
    
End Function

Private Function CheckLabelsPrintedCriteria(cPriceBO As cStock_Wickes.cPriceChange)
    
    CheckLabelsPrintedCriteria = cPriceBO.ShelfLabelPrinted Or cPriceBO.SmallLabel Or cPriceBO.MediumLabel Or cPriceBO.LargeLabel
    
    If (chkLabelsAlreadyPrinted.Value = True) Then
        CheckLabelsPrintedCriteria = cPriceBO.ShelfLabelPrinted And Not (cPriceBO.SmallLabel Or cPriceBO.MediumLabel Or cPriceBO.LargeLabel)
    End If
    
    If (chkLabelsToBePrinted.Value = True) Then
        CheckLabelsPrintedCriteria = cPriceBO.SmallLabel Or cPriceBO.MediumLabel Or cPriceBO.LargeLabel
    End If
    
End Function

Private Function CheckPriceDifferenceCriteria(oItemBO As cStock_Wickes.cInventory, cPriceBO As cStock_Wickes.cPriceChange) As Boolean
    
    CheckPriceDifferenceCriteria = True
    
    If (chkPriceDecrease.Value = True) Then
        If (oItemBO.NormalSellPrice <= cPriceBO.NewPrice) Then CheckPriceDifferenceCriteria = False
    End If
                
    If (chkPriceIncrease.Value = True) Then
        If (oItemBO.NormalSellPrice >= cPriceBO.NewPrice) Then CheckPriceDifferenceCriteria = False
    End If

End Function

Private Function CheckDropDownStatusCriteria(oItemBO As cStock_Wickes.cInventory) As Boolean

Dim lngStatus   As Long

    CheckDropDownStatusCriteria = True
    
    lngStatus = cmbStatus.ItemData(cmbStatus.ListIndex)

    If (lngStatus = STATUS_PRICE_CHANGE_ACTION_REQUIRED) Then
        If ((oItemBO.ItemObsolete = True) Or (oItemBO.NonStockItem = True) Or (oItemBO.AutoApplyPriceChanges = True)) And (oItemBO.MarkDownQuantity + oItemBO.QuantityAtHand = 0) Then CheckDropDownStatusCriteria = False
    End If
                        
    If (lngStatus = STATUS_OVERDUE) Then
        If ((oItemBO.ItemObsolete = True) Or (oItemBO.NonStockItem = True) Or (oItemBO.AutoApplyPriceChanges = True)) And (oItemBO.MarkDownQuantity + oItemBO.QuantityAtHand = 0) Then CheckDropDownStatusCriteria = False
    End If
                                   
    If (lngStatus = STATUS_NON_STANDART_PRODUCTS) Then
        If (oItemBO.ItemObsolete = False) And (oItemBO.NonStockItem = False) And (oItemBO.AutoApplyPriceChanges = False) Then CheckDropDownStatusCriteria = False
        If (oItemBO.MarkDownQuantity + oItemBO.QuantityAtHand = 0) Then CheckDropDownStatusCriteria = False
    End If

End Function

Private Sub DisplayPriceChanges(oItemBO As cStock_Wickes.cInventory, cPriceBO As cStock_Wickes.cPriceChange)

Dim effectiveDate As String

    effectiveDate = Format(cPriceBO.effectiveDate, "DD/MM/YY")
    
    sprdReport.MaxRows = sprdReport.MaxRows + 1
    sprdReport.Row = sprdReport.MaxRows
    sprdReport.Col = COL_DESC
    sprdReport.Text = IIf(oItemBO.Description = "", "STKMAS Entry Missing", oItemBO.Description)
    sprdReport.Col = COL_EFF_DATE
    sprdReport.Text = effectiveDate
    sprdReport.Col = COL_OLD_PRICE
    If (cmbStatus.ItemData(cmbStatus.ListIndex) = STATUS_OVERDUE) Then
        sprdReport.Text = oItemBO.PriorSellPrice
    Else
        sprdReport.Text = oItemBO.NormalSellPrice
    End If
    sprdReport.Col = COL_SKU
    sprdReport.Text = cPriceBO.PartCode
    sprdReport.Col = COL_STATUS
    sprdReport.Text = cPriceBO.ChangeStatus
    sprdReport.Col = COL_NO_LABELS
    sprdReport.Text = oItemBO.NoOfSmallLabels & "/" & oItemBO.NoOfMediumLabels & "/" & oItemBO.NoOfLargeLabels
    sprdReport.Col = COL_NEW_PRICE
    sprdReport.Text = cPriceBO.NewPrice
    sprdReport.Col = COL_SOH
    sprdReport.Text = oItemBO.QuantityAtHand
    sprdReport.Col = COL_MARKDOWN
    sprdReport.Text = oItemBO.MarkDownQuantity
    sprdReport.Col = COL_DAYS
    sprdReport.Text = DateDiff("d", effectiveDate, mdteSystemDate)
    sprdReport.Col = COL_SUPPLIER
    sprdReport.Text = oItemBO.SupplierNo
    sprdReport.Col = COL_SORTDATE
    sprdReport.Text = Format(effectiveDate, "YYYYMMDD")
    
    Call DisplayPlanogram(oItemBO.PartCode)

End Sub

Private Sub cmdExit_Click()

    End

End Sub

Private Sub cmdReset_Click()

    chkAllPrices.Value = True
    ucdtDateFrom.Text = DisplayDate(DateAdd("d", 7, mdteSystemDate), False)
    sprdReport.MaxRows = 0
    cmdPrint.Visible = False
    Call cmbStatus.SetFocus
    
End Sub

Private Sub Form_Load()
    Dim oRetOpt As cRetailOptions
    Dim oSysDat As cSystemDates
    Dim StatusImplementationFactory As New StatusFactory
    Dim StatusImplementaion As IStatus
    Dim StatusDescriptions() As String
    Dim StatusIndexIds() As Long
    Dim nextStatus As Integer

    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Call sprdReport.SetOddEvenRowColor(RGB(224, 224, 224), vbBlack, vbWhite, vbBlack)
    Call InitialiseStatusBar(sbStatus)
    
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    
    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.IBo_AddLoadField(FID_SYSTEMDATES_NextOpenDate)
    Call oSysDat.LoadMatches
    
    mdteSystemDate = oSysDat.NextOpenDate
    Set oSysDat = Nothing
    
    Set StatusImplementaion = StatusImplementationFactory.FactoryGetPriceChangeReportMaxRecords
    With StatusImplementaion
        StatusDescriptions = .GetStatusDescriptionList()
        StatusIndexIds = .GetStatusIndexList()
    End With
        
    For nextStatus = 0 To UBound(StatusDescriptions)
        With cmbStatus
            Call .AddItem(StatusDescriptions(nextStatus))
            .ItemData(.NewIndex) = StatusIndexIds(nextStatus)
            If nextStatus = 0 Then
                .ListIndex = 0
            End If
        End With
    Next nextStatus
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 11650) Then
        Me.Width = 11650
        Exit Sub
    End If
    
    sprdReport.Width = Me.Width - sprdReport.Left * 4
    sprdReport.Height = Me.Height - sprdReport.Top - cmdPrint.Height - 720 - sbStatus.Height
    cmdPrint.Top = sprdReport.Top + sprdReport.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdPrint.Left = sprdReport.Left + sprdReport.Width - cmdPrint.Width
    ucpbProgress.Left = (Me.Width - ucpbProgress.Width) / 2
    ucpbProgress.Top = (Me.Height - ucpbProgress.Height - sbStatus.Height) / 2

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case vbKeyF3 ' if F3 then call 'Reset Search Criteria
                    If cmdReset.Visible = True Then cmdReset_Click
                Case (vbKeyF7):  'if F7 then call search
                    If cmdApply.Visible = True Then Call cmdApply_Click
                Case (vbKeyF9):  'if F9 then call print
                    If cmdPrint.Visible = True Then Call cmdPrint_click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub
    
Private Sub cmdPrint_click()

Dim strHeader   As String
Dim oSysOptBO   As cSystemOptions

    Set oSysOptBO = goSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreNumber)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreName)
    Call oSysOptBO.LoadMatches

    ' 02/05/03  KVB  remove Printed dd/mm/yy from strHeader
    strHeader = "Store No: " & oSysOptBO.StoreNumber & " " & _
                oSysOptBO.StoreName & "  -  Outstanding Price Changes Report "
    
    ' /c is Centre text      /fb1 is Font bold on
    sprdReport.PrintHeader = "/c/fb1" & strHeader
    sprdReport.PrintJobName = "Price Changes Report"
    sprdReport.PrintFooter = GetFooter
    sprdReport.PrintOrientation = PrintOrientationLandscape
    sprdReport.PrintRowHeaders = False
    sprdReport.PrintScalingMethod = PrintScalingMethodSmartPrint
    sprdReport.Refresh
    Call sprdReport.PrintSheet
            
End Sub

Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        
        'Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case TRAN_START_DATE, SELECTION_DATE_DB
                strSignFound = Left$(strTempDate, Len(strTempDate) - 8)
                               
            Case TRAN_WEEKENDING
                blnSetOpts = True
                        
            Case TRAN_END_DATE
                blnSetOpts = True
                
            Case DEST_CODE
                strTempDate = Mid$(strSection, 3)
                mstrDestCode = strTempDate
                blnSetOpts = True
            
            'Added v1.0.11
            Case CALLED_FROM
                If Mid(strSection, 3) = CF_CLOSE Then
                    mblnFromNightlyClose = True
                End If
            
        End Select
    
    Next lngSectNo
    
    If blnSetOpts = True Then Call cmdApply_Click

End Sub

Private Sub ucdtDateFrom_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyReturn) Then Call SendKeys(vbTab)
    If (KeyAscii = vbKeyEscape) Then Call SendKeys("+{tab}")

End Sub

Private Sub DisplayPlanogram(strSKU As String)

Dim oPlanGramBO As cStock_Wickes.cPlangram
Dim strPGLocs   As String
Dim strLocation As String
Dim colPGLocs   As Collection
Dim strFirstLoc As String
Dim lngFirstLoc As Long
Dim lngLocNo    As Long
        
    'TCH1230 - Added Plangram sequencing
    Set oPlanGramBO = goDatabase.CreateBusinessObject(CLASSID_PLANGRAM)
    Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_PartCode, strSKU)
    Set colPGLocs = oPlanGramBO.LoadMatches
    While (colPGLocs.Count > 0)
        strFirstLoc = "ZZZZZZZZZZZZZ"
        For lngLocNo = 1 To colPGLocs.Count Step 1
            Set oPlanGramBO = colPGLocs(lngLocNo)
            strLocation = String(10 - Len(Str(oPlanGramBO.PlanNumber)), "0") & oPlanGramBO.PlanNumber & "/" & String(10 - Len(oPlanGramBO.PlanSegment), "0") & oPlanGramBO.PlanSegment & "/" & String(10 - Len(oPlanGramBO.ShelfNumber), "0") & oPlanGramBO.ShelfNumber
            If (strFirstLoc > strLocation) Then
                lngFirstLoc = lngLocNo
                strFirstLoc = strLocation
            End If
        Next
        Set oPlanGramBO = colPGLocs(lngFirstLoc)
        strPGLocs = strPGLocs & strFirstLoc & "-" & oPlanGramBO.PlanName & "##"
        Set oPlanGramBO = Nothing
        Call colPGLocs.Remove(lngFirstLoc)
    Wend
    sprdReport.Col = COL_OTHER_PLANS
    sprdReport.Text = strPGLocs
    If (strPGLocs = "") Then sprdReport.Text = "N/A" 'flag with something to show as a valid line
    sprdReport.Col = COL_PRIME_PLAN
    sprdReport.Text = 1

End Sub

Private Function DecodeLocation(strLocation As String) As String

Dim strLoc  As String

    strLoc = "/" & strLocation
    While (InStr(strLoc, "/0") > 0)
        strLoc = Replace(strLoc, "/0", "/")
    Wend
    DecodeLocation = Mid$(strLoc, 2)

End Function



