VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "StatusExisting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Const STATUS_PRICE_CHANGE_ACTION_REQUIRED As Long = 1
Const STATUS_OVERDUE As Long = 2
Const STATUS_NON_STANDART_PRODUCTS As Long = 3

Implements IStatus

Private Function IStatus_GetStatusDescriptionList() As String()
    Dim StatusDescriptionList(0 To 2) As String
    
    StatusDescriptionList(0) = "Price Change Action Required"
    StatusDescriptionList(1) = "Overdue"
    StatusDescriptionList(2) = "Non Standard Products"
    
    IStatus_GetStatusDescriptionList = StatusDescriptionList
End Function
    
Private Function IStatus_GetStatusIndexList() As Long()
    Dim StatusIndexList(0 To 2) As Long
    
    StatusIndexList(0) = STATUS_PRICE_CHANGE_ACTION_REQUIRED
    StatusIndexList(1) = STATUS_OVERDUE
    StatusIndexList(2) = STATUS_NON_STANDART_PRODUCTS
    
    IStatus_GetStatusIndexList = StatusIndexList
End Function
