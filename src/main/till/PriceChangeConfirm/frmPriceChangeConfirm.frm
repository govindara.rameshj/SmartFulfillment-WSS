VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Begin VB.Form frmPriceChangeConfirm 
   Caption         =   "Price Change Audit Report"
   ClientHeight    =   7500
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13200
   Icon            =   "frmPriceChangeConfirm.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7500
   ScaleWidth      =   13200
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame fraCriteria 
      Caption         =   "Select Report Criteria"
      Height          =   1815
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   9255
      Begin VB.CommandButton btnReset 
         Caption         =   "Reset"
         Height          =   495
         Left            =   3960
         TabIndex        =   12
         Top             =   720
         Width           =   1330
      End
      Begin VB.CommandButton btnApply 
         Caption         =   "Apply"
         Height          =   495
         Left            =   2520
         TabIndex        =   11
         Top             =   720
         Width           =   1330
      End
      Begin VB.Frame fraPriceChanges 
         Caption         =   "Price Changes"
         Height          =   1455
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   2055
         Begin VB.OptionButton chkPriceBoth 
            Caption         =   "Both"
            Height          =   375
            Left            =   240
            TabIndex        =   13
            Top             =   980
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton chkPriceDecrease 
            Caption         =   "Decreases Only"
            Height          =   195
            Left            =   240
            TabIndex        =   10
            Top             =   720
            Width           =   1575
         End
         Begin VB.OptionButton chkPriceIncrease 
            Caption         =   "Increases Only"
            Height          =   255
            Left            =   240
            TabIndex        =   9
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.ComboBox cmbSortBy 
         Height          =   315
         ItemData        =   "frmPriceChangeConfirm.frx":0A96
         Left            =   6600
         List            =   "frmPriceChangeConfirm.frx":0AA0
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   810
         Width           =   2235
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Sorted By"
         Height          =   255
         Left            =   5640
         TabIndex        =   7
         Top             =   840
         Width           =   1035
      End
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   6600
      TabIndex        =   4
      Top             =   3060
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9 - Print"
      Height          =   435
      Left            =   3900
      TabIndex        =   2
      Top             =   6480
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   435
      Left            =   60
      TabIndex        =   1
      Top             =   6360
      Width           =   1155
   End
   Begin FPSpreadADO.fpSpread sprdReport 
      Height          =   4635
      Left            =   60
      TabIndex        =   0
      Top             =   2040
      Width           =   13455
      _Version        =   458752
      _ExtentX        =   23733
      _ExtentY        =   8176
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   13
      MaxRows         =   1
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmPriceChangeConfirm.frx":0AB4
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   7125
      Width           =   13200
      _ExtentX        =   23283
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmPriceChangeConfirm.frx":10B3
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16113
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "19:21"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPriceChangeConfirm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Refunds Report/frmRefundReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'*
'**********************************************************************************************
'* Summary of Parameters that can be passed to this program -
'*      TS  Start Date followed by one of the following =  >  <
'*                           e.g. (/P='TS=20030326,  or  (/P>'TS>20030429,
'*      DB  Dated - when called from Daily Banking
'*                           e.g. (/P='DB=20030326
'*      TD  End date         e.g. TD20030329
'*      WE  Week Ending      e.g. WE=20030329     (note date should be a Saturday)
'*      SB  Show Button      if parameter is present Cash & Other Totals CAN be revealed
'*                           if parameter is absent  Cash & Other Totals cannot be revealed
'*      DC  Destination Code DCP Printer or DCS Preview
'*      CF  Called From      If CFC then the form is called in
'*                           in a automatic (Close) fashion.
'*
'**********************************************************************************************
'* SAMPLE (/P='DB=20031020,SB,STO,DCS,GRD,DL0' /u='044' -c='ffff')
'</CAMH>***************************************************************************************
Option Explicit

Const COL_SKU As Long = 1
Const COL_DESC As Long = 2
Const COL_NEW_PRICE As Long = 3
Const COL_OLD_PRICE As Long = 4
Const COL_SOH As Long = 5
Const COL_HHT As Long = 6
Const COL_PLANGRAM As Long = 7
Const COL_NO_LABELS As Long = 8
Const COL_SHELF_LABEL As Long = 9
Const COL_MANAGER As Long = 10
Const COL_OTHER_PLANS As Long = 11
Const COL_PRIME_PLAN As Long = 12
Const COL_SORT_LOC  As Long = 13

Private mdteSystemDate  As Date
Private mblnAutoApply   As Boolean

Private mblnFromNightlyClose As Boolean
Private mstrDestCode         As Boolean

Private Sub LoadChanges()

Dim cPriceBO    As cStock_Wickes.cPriceChange
Dim oItemBO     As cStock_Wickes.cInventory
Dim colPrices   As Collection
Dim colItems    As Collection
Dim lngRowNo    As Long
Dim lngID       As Long
Dim vntDate     As Variant

Dim oRow        As CRowSelector

    Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    
    Set colItems = GetAllProductsWithAppliedPriceChange()
    
    sprdReport.MaxRows = 0
    ucpbProgress.Caption1 = "Displaying List"
    
    If (colItems.Count > 0) Then ucpbProgress.Max = colItems.Count
    
    For lngRowNo = 1 To colItems.Count Step 1
    
        ucpbProgress.Value = lngRowNo
        
        Set oItemBO = colItems(CLng(lngRowNo))
        
        Dim blnGoodPriceChange As Boolean
        blnGoodPriceChange = True
        
        If (chkPriceIncrease.Value = True) Then
            blnGoodPriceChange = oItemBO.NormalSellPrice > oItemBO.PriorSellPrice
        End If
        
        If (chkPriceDecrease.Value = True) Then
            blnGoodPriceChange = oItemBO.NormalSellPrice < oItemBO.PriorSellPrice
        End If
        
        If (blnGoodPriceChange) Then
        
            Set oRow = goSession.Root.CreateUtilityObject("CRowSelector")
            Set cPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
            
            cPriceBO.PartCode = oItemBO.PartCode
            cPriceBO.ChangeStatus = "U"
            cPriceBO.AutoAppliedDate = oItemBO.PriceEffectiveDate
            cPriceBO.NewPrice = oItemBO.NormalSellPrice
            Call oRow.AddSelection(CMP_EQUAL, cPriceBO.GetField(FID_PRICECHANGE_PartCode))
            Call oRow.AddSelection(CMP_NOTEQUAL, cPriceBO.GetField(FID_PRICECHANGE_ChangeStatus))
            Call oRow.AddSelection(CMP_EQUAL, cPriceBO.GetField(FID_PRICECHANGE_AutoAppliedDate))
            Call oRow.AddSelection(CMP_EQUAL, cPriceBO.GetField(FID_PRICECHANGE_NewPrice))
            
            vntDate = goDatabase.GetAggregateValue(AGG_MAX, cPriceBO.GetField(FID_PRICECHANGE_EffectiveDate), oRow)
    
            If (IsDate(vntDate) = True) Then
                Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_PartCode, oItemBO.PartCode)
                Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_EffectiveDate, CDate(vntDate))
                Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_NewPrice, oItemBO.NormalSellPrice)
                Call cPriceBO.AddLoadFilter(CMP_NOTEQUAL, FID_PRICECHANGE_ChangeStatus, "U")
                
                Set colPrices = cPriceBO.LoadMatches
                
                If (colPrices.Count > 1) Then
                    Set cPriceBO = GetLastPriceChangeFromByEventCode(colPrices)
                Else
                    Set cPriceBO = colPrices(CLng(1))
                End If
        
                sprdReport.MaxRows = sprdReport.MaxRows + 1
                sprdReport.Row = sprdReport.MaxRows
                sprdReport.Col = COL_SKU
                sprdReport.Text = oItemBO.PartCode
                sprdReport.Col = COL_DESC
                sprdReport.Text = IIf(oItemBO.Description = "", "STKMAS Entry Missing", oItemBO.Description)
                sprdReport.Col = COL_NEW_PRICE
                sprdReport.Text = oItemBO.NormalSellPrice
                sprdReport.Col = COL_OLD_PRICE
                sprdReport.Text = oItemBO.PriorSellPrice
                sprdReport.Col = COL_SOH
                sprdReport.Text = oItemBO.QuantityAtHand
                sprdReport.Col = COL_NO_LABELS
                sprdReport.Text = oItemBO.NoOfSmallLabels & "/" & oItemBO.NoOfMediumLabels & "/" & oItemBO.NoOfLargeLabels
                sprdReport.Col = COL_SHELF_LABEL
                sprdReport.Text = Abs(cPriceBO.ShelfLabelPrinted)
                sprdReport.Col = COL_HHT
                sprdReport.Text = IIf(cPriceBO.ChangeStatus = "H", "YES", "NO")
                
                Call DisplayPlanogram(oItemBO.PartCode)
        
            End If
        End If
    Next lngRowNo
    
    sprdReport.ColWidth(COL_DESC) = sprdReport.MaxTextColWidth(COL_DESC) + 1
    ucpbProgress.Visible = False
    Screen.MousePointer = vbNormal
    
    If sprdReport.MaxRows <> 0 Then
        sprdReport.SortKey(1) = COL_SKU
        sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
    End If

End Sub

Private Function GetAllProductsWithAppliedPriceChange() As Collection

Dim colManuallyAppliedPrices   As Collection
Dim oSysDat As cSystemDates

    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.LoadMatches
    
    Set GetAllProductsWithAppliedPriceChange = GetProductsByPriceChangeAppliedDate(mdteSystemDate)
    Set colManuallyAppliedPrices = GetProductsByPriceChangeAppliedDate(oSysDat.NextOpenDate)
    
    Dim varTemp As cStock_Wickes.cInventory
            
    For Each varTemp In colManuallyAppliedPrices
        GetAllProductsWithAppliedPriceChange.Add varTemp
    Next varTemp
    
    Set GetAllProductsWithAppliedPriceChange = GetRefinedCollectionOfNonStandartProductsWithZeroSOH(GetAllProductsWithAppliedPriceChange)

End Function

Private Function GetRefinedCollectionOfNonStandartProductsWithZeroSOH(colItems As Collection) As Collection

    Set GetRefinedCollectionOfNonStandartProductsWithZeroSOH = New Collection
    
    Dim varTemp As cStock_Wickes.cInventory
    
    For Each varTemp In colItems
        If Not (((varTemp.ItemObsolete = True) Or (varTemp.NonStockItem = True) Or (varTemp.AutoApplyPriceChanges = True)) And (varTemp.MarkDownQuantity + varTemp.QuantityAtHand = 0)) Then
            GetRefinedCollectionOfNonStandartProductsWithZeroSOH.Add varTemp
        End If
    Next varTemp
    
End Function

Private Function GetProductsByPriceChangeAppliedDate(searchDate As Date) As Collection

Dim oItemBO     As cStock_Wickes.cInventory

    Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PriceEffectiveDate, searchDate)
    Call oItemBO.AddLoadField(FID_INVENTORY_PartCode)
    Call oItemBO.AddLoadField(FID_INVENTORY_Description)
    Call oItemBO.AddLoadField(FID_INVENTORY_NormalSellPrice)
    Call oItemBO.AddLoadField(FID_INVENTORY_PriorSellPrice)
    Call oItemBO.AddLoadField(FID_INVENTORY_SupplierNo)
    Call oItemBO.AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItemBO.AddLoadField(FID_INVENTORY_NonStockItem)
    Call oItemBO.AddLoadField(FID_INVENTORY_NoOfSmallLabels)
    Call oItemBO.AddLoadField(FID_INVENTORY_NoOfMediumLabels)
    Call oItemBO.AddLoadField(FID_INVENTORY_NoOfLargeLabels)
    Call oItemBO.AddLoadField(FID_INVENTORY_ItemObsolete)
    Call oItemBO.AddLoadField(FID_INVENTORY_AutoApplyPriceChanges)
    Call oItemBO.AddLoadField(FID_INVENTORY_NonStockItem)
    Call oItemBO.AddLoadField(FID_INVENTORY_MarkDownQuantity)
    
    ucpbProgress.Visible = True
    Screen.MousePointer = vbHourglass
    ucpbProgress.Caption1 = "Loading List"
    Set GetProductsByPriceChangeAppliedDate = oItemBO.LoadMatches
End Function

Private Function GetLastPriceChangeFromByEventCode(colPrices As Collection) As cStock_Wickes.cPriceChange

Dim cPriceBO    As cStock_Wickes.cPriceChange
Dim lngRowNo    As Long
    Set cPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
    
    Set GetLastPriceChangeFromByEventCode = colPrices(CLng(1))
    
    For lngRowNo = 2 To colPrices.Count Step 1
        Set cPriceBO = colPrices(CLng(lngRowNo))
        If Not (cPriceBO.ChangeStatus = "S" And GetLastPriceChangeFromByEventCode.ChangeStatus <> "S") Then
            If (CLng(GetLastPriceChangeFromByEventCode.EventNumber) < CLng(cPriceBO.EventNumber)) Then
                Set GetLastPriceChangeFromByEventCode = cPriceBO
            End If
        End If
    Next lngRowNo
End Function

Private Sub btnApply_Click()
    Call LoadChanges
End Sub

Private Sub btnReset_Click()
    chkPriceBoth.Value = True
End Sub

Private Sub cmbSortBy_Click()

Dim lngLineNo As Long
Dim vntPGLocs As Variant
Dim lngLocNo  As Integer
Dim strPlan   As String
Dim lngStart  As Long

    sprdReport.Redraw = False
    Screen.MousePointer = vbHourglass
    For lngLineNo = 1 To sprdReport.MaxRows Step 1
        Call sprdReport.RemoveCellSpan(COL_SKU, lngLineNo)
        Call sprdReport.RemoveCellSpan(COL_NO_LABELS, lngLineNo)
    Next lngLineNo
    'Group all info lines together
    sprdReport.SortKey(1) = COL_PRIME_PLAN
    sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
    'delete info lines
    lngLineNo = sprdReport.SearchCol(COL_PRIME_PLAN, 1, sprdReport.MaxRows, "", SearchFlagsGreaterOrEqual)
    If lngLineNo > 0 Then
        Call sprdReport.DeleteRows(lngLineNo, (sprdReport.MaxRows - lngLineNo) + 1)
        sprdReport.MaxRows = lngLineNo - 1
    End If
    'sort by selected criteria (SKU/Plangram
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 0) Then
        sprdReport.SortKey(1) = COL_SKU
        sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdReport.MaxRows To 1 Step -1
            sprdReport.Row = lngLineNo
            sprdReport.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdReport.Text, "##")
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdReport.MaxRows = sprdReport.MaxRows + 1
                        Call sprdReport.InsertRows(sprdReport.Row + 1, 1)
                        sprdReport.Row = sprdReport.Row + 1
                        sprdReport.Col = COL_PLANGRAM
                        sprdReport.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        If (lngStart = -1) Then lngStart = sprdReport.Row
                    Else
                        sprdReport.Col = COL_SORT_LOC
                        sprdReport.Text = vntPGLocs(lngLocNo)
                        sprdReport.Col = COL_PLANGRAM
                        sprdReport.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdReport.AddCellSpan(COL_SKU, lngStart, COL_PLANGRAM - 1, sprdReport.Row - lngStart + 1)
                Call sprdReport.AddCellSpan(COL_NO_LABELS, lngStart, COL_MANAGER - COL_PLANGRAM, sprdReport.Row - lngStart + 1)
                sprdReport.Col = COL_SKU
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
                sprdReport.Col = COL_NO_LABELS
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
            End If

        Next lngLineNo
    End If
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 1) Then
        sprdReport.SortKey(1) = COL_SKU
        sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdReport.MaxRows To 1 Step -1
            sprdReport.Row = lngLineNo
            sprdReport.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdReport.Text, "##")
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdReport.MaxRows = sprdReport.MaxRows + 1
                        Call sprdReport.InsertRows(sprdReport.Row + 1, 1)
                        sprdReport.Row = sprdReport.Row + 1
                        Call sprdReport.CopyRowRange(lngLineNo, lngLineNo, sprdReport.Row)
                        sprdReport.Col = COL_PRIME_PLAN
                        sprdReport.Text = ""
                    End If
                    sprdReport.Col = COL_SORT_LOC
                    sprdReport.Text = vntPGLocs(lngLocNo)
                    sprdReport.Col = COL_PLANGRAM
                    sprdReport.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                End If
            Next lngLocNo
        Next lngLineNo
        'Sort by Plan Gram Locations
        sprdReport.SortKey(1) = COL_SORT_LOC
        sprdReport.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdReport.Sort(-1, -1, -1, -1, SortByRow)
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdReport.MaxRows To 1 Step -1
            sprdReport.Row = lngLineNo
            sprdReport.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdReport.Text, "##")
            sprdReport.Col = COL_SORT_LOC
            strPlan = sprdReport.Text
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (strPlan <> vntPGLocs(lngLocNo)) Then
                        sprdReport.MaxRows = sprdReport.MaxRows + 1
                        Call sprdReport.InsertRows(sprdReport.Row + 1, 1)
                        sprdReport.Row = sprdReport.Row + 1
                        sprdReport.Col = COL_PLANGRAM
                        sprdReport.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        If (lngStart = -1) Then lngStart = sprdReport.Row
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdReport.AddCellSpan(COL_SKU, lngStart, COL_PLANGRAM - 1, sprdReport.Row - lngStart + 1)
                Call sprdReport.AddCellSpan(COL_NO_LABELS, lngStart, COL_MANAGER - COL_PLANGRAM, sprdReport.Row - lngStart + 1)
                sprdReport.Col = COL_SKU
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
                sprdReport.Col = COL_NO_LABELS
                sprdReport.Row = lngStart
                sprdReport.BackColor = vbWhite
            End If
        Next lngLineNo
    End If
    sprdReport.ColWidth(COL_PLANGRAM) = sprdReport.MaxTextColWidth(COL_PLANGRAM) + 2
    Screen.MousePointer = vbNormal
    sprdReport.Redraw = True

End Sub

Private Sub cmdExit_Click()

    End

End Sub

Private Sub Form_Load()

Dim oRetOpt As cRetailOptions
Dim oSysDat As cSystemDates

    Call GetRoot
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    Call sprdReport.SetOddEvenRowColor(RGB(224, 224, 224), vbBlack, vbWhite, vbBlack)
    
    Me.Show
    Call InitialiseStatusBar(sbStatus)
    DoEvents
    
    Set oRetOpt = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oRetOpt.LoadMatches
    
'    mblnAutoApply = oRetOpt.Autoapplyprices
    Set oRetOpt = Nothing
    
    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.LoadMatches
    
    mdteSystemDate = oSysDat.TodaysDate
    Set oSysDat = Nothing
    
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    cmbSortBy.ListIndex = 0
    
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 4500) Then
        Me.Width = 4500
        Exit Sub
    End If
    
    sprdReport.Width = Me.Width - sprdReport.Left * 4
    sprdReport.Height = Me.Height - sprdReport.Top - cmdPrint.Height - 720 - sbStatus.Height
    cmdPrint.Top = sprdReport.Top + sprdReport.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdPrint.Left = sprdReport.Left + sprdReport.Width - cmdPrint.Width
    ucpbProgress.Left = (Me.Width - ucpbProgress.Width) / 2
    ucpbProgress.Top = (Me.Height - ucpbProgress.Height - sbStatus.Height) / 2

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF9):  'if F9 then call print
                    If cmdPrint.Visible = True Then Call cmdPrint_click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

    
Private Sub cmdPrint_click()

Dim strHeader As String
Dim oSysOptBO   As cSystemOptions

    Set oSysOptBO = goSession.Database.CreateBusinessObject(CLASSID_SYSTEMOPTIONS)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreNumber)
    Call oSysOptBO.IBo_AddLoadField(FID_SYSTEMOPTIONS_StoreName)
    Call oSysOptBO.LoadMatches


    sprdReport.MaxRows = sprdReport.MaxRows + 3
    Call sprdReport.AddCellSpan(COL_SKU, sprdReport.MaxRows - 2, COL_OLD_PRICE, sprdReport.MaxRows)
    Call sprdReport.AddCellSpan(COL_SOH, sprdReport.MaxRows - 2, 4, 1)
    Call sprdReport.AddCellSpan(COL_SOH, sprdReport.MaxRows - 1, 4, 1)
    Call sprdReport.AddCellSpan(COL_SOH, sprdReport.MaxRows, 4, 1)
    
    sprdReport.Col = COL_SOH
    sprdReport.Row = sprdReport.MaxRows - 2
    sprdReport.CellType = CellTypeEdit
    sprdReport.TypeEditMultiLine = True
    sprdReport.TypeMaxEditLen = 500
    sprdReport.Text = "I confirm that the price changes actioned on the above" & vbNewLine & _
                      "SKU's are correct and that all S.E.L. and point of sale" & vbNewLine & _
                      "Signage are correct"
    sprdReport.TypeHAlign = TypeHAlignCenter
    sprdReport.RowHeight(sprdReport.MaxRows - 2) = 30
    
    sprdReport.Row = sprdReport.MaxRows - 1
    sprdReport.CellType = CellTypeEdit
    sprdReport.TypeEditMultiLine = True
    sprdReport.TypeMaxEditLen = 100
    sprdReport.Text = vbNewLine & "Sign  ____________________________________"
    sprdReport.TypeHAlign = TypeHAlignCenter
    sprdReport.RowHeight(sprdReport.MaxRows - 1) = 30
    
    sprdReport.Row = sprdReport.MaxRows
    sprdReport.CellType = CellTypeEdit
    sprdReport.TypeEditMultiLine = True
    sprdReport.TypeMaxEditLen = 100
    sprdReport.Text = vbNewLine & "Print ____________________________________"
    sprdReport.TypeHAlign = TypeHAlignCenter
    sprdReport.RowHeight(sprdReport.MaxRows) = 30
    
    ' 02/05/03  KVB  remove Printed dd/mm/yy from strHeader
    strHeader = "Store No: " & oSysOptBO.StoreNumber & " " & _
                oSysOptBO.StoreName & "/n/cPrice Change Audit Report for " & Format(mdteSystemDate, "DD/MM/YY")
    
    ' /c is Centre text      /fb1 is Font bold on
    sprdReport.PrintHeader = "/c/fb1" & strHeader
    
    sprdReport.PrintJobName = "Price Changes Confirmation Report"
       
    sprdReport.PrintFooter = GetFooter

    sprdReport.PrintOrientation = PrintOrientationLandscape
    sprdReport.PrintRowHeaders = False
    
    'Added v1.0.11
'    If mblnFromNightlyClose = True Then
'        'do nothing
'    Else
'        frmPreview.Show vbModal
'
'    End If
    
    sprdReport.Refresh
    
    Call sprdReport.PrintSheet
    
    sprdReport.MaxRows = sprdReport.MaxRows - 3
    ' clean up
    On Error Resume Next
        
End Sub

Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        'Debug.Print lngSectNo & "-" & vntSection(lngSectNo)
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case (CALLED_FROM):
                If Mid(strSection, 3) = CF_CLOSE Then
                    mblnFromNightlyClose = True
                    blnSetOpts = True
                End If
        End Select
    Next lngSectNo
    
    If (blnSetOpts = True) Then Call LoadChanges

End Sub


Private Sub DisplayPlanogram(strSKU As String)

Dim oPlanGramBO As cStock_Wickes.cPlangram
Dim strPGLocs   As String
Dim strLocation As String
Dim colPGLocs   As Collection
Dim strFirstLoc As String
Dim lngFirstLoc As Long
Dim lngLocNo    As Long
        
    'TCH1230 - Added Plangram sequencing
    Set oPlanGramBO = goDatabase.CreateBusinessObject(CLASSID_PLANGRAM)
    Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_PartCode, strSKU)
    Set colPGLocs = oPlanGramBO.LoadMatches
    While (colPGLocs.Count > 0)
        strFirstLoc = "ZZZZZZZZZZZZZ"
        For lngLocNo = 1 To colPGLocs.Count Step 1
            Set oPlanGramBO = colPGLocs(lngLocNo)
            strLocation = String(10 - Len(Str(oPlanGramBO.PlanNumber)), "0") & oPlanGramBO.PlanNumber & "/" & String(10 - Len(oPlanGramBO.PlanSegment), "0") & oPlanGramBO.PlanSegment & "/" & String(10 - Len(oPlanGramBO.ShelfNumber), "0") & oPlanGramBO.ShelfNumber
            If (strFirstLoc > strLocation) Then
                lngFirstLoc = lngLocNo
                strFirstLoc = strLocation
            End If
        Next
        Set oPlanGramBO = colPGLocs(lngFirstLoc)
'        strPGLocs = strPGLocs & oPlanGramBO.PlanNumber & "/" & oPlanGramBO.PlanSegment & "/" & oPlanGramBO.ShelfNumber & "-" & oPlanGramBO.PlanName & "##"  '& oPlanGramBO.ShelfNumber & "-" & oPlanGramBO.PlanName & "##"
        strPGLocs = strPGLocs & strFirstLoc & "-" & oPlanGramBO.PlanName & "##"
        Set oPlanGramBO = Nothing
        Call colPGLocs.Remove(lngFirstLoc)
    Wend
    sprdReport.Col = COL_OTHER_PLANS
    sprdReport.Text = strPGLocs
    If (strPGLocs = "") Then sprdReport.Text = "N/A" 'flag with something to show as a valid line
    sprdReport.Col = COL_PRIME_PLAN
    sprdReport.Text = 1

End Sub

Private Function DecodeLocation(strLocation As String) As String

Dim strLoc  As String

    strLoc = "/" & strLocation
    While (InStr(strLoc, "/0") > 0)
        strLoc = Replace(strLoc, "/0", "/")
    Wend
    DecodeLocation = Mid$(strLoc, 2)

End Function


