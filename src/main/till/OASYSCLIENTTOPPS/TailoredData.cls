VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TailoredData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements OasysStartCom.ITailoredData
'
' $Archive: /VB/OasysClientTopps/TailoredDataTopps.cls $
' $Revision: 1 $
' $Date: 19/07/02 11:04 $
' $Author: Martynw $
'
'----------------------------------------------------------------------------
' This class simply provides all the tailored data, controlled by CTS.
' This data effectively provides ALL the system configuration that
' controls the added-value and chargeable options.  This allows us to
' provide all customers with all the same system software but they can
' only use what they have paid for.
'
' Various classes can simply get their data bundled into a user data type.
' Clearly we must try to ensure that the user types don't change and the
' simplest way is to check the size.  If there is a size mismatch we raise
' an error.
'
' This implementation provides tailored data for a 'generic' customer that
' has no hard-coded tailoring.  Any tailoring will either be generic or
' come from the registry.
'----------------------------------------------------------------------------
' Following constants copied from OasysBaseCom.Globals
Private Const TAILORED_DATA_CLIENT_MAJ_VERSION = 0
Private Const TAILORED_DATA_CLIENT_MIN_VERSION = 0
Private Const TAILORED_DATA_CLIENT_RELEASE = 1

Private Const OASYS_ERR_NULL_OBJECT_REFERENCE = vbObjectError + 3
Private Const OASYS_ERR_INVALID_ENTERPRISE_ID = vbObjectError + 5
Private Const OASYS_ERR_INCONSISTENT_OASYS_SETUP = vbObjectError + 6

Private m_oRoot As OasysStartCom.IOasysRoot     ' Useful object reference

Private Function ITailoredData_Initialise(Root As OasysStartCom.IOasysRoot) As Boolean
    ITailoredData_Initialise = False
    Set m_oRoot = Root
    If m_oRoot Is Nothing Then
        ' Integrity check, we don't need be too specific for such a gross problem
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "ITailoredData_ClientData() ", "Root reference unset."
    Else
        ITailoredData_Initialise = True
    End If
End Function

Private Property Get ITailoredData_ClientData() As OasysStartCom.tClientTailoring
    Dim d As tClientTailoring
    
    d.nMajorVersion = TAILORED_DATA_CLIENT_MAJ_VERSION
    d.nMinorVersion = TAILORED_DATA_CLIENT_MIN_VERSION
    d.nRelease = TAILORED_DATA_CLIENT_RELEASE
    d.strShortName = "Topps Tiles"
    d.strFullName = "Topps Tiles Limited"
    d.strMnemonic = "Topps"
    If m_oRoot Is Nothing Then
        Err.Raise OASYS_ERR_NULL_OBJECT_REFERENCE, "ITailoredData_ClientData() ", "Root reference unset."
    End If
    
    ITailoredData_ClientData = d
End Property

Private Property Get ITailoredData_EnterpriseCount() As Integer
    ITailoredData_EnterpriseCount = 2
End Property

Private Property Get ITailoredData_EnterpriseData(nId As Integer) As tEnterpriseTailoring
    If nId = 0 Or nId > ITailoredData_EnterpriseCount Then
        Err.Raise OASYS_ERR_INVALID_ENTERPRISE_ID, "ITailoredData_EnterpriseData() ", "Invalid Enterprise Id=" & nId & ", max=" & ITailoredData_EnterpriseCount
    Else
        Select Case nId
        Case Is = 1
            ITailoredData_EnterpriseData = EnterpriseData1
        Case Is = 2
            ITailoredData_EnterpriseData = EnterpriseData2
        Case Else
            Err.Raise OASYS_ERR_INVALID_ENTERPRISE_ID, "ITailoredData_EnterpriseData() ", "Enterprise Id=" & nId & ", Has missing tailored data."
        End Select
    End If

End Property

Private Property Get EnterpriseData1() As tEnterpriseTailoring
    Dim d As tEnterpriseTailoring
    ' First Enterprise data is derived from the Client data.
    Dim ClientData As OasysStartCom.tClientTailoring
    ClientData = ITailoredData_ClientData
    d.strShortName = ClientData.strShortName
    d.strFullName = ClientData.strFullName
    EnterpriseData1 = d
End Property

Private Property Get EnterpriseData2() As tEnterpriseTailoring
    Dim d As tEnterpriseTailoring
    d.strShortName = "Topps Discount Stores"
    d.strFullName = "Topps Discount Stores Plc."
    EnterpriseData2 = d
End Property

Private Sub Class_Terminate()
    Set m_oRoot = Nothing
End Sub
