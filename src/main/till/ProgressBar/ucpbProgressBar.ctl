VERSION 5.00
Object = "{C8530F8A-C19C-11D2-99D6-9419F37DBB29}#1.1#0"; "ccrpprg6.ocx"
Begin VB.UserControl ucpbProgressBar 
   ClientHeight    =   1860
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5100
   ScaleHeight     =   1860
   ScaleWidth      =   5100
   ToolboxBitmap   =   "ucpbProgressBar.ctx":0000
   Begin VB.PictureBox Frame1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1860
      Left            =   0
      ScaleHeight     =   1830
      ScaleWidth      =   5070
      TabIndex        =   0
      Top             =   0
      Width           =   5100
      Begin CCRProgressBar6.ccrpProgressBar ccrpProgressBar1 
         Height          =   375
         Left            =   240
         Top             =   1140
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   661
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblFrame1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Progress Indicator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   2
         Top             =   60
         Width           =   1560
      End
      Begin VB.Label lblCaption1 
         BackStyle       =   0  'Transparent
         Caption         =   "Current Operation"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   660
         Width           =   4575
      End
   End
End
Attribute VB_Name = "ucpbProgressBar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit
'Default Property Values:
Const m_def_Enabled = 0
'Property Variables:
Dim m_Enabled As Boolean



Public Property Get Enabled() As Boolean
Attribute Enabled.VB_Description = "Returns/sets a value that determines whether an object can respond to user-generated events."
  Enabled = m_Enabled
End Property

Public Property Let Enabled(ByVal New_Enabled As Boolean)
  m_Enabled = New_Enabled
  PropertyChanged "Enabled"
End Property

Public Sub Refresh()
Attribute Refresh.VB_Description = "Forces a complete repaint of a object."
   
    Frame1.Refresh
   
End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=ccrpProgressBar1,ccrpProgressBar1,-1,Max
Public Property Get Max() As Long
Attribute Max.VB_Description = "Returns/sets the maximum value of the Progress Bar"
  Max = ccrpProgressBar1.Max
End Property

Public Property Let Max(ByVal New_Max As Long)
  If New_Max < ccrpProgressBar1.Value Then ccrpProgressBar1.Value = New_Max
  ccrpProgressBar1.Max() = New_Max
  PropertyChanged "Max"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=ccrpProgressBar1,ccrpProgressBar1,-1,Min
Public Property Get Min() As Long
Attribute Min.VB_Description = "Returns/sets the minimum position for the progress bar"
  Min = ccrpProgressBar1.Min
End Property

Public Property Let Min(ByVal New_Min As Long)
  ccrpProgressBar1.Min() = New_Min
  PropertyChanged "Min"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=ccrpProgressBar1,ccrpProgressBar1,-1,Smooth
Public Property Get Smooth() As Boolean
Attribute Smooth.VB_Description = "Returns/set whether or no a smooth Progress Bar is drawn."
  Smooth = ccrpProgressBar1.Smooth
End Property

Public Property Let Smooth(ByVal New_Smooth As Boolean)
  ccrpProgressBar1.Smooth() = New_Smooth
  PropertyChanged "Smooth"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=ccrpProgressBar1,ccrpProgressBar1,-1,Value
Public Property Get Value() As Long
Attribute Value.VB_Description = "Returns/sets the current position of the progress bar"
  Value = ccrpProgressBar1.Value
End Property

Public Property Let Value(ByVal New_Value As Long)
    If New_Value > ccrpProgressBar1.Max Then New_Value = ccrpProgressBar1.Max
    ccrpProgressBar1.Value() = New_Value
    PropertyChanged "Value"
End Property

Private Sub lblCaption1_Change()

    Call lblCaption1.Refresh

End Sub

Private Sub UserControl_Initialize()
  
  UserControl.Width = Frame1.Width
  UserControl.Height = Frame1.Height

End Sub

'Initialize Properties for User Control
Private Sub UserControl_InitProperties()
  m_Enabled = m_def_Enabled
  UserControl.Width = Frame1.Width
  UserControl.Height = Frame1.Height
End Sub

'Load property values from storage
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)

  m_Enabled = PropBag.ReadProperty("Enabled", m_def_Enabled)
  ccrpProgressBar1.Max = PropBag.ReadProperty("Max", 100)
  ccrpProgressBar1.Min = PropBag.ReadProperty("Min", 0)
  ccrpProgressBar1.Smooth = PropBag.ReadProperty("Smooth", True)
  ccrpProgressBar1.Value = PropBag.ReadProperty("Value", 25)
  lblFrame1.Caption = PropBag.ReadProperty("Title", "Frame1")
  lblCaption1.Caption = PropBag.ReadProperty("Caption1", "Label1")
  ccrpProgressBar1.FillColor = PropBag.ReadProperty("FillColor", &H8000000D)
  UserControl.Width = PropBag.ReadProperty("Width", 1860)
  UserControl.Height = PropBag.ReadProperty("Height", 5100)
    Debug.Print Now

End Sub

Private Sub UserControl_Resize()
    
    UserControl.Width = 5100
    UserControl.Height = 1860

End Sub

'Write property values to storage
Private Sub UserControl_WriteProperties(PropBag As PropertyBag)

  Call PropBag.WriteProperty("Enabled", m_Enabled, m_def_Enabled)
  Call PropBag.WriteProperty("Max", ccrpProgressBar1.Max, 100)
  Call PropBag.WriteProperty("Min", ccrpProgressBar1.Min, 0)
  Call PropBag.WriteProperty("Smooth", ccrpProgressBar1.Smooth, True)
  Call PropBag.WriteProperty("Value", ccrpProgressBar1.Value, 25)
  Call PropBag.WriteProperty("Title", lblFrame1.Caption, "Frame1")
  Call PropBag.WriteProperty("Caption1", lblCaption1.Caption, "Label1")
  Call PropBag.WriteProperty("FillColor", ccrpProgressBar1.FillColor, &H8000000D)
  Call PropBag.WriteProperty("Width", UserControl.Width, 1860)
  Call PropBag.WriteProperty("Height", UserControl.Height, 5100)
End Sub

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=Frame1,Frame1,-1,Caption
Public Property Get Title() As String
Attribute Title.VB_Description = "Returns/sets the text displayed in an object's title bar or below an object's icon."
  Title = lblFrame1.Caption
End Property

Public Property Let Title(ByVal New_Title As String)
  lblFrame1.Caption() = New_Title
  PropertyChanged "Title"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=lblCaption1,lblCaption1,-1,Caption
Public Property Get Caption1() As String
Attribute Caption1.VB_Description = "Returns/sets the text displayed in an object's title bar or below an object's icon."
  Caption1 = lblCaption1.Caption
End Property

Public Property Let Caption1(ByVal New_Caption1 As String)
  lblCaption1.Caption() = New_Caption1
  PropertyChanged "Caption1"
End Property

'WARNING! DO NOT REMOVE OR MODIFY THE FOLLOWING COMMENTED LINES!
'MappingInfo=ccrpProgressBar1,ccrpProgressBar1,-1,FillColor
Public Property Get FillColor() As OLE_COLOR
Attribute FillColor.VB_Description = "Returns/sets the color used to fill the progress bar."
  FillColor = ccrpProgressBar1.FillColor
End Property

Public Property Let FillColor(ByVal New_FillColor As OLE_COLOR)
  ccrpProgressBar1.FillColor() = New_FillColor
  PropertyChanged "FillColor"
End Property

