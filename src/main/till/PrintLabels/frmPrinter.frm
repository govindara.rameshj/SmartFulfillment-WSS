VERSION 5.00
Begin VB.Form frmPrinter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Printer on which to Print labels"
   ClientHeight    =   5835
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6300
   Icon            =   "frmPrinter.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5835
   ScaleWidth      =   6300
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraPrinter 
      Caption         =   "Current Printer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   6075
      Begin VB.Label lblPrinter 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   5835
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4620
      TabIndex        =   3
      Top             =   4980
      Width           =   1575
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select Printer"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   2
      Top             =   4980
      Width           =   2175
   End
   Begin VB.ListBox lstPrinters 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3300
      Left            =   120
      TabIndex        =   1
      Top             =   1560
      Width           =   6015
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Select Printer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   1140
      Width           =   2655
   End
End
Attribute VB_Name = "frmPrinter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()

    Me.Hide

End Sub

Private Sub cmdSelect_Click()

Dim oPrinter As Printer
    
    For Each oPrinter In Printers
        If (oPrinter.DeviceName = lstPrinters.Text) Then
            Set Printer = oPrinter
            Exit For
        End If
    Next
    Me.Hide

End Sub

Private Sub Form_Load()

Dim oPrinter As Printer

    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    For Each oPrinter In Printers
        lstPrinters.AddItem (oPrinter.DeviceName)
    Next
    
    If (lstPrinters.ListCount > 0) Then lstPrinters.ListIndex = 0
    lblPrinter.Caption = Printer.DeviceName
    
End Sub
