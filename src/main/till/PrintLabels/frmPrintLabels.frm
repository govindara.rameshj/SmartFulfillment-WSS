VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Object = "{B3202B8D-B8B9-4655-9712-EAB5BFD920D4}#1.0#0"; "ItemFilter.ocx"
Begin VB.Form frmPrintLabels 
   Caption         =   "Print labels / barcodes"
   ClientHeight    =   7875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13755
   Icon            =   "frmPrintLabels.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7875
   ScaleWidth      =   13755
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraSortBy 
      Caption         =   "Display Order"
      Height          =   1755
      Left            =   11520
      TabIndex        =   49
      Top             =   60
      Visible         =   0   'False
      Width           =   1995
      Begin VB.ComboBox cmbSortBy 
         Height          =   315
         ItemData        =   "frmPrintLabels.frx":0A96
         Left            =   180
         List            =   "frmPrintLabels.frx":0AA0
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   660
         Width           =   1635
      End
      Begin VB.Label Label8 
         Caption         =   "Sort By"
         Height          =   255
         Left            =   180
         TabIndex        =   51
         Top             =   420
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdShowPG 
      Caption         =   "F4-Show/Hide Other Planogram Locs"
      Height          =   435
      Left            =   9660
      TabIndex        =   28
      Top             =   6600
      Visible         =   0   'False
      Width           =   1695
   End
   Begin CTSProgBar.ucpbProgressBar ucpbProgress 
      Height          =   1860
      Left            =   4680
      TabIndex        =   26
      Top             =   2280
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin VB.Frame fraPrinter 
      Height          =   495
      Left            =   1380
      TabIndex        =   21
      Top             =   6600
      Width           =   5355
      Begin VB.CommandButton cmdPrinter 
         Caption         =   "Printer"
         Height          =   315
         Left            =   4260
         TabIndex        =   24
         Top             =   120
         Width           =   1035
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Printer"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   180
         Width           =   615
      End
      Begin VB.Label lblPrinter 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   660
         TabIndex        =   23
         Top             =   120
         Width           =   3435
      End
   End
   Begin VB.Frame fraEntryMode 
      Caption         =   "Select Entry Mode"
      Height          =   1755
      Left            =   2280
      TabIndex        =   2
      Top             =   60
      Visible         =   0   'False
      Width           =   9195
      Begin VB.Frame fraPriceChanges 
         Caption         =   "Price Change Criteria"
         Height          =   1515
         Left            =   1680
         TabIndex        =   36
         Top             =   120
         Visible         =   0   'False
         Width           =   4995
         Begin VB.CommandButton cmdGetPriceChanges 
            Caption         =   "F7-Process"
            Height          =   315
            Left            =   3900
            TabIndex        =   46
            Top             =   1020
            Width           =   975
         End
         Begin VB.Frame fraSKURange 
            Height          =   495
            Left            =   1260
            TabIndex        =   41
            Top             =   900
            Width           =   2595
            Begin VB.TextBox txtPCEndSKU 
               Height          =   315
               Left            =   1860
               MaxLength       =   15
               TabIndex        =   43
               Text            =   "888888"
               Top             =   120
               Width           =   675
            End
            Begin VB.TextBox txtPCStartSKU 
               Height          =   315
               Left            =   840
               MaxLength       =   15
               TabIndex        =   42
               Text            =   "888888"
               Top             =   120
               Width           =   675
            End
            Begin VB.Label Label7 
               Caption         =   "To"
               Height          =   255
               Left            =   1560
               TabIndex        =   45
               Top             =   180
               Width           =   255
            End
            Begin VB.Label Label6 
               Caption         =   "SKU Start"
               Height          =   195
               Left            =   60
               TabIndex        =   44
               Top             =   180
               Width           =   855
            End
         End
         Begin VB.CheckBox chkAllSKUs 
            Caption         =   "All  SKU's"
            Height          =   255
            Left            =   180
            TabIndex        =   40
            Top             =   1080
            Width           =   1035
         End
         Begin VB.OptionButton optPriceChange 
            Caption         =   "Price Decreases"
            Height          =   195
            Index           =   1
            Left            =   1740
            TabIndex        =   39
            Top             =   300
            Width           =   1575
         End
         Begin VB.OptionButton optPriceChange 
            Caption         =   "Price Increases"
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   38
            Top             =   300
            Width           =   1455
         End
         Begin VB.CheckBox chkApplied 
            Caption         =   "Applied"
            Height          =   255
            Left            =   2640
            TabIndex        =   37
            Top             =   600
            Width           =   975
         End
         Begin ucEditDate.ucDateText dtxtStartDate 
            Height          =   315
            Left            =   1260
            TabIndex        =   47
            Top             =   600
            Width           =   915
            _ExtentX        =   1614
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DateFormat      =   "DD/MM/YY"
            Text            =   "19/10/06"
         End
         Begin VB.Label Label5 
            Caption         =   "Up To Date"
            Height          =   195
            Left            =   180
            TabIndex        =   48
            Top             =   660
            Width           =   915
         End
      End
      Begin VB.CommandButton cmdStockEnquiry 
         Caption         =   "F2 Stock Enquiry"
         Height          =   435
         Left            =   1620
         TabIndex        =   35
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton cmdEnterSKUs 
         Caption         =   "Enter SKUs"
         Height          =   435
         Left            =   1620
         TabIndex        =   27
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdReset 
         Caption         =   "F3 - Reset"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   1440
         Width           =   1335
      End
      Begin VB.OptionButton optEntryMode 
         Caption         =   "Supplier"
         Height          =   255
         Index           =   2
         Left            =   180
         TabIndex        =   5
         Top             =   720
         Width           =   1275
      End
      Begin VB.OptionButton optEntryMode 
         Caption         =   "SKU"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   3
         Top             =   240
         Width           =   1275
      End
      Begin VB.OptionButton optEntryMode 
         Caption         =   "Price Changes"
         Height          =   255
         Index           =   3
         Left            =   180
         TabIndex        =   6
         Top             =   960
         Width           =   1395
      End
      Begin VB.OptionButton optEntryMode 
         Caption         =   "HHT Request"
         Height          =   255
         Index           =   4
         Left            =   180
         TabIndex        =   7
         Top             =   1200
         Width           =   1395
      End
      Begin VB.OptionButton optEntryMode 
         Caption         =   "Planogram"
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   4
         Top             =   480
         Width           =   1395
      End
      Begin VB.Frame fraHHTHeaders 
         Caption         =   "HHT Requests"
         Height          =   1515
         Left            =   1620
         TabIndex        =   19
         Top             =   120
         Visible         =   0   'False
         Width           =   5115
         Begin FPSpreadADO.fpSpread sprdHHT 
            Height          =   1095
            Left            =   120
            TabIndex        =   20
            Top             =   240
            Width           =   4875
            _Version        =   458752
            _ExtentX        =   8599
            _ExtentY        =   1931
            _StockProps     =   64
            DisplayColHeaders=   0   'False
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   7
            MaxRows         =   5
            ScrollBars      =   0
            SpreadDesigner  =   "frmPrintLabels.frx":0AB4
            UserResize      =   0
         End
      End
      Begin VB.Frame fraPlangram 
         Caption         =   "Planogram Criteria"
         Height          =   1575
         Left            =   1620
         TabIndex        =   8
         Top             =   120
         Width           =   7455
         Begin VB.Frame fraPGButtons 
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   60
            TabIndex        =   30
            Top             =   1260
            Width           =   7335
            Begin VB.CommandButton cmdPGProcess 
               Caption         =   "F7-Process"
               Height          =   255
               Left            =   6240
               TabIndex        =   33
               Top             =   0
               Width           =   1095
            End
            Begin VB.CommandButton cmdAllPlangram 
               Caption         =   "Select All"
               Height          =   255
               Left            =   0
               TabIndex        =   32
               Top             =   0
               Width           =   1335
            End
            Begin VB.CommandButton cmdNoPlangram 
               Caption         =   "De-select All"
               Height          =   255
               Left            =   1500
               TabIndex        =   31
               Top             =   0
               Width           =   1335
            End
         End
         Begin FPSpreadADO.fpSpread sprdPlangram 
            Height          =   1035
            Left            =   60
            TabIndex        =   9
            Top             =   180
            Width           =   7335
            _Version        =   458752
            _ExtentX        =   12938
            _ExtentY        =   1826
            _StockProps     =   64
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   204
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   4
            MaxRows         =   1
            OperationMode   =   2
            ScrollBars      =   2
            SelectBlockOptions=   2
            SpreadDesigner  =   "frmPrintLabels.frx":15D4
            UserResize      =   2
         End
      End
      Begin VB.Frame fraSupplier 
         Caption         =   "Supplier Criteria"
         Height          =   1455
         Left            =   1620
         TabIndex        =   10
         Top             =   180
         Visible         =   0   'False
         Width           =   2715
         Begin VB.CommandButton cmdGetSuppItems 
            Caption         =   "F7-Process"
            Height          =   315
            Left            =   1020
            TabIndex        =   15
            Top             =   960
            Width           =   1335
         End
         Begin VB.TextBox txtEndSupplier 
            Height          =   300
            Left            =   1380
            MaxLength       =   6
            TabIndex        =   14
            Top             =   600
            Width           =   975
         End
         Begin VB.TextBox txtStartSupplier 
            Height          =   300
            Left            =   1380
            MaxLength       =   6
            TabIndex        =   12
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label4 
            Caption         =   "End Supplier"
            Height          =   255
            Left            =   180
            TabIndex        =   13
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label Label3 
            Caption         =   "Start Supplier"
            Height          =   255
            Left            =   180
            TabIndex        =   11
            Top             =   240
            Width           =   1035
         End
      End
   End
   Begin VB.ListBox lstFormats 
      Height          =   1425
      Left            =   120
      TabIndex        =   1
      Top             =   300
      Width           =   2055
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "F9 - Print"
      Height          =   435
      Left            =   8280
      TabIndex        =   25
      Top             =   6600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10 - Exit"
      Height          =   435
      Left            =   120
      TabIndex        =   17
      Top             =   6660
      Width           =   1155
   End
   Begin FPSpreadADO.fpSpread sprdSKUs 
      Height          =   4695
      Left            =   120
      TabIndex        =   16
      Top             =   1800
      Visible         =   0   'False
      Width           =   13455
      _Version        =   458752
      _ExtentX        =   23733
      _ExtentY        =   8281
      _StockProps     =   64
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   26
      MaxRows         =   1
      OperationMode   =   2
      RowHeaderDisplay=   0
      SpreadDesigner  =   "frmPrintLabels.frx":199A
      UserResize      =   2
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   18
      Top             =   7500
      Width           =   13755
      _ExtentX        =   24262
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1147
            MinWidth        =   1147
            Picture         =   "frmPrintLabels.frx":24BF
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17092
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "12:01"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ItemFilter_UC_Wickes.ucItemFilter ucItemFind 
      Height          =   255
      Left            =   12360
      TabIndex        =   34
      Top             =   6720
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   450
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label Type"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   915
   End
End
Attribute VB_Name = "frmPrintLabels"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module :
'* Date   : 19/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Refunds Report/frmRefundReport.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 26/06/06 16:19 $ $Revision: 8 $
'* Versions:
'* 19/09/02    mauricem
'*             Header added.
'* 26/03/08    mauricem
'*             WIX1312 - Added WEEE Rate for each item
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmPrintLabels"

Const COL_SKU       As Long = 1
Const COL_DESC      As Long = 2
Const COL_PLANOGRAM As Long = 3
Const COL_SUPPLIER  As Long = 4
Const COL_PRICE     As Long = 5
Const COL_WEEE      As Long = 6
Const COL_SPACER    As Long = 7
Const COL_QTY       As Long = 8
Const COL_EAN       As Long = 9
Const COL_PREV_PR   As Long = 10
Const COL_EQUIV_PR  As Long = 11
Const COL_UOM       As Long = 12
Const COL_OBS       As Long = 13
Const COL_STOCK_OFF As Long = 14
Const COL_FRAGILE   As Long = 15
Const COL_TIMB      As Long = 16
Const COL_PACK      As Long = 17
Const COL_ID        As Long = 18
Const COL_PRINT     As Long = 19
Const COL_OTHER_PLANS As Long = 20
Const COL_PRIME_PLAN  As Long = 21
Const COL_SORT_SUPP   As Long = 22
Const COL_PRIME_LOC   As Long = 23
Const COL_SORT_LOC    As Long = 24
Const COL_HHT_POS     As Long = 25
Const COL_APPLIED     As Long = 26

Const OPT_ENTRY_SKU As Long = 0
Const OPT_ENTRY_PLANGRAM As Long = 1
Const OPT_ENTRY_SUPPLIER As Long = 2
Const OPT_ENTRY_PRICES As Long = 3
Const OPT_ENTRY_HHT As Long = 4

Const COL_PG_REF As Long = 1
Const COL_PG_DESC As Long = 2
Const COL_PG_RETRIEVE As Long = 3
Const COL_PG_ID As Long = 4

Const PRM_TRANSNO               As Long = 911
Const PRM_LABELS_PATH           As Long = 500
Const PRM_LABELS_PAGE           As Long = 501
Const PRM_USE_WEEE_RATES        As Long = 846 'added 16/04/08 - check if WEEE Rates
Const PRM_REF_834_SWITCHABLE    As Long = 980834

Const USE_PLANGRAM As Boolean = True

Private mdteSystemDate  As Date
Private mdteTodayDate  As Date
Private mblnAutoApply   As Boolean

Private mblnFromNightlyClose As Boolean
Private mstrDestCode         As Boolean

Private moPrintStore    As cStore
Private mcolLabels      As New Collection

Private mstrSKU As String

Dim moTempFilBO      As cTempFil
Dim mcolHHTLabels    As Collection
Dim mcolSuppliers    As New Collection
Dim mcolWEEESKUs     As New Collection 'WIX1337 - holds WEEE SKU's
Dim mlngNoSmlLblPage As Long
Dim mlngNoMedLblPage As Long
Dim mlngNoLrgLblPage As Long

Dim mlngPlanagramSize As Long

Dim mblnHidden  As Boolean

Private Sub DisplayPlanogram(strSKU As String)

Dim oPlanGramBO As cStock_Wickes.cPlangram
Dim strPGLocs   As String
Dim strLocation As String
Dim colPGLocs   As Collection
Dim strFirstLoc As String
Dim lngFirstLoc As Long
Dim lngLocNo    As Long
        
    'TCH1230 - Added Plangram sequencing
    Set oPlanGramBO = goDatabase.CreateBusinessObject(CLASSID_PLANGRAM)
    Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_PartCode, strSKU)
    Set colPGLocs = oPlanGramBO.LoadMatches
    While (colPGLocs.Count > 0)
        'Step through list of locations and locate first in sequence
        strFirstLoc = "ZZZZZZZZZZZZZ"
        For lngLocNo = 1 To colPGLocs.Count Step 1
            Set oPlanGramBO = colPGLocs(lngLocNo)
            strLocation = String(10 - Len(Str(oPlanGramBO.PlanNumber)), "0") & oPlanGramBO.PlanNumber & "/" & String(10 - Len(oPlanGramBO.PlanSegment), "0") & oPlanGramBO.PlanSegment & "/" & String(10 - Len(oPlanGramBO.ShelfNumber), "0") & oPlanGramBO.ShelfNumber & "/" & String(10 - Len(oPlanGramBO.ShelfSequence), "0") & oPlanGramBO.ShelfSequence
            If (strFirstLoc > strLocation) Then
                lngFirstLoc = lngLocNo
                strFirstLoc = strLocation
            End If
        Next
        Set oPlanGramBO = colPGLocs(lngFirstLoc)
        strPGLocs = strPGLocs & strFirstLoc & "-" & oPlanGramBO.PlanName & "##"
        Set oPlanGramBO = Nothing
        'Remove first location so that it is not repeated
        Call colPGLocs.Remove(lngFirstLoc)
    Wend
    sprdSKUs.Col = COL_OTHER_PLANS
    sprdSKUs.Text = strPGLocs
    If (strPGLocs = "") Then sprdSKUs.Text = "N/A" 'flag with something to show as a valid line
    sprdSKUs.Col = COL_PRIME_PLAN
    sprdSKUs.Text = 1

End Sub

Private Sub LoadPriceChanges()

Dim cPriceBO    As cStock_Wickes.cPriceChange
Dim oItemBO     As cStock_Wickes.cInventory
Dim colPrices   As Collection
Dim dicEffectivePrice As Dictionary
Dim lngPGNo     As Long
Dim lngRowNo    As Long
Dim oLabelInfo  As clsLabelInfo
Dim blnSkip     As Boolean
Dim lngPrintQty As Long
Dim strLableWhere As String
Dim strSkuWhere As String
Dim strAppliedWhere As String
Dim IntLoadPCSystemDays As Integer

    Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
     
    strLableWhere = ""
    strSkuWhere = ""
    strAppliedWhere = ""
       
    Set cPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
         
    Call cPriceBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_EffectiveDate, GetDate(dtxtStartDate.Text))
    
    If chkApplied.Value = 1 Then
        IntLoadPCSystemDays = CInt(goSession.GetParameter(504))
        Call cPriceBO.AddLoadFilter(CMP_NOTEQUAL, FID_PRICECHANGE_ChangeStatus, "U")
        Call cPriceBO.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_PRICECHANGE_EffectiveDate, DateAdd("d", -IntLoadPCSystemDays, GetDate(dtxtStartDate.Text)))
        strAppliedWhere = "AND PSTA <> 'U' AND PDAT >= CONVERT(date, '" & Format(DateAdd("d", -IntLoadPCSystemDays, GetDate(dtxtStartDate.Text)), "yyyy-mm-dd") & "', 120) "
    End If
    
    Select Case (oLabelInfo.Size)
        Case ("S"):   Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_SmallLabel, True)
                      strLableWhere = "AND LABS = 1"
        Case ("M"):   Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_MediumLabel, True)
                      strLableWhere = "AND LABM = 1"
        Case ("L"):   Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_LargeLabel, True)
                      strLableWhere = "AND LABL = 1"
        Case Else:    Call MsgBoxEx("Unrecognised Label Size-'" & oLabelInfo.Size & "'" & vbNewLine & "Exiting Price Change Retrieval", vbInformation, "Invalid Label Size", , , , , RGBMsgBox_WarnColour)
                      Call cmdReset_Click
                      Exit Sub
    End Select
    
    If (txtPCStartSKU.Text <> "") Or (txtPCEndSKU.Text <> "") Then
        If (txtPCStartSKU.Text <> "") Then txtPCStartSKU.Text = Format(txtPCStartSKU.Text, "000000")
        If (txtPCEndSKU.Text <> "") Then txtPCEndSKU.Text = Format(txtPCEndSKU.Text, "000000")
        If (txtPCStartSKU.Text = txtPCEndSKU.Text) Then
            Call cPriceBO.AddLoadFilter(CMP_EQUAL, FID_PRICECHANGE_PartCode, Format(txtPCStartSKU.Text, "000000"))
            strSkuWhere = " AND SKUN = '" & txtPCStartSKU.Text & "'"
        Else
            If (txtPCStartSKU.Text <> "") Then Call cPriceBO.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_PRICECHANGE_PartCode, txtPCStartSKU.Text)
            If (txtPCEndSKU.Text <> "") Then Call cPriceBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_PRICECHANGE_PartCode, txtPCEndSKU.Text)
            strSkuWhere = " AND (SKUN >= '" & txtPCStartSKU.Text & "' AND SKUN <= '" & txtPCEndSKU.Text & "')"
        End If
    End If
      
    Screen.MousePointer = vbHourglass
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving Price Changes"
    Set colPrices = cPriceBO.LoadMatches
    Set dicEffectivePrice = GetEffectivePriceDictionary(Format(mdteSystemDate, "yyyy-mm-dd"), strAppliedWhere, strLableWhere, strSkuWhere)
        
    If (colPrices.Count > 0) Then ucpbProgress.Max = colPrices.Count
    ucpbProgress.Caption1 = "Displaying Items"
    sprdSKUs.MaxRows = 0
     
    For lngRowNo = 1 To colPrices.Count Step 1
        blnSkip = False
        ucpbProgress.Value = lngRowNo
        Set cPriceBO = colPrices(lngRowNo)
                
        If cPriceBO.EffectiveDate <= mdteSystemDate Then
            If dicEffectivePrice.Exists(cPriceBO.PartCode) Then
                If dicEffectivePrice(cPriceBO.PartCode) <> cPriceBO.EffectiveDate Then
                    blnSkip = True
                End If
            End If
        End If
                   
        Set oItemBO = SetUpItem
        Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, cPriceBO.PartCode)
        Call oItemBO.LoadMatches
                    
        Select Case (oLabelInfo.Size)
            Case ("S"): lngPrintQty = oItemBO.NoOfSmallLabels
                        If (oItemBO.NoOfSmallLabels = 0) Then
                            cPriceBO.SmallLabel = False
                            Call cPriceBO.IBo_SaveIfExists
                            blnSkip = True
                        End If
            Case ("M"): lngPrintQty = oItemBO.NoOfMediumLabels
                        If (oItemBO.NoOfMediumLabels = 0) Then
                            cPriceBO.MediumLabel = False
                            Call cPriceBO.IBo_SaveIfExists
                            blnSkip = True
                        End If
            Case ("L"): lngPrintQty = oItemBO.NoOfLargeLabels
                        If (oItemBO.NoOfLargeLabels = 0) Then
                            cPriceBO.LargeLabel = False
                            Call cPriceBO.IBo_SaveIfExists
                            blnSkip = True
                        End If
        End Select
                        
        'Perform price change increase/decrease check
        If (optPriceChange(1).Value = True) Then
            If (oItemBO.NormalSellPrice < cPriceBO.NewPrice) Then blnSkip = True
        Else
            If (oItemBO.NormalSellPrice > cPriceBO.NewPrice) Then blnSkip = True
        End If
        
        If (oItemBO.AutoApplyPriceChanges = True) And ((oItemBO.QuantityAtHand + oItemBO.MarkDownQuantity) = 0) Then blnSkip = True
        If (oItemBO.NonStockItem = True) And ((oItemBO.QuantityAtHand + oItemBO.MarkDownQuantity) = 0) Then blnSkip = True
        If (oItemBO.ItemObsolete = True) And ((oItemBO.QuantityAtHand + oItemBO.MarkDownQuantity) = 0) Then blnSkip = True
                 
        If (blnSkip = False) Then
            sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
            sprdSKUs.Row = sprdSKUs.MaxRows
            sprdSKUs.Col = COL_SKU
            sprdSKUs.Text = cPriceBO.PartCode
            sprdSKUs.Lock = True
            sprdSKUs.Col = COL_DESC
            sprdSKUs.Text = IIf(oItemBO.Description = "", "STKMAS Entry Missing", oItemBO.Description)
            sprdSKUs.Col = COL_PRICE
            sprdSKUs.Text = cPriceBO.NewPrice + WEEECost(oItemBO.PRFSKU)
            sprdSKUs.Col = COL_WEEE
            sprdSKUs.Text = WEEECost(oItemBO.PRFSKU)
            sprdSKUs.Col = COL_SUPPLIER
            sprdSKUs.Text = oItemBO.SupplierNo & "-" & SupplierName(oItemBO.SupplierNo)
            sprdSKUs.Col = COL_EQUIV_PR
            sprdSKUs.Text = oItemBO.EquivalentPriceMult
            sprdSKUs.Col = COL_UOM
            sprdSKUs.Text = oItemBO.EquivalentPriceUnit
            sprdSKUs.Col = COL_QTY
            sprdSKUs.Text = lngPrintQty
            sprdSKUs.Col = COL_ID
            sprdSKUs.Text = cPriceBO.EffectiveDate
            sprdSKUs.Col = COL_APPLIED
            sprdSKUs.Value = IIf(cPriceBO.ChangeStatus = "U", 0, 1)
            sprdSKUs.ColHidden = False
            sprdSKUs.Col = COL_PRINT
            sprdSKUs.Value = 1
            Call DisplayPlanogram(oItemBO.PartCode)
        End If
    Next lngRowNo

    sprdSKUs.ColWidth(COL_DESC) = sprdSKUs.MaxTextColWidth(COL_DESC)
    ucpbProgress.Visible = False
    If (sprdSKUs.MaxRows > 0) Then
        sprdSKUs.Visible = True
        cmdPrint.Visible = True
        cmdShowPG.Visible = True
        Call sprdSKUs.SetFocus
        
        If (cmbSortBy.ListIndex = 0) Then
            Call cmbSortBy_Click
        Else
            cmbSortBy.ListIndex = 0
        End If

    Else
        Call MsgBoxEx("No SKUs found that match selected criteria", vbInformation, "No Matches Found", , , , , RGBMSGBox_PromptColour)
    End If
    
    mblnHidden = False
    Screen.MousePointer = vbNormal
    fraSortBy.Visible = True
End Sub

Private Function GetEffectivePriceDictionary(strDate As String, strApplied As String, strLable As String, strSKU As String) As Dictionary
    Dim oConnection As Connection
    Dim objCommand As ADODB.Command
    Dim rsPrice As Recordset
    Dim dicPrice As New Dictionary
    
    Set oConnection = goDatabase.Connection
    Set objCommand = New ADODB.Command
    
    With objCommand
        .ActiveConnection = oConnection
        .CommandType = adCmdText
        .CommandText = "SELECT SKUN, MAX(PDAT) AS MPDAT FROM dbo.PRCCHG WHERE PDAT <= '" & _
                        Format(strDate, "yyyy-mm-dd") & "' " & strApplied & strLable & strSKU & " GROUP BY SKUN"
        .Prepared = True
    End With
   
    Set rsPrice = objCommand.Execute()
   
    Do While Not rsPrice.EOF
        dicPrice.Add rsPrice(0).Value, rsPrice(1).Value
        rsPrice.MoveNext
    Loop

    Set GetEffectivePriceDictionary = dicPrice
    
End Function

Private Sub LoadSupplier()

Dim cPriceBO    As cStock_Wickes.cPriceChange
Dim oItemBO     As cStock_Wickes.cInventory
Dim colPrices   As Collection
Dim colItems    As Collection
Dim lngRowNo    As Long
Dim oLabelInfo  As clsLabelInfo

    Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
    If (txtStartSupplier.Text = "") And (txtEndSupplier.Text = "") Then
        If (MsgBoxEx("No Supplier codes entered!" & vbNewLine & "This will retrieve all SKUs and may take some time." & vbNewLine & vbNewLine & "Do you wish to proceed?", vbYesNo + vbDefaultButton2, "Retrieve SKUs", , , , , RGBMSGBox_PromptColour) = vbNo) Then Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving Items"
    Set oItemBO = SetUpItem
    If (txtStartSupplier.Text <> "") Or (txtEndSupplier.Text <> "") Then
        If (txtStartSupplier.Text = txtEndSupplier.Text) Then
            Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_SupplierNo, txtStartSupplier.Text)
        Else
            If (txtStartSupplier.Text <> "") Then Call oItemBO.AddLoadFilter(CMP_GREATEREQUALTHAN, FID_INVENTORY_SupplierNo, txtStartSupplier.Text)
            If (txtEndSupplier.Text <> "") Then Call oItemBO.AddLoadFilter(CMP_LESSEQUALTHAN, FID_INVENTORY_SupplierNo, txtEndSupplier.Text)
        End If
    End If
    Select Case (oLabelInfo.Size)
        Case ("S"): Call oItemBO.AddLoadFilter(CMP_GREATERTHAN, FID_INVENTORY_NoOfSmallLabels, "0")
        Case ("M"): Call oItemBO.AddLoadFilter(CMP_GREATERTHAN, FID_INVENTORY_NoOfMediumLabels, "0")
        Case ("L"): Call oItemBO.AddLoadFilter(CMP_GREATERTHAN, FID_INVENTORY_NoOfLargeLabels, "0")
    End Select
    Call oItemBO.LoadMatches
    Screen.MousePointer = vbHourglass
    Set colItems = oItemBO.LoadMatches
    
    If (colItems.Count > 0) Then ucpbProgress.Max = colItems.Count
    ucpbProgress.Caption1 = "Displaying Items"
    sprdSKUs.MaxRows = 0
    
    For lngRowNo = 1 To colItems.Count Step 1
        ucpbProgress.Value = lngRowNo
        Set oItemBO = colItems(lngRowNo)
                       
        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
        sprdSKUs.Row = sprdSKUs.MaxRows
        sprdSKUs.Col = COL_SKU
        sprdSKUs.Text = oItemBO.PartCode
        sprdSKUs.Col = COL_DESC
        sprdSKUs.Text = oItemBO.Description
        sprdSKUs.Col = COL_PRICE
        sprdSKUs.Text = oItemBO.NormalSellPrice + WEEECost(oItemBO.PRFSKU)
        sprdSKUs.Col = COL_WEEE
        sprdSKUs.Text = WEEECost(oItemBO.PRFSKU)
        sprdSKUs.Col = COL_SUPPLIER
        sprdSKUs.Text = oItemBO.SupplierNo & "-" & SupplierName(oItemBO.SupplierNo)
        sprdSKUs.Col = COL_SORT_SUPP
        sprdSKUs.Text = oItemBO.SupplierNo & "-" & SupplierName(oItemBO.SupplierNo)
        sprdSKUs.Col = COL_EQUIV_PR
        sprdSKUs.Text = oItemBO.EquivalentPriceMult
        sprdSKUs.Col = COL_UOM
        sprdSKUs.Text = oItemBO.EquivalentPriceUnit
        sprdSKUs.Col = COL_QTY
        Select Case (oLabelInfo.Size)
            Case ("S"): sprdSKUs.Value = oItemBO.NoOfSmallLabels
            Case ("M"): sprdSKUs.Value = oItemBO.NoOfMediumLabels
            Case ("L"): sprdSKUs.Value = oItemBO.NoOfLargeLabels
        End Select
        sprdSKUs.Col = COL_PRINT
        sprdSKUs.Value = 1
        Call DisplayPlanogram(oItemBO.PartCode)
    Next lngRowNo
    
    sprdSKUs.Col = -1
    sprdSKUs.Row = -1
    sprdSKUs.Lock = True
    sprdSKUs.Col = COL_QTY
    sprdSKUs.Lock = False
    
    sprdSKUs.ColWidth(COL_DESC) = sprdSKUs.MaxTextColWidth(COL_DESC)
    ucpbProgress.Visible = False
    Screen.MousePointer = vbNormal
    If (sprdSKUs.MaxRows = 0) Then
        Call MsgBoxEx("No SKUs retrieved for entered criteria!" & vbNewLine & "Check values entered!", vbOKOnly, "No SKUs Found", , , , , RGBMsgBox_WarnColour)
    Else
        Call cmbSortBy_Click
        sprdSKUs.Visible = True
        sprdSKUs.SetFocus
        cmdPrint.Visible = True
        cmdShowPG.Visible = True
        Call sprdSKUs.SetActiveCell(COL_QTY, 1)
    End If
    mblnHidden = False
    fraSortBy.Visible = True
End Sub

Private Sub chkAllSKUs_Click()

    If (chkAllSKUs.Value = 0) Then
        fraSKURange.Visible = True
    Else
        fraSKURange.Visible = False
        txtPCStartSKU.Text = ""
        txtPCEndSKU.Text = ""
    End If

End Sub

Private Sub chkAllSKUs_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If

End Sub

Private Sub cmbSortBy_Click()

Dim lngLineNo As Long
Dim vntPGLocs As Variant
Dim lngLocNo  As Integer
Dim strPlan   As String
Dim lngStart  As Long
Dim strLocRef As String
        
    sprdSKUs.ReDraw = False
    Screen.MousePointer = vbHourglass
    For lngLineNo = 1 To sprdSKUs.MaxRows Step 1
        Call sprdSKUs.RemoveCellSpan(COL_SKU, lngLineNo)
        Call sprdSKUs.RemoveCellSpan(COL_SUPPLIER, lngLineNo)
    Next lngLineNo
    'Group all info lines together
    sprdSKUs.SortKey(1) = COL_PRIME_PLAN
    sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdSKUs.Sort(-1, -1, -1, -1, SortByRow)
    'delete info lines
    lngLineNo = sprdSKUs.SearchCol(COL_PRIME_PLAN, 1, sprdSKUs.MaxRows, "", SearchFlagsGreaterOrEqual)
    If lngLineNo > 0 Then
        Call sprdSKUs.DeleteRows(lngLineNo, (sprdSKUs.MaxRows - lngLineNo) + 1)
        sprdSKUs.MaxRows = lngLineNo - 1
    End If
    'sort by selected criteria (SKU/Plangram
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 0) Or (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 2) Then
        If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 0) Then
            sprdSKUs.SortKey(1) = COL_SKU
            If (optEntryMode(OPT_ENTRY_PRICES).Value = True) Then
                sprdSKUs.SortKey(2) = COL_ID
                sprdSKUs.SortKeyOrder(2) = SortKeyOrderAscending
            End If
        Else
            If ((optEntryMode(OPT_ENTRY_SKU).Value = True) Or (optEntryMode(OPT_ENTRY_HHT).Value = True)) Then
                sprdSKUs.SortKey(1) = COL_ID
                sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
                sprdSKUs.SortKey(2) = COL_SKU
                sprdSKUs.SortKeyOrder(2) = SortKeyOrderAscending
            Else
                sprdSKUs.SortKey(1) = COL_SORT_SUPP
                sprdSKUs.SortKey(2) = COL_SKU
            End If
        End If
        sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdSKUs.Sort(-1, -1, -1, -1, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdSKUs.MaxRows To 1 Step -1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        sprdSKUs.Col = COL_SORT_LOC
                        sprdSKUs.Text = vntPGLocs(lngLocNo)
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        sprdSKUs.Col = COL_QTY
                        sprdSKUs.Value = ""
                        sprdSKUs.Col = COL_PRIME_LOC
                        sprdSKUs.Value = strLocRef
                        sprdSKUs.Col = COL_PRINT
                        sprdSKUs.CellType = CellTypeStaticText
                        sprdSKUs.Text = ""
                        sprdSKUs.Col = COL_APPLIED
                        sprdSKUs.CellType = CellTypeStaticText
                        sprdSKUs.Text = ""
                        If (lngStart = -1) Then lngStart = sprdSKUs.Row
                    Else
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        strLocRef = sprdSKUs.Text
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdSKUs.AddCellSpan(COL_SKU, lngStart, COL_PLANOGRAM - 1, sprdSKUs.Row - lngStart + 1)
                If (optEntryMode(OPT_ENTRY_PRICES).Value = True) Then
                    Call sprdSKUs.AddCellSpan(COL_SUPPLIER, lngStart, COL_APPLIED - COL_SUPPLIER + 1, sprdSKUs.Row - lngStart + 1)
                Else
                    Call sprdSKUs.AddCellSpan(COL_SUPPLIER, lngStart, COL_PRINT - COL_SUPPLIER + 1, sprdSKUs.Row - lngStart + 1)
                End If
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
                sprdSKUs.Lock = True
                sprdSKUs.Col = COL_SUPPLIER
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
            End If
        Next lngLineNo
    End If
    If (cmbSortBy.ItemData(cmbSortBy.ListIndex) = 1) Then
        sprdSKUs.SortKey(1) = COL_SKU
        sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdSKUs.Sort(-1, -1, -1, -1, SortByRow)
        
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdSKUs.MaxRows To 1 Step -1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        Call sprdSKUs.CopyRowRange(lngLineNo, lngLineNo, sprdSKUs.Row)
                        sprdSKUs.Col = COL_QTY
                        sprdSKUs.ForeColor = vbGrayText
                        sprdSKUs.Col = COL_PRIME_LOC
                        sprdSKUs.Value = strLocRef
                        sprdSKUs.Col = COL_PRINT
                        sprdSKUs.CellType = CellTypeStaticText
                        sprdSKUs.Text = ""
                        sprdSKUs.Col = COL_APPLIED
                        sprdSKUs.CellType = CellTypeStaticText
                        sprdSKUs.Text = ""
                        sprdSKUs.Col = COL_PRIME_PLAN
                        sprdSKUs.Text = ""
                    End If
                    sprdSKUs.Col = COL_SORT_LOC
                    sprdSKUs.Text = vntPGLocs(lngLocNo)
                    sprdSKUs.Col = COL_PLANOGRAM
                    sprdSKUs.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                    If (lngLocNo = 0) Then strLocRef = sprdSKUs.Text
                End If
            Next lngLocNo
        Next lngLineNo
        'Sort by Plan Gram Locations
        sprdSKUs.SortKey(1) = COL_SORT_LOC
        sprdSKUs.SortKeyOrder(1) = SortKeyOrderAscending
        Call sprdSKUs.Sort(-1, -1, -1, -1, SortByRow)
        'Step through lines and add lines for extra locations
        For lngLineNo = sprdSKUs.MaxRows To 1 Step -1
            sprdSKUs.Row = lngLineNo
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            sprdSKUs.Col = COL_SORT_LOC
            strPlan = sprdSKUs.Text
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (strPlan <> vntPGLocs(lngLocNo)) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = "  " & DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        sprdSKUs.Col = COL_PRINT
                        sprdSKUs.CellType = CellTypeStaticText
                        sprdSKUs.Col = COL_APPLIED
                        sprdSKUs.CellType = CellTypeStaticText
                        If (lngStart = -1) Then lngStart = sprdSKUs.Row
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdSKUs.AddCellSpan(COL_SKU, lngStart, COL_PLANOGRAM - 1, sprdSKUs.Row - lngStart + 1)
                If (optEntryMode(OPT_ENTRY_PRICES).Value = True) Then
                    Call sprdSKUs.AddCellSpan(COL_SUPPLIER, lngStart, COL_APPLIED - COL_SUPPLIER + 1, sprdSKUs.Row - lngStart + 1)
                Else
                    Call sprdSKUs.AddCellSpan(COL_SUPPLIER, lngStart, COL_PRINT - COL_SUPPLIER + 1, sprdSKUs.Row - lngStart + 1)
                End If
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
                sprdSKUs.Col = COL_SUPPLIER
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
            End If
        Next lngLineNo
    End If
    If (optEntryMode(OPT_ENTRY_SKU).Value = True) Then
        If (sprdSKUs.Visible = True) Then Call sprdSKUs.SetFocus
        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
        sprdSKUs.Col = COL_OTHER_PLANS
        sprdSKUs.Row = sprdSKUs.MaxRows
        Call sprdSKUs.SetActiveCell(COL_SKU, sprdSKUs.MaxRows)
    End If
    
    Screen.MousePointer = vbNormal
    sprdSKUs.ReDraw = True

End Sub

Private Sub cmbSortBy_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If
    If (KeyAscii = vbKeyEscape) Then
        KeyAscii = 0
        Call SendKeys("+{tab}")
    End If

End Sub

Private Sub cmdAllPlangram_Click()

    sprdPlangram.Col = COL_PG_RETRIEVE
    sprdPlangram.Row = -1
    sprdPlangram.Value = 1

End Sub

Private Sub cmdEnterSKUs_Click()

    cmdStockEnquiry.Visible = True
    
    sprdSKUs.MaxRows = 0
    sprdSKUs.Visible = True
    sprdSKUs.Col = -1
    sprdSKUs.Row = -1
    sprdSKUs.Lock = True
    sprdSKUs.Col = COL_SKU
    sprdSKUs.Lock = False
    fraSortBy.Visible = True
    If (sprdSKUs.MaxRows = 0) Then sprdSKUs.MaxRows = 1
    sprdSKUs.SetFocus
    Call sprdSKUs.SetActiveCell(COL_SKU, 1)
       
    sprdSKUs.EditMode = True

End Sub

Private Sub cmdExit_Click()

    End

End Sub

Private Sub cmdGetPriceChanges_Click()

    Call LoadPriceChanges

End Sub

Private Sub cmdGetPriceChanges_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If

End Sub

Private Sub cmdGetSuppItems_Click()

    Call LoadSupplier

End Sub

Private Sub cmdGetSuppItems_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If

End Sub

Private Sub cmdNoPlangram_Click()
    
    sprdPlangram.Col = COL_PG_RETRIEVE
    sprdPlangram.Row = -1
    sprdPlangram.Value = 0

End Sub

Private Sub cmdPGProcess_Click()

Dim oItemBO     As cStock_Wickes.cInventory
Dim oPlanGramBO As cStock_Wickes.cPlangram
Dim colPGrams   As Collection
Dim lngPGNo     As Long
Dim lngRowNo    As Long
Dim lngPlanNo   As Long
Dim oLabelInfo  As clsLabelInfo
Dim blnSkip     As Boolean
Dim lngPrintQty As Long
Dim lngSKUPos   As Long

Dim strPlanNo       As String
Dim strPlanSeg      As String
Dim strPlanShelf    As String

    Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
    
    Screen.MousePointer = vbHourglass
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving Planograms"
    
    sprdPlangram.SortKey(1) = COL_PG_RETRIEVE
    sprdPlangram.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdPlangram.Sort(-1, -1, -1, -1, SortByRow)
    
    lngPGNo = sprdPlangram.SearchCol(COL_PG_RETRIEVE, 1, sprdPlangram.MaxRows, 1, SearchFlagsGreaterOrEqual)
    
    If (lngPGNo = -1) Then
        Call MsgBoxEx("No Planogram locations selected for retrieval", vbInformation, "No Matches Found", , , , , RGBMSGBox_PromptColour)
        Screen.MousePointer = vbNormal
        If (sprdPlangram.Visible = True) Then Call sprdPlangram.SetFocus
        ucpbProgress.Visible = False
        Exit Sub
    End If
    
    ucpbProgress.Max = sprdPlangram.MaxRows - lngPGNo + 1
    ucpbProgress.Caption1 = "Displaying Items"
    sprdSKUs.MaxRows = 0
    
    For lngRowNo = lngPGNo To sprdPlangram.MaxRows Step 1
        blnSkip = False
        ucpbProgress.Value = lngRowNo - lngPGNo + 1
        sprdPlangram.Col = COL_PG_REF
        sprdPlangram.Row = lngRowNo
        strPlanNo = sprdPlangram.Text
        strPlanSeg = Mid$(strPlanNo, InStr(strPlanNo, "/") + 1)
        strPlanShelf = Mid$(strPlanSeg, InStr(strPlanSeg, "/") + 1)
        strPlanNo = Left$(strPlanNo, InStr(strPlanNo, "/") - 1)
        strPlanSeg = Left$(strPlanSeg, InStr(strPlanSeg, "/") - 1)
        Set oPlanGramBO = goDatabase.CreateBusinessObject(CLASSID_PLANGRAM)
        Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_PlanNumber, strPlanNo)
        Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_PlanSegment, strPlanSeg)
        Call oPlanGramBO.AddLoadFilter(CMP_EQUAL, FID_PLANGRAM_ShelfNumber, strPlanShelf)
        Set colPGrams = oPlanGramBO.LoadMatches
        For Each oPlanGramBO In colPGrams
            'Check if SKU alreday exists and if so, ignore and do not display
            lngSKUPos = sprdSKUs.SearchCol(COL_SKU, -1, sprdSKUs.MaxRows, oPlanGramBO.PartCode, SearchFlagsValue)
            sprdSKUs.Col = COL_SKU
            sprdSKUs.Row = lngSKUPos
            If (sprdSKUs.Text <> oPlanGramBO.PartCode) Then
            
                Set oItemBO = SetUpItem
                Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, oPlanGramBO.PartCode)
                
                Select Case (oLabelInfo.Size)
                    Case ("S"): Call oItemBO.AddLoadFilter(CMP_GREATERTHAN, FID_INVENTORY_NoOfSmallLabels, 0)
                    Case ("M"): Call oItemBO.AddLoadFilter(CMP_GREATERTHAN, FID_INVENTORY_NoOfMediumLabels, 0)
                    Case ("L"): Call oItemBO.AddLoadFilter(CMP_GREATERTHAN, FID_INVENTORY_NoOfLargeLabels, 0)
                End Select
                                
                Call oItemBO.LoadMatches
                                         
                Select Case (oLabelInfo.Size)
                    Case ("S"): lngPrintQty = oItemBO.NoOfSmallLabels
                    Case ("M"): lngPrintQty = oItemBO.NoOfMediumLabels
                    Case ("L"): lngPrintQty = oItemBO.NoOfLargeLabels
                End Select
                blnSkip = False
                                
                If (oItemBO.AutoApplyPriceChanges = True) Then blnSkip = True
                If (oItemBO.NonStockItem = True) And (oItemBO.QuantityAtHand = 0) And (oItemBO.MarkDownQuantity = 0) Then blnSkip = True
                If (oItemBO.ItemObsolete = True) And (oItemBO.QuantityAtHand = 0) And (oItemBO.MarkDownQuantity = 0) Then blnSkip = True
                If (oItemBO.PartCode <> oPlanGramBO.PartCode) Then blnSkip = True
                
                If (blnSkip = False) Then
                    sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                    sprdSKUs.Row = sprdSKUs.MaxRows
                    sprdSKUs.Col = COL_SKU
                    sprdSKUs.Text = oPlanGramBO.PartCode
                    sprdSKUs.Lock = True
                    sprdSKUs.Col = COL_DESC
                    sprdSKUs.Text = oItemBO.Description
                    sprdSKUs.Col = COL_PRICE
                    sprdSKUs.Text = oItemBO.NormalSellPrice + WEEECost(oItemBO.PRFSKU)
                    sprdSKUs.Col = COL_WEEE
                    sprdSKUs.Text = WEEECost(oItemBO.PRFSKU)
                    sprdSKUs.Col = COL_SUPPLIER
                    sprdSKUs.Text = oItemBO.SupplierNo & "-" & SupplierName(oItemBO.SupplierNo)
                    sprdSKUs.Col = COL_EQUIV_PR
                    sprdSKUs.Text = oItemBO.EquivalentPriceMult
                    sprdSKUs.Col = COL_UOM
                    sprdSKUs.Text = oItemBO.EquivalentPriceUnit
                    sprdSKUs.Col = COL_QTY
                    sprdSKUs.Text = lngPrintQty
                    sprdSKUs.Col = COL_ID
                    sprdSKUs.Col = COL_PRINT
                    sprdSKUs.Value = 1
                    Call DisplayPlanogram(oItemBO.PartCode)
                End If 'good sku to display
            End If 'SKU already displayed so dont repeat
        Next 'location to retrieve and display
    Next lngRowNo
    
    sprdPlangram.SortKey(1) = COL_PG_REF
    sprdPlangram.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdPlangram.Sort(-1, -1, -1, -1, SortByRow)
    
    sprdSKUs.ColWidth(COL_DESC) = sprdSKUs.MaxTextColWidth(COL_DESC)
    ucpbProgress.Visible = False
    If (sprdSKUs.MaxRows > 0) Then
        
        If (cmbSortBy.ListIndex = 1) Then
            Call cmbSortBy_Click
        Else
            cmbSortBy.ListIndex = 1
        End If
        
        Call Shrink_Plangram
        sprdSKUs.Visible = True
        cmdPrint.Visible = True
        cmdShowPG.Visible = True
        Call sprdSKUs.SetFocus
    Else
        Call MsgBoxEx("No SKUs found that match selected criteria", vbInformation, "No Matches Found", , , , , RGBMSGBox_PromptColour)
    End If
    
    Screen.MousePointer = vbNormal
    mblnHidden = False
    fraSortBy.Visible = True
End Sub

Private Sub cmdPrint_Click()

Dim lngRowNo    As Long
Dim lngTotal    As Long
Dim oFSO    As New FileSystemObject
Dim tsSKUs  As TextStream
Dim strFilePath As String
Dim lngCount    As Long
Dim strData     As String
Dim dteEffDate  As Date
Dim blnPrint    As Boolean

Dim strCurrPG   As String
Dim strThisPG   As String 'current Lines Planogram No and Segment
Dim lngSortBy   As Long

Dim oFiles      As Files
Dim oFile       As File

Dim oTempHHT    As cTempFil
Dim oLabelInfo  As clsLabelInfo
Dim oPriceBO    As cPriceChange
Dim strSKU      As String
Dim lngLineNo   As Long

Dim dblEquivFct As Double
Dim lngPrintQty As Long
Dim lngNoLabels As Long
Dim lngNoPages  As Long

    
    Call DebugMsg(MODULE_NAME, "cmdPrint_Click", endlTraceIn)
    'Preserve display order and make sure it is in Planagram order
    lngSortBy = cmbSortBy.ListIndex
    'For HHT and SKU Entry, allow the print order to be done in Planogram and Keyed Order
    If (optEntryMode(OPT_ENTRY_SKU).Value <> True) And (optEntryMode(OPT_ENTRY_HHT).Value <> True) Then
        If (cmbSortBy.ListIndex = 0) Then cmbSortBy.ListIndex = 1 'force SKU to Planagram
    Else
        If (cmbSortBy.ListIndex = 0) Then cmbSortBy.ListIndex = 2 'force SKU to Keyed order
    End If
    
    Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
        
    strFilePath = UCase(goSession.GetParameter(PRM_LABELS_PATH) & Format(goSession.CurrentEnterprise.IEnterprise_WorkstationID, "000") & "\" & Replace(UCase(oLabelInfo.FileName), "SGN", "TXT"))
    
    If (oFSO.FolderExists(goSession.GetParameter(PRM_LABELS_PATH)) = False) Then oFSO.CreateFolder (goSession.GetParameter(PRM_LABELS_PATH))
    If (oFSO.FolderExists(goSession.GetParameter(PRM_LABELS_PATH) & Format(goSession.CurrentEnterprise.IEnterprise_WorkstationID, "000")) = False) Then oFSO.CreateFolder (goSession.GetParameter(PRM_LABELS_PATH) & Format(goSession.CurrentEnterprise.IEnterprise_WorkstationID, "000"))

    
    'Copy latest label templates into folder
    Set oFiles = oFSO.GetFolder(goSession.GetParameter(PRM_LABELS_PATH)).Files
    For Each oFile In oFiles
        Call oFSO.CopyFile(oFile.path, goSession.GetParameter(PRM_LABELS_PATH) & Format(goSession.CurrentEnterprise.IEnterprise_WorkstationID, "000") & "\", True)
    Next
    
    Call DebugMsg(MODULE_NAME, "cmdPrint_Click", endlDebug, "Create Export File-" & strFilePath)
    Set tsSKUs = oFSO.CreateTextFile(strFilePath, True)
    Call tsSKUs.WriteLine("SEQN NO,SKU,DESCRIPTION,EAN,PRICE,PREV PRICE,EQUIV PRICE,EQUIV UNITS,OBSELETE,STOCK OFF SALE,FRAGILE,TIMBER,PACK QTY,PLANGRAM,PRIMARY PG,PRF")
    
    lngTotal = 0
    strCurrPG = ""
    For lngRowNo = 1 To sprdSKUs.MaxRows Step 1
        strData = ""
        sprdSKUs.Row = lngRowNo
        sprdSKUs.Col = COL_PRINT
        blnPrint = Val(sprdSKUs.Value)
        sprdSKUs.Col = COL_QTY
        lngPrintQty = Val(sprdSKUs.Value)
        If (blnPrint = True) And (lngPrintQty > 0) Then
            sprdSKUs.Col = COL_PLANOGRAM
            strThisPG = sprdSKUs.Text
            If (strThisPG = "N/A") Then strThisPG = "N/A/"
            strThisPG = Left$(strThisPG, InStr(InStr(strThisPG, "/") + 1, strThisPG, "/") - 1)
            If (strThisPG <> strCurrPG) Then
                strCurrPG = strThisPG
                If (cmbSortBy.ListIndex <> 2) Then  'Keyed In Order does not require location identifiers
                    lngTotal = lngTotal + 1
                    strData = strData & lngTotal & ",000000,,,0,0,0,"
                    strData = strData & ",,,,,," & Replace(sprdSKUs.Text, "-", " ") & ",,"
                    Call tsSKUs.WriteLine(strData)
                End If
                strData = ""
            End If
                        
            sprdSKUs.Col = COL_SKU
            strData = "," & sprdSKUs.Text
            sprdSKUs.Col = COL_DESC
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_EAN
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_PRICE
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_PREV_PR
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_EQUIV_PR
            dblEquivFct = Val(sprdSKUs.Text)
            If (dblEquivFct > 0) Then
                sprdSKUs.Col = COL_PRICE
                dblEquivFct = Val(sprdSKUs.Value) / dblEquivFct
                If (dblEquivFct < 1) Then
                    dblEquivFct = dblEquivFct + 0.000499
                    strData = strData & "," & Format(dblEquivFct, "0.000")
                Else
                    dblEquivFct = dblEquivFct + 0.00499
                    strData = strData & "," & Format(dblEquivFct, "0.00")
                End If
            Else
                strData = strData & ","
            End If
            sprdSKUs.Col = COL_UOM
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_OBS
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_STOCK_OFF
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_FRAGILE
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_TIMB
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_PACK
            strData = strData & "," & sprdSKUs.Text
            sprdSKUs.Col = COL_PLANOGRAM
            strData = strData & "," & Replace(sprdSKUs.Text, "-", " ")
            sprdSKUs.Col = COL_PRIME_LOC
            strData = strData & "," & Replace(sprdSKUs.Text, "-", " ")
            sprdSKUs.Col = COL_WEEE
            strData = strData & "," & Replace(sprdSKUs.Text, "-", " ")
            sprdSKUs.Col = COL_QTY
            For lngCount = 1 To Val(sprdSKUs.Value) Step 1
                lngTotal = lngTotal + 1
                Call tsSKUs.WriteLine(lngTotal & strData)
            Next lngCount
        Else
            'no quantity so check if Secondary Location holder
            sprdSKUs.Col = COL_PRIME_LOC
            If (sprdSKUs.Text <> "") Then 'has primary location so check if that has been printed
                sprdSKUs.Col = COL_SKU
                strSKU = sprdSKUs.Text
                sprdSKUs.Col = COL_PRIME_LOC
                lngLineNo = sprdSKUs.SearchCol(COL_PLANOGRAM, -1, lngRowNo, sprdSKUs.Text, SearchFlagsValue)
                
                lngLineNo = sprdSKUs.SearchCol(COL_SKU, lngLineNo - 1, lngRowNo, strSKU, SearchFlagsNone)
                
                sprdSKUs.Row = lngLineNo
                sprdSKUs.Col = COL_PRINT
                blnPrint = Val(sprdSKUs.Value)
                sprdSKUs.Col = COL_QTY
                lngPrintQty = Val(sprdSKUs.Value)
                If (blnPrint = True) And (lngPrintQty > 0) Then
                    sprdSKUs.Row = lngRowNo
                    'check if secondary location is in new Segment and print Location label if so
                    sprdSKUs.Col = COL_PLANOGRAM
                    strThisPG = Left$(sprdSKUs.Text, InStr(InStr(sprdSKUs.Text, "/") + 1, sprdSKUs.Text, "/") - 1)
                    If (strThisPG <> strCurrPG) Then
                        strCurrPG = strThisPG
                        If (cmbSortBy.ListIndex <> 2) Then 'Keyed In Order does not require location identifiers
                            lngTotal = lngTotal + 1
                            strData = lngTotal & ",000000,,,0,0,0,"
                            strData = strData & ",,,,,," & Replace(sprdSKUs.Text, "-", " ") & ",,"
                            Call tsSKUs.WriteLine(strData)
                        End If
                    End If
                                        
                    'Now print Secondary Location labels
                    sprdSKUs.Col = COL_SKU
                    strData = "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_DESC
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_EAN
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_PRICE
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_PREV_PR
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_EQUIV_PR
                    dblEquivFct = Val(sprdSKUs.Text)
                    If (dblEquivFct > 0) Then
                        sprdSKUs.Col = COL_PRICE
                        dblEquivFct = Val(sprdSKUs.Value) / dblEquivFct
                        If (dblEquivFct < 1) Then
                            dblEquivFct = dblEquivFct + 0.000499
                            strData = strData & "," & Format(dblEquivFct, "0.000")
                        Else
                            dblEquivFct = dblEquivFct + 0.00499
                            strData = strData & "," & Format(dblEquivFct, "0.00")
                        End If
                    Else
                        strData = strData & ","
                    End If
                    sprdSKUs.Col = COL_UOM
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_OBS
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_STOCK_OFF
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_FRAGILE
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_TIMB
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_PACK
                    strData = strData & "," & sprdSKUs.Text
                    sprdSKUs.Col = COL_PLANOGRAM
                    strData = strData & "," & Replace(sprdSKUs.Text, "-", " ")
                    sprdSKUs.Col = COL_PRIME_LOC
                    strData = strData & "," & Replace(sprdSKUs.Text, "-", " ")
                    sprdSKUs.Col = COL_WEEE
                    strData = strData & "," & Replace(sprdSKUs.Text, "-", " ")
                    For lngCount = 1 To lngPrintQty Step 1
                        lngTotal = lngTotal + 1
                        Call tsSKUs.WriteLine(lngTotal & strData)
                    Next lngCount
                End If 'Primary Location was selected for printing
            End If
        End If
    Next lngRowNo
    
    Call tsSKUs.Close
     
    'Swicth back to original Sorted Order
    If (lngSortBy <> 1) Then cmbSortBy.ListIndex = lngSortBy
         
    If (lngTotal = 0) Then
        Call MsgBoxEx("All SKUs in the list have had the print label flag deselected." & vbNewLine & "No labels will be printed.", vbOKOnly, "No Labels Selected", , , , , RGBMsgBox_WarnColour)
        Exit Sub
    End If
    
    'Added WIX Referral 2008-004 - display number of pages to print
    Call DebugMsg(MODULE_NAME, "cmdPrint_click", endlDebug, "Labels=" & oLabelInfo.Size & " Total=" & lngTotal)
    Select Case (oLabelInfo.Size)
        Case ("S"): lngNoPages = ((lngTotal - 1) \ mlngNoSmlLblPage) + 1
        Case ("M"): lngNoPages = ((lngTotal - 1) \ mlngNoMedLblPage) + 1
        Case ("L"): lngNoPages = ((lngTotal - 1) \ mlngNoLrgLblPage) + 1
    End Select
    Call MsgBoxEx("Number of Pages required : " & lngNoPages & " (" & lngTotal & " labels)", vbOKOnly, "Print labels", , , , , RGBMSGBox_PromptColour)
    
    CopyLabelsFile (strFilePath)
    
    Call DebugMsg(MODULE_NAME, "cmdPrint_Click", endlDebug, "Executing Print Program")
    Call ShellWait("C:\Program Files\Image Computer Systems\EnGine\Engine.exe " & Replace(strFilePath, ".TXT", ".SGN") & " /PD" & goSession.GetParameter(PRM_LABELS_PATH) & Format(goSession.CurrentEnterprise.IEnterprise_WorkstationID, "000") & " /PN" & Chr(34) & lblPrinter.Caption & Chr(34), SW_HIDE)
    
    If (oFSO.FileExists("C:\Program Files\Image Computer Systems\EnGine\Engine.LOG") = True) Then
        Set oFile = oFSO.GetFile("C:\Program Files\Image Computer Systems\EnGine\Engine.LOG")
        If (oFile.Size = 0) Then
            If (MsgBoxEx("Please confirm that labels have printed correctly.", vbYesNoCancel, "Confirm Labels Printed", , "No - Reprint", , , RGBMSGBox_PromptColour) = vbNo) Then
                Call cmdPrint_Click
                Exit Sub
            Else
                'If printed ok then flag labels as printed
                If (optEntryMode(OPT_ENTRY_HHT).Value = True) Then
                    Call DebugMsg(MODULE_NAME, "cmdPrint_Click", endlDebug, "Updating HHT Labels printed")
                    For lngRowNo = 1 To sprdSKUs.MaxRows Step 1
                        sprdSKUs.Row = lngRowNo
                        sprdSKUs.Col = COL_PRINT
                        blnPrint = Val(sprdSKUs.Value)
                        sprdSKUs.Col = COL_QTY
                        If (blnPrint = True) And (Val(sprdSKUs.Value) > 0) Then
                            Call DebugMsg(MODULE_NAME, "cmdPrint_Click", endlDebug, "Updating HHT Label")
                            sprdSKUs.Col = COL_HHT_POS
                            Set oTempHHT = mcolHHTLabels(Val(sprdSKUs.Value))
                            sprdSKUs.Col = COL_QTY
                            Call oTempHHT.UpdateLabelsPrinted(oLabelInfo.Size, Val(sprdSKUs.Value))
                            Call moTempFilBO.UpdateLabelsPrinted(oLabelInfo.Size, Val(sprdSKUs.Value))
                        End If
                    Next lngRowNo
                    Set moTempFilBO = Nothing
                End If
                If (optEntryMode(OPT_ENTRY_PRICES).Value = True) And (oLabelInfo.LabelType = "L") Then
                    For lngRowNo = 1 To sprdSKUs.MaxRows Step 1
                        Set oPriceBO = goDatabase.CreateBusinessObject(CLASSID_PRICECHANGE)
                        sprdSKUs.Row = lngRowNo
                        sprdSKUs.Col = COL_ID
                        If sprdSKUs.Text <> "" Then
                            dteEffDate = sprdSKUs.Text
                            sprdSKUs.Col = COL_SKU
                            Call oPriceBO.UpdateLabelsPrinted(sprdSKUs.Text, dteEffDate, oLabelInfo.Size)
                        End If
                    Next lngRowNo
                End If
                sprdSKUs.MaxRows = 0
                Call cmdReset_Click
            End If
        Else
            Set tsSKUs = oFSO.OpenTextFile("C:\Program Files\Image Computer Systems\EnGine\Engine.LOG", ForReading, False)
            Call MsgBoxEx("Error occurred when calling Print Engine" & vbNewLine & vbNewLine & tsSKUs.ReadAll & vbNewLine & "Call CTS for further assistance.", vbInformation, , , , , , RGBMsgBox_WarnColour)
        End If
    End If
    Call DebugMsg(MODULE_NAME, "cmdPrint_Click", endlTraceOut)

End Sub

Public Sub CopyLabelsFile(path As String)

On Error GoTo CopyLabelsFileErrHandler
    Dim fso As New FileSystemObject
    Dim pathCopy As String
    pathCopy = fso.GetParentFolderName(fso.GetParentFolderName(path))
    pathCopy = fso.BuildPath(pathCopy, "History")
    
    If Not fso.FolderExists(pathCopy) Then
    fso.CreateFolder pathCopy
    End If
    
    Dim priceIncDec As String
    
    If (optPriceChange(1).Value = True) Then
        priceIncDec = "_DEC"
    Else
        priceIncDec = "_INC"
    End If
    
    Dim newFileName As String
    newFileName = Replace(fso.GetFileName(path), "." + fso.GetExtensionName(path), "_" & Format$(Now, "yyyymmdd_hhmmss") & priceIncDec) & "." & fso.GetExtensionName(path)
    
    fso.CopyFile path, fso.BuildPath(pathCopy, newFileName)
    
Exit Sub

CopyLabelsFileErrHandler:
    Call MsgBoxEx("Unable to create copy of file for reprint" & vbCrLf & "Error: " & Err.Description, vbInformation, , , , , , RGBMsgBox_WarnColour)
    Call DebugMsg(MODULE_NAME, "CopyLabelsFile", endlDebug, "Error has occured during copying files for reprint process")
End Sub

Private Sub cmdPrinter_Click()
            
    Load frmPrinter
    Call frmPrinter.Show(vbModal)
    Unload frmPrinter
    lblPrinter.Caption = Printer.DeviceName
    
End Sub

Private Sub cmdReset_Click()
        
    If (sprdSKUs.Visible = True) And (sprdSKUs.MaxRows > 0) Then
        If (MsgBoxEx("Ignore SKU's selected for printing?", vbInformation + vbYesNo, "Reset Criteria", , , , , RGBMSGBox_PromptColour) = vbNo) Then Exit Sub
    End If
    Set moTempFilBO = Nothing
    lstFormats.Enabled = True
    Call lstFormats.SetFocus
    fraEntryMode.Visible = False
    cmdStockEnquiry.Visible = False
    fraSortBy.Visible = False
    sprdPlangram.Col = COL_PG_RETRIEVE
    sprdPlangram.Row = -1
    sprdPlangram.Value = 0
    sprdPlangram.TopRow = 1
    sprdSKUs.LeftCol = 1
    sprdSKUs.MaxRows = 0
    sprdSKUs.Visible = False
    cmdPrint.Visible = False
    cmdShowPG.Visible = False
    sbStatus.Panels(3).Text = "Select Label Type and press Enter"
    If (lstFormats.Enabled = True) Then Call lstFormats.SetFocus

End Sub

Private Sub cmdShowPG_Click()

Dim lngLineNo As Long

    Screen.MousePointer = vbHourglass
    sprdSKUs.ReDraw = False
    mblnHidden = Not mblnHidden
    For lngLineNo = 1 To sprdSKUs.MaxRows Step 1
        sprdSKUs.Col = COL_SKU
        sprdSKUs.Row = lngLineNo
        If (sprdSKUs.Text = "") Then sprdSKUs.RowHidden = mblnHidden
    Next lngLineNo
    sprdSKUs.ReDraw = True
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdStockEnquiry_Click()
    
    ucItemFind.ViewOnly = False
    ucItemFind.ItemEnquiryType = EnquiryType.etFullItems
    ucItemFind.Visible = True

    Call ucItemFind.FillScreen(ucItemFind)
  
    ucItemFind.Visible = False
    
End Sub

Public Sub ucItemFind_Apply(PartCode As String)
 
Dim currentRow As Integer
    
    For currentRow = 1 To sprdSKUs.MaxRows Step 1
        sprdSKUs.Row = currentRow
        sprdSKUs.Col = 1
        If sprdSKUs.Text = "" Then
            sprdSKUs.SetFocus
            sprdSKUs.EditMode = True
            sprdSKUs.Text = PartCode
            Call FillSKUDetails(COL_SKU, sprdSKUs.Row, 0, True)
            sprdSKUs.EditMode = False
            Exit For
        End If
    Next

End Sub

Private Sub ucItemFind_Cancel()

    DoEvents
    mstrSKU = ""

End Sub

Private Sub dtxtStartDate_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If

End Sub

Private Sub Form_Load()

Dim oRetOpt As cRetailOptions
Dim oSysDat As cSystemDates
Dim strPage As String
Dim vntPage As Variant
Dim lngOptNo As Long

    ucItemFind.Visible = False
    
    Call GetRoot
    sprdPlangram.MaxRows = 0
    
    If (USE_PLANGRAM = False) Then
        sprdSKUs.Col = COL_PLANOGRAM
        sprdSKUs.ColHidden = True
        optEntryMode(OPT_ENTRY_PLANGRAM).Visible = False
        optEntryMode(OPT_ENTRY_SKU).Top = optEntryMode(OPT_ENTRY_PLANGRAM).Top
    End If
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    Call sprdSKUs.SetOddEvenRowColor(RGB(224, 224, 224), vbBlack, vbWhite, vbBlack)
    
    lblPrinter.Caption = Printer.DeviceName
    mlngPlanagramSize = fraEntryMode.Height
    
    Call InitialiseStatusBar(sbStatus)
    
    strPage = goSession.GetParameter(PRM_LABELS_PAGE)
    vntPage = Split(strPage, ",")
    For lngOptNo = LBound(vntPage) To UBound(vntPage) Step 1
        Select Case (Left$(vntPage(lngOptNo), 1))
            Case ("S"): mlngNoSmlLblPage = Val(Mid(vntPage(lngOptNo), 2))
            Case ("M"): mlngNoMedLblPage = Val(Mid(vntPage(lngOptNo), 2))
            Case ("L"): mlngNoLrgLblPage = Val(Mid(vntPage(lngOptNo), 2))
        End Select
    Next lngOptNo
    
    Call LoadLabels
    
    If (goSession.GetParameter(PRM_USE_WEEE_RATES) = False) Then
        sprdSKUs.Col = COL_WEEE
        sprdSKUs.ColHidden = True
        sprdSKUs.Col = COL_PRICE
        sprdSKUs.Row = 0
        sprdSKUs.Text = "Price"
    End If
    
    Me.Show
    
    Set oRetOpt = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oRetOpt.LoadMatches
    
    Set oRetOpt = Nothing
    
    Set oSysDat = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oSysDat.LoadMatches
    
    mdteSystemDate = oSysDat.NextOpenDate
    mdteTodayDate = oSysDat.TodaysDate
    Set oSysDat = Nothing
    
    Call cmdReset_Click
    
End Sub

Private Sub Form_Resize()

    If (Me.WindowState = vbMinimized) Then Exit Sub
    If (Me.Height < 4300) Then
        Me.Height = 4300
        Exit Sub
    End If
    
    If (Me.Width < 4500) Then
        Me.Width = 4500
        Exit Sub
    End If
    
    sprdSKUs.Width = Me.Width - sprdSKUs.Left * 4
    sprdSKUs.Height = Me.Height - sprdSKUs.Top - cmdPrint.Height - 720 - sbStatus.Height
    cmdPrint.Top = sprdSKUs.Top + sprdSKUs.Height + 120
    cmdExit.Top = cmdPrint.Top
    cmdPrint.Left = sprdSKUs.Left + sprdSKUs.Width - cmdPrint.Width
    cmdShowPG.Top = cmdPrint.Top
    cmdShowPG.Left = fraPrinter.Left + fraPrinter.Width + 120
    fraPrinter.Top = cmdPrint.Top
    ucpbProgress.Left = (Me.Width - ucpbProgress.Width) / 2
    ucpbProgress.Top = (Me.Height - 480 - ucpbProgress.Height) / 2

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case (Shift)
        Case (0):
            Select Case (KeyCode)
                Case (vbKeyF2):
                    If (cmdStockEnquiry.Visible = True) Then Call cmdStockEnquiry_Click
                Case (vbKeyF3): 'if F3 then exit
                    Call cmdReset_Click
                Case (vbKeyF4):
                    If (cmdShowPG.Visible = True) Then cmdShowPG.Value = True
                Case (vbKeyF7): 'if F7 then check if any process buttons active
                    If (cmdPGProcess.Visible = True) Then cmdPGProcess.Value = True
                    If (cmdGetPriceChanges.Visible = True) Then cmdGetPriceChanges.Value = True
                    If (cmdGetSuppItems.Visible = True) Then cmdGetSuppItems.Value = True
                    KeyCode = 0
                Case (vbKeyF9):
                    If (cmdPrint.Visible = True) Then Call cmdPrint_Click
                Case (vbKeyF10): 'if F10 then exit
                    Call cmdExit_Click
            End Select 'Key pressed with no Shift/Alt combination
    End Select

End Sub

Private Sub DecodeParameters(strCommand As String)

Const TRAN_START_DATE    As String = "TS"
Const SELECTION_DATE_DB  As String = "DB"  ' This program called from Daily Banking
Const TRAN_END_DATE      As String = "TD"
Const TRAN_WEEKENDING    As String = "WE"
Const CALLED_FROM        As String = "CF"  'Added 22/10/04 v1.0.11
Const CF_CLOSE           As String = "C"   'Added 22/10/04 v1.0.11


Dim strTempDate  As String
Dim dteTempDate  As Date
Dim strSection   As String
Dim strSignFound As String
Dim vntSection   As Variant
Dim lngSectNo    As Long
Dim lngItem      As Long
Dim blnSetOpts   As Boolean

    vntSection = Split(strCommand, ",")
    
    blnSetOpts = False
    mblnFromNightlyClose = False
    
    For lngSectNo = 0 To UBound(vntSection) Step 1
        strSection = vntSection(lngSectNo)
        Select Case (Left$(strSection, 2))
            Case (CALLED_FROM):
                If Mid(strSection, 3) = CF_CLOSE Then
                    mblnFromNightlyClose = True
                    blnSetOpts = True
                End If
        End Select
    Next lngSectNo
    
    If (blnSetOpts = True) Then Call LoadPriceChanges

End Sub

Private Sub LoadLabels()

Const POS_NAME As Long = 0
Const POS_TYPE As Long = 1
Const POS_SIZE As Long = 2
Const POS_FILE As Long = 3

Dim oFSO    As New FileSystemObject
Dim tsTypes As TextStream
Dim strFilePath As String
Dim cLabels     As clsLabelInfo
Dim strData     As String
Dim vntFile     As Variant
Dim strStatus   As String

    On Error GoTo LoadLabels_Err
    
    strFilePath = goSession.GetParameter(910)
    
    strStatus = "Opening file - " & strFilePath & "LABELSTYPES.TXT"
    If (oFSO.FileExists(strFilePath & "LABELTYPES.TXT") = False) Then
        Call MsgBoxEx("Unable to locate Label Type Definition file - '" & strFilePath & "LABELTYPES.TXT'", vbInformation, "Missing Labels File", , , , , RGBMsgBox_WarnColour)
        End
    End If
    
    Set tsTypes = oFSO.OpenTextFile(strFilePath & "LABELTYPES.TXT", ForReading, False)
    
    While (Not tsTypes.AtEndOfStream)
        strData = Trim$(tsTypes.ReadLine)
        strStatus = "Processing '" & strData & "'"
        If (strData <> "") Then
            vntFile = Split(strData, vbTab)
            Set cLabels = New clsLabelInfo
            cLabels.DisplayName = vntFile(POS_NAME)
            cLabels.FileName = vntFile(POS_FILE)
            cLabels.LabelType = vntFile(POS_TYPE)
            cLabels.Size = vntFile(POS_SIZE)
            Call lstFormats.AddItem(cLabels.DisplayName)
            Call mcolLabels.Add(cLabels)
            lstFormats.ItemData(lstFormats.NewIndex) = mcolLabels.Count
        End If
    Wend
    
    Call tsTypes.Close
    strStatus = "Labels Extracted"
    
    If (lstFormats.ListCount > 0) Then lstFormats.ListIndex = 0
    
    Exit Sub
    
LoadLabels_Err:

    Call MsgBoxEx("Error has occurred when loading Label Configuration File(" & strStatus & ")" & vbNewLine & "System will exit", vbInformation, "Error Occurred", , , , , RGBMsgBox_WarnColour)
    End
    
End Sub

Private Sub lstFormats_DblClick()

    Call lstFormats_KeyPress(vbKeyReturn)
    
End Sub

Private Sub lstFormats_KeyPress(KeyAscii As Integer)

Dim oLabelInfo  As clsLabelInfo
    
    If (lstFormats.ListIndex <> -1) And (KeyAscii = vbKeyReturn) Then
        sbStatus.Panels(3).Text = ""
        Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
        fraEntryMode.Visible = True
        optEntryMode(OPT_ENTRY_SKU).Value = True
        Call optEntryMode(OPT_ENTRY_SKU).SetFocus
        If (oLabelInfo.LabelType = "L") Then 'if Labels required the show applicable modes
            optEntryMode(OPT_ENTRY_HHT).Visible = True
            optEntryMode(OPT_ENTRY_PRICES).Visible = True
        Else
            optEntryMode(OPT_ENTRY_HHT).Visible = False
            optEntryMode(OPT_ENTRY_PRICES).Visible = False
        End If
        lstFormats.Enabled = False
    End If
    
End Sub

Private Sub optEntryMode_Click(Index As Integer)

    sprdSKUs.Visible = False
    sprdSKUs.Col = COL_APPLIED
    sprdSKUs.ColHidden = True
    cmdEnterSKUs.Visible = False
    cmdStockEnquiry.Visible = False
    fraSupplier.Visible = False
    fraPriceChanges.Visible = False
    fraHHTHeaders.Visible = False
    fraPlangram.Visible = False
    sprdSKUs.MaxRows = 0
    cmdPrint.Visible = False
    cmdShowPG.Visible = False
    Call ClearSortBy
    Select Case (Index)
        Case (OPT_ENTRY_SKU):
            cmdEnterSKUs.Visible = True
            cmbSortBy.ListIndex = 0
            Call cmbSortBy.AddItem("Keyed")
            cmbSortBy.ItemData(cmbSortBy.NewIndex) = 2
            cmbSortBy.ListIndex = cmbSortBy.NewIndex
            Call Shrink_Plangram
        Case (OPT_ENTRY_PLANGRAM):
            sprdPlangram.Row = -1
            sprdPlangram.Col = COL_PG_RETRIEVE
            sprdPlangram.Value = 0
            fraPlangram.Visible = True
            cmbSortBy.ListIndex = 1
            If (sprdPlangram.Enabled = True) And (sprdPlangram.MaxRows = 0) Then Call LoadPlanGram
            Call Expand_Plangram
        Case (OPT_ENTRY_SUPPLIER):
            fraSupplier.Visible = True
            txtStartSupplier.Text = ""
            txtEndSupplier.Text = ""
            Call cmbSortBy.AddItem("Supplier")
            cmbSortBy.ItemData(cmbSortBy.NewIndex) = 2
            cmbSortBy.ListIndex = cmbSortBy.NewIndex
            Call Shrink_Plangram
        Case (OPT_ENTRY_PRICES):
            optPriceChange(0).Value = 1
            dtxtStartDate.Text = DisplayDate(DateAdd("d", 7, mdteSystemDate), False)
            txtPCStartSKU.Text = ""
            txtPCEndSKU.Text = ""
            chkAllSKUs.Value = 1
            fraPriceChanges.Visible = True
            chkApplied.Value = 0
            Call Shrink_Plangram
        Case (OPT_ENTRY_HHT):
            fraHHTHeaders.Visible = True
            Call LoadHHTHeader
            Call cmbSortBy.AddItem("Keyed")
            cmbSortBy.ItemData(cmbSortBy.NewIndex) = 2
            cmbSortBy.ListIndex = cmbSortBy.NewIndex
            Call Shrink_Plangram
    End Select
    
End Sub

Private Sub LoadHHTHeader()

Const ROW_SMALL = 3
Const ROW_MEDIUM = 4
Const ROW_LARGE = 5

Dim lngNoSmlReq     As Long
Dim lngNoSmlDone    As Long
Dim lngNoMedReq     As Long
Dim lngNoMedDone    As Long
Dim lngNoLrgReq     As Long
Dim lngNoLrgDone    As Long

Dim oItemBO     As cStock_Wickes.cInventory
Dim oHHTLabel   As cTempFil
Dim lngRowNo    As Long
Dim oLabelInfo  As clsLabelInfo

Dim HHTLabelImplementationFactory As HHTLabelCommon.HHTLabelFactory
Dim HHTLabelImplementation As HHTLabelCommon.IHHTLabel
    
    ucpbProgress.Visible = True
    ucpbProgress.Value = 0
    ucpbProgress.Caption1 = "Retrieving Items"
    
    Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
    
    If (moTempFilBO Is Nothing) Then
        Screen.MousePointer = vbHourglass
        Set moTempFilBO = goDatabase.CreateBusinessObject(CLASSID_TEMPFIL)
        Call LocalLoadLabelPrintHeader(lngNoSmlReq, lngNoMedReq, lngNoLrgReq, lngNoSmlDone, lngNoMedDone, lngNoLrgDone)
        sprdHHT.Col = 2
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = lngNoSmlReq
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = lngNoMedReq
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = lngNoLrgReq
        sprdHHT.Col = 3
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = 0
        If (lngNoSmlReq > 0) Then sprdHHT.Text = ((lngNoSmlReq - 1) \ mlngNoSmlLblPage) + 1
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = 0
        If (lngNoMedReq > 0) Then sprdHHT.Text = ((lngNoMedReq - 1) \ mlngNoMedLblPage) + 1
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = 0
        If (lngNoLrgReq > 0) Then sprdHHT.Text = ((lngNoLrgReq - 1) \ mlngNoLrgLblPage) + 1
        sprdHHT.Col = 4
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = lngNoSmlDone
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = lngNoMedDone
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = lngNoLrgDone
        sprdHHT.Col = 5
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = 0
        If (lngNoSmlDone > 0) Then sprdHHT.Text = ((lngNoSmlDone - 1) \ mlngNoSmlLblPage) + 1
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = 0
        If (lngNoMedDone > 0) Then sprdHHT.Text = ((lngNoMedDone - 1) \ mlngNoMedLblPage) + 1
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = 0
        If (lngNoLrgDone > 0) Then sprdHHT.Text = ((lngNoLrgDone - 1) \ mlngNoLrgLblPage) + 1
        sprdHHT.Col = 6
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = lngNoSmlReq - lngNoSmlDone
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = lngNoMedReq - lngNoMedDone
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = lngNoLrgReq - lngNoLrgDone
        sprdHHT.Col = 7
        sprdHHT.Row = ROW_SMALL
        sprdHHT.Text = 0
        If (lngNoSmlReq - lngNoSmlDone > 0) Then sprdHHT.Text = ((lngNoSmlReq - lngNoSmlDone - 1) \ mlngNoSmlLblPage) + 1
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.Text = 0
        If (lngNoMedReq - lngNoMedDone > 0) Then sprdHHT.Text = ((lngNoMedReq - lngNoMedDone - 1) \ mlngNoMedLblPage) + 1
        sprdHHT.Row = ROW_LARGE
        sprdHHT.Text = 0
        If (lngNoLrgReq - lngNoLrgDone > 0) Then sprdHHT.Text = ((lngNoLrgReq - lngNoLrgDone - 1) \ mlngNoLrgLblPage) + 1
        'Referral WIX2008-004 show only selected label size info
        sprdHHT.Row = ROW_SMALL
        sprdHHT.RowHidden = True
        sprdHHT.Row = ROW_MEDIUM
        sprdHHT.RowHidden = True
        sprdHHT.Row = ROW_LARGE
        sprdHHT.RowHidden = True
        Select Case (oLabelInfo.Size)
            Case ("S"): sprdHHT.Row = ROW_SMALL
                        sprdHHT.RowHidden = False
            Case ("M"): sprdHHT.Row = ROW_MEDIUM
                        sprdHHT.RowHidden = False
            Case ("L"): sprdHHT.Row = ROW_LARGE
                        sprdHHT.RowHidden = False
        End Select
    End If
        
    If (mcolHHTLabels.Count > 0) Then ucpbProgress.Max = mcolHHTLabels.Count
    ucpbProgress.Caption1 = "Displaying Items"
    sprdSKUs.MaxRows = 0
    
    Set HHTLabelImplementationFactory = New HHTLabelCommon.HHTLabelFactory
    Call HHTLabelImplementationFactory.Initialise(goSession)
    Set HHTLabelImplementation = HHTLabelImplementationFactory.GetImplementation
    
    For lngRowNo = 1 To mcolHHTLabels.Count Step 1
        ucpbProgress.Value = lngRowNo
        Set oHHTLabel = mcolHHTLabels(lngRowNo)
        If (oHHTLabel.NoLabels(oLabelInfo.Size, True) > 0) Then 'Labels in selected size requested
            If (oHHTLabel.NoLabels(oLabelInfo.Size, True) > oHHTLabel.NoLabels(oLabelInfo.Size, False)) Then 'Labels in selected size requested but not all printed
                Set oItemBO = SetUpItem
                Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, HHTLabelImplementation.GetSkuFromKeyData(oHHTLabel.KeyData))
                Call oItemBO.AddLoadField(FID_INVENTORY_EAN)
                Call oItemBO.LoadMatches
                        
                sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                sprdSKUs.Row = sprdSKUs.MaxRows
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Text = oItemBO.PartCode
                sprdSKUs.Lock = True
                sprdSKUs.Col = COL_DESC
                sprdSKUs.Text = oItemBO.Description
                sprdSKUs.Col = COL_PRICE
                sprdSKUs.Text = oItemBO.NormalSellPrice + WEEECost(oItemBO.PRFSKU)
                sprdSKUs.Col = COL_WEEE
                sprdSKUs.Text = WEEECost(oItemBO.PRFSKU)
                sprdSKUs.Col = COL_SUPPLIER
                sprdSKUs.Text = oItemBO.SupplierNo & "-" & SupplierName(oItemBO.SupplierNo)
                sprdSKUs.Col = COL_EQUIV_PR
                sprdSKUs.Text = oItemBO.EquivalentPriceMult
                sprdSKUs.Col = COL_UOM
                sprdSKUs.Text = oItemBO.EquivalentPriceUnit
                sprdSKUs.Col = COL_QTY
                sprdSKUs.Text = oHHTLabel.NoLabels(oLabelInfo.Size, True)
                sprdSKUs.Col = COL_ID
                sprdSKUs.Text = Format(oHHTLabel.Id, "00000000")
                sprdSKUs.Col = COL_HHT_POS
                sprdSKUs.Text = lngRowNo 'flag which BO pos in collection to update
                sprdSKUs.Col = COL_PRINT
                sprdSKUs.Value = 1
                Call DisplayPlanogram(oItemBO.PartCode)
            End If
        End If
    Next lngRowNo
    
    sprdSKUs.ColWidth(COL_DESC) = sprdSKUs.MaxTextColWidth(COL_DESC)
    ucpbProgress.Visible = False
    If (sprdSKUs.MaxRows > 0) Then
        sprdSKUs.Visible = True
        cmdPrint.Visible = True
        cmdShowPG.Visible = True
        fraSortBy.Visible = True
        sprdSKUs.SetFocus
        
        If (cmbSortBy.ListIndex = 0) Then
            Call cmbSortBy_Click
        Else
            cmbSortBy.ListIndex = 0
        End If
    Else
        Call MsgBoxEx("There are no labels of the requested size to print!" & vbNewLine & "Select another label size.", vbInformation, "No Labels to print", , , , , RGBMsgBox_WarnColour)
        Call cmdReset_Click
    End If
    mblnHidden = False
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub optEntryMode_KeyPress(Index As Integer, KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        Call cmdReset_Click
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Select Case (Index)
            Case (OPT_ENTRY_SKU):
                Call cmdEnterSKUs_Click
            Case (OPT_ENTRY_PLANGRAM):
                Call sprdPlangram.SetFocus
            Case (OPT_ENTRY_SUPPLIER):
                txtStartSupplier.SetFocus
            Case (OPT_ENTRY_PRICES):
                optPriceChange(0).SetFocus
            Case (OPT_ENTRY_HHT):
                fraSupplier.Visible = False
                fraPriceChanges.Visible = False
        End Select
    End If
    
End Sub

Private Sub optPriceChange_KeyPress(Index As Integer, KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If
    
End Sub

Private Sub sprdPlangram_Click(ByVal Col As Long, ByVal Row As Long)

    If (sprdPlangram.EditMode = False) And (Row > 0) Then
        Call sprdPlangram.SetActiveCell(COL_PG_RETRIEVE, Row)
        sprdPlangram.EditMode = True
        sprdPlangram.Col = COL_PG_RETRIEVE
        sprdPlangram.Row = Row
        sprdPlangram.Value = Not sprdPlangram.Value
    End If
    
End Sub

Private Sub sprdPlangram_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Or (KeyAscii = vbKeySpace) Then
        sprdPlangram.Col = COL_PG_RETRIEVE
        sprdPlangram.Row = sprdPlangram.SelBlockRow
        If (sprdPlangram.Value = "1") Then
            sprdPlangram.Value = "0"
        Else
            sprdPlangram.Value = "1"
        End If
    End If
    
End Sub

Private Sub Expand_Plangram()

    fraEntryMode.Height = fraPrinter.Top - 60 - fraEntryMode.Top
    fraPlangram.Height = fraEntryMode.Height - (fraPlangram.Top * 2)
    sprdPlangram.Height = fraPlangram.Height - fraPGButtons.Height - 180 - fraPlangram.Top
    fraPGButtons.Top = sprdPlangram.Top + sprdPlangram.Height + 60

End Sub

Private Sub Shrink_Plangram()

    fraEntryMode.Height = mlngPlanagramSize
    fraPlangram.Height = fraEntryMode.Height - (fraPlangram.Top * 2)
    sprdPlangram.Height = fraPlangram.Height - fraPGButtons.Height - 180 - fraPlangram.Top
    fraPGButtons.Top = sprdPlangram.Top + sprdPlangram.Height + 60
    
End Sub

Private Sub sprdSKUs_Click(ByVal Col As Long, ByVal Row As Long)

    If (sprdSKUs.EditMode = False) And (Row > 0) Then
        Call sprdSKUs.SetActiveCell(COL_PRINT, Row)
        sprdSKUs.EditMode = True
        sprdSKUs.Col = COL_PRINT
        sprdSKUs.Row = Row
        If (sprdSKUs.CellType = CellTypeCheckBox) Then sprdSKUs.Value = Not sprdSKUs.Value
    End If
    
End Sub

Private Sub sprdSKUs_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

    Call FillSKUDetails(Col, Row, Mode, ChangeMade)
    
End Sub

Private Sub FillSKUDetails(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)

Dim oItemBO     As cInventory
Dim oLabelInfo  As clsLabelInfo
Dim vntPGLocs   As Variant
Dim lngStart As Long
Dim lngLocNo    As Long
Dim lngLineNo   As Long
Dim lngStartRow As Long

    If (Mode = 0) And (ChangeMade = True) And (Col = COL_SKU) Then
        sprdSKUs.Col = COL_SKU
        sprdSKUs.Row = Row
        
        If sprdSKUs.Text = "" Then
            sprdSKUs.Col = COL_DESC
            sprdSKUs.Text = ""
            sprdSKUs.Col = COL_PRICE
            sprdSKUs.Text = ""
            sprdSKUs.Col = COL_WEEE
            sprdSKUs.Text = ""
            sprdSKUs.Col = COL_SUPPLIER
            sprdSKUs.Text = ""
            sprdSKUs.Col = COL_QTY
            sprdSKUs.Text = ""
            sprdSKUs.Col = COL_PRINT
            sprdSKUs.Text = 0
            If (sprdSKUs.Row <> sprdSKUs.MaxRows) Then
                sprdSKUs.Col = COL_SKU
                lngStart = 0
                For lngLineNo = sprdSKUs.Row To sprdSKUs.MaxRows - 1 Step 1
                    sprdSKUs.Row = lngLineNo
                    If (sprdSKUs.Text = "") Then
                        lngStart = lngStart + 1
                    Else
                        Exit For
                    End If
                Next lngLineNo
                Call sprdSKUs.DeleteRows(Row, lngStart)
                sprdSKUs.MaxRows = sprdSKUs.MaxRows - lngStart
            End If
        Else
            Set oLabelInfo = mcolLabels(lstFormats.ItemData(lstFormats.ListIndex))
            Set oItemBO = SetUpItem
            Call oItemBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, sprdSKUs.Text)
            
            lngLineNo = oItemBO.LoadMatches.Count
            If (lngLineNo = 0) Or (IsLabelledSKU(oItemBO.PartCode) = False) Then
                If (lngLineNo = 0) Then
                    Call MsgBoxEx("SKU - '" & sprdSKUs.Text & "' not found!" & vbNewLine & "SKU not added to list", vbInformation, "SKU not found", , , , , RGBMsgBox_WarnColour)
                Else
                    Call MsgBoxEx("SKU - '" & sprdSKUs.Text & ":" & oItemBO.Description & "' does not have Labels!" & vbNewLine & "SKU not added to list", vbInformation, "SKU not valid", , , , , RGBMsgBox_WarnColour)
                End If
                Call sprdSKUs.SetActiveCell(COL_SKU, Row)
                sprdSKUs.Col = COL_DESC
                sprdSKUs.Text = ""
                sprdSKUs.Col = COL_PRICE
                sprdSKUs.Text = ""
                sprdSKUs.Col = COL_WEEE
                sprdSKUs.Text = ""
                sprdSKUs.Col = COL_SUPPLIER
                sprdSKUs.Text = ""
                sprdSKUs.Col = COL_QTY
                sprdSKUs.Text = ""
                sprdSKUs.Col = COL_PRINT
                sprdSKUs.Text = 0
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Text = ""
                If (sprdSKUs.Row <> sprdSKUs.MaxRows) Then
                    sprdSKUs.Col = COL_SKU
                    lngStart = 0
                    For lngLineNo = sprdSKUs.Row To sprdSKUs.MaxRows - 1 Step 1
                        sprdSKUs.Row = lngLineNo
                        If (sprdSKUs.Text = "") Then
                            lngStart = lngStart + 1
                        Else
                            Exit For
                        End If
                    Next lngLineNo
                    Call sprdSKUs.DeleteRows(Row, lngStart)
                    sprdSKUs.MaxRows = sprdSKUs.MaxRows - lngStart
                End If
                Exit Sub
            End If
                    
            sprdSKUs.Row = Row
            sprdSKUs.Col = COL_DESC
            If (sprdSKUs.Row <> sprdSKUs.MaxRows) And (sprdSKUs.Text <> "") Then
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = sprdSKUs.Row + 1
                lngStart = 0
                For lngLineNo = sprdSKUs.Row To sprdSKUs.MaxRows - 1 Step 1
                    sprdSKUs.Row = lngLineNo
                    If (sprdSKUs.Text = "") Then
                        lngStart = lngStart + 1
                    Else
                        Exit For
                    End If
                Next lngLineNo
                Call sprdSKUs.DeleteRows(Row + 1, lngStart)
                sprdSKUs.MaxRows = sprdSKUs.MaxRows - lngStart
            End If
            
            sprdSKUs.Col = COL_SKU
            sprdSKUs.Text = oItemBO.PartCode
            sprdSKUs.Col = COL_DESC
            sprdSKUs.Text = oItemBO.Description
            sprdSKUs.Col = COL_PRICE
            sprdSKUs.Text = oItemBO.NormalSellPrice + WEEECost(oItemBO.PRFSKU)
            sprdSKUs.Col = COL_WEEE
            sprdSKUs.Text = WEEECost(oItemBO.PRFSKU)
            sprdSKUs.Col = COL_SUPPLIER
            sprdSKUs.Text = oItemBO.SupplierNo & "-" & SupplierName(oItemBO.SupplierNo)
            sprdSKUs.Col = COL_EQUIV_PR
            sprdSKUs.Text = oItemBO.EquivalentPriceMult
            sprdSKUs.Col = COL_UOM
            sprdSKUs.Text = oItemBO.EquivalentPriceUnit
            sprdSKUs.Col = COL_ID
            sprdSKUs.Text = Format(sprdSKUs.MaxRows, "00000000")
            sprdSKUs.ColWidth(COL_DESC) = sprdSKUs.MaxTextColWidth(COL_DESC)
            If (sprdSKUs.ColWidth(COL_DESC) < 30) Then sprdSKUs.ColWidth(COL_DESC) = 30
            sprdSKUs.Col = COL_QTY
            Select Case (oLabelInfo.Size)
                Case ("S"): sprdSKUs.Value = oItemBO.NoOfSmallLabels
                Case ("M"): sprdSKUs.Value = oItemBO.NoOfMediumLabels
                Case ("L"): sprdSKUs.Value = oItemBO.NoOfLargeLabels
            End Select
            If (Val(sprdSKUs.Value) = 0) Then sprdSKUs.Value = 1
            sprdSKUs.Lock = False
            lngStartRow = sprdSKUs.Row
            Call DisplayPlanogram(oItemBO.PartCode)
            
            sprdSKUs.Col = COL_OTHER_PLANS
            vntPGLocs = Split(sprdSKUs.Text, "##")
            lngStart = -1
            For lngLocNo = LBound(vntPGLocs) To UBound(vntPGLocs) Step 1
                If (vntPGLocs(lngLocNo) <> "") Then 'stragler from list - just ignore
                    If (lngLocNo > 0) Then
                        sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                        Call sprdSKUs.InsertRows(sprdSKUs.Row + 1, 1)
                        sprdSKUs.Row = sprdSKUs.Row + 1
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                        sprdSKUs.Col = COL_QTY
                        sprdSKUs.Value = ""
                        sprdSKUs.Lock = True
                        sprdSKUs.Col = COL_PRINT
                        sprdSKUs.CellType = CellTypeStaticText
                        sprdSKUs.Text = ""
                        If (lngStart = -1) Then lngStart = sprdSKUs.Row
                    Else
                        sprdSKUs.Col = COL_PLANOGRAM
                        sprdSKUs.Text = DecodeLocation(CStr(vntPGLocs(lngLocNo)))
                    End If
                End If
            Next lngLocNo
            If (lngStart <> -1) Then
                Call sprdSKUs.AddCellSpan(COL_SKU, lngStart, COL_PLANOGRAM - 1, sprdSKUs.Row - lngStart + 1)
                Call sprdSKUs.AddCellSpan(COL_SUPPLIER, lngStart, COL_PRINT - COL_SUPPLIER + 1, sprdSKUs.Row - lngStart + 1)
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
                sprdSKUs.Lock = True
                sprdSKUs.Col = COL_SUPPLIER
                sprdSKUs.Row = lngStart
                sprdSKUs.BackColor = vbWhite
            End If
            sprdSKUs.Row = Row
            Call sprdSKUs.SetActiveCell(COL_QTY, Row)
            sprdSKUs.EditMode = True
        End If
    End If
    If (Mode = 0) And (ChangeMade = False) And (Col = COL_SKU) Then
        sprdSKUs.Col = COL_SKU
        sprdSKUs.Row = Row
        If sprdSKUs.Text = "" Then
            Call sprdSKUs.SetActiveCell(COL_SKU, Row)
        End If
    End If
    If (Mode = 0) And (Col = COL_QTY) Then
        sprdSKUs.Row = Row
        sprdSKUs.Col = COL_QTY
        If (Val(sprdSKUs.Text) > goSession.GetParameter(200)) Then
            If (MsgBoxEx("Entered Print label quantity of (" & sprdSKUs.Text & ") is High" & vbNewLine & "Confirm quantity is correct", vbYesNo + vbDefaultButton2, "High quantity entered", , , , , vbRed) = vbNo) Then
                sprdSKUs.Value = 0
                Exit Sub
            End If
        End If
        cmdPrint.Visible = True
        cmdShowPG.Visible = True
        If (optEntryMode(OPT_ENTRY_SKU).Value = True) Then
            sprdSKUs.Row = Row
            sprdSKUs.Col = COL_PRINT
            sprdSKUs.Value = 1
            If (Row = sprdSKUs.MaxRows) Then
                sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                sprdSKUs.Col = COL_SKU
                sprdSKUs.Row = Row
                Call sprdSKUs.SetActiveCell(COL_SKU, sprdSKUs.MaxRows)
            Else
                For lngLocNo = Row + 1 To sprdSKUs.MaxRows Step 1
                    sprdSKUs.Col = COL_PRIME_PLAN
                    sprdSKUs.Row = lngLocNo
                    If (Val(sprdSKUs.Text) <> 0) Then Exit For
                Next lngLocNo
                If (Val(sprdSKUs.Text) = 0) Then
                    sprdSKUs.MaxRows = sprdSKUs.MaxRows + 1
                    sprdSKUs.Row = sprdSKUs.MaxRows
                End If
                sprdSKUs.Col = COL_SKU
                Call sprdSKUs.SetActiveCell(COL_SKU, sprdSKUs.Row)
                sprdSKUs.EditMode = True
            End If
        End If
    End If

End Sub

Private Sub sprdSKUs_KeyPress(KeyAscii As Integer)

    If ((KeyAscii = vbKeyReturn) Or (KeyAscii = vbKeySpace)) And (optEntryMode(OPT_ENTRY_SKU).Value = False) And (sprdSKUs.EditMode = False) Then
        sprdSKUs.Col = COL_PRINT
        sprdSKUs.Row = sprdSKUs.SelBlockRow
        If (sprdSKUs.CellType = CellTypeCheckBox) And (sprdSKUs.Row > 0) Then sprdSKUs.Value = Abs(sprdSKUs.Value - 1)
    End If

    If ((KeyAscii >= 48) And (KeyAscii <= 57)) And (optEntryMode(OPT_ENTRY_SKU).Value = False) And (sprdSKUs.EditMode = False) Then
        sprdSKUs.Col = COL_QTY
        sprdSKUs.Row = sprdSKUs.SelBlockRow
        If (sprdSKUs.CellType = CellTypeNumber) And (sprdSKUs.Row > 0) Then
            Call sprdSKUs.SetActiveCell(COL_QTY, sprdSKUs.Row)
            sprdSKUs.EditMode = True
            DoEvents
        End If
    End If

End Sub

Private Sub txtEndSupplier_GotFocus()

    txtEndSupplier.SelStart = 0
    txtEndSupplier.SelLength = Len(txtEndSupplier.Text)

End Sub

Private Sub txtEndSupplier_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If

End Sub

Private Sub txtPCEndSKU_GotFocus()
    
    txtPCEndSKU.SelStart = 0
    txtPCEndSKU.SelLength = Len(txtPCEndSKU.Text)

End Sub

Private Sub txtPCEndSKU_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If

End Sub

Private Sub txtPCStartSKU_GotFocus()

    txtPCStartSKU.SelStart = 0
    txtPCStartSKU.SelLength = Len(txtPCStartSKU.Text)

End Sub

Private Sub txtPCStartSKU_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyEscape) Then
        Call SendKeys("+{tab}")
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If

End Sub

Private Sub txtStartSupplier_GotFocus()
    
    txtStartSupplier.SelStart = 0
    txtStartSupplier.SelLength = Len(txtStartSupplier.Text)

End Sub

Private Sub txtStartSupplier_KeyPress(KeyAscii As Integer)
    
    If (KeyAscii = vbKeyEscape) Then
        optEntryMode(OPT_ENTRY_SUPPLIER).SetFocus
        KeyAscii = 0
    End If
    If (KeyAscii = vbKeyReturn) Then
        Call SendKeys(vbTab)
        KeyAscii = 0
    End If

End Sub

Private Function SupplierName(strSuppCode As String) As String

Dim oSuppBO     As cSupplier
Dim strSuppName As String

    On Error Resume Next
    strSuppName = mcolSuppliers(strSuppCode)
    Err.Clear
    On Error GoTo SupplierError
    
    If (strSuppName = "") Then
        Set oSuppBO = goDatabase.CreateBusinessObject(CLASSID_SUPPLIER)
        Call oSuppBO.IBo_AddLoadFilter(CMP_EQUAL, FID_SUPPLIER_SupplierNo, strSuppCode)
        Call oSuppBO.IBo_AddLoadField(FID_SUPPLIER_Name)
        Call oSuppBO.LoadMatches
        strSuppName = oSuppBO.Name
        If (oSuppBO.Name = "") Then strSuppName = "NOT FOUND"
        Call mcolSuppliers.Add(strSuppName, strSuppCode)
    End If
    
    SupplierName = strSuppName
    
    Exit Function
    
SupplierError:

    strSuppName = "NOT FOUND-ERROR"

End Function

Private Sub LoadPlanGram()

Const PROCEDURE_NAME As String = MODULE_NAME & ".GetCompetitorsList"

Dim oPlanGramBO As cPlangram
Dim colPGrams   As Collection
Dim lngRowNo    As Long
Dim vntList     As Variant
Dim rsLocs      As ADODB.Recordset

    Set oPlanGramBO = goDatabase.CreateBusinessObject(CLASSID_PLANGRAM)
    Call oPlanGramBO.IBo_AddLoadField(FID_PLANGRAM_PlanNumber)
    Call oPlanGramBO.IBo_AddLoadField(FID_PLANGRAM_PlanSegment)
    Call oPlanGramBO.IBo_AddLoadField(FID_PLANGRAM_ShelfNumber)
    Call oPlanGramBO.IBo_AddLoadField(FID_PLANGRAM_PlanName)
    
    Set rsLocs = New Recordset
    Call rsLocs.Open("SELECT DISTINCT PLANNO,PLANSEGN,FIXTURENO,PLANNAME FROM PLANGRAM", goDatabase.Connection)
    
    While (Not rsLocs.EOF)
        sprdPlangram.MaxRows = sprdPlangram.MaxRows + 1
        sprdPlangram.Row = sprdPlangram.MaxRows
        sprdPlangram.Col = COL_PG_REF
        sprdPlangram.Text = rsLocs("PLANNO") & "/" & rsLocs("PLANSEGN") & "/" & rsLocs("FIXTURENO")
        sprdPlangram.Col = COL_PG_DESC
        sprdPlangram.Text = rsLocs("PLANNAME")
        sprdPlangram.Col = COL_PG_RETRIEVE
        sprdPlangram.Value = 0
        Call rsLocs.MoveNext
    Wend
    Call rsLocs.Close
        
    mblnHidden = False
    sprdPlangram.SortKey(1) = COL_PG_DESC
    sprdPlangram.SortKeyOrder(1) = SortKeyOrderAscending
    Call sprdPlangram.Sort(-1, -1, -1, -1, SortByRow)
     
End Sub

Private Sub ClearSortBy()

Dim lngOptNo As Long

    For lngOptNo = cmbSortBy.ListCount - 1 To 0 Step -1
        If (cmbSortBy.ItemData(lngOptNo) > 1) Then Call cmbSortBy.RemoveItem(lngOptNo)
    Next lngOptNo

End Sub

Private Function DecodeLocation(strLocation As String) As String

Dim strLoc  As String

    strLoc = "/" & strLocation
    While (InStr(strLoc, "/0") > 0)
        strLoc = Replace(strLoc, "/0", "/")
    Wend
    DecodeLocation = Mid$(strLoc, 2)

End Function

Private Function SetUpItem() As cInventory

Dim oItemBO As cInventory

    Set oItemBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call oItemBO.AddLoadField(FID_INVENTORY_PartCode)
    Call oItemBO.AddLoadField(FID_INVENTORY_Description)
    Call oItemBO.AddLoadField(FID_INVENTORY_NormalSellPrice)
    Call oItemBO.AddLoadField(FID_INVENTORY_PriorSellPrice)
    Call oItemBO.AddLoadField(FID_INVENTORY_SupplierNo)
    Call oItemBO.AddLoadField(FID_INVENTORY_QuantityAtHand)
    Call oItemBO.AddLoadField(FID_INVENTORY_NonStockItem)
    Call oItemBO.AddLoadField(FID_INVENTORY_EquivalentPriceMult)
    Call oItemBO.AddLoadField(FID_INVENTORY_EquivalentPriceUnit)
    Call oItemBO.AddLoadField(FID_INVENTORY_NoOfSmallLabels)
    Call oItemBO.AddLoadField(FID_INVENTORY_NoOfMediumLabels)
    Call oItemBO.AddLoadField(FID_INVENTORY_NoOfLargeLabels)
    Call oItemBO.AddLoadField(FID_INVENTORY_AutoApplyPriceChanges)
    Call oItemBO.AddLoadField(FID_INVENTORY_ItemObsolete)
    Call oItemBO.AddLoadField(FID_INVENTORY_MarkDownQuantity)
    Call oItemBO.AddLoadField(FID_INVENTORY_PRFSKU)
    
    Set SetUpItem = oItemBO

End Function

Private Function WEEECost(strPRFSKU As String) As Currency

Dim oWEEESKU As cInventory

    WEEECost = 0
        
    If (Val(strPRFSKU) = 0) Then Exit Function
    
    If (goSession.GetParameter(PRM_USE_WEEE_RATES) = False) Then Exit Function

    On Error Resume Next
    Set oWEEESKU = mcolWEEESKUs(strPRFSKU)
    On Error GoTo 0
    
    If ((oWEEESKU Is Nothing) = True) Then
        Set oWEEESKU = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        Call oWEEESKU.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, strPRFSKU)
        Call oWEEESKU.AddLoadField(FID_INVENTORY_PartCode)
        Call oWEEESKU.AddLoadField(FID_INVENTORY_NormalSellPrice)
        If (oWEEESKU.LoadMatches.Count = 0) Then
            Call DebugMsg(MODULE_NAME, "GetWEEECost", endlDebug, "")
            Exit Function
        End If
        Call mcolWEEESKUs.Add(oWEEESKU, strPRFSKU)
    End If
    
    WEEECost = oWEEESKU.NormalSellPrice
    
End Function

Private Function IsLabelledSKU(strSKU As String) As Boolean

Dim oItemWixBO As cInventoryWickes

    IsLabelledSKU = False
    If strSKU = "" Then Exit Function

    Set oItemWixBO = goDatabase.CreateBusinessObject(CLASSID_INVENTORYWICKES)
    Call oItemWixBO.AddLoadFilter(CMP_EQUAL, FID_INVENTORYWICKES_PartCode, strSKU)
    Call oItemWixBO.AddLoadField(FID_INVENTORYWICKES_SaleTypeAttribute)
    Call oItemWixBO.LoadMatches
    If (oItemWixBO.SaleTypeAttribute <> "D") And (oItemWixBO.SaleTypeAttribute <> "V") And (oItemWixBO.SaleTypeAttribute <> "W") Then
        IsLabelledSKU = True
        Exit Function
    End If
    
    Set oItemWixBO = Nothing
    
End Function 'IsLabelledSKU

Private Sub LocalLoadLabelPrintHeader(ByRef lngNoSmlReq As Long, ByRef lngNoMedReq As Long, ByRef lngNoLrgReq As Long, ByRef lngNoSmlDone As Long, ByRef lngNoMedDone As Long, ByRef lngNoLrgDone As Long)

Dim rsTmpFil    As New ADODB.Recordset
Dim rsStock     As ADODB.Recordset
Dim oBO         As cTempFil
Dim sData       As String
Dim lFieldNo    As Long

Dim HHTLabelImplementationFactory As HHTLabelCommon.HHTLabelFactory
Dim HHTLabelImplementation As HHTLabelCommon.IHHTLabel
Dim SummaryRecordFKeyIdentifier As String
        
    Set HHTLabelImplementationFactory = New HHTLabelCommon.HHTLabelFactory
    Call HHTLabelImplementationFactory.Initialise(goSession)
    Set HHTLabelImplementation = HHTLabelImplementationFactory.GetImplementation
    SummaryRecordFKeyIdentifier = HHTLabelImplementation.LabelRequestSummaryRecordFKeyIdentifier

    ' Create new or clear previous entries
    If mcolHHTLabels Is Nothing Then
        Set mcolHHTLabels = New Collection
    Else
        On Error GoTo SkipSwitch
        If goSession.GetParameter(PRM_REF_834_SWITCHABLE) = True Then
            Call ClearLabelCollection
        Else
SkipSwitch:
            On Error GoTo 0
            Err.Clear
            With mcolHHTLabels
                Do While .Count > 0
                    Call .Remove(0)
                Loop
            End With
        End If
    End If
    With rsTmpFil
        On Error GoTo SkipAgain
        If goSession.GetParameter(PRM_REF_834_SWITCHABLE) = True Then
            Call .Open("select * from TMPFIL where FKEY like 'PCPLRQ%' and FKEY <> '" & SummaryRecordFKeyIdentifier & "'", goDatabase.Connection)
        Else
SkipAgain:
            On Error GoTo 0
            Err.Clear
            Call .Open("select * from TMPFIL where FKEY like 'PCPLRQ%'", goDatabase.Connection)
        End If
        If Not .EOF = True Then
            .MoveFirst
            Do While Not .EOF
                Set oBO = goSession.Database.CreateBusinessObject(CLASSID_TEMPFIL)
                For lFieldNo = 0 To .Fields.Count - 1 Step 1
                    With .Fields.Item(lFieldNo)
                        If Not IsNull(.Value) Then
                            Select Case .Name
                                Case "TKEY"
                                    oBO.Id = .Value
                                Case "WSID"
                                    oBO.WorkstationID = .Value
                                Case "FKEY"
                                    oBO.KeyData = .Value
                                Case "DATE1"
                                    oBO.KeyDate = .Value
                                Case "DATA"
                                    oBO.Value = .Value
                                Case "DAT1"
                                    oBO.Data1 = .Value
                                Case "DAT2"
                                    oBO.Data2 = .Value
                                Case "DAT3"
                                    oBO.Data3 = .Value
                                Case "DAT4"
                                    oBO.Data4 = .Value
                            End Select
                        End If
                    End With
                Next lFieldNo
                Call mcolHHTLabels.Add(oBO)
                .MoveNext
            Loop
        End If
        Call .Close
        
        If HHTLabelImplementation.PopulateSummaryRecordAndExtractDataFieldFromTMPFILRecordSet(rsTmpFil, goSession.Database.Connection, moTempFilBO, sData) Then
            lngNoSmlReq = 0
            lngNoMedReq = 0
            lngNoLrgReq = 0
            lngNoSmlDone = 0
            lngNoMedDone = 0
            lngNoLrgDone = 0
            lngNoSmlReq = Val(Mid(sData, 14, 6))
            lngNoMedReq = Val(Mid(sData, 20, 6))
            lngNoLrgReq = Val(Mid(sData, 26, 6))
            lngNoSmlDone = Val(Mid(sData, 32, 6))
            lngNoMedDone = Val(Mid(sData, 38, 6))
            lngNoLrgDone = Val(Mid(sData, 44, 6))
        End If
    End With
    
End Sub

'Referral 834
Private Sub ClearLabelCollection()

    'Clear the collection
    With mcolHHTLabels
        Do While .Count > 0
            Call .Remove(.Count)
        Loop
    End With
    
End Sub
