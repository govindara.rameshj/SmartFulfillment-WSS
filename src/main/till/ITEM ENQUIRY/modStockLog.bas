Attribute VB_Name = "modStockLog"
'<CAMH>****************************************************************************************
'* Module : modStockLog
'* Date   : 03/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Item Enquiry/modStockLog.bas $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $
'* $Date: 4/04/03 3:58p $
'* $Revision: 1 $
'* Versions:
'* 03/10/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modStockLog"


Public Const COL_LOG_DATE As Long = 1
Public Const COL_LOG_TIME As Long = 2
Public Const COL_LOG_SKU As Long = 3
Public Const COL_LOG_TYPE As Long = 4
Public Const COL_LOG_PRE_QTY As Long = 5
Public Const COL_LOG_QTY As Long = 6
Public Const COL_LOG_ORIGDOC As Long = 7
Public Const COL_LOG_TILLNO As Long = 8
Public Const COL_LOG_STORE As Long = 9
Public Const COL_LOG_USER As Long = 10
Public Const COL_LOG_LINETYPE As Long = 11
Public Const COL_LOG_KEY As Long = 12
Public Const COL_LOG_SDATE As Long = 13


Public Sub FillInTransactions(ByVal strPartCode As String, ByVal dteStartDate As Date, ByVal dteEndDate As Date, ByVal lngTranType As Long, ByRef sprdHistory As vaSpread, ByRef ucpbProgress As ucpbProgressBar)

Dim oStockLog    As Object
Dim lLineNo      As Integer
Dim colBO        As Collection
Dim strTranType  As String

On Error GoTo bad_transaction

Screen.MousePointer = 11

    sprdHistory.MaxRows = 0
    Set oStockLog = goDatabase.CreateBusinessObject(CLASSID_STOCKLOG)
    
'    If cmbRemRef.Text <> "All" Then where_str = where_str & " AND Reference " & cmbRemRef.Text & " '" & txtCritRef.Text & "'"
'    If cmbRemAmt.Text <> "All" Then where_str = where_str & " AND (ABS(TranAmount) " & cmbRemAmt.Text & txtCritAmt.Text & " AND Transactions.TranType = 2) OR (ABS(TranAmount+VATAmount) " & cmbRemAmt.Text & txtCritAmt.Text & " AND Transactions.TranType IN (1,3,4,5,6,7,8))"
    'If chkCritProv.Value = 1 Then where_str = where_str & " AND TranAmount + VATAmount < 0 "
    strTranType = ""
    If lngTranType <> 0 Then
      Select Case (lngTranType)
        Case (1): strTranType = strTranType & "BS"
        Case (2): strTranType = strTranType & "II"
        Case (3): strTranType = strTranType & "IO"
        Case (4): strTranType = strTranType & "AD"
        Case (5): strTranType = strTranType & "RE"
        Case (6): strTranType = strTranType & "SR"
        Case (7): strTranType = strTranType & "SV"
        Case (8): strTranType = strTranType & "RF"
        Case (9): strTranType = strTranType & "SA"
         Case Else
      End Select
    End If
    '*************************************************
    '* create list of all transactions that occurred *
    '* for Creditor within date bounds               *
    '*************************************************
    
    If strTranType <> "" Then Call oStockLog.IBo_AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_MovementType, strTranType)
    If strPartCode <> "" Then 'if viewing 1 part code then filter and hide SKU column
        Call oStockLog.IBo_AddLoadFilter(CMP_EQUAL, FID_STOCKLOG_PartCode, strPartCode)
        sprdHistory.Col = COL_LOG_SKU
        sprdHistory.ColHidden = True
    Else
        sprdHistory.Col = COL_LOG_SKU
        sprdHistory.ColHidden = False
    End If
    
    'Check if start and end dates are required, if so add to filters
    If Year(dteStartDate) > 1899 Then Call oStockLog.IBo_AddLoadFilter(CMP_GREATEREQUALTHAN, FID_STOCKLOG_LogDate, dteStartDate)
    If Year(dteEndDate) > 1899 Then Call oStockLog.IBo_AddLoadFilter(CMP_LESSEQUALTHAN, FID_STOCKLOG_LogDate, dteEndDate)
    
    Set colBO = New Collection
    Set colBO = oStockLog.IBo_LoadMatches
    
    If colBO.Count > 0 Then ucpbProgress.Max = colBO.Count
    
    For lLineNo = 1 To colBO.Count Step 1
        sprdHistory.MaxRows = lLineNo
        ucpbProgress.Value = lLineNo
        sprdHistory.Row = sprdHistory.MaxRows
        sprdHistory.Col = COL_LOG_KEY
        sprdHistory.Text = colBO(CLng(lLineNo)).Key
        sprdHistory.Col = COL_LOG_DATE
        sprdHistory.Text = DisplayDate(colBO(CLng(lLineNo)).LogDate, False)
        sprdHistory.Col = COL_LOG_TIME
        sprdHistory.Text = Format(colBO(CLng(lLineNo)).LogTime, "@@:@@:@@")
        sprdHistory.Col = COL_LOG_SKU
        sprdHistory.Text = colBO(CLng(lLineNo)).PartCode
        sprdHistory.Col = COL_LOG_TYPE
        sprdHistory.Text = colBO(CLng(lLineNo)).MovementDesc
        sprdHistory.Col = COL_LOG_PRE_QTY
        sprdHistory.Text = colBO(CLng(lLineNo)).OnHandPrior
        sprdHistory.Col = COL_LOG_QTY
        sprdHistory.Text = colBO(CLng(lLineNo)).QuantityAdjusted
        sprdHistory.Col = COL_LOG_ORIGDOC
        sprdHistory.Text = colBO(CLng(lLineNo)).TransactionNo
        sprdHistory.Col = COL_LOG_TILLNO
        sprdHistory.Text = colBO(CLng(lLineNo)).TillNumber
        If sprdHistory.Text <> "" Then sprdHistory.BackColor = RGB_WHITE
        sprdHistory.Col = COL_LOG_STORE
        sprdHistory.Text = colBO(CLng(lLineNo)).storenumber
        If sprdHistory.Text <> "" Then sprdHistory.BackColor = RGB_WHITE
        sprdHistory.Col = COL_LOG_USER
        sprdHistory.Text = colBO(CLng(lLineNo)).user
        sprdHistory.Col = COL_LOG_LINETYPE
        sprdHistory.Text = 1 'flag as line of data
        sprdHistory.Col = COL_LOG_SDATE
        sprdHistory.Text = Format(colBO(CLng(lLineNo)).LogDate, "YYYYMMDD") & colBO(CLng(lLineNo)).LogTime
    Next lLineNo
            
    ucpbProgress.Value = 0
    
    Screen.MousePointer = 0
    
    Exit Sub
    
bad_transaction:

If Err.Number <> 94 And Err.Number <> 13 Then
    MsgBox (Error$), , "Error retreiving Transactions"
    Screen.MousePointer = 0
    Exit Sub
End If

Resume Next
  
End Sub


