VERSION 5.00
Object = "{6514F5A0-641C-11D2-9FD0-0020AF131A57}#3.0#0"; "fpFlp30.ocx"
Object = "{F537A415-F16A-11D6-B070-0020AFC9A0C8}#1.0#0"; "HotkeyList.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#7.0#0"; "FPSPR70.ocx"
Object = "{8DDE6232-1BB0-11D0-81C3-0080C7A2EF7D}#3.0#0"; "Flp32a30.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{B3202B8D-B8B9-4655-9712-EAB5BFD920D4}#1.0#0"; "ItemFilter.ocx"
Object = "{9BA18739-054D-4172-8E42-118133CE2FC4}#1.0#0"; "EditDateCtl.ocx"
Object = "{5A45CCFD-8E3A-4E95-BAE5-3A0B8FF0FA2A}#1.0#0"; "CTSProgBar.ocx"
Begin VB.Form frmItems 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFC0C0&
   Caption         =   "Item Enquiry"
   ClientHeight    =   8295
   ClientLeft      =   -75
   ClientTop       =   315
   ClientWidth     =   12135
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "frmItems.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   12135
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ItemFilter_UC_Wickes.ucItemFilter ucifSearch 
      Height          =   135
      Left            =   8280
      TabIndex        =   160
      Top             =   8040
      Width           =   735
      _ExtentX        =   0
      _ExtentY        =   0
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   13
      Top             =   7920
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmItems.frx":058A
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13600
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "10:26"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin CTSProgBar.ucpbProgressBar ucProgress 
      Height          =   1860
      Left            =   3547
      TabIndex        =   95
      Top             =   3000
      Visible         =   0   'False
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3281
      Smooth          =   0   'False
      Value           =   0
      Title           =   "Progress Indicator"
      Caption1        =   "Current Operation"
      Object.Width           =   5100
      Object.Height          =   1860
   End
   Begin TabDlg.SSTab tabDetails 
      Height          =   5295
      Left            =   120
      TabIndex        =   9
      Top             =   2040
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   9340
      _Version        =   393216
      Tabs            =   7
      Tab             =   6
      TabsPerRow      =   8
      TabHeight       =   520
      BackColor       =   16761024
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Alt &1.Tech Info"
      TabPicture(0)   =   "frmItems.frx":2756
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "frTechInfo"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Alt &2.PO's"
      TabPicture(1)   =   "frmItems.frx":2772
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraPODetails"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Alt &3. CO's"
      TabPicture(2)   =   "frmItems.frx":278E
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "Alt &4.Movement"
      TabPicture(3)   =   "frmItems.frx":27AA
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraMovements"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Alt &5. Prices"
      TabPicture(4)   =   "frmItems.frx":27C6
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraPrices"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Alt &6. Supplier"
      TabPicture(5)   =   "frmItems.frx":27E2
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "frSupplier"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Alt &7. Other Info"
      TabPicture(6)   =   "frmItems.frx":27FE
      Tab(6).ControlEnabled=   -1  'True
      Tab(6).Control(0)=   "frSummary"
      Tab(6).Control(0).Enabled=   0   'False
      Tab(6).Control(1)=   "fraOtherInfo"
      Tab(6).Control(1).Enabled=   0   'False
      Tab(6).ControlCount=   2
      Begin VB.Frame fraOtherInfo 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   3975
         Left            =   120
         TabIndex        =   96
         Top             =   360
         Width           =   11295
         Begin VB.ComboBox cmbStatus 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "frmItems.frx":281A
            Left            =   4440
            List            =   "frmItems.frx":2824
            Style           =   2  'Dropdown List
            TabIndex        =   115
            Top             =   2400
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Frame fraHide 
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   240
            TabIndex        =   108
            Top             =   240
            Width           =   9855
            Begin VB.Label Label49 
               BackStyle       =   0  'Transparent
               Caption         =   "Last Received"
               Height          =   255
               Left            =   7320
               TabIndex        =   114
               Top             =   0
               Width           =   1335
            End
            Begin VB.Label lblLastReceived 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   8640
               TabIndex        =   113
               Top             =   0
               Width           =   1215
            End
            Begin VB.Label Label47 
               BackStyle       =   0  'Transparent
               Caption         =   "Last Ordered"
               Height          =   255
               Left            =   3480
               TabIndex        =   112
               Top             =   0
               Width           =   1215
            End
            Begin VB.Label lblLastOrdered 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   4800
               TabIndex        =   111
               Top             =   0
               Width           =   1215
            End
            Begin VB.Label lblLastSold 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1200
               TabIndex        =   110
               Top             =   0
               Width           =   1095
            End
            Begin VB.Label Label14 
               BackStyle       =   0  'Transparent
               Caption         =   "Last Sold"
               Height          =   255
               Left            =   0
               TabIndex        =   109
               Top             =   0
               Width           =   975
            End
         End
         Begin VB.Frame frConfig 
            Enabled         =   0   'False
            Height          =   1995
            Left            =   120
            TabIndex        =   97
            Top             =   1380
            Width           =   3735
            Begin VB.Label lblFlag5 
               Caption         =   "lblFlag5"
               Height          =   255
               Left            =   120
               TabIndex        =   107
               Top             =   1620
               Width           =   1455
            End
            Begin VB.Label lblFlag4 
               Caption         =   "lblFlag4"
               Height          =   255
               Left            =   120
               TabIndex        =   106
               Top             =   1260
               Width           =   1455
            End
            Begin VB.Label lblFlag3 
               Caption         =   "lblFlag3"
               Height          =   255
               Left            =   120
               TabIndex        =   105
               Top             =   900
               Width           =   1455
            End
            Begin VB.Label lblFlag2 
               Caption         =   "lblFlag2"
               Height          =   255
               Left            =   120
               TabIndex        =   104
               Top             =   540
               Width           =   1455
            End
            Begin VB.Label lblFlag1 
               Caption         =   "lblFlag1"
               Height          =   255
               Left            =   120
               TabIndex        =   103
               Top             =   180
               Width           =   1455
            End
            Begin VB.Label lblFlag1Value 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1680
               TabIndex        =   102
               Top             =   180
               Width           =   1935
            End
            Begin VB.Label lblFlag2Value 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1680
               TabIndex        =   101
               Top             =   540
               Width           =   1935
            End
            Begin VB.Label lblFlag3Value 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1680
               TabIndex        =   100
               Top             =   900
               Width           =   1935
            End
            Begin VB.Label lblFlag4Value 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1680
               TabIndex        =   99
               Top             =   1260
               Width           =   1935
            End
            Begin VB.Label lblFlag5Value 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1680
               TabIndex        =   98
               Top             =   1620
               Width           =   1935
            End
         End
         Begin VB.Label Label52 
            BackStyle       =   0  'Transparent
            Caption         =   "Buy Units"
            Height          =   255
            Left            =   4080
            TabIndex        =   131
            Top             =   1080
            Width           =   855
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Selling Units"
            Height          =   255
            Left            =   240
            TabIndex        =   130
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Weight"
            Height          =   255
            Left            =   4800
            TabIndex        =   129
            Top             =   3480
            Width           =   615
         End
         Begin VB.Label Label45 
            BackStyle       =   0  'Transparent
            Caption         =   "Man Prod Code"
            Height          =   255
            Left            =   7200
            TabIndex        =   128
            Top             =   3480
            Width           =   1455
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "Width"
            Height          =   255
            Left            =   120
            TabIndex        =   127
            Top             =   3480
            Width           =   615
         End
         Begin VB.Label Label15 
            BackStyle       =   0  'Transparent
            Caption         =   "Height"
            Height          =   255
            Left            =   2400
            TabIndex        =   126
            Top             =   3480
            Width           =   615
         End
         Begin VB.Label Label32 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Group"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   4080
            TabIndex        =   125
            Top             =   720
            Width           =   735
         End
         Begin VB.Label Label36 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Department"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   240
            TabIndex        =   124
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label lblSellInPacksOf 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   1440
            TabIndex        =   123
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblBuyInPacksOf 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   5040
            TabIndex        =   122
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label lblWidth 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   720
            TabIndex        =   121
            Top             =   3480
            Width           =   975
         End
         Begin VB.Label lblHeight 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   3000
            TabIndex        =   120
            Top             =   3480
            Width           =   975
         End
         Begin VB.Label lblWeight 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   5520
            TabIndex        =   119
            Top             =   3480
            Width           =   975
         End
         Begin VB.Label lblManuProdCode 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   8640
            TabIndex        =   118
            Top             =   3480
            Width           =   1815
         End
         Begin VB.Label lblPackSize 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   8880
            TabIndex        =   117
            Top             =   1080
            Width           =   1215
         End
         Begin VB.Label Label33 
            BackStyle       =   0  'Transparent
            Caption         =   "Pack Size"
            Height          =   255
            Left            =   7560
            TabIndex        =   116
            Top             =   1080
            Width           =   1215
         End
      End
      Begin VB.Frame frTechInfo 
         BorderStyle     =   0  'None
         Height          =   3975
         Left            =   -74880
         TabIndex        =   37
         Top             =   360
         Width           =   11655
         Begin VB.PictureBox picPhoto 
            Height          =   3135
            Left            =   120
            Picture         =   "frmItems.frx":283B
            ScaleHeight     =   3075
            ScaleWidth      =   4635
            TabIndex        =   94
            Top             =   120
            Width           =   4695
         End
         Begin VB.Label lblTechInfo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   3135
            Left            =   4920
            TabIndex        =   66
            Top             =   120
            Width           =   6495
         End
      End
      Begin VB.Frame fraPODetails 
         BorderStyle     =   0  'None
         Height          =   4875
         Left            =   -74880
         TabIndex        =   70
         Top             =   360
         Width           =   11655
         Begin VB.Frame fraPOCriteria 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Select Order Filter Criteria"
            Height          =   975
            Left            =   0
            TabIndex        =   72
            Top             =   3840
            Width           =   11655
            Begin VB.CommandButton cmdPOReset 
               Caption         =   "F3-Reset"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   9720
               TabIndex        =   89
               Top             =   600
               Width           =   855
            End
            Begin VB.CommandButton cmdPOPrint 
               Caption         =   "F9-Print"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   10720
               TabIndex        =   88
               Top             =   600
               Width           =   855
            End
            Begin VB.CommandButton cmdPOApply 
               Caption         =   "F7-Apply"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   8880
               TabIndex        =   87
               Top             =   600
               Width           =   855
            End
            Begin VB.TextBox txtPOOrderNo 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1260
               TabIndex        =   74
               Text            =   "0"
               Top             =   240
               Width           =   735
            End
            Begin VB.ComboBox cmbPODueDateCrit 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frmItems.frx":48B5
               Left            =   6300
               List            =   "frmItems.frx":48C8
               Style           =   2  'Dropdown List
               TabIndex        =   81
               Top             =   240
               Width           =   840
            End
            Begin VB.ComboBox cmbPOrdDateCrit 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frmItems.frx":48E2
               Left            =   2820
               List            =   "frmItems.frx":48F5
               Style           =   2  'Dropdown List
               TabIndex        =   76
               Top             =   240
               Width           =   840
            End
            Begin VB.ComboBox cmbPOStatus 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frmItems.frx":490F
               Left            =   9900
               List            =   "frmItems.frx":491F
               Style           =   2  'Dropdown List
               TabIndex        =   86
               Top             =   240
               Width           =   1035
            End
            Begin ucEditDate.ucDateText dtxtDueStartDate 
               Height          =   285
               Left            =   7200
               TabIndex        =   82
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DateFormat      =   "DD/MM/YY"
               Text            =   "10/01/03"
            End
            Begin ucEditDate.ucDateText dtxtPOrdStartDate 
               Height          =   285
               Left            =   3720
               TabIndex        =   77
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DateFormat      =   "DD/MM/YY"
               Text            =   "10/01/03"
            End
            Begin ucEditDate.ucDateText dtxtPOrdEndDate 
               Height          =   285
               Left            =   4860
               TabIndex        =   79
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DateFormat      =   "DD/MM/YY"
               Text            =   "10/01/03"
            End
            Begin ucEditDate.ucDateText dtxtDueEndDate 
               Height          =   285
               Left            =   8340
               TabIndex        =   84
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DateFormat      =   "DD/MM/YY"
               Text            =   "10/01/03"
            End
            Begin VB.Label Label44 
               BackStyle       =   0  'Transparent
               Caption         =   "Order No"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   540
               TabIndex        =   73
               Top             =   270
               Width           =   855
            End
            Begin VB.Label Label30 
               BackStyle       =   0  'Transparent
               Caption         =   "Ordered"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2160
               TabIndex        =   75
               Top             =   270
               Width           =   615
            End
            Begin VB.Label Label26 
               BackStyle       =   0  'Transparent
               Caption         =   "Due"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   5940
               TabIndex        =   80
               Top             =   270
               Width           =   375
            End
            Begin VB.Label Label22 
               BackStyle       =   0  'Transparent
               Caption         =   "Status"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   9360
               TabIndex        =   85
               Top             =   270
               Width           =   615
            End
            Begin VB.Label lblPOTolbl 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "to"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   4605
               TabIndex        =   78
               Top             =   270
               Width           =   255
            End
            Begin VB.Label lblDueTolbl 
               BackStyle       =   0  'Transparent
               Caption         =   "to"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   8130
               TabIndex        =   83
               Top             =   270
               Width           =   255
            End
         End
         Begin FPSpreadADO.fpSpread sprdPOTrans 
            Height          =   3735
            Left            =   0
            TabIndex        =   71
            Top             =   60
            Width           =   11655
            _Version        =   458752
            _ExtentX        =   20558
            _ExtentY        =   6588
            _StockProps     =   64
            ColsFrozen      =   5
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   35
            MaxRows         =   1
            OperationMode   =   2
            RowHeaderDisplay=   0
            SelectBlockOptions=   0
            SpreadDesigner  =   "frmItems.frx":4941
            UserResize      =   1
         End
      End
      Begin VB.Frame fraPrices 
         BorderStyle     =   0  'None
         Height          =   3255
         Left            =   -74880
         TabIndex        =   69
         Top             =   480
         Width           =   6495
         Begin VB.PictureBox fraLevels 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0C0C0&
            ForeColor       =   &H80000008&
            Height          =   3075
            Left            =   0
            ScaleHeight     =   3045
            ScaleWidth      =   4785
            TabIndex        =   137
            Top             =   0
            Width           =   4815
            Begin VB.Label lblTradePricePrior 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   3240
               TabIndex        =   159
               Top             =   780
               Width           =   1215
            End
            Begin VB.Label lblTradePrice 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   158
               Top             =   780
               Width           =   1215
            End
            Begin VB.Label Label62 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Trade Price"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   240
               TabIndex        =   157
               Top             =   780
               Width           =   1095
            End
            Begin VB.Label lbl4smPricePrior 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   3240
               TabIndex        =   156
               Top             =   1140
               Width           =   1215
            End
            Begin VB.Label lbl4smPrice 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   155
               Top             =   1140
               Width           =   1215
            End
            Begin VB.Label Label59 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "4sm Price"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   240
               TabIndex        =   154
               Top             =   1140
               Width           =   975
            End
            Begin VB.Label lbl4smMargin 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   153
               Top             =   2580
               Width           =   1215
            End
            Begin VB.Label Label55 
               BackStyle       =   0  'Transparent
               Caption         =   "4sm Margin"
               Height          =   255
               Left            =   240
               TabIndex        =   152
               Top             =   2580
               Width           =   1215
            End
            Begin VB.Label lblTradeMargin 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   151
               Top             =   2220
               Width           =   1215
            End
            Begin VB.Label Label46 
               BackStyle       =   0  'Transparent
               Caption         =   "Trade Margin"
               Height          =   255
               Left            =   240
               TabIndex        =   150
               Top             =   2220
               Width           =   1215
            End
            Begin VB.Label Label41 
               BackStyle       =   0  'Transparent
               Caption         =   "Prior"
               Height          =   255
               Left            =   3240
               TabIndex        =   149
               Top             =   180
               Width           =   495
            End
            Begin VB.Label lblSellPriceMarginPrior 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   3240
               TabIndex        =   148
               Top             =   420
               Width           =   1215
            End
            Begin VB.Label Label19 
               BackStyle       =   0  'Transparent
               Caption         =   "Current"
               Height          =   255
               Left            =   1560
               TabIndex        =   147
               Top             =   180
               Width           =   975
            End
            Begin VB.Label Label28 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               Caption         =   "Retail Price"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   240
               TabIndex        =   146
               Top             =   420
               Width           =   1095
            End
            Begin VB.Label lblSellPriceMargin 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   145
               Top             =   420
               Width           =   1215
            End
            Begin VB.Label Label12 
               BackStyle       =   0  'Transparent
               Caption         =   "Cost Price"
               Height          =   255
               Left            =   240
               TabIndex        =   144
               Top             =   1500
               Width           =   975
            End
            Begin VB.Label Label16 
               BackStyle       =   0  'Transparent
               Caption         =   "Full Margin"
               Height          =   255
               Left            =   240
               TabIndex        =   143
               Top             =   1860
               Width           =   975
            End
            Begin VB.Label lblMargin 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   142
               Top             =   1860
               Width           =   1215
            End
            Begin VB.Label lblCostPrice 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "0.00"
               ForeColor       =   &H80000008&
               Height          =   255
               Left            =   1560
               TabIndex        =   141
               Top             =   1500
               Width           =   1215
            End
            Begin VB.Label Label54 
               BackStyle       =   0  'Transparent
               Caption         =   "%"
               Height          =   255
               Left            =   2820
               TabIndex        =   140
               Top             =   2580
               Width           =   375
            End
            Begin VB.Label Label43 
               BackStyle       =   0  'Transparent
               Caption         =   "%"
               Height          =   255
               Left            =   2820
               TabIndex        =   139
               Top             =   2220
               Width           =   375
            End
            Begin VB.Label Label29 
               BackStyle       =   0  'Transparent
               Caption         =   "%"
               Height          =   255
               Left            =   2820
               TabIndex        =   138
               Top             =   1860
               Width           =   375
            End
         End
         Begin FPSpreadADO.fpSpread sprdPrices 
            Height          =   855
            Left            =   0
            TabIndex        =   92
            Top             =   360
            Visible         =   0   'False
            Width           =   5415
            _Version        =   458752
            _ExtentX        =   9551
            _ExtentY        =   1508
            _StockProps     =   64
            DisplayColHeaders=   0   'False
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   4
            MaxRows         =   3
            ScrollBars      =   0
            SpreadDesigner  =   "frmItems.frx":588A
         End
      End
      Begin VB.Frame frSupplier 
         BorderStyle     =   0  'None
         Height          =   2535
         Left            =   -74640
         TabIndex        =   50
         Top             =   480
         Width           =   8295
         Begin VB.Label lblSupplierlbl 
            Caption         =   "Supplier"
            Height          =   255
            Left            =   0
            TabIndex        =   58
            Top             =   0
            Width           =   855
         End
         Begin VB.Label lblSupplier 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   " "
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   960
            TabIndex        =   57
            Top             =   0
            Width           =   3495
         End
         Begin VB.Label Label25 
            Caption         =   "Address"
            Height          =   255
            Left            =   0
            TabIndex        =   56
            Top             =   480
            Width           =   855
         End
         Begin VB.Label lblSuppAddress 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   975
            Left            =   960
            TabIndex        =   55
            Top             =   480
            Width           =   3495
         End
         Begin VB.Label Label27 
            Caption         =   "Phone No"
            Height          =   255
            Left            =   0
            TabIndex        =   54
            Top             =   1680
            Width           =   855
         End
         Begin VB.Label lblSuppPhoneNo 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   960
            TabIndex        =   53
            Top             =   1680
            Width           =   1695
         End
         Begin VB.Label Label31 
            Caption         =   "Fax No"
            Height          =   255
            Left            =   0
            TabIndex        =   52
            Top             =   2040
            Width           =   735
         End
         Begin VB.Label lblSuppfaxNo 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   960
            TabIndex        =   51
            Top             =   2040
            Width           =   1695
         End
      End
      Begin VB.Frame fraMovements 
         Caption         =   " "
         Height          =   4575
         Left            =   -74880
         TabIndex        =   38
         Top             =   360
         Width           =   11655
         Begin VB.Frame fraSMCriteria 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Select Transaction History Display Criteria"
            Height          =   735
            Left            =   0
            TabIndex        =   39
            Top             =   3840
            Width           =   11655
            Begin VB.ComboBox cmbSMType 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frmItems.frx":5BC3
               Left            =   5040
               List            =   "frmItems.frx":5BF1
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   46
               Top             =   300
               Width           =   2415
            End
            Begin VB.CommandButton cmdSMReset 
               Caption         =   "F3-Reset"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   9720
               TabIndex        =   49
               Top             =   240
               Width           =   855
            End
            Begin VB.CommandButton cmdSMPrint 
               Caption         =   "F9-Print"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   10680
               TabIndex        =   48
               Top             =   240
               Width           =   855
            End
            Begin VB.CommandButton cmdSMApply 
               Caption         =   "F7-Apply"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   8880
               TabIndex        =   47
               Top             =   240
               Width           =   855
            End
            Begin VB.ComboBox cmbSMDateCrit 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               ItemData        =   "frmItems.frx":5CB7
               Left            =   900
               List            =   "frmItems.frx":5CCA
               Style           =   2  'Dropdown List
               TabIndex        =   41
               Top             =   300
               Width           =   735
            End
            Begin ucEditDate.ucDateText dtxtSMStartDate 
               Height          =   285
               Left            =   1740
               TabIndex        =   42
               Top             =   300
               Width           =   975
               _ExtentX        =   1720
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DateFormat      =   "DD/MM/YY"
               Text            =   "28/01/03"
            End
            Begin ucEditDate.ucDateText dtxtSMEndDate 
               Height          =   285
               Left            =   2940
               TabIndex        =   44
               Top             =   300
               Width           =   975
               _ExtentX        =   1720
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DateFormat      =   "DD/MM/YY"
               Text            =   "28/01/03"
            End
            Begin VB.Label Label37 
               BackStyle       =   0  'Transparent
               Height          =   255
               Left            =   3120
               TabIndex        =   90
               Top             =   720
               Width           =   1335
            End
            Begin VB.Label lblSMToLbl 
               Alignment       =   2  'Center
               BackStyle       =   0  'Transparent
               Caption         =   "to"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   2700
               TabIndex        =   43
               Top             =   340
               Width           =   255
            End
            Begin VB.Label Label35 
               BackStyle       =   0  'Transparent
               Caption         =   "Type"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   4560
               TabIndex        =   45
               Top             =   340
               Width           =   495
            End
            Begin VB.Label Label34 
               BackStyle       =   0  'Transparent
               Caption         =   "Date"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   480
               TabIndex        =   40
               Top             =   340
               Width           =   495
            End
         End
         Begin FPSpreadADO.fpSpread sprdSMTrans 
            Height          =   5175
            Left            =   0
            TabIndex        =   91
            Top             =   0
            Width           =   11415
            _Version        =   458752
            _ExtentX        =   20135
            _ExtentY        =   9128
            _StockProps     =   64
            ArrowsExitEditMode=   -1  'True
            BackColorStyle  =   1
            ColHeaderDisplay=   0
            DisplayRowHeaders=   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   15
            MaxRows         =   1
            RowHeaderDisplay=   0
            SpreadDesigner  =   "frmItems.frx":5CE4
            UserResize      =   1
            VisibleCols     =   5
            VisibleRows     =   1
         End
      End
      Begin VB.Frame frSummary 
         BorderStyle     =   0  'None
         Height          =   3975
         Left            =   120
         TabIndex        =   132
         Top             =   360
         Width           =   11655
         Begin VB.Frame frPeriodTotals 
            Caption         =   "Period Totals"
            Height          =   1095
            Left            =   360
            TabIndex        =   135
            Top             =   0
            Width           =   11655
            Begin FPSpreadADO.fpSpread sprdPeriodTot 
               Height          =   975
               Left            =   1560
               TabIndex        =   136
               Top             =   120
               Width           =   9975
               _Version        =   458752
               _ExtentX        =   17595
               _ExtentY        =   1720
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaxCols         =   10
               MaxRows         =   2
               ScrollBars      =   0
               SpreadDesigner  =   "frmItems.frx":6443
            End
         End
         Begin VB.Frame frWeeklyTotals 
            Caption         =   "WeeklyTotals"
            Height          =   2055
            Left            =   0
            TabIndex        =   133
            Top             =   1200
            Width           =   11655
            Begin FPSpreadADO.fpSpread sprdWeekTot 
               Height          =   1935
               Left            =   1560
               TabIndex        =   134
               Top             =   120
               Width           =   7335
               _Version        =   458752
               _ExtentX        =   12938
               _ExtentY        =   3413
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaxCols         =   6
               MaxRows         =   14
               RetainSelBlock  =   0   'False
               RowHeaderDisplay=   0
               ScrollBarExtMode=   -1  'True
               ScrollBarMaxAlign=   0   'False
               ScrollBars      =   2
               ScrollBarShowMax=   0   'False
               SpreadDesigner  =   "frmItems.frx":6E80
            End
         End
      End
   End
   Begin prjHotkeyList.ucHotkeyList uchlHelpKeys 
      Height          =   2835
      Left            =   4320
      TabIndex        =   12
      Top             =   2880
      Visible         =   0   'False
      Width           =   3510
      _ExtentX        =   6191
      _ExtentY        =   5001
   End
   Begin CTSProgBar.ucpbProgressBar ucpbItem 
      Height          =   1815
      Left            =   3720
      TabIndex        =   10
      Top             =   2520
      Visible         =   0   'False
      Width           =   5055
      _ExtentX        =   8996
      _ExtentY        =   3281
      FillColor       =   16711680
      Object.Width           =   5055
      Object.Height          =   1815
   End
   Begin MSComctlLib.Slider sldDetails 
      Height          =   3855
      Left            =   0
      TabIndex        =   14
      Top             =   360
      Visible         =   0   'False
      Width           =   90
      _ExtentX        =   159
      _ExtentY        =   6800
      _Version        =   393216
      Orientation     =   1
      Max             =   100
      SelStart        =   95
      TickStyle       =   3
      Value           =   95
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   120
      TabIndex        =   62
      Top             =   7440
      Width           =   7935
      Begin VB.CommandButton cmdPrint 
         Caption         =   "F9-Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5880
         TabIndex        =   65
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.CommandButton cmdFuzzy 
         Caption         =   "F2-Fuzzy"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   64
         Top             =   0
         Width           =   1095
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "F10-Exit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6960
         TabIndex        =   63
         Top             =   0
         Width           =   975
      End
   End
   Begin VB.Frame frDetails 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   5775
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   12015
      Begin LpLib.fpCombo cmbSupplier 
         Height          =   285
         Left            =   6360
         TabIndex        =   93
         Top             =   0
         Visible         =   0   'False
         Width           =   4935
         _Version        =   196608
         _ExtentX        =   8705
         _ExtentY        =   503
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   0   'False
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   2
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   3
         ThreeDInsideStyle=   0
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483642
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   0
         ThreeDOutsideHighlightColor=   16777215
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   1
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   0
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   -1
         ComboGap        =   7
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         ListPosition    =   0
         ButtonThreeDAppearance=   2
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmItems.frx":7656
      End
      Begin LpADOLib.fpComboAdo cmbStSearch 
         Height          =   315
         Left            =   480
         TabIndex        =   5
         Top             =   120
         Visible         =   0   'False
         Width           =   6375
         _Version        =   196608
         _ExtentX        =   11245
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Text            =   ""
         Columns         =   3
         Sorted          =   0
         SelDrawFocusRect=   -1  'True
         ColumnSeparatorChar=   9
         ColumnSearch    =   -1
         ColumnWidthScale=   2
         RowHeight       =   -1
         WrapList        =   0   'False
         WrapWidth       =   0
         AutoSearch      =   1
         SearchMethod    =   0
         VirtualMode     =   0   'False
         VRowCount       =   0
         DataSync        =   0
         ThreeDInsideStyle=   1
         ThreeDInsideHighlightColor=   -2147483633
         ThreeDInsideShadowColor=   -2147483627
         ThreeDInsideWidth=   1
         ThreeDOutsideStyle=   1
         ThreeDOutsideHighlightColor=   -2147483628
         ThreeDOutsideShadowColor=   -2147483632
         ThreeDOutsideWidth=   1
         ThreeDFrameWidth=   0
         BorderStyle     =   0
         BorderColor     =   -2147483642
         BorderWidth     =   1
         ThreeDOnFocusInvert=   0   'False
         ThreeDFrameColor=   -2147483633
         Appearance      =   2
         BorderDropShadow=   0
         BorderDropShadowColor=   -2147483632
         BorderDropShadowWidth=   3
         ScrollHScale    =   2
         ScrollHInc      =   0
         ColsFrozen      =   0
         ScrollBarV      =   1
         NoIntegralHeight=   0   'False
         HighestPrecedence=   0
         AllowColResize  =   0
         AllowColDragDrop=   0
         ReadOnly        =   0   'False
         VScrollSpecial  =   0   'False
         VScrollSpecialType=   0
         EnableKeyEvents =   -1  'True
         EnableTopChangeEvent=   -1  'True
         DataAutoHeadings=   -1  'True
         DataAutoSizeCols=   2
         SearchIgnoreCase=   -1  'True
         ScrollBarH      =   1
         DataFieldList   =   ""
         ColumnEdit      =   -1
         ColumnBound     =   -1
         Style           =   2
         MaxDrop         =   8
         ListWidth       =   -1
         EditHeight      =   -1
         GrayAreaColor   =   -2147483633
         ListLeftOffset  =   0
         ComboGap        =   -2
         MaxEditLen      =   150
         VirtualPageSize =   0
         VirtualPagesAhead=   0
         ExtendCol       =   0
         ColumnLevels    =   1
         ListGrayAreaColor=   -2147483637
         GroupHeaderHeight=   -1
         GroupHeaderShow =   0   'False
         AllowGrpResize  =   0
         AllowGrpDragDrop=   0
         MergeAdjustView =   0   'False
         ColumnHeaderShow=   -1  'True
         ColumnHeaderHeight=   -1
         GrpsFrozen      =   0
         BorderGrayAreaColor=   -2147483637
         ExtendRow       =   0
         EnableClickEvent=   -1  'True
         ListPosition    =   0
         ButtonThreeDAppearance=   0
         DataMemberList  =   ""
         OLEDragMode     =   0
         OLEDropMode     =   0
         Redraw          =   -1  'True
         AutoSearchFill  =   0   'False
         AutoSearchFillDelay=   500
         EditMarginLeft  =   1
         EditMarginTop   =   1
         EditMarginRight =   0
         EditMarginBottom=   3
         ResizeRowToFont =   0   'False
         TextTipMultiLine=   0
         AutoMenu        =   -1  'True
         EditAlignH      =   0
         EditAlignV      =   0
         ColDesigner     =   "frmItems.frx":7A2F
      End
      Begin VB.Timer tmrPrices 
         Left            =   9240
         Top             =   1680
      End
      Begin VB.CommandButton cmdPrev 
         Height          =   255
         Left            =   9720
         Picture         =   "frmItems.frx":7D72
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   1200
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton cmdNext 
         Height          =   255
         Left            =   10680
         Picture         =   "frmItems.frx":8048
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1200
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Label lblCoreItem 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3600
         TabIndex        =   68
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Status"
         Height          =   255
         Left            =   0
         TabIndex        =   67
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lbl4MDisc 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   8760
         TabIndex        =   32
         Top             =   480
         Width           =   495
      End
      Begin VB.Label lblNoMatches 
         Caption         =   "Label11"
         Height          =   255
         Left            =   9960
         TabIndex        =   61
         Top             =   1200
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label Label21 
         BackStyle       =   0  'Transparent
         Caption         =   "Allow Refund"
         Height          =   255
         Left            =   9720
         TabIndex        =   60
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label lblAllowReturns 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   10920
         TabIndex        =   59
         Top             =   1680
         Width           =   495
      End
      Begin VB.Label Label20 
         BackStyle       =   0  'Transparent
         Caption         =   "Disc Allowed"
         Height          =   255
         Left            =   9720
         TabIndex        =   36
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Qty Disc"
         Height          =   255
         Left            =   7680
         TabIndex        =   35
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label lblWarehoused 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   5640
         TabIndex        =   34
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label lblDiscAllowed 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   10920
         TabIndex        =   33
         Top             =   480
         Width           =   495
      End
      Begin VB.Label lblAtHand 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1080
         TabIndex        =   31
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Available"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2640
         TabIndex        =   30
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblSize 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6720
         TabIndex        =   29
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label lblDescription 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1680
         TabIndex        =   28
         Top             =   120
         Width           =   4335
      End
      Begin VB.Label lblSpecialPrice 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3600
         TabIndex        =   27
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label lblSellPrice 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1080
         TabIndex        =   26
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label lblMaxLevel 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1080
         TabIndex        =   25
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblMinLevel 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3600
         TabIndex        =   24
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblCrUsedlbl 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Stock"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   0
         TabIndex        =   23
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label17 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Max Level"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   0
         TabIndex        =   22
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Min Level"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2640
         TabIndex        =   21
         Top             =   1200
         Width           =   855
      End
      Begin VB.Label lblStockAvail 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   3600
         TabIndex        =   20
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Display Qty"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   7680
         TabIndex        =   19
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblQtyOnDisplay 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   8760
         TabIndex        =   18
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Qty On Order"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4920
         TabIndex        =   17
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label lblQtyOnOrder 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0.00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   6240
         TabIndex        =   16
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblStatus 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1080
         TabIndex        =   15
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label Label50 
         BackStyle       =   0  'Transparent
         Caption         =   "Size"
         Height          =   255
         Left            =   6240
         TabIndex        =   6
         Top             =   120
         Width           =   495
      End
      Begin VB.Label lblItemID 
         BackStyle       =   0  'Transparent
         Caption         =   "lblItemID"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   3840
         Width           =   1215
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Special"
         Height          =   255
         Left            =   2640
         TabIndex        =   8
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label lblPartCode 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   120
         Width           =   975
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Retail"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   0
         TabIndex        =   7
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label18 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "SKU"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   0
         TabIndex        =   1
         Top             =   120
         Width           =   495
      End
   End
End
Attribute VB_Name = "frmItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmItems
'* Date   : 03/10/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Item Enquiry/frmItems.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Damians $ $Date: 20/05/04 15:14 $ $Revision: 12 $
'* Versions:
'*  03/10/02    mauricem    1.0.0   Header added.
'*  30/03/05    tjnorris    1.1.0   Updated to work with changes made to ItemFilter.ocx
'*                                  Initial attempt for Wickes conversion.
'*
'* 07/11/05 DaveF   v1.0.16 Changed to stop the overflow error in method DisplayItem().
'*
'* 28/11/06 DaveF   v1.0.19 Compiled with the new ItemFilter_UC interface only control.
'*
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME       As String = "frmItems"

Const TranPayment = 1
Const DelEntry = 2
Const NewEntry = 3

Const UNIT_DESC         As Long = 1
Const UNIT_THISITEM     As Long = 2
Const UNIT_BUYIN        As Long = 3
Const UNIT_SELLIN       As Long = 4
Const UNIT_FACTOR       As Long = 5
Const UNIT_CODE         As Long = 6
Const UNIT_EDITED       As Long = 7

Const PRM_FLAG1_LABEL   As Long = 111
Const PRM_FLAG2_LABEL   As Long = 112
Const PRM_FLAG3_LABEL   As Long = 113
Const PRM_FLAG4_LABEL   As Long = 114
Const PRM_FLAG5_LABEL   As Long = 115

Private Const CRIT_EQUALS        As Long = 0
Private Const CRIT_GREATERTHAN   As Long = 1
Private Const CRIT_LESSTHAN      As Long = 2
Private Const CRIT_ALL           As Long = 3
Private Const CRIT_FROM          As Long = 4
Private Const CRIT_WEEKENDING    As Long = 5

Private Const STATUS_ALL         As Long = 0
Private Const STATUS_EXPIRED     As Long = 1
Private Const STATUS_COMPLETE    As Long = 2
Private Const STATUS_CANCELLED   As Long = 3
Private Const STATUS_PROCESSED   As Long = 4
Private Const STATUS_OUTSTANDING As Long = 5

Const TAB_TEXT = 0
Const TAB_PO_ENQ = 1
Const TAB_CO_ENQ = 2
Const TAB_SM_TRANS = 3
Const TAB_PRICES = 4
Const TAB_SUPPLIER = 5
Const TAB_STATS = 7
Const TAB_OTHER = 6

Const RGB_OldColour     As Long = RGB_WHITE

Dim rstUnits            As Recordset
Dim cItem               As cInventory

Dim mlngSMAllDates      As Byte
Dim mlngSMAllMoves      As Byte
Dim mlngPOAllDates      As Byte
Dim mlngPOAllDues       As Byte
Dim mlngPOAllStatus     As Byte
Dim mlngCOAllDates      As Byte
Dim mlngCOAllStatus     As Byte
Dim mlngStatusAll       As Byte
Dim Dflt_SortBy         As Byte
Dim lTabTop             As Long

Dim adoconn             As ADODB.Connection
Dim blnFullView         As Boolean ' flag set upon load to indicate if Branch or Head Offive View
Dim blnRszText          As Boolean
Dim blnRszPOTrans       As Boolean
Dim blnRszCOTrans       As Boolean
Dim blnRszTrans         As Boolean
Dim blnRszPrices        As Boolean
Dim blnRszSupp          As Boolean
Dim blnRszSumm          As Boolean
Dim blnRszOther         As Boolean
Dim blnSuppLoaded       As Boolean
Dim blnTechLoaded       As Boolean

Dim mblnIgnoreFirstNext As Boolean
Dim mstrDispFormat      As String
Dim mstrPrintStore      As String
Dim mstrSuppNo          As String 'hold current items supplier number for displaying when TAB clicked on

Dim mlngAllCOStatus     As Long
Dim mlngAllCODates      As Long
Dim mlngAllPOStatus     As Long
Dim mlngAllPODates      As Long
Dim mlngAllPODueDates   As Long
Dim mlngAllSMTypes      As Long
Dim mlngAllSMDates      As Long

Private Sub ClearScreen()
Dim lngRowNo As Long

    'clear currently displayed Item from Main Screen
    lblPartCode.Caption = vbNullString
    lblDescription.Caption = vbNullString
    lblSize.Caption = vbNullString
    lblWidth.Caption = "0.00"
    lblHeight.Caption = "0.00"
    lblWeight.Caption = "0.00"
    lblManuProdCode.Caption = vbNullString
    lblLastSold.Caption = "N/A"
    lblLastOrdered.Caption = "N/A"
    lblSpecialPrice.Caption = 0
    lblSellPrice.Caption = 0
    lblCostPrice.Caption = 0
    lblStockAvail.Caption = "0.00"
    lblAtHand.Caption = 0
    lblMinLevel.Caption = 0
    lblMaxLevel.Caption = 0
    lblCoreItem.Caption = vbNullString
    cmbStatus.ListIndex = mlngStatusAll
'    ntxtActualWeight.Value = 0
'    ntxtProdWeight.Value = 0
    
    lblSellPriceMarginPrior.Caption = 0
    lblTradePrice.Caption = 0
    lblTradePricePrior.Caption = 0
    lbl4smPrice.Caption = 0
    lbl4smPricePrior.Caption = 0
    lblTradeMargin.Caption = 0
    lbl4smMargin.Caption = 0
    
    lblAllowReturns.Caption = vbNullString
    lblDiscAllowed.Caption = vbNullString
    lblMargin.Caption = "0.000"
    lblWarehoused.Caption = vbNullString
    lbl4MDisc.Caption = vbNullString
    lblStatus.Caption = vbNullString
    
    sprdWeekTot.Col = 3
    sprdWeekTot.Row = -1
    sprdWeekTot.Text = 0
  
    lblSupplier.Caption = vbNullString
    lblSuppAddress.Caption = vbNullString
    lblSuppPhoneNo.Caption = vbNullString
    lblSuppfaxNo.Caption = vbNullString
    
    lblTechInfo.Caption = vbNullString
    blnTechLoaded = False
  
End Sub

Private Sub cmbCOrdDateCrit_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub


Private Sub cmbCritTran_GotFocus()
    Call SendKeys(KEY_DROPDOWN)

End Sub


Private Sub cmbPODueDateCrit_Click()
    
    On Error Resume Next
    
    Select Case (cmbPODueDateCrit.ItemData(cmbPODueDateCrit.ListIndex))
        Case CRIT_LESSTHAN, CRIT_GREATERTHAN, CRIT_EQUALS
            dtxtDueStartDate.Enabled = True
            dtxtDueStartDate.Visible = True
            lblDueTolbl.Visible = False
            dtxtDueEndDate.Enabled = False
            dtxtDueEndDate.Visible = False
            
        Case CRIT_ALL
            dtxtDueStartDate.Enabled = False
            dtxtDueStartDate.Visible = False
            lblDueTolbl.Visible = False
            dtxtDueEndDate.Enabled = False
            dtxtDueEndDate.Visible = False
            
        Case CRIT_FROM
            dtxtDueStartDate.Enabled = True
            dtxtDueStartDate.Visible = True
            lblDueTolbl.Visible = True
            dtxtDueEndDate.Enabled = True
            dtxtDueEndDate.Visible = True
            
    End Select

End Sub

Private Sub cmbPODueDateCrit_GotFocus()
    
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbPOrdDateCrit_Click()

    On Error Resume Next
    
    Select Case (cmbPOrdDateCrit.ItemData(cmbPOrdDateCrit.ListIndex))
        Case CRIT_LESSTHAN, CRIT_GREATERTHAN, CRIT_EQUALS
            dtxtPOrdStartDate.Enabled = True
            dtxtPOrdStartDate.Visible = True
            lblPOTolbl.Visible = False
            dtxtPOrdEndDate.Enabled = False
            dtxtPOrdEndDate.Visible = False
            
        Case CRIT_ALL
            dtxtPOrdStartDate.Enabled = False
            dtxtPOrdStartDate.Visible = False
            lblPOTolbl.Visible = False
            dtxtPOrdEndDate.Enabled = False
            dtxtPOrdEndDate.Visible = False
            
        Case CRIT_FROM
            dtxtPOrdStartDate.Enabled = True
            dtxtPOrdStartDate.Visible = True
            lblPOTolbl.Visible = True
            dtxtPOrdEndDate.Enabled = True
            dtxtPOrdEndDate.Visible = True
            
    End Select
    
End Sub

Private Sub cmbPOrdDateCrit_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbPOStatus_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbSMDateCrit_Click()

    On Error Resume Next
    
    Select Case (cmbSMDateCrit.ItemData(cmbSMDateCrit.ListIndex))
        Case CRIT_LESSTHAN, CRIT_GREATERTHAN, CRIT_EQUALS
            dtxtSMStartDate.Enabled = True
            dtxtSMStartDate.Visible = True
            lblSMToLbl.Visible = False
            dtxtSMEndDate.Enabled = False
            dtxtSMEndDate.Visible = False
            
        Case CRIT_ALL
            dtxtSMStartDate.Enabled = False
            dtxtSMStartDate.Visible = False
            lblSMToLbl.Visible = False
            dtxtSMEndDate.Enabled = False
            dtxtSMEndDate.Visible = False
            
        Case CRIT_FROM
            dtxtSMStartDate.Enabled = True
            dtxtSMStartDate.Visible = True
            lblSMToLbl.Visible = True
            dtxtSMEndDate.Enabled = True
            dtxtSMEndDate.Visible = True
            
    End Select
    
End Sub

Private Sub cmbSMDateCrit_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbSMRefCrit_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbSMType_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbStatus_GotFocus()
    SendKeys (KEY_DROPDOWN)

End Sub

Private Sub cmbStSearch_CloseUp()

    If cmbStSearch.ListIndex = -1 Then Exit Sub
    
    cmbStSearch.Row = cmbStSearch.ListIndex
    cmbStSearch.Text = cmbStSearch.List(cmbStSearch.Row)
    Call DisplayItem
    cmbStSearch.Visible = False
    mblnIgnoreFirstNext = True

End Sub

Private Sub cmbStSearch_GotFocus()
  cmbStSearch.ListDown = True

End Sub

Private Sub cmbStSearch_LostFocus()
  cmbStSearch.Visible = False

End Sub


Private Sub cmdEnqOnDisplay_Click()

    Load frmMaintDisplay
    frmMaintDisplay.LoadLocations (lblPartCode.Caption)
    Call frmMaintDisplay.Show(vbModal)

End Sub


Private Sub LoadSuppliers()
Dim cSuppList As cSuppliers

    Set cSuppList = goDatabase.CreateBusinessObject(CLASSID_SUPPLIERS)
    
    Call cSuppList.GetList
    Call cSuppList.MoveFirst
    
    While Not (cSuppList.EndOfList)
        Call cmbSupplier.AddItem(cSuppList.EntryString & vbTab & Left$(cSuppList.SupplierNo, 2) & Val(Mid$(cSuppList.SupplierNo, 3)))
        Call cSuppList.MoveNext
        
    Wend
    
End Sub

Private Sub cmdExit_Click()
    Unload Me

End Sub

Private Sub cmdNext_Click()

    If (Not mblnIgnoreFirstNext) Then Call MenuNext
    mblnIgnoreFirstNext = False
    
End Sub

Private Sub cmdNext_LostFocus()
    mblnIgnoreFirstNext = False

End Sub

Private Sub cmdPOApply_Click()
Dim oPOLine      As cPurchaseLine
Dim oPOrderBO    As cPurchaseOrder
Dim colMatches   As Collection
Dim dteDateValue As Date
Dim lngComp      As Long
Dim lngLineNo    As Long
Dim intPnt       As Integer
Dim colPOHeaders As Collection

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdPOApply_Click"

On Error GoTo Bad_Apply
            
    sprdPOTrans.Col = COL_PO_SHOW
    sprdPOTrans.ColHidden = True
    sprdPOTrans.Col = COL_PO_WEIGHT
    sprdPOTrans.ColHidden = True
    sprdPOTrans.MaxRows = 0
    sprdPOTrans.MaxCols = COL_PO_KEY
    
    For intPnt = 1 To sprdPOTrans.MaxCols
        sprdPOTrans.Col = intPnt
        sprdPOTrans.Visible = True
        
    Next intPnt
    
'    cmdShowdetails.Visible = False
'    cmdDetails.Visible = False

    If dtxtPOrdStartDate.BackColor = RGB_RED Then 'invalid Order date entered so raise prompt
        Call MsgBox("Invalid Order Date value entered" & vbCrLf & "Clear out value or enter valid date", vbExclamation, "Perform Enquiry")
        dtxtPOrdStartDate.SetFocus
        Exit Sub
        
    End If
    
    If dtxtDueStartDate.BackColor = RGB_RED Then 'invalid Order date entered so raise prompt
        Call MsgBox("Invalid Due Date value entered" & vbCrLf & "Clear out value or enter valid date", vbExclamation, "Perform Enquiry")
        dtxtDueStartDate.SetFocus
        Exit Sub
        
    End If
    
    'Perform retrieval
    Screen.MousePointer = vbHourglass
    ucpbItem.Caption1 = "Accessing data"
    ucpbItem.Visible = True
    
    DoEvents
    
    ' read database CLASSID_PURCHASEORDER  - PURHDR
    Set oPOLine = goDatabase.CreateBusinessObject(CLASSID_POLINES)
    Call oPOLine.AddLoadFilter(CMP_EQUAL, FID_POLINES_PartCode, lblPartCode.Caption)
            
    If LenB(txtPOOrderNo.Text) <> 0 Then
        If Len(txtPOOrderNo.Text) < 6 Then txtPOOrderNo.Text = Format$(Val(txtPOOrderNo.Text), "000000")
        Call oPOLine.AddLoadFilter(CMP_EQUAL, FID_POLINES_PurchaseOrderNo, txtPOOrderNo.Text)
        
    End If
    
    'Set Purchase Order criteria
    Select Case (cmbPOStatus.ItemData(cmbPOStatus.ListIndex))
        Case (STATUS_PO_ALL):
        
        Case (STATUS_PO_OPEN):
            Call oPOLine.AddLoadFilter(CMP_EQUAL, FID_POLINES_Complete, False)
            
        Case (STATUS_PO_CANCEL):
            Call oPOLine.AddLoadFilter(CMP_EQUAL, FID_POLINES_Complete, True)
            
    End Select
    
    'Set Due date criteria
    If cmbPODueDateCrit.ItemData(cmbPODueDateCrit.ListIndex) <> CRIT_ALL Then
        dteDateValue = GetDate(dtxtDueStartDate.Text)
        If Year(dteDateValue) > 1899 Then
            Select Case (cmbPODueDateCrit.ItemData(cmbPODueDateCrit.ListIndex))
                Case Is = CRIT_LESSTHAN
                    lngComp = CMP_LESSEQUALTHAN
                    
                Case CRIT_GREATERTHAN, CRIT_FROM
                    lngComp = CMP_GREATEREQUALTHAN
                    
                Case Else
                    lngComp = CMP_EQUAL
                    
            End Select
            
            Call oPOLine.AddLoadFilter(lngComp, FID_POLINES_DueDate, dteDateValue)
            
        End If
        
        If (cmbPODueDateCrit.ItemData(cmbPODueDateCrit.ListIndex) = CRIT_FROM) Then
            Call oPOLine.AddLoadFilter(CMP_LESSEQUALTHAN, FID_POLINES_DueDate, GetDate(dtxtDueEndDate.Text))
            
        End If
    End If
    
    'Set Order date criteria
    If cmbPOrdDateCrit.ItemData(cmbPOrdDateCrit.ListIndex) <> CRIT_ALL Then
        dteDateValue = GetDate(dtxtPOrdStartDate.Text)
        If Year(dteDateValue) > 1899 Then
            Select Case (cmbPOrdDateCrit.ItemData(cmbPOrdDateCrit.ListIndex))
                Case Is = CRIT_LESSTHAN
                    lngComp = CMP_LESSEQUALTHAN
                    
                Case CRIT_GREATERTHAN, CRIT_FROM
                    lngComp = CMP_GREATEREQUALTHAN
                    
                Case Else
                    lngComp = CMP_EQUAL
                    
            End Select
            
            Call oPOLine.AddLoadFilter(lngComp, FID_POLINES_OrderDate, dteDateValue)
            
        End If
        
        If (cmbPOrdDateCrit.ItemData(cmbPOrdDateCrit.ListIndex) = CRIT_FROM) Then
            Call oPOLine.AddLoadFilter(CMP_LESSEQUALTHAN, FID_POLINES_OrderDate, GetDate(dtxtPOrdEndDate.Text))
            
        End If
    End If
               
'************
    'Make call on entries to return collection of all entries
    Set colPOHeaders = oPOLine.LoadMatches
    Set colMatches = New Collection
    
    If colMatches.Count > 0 Then ucpbItem.Max = colMatches.Count * 2
    
    ucpbItem.Value = 0
    ucpbItem.Caption1 = "Accessing Order Headers"
    DoEvents
    
    For lngLineNo = 1 To colPOHeaders.Count Step 1
        ucpbItem.Value = lngLineNo
        'Create new Order Header to add to collection
        Set oPOrderBO = goDatabase.CreateBusinessObject(CLASSID_PURCHASEORDER)
        Call oPOrderBO.AddLoadFilter(CMP_EQUAL, FID_PURCHASEORDER_OrderNumber, colPOHeaders(lngLineNo).PurchaseOrderNo)
        'Create list of fields that should be selected from database
        Call SetPOrderBOLoadFields(oPOrderBO)
        Call oPOrderBO.LoadMatches
        Call colMatches.Add(oPOrderBO)
        
    Next lngLineNo
    
'*******
    If cmbSupplier.ListCount = 0 Then Call LoadSuppliers
    lblNoMatches.Caption = colPOHeaders.Count
    
    'Display number of matches found
    ucpbItem.Caption1 = "Displaying data"
    DoEvents
    cmbSupplier.SearchMethod = SearchMethodPartialMatch
    cmbSupplier.SearchIgnoreCase = True
    
    'Now that data has been accessed, display on screen
    Call DisplayPOData(colMatches, sprdPOTrans, cmbSupplier, ucpbItem)
    
    On Error Resume Next
    
    If sprdPOTrans.MaxRows > 0 Then
        ' SORT the grid
        sprdPOTrans.ColWidth(COL_PO_STATUS) = sprdPOTrans.MaxTextColWidth(COL_PO_STATUS) + 1
        sprdPOTrans.Row = 1
        sprdPOTrans.Col = COL_PO_ONO
        Debug.Print sprdPOTrans.Text
        sprdPOTrans.Col = COL_PO_ONO_RELNO
        Debug.Print sprdPOTrans.Text
'        Call sprdPOTrans_Click(sprdPOTrans.Col, sprdPOTrans.Row)
        If sprdPOTrans.Visible = True Then sprdPOTrans.SetFocus
        cmdPOPrint.Visible = True
'        cmdShowdetails.Visible = True
'        cmdDetails.Visible = True

    Else
        If sprdPOTrans.Visible = True Then txtPOOrderNo.SetFocus
        cmdPOPrint.Visible = False
        
    End If
    
    ' Hide columns
    Screen.MousePointer = vbNormal
    ucpbItem.Visible = False
    cmdPOReset.Visible = True
    Exit Sub
    
Bad_Apply:
    
    Screen.MousePointer = vbNormal
    ucpbItem.Visible = False
    lblNoMatches.Caption = "Error..." & Err.Description
    Call MsgBox(lblNoMatches.Caption)
    Exit Sub
    Resume Next

End Sub 'cmdPOApply_Click

Private Sub cmdPOPrint_Click()
Dim strHeader   As String
Dim strFooter   As String
Dim oPrintStore As cStore

    ' get name and address for current store to put at top of document
    If (oPrintStore Is Nothing) Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.Load
        mstrPrintStore = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & oPrintStore.AddressLine1
    
    End If
   
    strHeader = mstrPrintStore & "/n/cItem Enquiry - Purchase Orders Report"
    strHeader = strHeader & "/n/c for " & lblPartCode.Caption & " " & lblDescription.Caption & " " & lblSize.Caption
        
    If LenB(txtPOOrderNo.Text) <> 0 Then strHeader = strHeader & " on Order No : " & txtPOOrderNo.Text
    strHeader = strHeader & "/nFor status - '" & cmbStatus.Text & "'"
        
    'Add date criteria
    If (cmbPOrdDateCrit.Enabled = True) Then
        strHeader = strHeader & " /nDated "
        Select Case (cmbPOrdDateCrit.ItemData(cmbPOrdDateCrit.ListIndex))
            Case (CRIT_ALL): strHeader = strHeader & ": ALL"
            Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtPOrdStartDate.Text & " to " & dtxtPOrdEndDate.Text
            Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtPOrdStartDate.Text
            Case Else: strHeader = strHeader & cmbPOrdDateCrit.Text & ": " & dtxtPOrdStartDate.Text
        
        End Select
        
        strHeader = strHeader & Space$(20) & " Due Dates "
        
    Else
        strHeader = strHeader & " /nDue Dates "
    
    End If
    
    Select Case (cmbPODueDateCrit.ItemData(cmbPODueDateCrit.ListIndex))
        Case (CRIT_ALL): strHeader = strHeader & ": ALL"
        Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtDueStartDate.Text & " to " & dtxtDueEndDate.Text
        Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtDueStartDate.Text
        Case Else: strHeader = strHeader & cmbPODueDateCrit.Text & ": " & dtxtDueStartDate.Text
    
    End Select
    
    ' /c is Centre text         /fb1 is Font Bold on
    sprdPOTrans.PrintHeader = "/c/fb1" & strHeader
    sprdPOTrans.PrintJobName = "Item Enquiry - Purchase Orders Report"
    
    ' Set up Footer - program version no date/time printed
    sprdPOTrans.PrintFooter = GetFooter

    Call sprdPOTrans.PrintSheet
    cmdPOReset.Value = True

End Sub

Private Sub cmdPOReset_Click()

    cmbPOrdDateCrit.ListIndex = mlngAllPODates
    cmbPODueDateCrit.ListIndex = mlngAllPODueDates
    cmbPOStatus.ListIndex = mlngPOAllStatus
    dtxtPOrdStartDate.Text = "--/--/--"
    dtxtPOrdEndDate.Text = "--/--/--"
    dtxtDueStartDate.Text = "--/--/--"
    dtxtDueEndDate.Text = "--/--/--"
    cmdPOPrint.Visible = False
    sprdPOTrans.MaxRows = 0
    sprdPOTrans.LeftCol = 1
    txtPOOrderNo.Text = vbNullString
    DoEvents
    
    If txtPOOrderNo.Enabled Then
        Call txtPOOrderNo.SetFocus
    
    End If
    
End Sub

Private Sub cmdPrev_Click()
    'Call mnuIPrev_Click
    Call MenuPrevious
    
End Sub

'Added 1.3.0 optional parameter
Private Sub DisplayItem(Optional ByVal blnFromUseList As Boolean = False)
Dim lngLineNo   As Long
Dim lngRowNo    As Long
Dim dblMargin   As Double
  
    On Error GoTo bad_Item

    Screen.MousePointer = vbHourglass
    'Get full Item BO from DB
    cmbStSearch.Col = 0
    
    lblItemID.Caption = cmbStSearch.ColText
    Set cItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
    Call cItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, lblItemID.Caption)
    Call cItem.LoadMatches
    
    'Reset screen
    Call ClearScreen
    
    lblPartCode.Caption = cItem.PartCode
    lblDescription.Caption = cItem.Description
'  lblQtyReserved.Caption = Format$(cItem.QuantityReserved, "0.000")
  
    lblWeight.Caption = cItem.Weight
    lblMinLevel.Caption = cItem.MinQuantity
    lblMaxLevel.Caption = cItem.MaxQuantity
    lblLastSold.Caption = DisplayDate(cItem.LastSold, False)
    lblSellPrice.Caption = cItem.NormalSellPrice
    lblSellPriceMargin.Caption = cItem.NormalSellPrice
    dblMargin = Val(Format$(cItem.NormalSellPrice / 1.175, "0.000"))
    If (dblMargin = 0) Then
        lblMargin.Caption = 0
    Else
        lblMargin.Caption = Format$((dblMargin - cItem.CostPrice) / dblMargin * 100, "0.00")
    End If
    lblCostPrice.Caption = cItem.CostPrice
    
    'Prices
    lblSellPriceMarginPrior.Caption = cItem.PriorSellPrice
    lbl4smPrice.Caption = "n\a"
    lbl4smPricePrior.Caption = "n\a"
    
    'Margins
    'dblMargin = Val(Format$(cItem.SpecialPrice / 1.175, "0.000"))
    'lblTradeMargin.Caption = Format$((dblMargin - cItem.CostPrice) / dblMargin * 100, "0.00")
        
    lbl4smMargin.Caption = "n\a"
    lblPackSize.Caption = cItem.SupplierPackSize
    lblManuProdCode.Caption = cItem.SupplierPartCode
    mstrSuppNo = cItem.SupplierNo
    lblLastOrdered.Caption = DisplayDate(cItem.LastOrdered, False)
    lblLastReceived.Caption = DisplayDate(cItem.LastReceived, False)
    lblBuyInPacksOf.Caption = cItem.SupplierPackSize
    lblSellInPacksOf.Caption = cItem.SupplierPackSize
    lblSize.Caption = cItem.ItemVolume
    lblAtHand.Caption = Format$(cItem.QuantityAtHand, mstrDispFormat)
    lblStockAvail.Caption = Format$(cItem.QuantityAtHand, mstrDispFormat)
    lblAllowReturns.Caption = "Yes"
    
    'Changed to true for discount allowed - appears to be wrong way round
    lblDiscAllowed.Caption = "Yes"
    lbl4MDisc.Caption = "No"
    lblManuProdCode.Caption = cItem.SupplierPartCode
    
    'Fill In Summary TAB
    lblQtyOnOrder.Caption = Format$(cItem.QuantityOnOrder, mstrDispFormat)
    
'    lblMargin.Caption = Format$((Val(lblSellPriceEx.Caption) - ntxtCostPrice.Value) / Val(lblSellPriceEx.Caption) * 100, "0.000")
    
    If (tabDetails.Tab = TAB_SUPPLIER) Then Call DisplaySupplier
    
    If (tabDetails.Tab = TAB_TEXT) Then
        lblTechInfo.Caption = cItem.Description
        blnTechLoaded = True
        
    End If
        
    'fill in Period Totals - Fill in Values first
    sprdPeriodTot.Row = 1
    sprdPeriodTot.Col = 1
    sprdPeriodTot.Text = cItem.ValueSoldYesterday
    sprdPeriodTot.Col = 2
    sprdPeriodTot.Text = cItem.ValueSoldThisPeriod
    sprdPeriodTot.Col = 3
    sprdPeriodTot.Text = cItem.ValueSoldThisYear
    sprdPeriodTot.Col = 4
    sprdPeriodTot.Text = cItem.ValueSoldLastYear
    'fill in Period Totals - Fill in Units first
    sprdPeriodTot.Row = 2
    sprdPeriodTot.Col = 1
    sprdPeriodTot.Text = cItem.UnitsSoldYesterday
    sprdPeriodTot.Col = 2
    sprdPeriodTot.Text = cItem.UnitsSoldThisPeriod
    sprdPeriodTot.Col = 3
    sprdPeriodTot.Text = cItem.UnitsSoldThisYear
    sprdPeriodTot.Col = 4
    sprdPeriodTot.Text = cItem.UnitsSoldLastYear
    sprdPeriodTot.Col = 5
    sprdPeriodTot.Text = cItem.UnitsSoldLastPeriod
    sprdPeriodTot.Col = 8
    sprdPeriodTot.Text = cItem.DaysOutStockPeriod
'    sprdPeriodTot.Col = 9
'    sprdPeriodTot.Text = cItem.DaysOutStockLastPeriod
'    sprdPeriodTot.Col = 10
'    sprdPeriodTot.Text = cItem.DaysOutStockPriorPeriod

    sprdWeekTot.MaxRows = 13
    sprdWeekTot.Col = 3
    
    For lngLineNo = 1 To 13 Step 1
        sprdWeekTot.Row = lngLineNo
        sprdWeekTot.Text = cItem.UnitsSoldWeek(lngLineNo)
        
    Next lngLineNo

    If (LenB(cItem.Status) <> 0) Then
        cmbStatus.ListIndex = 1
        For lngLineNo = 0 To cmbStatus.ListCount - 1 Step 1
            If (Left$(cmbStatus.List(lngLineNo), 2) = cItem.Status) Then
                cmbStatus.ListIndex = lngLineNo
                Exit For
                
            End If
            
        Next lngLineNo
    End If

    cmdPOReset.Value = True
    cmdSMReset.Value = True

    tabDetails.Tab = TAB_TEXT
    ucpbItem.Visible = False
    Screen.MousePointer = vbNormal

Exit Sub

bad_Item:

  If (Err.Number <> 94) Then
      Call Err.Report(MODULE_NAME, "Display_Item", 1, True, "Display Item Details", "Error retreiving detected :-")
      Screen.MousePointer = 0
      ucpbItem.Visible = False
      Exit Sub
      
  End If
  
  Resume Next
  
End Sub


Private Sub fill_in_combos()

    On Error GoTo bad_lookup_combos
  
    ucpbItem.Caption1 = "Loading Units of Measure"
    ucpbItem.Refresh
    ucpbItem.Caption1 = "Loading VAT Rates"
    ucpbItem.Refresh
    Exit Sub
  
bad_lookup_combos:
  
    MsgBox (Error$), vbExclamation, "Error accessing General Look-Ups"
    Exit Sub
  
End Sub

Private Sub cmdEnqOnReturn_Click()
    
    Load frmDetails
    Call frmDetails.DisplayOpenReturns(lblPartCode.Caption, lblDescription.Caption)

End Sub


Private Sub cmdEnqBranches_Click()

    Load frmDetails
    Call frmDetails.Show(vbModal)

End Sub

Private Sub cmdEnqOnOrder_Click()

    Load frmDetails
    Call frmDetails.DisplayPurchaseOrders(lblPartCode.Caption, lblDescription.Caption)

End Sub

Private Function GetPrintHeader() As String
Dim oPrintStore  As cStore
Dim strHeader    As String

    If (LenB(mstrPrintStore) = 0) Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.Load
        mstrPrintStore = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & oPrintStore.AddressLine1
        
    End If
   
    strHeader = mstrPrintStore & "  -  " & "Sales Order Report" & "/n/cStatus Report on "
    Set oPrintStore = Nothing
    GetPrintHeader = strHeader

End Function

Private Sub cmdSMApply_Click()
Dim dteStartDate As Date
Dim dteEndDate   As Date

    'Check that we have valid dates
    If dtxtSMStartDate.Visible And (dtxtSMStartDate.Text = "--/--/--") Then
        Call MsgBoxEx(dtxtSMStartDate.Text & " is not a valid date.", vbOKOnly + vbInformation, "Invalid Date")
        Exit Sub
        
    End If
    
    If dtxtSMEndDate.Visible And (dtxtSMEndDate.Text = "--/--/--") Then
        Call MsgBoxEx(dtxtSMEndDate.Text & " is not a valid date.", vbOKOnly + vbInformation, "Invalid Date")
        Exit Sub
        
    End If

    Screen.MousePointer = 11
    ucpbItem.Title = "Accessing Item History"
    ucpbItem.Caption1 = "Retrieving Transaction History within Criteria"
    ucpbItem.Visible = True
    'retreive starting date for period if date left blank
    
    'Set date criteria
    If (cmbSMDateCrit.ItemData(cmbSMDateCrit.ListIndex) <> CRIT_ALL) Then
        Select Case (cmbSMDateCrit.ItemData(cmbSMDateCrit.ListIndex))
            Case (CRIT_GREATERTHAN):
                dteStartDate = GetDate(dtxtSMStartDate.Text)
                
            Case (CRIT_LESSTHAN):
                dteEndDate = GetDate(dtxtSMEndDate.Text)
                
            Case (CRIT_FROM):
                dteStartDate = GetDate(dtxtSMStartDate.Text)
                dteEndDate = GetDate(dtxtSMEndDate.Text)
                
            Case Else:
                dteStartDate = GetDate(dtxtSMStartDate.Text)
        
        End Select
    End If
    
    DoEvents
  
    Call FillInTransactions(lblPartCode.Caption, cmbSMDateCrit.ItemData(cmbSMDateCrit.ListIndex), dteStartDate, dteEndDate, cmbSMType.ItemData(cmbSMType.ListIndex), sprdSMTrans, ucpbItem, True)
    
    If (sprdSMTrans.MaxRows > 0) Then cmdSMPrint.Visible = True
    ucpbItem.Visible = False
    
    If (sprdSMTrans.Enabled = True) And (sprdSMTrans.Visible = True) Then Call sprdSMTrans.SetFocus
    Screen.MousePointer = 0


End Sub

Private Sub cmdSMPrint_Click()
    
    sprdSMTrans.PrintFooter = GetFooter
    sprdSMTrans.PrintRowHeaders = False
    sprdSMTrans.PrintGrid = False
    sprdSMTrans.PrintHeader = GetSMPrintHeader
    sprdSMTrans.PrintOrientation = PrintOrientationPortrait
    Call sprdSMTrans.PrintSheet
    cmdSMReset.Value = True

End Sub

Private Function GetSMPrintHeader() As String
Dim oPrintStore  As cStore
Dim strHeader    As String

    If (LenB(mstrPrintStore) = 0) Then
        Set oPrintStore = goSession.Database.CreateBusinessObject(CLASSID_STORE)
        Call oPrintStore.AddLoadFilter(CMP_EQUAL, FID_STORE_StoreNumber, goSession.CurrentEnterprise.IEnterprise_StoreNumber)
        Call oPrintStore.Load
        mstrPrintStore = "Store No: " & Str$(goSession.CurrentEnterprise.IEnterprise_StoreNumber) & "  " & oPrintStore.AddressLine1
        
    End If
   
    strHeader = mstrPrintStore & "/n/cStock Movements Report - " & cmbSMType.Text
    strHeader = strHeader & "/n/c for " & lblPartCode.Caption & " " & lblDescription.Caption & " " & lblSize.Caption
        
    'Add date criteria
    If cmbSMDateCrit.Enabled Then
        strHeader = strHeader & "/n/c Dated "
        Select Case (cmbSMDateCrit.ItemData(cmbSMDateCrit.ListIndex))
            Case (CRIT_ALL): strHeader = strHeader & ": ALL"
            Case (CRIT_FROM): strHeader = strHeader & "From " & dtxtSMStartDate.Text & " to " & dtxtSMEndDate.Text
            Case (CRIT_EQUALS): strHeader = strHeader & ": " & dtxtSMStartDate.Text
            Case Else: strHeader = strHeader & cmbSMDateCrit.Text & ": " & dtxtSMStartDate.Text
            
        End Select
    
    End If
    
    Set oPrintStore = Nothing
    GetSMPrintHeader = strHeader

End Function 'GetSMPrintHeader


Private Sub cmdSMReset_Click()

Dim lngType As Long
    
    sprdSMTrans.MaxRows = 0
    dtxtSMStartDate.Text = "--/--/--"
    dtxtSMEndDate.Text = "--/--/--"
    cmdSMPrint.Visible = False
    cmbSMDateCrit.ListIndex = mlngSMAllDates
    cmbSMType.ListIndex = mlngSMAllMoves
    DoEvents
    If cmbSMDateCrit.Enabled Then Call cmbSMDateCrit.SetFocus

End Sub

Private Sub dtxtDueEndDate_LostFocus()
    
    If LenB(dtxtDueEndDate.Text) <> 0 Then
        If Year(GetDate(dtxtDueEndDate.Text)) = 1899 Then
            dtxtDueEndDate.BackColor = RGB_RED
        Else
            dtxtDueEndDate.BackColor = RGB_WHITE
        End If
        
    Else
        dtxtDueEndDate.BackColor = RGB_WHITE
        
    End If

End Sub

Private Sub dtxtDueStartDate_GotFocus()
    dtxtDueStartDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtDueStartDate_LostFocus()
    
    If LenB(dtxtDueStartDate.Text) <> 0 Then
        If Year(GetDate(dtxtDueStartDate.Text)) = 1899 Then
            dtxtDueStartDate.BackColor = RGB_RED
        Else
            dtxtDueStartDate.BackColor = RGB_WHITE
        End If
        
    Else
        dtxtDueStartDate.BackColor = RGB_WHITE
        
    End If

End Sub

Private Sub dtxtPOrdEndDate_GotFocus()
    dtxtPOrdEndDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtSMEndDate_GotFocus()
    dtxtSMEndDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtSMEndDate_LostFocus()
    
    If LenB(dtxtSMEndDate.Text) <> 0 Then
        If Year(GetDate(dtxtSMEndDate.Text)) = 1899 Then
            dtxtSMEndDate.BackColor = RGB_RED
        Else
            dtxtSMEndDate.BackColor = RGB_WHITE
        End If
        
    Else
        dtxtSMEndDate.BackColor = RGB_WHITE
        
    End If

End Sub

Private Sub dtxtSMStartDate_GotFocus()
    
    dtxtSMStartDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtSMStartDate_LostFocus()
    
    If LenB(dtxtSMStartDate.Text) <> 0 Then
        If Year(GetDate(dtxtSMStartDate.Text)) = 1899 Then
            dtxtSMStartDate.BackColor = RGB_RED
        Else
            dtxtSMStartDate.BackColor = RGB_WHITE
        End If
        
    Else
        dtxtSMStartDate.BackColor = RGB_WHITE
        
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If (KeyCode = vbKeyF1) Then uchlHelpKeys.Visible = True
    If (KeyCode = vbKeyF2) Then cmdFuzzy.Value = True
    
    If (KeyCode = vbKeyF3) Then
        If cmdPOReset.Enabled Then cmdPOReset.Value = True
        If cmdSMReset.Enabled Then cmdSMReset.Value = True
    
    End If
    
    If (KeyCode = vbKeyF7) Then
        If cmdPOApply.Enabled Then cmdPOApply.Value = True
        If cmdSMApply.Enabled Then cmdSMApply.Value = True
    
    End If
    
    If (KeyCode = vbKeyF9) Then
        If (cmdPOPrint.Enabled And cmdPOPrint.Visible) Then cmdPOPrint.Value = True
        If (cmdSMPrint.Enabled And cmdSMPrint.Visible) Then cmdSMPrint.Value = True
        
    End If
    
    If (KeyCode = 190) And (Shift = 2) And cmdNext.Visible Then cmdNext.Value = True
    If (KeyCode = 188) And (Shift = 2) And cmdPrev.Visible Then cmdPrev.Value = True
    If (KeyCode = vbKeyF10) Then cmdExit.Value = True
  
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
Dim blnMove As Boolean

    blnMove = False
    If (KeyAscii = vbKeyReturn) Then
        If TypeOf ActiveControl Is ucDateText Then blnMove = True
        If TypeOf ActiveControl Is Slider Then blnMove = True
        If TypeOf ActiveControl Is TextBox Then blnMove = True
        If TypeOf ActiveControl Is ComboBox Then blnMove = True
        If TypeOf ActiveControl Is CheckBox Then blnMove = True
        If TypeOf ActiveControl Is ucNumberText Then blnMove = True
        If TypeOf ActiveControl Is SSTab Then blnMove = True
        
        If blnMove Then
            KeyAscii = 0
            SendKeys (vbTab)
            DoEvents
        
        End If
    End If

    blnMove = False
    If (KeyAscii = vbKeyEscape) Then
        If (ActiveControl.Name = "txtPOOrderNo") Or (ActiveControl.Name = "cmbCOrdDateCrit") _
             Or (ActiveControl.Name = "cmbSMDateCrit") Then Exit Sub
             
        If TypeOf ActiveControl Is ucDateText Then blnMove = True
        If TypeOf ActiveControl Is Slider Then blnMove = True
        If TypeOf ActiveControl Is TextBox Then blnMove = True
        If TypeOf ActiveControl Is ComboBox Then blnMove = True
        If TypeOf ActiveControl Is CheckBox Then blnMove = True
        If TypeOf ActiveControl Is ucNumberText Then blnMove = True
        If TypeOf ActiveControl Is SSTab Then blnMove = True
        If TypeOf ActiveControl Is CommandButton Then blnMove = True
        
        If blnMove Then
            KeyAscii = 0
            Call SendKeys("+{tab}")
            DoEvents
            
        End If
    End If

End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyF1) Then
        uchlHelpKeys.Visible = False
        
    End If

End Sub

Private Sub Form_Load()
Const PRM_MULTIPLE_SUPPLIERS As Long = 118
Const PRM_DISP_BRANCH_QTY As Long = 310
Const PRM_DISP_CUSTORD_QTY As Long = 311

Dim lngItemNo       As Long
Dim no_cols         As Double 'used for setting Balances width
Dim oEnterprise     As cSystemDates
Dim lngBackColor    As Long
Dim lngQtyDecNum    As Long
Dim lngValueDecNum  As Long

'On Error Resume Next

    'set up Purchase Order enquiry
    Call SetUpHidePOColumns
    Call HidePOLineItems
    Call HidePOColumns(sprdPOTrans)
    
    'configure Item Search
    ucifSearch.ShowSelectActiveOnly = False
    ucifSearch.ShowSupplier = False
    
    Call uchlHelpKeys.AddHotKey("F2", "Fuzzy Match")
    Call uchlHelpKeys.AddHotKey("F3", "Item list select")
    
    ucpbItem.Title = "Loading Editor Enquiry"
    ucpbItem.Visible = True
    ucpbItem.Caption1 = "Loading"
    ucpbItem.Value = 0
    ucpbItem.Max = 5
        
    Me.Show
    DoEvents
    
    lTabTop = tabDetails.Top
  
    ucpbItem.Caption1 = "Setting Up Periods"
    ucpbItem.Value = 1
    ucpbItem.Refresh
    
    Set goRoot = GetRoot
    Call InitialiseStatusBar(sbStatus)
    Call ConfigureFlags
    
    lngBackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    BackColor = lngBackColor
    frDetails.BackColor = lngBackColor
    tabDetails.BackColor = lngBackColor
    fraButtons.BackColor = lngBackColor
    
    lngQtyDecNum = goSession.GetParameter(PRM_QTY_DEC_PLACES)
    mstrDispFormat = "0"
    If (lngQtyDecNum > 0) Then
        mstrDispFormat = mstrDispFormat & Replace(Space$(lngQtyDecNum), " ", "0")
        
    End If
    
    lngValueDecNum = goSession.GetParameter(PRM_VALUE_DEC_PLACES)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    RGBMSGBox_PromptColour = goSession.GetParameter(PRM_MSGBOX_PROMPT_COLOUR)
    RGBMsgBox_WarnColour = goSession.GetParameter(PRM_MSGBOX_WARN_COLOUR)
    
'    tabDetails.TabVisible(TAB_SUPPLIER) = goSession.GetParameter(118)
    
    Me.Show
    
'    Set cItems = goDatabase.createbusinessObject(CLASSID_INVENTORYS)
    
    Set oEnterprise = goDatabase.CreateBusinessObject(CLASSID_SYSTEMDATES)
    Call oEnterprise.IBo_SetLoadField(CMP_EQUAL, FID_SYSTEMDATES_ID, "01")
    Call oEnterprise.IBo_Load
    
    For lngItemNo = 1 To 13 Step 1
        sprdWeekTot.Row = lngItemNo
        sprdWeekTot.Col = 2
        sprdWeekTot.Text = Format$(oEnterprise.WeekEndDates(lngItemNo), "dd/mm/yy")
        sprdWeekTot.Col = 1
        sprdWeekTot.Text = Format$(DateAdd("d", -7, oEnterprise.WeekEndDates(lngItemNo)), "dd/mm/yy")
        
    Next lngItemNo
    
        
    ucifSearch.UseList = True
    
    ucpbItem.Caption1 = "Retrieving List of Items"
    ucpbItem.Value = 2
    ucpbItem.Refresh
    DoEvents
    
    cmbStSearch.VRowCount = 0
    
    ucpbItem.Caption1 = "Filling in LookUps"
    ucpbItem.Value = 3
    ucpbItem.Refresh
    Call fill_in_combos
    
    ucpbItem.Caption1 = "Resetting Display"
    ucpbItem.Value = 4
    ucpbItem.Refresh
    Call ClearScreen
     
    ucpbItem.Caption1 = "Resetting criteria"
    ucpbItem.Value = 5
    ucpbItem.Refresh
    
    Call SetUpCombos
    
    ucpbItem.Caption1 = "Loading item selection"
    ucpbItem.Refresh
    ucpbItem.Visible = False
    
    If LenB(goSession.ProgramParams) = 0 Then
        cmdFuzzy.Value = True
    
    Else
        If (Left$(goSession.ProgramParams, 4) = "SKU=") Then
            Call ucifSearch_Apply(Mid$(goSession.ProgramParams, 5))
        
        End If
    End If
    
End Sub

Private Sub HidePOLineItems()
                
    sprdPOTrans.Col = COL_PO_LINENO
    sprdPOTrans.ColHidden = True
    sprdPOTrans.Col = COL_PO_PARTCODE
    sprdPOTrans.ColHidden = True
    sprdPOTrans.Col = COL_PO_DESC
    sprdPOTrans.ColHidden = True
    sprdPOTrans.Col = COL_PO_ORDERQTY
    sprdPOTrans.ColHidden = True
    sprdPOTrans.Col = COL_PO_QTYDEL
    sprdPOTrans.ColHidden = True
    sprdPOTrans.Col = COL_PO_QTYBORD
    sprdPOTrans.ColHidden = True

End Sub

Private Sub ConfigureFlags()
Dim lngTop As Long
Dim lngGap As Long

    lngGap = lblFlag2.Top - lblFlag1.Top
    lngTop = lblFlag1.Top
    lblFlag1.Caption = goSession.GetParameter(PRM_FLAG1_LABEL)
    
    If LenB(lblFlag1.Caption) = 0 Then lblFlag1.Caption = "Flag 1"
    
    lngTop = lngTop + lngGap
    lblFlag2.Top = lngTop
    lblFlag2Value.Top = lngTop
    lblFlag2.Caption = goSession.GetParameter(PRM_FLAG2_LABEL)
    
    If LenB(lblFlag2.Caption) = 0 Then lblFlag2.Caption = "Flag 2"
    
    lngTop = lngTop + lngGap
    lblFlag3.Top = lngTop
    lblFlag3Value.Top = lngTop
    lblFlag3.Caption = goSession.GetParameter(PRM_FLAG3_LABEL)
    
    If LenB(lblFlag3.Caption) = 0 Then lblFlag3.Caption = "Flag 3"
    
    lngTop = lngTop + lngGap
    lblFlag4.Top = lngTop
    lblFlag4Value.Top = lngTop
    lblFlag4.Caption = goSession.GetParameter(PRM_FLAG4_LABEL)
    
    If LenB(lblFlag4.Caption) = 0 Then lblFlag4.Caption = "Flag 4"
    
    lngTop = lngTop + lngGap
    lblFlag5.Top = lngTop
    lblFlag5Value.Top = lngTop
    lblFlag5.Caption = goSession.GetParameter(PRM_FLAG5_LABEL)
    
    If LenB(lblFlag5.Caption) = 0 Then lblFlag5.Caption = "Flag 5"
    
End Sub

Private Sub Form_Resize()

    Call DebugMsg(MODULE_NAME, "Resize", endlTraceIn)
    
    If WindowState = vbMinimized Then Exit Sub

    If Width < 12390 Then
        Width = 12390
        Exit Sub
        
    End If
    
    If Height < 7920 Then
        Height = 7920
        Exit Sub
        
    End If
    
    blnRszText = True
    blnRszPOTrans = True
    blnRszCOTrans = True
    blnRszTrans = True
    blnRszPrices = True
    blnRszSupp = True
    blnRszSumm = True
    blnRszOther = True
    
    If Me.Width > ucProgress.Width Then ucProgress.Left = (Me.Width - ucProgress.Width) / 2
    If Me.Height > ucProgress.Height Then ucProgress.Top = (Me.Height - ucProgress.Height) / 2
    
    tabDetails.Width = Me.Width - (tabDetails.Left * 3)
    tabDetails.Height = Me.Height - (sbStatus.Height * 3) - tabDetails.Top - fraButtons.Height
    fraButtons.Width = tabDetails.Width + 120
    fraButtons.Top = tabDetails.Top + tabDetails.Height + 120
    cmdExit.Left = fraButtons.Width - cmdExit.Width - 120
    
    'Move F11 and F4
    Call ResizeTabs
    Call DebugMsg(MODULE_NAME, "Resize", endlTraceOut)

End Sub

Private Sub ResizeTabs()
    
    If (tabDetails.Tab = TAB_PO_ENQ) And blnRszPOTrans Then
        fraPODetails.Width = tabDetails.Width - fraPODetails.Left * 2
        sprdPOTrans.Width = fraPODetails.Width
        fraPODetails.Height = tabDetails.Height - fraPODetails.Top - 120
        sprdPOTrans.Height = fraPODetails.Height - fraPOCriteria.Height
        fraPOCriteria.Top = sprdPOTrans.Height
        fraPOCriteria.Width = fraPODetails.Width
        blnRszPOTrans = False
        
    End If
    
    If (tabDetails.Tab = TAB_SM_TRANS) And blnRszTrans Then
        fraMovements.Width = tabDetails.Width - fraMovements.Left * 2
        sprdSMTrans.Width = fraMovements.Width
        fraMovements.Height = tabDetails.Height - fraMovements.Top - 120
        sprdSMTrans.Height = fraMovements.Height - fraSMCriteria.Height
        fraSMCriteria.Top = sprdSMTrans.Height
        fraSMCriteria.Width = fraMovements.Width
        blnRszTrans = False
        
    End If
    
    If (tabDetails.Tab = TAB_PRICES) And blnRszPrices Then
        fraPrices.Width = tabDetails.Width - fraPrices.Left * 2
        fraPrices.Height = tabDetails.Height - fraPrices.Top - 120
        blnRszPrices = False
        
    End If
    
    If (tabDetails.Tab = TAB_TEXT) And blnRszText Then
        frTechInfo.Width = tabDetails.Width - frTechInfo.Left * 2
        lblTechInfo.Width = frTechInfo.Width
        frTechInfo.Height = tabDetails.Height - frTechInfo.Top - 120
        lblTechInfo.Height = frTechInfo.Height - 240
        blnRszText = False
        
        If (Not blnTechLoaded) And (LenB(lblPartCode.Caption) <> 0) Then
            lblTechInfo.Caption = cItem.Description
            blnTechLoaded = True
            
        End If
    End If
    
    If (tabDetails.Tab = TAB_OTHER) And blnRszOther Then
        fraOtherInfo.Width = tabDetails.Width - fraOtherInfo.Left * 2
        fraOtherInfo.Height = tabDetails.Height - fraOtherInfo.Top - 120
        blnRszOther = False
        
    End If

End Sub

Private Sub lblNoMatches_Change()

    If Val(lblNoMatches.Caption) = 1 Then
        cmdNext.Visible = False
        cmdPrev.Visible = False
        
    Else
        cmdNext.Visible = True
        cmdPrev.Visible = True
        
    End If

End Sub

Private Sub cmdFuzzy_Click()
        
    Screen.MousePointer = vbHourglass
    Call Me.ucifSearch.Initialise(goSession, Me)
    ucifSearch.Visible = True
    Call ucifSearch.FillScreen(ucifSearch)
    If ucifSearch.Visible Then ucifSearch.SetFocus
    Screen.MousePointer = vbDefault

End Sub

Private Sub sprdPeriodTot_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdPeriodTot.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub sprdWeekTot_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdWeekTot.EditMode) Then Call SendKeys("+{tab}")

End Sub

Private Sub tmrPrices_Timer()

    If tabDetails.Tab = TAB_PRICES Then tabDetails.Tab = TAB_TEXT
    tmrPrices.Interval = 0

End Sub

Private Sub txtPOOrderNo_GotFocus()
    txtPOOrderNo.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPOOrderNo_LostFocus()
    txtPOOrderNo.BackColor = RGB_WHITE

End Sub


Private Sub sprdSMTrans_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Call SendKeys("{tab}")
    If KeyAscii = 27 And (Not sprdSMTrans.EditMode) Then Call SendKeys("+{tab}")

End Sub


Private Sub tabDetails_Click(PreviousTab As Integer)
Dim lngTabNo As Long

    'Disable all frames
    fraPODetails.Enabled = False
    fraMovements.Enabled = False
    frSummary.Enabled = False
    fraPrices.Enabled = False
    frTechInfo.Enabled = False
    
    'Enable frame for selected tab
    Select Case (tabDetails.Tab)
        Case (TAB_PO_ENQ)
            fraPODetails.Enabled = True
            If (txtPOOrderNo.Visible And txtPOOrderNo.Enabled) Then txtPOOrderNo.SetFocus
            tmrPrices.Interval = 0
            tmrPrices.Interval = 0
                             
        Case (TAB_SM_TRANS)
            fraMovements.Enabled = True
            If (cmbSMDateCrit.Visible And cmbSMDateCrit.Enabled) Then cmbSMDateCrit.SetFocus
            tmrPrices.Interval = 0
                             
        Case (TAB_STATS)
            frSummary.Enabled = True
            tmrPrices.Interval = 0
                             
        Case (TAB_PRICES)
            fraPrices.Enabled = True
            tmrPrices.Interval = 3000
                             
        Case (TAB_TEXT)
            frTechInfo.Enabled = True
            tmrPrices.Interval = 0
                             
    End Select
           
    Call ResizeTabs
    If (tabDetails.Tab = TAB_SUPPLIER) Then
        Call DisplaySupplier
        
    End If

End Sub

Private Sub DisplaySupplier()
Dim oSupplier As cSupplier
    
    If LenB(lblSupplier.Caption) = 0 Then
        Set oSupplier = goDatabase.CreateBusinessObject(CLASSID_SUPPLIER)
        Call oSupplier.AddLoadFilter(CMP_EQUAL, FID_SUPPLIER_SupplierNo, mstrSuppNo)
        Call oSupplier.AddLoadField(FID_SUPPLIER_SupplierNo)
        Call oSupplier.AddLoadField(FID_SUPPLIER_Name)
        Call oSupplier.LoadMatches
        lblSupplier.Caption = oSupplier.SupplierNo & "-" & oSupplier.Name
        Set oSupplier = Nothing
        
    End If
    
End Sub

Private Sub ucifSearch_Apply(PartCode As String)
    
    'clear out previous entries
    Call cmbStSearch.Clear
    Call cmbStSearch.AddItem(PartCode)
    cmbStSearch.Visible = False
    
    lblNoMatches.Caption = 1
    ucifSearch.Visible = False
    cmbStSearch.ListIndex = 0
    Call DisplayItem

End Sub

Private Sub ucifSearch_Cancel()

    ucifSearch.Visible = False
    If Val(lblNoMatches.Caption) = 0 Then
        If MsgBoxEx("No items selected to view" & vbCrLf & "Exit enquiry now", vbYesNo + vbInformation, Me.Caption, , , , , RGBMSGBox_PromptColour) = vbYes Then
            Call Unload(Me)
            
        Else
            cmdFuzzy.Value = True
            
        End If
    End If

End Sub

Private Sub ucifSearch_LostFocus()
    ucifSearch.Visible = False

End Sub


Private Sub mnuINext_Click()
    Call MenuNext

End Sub

Private Sub MenuNext()
Dim intKeyIn As Long
    
    If cmbStSearch.ListIndex = cmbStSearch.ListCount - 1 Then
        MsgBox ("No more Items in Database"), , "Error moving to next Item"
        Exit Sub
        
    End If
    
    cmbStSearch.ListIndex = cmbStSearch.ListIndex + 1
    Call DisplayItem

End Sub

Private Sub mnuIPrev_Click()
    Call MenuPrevious
    
End Sub

Private Sub MenuPrevious()
Dim intKeyIn As Long
  
    If cmbStSearch.ListIndex = 0 Then
        MsgBox ("No previous Items in Database"), , "Error moving to previous Item"
        Exit Sub
        
    End If
    
    cmbStSearch.ListIndex = cmbStSearch.ListIndex - 1
    Call DisplayItem

End Sub

Private Sub Slddetails_Click()

  If sldDetails.Value < 9 Then sldDetails.Value = 9
  If sldDetails.Value > 95 Then sldDetails.Value = 95
  tabDetails.Top = sldDetails.Value * 40 + 60
  tabDetails.Height = (Screen.Height - 380 * 2) - (tabDetails.Top + 300)
  If tabDetails.Height > 680 Then
'    spltTrans.Height = tabDetails.Height - (560 + Me.cmdTranFilter.Height)
'    sprdSuppliers.Height = spltTrans.Height
    Call ResizeTabs
  Else
  End If

End Sub

Private Sub ucifSearch_UseList(SearchCriteria As Collection, oMatchingItems As Object)
Dim lCritNo As Integer
Dim lFieldID As Long
Dim strRow As String
    
    Screen.MousePointer = vbHourglass
    
    'Remove previous entries
    Call cmbStSearch.Clear
    
    'Link  retrieved list to display list
    With oMatchingItems.Recordset
        If (.RecordCount > 0) Then
            .MoveFirst
            While (Not .EOF)
                strRow = !skun & vbTab & !descr & vbTab & !Status
                cmbStSearch.AddItem strRow
                .MoveNext
                
            Wend
        End If
    End With
    
'    Set cmbStSearch.DataSourceList = oMatchingItems

    cmbStSearch.Col = 0
    cmbStSearch.ColWidth = 10
    cmbStSearch.ColHeaderText = "SKU"
    
    cmbStSearch.Col = 1
    cmbStSearch.ColWidth = 45
    cmbStSearch.ColHeaderText = "DESCRIPTION"
    cmbStSearch.ColSortSeq = 0
    cmbStSearch.ColSortDataType = ColSortDataTypeTextNoCase
    cmbStSearch.ColSorted = SortedAscending
    
    cmbStSearch.Col = 2
    cmbStSearch.ColWidth = 11
    cmbStSearch.ColHeaderText = "STATUS"
    
    'Select and display first item
    lblNoMatches.Caption = cmbStSearch.ListCount
    ucifSearch.Visible = False
    cmbStSearch.ListIndex = 0
    cmbStSearch.Visible = False
    
    If cmbStSearch.ListCount = 0 Then
        Call ClearScreen
    Else
        Call DisplayItem
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub SetUpCombos()
Dim lngItem As Long
    
'CONVERT
    cmbPOrdDateCrit.Clear
    Call cmbPOrdDateCrit.AddItem("After")
    cmbPOrdDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbPOrdDateCrit.AddItem("Before")
    cmbPOrdDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbPOrdDateCrit.AddItem("Dated")
    cmbPOrdDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbPOrdDateCrit.AddItem("All")
    cmbPOrdDateCrit.ItemData(3) = CRIT_ALL
    Call cmbPOrdDateCrit.AddItem("From")
    cmbPOrdDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItem = 0 To cmbPOrdDateCrit.ListCount Step 1
        If cmbPOrdDateCrit.ItemData(lngItem) = CRIT_ALL Then
            cmbPOrdDateCrit.ListIndex = lngItem
            mlngAllPODates = lngItem
            Exit For
        End If
    Next lngItem

'CONVERT
    cmbPODueDateCrit.Clear
    Call cmbPODueDateCrit.AddItem("After")
    cmbPODueDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbPODueDateCrit.AddItem("Before")
    cmbPODueDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbPODueDateCrit.AddItem("Dated")
    cmbPODueDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbPODueDateCrit.AddItem("All")
    cmbPODueDateCrit.ItemData(3) = CRIT_ALL
    Call cmbPODueDateCrit.AddItem("From")
    cmbPODueDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItem = 0 To cmbPODueDateCrit.ListCount Step 1
        If cmbPODueDateCrit.ItemData(lngItem) = CRIT_ALL Then
            cmbPODueDateCrit.ListIndex = lngItem
            mlngAllPODueDates = lngItem
            Exit For
            
        End If
    Next lngItem


    For lngItem = 0 To cmbPOStatus.ListCount Step 1
        If cmbPOStatus.ItemData(lngItem) = mlngAllPOStatus Then
            cmbPOStatus.ListIndex = lngItem
            mlngAllPOStatus = lngItem
            Exit For
            
        End If
    Next lngItem

'CONVERT
    cmbSMDateCrit.Clear
    Call cmbSMDateCrit.AddItem("After")
    cmbSMDateCrit.ItemData(0) = CRIT_GREATERTHAN
    Call cmbSMDateCrit.AddItem("Before")
    cmbSMDateCrit.ItemData(1) = CRIT_LESSTHAN
    Call cmbSMDateCrit.AddItem("Dated")
    cmbSMDateCrit.ItemData(2) = CRIT_EQUALS
    Call cmbSMDateCrit.AddItem("All")
    cmbSMDateCrit.ItemData(3) = CRIT_ALL
    Call cmbSMDateCrit.AddItem("From")
    cmbSMDateCrit.ItemData(4) = CRIT_FROM
    
    For lngItem = 0 To cmbSMDateCrit.ListCount Step 1
        If cmbSMDateCrit.ItemData(lngItem) = CRIT_ALL Then
            cmbSMDateCrit.ListIndex = lngItem
            mlngSMAllDates = lngItem
            Exit For
            
        End If
    Next lngItem

    For lngItem = 0 To cmbSMType.ListCount Step 1
        If cmbSMType.ItemData(lngItem) = 0 Then
            cmbSMType.ListIndex = lngItem
            mlngSMAllMoves = lngItem
            Exit For
            
        End If
    Next lngItem

End Sub

Private Sub dtxtPOrdStartDate_GotFocus()
    dtxtPOrdStartDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtDueEndDate_GotFocus()
    dtxtDueEndDate.BackColor = RGBEdit_Colour

End Sub

Private Sub dtxtPOrdStartDate_LostFocus()
    
    If LenB(dtxtPOrdStartDate.Text) <> 0 Then
        If Year(GetDate(dtxtPOrdStartDate.Text)) = 1899 Then
            dtxtPOrdStartDate.BackColor = RGB_RED
        Else
            dtxtPOrdStartDate.BackColor = RGB_WHITE
        End If
        
    Else
        dtxtPOrdStartDate.BackColor = RGB_WHITE
        
    End If

End Sub
Private Sub dtxtPOrdEndDate_LostFocus()
    
    If LenB(dtxtPOrdEndDate.Text) <> 0 Then
        If Year(GetDate(dtxtPOrdEndDate.Text)) = 1899 Then
            dtxtPOrdEndDate.BackColor = RGB_RED
        Else
            dtxtPOrdEndDate.BackColor = RGB_WHITE
        End If
        
    Else
        dtxtPOrdEndDate.BackColor = RGB_WHITE
        
    End If

End Sub

Private Sub LoadAndPrepareDetailsForm(ByVal strHeading As String, _
                                      ByRef lngColumnsToHide() As Long)
Dim intCol As Integer

    Load frmSoDetails
    
    With frmSoDetails
        .BackColor = Me.BackColor
        .ucProgress.FillColor = ucProgress.FillColor
        .sprdDetails.MaxRows = 0
        .sprdDetails.MaxCols = COL_SO_CANC_BY ' COL_SO_REMARK
        .Caption = "Customer Sales Order Viewer"
        If LenB(strHeading) <> 0 Then .Caption = .Caption & " - " & strHeading
        
        ' hide unwanted columns
        Call HideColumns(.sprdDetails, lngColumnsToHide())
        
        ' copy headings
        For intCol = 1 To COL_SO_CANC_BY Step 1 '  COL_SO_REMARK
            .sprdDetails.Row = 0
            .sprdDetails.Col = intCol
            
    '        ' copy the text, setting cell type, background color etc
    '        sprdEdit.Row = lngSelectedRow
    '        .sprdDetails.Row = .sprdDetails.MaxRows
    '        .sprdDetails.CellType = sprdEdit.CellType
    '        .sprdDetails.ForeColor = sprdEdit.ForeColor
    '        .sprdDetails.BackColor = sprdEdit.BackColor
    '        .sprdDetails.TypeHAlign = sprdEdit.TypeHAlign
    '        .sprdDetails.FontStrikethru = sprdEdit.FontStrikethru
            
        Next intCol
    End With

End Sub
