VERSION 5.00
Begin VB.Form frmPassword 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change Password"
   ClientHeight    =   2205
   ClientLeft      =   1575
   ClientTop       =   1545
   ClientWidth     =   3420
   Icon            =   "frmPassword.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2205
   ScaleWidth      =   3420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExit 
      Caption         =   "F10-Exit"
      Height          =   375
      Left            =   2160
      TabIndex        =   6
      Top             =   1680
      Width           =   1095
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "F5-Save"
      Height          =   375
      Left            =   840
      TabIndex        =   5
      Top             =   1680
      Width           =   1095
   End
   Begin VB.TextBox txtConfirmPwd 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   1200
      Width           =   1335
   End
   Begin VB.TextBox txtNewPwd 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   1920
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label lblOldPwd 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1920
      TabIndex        =   10
      Top             =   480
      Width           =   1335
   End
   Begin VB.Label lblFullName 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1440
      TabIndex        =   9
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label lblUserID 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   600
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "User"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Confirm Password"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "New Password"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Old Password"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   1455
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmPassword
'* Date   : 02/02/03
'* Author : Unknown
'*$Archive: /Projects/OasysV2/VB/Logon/frmPassword.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 11/08/03 10:52 $ $Revision: 4 $
'* Versions:
'* 02/02/03    Unknown
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmPassword"

Dim moUser As Object

Public Sub SetPassword(ByRef oUser As Object)

Const PAD_PWD As String = "*****"

    lblUserID.Caption = oUser.EmployeeID
    lblFullName.Caption = oUser.FullName
    lblOldPwd.Caption = Left$(PAD_PWD, Len(oUser.Password))
    Set moUser = oUser
    Me.Show (vbModal)

End Sub

Private Sub cmdExit_Click()

    If LenB(lblOldPwd.Caption) = 0 Then
        Call MsgBox("Password has expired" & vbCrLf & " User ID has been disabled until valid password entered", vbExclamation, "Password updated - aborted")
    Else
        If moUser.SetPassword(vbNullString) Then
            Call MsgBox("User has been marked as expired - no further logins will be accepted until password updated", vbInformation, "Password Updated")
        Else
            Call MsgBox("Attempt to update password failed", vbInformation, "Password Update")
        End If
    End If
    Unload Me

End Sub

Private Sub cmdExit_GotFocus()
    
    cmdExit.FontBold = True

End Sub

Private Sub cmdExit_LostFocus()
    
    cmdExit.FontBold = False

End Sub

Private Sub cmdSave_Click()

Dim strPassword As String

    If LenB(txtNewPwd.Text) = 0 Then
        Call MsgBox("No password entered" & vbCrLf & "Enter password and re-save", vbExclamation, "Password error")
        txtNewPwd.SetFocus
        Exit Sub
    End If
    
    'pad password to 5 characters
    strPassword = txtNewPwd.Text
    If Len(strPassword) < 5 Then
        While (Len(strPassword) < 5)
            strPassword = "0" & strPassword
        Wend
    End If

    If strPassword = moUser.Password Then
        Call MsgBox("Entered password must be different from existing password" & vbCrLf & "Enter unique password and re-save", vbExclamation, "Password error")
        txtNewPwd.SetFocus
        Exit Sub
    End If
    
    If txtNewPwd.Text <> txtConfirmPwd.Text Then
        Call MsgBox("Entered password and confirmation do not match" & vbCrLf & "Re-enter passwords", vbExclamation, "Password error")
        txtNewPwd.SetFocus
        Exit Sub
    End If
    
    If moUser.SetPassword(strPassword) Then
        Call MsgBox("Password successfully updated", vbInformation, "Password Updated")
        Unload Me
    Else
        Call MsgBox("Attempt to update password failed", vbInformation, "Password Update")
    End If
    
End Sub

Private Sub cmdSave_GotFocus()
    
    cmdSave.FontBold = True

End Sub

Private Sub cmdSave_LostFocus()
    
    cmdSave.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                    If cmdSave.Visible Then
                        KeyCode = 0
                        Call cmdSave_Click
                    End If
            Case (vbKeyF10):
                    If cmdExit.Visible Then
                        KeyCode = 0
                        Call cmdExit_Click
                    End If
         End Select
     End If

End Sub

Private Sub Form_Load()

    Call CentreForm(Me)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)

End Sub

Private Sub txtConfirmPwd_GotFocus()

    txtConfirmPwd.SelStart = 0
    txtConfirmPwd.SelLength = Len(txtConfirmPwd)
    txtConfirmPwd.BackColor = RGBEdit_Colour
    

End Sub

Private Sub txtConfirmPwd_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub txtConfirmPwd_LostFocus()

    txtConfirmPwd.BackColor = lblOldPwd.BackColor

End Sub

Private Sub txtNewPwd_GotFocus()
    
    txtNewPwd.SelStart = 0
    txtNewPwd.SelLength = Len(txtNewPwd)
    txtNewPwd.BackColor = RGBEdit_Colour

End Sub

Private Sub txtNewPwd_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then
        KeyAscii = 0
        Call SendKeys(vbTab)
    End If

End Sub

Private Sub txtNewPwd_LostFocus()
    
    txtNewPwd.BackColor = lblOldPwd.BackColor

End Sub
