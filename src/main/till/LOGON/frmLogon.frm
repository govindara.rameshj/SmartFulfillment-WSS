VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmLogon 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Logon"
   ClientHeight    =   7470
   ClientLeft      =   2295
   ClientTop       =   3375
   ClientWidth     =   10110
   ControlBox      =   0   'False
   Icon            =   "frmLogon.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7470
   ScaleWidth      =   10110
   StartUpPosition =   2  'CenterScreen
   Tag             =   "116"
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000000&
      DrawMode        =   1  'Blackness
      DrawStyle       =   5  'Transparent
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   6480
      Picture         =   "frmLogon.frx":058A
      ScaleHeight     =   585
      ScaleWidth      =   1845
      TabIndex        =   11
      Top             =   2460
      Width           =   1875
   End
   Begin VB.Timer tmrUpdate 
      Interval        =   60000
      Left            =   2460
      Top             =   2280
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "F10-Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   6720
      TabIndex        =   10
      Tag             =   "114"
      Top             =   4320
      Width           =   1635
   End
   Begin VB.CommandButton cmdLogon 
      Caption         =   "F5-Logon"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   4920
      TabIndex        =   9
      Tag             =   "113"
      Top             =   4320
      Width           =   1575
   End
   Begin VB.TextBox txtPassword 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      IMEMode         =   3  'DISABLE
      Left            =   4380
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   3720
      Width           =   855
   End
   Begin VB.TextBox txtUserID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   4380
      MaxLength       =   3
      TabIndex        =   1
      Top             =   2640
      Width           =   735
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   7095
      Width           =   10110
      _ExtentX        =   17833
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Picture         =   "frmLogon.frx":3C6C
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10478
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:23"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Up to 5 characters"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   5340
      TabIndex        =   7
      Tag             =   "115"
      Top             =   3720
      Width           =   1455
   End
   Begin VB.Label lblFullName 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   5220
      TabIndex        =   6
      Top             =   3180
      Width           =   3135
   End
   Begin VB.Label lblInitials 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   4380
      TabIndex        =   5
      Top             =   3180
      Width           =   735
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Password"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   3
      Tag             =   "112"
      Top             =   3780
      Width           =   1395
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   2
      Tag             =   "111"
      Top             =   3180
      Width           =   975
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "User ID"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   0
      Tag             =   "110"
      Top             =   2640
      Width           =   1095
   End
End
Attribute VB_Name = "frmLogon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'<CAMH>****************************************************************************************
'* Module : frmLogon
'* Date   : 26/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Logon/frmLogon.frm $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Richardc $ $Date: 20/01/04 8:45 $ $Revision: 12 $
'* Versions:
'* 26/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "frmLogon"
Const PRM_ALLOW_EXIT As Long = 105
Const PRM_CHECK_EXPIRED As Long = 106

Dim moUser           As cUser
Dim strFailedMsg     As String
Dim m_oLogon         As clsLogon
Dim mblnCheckExpired As Boolean
Dim mstrWSID         As String
Dim mstrReleaseRoot  As String
Dim mblnCheckUpd     As Boolean
Dim mlngCountdown    As Long
Dim moFSO            As FileSystemObject
Dim mblnUpdates      As Boolean
Dim mstrOldUserID    As String



Private Sub cmdCancel_Click()

Dim oWStationBO     As cWorkStation

    txtUserID.Text = vbNullString
    Set oWStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
    Call oWStationBO.IBo_AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, mstrWSID)
    Call oWStationBO.IBo_LoadMatches
    Call oWStationBO.RecordWSAsLoggedOut(mstrWSID)
    Set oWStationBO = Nothing
    Call Me.Hide

End Sub

Private Sub cmdCancel_GotFocus()
    
    cmdCancel.FontBold = True

End Sub

Private Sub cmdCancel_LostFocus()
    
    cmdCancel.FontBold = False

End Sub

Private Sub cmdLogon_Click()

Const PROCEDURE_NAME As String = MODULE_NAME & ".cmdLogon_Click"

Dim blnLoggedIn     As Boolean
Dim strPassword     As String
Dim oWStationBO     As cWorkStation
Dim oStoreStatusBO  As cStoreStatus

    If Len(txtUserID.Text) <> 3 Then txtUserID.Text = Left$("000", 3 - Len(txtUserID.Text)) & txtUserID.Text
    blnLoggedIn = False
    If (Not moUser Is Nothing) And (LenB(lblInitials.Caption) <> 0) Then
        'pad password to 5 characters
        strPassword = txtPassword.Text
        If Len(strPassword) < 5 Then
            While (Len(strPassword) < 5)
                strPassword = "0" & strPassword
            Wend
        End If
        If strPassword = moUser.Password Then
            blnLoggedIn = True
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "User Verified-" & blnLoggedIn)
            If (moUser.PasswordValidTill < Now()) And (mblnCheckExpired = True) Then
                Call MsgBoxEx("Password has expired for user '" & moUser.EmployeeID & "-" & moUser.FullName & "'", vbInformation, "Password Expired", , , , , RGBMsgBox_WarnColour)
                Load frmPassword
                Screen.MousePointer = vbNormal
                Call frmPassword.SetPassword(moUser)
                If (moUser.Password = txtPassword.Text) And (LenB(txtPassword.Text) = 0) Then blnLoggedIn = False
                Screen.MousePointer = vbHourglass
            End If
            'Added 6/4/05
            Set oStoreStatusBO = goDatabase.CreateBusinessObject(CLASSID_STORESTATUS)
            Call oStoreStatusBO.IBo_Load
            If (oStoreStatusBO.MasterPCOpened = False) Then
                Call MsgBoxEx("WARNING : Master has not been opened", vbExclamation, "Master PC not Open", , , , , RGBMsgBox_WarnColour)
'                Set oStoreStatusBO = Nothing
'                Exit Sub
            End If
            If (oStoreStatusBO.DateOpened <> Date) Then
                Call MsgBoxEx("WARNING : Master not opened for today - " & Format(Date, "dd/mm/yy"), vbExclamation, "Master not Opened", , , , , RGBMsgBox_WarnColour)
'                Set oStoreStatusBO = Nothing
'                Exit Sub
            End If
            Set oStoreStatusBO = Nothing
            Set oWStationBO = goDatabase.CreateBusinessObject(CLASSID_WORKSTATIONCONFIG)
            Call oWStationBO.IBo_AddLoadFilter(CMP_EQUAL, FID_WORKSTATIONCONFIG_id, mstrWSID)
            Call oWStationBO.IBo_LoadMatches
            If (oWStationBO.DateLastLoggedOn <> Date) Then
                Call ShellWait(App.Path & "\" & "DataTransfer.exe", SW_MAX)
            End If
            Call oWStationBO.RecordWSAsLoggedIn(mstrWSID)
            Set oWStationBO = Nothing
            Call m_oLogon.SetSessionUser(moUser)
            Call moUser.UpdateLastTillID
            
'            Call goSession.SetUserID(txtUserID.Text, lblInitials.Caption, lblFullName.Caption)
        Else
            Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "User Failed")
            Call MsgBox(strFailedMsg, vbExclamation, Me.Caption)
            txtPassword.Text = ""
            txtPassword.SetFocus
        End If
    Else
        Set moUser = goDatabase.CreateBusinessObject(CLASSID_USER)
        moUser.EmployeeID = txtUserID.Text
        Call moUser.IBo_AddLoadField(FID_USER_Initials)
        Call moUser.IBo_AddLoadField(FID_USER_FullName)
        Call moUser.IBo_AddLoadField(FID_USER_Password)
        Call moUser.IBo_AddLoadField(FID_USER_PasswordValidTill)
        Call moUser.IBo_AddLoadField(FID_USER_AuthorisationCode)
        Call moUser.IBo_AddLoadField(FID_USER_AuthorisationValidTill)
        If LenB(moUser.EmployeeID) = 0 Then moUser.EmployeeID = "***" 'force invalid lookup
        Call DebugMsg(MODULE_NAME, PROCEDURE_NAME, endlDebug, "UserID Validated")
        If moUser.IBo_Load = True Then
            lblInitials.Caption = moUser.Initials
            lblFullName.Caption = moUser.FullName
            txtPassword.Enabled = True
            Call txtPassword.SetFocus
        Else
            txtPassword.Text = vbNullString
            txtPassword.Enabled = False
            lblInitials.Caption = vbNullString
            lblFullName.Caption = "INVALID ID"
            Call txtUserID.SetFocus
            Call txtUserID_GotFocus
            Set moUser = Nothing
        End If
    End If
    Screen.MousePointer = vbNormal
    If blnLoggedIn Then Call Me.Hide
    
End Sub

Private Sub cmdLogon_GotFocus()

    cmdLogon.FontBold = True

End Sub

Private Sub cmdLogon_LostFocus()
    
    cmdLogon.FontBold = False

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   
    
    If Shift = 0 Then
        Select Case (KeyCode)
            Case (vbKeyF5):
                    If cmdLogon.Visible Then
                        KeyCode = 0
                        Call cmdLogon_Click
                    End If
            Case (vbKeyF10):
                    If cmdCancel.Visible Then
                        KeyCode = 0
                        Call cmdCancel_Click
                    End If
         End Select
     End If

End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)

    mlngCountdown = 10 'if keypressed then reset count down to auto check for updates
    If KeyAscii = vbKeyEscape Then KeyAscii = 0
    
    
End Sub

Private Sub Form_Load()

'Dim oTrans   As CTSTranslator.clsTranslator
Dim lCtlNo   As Long

    Debug.Assert Not (m_oLogon Is Nothing) ' Forgot to call ParentLogon() earlier
'    'Set oTrans = New clsTranslator
'    'Translate form caption first
'    'Me.Caption = oTrans.TranslateStr(Me.Tag, Me.Caption)
'    'Step through each control and translate
'    For lCtlNo = 0 To Me.Controls.Count - 1 Step 1
'        If Val(Controls(lCtlNo).Tag) > 0 Then Controls(lCtlNo).Caption = oTrans.TranslateStr(Controls(lCtlNo).Tag, Controls(lCtlNo).Caption)
'        Debug.Print Controls(lCtlNo).Name
'    Next lCtlNo
'    strFailedMsg = oTrans.TranslateStr(117, "Invalid password for User ID, re-enter password")
'    Set oTrans = Nothing
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    RGBEdit_Colour = goSession.GetParameter(PRM_EDITCOLOUR)
    cmdCancel.Visible = goSession.GetParameter(PRM_ALLOW_EXIT)
    mblnCheckExpired = goSession.GetParameter(PRM_CHECK_EXPIRED)
    Call InitialiseStatusBar(sbStatus)
    Call CentreForm(Me)
    
    mstrWSID = Format$(Val(goSession.CurrentEnterprise.IEnterprise_WorkstationID), "00")
    
    'check if release software is installed
    mstrReleaseRoot = GetRegSetting("software\CTS Retail\DistSW", "ReleaseFolder", vbNullString, HKEY_LOCAL_MACHINE)
    Set moFSO = New FileSystemObject
    If LenB(mstrReleaseRoot) <> 0 Then
        If Right$(mstrReleaseRoot, 1) <> "\" Then mstrReleaseRoot = mstrReleaseRoot & "\"
        mblnCheckUpd = moFSO.FolderExists(mstrReleaseRoot & "Workstations\WS" & mstrWSID)
    End If
    Call DebugMsg(MODULE_NAME, "Form_Load", endlDebug, "Release System Check-" & mblnCheckUpd & " at " & mstrReleaseRoot)
    

End Sub

Private Sub tmrUpdate_Timer()
    
    mlngCountdown = mlngCountdown - 1
    If (mblnCheckUpd = True) And (mlngCountdown <= 1) Then Call CheckForNewUpdates
    If mblnUpdates = True Then
        txtUserID.Text = vbNullString
        Call Me.Hide
    End If


End Sub

Private Sub txtPassword_GotFocus()
    
    txtPassword.SelStart = 0
    txtPassword.SelLength = Len(txtPassword.Text)
    txtPassword.BackColor = RGBEdit_Colour

End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then Call cmdLogon_Click

End Sub

Private Sub txtPassword_LostFocus()
    
    txtPassword.BackColor = lblInitials.BackColor

End Sub

Private Sub txtUserID_Change()

    lblInitials.Caption = vbNullString
    lblFullName.Caption = vbNullString
    txtPassword.Text = vbNullString
    txtPassword.Enabled = False
    
End Sub

Private Sub txtUserID_GotFocus()

    txtUserID.SelStart = 0
    txtUserID.SelLength = Len(txtUserID.Text)
    txtUserID.BackColor = RGBEdit_Colour

End Sub

Private Sub txtUserID_KeyPress(KeyAscii As Integer)

    If (KeyAscii = vbKeyReturn) Then Call cmdLogon_Click

End Sub

Friend Property Let ParentLogon(oLogon As clsLogon)
    Debug.Assert Not (oLogon Is Nothing)
    Set m_oLogon = oLogon
End Property

Private Sub txtUserID_KeyUp(KeyCode As Integer, Shift As Integer)
    
    'Check if new keypressed makes up full User ID, so auto move to password
    If (Len(txtUserID.Text) = 3) And (Len(mstrOldUserID) = 2) Then
        KeyCode = 0
        Call txtUserID_KeyPress(vbKeyReturn)
    End If
    mstrOldUserID = txtUserID.Text

End Sub

Private Sub txtUserID_LostFocus()
    
    txtUserID.BackColor = lblInitials.BackColor

End Sub

Private Sub CheckForNewUpdates()

Const VER_FILE_EXT  As String = ".GO"
Const STATUS_FILE  As String = "STATUS.SRF"
Const PRM_WSUPDATE_PATH As Long = 180
Const PRM_WSNOTIFY_PATH As Long = 181

Dim oFile     As File
Dim oFiles    As Files
Dim strFName  As String
Dim strUpdEXE As String

    mlngCountdown = 10

    'get list of files in Workstation folder
    Set oFiles = moFSO.GetFolder(mstrReleaseRoot & "Workstations\WS" & mstrWSID).Files
    mblnUpdates = False
    
    Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Checked for files (" & oFiles.Count & " found)")
    
    For Each oFile In oFiles
        strFName = UCase$(oFile.Name)
        'check if any of the files have correct prefix and suffix
        If (InStr(strFName, mstrWSID & ".") = 1) And (InStr(strFName, VER_FILE_EXT) > 1) Then
            Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Checked for files (" & oFile.Name & " found)")
            mblnUpdates = True
            Exit For
        End If
        If (InStr(strFName, STATUS_FILE) = 1) Then
            Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Checked for Status file(" & oFile.Name & " found)")
            mblnUpdates = True
            Exit For
        End If
    Next
    
    If mblnUpdates = True Then
        strUpdEXE = goSession.GetParameter(PRM_WSUPDATE_PATH)
        Call MsgBoxEx("New version detected" & vbCrLf & "Exiting menu to run update system" & vbCrLf & vbCrLf & "Press OK to continue", vbExclamation, "CTS Work-station update system", , , , , RGB_RED)
        Call DebugMsg(MODULE_NAME, "CheckForNewUpdates", endlDebug, "Upd EXE - " & strUpdEXE)
        On Error Resume Next
        Call Shell(strUpdEXE & " EXIT " & App.Path & "\" & App.EXEName, vbNormalFocus)
    End If
    
End Sub





