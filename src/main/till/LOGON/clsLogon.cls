VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLogon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'<CAMH>****************************************************************************************
'* Module : clsLogon
'* Date   : 26/11/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Logon/clsLogon.cls $
'**********************************************************************************************
'* Summary:
'**********************************************************************************************
'* $Author: Mauricem $ $Date: 1/29/03 4:29p $ $Revision: 3 $
'* Versions:
'* 26/11/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "clsLogon"

Private m_oSysRoot          As ISysRoot       ' ISysRoot interface

Public Function ValidateUser(ByRef oSession As ISession) As Boolean

    Debug.Assert Not (m_oSysRoot Is Nothing)    ' Not created using Root.CreateUtilityObject()
    
    ValidateUser = False
    
    Set goSession = oSession
    Set goDatabase = goSession.Database

    frmLogon.ParentLogon = Me
    Load frmLogon
    frmLogon.Show vbModal
    If LenB(frmLogon.txtUserID.Text) <> 0 Then ValidateUser = True
    Unload frmLogon

End Function

Public Property Set SysRoot(oSysRoot As ISysRoot)
    ' This method acquires the restricted, friend interface to the root
    ' in order to set the user id in the session.
    ' This method is called when the object is created by the Root.CreateUtilityObject() method.
    Set m_oSysRoot = oSysRoot
End Property

Friend Function SetSessionUser(oUser As cUser)
    goSession.SetCurrentUser oUser, m_oSysRoot
End Function

