Attribute VB_Name = "modBo"
'<CAMH>****************************************************************************************
'* Module : modBo
'* Date   : 11/09/02
'* Author : mauricem
'*$Archive: /Projects/OasysV2/VB/Common/modBo.bas $
'**********************************************************************************************
'* Summary:
'* We list the business object Ids here in a module that belongs
'* to the OasysRootCom but may be built into other applications
'* in order to use a common set of Business Object class Id numbers.
'* We don't use the enumeration, we simply pass the values around as IDs.
'**********************************************************************************************
'* $Author: Richardc $ $Date: 2/06/04 16:07 $ $Revision: 117 $
'* Versions:
'* 11/09/02    mauricem
'*             Header added.
'</CAMH>***************************************************************************************
Option Explicit

Const MODULE_NAME As String = "modBo"

'* Define all Class IDs for each Business Object used within the system
Public Const CLASSID_ACTIVITYLOG = 1
Public Const CLASSID_AUDITMASTER = 2
Public Const CLASSID_CARDINFO = 3
Public Const CLASSID_CASHBALANCECONTROL = 4
Public Const CLASSID_CASHBALANCEHDR = 5
Public Const CLASSID_CASHBALANCESUMMARY = 6
Public Const CLASSID_CASHIER = 7
Public Const CLASSID_CASHIERCASHBAL = 8
Public Const CLASSID_CASHIERTOTALS = 9
Public Const CLASSID_CONSHEADER = 10
Public Const CLASSID_COUNTEDSKU = 11
Public Const CLASSID_DELNOTEHEADER = 12
Public Const CLASSID_DELNOTELINE = 13
Public Const CLASSID_DELNOTENARRATIVE = 14
Public Const CLASSID_DEPOSIT = 15
Public Const CLASSID_DEPTANALYSIS = 16
Public Const CLASSID_DESPATCHMETHOD = 17
Public Const CLASSID_EAN = 18
Public Const CLASSID_EDICHANGECODE = 19
Public Const CLASSID_EDIDELHEADER = 20
Public Const CLASSID_EDIDELLINE = 21
Public Const CLASSID_EDIFILECONTROL = 22
Public Const CLASSID_EDIPOEARLY = 23
Public Const CLASSID_EDIPOLATE = 24
Public Const CLASSID_EDIPOLINES = 25
Public Const CLASSID_EDIPOQTYDISCREPANCY = 26
Public Const CLASSID_EDIPOQTYDISCREPANCYLINES = 27
Public Const CLASSID_GROSSMARGINS = 28
Public Const CLASSID_HIERARCHYCATEGORY = 29
Public Const CLASSID_HIERARCHYGROUP = 30
Public Const CLASSID_HIERARCHYSTYLE = 31
Public Const CLASSID_HIERARCHYSUBGROUP = 32
Public Const CLASSID_INVENTORY = 33
Public Const CLASSID_INVENTORYINFO = 34
Public Const CLASSID_INVENTORYS = 35
Public Const CLASSID_ISTINEDIHEADER = 36
Public Const CLASSID_ISTINEDILINE = 37
Public Const CLASSID_ISTINHEADER = 38
Public Const CLASSID_ISTLINE = 39
Public Const CLASSID_ISTOUTHEADER = 40
Public Const CLASSID_ITEMGROUP = 41
Public Const CLASSID_LINECOMMENTS = 42
Public Const CLASSID_LOCATIONTYPE = 43
Public Const CLASSID_MENU = 44
Public Const CLASSID_NIGHTLOG = 45
Public Const CLASSID_NIGHTMASTER = 46
Public Const CLASSID_OPDEALGROUP = 47
Public Const CLASSID_OPENDRAWERCODE = 48
Public Const CLASSID_OPHEADER = 49
Public Const CLASSID_OPMASTER = 50
Public Const CLASSID_OPMIXMATCH = 51
Public Const CLASSID_PAIDINCODE = 52
Public Const CLASSID_PAIDOUTCODE = 53
Public Const CLASSID_PARAMETER = 54
Public Const CLASSID_POLINES = 55
Public Const CLASSID_POSACTION = 56
Public Const CLASSID_POSEVENTLINE = 57
Public Const CLASSID_POSHEADER = 58
Public Const CLASSID_POSLINE = 59
Public Const CLASSID_POSMESSAGE = 60
Public Const CLASSID_POSPAYMENT = 61
Public Const CLASSID_PRICECHANGE = 62
Public Const CLASSID_PRICECUSINFO = 63
Public Const CLASSID_PRICELINEINFO = 64
Public Const CLASSID_PRICEOVERRIDE = 65
Public Const CLASSID_PRODUCTGROUP = 66
Public Const CLASSID_PURCHASECONSLINE = 67
Public Const CLASSID_PURCHASEORDER = 68
Public Const CLASSID_PURCHASEORDERLINE = 69
Public Const CLASSID_PURHDRNARRATIVE = 70
Public Const CLASSID_RELATEDITEM = 71
Public Const CLASSID_RETAILEROPTIONS = 72
Public Const CLASSID_RETNOTEHEADER = 73
Public Const CLASSID_RETURNCODE = 74
Public Const CLASSID_RETURNCUST = 75
Public Const CLASSID_RETURNHDR = 76
Public Const CLASSID_RETURNLINE = 77
Public Const CLASSID_RETURNNOTE = 78
Public Const CLASSID_RETURNNOTELINE = 79
Public Const CLASSID_SALESCUSTOMER = 80
Public Const CLASSID_SALESLEDGER = 81
Public Const CLASSID_SALETOTALS = 82
Public Const CLASSID_STOCKADJCODE = 83
Public Const CLASSID_STOCKADJPEND = 84
Public Const CLASSID_STOCKADJREASON = 85
Public Const CLASSID_STOCKADJUST = 86
Public Const CLASSID_STOCKCOUNTHEADER = 87
Public Const CLASSID_STOCKCOUNTLINE = 88
Public Const CLASSID_STOCKLEVEL = 89
Public Const CLASSID_STOCKLOCATION = 90
Public Const CLASSID_STOCKLOCATION2 = 91
Public Const CLASSID_STOCKLOG = 92
Public Const CLASSID_STOCKMOVE = 93
Public Const CLASSID_STOCKTAKE = 94
Public Const CLASSID_STOCKWOF = 95
Public Const CLASSID_STORE = 96
Public Const CLASSID_STORESTATUS = 97
Public Const CLASSID_SUPPLIER = 98
Public Const CLASSID_SUPPLIERS = 99
Public Const CLASSID_SYSTEMDATES = 100
Public Const CLASSID_SYSTEMNO = 101
Public Const CLASSID_SYSTEMOPTIONS = 102
Public Const CLASSID_TCFMASTER = 103
Public Const CLASSID_TENDERCONTROL = 104
Public Const CLASSID_TENDEROPTIONS = 105
Public Const CLASSID_TILLTOTALS = 106
Public Const CLASSID_TRANSACTION_DISCOUNT = 107
Public Const CLASSID_TYPESOFSALECONTROL = 108
Public Const CLASSID_TYPESOFSALEOPTIONS = 109
Public Const CLASSID_TYPESOFSALETENDERS = 110
Public Const CLASSID_UNITOFMEASURE = 111
Public Const CLASSID_USER = 112
Public Const CLASSID_VATRATES = 113
Public Const CLASSID_WORKSTATIONCONFIG = 114
Public Const CLASSID_INVENTORYWICKES = 115
Public Const CLASSID_POSMISSINGEAN = 116
Public Const CLASSID_REFUNDCODE = 117
Public Const CLASSID_TENDEROVERRIDE = 118
Public Const CLASSID_ZREADS = 119
'Public Const CLASSID_DISCOUNTRATE = 9
'Public Const CLASSID_PAYHOURS = 11
'Public Const CLASSID_DEPARTMENT = 17
'Public Const CLASSID_SOORDERTYPE = 20
'Public Const CLASSID_SORANGEDISC = 21
'Public Const CLASSID_SORANGE = 22
'Public Const CLASSID_SORANGEACC = 23
'Public Const CLASSID_SOHEADER = 23
'Public Const CLASSID_SOLINE = 24
'Public Const CLASSID_TRADER = 32
'Public Const CLASSID_DEPTGROUP = 53
'Public Const CLASSID_TRADERS = 61
'Public Const CLASSID_ITEMMESSAGES = 115
'Public Const CLASSID_TENDERTYPES = 77
'Public Const CLASSID_TILLTENDERTYPE = 91
'Public Const CLASSID_TILLLOOKUPS = 63

'---------- Insert new class IDs immediately before here, incrementing the
'---------- number and setting CLASSID_END_OF_STATIC to the same name
Public Const CLASSID_END_OF_STATIC = CLASSID_ZREADS


' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_SUPPLIER_SupplierNo = (CLASSID_SUPPLIER * &H10000) + 1
Public Const FID_SUPPLIER_Name = (CLASSID_SUPPLIER * &H10000) + 2
Public Const FID_SUPPLIER_Deleted = (CLASSID_SUPPLIER * &H10000) + 3
Public Const FID_SUPPLIER_BBCSiteNo = (CLASSID_SUPPLIER * &H10000) + 4
Public Const FID_SUPPLIER_AlphaKey = (CLASSID_SUPPLIER * &H10000) + 5
Public Const FID_SUPPLIER_HelpLineNo = (CLASSID_SUPPLIER * &H10000) + 6
Public Const FID_SUPPLIER_PrimaryMerchant = (CLASSID_SUPPLIER * &H10000) + 7
Public Const FID_SUPPLIER_TypeCode = (CLASSID_SUPPLIER * &H10000) + 8
Public Const FID_SUPPLIER_OrderDepotNo = (CLASSID_SUPPLIER * &H10000) + 9
Public Const FID_SUPPLIER_SOQControlNo = (CLASSID_SUPPLIER * &H10000) + 10
Public Const FID_SUPPLIER_AllowSOQ = (CLASSID_SUPPLIER * &H10000) + 11
Public Const FID_SUPPLIER_ReturnDepotNo = (CLASSID_SUPPLIER * &H10000) + 12
Public Const FID_SUPPLIER_DateOfLastPO = (CLASSID_SUPPLIER * &H10000) + 13
Public Const FID_SUPPLIER_DateOfLastReceipt = (CLASSID_SUPPLIER * &H10000) + 14
Public Const FID_SUPPLIER_NoOfCurrentPOs = (CLASSID_SUPPLIER * &H10000) + 15
Public Const FID_SUPPLIER_ValueOfCurrentPOs = (CLASSID_SUPPLIER * &H10000) + 16
Public Const FID_SUPPLIER_SumOfQtyOrdered = (CLASSID_SUPPLIER * &H10000) + 17
Public Const FID_SUPPLIER_SumOfQtyReceived = (CLASSID_SUPPLIER * &H10000) + 18
Public Const FID_SUPPLIER_SumOfPOLinesReceived = (CLASSID_SUPPLIER * &H10000) + 19
Public Const FID_SUPPLIER_SumOfPOLinesNotReceived = (CLASSID_SUPPLIER * &H10000) + 20
Public Const FID_SUPPLIER_SumOfLinesOver = (CLASSID_SUPPLIER * &H10000) + 21
Public Const FID_SUPPLIER_SumOfLinesUnder = (CLASSID_SUPPLIER * &H10000) + 22
Public Const FID_SUPPLIER_NoOfPOsReceived = (CLASSID_SUPPLIER * &H10000) + 23
Public Const FID_SUPPLIER_ValueOfPOsReceived = (CLASSID_SUPPLIER * &H10000) + 24
Public Const FID_SUPPLIER_DaysToRecievePOs = (CLASSID_SUPPLIER * &H10000) + 25
Public Const FID_SUPPLIER_FewestDays = (CLASSID_SUPPLIER * &H10000) + 26
Public Const FID_SUPPLIER_MostDays = (CLASSID_SUPPLIER * &H10000) + 27
Public Const FID_SUPPLIER_VendorCountCode = (CLASSID_SUPPLIER * &H10000) + 28
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SUPPLIER_END_OF_STATIC = FID_SUPPLIER_VendorCountCode


Public Const FID_PURCHASEORDER_Key = (CLASSID_PURCHASEORDER * &H10000) + 1
Public Const FID_PURCHASEORDER_OrderNumber = (CLASSID_PURCHASEORDER * &H10000) + 2
Public Const FID_PURCHASEORDER_SupplierNo = (CLASSID_PURCHASEORDER * &H10000) + 3
Public Const FID_PURCHASEORDER_OrderDate = (CLASSID_PURCHASEORDER * &H10000) + 4
Public Const FID_PURCHASEORDER_DueDate = (CLASSID_PURCHASEORDER * &H10000) + 5
Public Const FID_PURCHASEORDER_ReleaseNumber = (CLASSID_PURCHASEORDER * &H10000) + 6
Public Const FID_PURCHASEORDER_RaisedBy = (CLASSID_PURCHASEORDER * &H10000) + 7
Public Const FID_PURCHASEORDER_Source = (CLASSID_PURCHASEORDER * &H10000) + 8
Public Const FID_PURCHASEORDER_HOOrderNumber = (CLASSID_PURCHASEORDER * &H10000) + 9
Public Const FID_PURCHASEORDER_SOQNumber = (CLASSID_PURCHASEORDER * &H10000) + 10
Public Const FID_PURCHASEORDER_NoOfCartons = (CLASSID_PURCHASEORDER * &H10000) + 11
Public Const FID_PURCHASEORDER_QuantityOnOrder = (CLASSID_PURCHASEORDER * &H10000) + 12
Public Const FID_PURCHASEORDER_Value = (CLASSID_PURCHASEORDER * &H10000) + 13
Public Const FID_PURCHASEORDER_PrintFlag = (CLASSID_PURCHASEORDER * &H10000) + 14
Public Const FID_PURCHASEORDER_NoOfRePrints = (CLASSID_PURCHASEORDER * &H10000) + 15
Public Const FID_PURCHASEORDER_ReceiptNumber = (CLASSID_PURCHASEORDER * &H10000) + 16
Public Const FID_PURCHASEORDER_PricingNumber = (CLASSID_PURCHASEORDER * &H10000) + 17
Public Const FID_PURCHASEORDER_ReceivedAll = (CLASSID_PURCHASEORDER * &H10000) + 18
Public Const FID_PURCHASEORDER_ReceivedPart = (CLASSID_PURCHASEORDER * &H10000) + 19
Public Const FID_PURCHASEORDER_DeletedByMaint = (CLASSID_PURCHASEORDER * &H10000) + 20
Public Const FID_PURCHASEORDER_Commed = (CLASSID_PURCHASEORDER * &H10000) + 21
Public Const FID_PURCHASEORDER_CustomerOrderNo = (CLASSID_PURCHASEORDER * &H10000) + 22
Public Const FID_PURCHASEORDER_CommNumber = (CLASSID_PURCHASEORDER * &H10000) + 23
Public Const FID_PURCHASEORDER_Weight = (CLASSID_PURCHASEORDER * &H10000) + 24
Public Const FID_PURCHASEORDER_CompletedDate = (CLASSID_PURCHASEORDER * &H10000) + 25
Public Const FID_PURCHASEORDER_CancelledBy = (CLASSID_PURCHASEORDER * &H10000) + 26
Public Const FID_PURCHASEORDER_CarraigeValue = (CLASSID_PURCHASEORDER * &H10000) + 27
Public Const FID_PURCHASEORDER_TimeCreated = (CLASSID_PURCHASEORDER * &H10000) + 28
Public Const FID_PURCHASEORDER_HOReceivedDate = (CLASSID_PURCHASEORDER * &H10000) + 29
Public Const FID_PURCHASEORDER_HORecNotifyDate = (CLASSID_PURCHASEORDER * &H10000) + 30
Public Const FID_PURCHASEORDER_HORecNotifyTime = (CLASSID_PURCHASEORDER * &H10000) + 31
Public Const FID_PURCHASEORDER_HORecNotifyErrDone = (CLASSID_PURCHASEORDER * &H10000) + 32
Public Const FID_PURCHASEORDER_HOSuppConfDate = (CLASSID_PURCHASEORDER * &H10000) + 33
Public Const FID_PURCHASEORDER_HOSuppConfNotifyDate = (CLASSID_PURCHASEORDER * &H10000) + 34
Public Const FID_PURCHASEORDER_HOSuppConfNotifyTime = (CLASSID_PURCHASEORDER * &H10000) + 35
Public Const FID_PURCHASEORDER_HOSuppConfErrDone = (CLASSID_PURCHASEORDER * &H10000) + 36
Public Const FID_PURCHASEORDER_HOSuppDelDate = (CLASSID_PURCHASEORDER * &H10000) + 37
Public Const FID_PURCHASEORDER_HOSuppDelNotifyDate = (CLASSID_PURCHASEORDER * &H10000) + 38
Public Const FID_PURCHASEORDER_HOSuppDelNotifyTime = (CLASSID_PURCHASEORDER * &H10000) + 39
Public Const FID_PURCHASEORDER_HOSuppDelErrDone = (CLASSID_PURCHASEORDER * &H10000) + 40
Public Const FID_PURCHASEORDER_EDIConfDeliveryDate = (CLASSID_PURCHASEORDER * &H10000) + 41
Public Const FID_PURCHASEORDER_EDIStatus = (CLASSID_PURCHASEORDER * &H10000) + 42
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PURCHASEORDER_END_OF_STATIC = FID_PURCHASEORDER_EDIStatus


Public Const FID_MENU_ID = (CLASSID_MENU * &H10000) + 1
Public Const FID_MENU_TEXT = (CLASSID_MENU * &H10000) + 2
Public Const FID_MENU_EXECPATH = (CLASSID_MENU * &H10000) + 3
Public Const FID_MENU_ASPPAGE = (CLASSID_MENU * &H10000) + 4
Public Const FID_MENU_PARENTID = (CLASSID_MENU * &H10000) + 5
Public Const FID_MENU_SECURITY = (CLASSID_MENU * &H10000) + 6
Public Const FID_MENU_MENUTYPE = (CLASSID_MENU * &H10000) + 7
Public Const FID_MENU_SHORTCUTKEY = (CLASSID_MENU * &H10000) + 8
Public Const FID_MENU_ICONID = (CLASSID_MENU * &H10000) + 9
Public Const FID_MENU_WAITEXE = (CLASSID_MENU * &H10000) + 10
Public Const FID_MENU_ADDPATH = (CLASSID_MENU * &H10000) + 11
Public Const FID_MENU_GROUPID = (CLASSID_MENU * &H10000) + 12
Public Const FID_MENU_SENDSESSION = (CLASSID_MENU * &H10000) + 13
Public Const FID_MENU_PARAMETERS = (CLASSID_MENU * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_MENU_END_OF_STATIC = FID_MENU_PARAMETERS


Public Const FID_PURCHASEORDERLINE_PurchaseOrderNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 1
Public Const FID_PURCHASEORDERLINE_LineNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 2
Public Const FID_PURCHASEORDERLINE_PartCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 3
Public Const FID_PURCHASEORDERLINE_PartAlphaKey = (CLASSID_PURCHASEORDERLINE * &H10000) + 4
Public Const FID_PURCHASEORDERLINE_ManufacturerCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 5
Public Const FID_PURCHASEORDERLINE_SupplierPartCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 6
Public Const FID_PURCHASEORDERLINE_OrderQuantity = (CLASSID_PURCHASEORDERLINE * &H10000) + 7
Public Const FID_PURCHASEORDERLINE_OrderPrice = (CLASSID_PURCHASEORDERLINE * &H10000) + 8
Public Const FID_PURCHASEORDERLINE_CostPrice = (CLASSID_PURCHASEORDERLINE * &H10000) + 9
Public Const FID_PURCHASEORDERLINE_LastOrderDate = (CLASSID_PURCHASEORDERLINE * &H10000) + 10
Public Const FID_PURCHASEORDERLINE_QuantityReceived = (CLASSID_PURCHASEORDERLINE * &H10000) + 11
Public Const FID_PURCHASEORDERLINE_QuantityBackordered = (CLASSID_PURCHASEORDERLINE * &H10000) + 12
Public Const FID_PURCHASEORDERLINE_Complete = (CLASSID_PURCHASEORDERLINE * &H10000) + 13
Public Const FID_PURCHASEORDERLINE_DelNoteNumber = (CLASSID_PURCHASEORDERLINE * &H10000) + 14
Public Const FID_PURCHASEORDERLINE_DelNoteLineNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 15
Public Const FID_PURCHASEORDERLINE_DateLastReceipt = (CLASSID_PURCHASEORDERLINE * &H10000) + 16
Public Const FID_PURCHASEORDERLINE_Message = (CLASSID_PURCHASEORDERLINE * &H10000) + 17
Public Const FID_PURCHASEORDERLINE_QuantityPerUnit = (CLASSID_PURCHASEORDERLINE * &H10000) + 18
Public Const FID_PURCHASEORDERLINE_CustomerOrderNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 19
Public Const FID_PURCHASEORDERLINE_CustomerOrderLineNo = (CLASSID_PURCHASEORDERLINE * &H10000) + 20
Public Const FID_PURCHASEORDERLINE_EDIConfirmedQuantity = (CLASSID_PURCHASEORDERLINE * &H10000) + 21
Public Const FID_PURCHASEORDERLINE_EDIQuantityChangedCode = (CLASSID_PURCHASEORDERLINE * &H10000) + 22
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PURCHASEORDERLINE_END_OF_STATIC = FID_PURCHASEORDERLINE_EDIQuantityChangedCode


Public Const FID_POSHEADER_TranDate = (CLASSID_POSHEADER * &H10000) + 1
Public Const FID_POSHEADER_TillID = (CLASSID_POSHEADER * &H10000) + 2
Public Const FID_POSHEADER_TransactionNo = (CLASSID_POSHEADER * &H10000) + 3
Public Const FID_POSHEADER_CashierID = (CLASSID_POSHEADER * &H10000) + 4
Public Const FID_POSHEADER_TransactionTime = (CLASSID_POSHEADER * &H10000) + 5
Public Const FID_POSHEADER_TransactionCode = (CLASSID_POSHEADER * &H10000) + 6
Public Const FID_POSHEADER_OpenDrawerCode = (CLASSID_POSHEADER * &H10000) + 7
Public Const FID_POSHEADER_ReasonCode = (CLASSID_POSHEADER * &H10000) + 8
Public Const FID_POSHEADER_OrderNumber = (CLASSID_POSHEADER * &H10000) + 9
Public Const FID_POSHEADER_Voided = (CLASSID_POSHEADER * &H10000) + 10
Public Const FID_POSHEADER_TrainingMode = (CLASSID_POSHEADER * &H10000) + 11
Public Const FID_POSHEADER_DailyUpdateProc = (CLASSID_POSHEADER * &H10000) + 12
Public Const FID_POSHEADER_TranParked = (CLASSID_POSHEADER * &H10000) + 13
Public Const FID_POSHEADER_SupervisorNo = (CLASSID_POSHEADER * &H10000) + 14
Public Const FID_POSHEADER_SupervisorUsed = (CLASSID_POSHEADER * &H10000) + 15
Public Const FID_POSHEADER_VoidSupervisor = (CLASSID_POSHEADER * &H10000) + 16
Public Const FID_POSHEADER_DiscountSupervisor = (CLASSID_POSHEADER * &H10000) + 17
Public Const FID_POSHEADER_MerchandiseAmount = (CLASSID_POSHEADER * &H10000) + 18
Public Const FID_POSHEADER_NonMerchandiseAmount = (CLASSID_POSHEADER * &H10000) + 19
Public Const FID_POSHEADER_TaxAmount = (CLASSID_POSHEADER * &H10000) + 20
Public Const FID_POSHEADER_DiscountAmount = (CLASSID_POSHEADER * &H10000) + 21
Public Const FID_POSHEADER_TotalSaleAmount = (CLASSID_POSHEADER * &H10000) + 22
Public Const FID_POSHEADER_VATRates = (CLASSID_POSHEADER * &H10000) + 23
Public Const FID_POSHEADER_VATSymbol = (CLASSID_POSHEADER * &H10000) + 24
Public Const FID_POSHEADER_ExVATValue = (CLASSID_POSHEADER * &H10000) + 25
Public Const FID_POSHEADER_VATValue = (CLASSID_POSHEADER * &H10000) + 26
Public Const FID_POSHEADER_TransactionComplete = (CLASSID_POSHEADER * &H10000) + 27
Public Const FID_POSHEADER_CustomerAcctNo = (CLASSID_POSHEADER * &H10000) + 28
Public Const FID_POSHEADER_CustomerAcctCardNo = (CLASSID_POSHEADER * &H10000) + 29
Public Const FID_POSHEADER_CollectPostCode = (CLASSID_POSHEADER * &H10000) + 30
Public Const FID_POSHEADER_FromDCOrders = (CLASSID_POSHEADER * &H10000) + 31
Public Const FID_POSHEADER_EmployeeDiscountOnly = (CLASSID_POSHEADER * &H10000) + 32
Public Const FID_POSHEADER_RefundCashier = (CLASSID_POSHEADER * &H10000) + 33
Public Const FID_POSHEADER_RefundSupervisor = (CLASSID_POSHEADER * &H10000) + 34
Public Const FID_POSHEADER_RefundManager = (CLASSID_POSHEADER * &H10000) + 35
Public Const FID_POSHEADER_TenderOverrideCode = (CLASSID_POSHEADER * &H10000) + 36
Public Const FID_POSHEADER_RecoveredFromParked = (CLASSID_POSHEADER * &H10000) + 37
Public Const FID_POSHEADER_Offline = (CLASSID_POSHEADER * &H10000) + 38
Public Const FID_POSHEADER_GiftTokensPrinted = (CLASSID_POSHEADER * &H10000) + 39
Public Const FID_POSHEADER_Description = (CLASSID_POSHEADER * &H10000) + 40
Public Const FID_POSHEADER_AccountSale = (CLASSID_POSHEADER * &H10000) + 41
Public Const FID_POSHEADER_ExternalDocumentNo = (CLASSID_POSHEADER * &H10000) + 42
Public Const FID_POSHEADER_StoreNumber = (CLASSID_POSHEADER * &H10000) + 43
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSHEADER_END_OF_STATIC = FID_POSHEADER_StoreNumber


Public Const FID_POSLINE_TranDate = (CLASSID_POSLINE * &H10000) + 1
Public Const FID_POSLINE_TillID = (CLASSID_POSLINE * &H10000) + 2
Public Const FID_POSLINE_TransactionNumber = (CLASSID_POSLINE * &H10000) + 3
Public Const FID_POSLINE_SequenceNo = (CLASSID_POSLINE * &H10000) + 4
Public Const FID_POSLINE_PartCode = (CLASSID_POSLINE * &H10000) + 5
Public Const FID_POSLINE_DepartmentNumber = (CLASSID_POSLINE * &H10000) + 6
Public Const FID_POSLINE_BarcodeEntry = (CLASSID_POSLINE * &H10000) + 7
Public Const FID_POSLINE_QuantitySold = (CLASSID_POSLINE * &H10000) + 8
Public Const FID_POSLINE_ActualSellPrice = (CLASSID_POSLINE * &H10000) + 9
Public Const FID_POSLINE_ExtendedValue = (CLASSID_POSLINE * &H10000) + 10
Public Const FID_POSLINE_ExtendedCost = (CLASSID_POSLINE * &H10000) + 11
Public Const FID_POSLINE_LookUpPrice = (CLASSID_POSLINE * &H10000) + 12
Public Const FID_POSLINE_VATCode = (CLASSID_POSLINE * &H10000) + 13
Public Const FID_POSLINE_RelatedItems = (CLASSID_POSLINE * &H10000) + 14
Public Const FID_POSLINE_CatchAllItem = (CLASSID_POSLINE * &H10000) + 15
Public Const FID_POSLINE_LineReversed = (CLASSID_POSLINE * &H10000) + 16
Public Const FID_POSLINE_PriceOverrideReason = (CLASSID_POSLINE * &H10000) + 17
Public Const FID_POSLINE_SupervisorNumber = (CLASSID_POSLINE * &H10000) + 18
Public Const FID_POSLINE_ActualPriceExVAT = (CLASSID_POSLINE * &H10000) + 19
Public Const FID_POSLINE_OverrideMarginCode = (CLASSID_POSLINE * &H10000) + 20

Public Const FID_POSLINE_LastEventSequenceNo = (CLASSID_POSLINE * &H10000) + 21
Public Const FID_POSLINE_HierCategory = (CLASSID_POSLINE * &H10000) + 22
Public Const FID_POSLINE_HierGroup = (CLASSID_POSLINE * &H10000) + 23
Public Const FID_POSLINE_HierSubGroup = (CLASSID_POSLINE * &H10000) + 24
Public Const FID_POSLINE_HierStyle = (CLASSID_POSLINE * &H10000) + 25
Public Const FID_POSLINE_QuarantineSupervisor = (CLASSID_POSLINE * &H10000) + 26
Public Const FID_POSLINE_EmpSaleSecondaryMarginAmount = (CLASSID_POSLINE * &H10000) + 27
Public Const FID_POSLINE_MarkedDownStock = (CLASSID_POSLINE * &H10000) + 28
Public Const FID_POSLINE_SaleType = (CLASSID_POSLINE * &H10000) + 29
Public Const FID_POSLINE_VATCodeNumber = (CLASSID_POSLINE * &H10000) + 30
Public Const FID_POSLINE_PriceOverrideAmount = (CLASSID_POSLINE * &H10000) + 31
Public Const FID_POSLINE_ItemTagged = (CLASSID_POSLINE * &H10000) + 32
Public Const FID_POSLINE_TempPriceMarginAmount = (CLASSID_POSLINE * &H10000) + 33
Public Const FID_POSLINE_TempPriceMarginCode = (CLASSID_POSLINE * &H10000) + 34
Public Const FID_POSLINE_QtyBreakMarginAmount = (CLASSID_POSLINE * &H10000) + 35
Public Const FID_POSLINE_QtyBreakMarginCode = (CLASSID_POSLINE * &H10000) + 36
Public Const FID_POSLINE_DealGroupMarginAmount = (CLASSID_POSLINE * &H10000) + 37
Public Const FID_POSLINE_DealGroupMarginCode = (CLASSID_POSLINE * &H10000) + 38
Public Const FID_POSLINE_MultiBuyMarginAmount = (CLASSID_POSLINE * &H10000) + 39
Public Const FID_POSLINE_MultiBuyMarginCode = (CLASSID_POSLINE * &H10000) + 40
Public Const FID_POSLINE_HierarchyMarginAmount = (CLASSID_POSLINE * &H10000) + 41
Public Const FID_POSLINE_HierarchyMarginCode = (CLASSID_POSLINE * &H10000) + 42
Public Const FID_POSLINE_EmpSalePrimaryMarginAmount = (CLASSID_POSLINE * &H10000) + 43
Public Const FID_POSLINE_EmpSalePrimaryMarginCode = (CLASSID_POSLINE * &H10000) + 44
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSLINE_END_OF_STATIC = FID_POSLINE_EmpSalePrimaryMarginCode


Public Const FID_POSPAYMENT_TransactionDate = (CLASSID_POSPAYMENT * &H10000) + 1
Public Const FID_POSPAYMENT_TillID = (CLASSID_POSPAYMENT * &H10000) + 2
Public Const FID_POSPAYMENT_TransactionNumber = (CLASSID_POSPAYMENT * &H10000) + 3
Public Const FID_POSPAYMENT_SequenceNumber = (CLASSID_POSPAYMENT * &H10000) + 4
Public Const FID_POSPAYMENT_TenderType = (CLASSID_POSPAYMENT * &H10000) + 5
Public Const FID_POSPAYMENT_TenderAmount = (CLASSID_POSPAYMENT * &H10000) + 6
Public Const FID_POSPAYMENT_CreditCardNumber = (CLASSID_POSPAYMENT * &H10000) + 7
Public Const FID_POSPAYMENT_CreditCardStartDate = (CLASSID_POSPAYMENT * &H10000) + 8
Public Const FID_POSPAYMENT_CreditCardExpiryDate = (CLASSID_POSPAYMENT * &H10000) + 9
Public Const FID_POSPAYMENT_IssuerNumber = (CLASSID_POSPAYMENT * &H10000) + 10
Public Const FID_POSPAYMENT_AuthorisationCode = (CLASSID_POSPAYMENT * &H10000) + 11
Public Const FID_POSPAYMENT_CCNumberKeyed = (CLASSID_POSPAYMENT * &H10000) + 12
Public Const FID_POSPAYMENT_ChequeAccountNumber = (CLASSID_POSPAYMENT * &H10000) + 13
Public Const FID_POSPAYMENT_ChequeSortCode = (CLASSID_POSPAYMENT * &H10000) + 14
Public Const FID_POSPAYMENT_ChequeNumber = (CLASSID_POSPAYMENT * &H10000) + 15
Public Const FID_POSPAYMENT_SupervisorCode = (CLASSID_POSPAYMENT * &H10000) + 16
Public Const FID_POSPAYMENT_EFTPOSVoucherNo = (CLASSID_POSPAYMENT * &H10000) + 17
Public Const FID_POSPAYMENT_EFTPOSCommsDone = (CLASSID_POSPAYMENT * &H10000) + 18
Public Const FID_POSPAYMENT_EFTPOSMerchantNo = (CLASSID_POSPAYMENT * &H10000) + 19
Public Const FID_POSPAYMENT_DigitCount = (CLASSID_POSPAYMENT * &H10000) + 20
Public Const FID_POSPAYMENT_CouponNumber = (CLASSID_POSPAYMENT * &H10000) + 21
Public Const FID_POSPAYMENT_CouponPostCode = (CLASSID_POSPAYMENT * &H10000) + 22
Public Const FID_POSPAYMENT_CashBalanceProcessed = (CLASSID_POSPAYMENT * &H10000) + 23
Public Const FID_POSPAYMENT_AuthorisationType = (CLASSID_POSPAYMENT * &H10000) + 24
Public Const FID_POSPAYMENT_CashBackAmount = (CLASSID_POSPAYMENT * &H10000) + 25
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSPAYMENT_END_OF_STATIC = FID_POSPAYMENT_CashBackAmount

'Public Const FID_DISCOUNTRATE_DepartmentCode = (CLASSID_DISCOUNTRATE * &H10000) + 1
'Public Const FID_DISCOUNTRATE_GroupCode = (CLASSID_DISCOUNTRATE * &H10000) + 2
'Public Const FID_DISCOUNTRATE_PartCode = (CLASSID_DISCOUNTRATE * &H10000) + 3
'Public Const FID_DISCOUNTRATE_DiscountRate = (CLASSID_DISCOUNTRATE * &H10000) + 4
'Public Const FID_DISCOUNTRATE_StartDate = (CLASSID_DISCOUNTRATE * &H10000) + 5
'Public Const FID_DISCOUNTRATE_EndDate = (CLASSID_DISCOUNTRATE * &H10000) + 6
'Public Const FID_DISCOUNTRATE_MarginErosion = (CLASSID_DISCOUNTRATE * &H10000) + 7
'Public Const FID_DISCOUNTRATE_END_OF_STATIC = FID_DISCOUNTRATE_MarginErosion

Public Const FID_CASHIER_CashierNumber = (CLASSID_CASHIER * &H10000) + 1
Public Const FID_CASHIER_Deleted = (CLASSID_CASHIER * &H10000) + 2
Public Const FID_CASHIER_Name = (CLASSID_CASHIER * &H10000) + 3
Public Const FID_CASHIER_Position = (CLASSID_CASHIER * &H10000) + 4
Public Const FID_CASHIER_SignOnCode = (CLASSID_CASHIER * &H10000) + 5
Public Const FID_CASHIER_Supervisor = (CLASSID_CASHIER * &H10000) + 6
Public Const FID_CASHIER_DefaultFloatAmount = (CLASSID_CASHIER * &H10000) + 7
Public Const FID_CASHIER_NoOfSKULines = (CLASSID_CASHIER * &H10000) + 8
Public Const FID_CASHIER_SKURetailSales = (CLASSID_CASHIER * &H10000) + 9
Public Const FID_CASHIER_SKUCostOfSales = (CLASSID_CASHIER * &H10000) + 10
Public Const FID_CASHIER_NoOfPriceViolations = (CLASSID_CASHIER * &H10000) + 11
Public Const FID_CASHIER_MarkUpValueViolations = (CLASSID_CASHIER * &H10000) + 12
Public Const FID_CASHIER_ProjectSalesFlag = (CLASSID_CASHIER * &H10000) + 13
Public Const FID_CASHIER_NoOfPSSQuotes = (CLASSID_CASHIER * &H10000) + 14
Public Const FID_CASHIER_NoOfPSSCustomers = (CLASSID_CASHIER * &H10000) + 15
Public Const FID_CASHIER_NoOfPSSQuoteConversions = (CLASSID_CASHIER * &H10000) + 16
Public Const FID_CASHIER_NoOfPSSFinancedSales = (CLASSID_CASHIER * &H10000) + 17
Public Const FID_CASHIER_PSSSales = (CLASSID_CASHIER * &H10000) + 18
Public Const FID_CASHIER_PSSFinancedSales = (CLASSID_CASHIER * &H10000) + 19
Public Const FID_CASHIER_PSSInstallations = (CLASSID_CASHIER * &H10000) + 20
Public Const FID_CASHIER_PSSAccessories = (CLASSID_CASHIER * &H10000) + 21
Public Const FID_CASHIER_PSSCommissions = (CLASSID_CASHIER * &H10000) + 22
Public Const FID_CASHIER_PSSValues = (CLASSID_CASHIER * &H10000) + 23
Public Const FID_CASHIER_PSSCancelledOrders = (CLASSID_CASHIER * &H10000) + 24
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CASHIER_END_OF_STATIC = FID_CASHIER_PSSCancelledOrders

'Public Const FID_PAYHOURS_StoreNumber = (CLASSID_PAYHOURS * &H10000) + 1
'Public Const FID_PAYHOURS_CashierNo = (CLASSID_PAYHOURS * &H10000) + 2
'Public Const FID_PAYHOURS_CashierPayrollNo = (CLASSID_PAYHOURS * &H10000) + 3
'Public Const FID_PAYHOURS_DateHoursApplicable = (CLASSID_PAYHOURS * &H10000) + 4
'Public Const FID_PAYHOURS_WeekEndingDate = (CLASSID_PAYHOURS * &H10000) + 5
'Public Const FID_PAYHOURS_CommNo = (CLASSID_PAYHOURS * &H10000) + 6
'Public Const FID_PAYHOURS_HoursType = (CLASSID_PAYHOURS * &H10000) + 7
'Public Const FID_PAYHOURS_HoursWorked = (CLASSID_PAYHOURS * &H10000) + 8
'Public Const FID_PAYHOURS_OvertimeHours = (CLASSID_PAYHOURS * &H10000) + 9
'Public Const FID_PAYHOURS_ClockInTime = (CLASSID_PAYHOURS * &H10000) + 10
'Public Const FID_PAYHOURS_ClockOutTime = (CLASSID_PAYHOURS * &H10000) + 11
'Public Const FID_PAYHOURS_OvertimeOverriden = (CLASSID_PAYHOURS * &H10000) + 12
'Public Const FID_PAYHOURS_HoursAuthorised = (CLASSID_PAYHOURS * &H10000) + 13
'Public Const FID_PAYHOURS_AuthorisedID = (CLASSID_PAYHOURS * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_PAYHOURS_END_OF_STATIC = FID_PAYHOURS_AuthorisedID

Public Const FID_INVENTORY_PartCode = (CLASSID_INVENTORY * &H10000) + 1
Public Const FID_INVENTORY_Description = (CLASSID_INVENTORY * &H10000) + 2
Public Const FID_INVENTORY_VATRate = (CLASSID_INVENTORY * &H10000) + 3
Public Const FID_INVENTORY_SupplierUnitCode = (CLASSID_INVENTORY * &H10000) + 4
Public Const FID_INVENTORY_SupplierNo = (CLASSID_INVENTORY * &H10000) + 5
Public Const FID_INVENTORY_SupplierPartCode = (CLASSID_INVENTORY * &H10000) + 6
Public Const FID_INVENTORY_SupplierPackSize = (CLASSID_INVENTORY * &H10000) + 7
Public Const FID_INVENTORY_NormalSellPrice = (CLASSID_INVENTORY * &H10000) + 8
Public Const FID_INVENTORY_PriorSellPrice = (CLASSID_INVENTORY * &H10000) + 9
Public Const FID_INVENTORY_CostPrice = (CLASSID_INVENTORY * &H10000) + 10
Public Const FID_INVENTORY_LastSold = (CLASSID_INVENTORY * &H10000) + 11
Public Const FID_INVENTORY_LastReceived = (CLASSID_INVENTORY * &H10000) + 12
Public Const FID_INVENTORY_LastOrdered = (CLASSID_INVENTORY * &H10000) + 13
Public Const FID_INVENTORY_PriceEffectiveDate = (CLASSID_INVENTORY * &H10000) + 14
Public Const FID_INVENTORY_DateCreated = (CLASSID_INVENTORY * &H10000) + 15
Public Const FID_INVENTORY_DateObsolete = (CLASSID_INVENTORY * &H10000) + 16
Public Const FID_INVENTORY_DateDeleted = (CLASSID_INVENTORY * &H10000) + 17
Public Const FID_INVENTORY_QuantityAtHand = (CLASSID_INVENTORY * &H10000) + 18
Public Const FID_INVENTORY_QuantityOnOrder = (CLASSID_INVENTORY * &H10000) + 19
Public Const FID_INVENTORY_MinQuantity = (CLASSID_INVENTORY * &H10000) + 20
Public Const FID_INVENTORY_MaxQuantity = (CLASSID_INVENTORY * &H10000) + 21
Public Const FID_INVENTORY_Status = (CLASSID_INVENTORY * &H10000) + 22
Public Const FID_INVENTORY_RelatedItemSingle = (CLASSID_INVENTORY * &H10000) + 23
Public Const FID_INVENTORY_RelatedNoItems = (CLASSID_INVENTORY * &H10000) + 24
Public Const FID_INVENTORY_CatchAll = (CLASSID_INVENTORY * &H10000) + 25
Public Const FID_INVENTORY_ItemObsolete = (CLASSID_INVENTORY * &H10000) + 26
Public Const FID_INVENTORY_ItemDeleted = (CLASSID_INVENTORY * &H10000) + 27
Public Const FID_INVENTORY_ItemTagged = (CLASSID_INVENTORY * &H10000) + 28
Public Const FID_INVENTORY_Warranty = (CLASSID_INVENTORY * &H10000) + 29
Public Const FID_INVENTORY_EAN = (CLASSID_INVENTORY * &H10000) + 30
Public Const FID_INVENTORY_ValueSoldYesterday = (CLASSID_INVENTORY * &H10000) + 31
Public Const FID_INVENTORY_UnitsSoldYesterday = (CLASSID_INVENTORY * &H10000) + 32
Public Const FID_INVENTORY_ValueSoldThisWeek = (CLASSID_INVENTORY * &H10000) + 33
Public Const FID_INVENTORY_UnitsSoldThisWeek = (CLASSID_INVENTORY * &H10000) + 34
Public Const FID_INVENTORY_ValueSoldLastWeek = (CLASSID_INVENTORY * &H10000) + 35
Public Const FID_INVENTORY_UnitsSoldLastWeek = (CLASSID_INVENTORY * &H10000) + 36
Public Const FID_INVENTORY_ValueSoldThisPeriod = (CLASSID_INVENTORY * &H10000) + 37
Public Const FID_INVENTORY_UnitsSoldThisPeriod = (CLASSID_INVENTORY * &H10000) + 38
Public Const FID_INVENTORY_ValueSoldLastPeriod = (CLASSID_INVENTORY * &H10000) + 39
Public Const FID_INVENTORY_UnitsSoldLastPeriod = (CLASSID_INVENTORY * &H10000) + 40
Public Const FID_INVENTORY_ValueSoldThisYear = (CLASSID_INVENTORY * &H10000) + 41
Public Const FID_INVENTORY_UnitsSoldThisYear = (CLASSID_INVENTORY * &H10000) + 42
Public Const FID_INVENTORY_ValueSoldLastYear = (CLASSID_INVENTORY * &H10000) + 43
Public Const FID_INVENTORY_UnitsSoldLastYear = (CLASSID_INVENTORY * &H10000) + 44
Public Const FID_INVENTORY_DaysOutStockPeriod = (CLASSID_INVENTORY * &H10000) + 45
Public Const FID_INVENTORY_AverageWeeklySales = (CLASSID_INVENTORY * &H10000) + 46
Public Const FID_INVENTORY_DateFirstStock = (CLASSID_INVENTORY * &H10000) + 47
Public Const FID_INVENTORY_NoWeeksUpdated = (CLASSID_INVENTORY * &H10000) + 48
Public Const FID_INVENTORY_WeeklyUpdate = (CLASSID_INVENTORY * &H10000) + 49
Public Const FID_INVENTORY_ConfidentWeeklyAverage = (CLASSID_INVENTORY * &H10000) + 50
Public Const FID_INVENTORY_UnitsSoldWeek = (CLASSID_INVENTORY * &H10000) + 51
Public Const FID_INVENTORY_CalcDay = (CLASSID_INVENTORY * &H10000) + 52
Public Const FID_INVENTORY_Weight = (CLASSID_INVENTORY * &H10000) + 53
Public Const FID_INVENTORY_ShortDescription = (CLASSID_INVENTORY * &H10000) + 54
Public Const FID_INVENTORY_NoOfPriceChangeRecs = (CLASSID_INVENTORY * &H10000) + 55
Public Const FID_INVENTORY_NonStockItem = (CLASSID_INVENTORY * &H10000) + 56
Public Const FID_INVENTORY_DaysOutOfStock = (CLASSID_INVENTORY * &H10000) + 57
Public Const FID_INVENTORY_AvgWeeklySales = (CLASSID_INVENTORY * &H10000) + 58
Public Const FID_INVENTORY_SOQv4Weeks = (CLASSID_INVENTORY * &H10000) + 59
Public Const FID_INVENTORY_SOQv13Weeks = (CLASSID_INVENTORY * &H10000) + 60
Public Const FID_INVENTORY_SOQvLastWeek = (CLASSID_INVENTORY * &H10000) + 61
Public Const FID_INVENTORY_OrderPlacedThisSOQ = (CLASSID_INVENTORY * &H10000) + 62
Public Const FID_INVENTORY_UnitsReceivedToday = (CLASSID_INVENTORY * &H10000) + 63
Public Const FID_INVENTORY_ValueReceivedToday = (CLASSID_INVENTORY * &H10000) + 64
Public Const FID_INVENTORY_ActivityToday = (CLASSID_INVENTORY * &H10000) + 65
Public Const FID_INVENTORY_UnitsInOpenReturns = (CLASSID_INVENTORY * &H10000) + 66
Public Const FID_INVENTORY_ValueInOpenReturns = (CLASSID_INVENTORY * &H10000) + 67
Public Const FID_INVENTORY_HOCheckDigit = (CLASSID_INVENTORY * &H10000) + 68
Public Const FID_INVENTORY_NoOfSmallLabels = (CLASSID_INVENTORY * &H10000) + 69
Public Const FID_INVENTORY_UnitsStandingOrders = (CLASSID_INVENTORY * &H10000) + 70
Public Const FID_INVENTORY_ValueStandingOrders = (CLASSID_INVENTORY * &H10000) + 71
Public Const FID_INVENTORY_StandingOrderDay1 = (CLASSID_INVENTORY * &H10000) + 72
Public Const FID_INVENTORY_StandingOrderDay2 = (CLASSID_INVENTORY * &H10000) + 73
Public Const FID_INVENTORY_StandingOrderDay3 = (CLASSID_INVENTORY * &H10000) + 74
Public Const FID_INVENTORY_NoOfMediumLabels = (CLASSID_INVENTORY * &H10000) + 75
Public Const FID_INVENTORY_NoOfLargeLabels = (CLASSID_INVENTORY * &H10000) + 76
Public Const FID_INVENTORY_ItemVolume = (CLASSID_INVENTORY * &H10000) + 77
Public Const FID_INVENTORY_DoNotOrder = (CLASSID_INVENTORY * &H10000) + 78
Public Const FID_INVENTORY_AlternateSupplier = (CLASSID_INVENTORY * &H10000) + 79
Public Const FID_INVENTORY_InitialOrderDate = (CLASSID_INVENTORY * &H10000) + 80
Public Const FID_INVENTORY_FinalOrderDate = (CLASSID_INVENTORY * &H10000) + 81
Public Const FID_INVENTORY_PromotionalMinimum = (CLASSID_INVENTORY * &H10000) + 82
Public Const FID_INVENTORY_PromotionalMinimumStart = (CLASSID_INVENTORY * &H10000) + 83
Public Const FID_INVENTORY_PromotionalMinimumEnd = (CLASSID_INVENTORY * &H10000) + 84
Public Const FID_INVENTORY_PromotionalMinimumMCP = (CLASSID_INVENTORY * &H10000) + 85
Public Const FID_INVENTORY_SpacemanDisplayFactor = (CLASSID_INVENTORY * &H10000) + 86
Public Const FID_INVENTORY_DateMinimumLastChanged = (CLASSID_INVENTORY * &H10000) + 87
Public Const FID_INVENTORY_ParticipatingInMCP = (CLASSID_INVENTORY * &H10000) + 88
Public Const FID_INVENTORY_ItemTagType = (CLASSID_INVENTORY * &H10000) + 89
Public Const FID_INVENTORY_ProductEquivalentDescription = (CLASSID_INVENTORY * &H10000) + 90
Public Const FID_INVENTORY_HierCategoryGroup = (CLASSID_INVENTORY * &H10000) + 91
Public Const FID_INVENTORY_RetailPriceEventNo = (CLASSID_INVENTORY * &H10000) + 92
Public Const FID_INVENTORY_RetailPricePriority = (CLASSID_INVENTORY * &H10000) + 93
Public Const FID_INVENTORY_EquivalentPriceMult = (CLASSID_INVENTORY * &H10000) + 94
Public Const FID_INVENTORY_EquivalentPriceUnit = (CLASSID_INVENTORY * &H10000) + 95
Public Const FID_INVENTORY_MarkDownQuantity = (CLASSID_INVENTORY * &H10000) + 96
Public Const FID_INVENTORY_WriteOffQuantity = (CLASSID_INVENTORY * &H10000) + 97
Public Const FID_INVENTORY_AutoApplyPriceChanges = (CLASSID_INVENTORY * &H10000) + 98
Public Const FID_INVENTORY_AllowAdjustments = (CLASSID_INVENTORY * &H10000) + 99
Public Const FID_INVENTORY_SavedSupplier = (CLASSID_INVENTORY * &H10000) + 100
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_INVENTORY_END_OF_STATIC = FID_INVENTORY_SavedSupplier


Public Const FID_AUDITMASTER_PartCode = (CLASSID_AUDITMASTER * &H10000) + 1
Public Const FID_AUDITMASTER_OriginalOnHandQty = (CLASSID_AUDITMASTER * &H10000) + 2
Public Const FID_AUDITMASTER_EnteredOnHandQty = (CLASSID_AUDITMASTER * &H10000) + 3
Public Const FID_AUDITMASTER_Captured = (CLASSID_AUDITMASTER * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_AUDITMASTER_END_OF_STATIC = FID_AUDITMASTER_Captured


Public Const FID_STOCKMOVE_PartCode = (CLASSID_STOCKMOVE * &H10000) + 1
Public Const FID_STOCKMOVE_StartQty = (CLASSID_STOCKMOVE * &H10000) + 2
Public Const FID_STOCKMOVE_StartPrice = (CLASSID_STOCKMOVE * &H10000) + 3
Public Const FID_STOCKMOVE_CyclicCountValue = (CLASSID_STOCKMOVE * &H10000) + 4
Public Const FID_STOCKMOVE_DRLAdjustmentValue = (CLASSID_STOCKMOVE * &H10000) + 5
Public Const FID_STOCKMOVE_DRLReceiptsQty = (CLASSID_STOCKMOVE * &H10000) + 6
Public Const FID_STOCKMOVE_DRLReceiptsValue = (CLASSID_STOCKMOVE * &H10000) + 7
Public Const FID_STOCKMOVE_AdjustmentsQty = (CLASSID_STOCKMOVE * &H10000) + 8
Public Const FID_STOCKMOVE_AdjustmentsValue = (CLASSID_STOCKMOVE * &H10000) + 9
Public Const FID_STOCKMOVE_PriceViolations = (CLASSID_STOCKMOVE * &H10000) + 10
Public Const FID_STOCKMOVE_ISTNETQuantity = (CLASSID_STOCKMOVE * &H10000) + 11
Public Const FID_STOCKMOVE_ISTNetValue = (CLASSID_STOCKMOVE * &H10000) + 12
Public Const FID_STOCKMOVE_ReturnsQuantity = (CLASSID_STOCKMOVE * &H10000) + 13
Public Const FID_STOCKMOVE_ReturnsValue = (CLASSID_STOCKMOVE * &H10000) + 14
Public Const FID_STOCKMOVE_BulkToSingleQty = (CLASSID_STOCKMOVE * &H10000) + 15
Public Const FID_STOCKMOVE_BulkToSingleValue = (CLASSID_STOCKMOVE * &H10000) + 16
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKMOVE_END_OF_STATIC = FID_STOCKMOVE_BulkToSingleValue


Public Const FID_DELNOTEHEADER_DocNo = (CLASSID_DELNOTEHEADER * &H10000) + 1
Public Const FID_DELNOTEHEADER_DocType = (CLASSID_DELNOTEHEADER * &H10000) + 2
Public Const FID_DELNOTEHEADER_Value = (CLASSID_DELNOTEHEADER * &H10000) + 3
Public Const FID_DELNOTEHEADER_Cost = (CLASSID_DELNOTEHEADER * &H10000) + 4
Public Const FID_DELNOTEHEADER_DelNoteDate = (CLASSID_DELNOTEHEADER * &H10000) + 5
Public Const FID_DELNOTEHEADER_Comm = (CLASSID_DELNOTEHEADER * &H10000) + 6
Public Const FID_DELNOTEHEADER_OrigDocKey = (CLASSID_DELNOTEHEADER * &H10000) + 7
Public Const FID_DELNOTEHEADER_EnteredBy = (CLASSID_DELNOTEHEADER * &H10000) + 8
Public Const FID_DELNOTEHEADER_Comment = (CLASSID_DELNOTEHEADER * &H10000) + 9
Public Const FID_DELNOTEHEADER_SupplierONumber = (CLASSID_DELNOTEHEADER * &H10000) + 10
Public Const FID_DELNOTEHEADER_ConsignmentRef = (CLASSID_DELNOTEHEADER * &H10000) + 11
Public Const FID_DELNOTEHEADER_PONumber = (CLASSID_DELNOTEHEADER * &H10000) + 12
Public Const FID_DELNOTEHEADER_PODate = (CLASSID_DELNOTEHEADER * &H10000) + 13
Public Const FID_DELNOTEHEADER_AssignedPORelease = (CLASSID_DELNOTEHEADER * &H10000) + 14
Public Const FID_DELNOTEHEADER_SOQControlNumber = (CLASSID_DELNOTEHEADER * &H10000) + 15
Public Const FID_DELNOTEHEADER_SupplierDelNote = (CLASSID_DELNOTEHEADER * &H10000) + 16
Public Const FID_DELNOTEHEADER_Source = (CLASSID_DELNOTEHEADER * &H10000) + 17
Public Const FID_DELNOTEHEADER_StoreNo = (CLASSID_DELNOTEHEADER * &H10000) + 18
Public Const FID_DELNOTEHEADER_IBT = (CLASSID_DELNOTEHEADER * &H10000) + 19
Public Const FID_DELNOTEHEADER_Printed = (CLASSID_DELNOTEHEADER * &H10000) + 20
Public Const FID_DELNOTEHEADER_ConsignmentKey = (CLASSID_DELNOTEHEADER * &H10000) + 21
Public Const FID_DELNOTEHEADER_SupplierNumber = (CLASSID_DELNOTEHEADER * &H10000) + 22
Public Const FID_DELNOTEHEADER_ReturnDate = (CLASSID_DELNOTEHEADER * &H10000) + 23
Public Const FID_DELNOTEHEADER_OrigPONumber = (CLASSID_DELNOTEHEADER * &H10000) + 24
Public Const FID_DELNOTEHEADER_SupplierReturnNumber = (CLASSID_DELNOTEHEADER * &H10000) + 25
Public Const FID_DELNOTEHEADER_InvCrnMatched = (CLASSID_DELNOTEHEADER * &H10000) + 26
Public Const FID_DELNOTEHEADER_InvCrnDate = (CLASSID_DELNOTEHEADER * &H10000) + 27
Public Const FID_DELNOTEHEADER_InvCrnNumber = (CLASSID_DELNOTEHEADER * &H10000) + 28
Public Const FID_DELNOTEHEADER_VarianceValue = (CLASSID_DELNOTEHEADER * &H10000) + 29
Public Const FID_DELNOTEHEADER_InvoiceValue = (CLASSID_DELNOTEHEADER * &H10000) + 30
Public Const FID_DELNOTEHEADER_InvoiceVAT = (CLASSID_DELNOTEHEADER * &H10000) + 31
Public Const FID_DELNOTEHEADER_MatchingComment = (CLASSID_DELNOTEHEADER * &H10000) + 32
Public Const FID_DELNOTEHEADER_MatchingDate = (CLASSID_DELNOTEHEADER * &H10000) + 33
Public Const FID_DELNOTEHEADER_ReceiptDate = (CLASSID_DELNOTEHEADER * &H10000) + 34
Public Const FID_DELNOTEHEADER_CarraigeValue = (CLASSID_DELNOTEHEADER * &H10000) + 35
Public Const FID_DELNOTEHEADER_Commit = (CLASSID_DELNOTEHEADER * &H10000) + 36
Public Const FID_DELNOTEHEADER_OrigDocNo = FID_DELNOTEHEADER_MatchingComment
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_DELNOTEHEADER_END_OF_STATIC = FID_DELNOTEHEADER_OrigDocNo


Public Const FID_DELNOTELINE_DRLKey = (CLASSID_DELNOTELINE * &H10000) + 1
Public Const FID_DELNOTELINE_LineNo = (CLASSID_DELNOTELINE * &H10000) + 2
Public Const FID_DELNOTELINE_PartCode = (CLASSID_DELNOTELINE * &H10000) + 3
Public Const FID_DELNOTELINE_PartCodeAlphaCode = (CLASSID_DELNOTELINE * &H10000) + 4
Public Const FID_DELNOTELINE_OrderQty = (CLASSID_DELNOTELINE * &H10000) + 5
Public Const FID_DELNOTELINE_ReceivedQty = (CLASSID_DELNOTELINE * &H10000) + 6
Public Const FID_DELNOTELINE_OrderPrice = (CLASSID_DELNOTELINE * &H10000) + 7
Public Const FID_DELNOTELINE_BlanketDiscDesc = (CLASSID_DELNOTELINE * &H10000) + 8
Public Const FID_DELNOTELINE_BlanketPartCode = (CLASSID_DELNOTELINE * &H10000) + 9
Public Const FID_DELNOTELINE_CurrentPrice = (CLASSID_DELNOTELINE * &H10000) + 10
Public Const FID_DELNOTELINE_CurrentCost = (CLASSID_DELNOTELINE * &H10000) + 11
Public Const FID_DELNOTELINE_ShortageQty = (CLASSID_DELNOTELINE * &H10000) + 12
Public Const FID_DELNOTELINE_ReturnQty = (CLASSID_DELNOTELINE * &H10000) + 13
Public Const FID_DELNOTELINE_ToFollowQty = (CLASSID_DELNOTELINE * &H10000) + 14
Public Const FID_DELNOTELINE_ReturnReasonCode = (CLASSID_DELNOTELINE * &H10000) + 15
Public Const FID_DELNOTELINE_POLineNumber = (CLASSID_DELNOTELINE * &H10000) + 16
Public Const FID_DELNOTELINE_Checked = (CLASSID_DELNOTELINE * &H10000) + 17
Public Const FID_DELNOTELINE_SinglesSellingUnit = (CLASSID_DELNOTELINE * &H10000) + 18
Public Const FID_DELNOTELINE_OverQty = (CLASSID_DELNOTELINE * &H10000) + 19
Public Const FID_DELNOTELINE_Narrative = (CLASSID_DELNOTELINE * &H10000) + 20
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_DELNOTELINE_END_OF_STATIC = FID_DELNOTELINE_Narrative

'Public Const FID_DEPARTMENT_DepartmentCode = (CLASSID_DEPARTMENT * &H10000) + 1
'Public Const FID_DEPARTMENT_Description = (CLASSID_DEPARTMENT * &H10000) + 2
'Public Const FID_DEPARTMENT_Size = (CLASSID_DEPARTMENT * &H10000) + 3
'Public Const FID_DEPARTMENT_VatCode = (CLASSID_DEPARTMENT * &H10000) + 4
'Public Const FID_DEPARTMENT_CostFactor = (CLASSID_DEPARTMENT * &H10000) + 5
'Public Const FID_DEPARTMENT_NoPartLines = (CLASSID_DEPARTMENT * &H10000) + 6
'Public Const FID_DEPARTMENT_NoDeptLines = (CLASSID_DEPARTMENT * &H10000) + 7
'Public Const FID_DEPARTMENT_PartRetailSales = (CLASSID_DEPARTMENT * &H10000) + 8
'Public Const FID_DEPARTMENT_DeptRetailSales = (CLASSID_DEPARTMENT * &H10000) + 9
'Public Const FID_DEPARTMENT_PartCostOfSales = (CLASSID_DEPARTMENT * &H10000) + 10
'Public Const FID_DEPARTMENT_DeptCostOfSales = (CLASSID_DEPARTMENT * &H10000) + 11
'Public Const FID_DEPARTMENT_NoPriceViolations = (CLASSID_DEPARTMENT * &H10000) + 12
'Public Const FID_DEPARTMENT_ValueOfViolations = (CLASSID_DEPARTMENT * &H10000) + 13
'Public Const FID_DEPARTMENT_WeeklySalesValue = (CLASSID_DEPARTMENT * &H10000) + 14
'Public Const FID_DEPARTMENT_WeeklyItemLookups = (CLASSID_DEPARTMENT * &H10000) + 15
'Public Const FID_DEPARTMENT_WeeklyOutOfStock = (CLASSID_DEPARTMENT * &H10000) + 16
'Public Const FID_DEPARTMENT_AllowZeroPrices = (CLASSID_DEPARTMENT * &H10000) + 17
'Public Const FID_DEPARTMENT_DepartmentSales = (CLASSID_DEPARTMENT * &H10000) + 18
'Public Const FID_DEPARTMENT_END_OF_STATIC = FID_DEPARTMENT_DepartmentSales


Public Const FID_STORE_StoreNumber = (CLASSID_STORE * &H10000) + 1
Public Const FID_STORE_AddressLine1 = (CLASSID_STORE * &H10000) + 2
Public Const FID_STORE_AddressLine2 = (CLASSID_STORE * &H10000) + 3
Public Const FID_STORE_AddressLine3 = (CLASSID_STORE * &H10000) + 4
Public Const FID_STORE_AddressLine4 = (CLASSID_STORE * &H10000) + 5
Public Const FID_STORE_PostCode = (CLASSID_STORE * &H10000) + 6
Public Const FID_STORE_PhoneNo = (CLASSID_STORE * &H10000) + 7
Public Const FID_STORE_FaxNo = (CLASSID_STORE * &H10000) + 8
Public Const FID_STORE_SOQDay = (CLASSID_STORE * &H10000) + 9
Public Const FID_STORE_PriceBand = (CLASSID_STORE * &H10000) + 10
Public Const FID_STORE_TOPPS = (CLASSID_STORE * &H10000) + 11
Public Const FID_STORE_Warehouse = (CLASSID_STORE * &H10000) + 12
Public Const FID_STORE_StoreCategory = (CLASSID_STORE * &H10000) + 13
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STORE_END_OF_STATIC = FID_STORE_StoreCategory


Public Const FID_RETURNCUST_TransactionDate = (CLASSID_RETURNCUST * &H10000) + 1
Public Const FID_RETURNCUST_PCTillID = (CLASSID_RETURNCUST * &H10000) + 2
Public Const FID_RETURNCUST_TransactionNumber = (CLASSID_RETURNCUST * &H10000) + 3
Public Const FID_RETURNCUST_LineNumber = (CLASSID_RETURNCUST * &H10000) + 4
Public Const FID_RETURNCUST_CustomerName = (CLASSID_RETURNCUST * &H10000) + 5
Public Const FID_RETURNCUST_AddressLine1 = (CLASSID_RETURNCUST * &H10000) + 6
Public Const FID_RETURNCUST_AddressLine2 = (CLASSID_RETURNCUST * &H10000) + 7
Public Const FID_RETURNCUST_AddressLine3 = (CLASSID_RETURNCUST * &H10000) + 8
Public Const FID_RETURNCUST_PostCode = (CLASSID_RETURNCUST * &H10000) + 9
Public Const FID_RETURNCUST_PhoneNo = (CLASSID_RETURNCUST * &H10000) + 10
Public Const FID_RETURNCUST_RefundReason = (CLASSID_RETURNCUST * &H10000) + 11
Public Const FID_RETURNCUST_OrigTranValidated = (CLASSID_RETURNCUST * &H10000) + 12
Public Const FID_RETURNCUST_OrigTenderType = (CLASSID_RETURNCUST * &H10000) + 13
Public Const FID_RETURNCUST_LabelsRequired = (CLASSID_RETURNCUST * &H10000) + 14
Public Const FID_RETURNCUST_HouseName = (CLASSID_RETURNCUST * &H10000) + 15
Public Const FID_RETURNCUST_OrigTranStore = (CLASSID_RETURNCUST * &H10000) + 16
Public Const FID_RETURNCUST_OrigTranDate = (CLASSID_RETURNCUST * &H10000) + 17
Public Const FID_RETURNCUST_OrigTranTill = (CLASSID_RETURNCUST * &H10000) + 18
Public Const FID_RETURNCUST_OrigTranNumb = (CLASSID_RETURNCUST * &H10000) + 19
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETURNCUST_END_OF_STATIC = FID_RETURNCUST_OrigTranNumb


Public Const FID_SYSTEMNO_RecID = (CLASSID_SYSTEMNO * &H10000) + 1
Public Const FID_SYSTEMNO_Value = (CLASSID_SYSTEMNO * &H10000) + 2
Public Const FID_SYSTEMNO_Width = (CLASSID_SYSTEMNO * &H10000) + 3
Public Const FID_SYSTEMNO_ID = (CLASSID_SYSTEMNO * &H10000) + 4 'TBA
Public Const FID_SYSTEMNO_Prefix = (CLASSID_SYSTEMNO * &H10000) + 5 'TBA
Public Const FID_SYSTEMNO_Suffix = (CLASSID_SYSTEMNO * &H10000) + 6 'TBA
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SYSTEMNO_END_OF_STATIC = FID_SYSTEMNO_Suffix


Public Const FID_NIGHTLOG_LogDate = (CLASSID_NIGHTLOG * &H10000) + 1
Public Const FID_NIGHTLOG_SetNumber = (CLASSID_NIGHTLOG * &H10000) + 2
Public Const FID_NIGHTLOG_TaskNumber = (CLASSID_NIGHTLOG * &H10000) + 3
Public Const FID_NIGHTLOG_Description = (CLASSID_NIGHTLOG * &H10000) + 4
Public Const FID_NIGHTLOG_ProgramPath = (CLASSID_NIGHTLOG * &H10000) + 5
Public Const FID_NIGHTLOG_DateStarted = (CLASSID_NIGHTLOG * &H10000) + 6
Public Const FID_NIGHTLOG_TimeStarted = (CLASSID_NIGHTLOG * &H10000) + 7
Public Const FID_NIGHTLOG_DateEnded = (CLASSID_NIGHTLOG * &H10000) + 8
Public Const FID_NIGHTLOG_TimeEnded = (CLASSID_NIGHTLOG * &H10000) + 9
Public Const FID_NIGHTLOG_DateAborted = (CLASSID_NIGHTLOG * &H10000) + 10
Public Const FID_NIGHTLOG_TimeAborted = (CLASSID_NIGHTLOG * &H10000) + 11
Public Const FID_NIGHTLOG_JobError = (CLASSID_NIGHTLOG * &H10000) + 12
Public Const FID_NIGHTLOG_ErrorMessage = (CLASSID_NIGHTLOG * &H10000) + 13
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_NIGHTLOG_END_OF_STATIC = FID_NIGHTLOG_ErrorMessage


Public Const FID_NIGHTMASTER_SetNumber = (CLASSID_NIGHTMASTER * &H10000) + 1
Public Const FID_NIGHTMASTER_TaskNumber = (CLASSID_NIGHTMASTER * &H10000) + 2
Public Const FID_NIGHTMASTER_Description = (CLASSID_NIGHTMASTER * &H10000) + 3
Public Const FID_NIGHTMASTER_ProgramPath = (CLASSID_NIGHTMASTER * &H10000) + 4
Public Const FID_NIGHTMASTER_RunNightly = (CLASSID_NIGHTMASTER * &H10000) + 5
Public Const FID_NIGHTMASTER_RunInRetry = (CLASSID_NIGHTMASTER * &H10000) + 6
Public Const FID_NIGHTMASTER_RunAtWeekEnd = (CLASSID_NIGHTMASTER * &H10000) + 7
Public Const FID_NIGHTMASTER_RunAtPeriodEnd = (CLASSID_NIGHTMASTER * &H10000) + 8
Public Const FID_NIGHTMASTER_RunBeforeStoreLive = (CLASSID_NIGHTMASTER * &H10000) + 9
Public Const FID_NIGHTMASTER_OptionNumber = (CLASSID_NIGHTMASTER * &H10000) + 10
Public Const FID_NIGHTMASTER_AbortOnError = (CLASSID_NIGHTMASTER * &H10000) + 11
Public Const FID_NIGHTMASTER_ErrorHandler = (CLASSID_NIGHTMASTER * &H10000) + 12
Public Const FID_NIGHTMASTER_RunOnDayWeek = (CLASSID_NIGHTMASTER * &H10000) + 13
Public Const FID_NIGHTMASTER_StartDate = (CLASSID_NIGHTMASTER * &H10000) + 14
Public Const FID_NIGHTMASTER_EndDate = (CLASSID_NIGHTMASTER * &H10000) + 15
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_NIGHTMASTER_END_OF_STATIC = FID_NIGHTMASTER_EndDate

'Public Const FID_SOHEADER_OrderNo = (CLASSID_SOHEADER * &H10000) + 1
'Public Const FID_SOHEADER_CustomerNo = (CLASSID_SOHEADER * &H10000) + 2
'Public Const FID_SOHEADER_Salesman = (CLASSID_SOHEADER * &H10000) + 3
'Public Const FID_SOHEADER_TillID = (CLASSID_SOHEADER * &H10000) + 4
'Public Const FID_SOHEADER_CustomerPresent = (CLASSID_SOHEADER * &H10000) + 5
'Public Const FID_SOHEADER_Name = (CLASSID_SOHEADER * &H10000) + 6
'Public Const FID_SOHEADER_AddressLine1 = (CLASSID_SOHEADER * &H10000) + 7
'Public Const FID_SOHEADER_AddressLine2 = (CLASSID_SOHEADER * &H10000) + 8
'Public Const FID_SOHEADER_AddressLine3 = (CLASSID_SOHEADER * &H10000) + 9
'Public Const FID_SOHEADER_PostCode = (CLASSID_SOHEADER * &H10000) + 10
'Public Const FID_SOHEADER_TelephoneNo = (CLASSID_SOHEADER * &H10000) + 11
'Public Const FID_SOHEADER_SecondTelNo = (CLASSID_SOHEADER * &H10000) + 12
'Public Const FID_SOHEADER_DepositNumber = (CLASSID_SOHEADER * &H10000) + 13
'Public Const FID_SOHEADER_DepositValue = (CLASSID_SOHEADER * &H10000) + 14
'Public Const FID_SOHEADER_OrderProcessed = (CLASSID_SOHEADER * &H10000) + 15
'Public Const FID_SOHEADER_Printed = (CLASSID_SOHEADER * &H10000) + 16
'Public Const FID_SOHEADER_Status = (CLASSID_SOHEADER * &H10000) + 17
'Public Const FID_SOHEADER_QuoteDate = (CLASSID_SOHEADER * &H10000) + 18
'Public Const FID_SOHEADER_QuoteExpiry = (CLASSID_SOHEADER * &H10000) + 19
'Public Const FID_SOHEADER_OrderDate = (CLASSID_SOHEADER * &H10000) + 20
'Public Const FID_SOHEADER_ETADate = (CLASSID_SOHEADER * &H10000) + 21
'Public Const FID_SOHEADER_NoDeliveries = (CLASSID_SOHEADER * &H10000) + 22
'Public Const FID_SOHEADER_Complete = (CLASSID_SOHEADER * &H10000) + 23
'Public Const FID_SOHEADER_LastDespatchDate = (CLASSID_SOHEADER * &H10000) + 24
'Public Const FID_SOHEADER_Cancelled = (CLASSID_SOHEADER * &H10000) + 25
'Public Const FID_SOHEADER_CancelledDate = (CLASSID_SOHEADER * &H10000) + 26
'Public Const FID_SOHEADER_CancelledCashierID = (CLASSID_SOHEADER * &H10000) + 27
'Public Const FID_SOHEADER_OrderValue = (CLASSID_SOHEADER * &H10000) + 28
'Public Const FID_SOHEADER_OutStandingValue = (CLASSID_SOHEADER * &H10000) + 29
'Public Const FID_SOHEADER_Remarks = (CLASSID_SOHEADER * &H10000) + 30
'Public Const FID_SOHEADER_NameAlphaKey = (CLASSID_SOHEADER * &H10000) + 31
'Public Const FID_SOHEADER_DiscountReasonCode = (CLASSID_SOHEADER * &H10000) + 32
''---------- Insert new Field IDs immediately before here,
''---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_SOHEADER_END_OF_STATIC = FID_SOHEADER_DiscountReasonCode

'Public Const FID_SOLINE_OrderNo = (CLASSID_SOLINE * &H10000) + 1
'Public Const FID_SOLINE_LineNo = (CLASSID_SOLINE * &H10000) + 2
'Public Const FID_SOLINE_PartCode = (CLASSID_SOLINE * &H10000) + 3
'Public Const FID_SOLINE_Description = (CLASSID_SOLINE * &H10000) + 4
'Public Const FID_SOLINE_SupplierNo = (CLASSID_SOLINE * &H10000) + 5
'Public Const FID_SOLINE_OrderQuantity = (CLASSID_SOLINE * &H10000) + 6
'Public Const FID_SOLINE_LookUpPrice = (CLASSID_SOLINE * &H10000) + 7
'Public Const FID_SOLINE_OrderPrice = (CLASSID_SOLINE * &H10000) + 8
'Public Const FID_SOLINE_ExtendedValue = (CLASSID_SOLINE * &H10000) + 9
'Public Const FID_SOLINE_ExtendedCost = (CLASSID_SOLINE * &H10000) + 10
'Public Const FID_SOLINE_LineDiscount = (CLASSID_SOLINE * &H10000) + 11
'Public Const FID_SOLINE_QtyPerUnit = (CLASSID_SOLINE * &H10000) + 12
'Public Const FID_SOLINE_RaiseOrder = (CLASSID_SOLINE * &H10000) + 13
'Public Const FID_SOLINE_PurchaseOrderNo = (CLASSID_SOLINE * &H10000) + 14
'Public Const FID_SOLINE_LastDelNoteNo = (CLASSID_SOLINE * &H10000) + 15
'Public Const FID_SOLINE_QuantitySold = (CLASSID_SOLINE * &H10000) + 16
'Public Const FID_SOLINE_Cancelled = (CLASSID_SOLINE * &H10000) + 17
'Public Const FID_SOLINE_Complete = (CLASSID_SOLINE * &H10000) + 18
'Public Const FID_SOLINE_CancellationReason = (CLASSID_SOLINE * &H10000) + 19
'Public Const FID_SOLINE_DateCreated = (CLASSID_SOLINE * &H10000) + 20
'Public Const FID_SOLINE_DiscountReasonCode = (CLASSID_SOLINE * &H10000) + 21
'Public Const FID_SOLINE_RefundAllowed = (CLASSID_SOLINE * &H10000) + 22
''---------- Insert new Field IDs immediately before here,
''---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_SOLINE_END_OF_STATIC = FID_SOLINE_RefundAllowed

'Public Const FID_SOORDERTYPE_OrderType = (CLASSID_SOORDERTYPE * &H10000) + 1
'Public Const FID_SOORDERTYPE_Description = (CLASSID_SOORDERTYPE * &H10000) + 2
'Public Const FID_SOORDERTYPE_Deleted = (CLASSID_SOORDERTYPE * &H10000) + 3
'Public Const FID_SOORDERTYPE_CreatePurchaseOrder = (CLASSID_SOORDERTYPE * &H10000) + 4
'Public Const FID_SOORDERTYPE_QuoteValidFor = (CLASSID_SOORDERTYPE * &H10000) + 5
'Public Const FID_SOORDERTYPE_DayOfWeek = (CLASSID_SOORDERTYPE * &H10000) + 6
'Public Const FID_SOORDERTYPE_NoDaysWithoutInstallation = (CLASSID_SOORDERTYPE * &H10000) + 7
'Public Const FID_SOORDERTYPE_ExtraDaysInstallation = (CLASSID_SOORDERTYPE * &H10000) + 8
''---------- Insert new Field IDs immediately before here,
''---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_SOORDERTYPE_END_OF_STATIC = FID_SOORDERTYPE_ExtraDaysInstallation


'Public Const FID_SORANGEDISC_Percentage = (CLASSID_SORANGEDISC * &H10000) + 1
'Public Const FID_SORANGEDISC_StartDate = (CLASSID_SORANGEDISC * &H10000) + 2
'Public Const FID_SORANGEDISC_EndDate = (CLASSID_SORANGEDISC * &H10000) + 3
''---------- Insert new Field IDs immediately before here,
''---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_SORANGEDISC_END_OF_STATIC = FID_SORANGEDISC_EndDate


'Public Const FID_SORANGE_OrderType = (CLASSID_SORANGE * &H10000) + 1
'Public Const FID_SORANGE_DepartmentNo = (CLASSID_SORANGE * &H10000) + 2
'Public Const FID_SORANGE_GroupNo = (CLASSID_SORANGE * &H10000) + 3
'Public Const FID_SORANGE_Deleted = (CLASSID_SORANGE * &H10000) + 4
'Public Const FID_SORANGE_AccessoriesAllowed = (CLASSID_SORANGE * &H10000) + 5
'Public Const FID_SORANGE_InstallationPartCode = (CLASSID_SORANGE * &H10000) + 6
'Public Const FID_SORANGE_Discount = (CLASSID_SORANGE * &H10000) + 7
''---------- Insert new Field IDs immediately before here,
''---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_SORANGE_END_OF_STATIC = FID_SORANGE_Discount

'Public Const FID_SORANGEACC_RangeOrderType = (CLASSID_SORANGEACC * &H10000) + 1
'Public Const FID_SORANGEACC_RangeDepartmentNo = (CLASSID_SORANGEACC * &H10000) + 2
'Public Const FID_SORANGEACC_RangeGroupNo = (CLASSID_SORANGEACC * &H10000) + 3
'Public Const FID_SORANGEACC_AccessoryDepartment = (CLASSID_SORANGEACC * &H10000) + 4
'Public Const FID_SORANGEACC_AccessoryGroup = (CLASSID_SORANGEACC * &H10000) + 5
'Public Const FID_SORANGEACC_Deleted = (CLASSID_SORANGEACC * &H10000) + 6
'Public Const FID_SORANGEACC_Discount = (CLASSID_SORANGEACC * &H10000) + 7
''---------- Insert new Field IDs immediately before here,
''---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_SORANGEACC_END_OF_STATIC = FID_SORANGEACC_Discount


Public Const FID_RETURNHDR_ReturnID = (CLASSID_RETURNHDR * &H10000) + 1
Public Const FID_RETURNHDR_SupplierReturnNo = (CLASSID_RETURNHDR * &H10000) + 2
Public Const FID_RETURNHDR_SupplierNo = (CLASSID_RETURNHDR * &H10000) + 3
Public Const FID_RETURNHDR_DateCreated = (CLASSID_RETURNHDR * &H10000) + 4
Public Const FID_RETURNHDR_CollectionDate = (CLASSID_RETURNHDR * &H10000) + 5
Public Const FID_RETURNHDR_PurchaseOrderNo = (CLASSID_RETURNHDR * &H10000) + 6
Public Const FID_RETURNHDR_DRLNo = (CLASSID_RETURNHDR * &H10000) + 7
Public Const FID_RETURNHDR_ReturnValue = (CLASSID_RETURNHDR * &H10000) + 8
Public Const FID_RETURNHDR_Printed = (CLASSID_RETURNHDR * &H10000) + 9
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETURNHDR_END_OF_STATIC = FID_RETURNHDR_Printed


Public Const FID_RETURNLINE_ReturnID = (CLASSID_RETURNLINE * &H10000) + 1
Public Const FID_RETURNLINE_LineNo = (CLASSID_RETURNLINE * &H10000) + 2
Public Const FID_RETURNLINE_PartCode = (CLASSID_RETURNLINE * &H10000) + 3
Public Const FID_RETURNLINE_PartAlphaKey = (CLASSID_RETURNLINE * &H10000) + 4
Public Const FID_RETURNLINE_QuantityReturned = (CLASSID_RETURNLINE * &H10000) + 5
Public Const FID_RETURNLINE_Price = (CLASSID_RETURNLINE * &H10000) + 6
Public Const FID_RETURNLINE_Cost = (CLASSID_RETURNLINE * &H10000) + 7
Public Const FID_RETURNLINE_ReturnCode = (CLASSID_RETURNLINE * &H10000) + 8
Public Const FID_RETURNLINE_ReturnReason = (CLASSID_RETURNLINE * &H10000) + 9
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETURNLINE_END_OF_STATIC = FID_RETURNLINE_ReturnReason


Public Const FID_CONSHEADER_ConsignmentNo = (CLASSID_CONSHEADER * &H10000) + 1
Public Const FID_CONSHEADER_PurchaseOrderNo = (CLASSID_CONSHEADER * &H10000) + 2
Public Const FID_CONSHEADER_PurchaseOrderRelNo = (CLASSID_CONSHEADER * &H10000) + 3
Public Const FID_CONSHEADER_RecordedInSystem = (CLASSID_CONSHEADER * &H10000) + 4
Public Const FID_CONSHEADER_ConsignmentDate = (CLASSID_CONSHEADER * &H10000) + 5
Public Const FID_CONSHEADER_SupplierNo = (CLASSID_CONSHEADER * &H10000) + 6
Public Const FID_CONSHEADER_IBTConsignment = (CLASSID_CONSHEADER * &H10000) + 7
Public Const FID_CONSHEADER_IBTStoreNo = (CLASSID_CONSHEADER * &H10000) + 8
Public Const FID_CONSHEADER_DeliveryNoteNo = (CLASSID_CONSHEADER * &H10000) + 9
Public Const FID_CONSHEADER_PrintInitials1 = (CLASSID_CONSHEADER * &H10000) + 10
Public Const FID_CONSHEADER_PrintTime1 = (CLASSID_CONSHEADER * &H10000) + 11
Public Const FID_CONSHEADER_PrintInitials2 = (CLASSID_CONSHEADER * &H10000) + 12
Public Const FID_CONSHEADER_PrintTime2 = (CLASSID_CONSHEADER * &H10000) + 13
Public Const FID_CONSHEADER_PrintInitials3 = (CLASSID_CONSHEADER * &H10000) + 14
Public Const FID_CONSHEADER_PrintTime3 = (CLASSID_CONSHEADER * &H10000) + 15
Public Const FID_CONSHEADER_PrintInitials4 = (CLASSID_CONSHEADER * &H10000) + 16
Public Const FID_CONSHEADER_PrintTime4 = (CLASSID_CONSHEADER * &H10000) + 17
Public Const FID_CONSHEADER_PrintInitials5 = (CLASSID_CONSHEADER * &H10000) + 18
Public Const FID_CONSHEADER_PrintTime5 = (CLASSID_CONSHEADER * &H10000) + 19
Public Const FID_CONSHEADER_PrintInitials6 = (CLASSID_CONSHEADER * &H10000) + 20
Public Const FID_CONSHEADER_PrintTime6 = (CLASSID_CONSHEADER * &H10000) + 21
Public Const FID_CONSHEADER_PrintedCount = (CLASSID_CONSHEADER * &H10000) + 22
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CONSHEADER_END_OF_STATIC = FID_CONSHEADER_PrintedCount

'Public Const FID_TRADER_TraderCardNo = (CLASSID_TRADER * &H10000) + 1
'Public Const FID_TRADER_TraderName = (CLASSID_TRADER * &H10000) + 2
'Public Const FID_TRADER_AddressLine1 = (CLASSID_TRADER * &H10000) + 3
'Public Const FID_TRADER_AddressLine2 = (CLASSID_TRADER * &H10000) + 4
'Public Const FID_TRADER_AddressLine3 = (CLASSID_TRADER * &H10000) + 5
'Public Const FID_TRADER_PostCode = (CLASSID_TRADER * &H10000) + 6
'Public Const FID_TRADER_TelephoneNo = (CLASSID_TRADER * &H10000) + 7
'Public Const FID_TRADER_FaxNo = (CLASSID_TRADER * &H10000) + 8
'Public Const FID_TRADER_DateOpened = (CLASSID_TRADER * &H10000) + 9
'Public Const FID_TRADER_TradeDiscount = (CLASSID_TRADER * &H10000) + 10
'Public Const FID_TRADER_TurnoverToDate = (CLASSID_TRADER * &H10000) + 11
'Public Const FID_TRADER_LastTransactionDate = (CLASSID_TRADER * &H10000) + 12
'Public Const FID_TRADER_Deleted = (CLASSID_TRADER * &H10000) + 13
'Public Const FID_TRADER_TraderType = (CLASSID_TRADER * &H10000) + 14
'Public Const FID_TRADER_AccountType = (CLASSID_TRADER * &H10000) + 15
'Public Const FID_TRADER_CreditLimit = (CLASSID_TRADER * &H10000) + 16
'Public Const FID_TRADER_AccountBalance = (CLASSID_TRADER * &H10000) + 17
'Public Const FID_TRADER_OnHold = (CLASSID_TRADER * &H10000) + 18
'Public Const FID_TRADER_LastPaymentDate = (CLASSID_TRADER * &H10000) + 19
'Public Const FID_TRADER_OriginatingStoreNo = (CLASSID_TRADER * &H10000) + 20
'Public Const FID_TRADER_END_OF_STATIC = FID_TRADER_OriginatingStoreNo


Public Const FID_DEPOSIT_DepositNo = (CLASSID_DEPOSIT * &H10000) + 1
Public Const FID_DEPOSIT_CustomerName = (CLASSID_DEPOSIT * &H10000) + 2
Public Const FID_DEPOSIT_DateCreated = (CLASSID_DEPOSIT * &H10000) + 3
Public Const FID_DEPOSIT_DepositAmount = (CLASSID_DEPOSIT * &H10000) + 4
Public Const FID_DEPOSIT_RefundAllowed = (CLASSID_DEPOSIT * &H10000) + 5
Public Const FID_DEPOSIT_DepositCode = (CLASSID_DEPOSIT * &H10000) + 6
Public Const FID_DEPOSIT_Refunded = (CLASSID_DEPOSIT * &H10000) + 7
Public Const FID_DEPOSIT_TillID = (CLASSID_DEPOSIT * &H10000) + 8
Public Const FID_DEPOSIT_TillTransactionNo = (CLASSID_DEPOSIT * &H10000) + 9
Public Const FID_DEPOSIT_CashierNo = (CLASSID_DEPOSIT * &H10000) + 10
Public Const FID_DEPOSIT_OrderNo = (CLASSID_DEPOSIT * &H10000) + 11
Public Const FID_DEPOSIT_DepositUsedAmount = (CLASSID_DEPOSIT * &H10000) + 12
Public Const FID_DEPOSIT_TransactionMoveType = (CLASSID_DEPOSIT * &H10000) + 13
Public Const FID_DEPOSIT_TransactionMoveCode = (CLASSID_DEPOSIT * &H10000) + 14
Public Const FID_DEPOSIT_TraderNo = (CLASSID_DEPOSIT * &H10000) + 15
Public Const FID_DEPOSIT_CommNo = (CLASSID_DEPOSIT * &H10000) + 16
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_DEPOSIT_END_OF_STATIC = FID_DEPOSIT_CommNo


Public Const FID_ISTOUTHEADER_DocNo = (CLASSID_ISTOUTHEADER * &H10000) + 1
Public Const FID_ISTOUTHEADER_DocType = (CLASSID_ISTOUTHEADER * &H10000) + 2
Public Const FID_ISTOUTHEADER_Value = (CLASSID_ISTOUTHEADER * &H10000) + 3
Public Const FID_ISTOUTHEADER_Cost = (CLASSID_ISTOUTHEADER * &H10000) + 4
Public Const FID_ISTOUTHEADER_DelNoteDate = (CLASSID_ISTOUTHEADER * &H10000) + 5
Public Const FID_ISTOUTHEADER_Comm = (CLASSID_ISTOUTHEADER * &H10000) + 6
Public Const FID_ISTOUTHEADER_OrigDocKey = (CLASSID_ISTOUTHEADER * &H10000) + 7
Public Const FID_ISTOUTHEADER_EnteredBy = (CLASSID_ISTOUTHEADER * &H10000) + 8
Public Const FID_ISTOUTHEADER_Comment = (CLASSID_ISTOUTHEADER * &H10000) + 9
Public Const FID_ISTOUTHEADER_ConsignmentRef = (CLASSID_ISTOUTHEADER * &H10000) + 10
Public Const FID_ISTOUTHEADER_Source = (CLASSID_ISTOUTHEADER * &H10000) + 11
Public Const FID_ISTOUTHEADER_StoreNumber = (CLASSID_ISTOUTHEADER * &H10000) + 12
Public Const FID_ISTOUTHEADER_IBT = (CLASSID_ISTOUTHEADER * &H10000) + 13
Public Const FID_ISTOUTHEADER_Printed = (CLASSID_ISTOUTHEADER * &H10000) + 14
Public Const FID_ISTOUTHEADER_ConsignmentKey = (CLASSID_ISTOUTHEADER * &H10000) + 15
Public Const FID_ISTOUTHEADER_DespatchMethod = (CLASSID_ISTOUTHEADER * &H10000) + 16
Public Const FID_ISTOUTHEADER_InvCrnMatched = (CLASSID_ISTOUTHEADER * &H10000) + 17
Public Const FID_ISTOUTHEADER_InvCrnDate = (CLASSID_ISTOUTHEADER * &H10000) + 18
Public Const FID_ISTOUTHEADER_InvCrnNumber = (CLASSID_ISTOUTHEADER * &H10000) + 19
Public Const FID_ISTOUTHEADER_VarianceValue = (CLASSID_ISTOUTHEADER * &H10000) + 20
Public Const FID_ISTOUTHEADER_InvoiceValue = (CLASSID_ISTOUTHEADER * &H10000) + 21
Public Const FID_ISTOUTHEADER_InvoiceVAT = (CLASSID_ISTOUTHEADER * &H10000) + 22
Public Const FID_ISTOUTHEADER_MatchingComment = (CLASSID_ISTOUTHEADER * &H10000) + 23
Public Const FID_ISTOUTHEADER_MatchingDate = (CLASSID_ISTOUTHEADER * &H10000) + 24
Public Const FID_ISTOUTHEADER_ReceiptDate = (CLASSID_ISTOUTHEADER * &H10000) + 25
Public Const FID_ISTOUTHEADER_CarraigeValue = (CLASSID_ISTOUTHEADER * &H10000) + 26
Public Const FID_ISTOUTHEADER_Commit = (CLASSID_ISTOUTHEADER * &H10000) + 27
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ISTOUTHEADER_END_OF_STATIC = FID_ISTOUTHEADER_Commit


Public Const FID_ISTLINE_DRLKey = (CLASSID_ISTLINE * &H10000) + 1
Public Const FID_ISTLINE_LineNo = (CLASSID_ISTLINE * &H10000) + 2
Public Const FID_ISTLINE_PartCode = (CLASSID_ISTLINE * &H10000) + 3
Public Const FID_ISTLINE_PartCodeAlphaCode = (CLASSID_ISTLINE * &H10000) + 4
Public Const FID_ISTLINE_OrderQty = (CLASSID_ISTLINE * &H10000) + 5
Public Const FID_ISTLINE_ReceivedQty = (CLASSID_ISTLINE * &H10000) + 6
Public Const FID_ISTLINE_OrderPrice = (CLASSID_ISTLINE * &H10000) + 7
Public Const FID_ISTLINE_BlanketDiscDesc = (CLASSID_ISTLINE * &H10000) + 8
Public Const FID_ISTLINE_BlanketPartCode = (CLASSID_ISTLINE * &H10000) + 9
Public Const FID_ISTLINE_CurrentPrice = (CLASSID_ISTLINE * &H10000) + 10
Public Const FID_ISTLINE_CurrentCost = (CLASSID_ISTLINE * &H10000) + 11
Public Const FID_ISTLINE_ISTQuantity = (CLASSID_ISTLINE * &H10000) + 12
Public Const FID_ISTLINE_ReturnQty = (CLASSID_ISTLINE * &H10000) + 13
Public Const FID_ISTLINE_ToFollowQty = (CLASSID_ISTLINE * &H10000) + 14
Public Const FID_ISTLINE_ReturnReasonCode = (CLASSID_ISTLINE * &H10000) + 15
Public Const FID_ISTLINE_POLineNumber = (CLASSID_ISTLINE * &H10000) + 16
Public Const FID_ISTLINE_Checked = (CLASSID_ISTLINE * &H10000) + 17
Public Const FID_ISTLINE_SinglesSellingUnit = (CLASSID_ISTLINE * &H10000) + 18
Public Const FID_ISTLINE_Narrative = (CLASSID_ISTLINE * &H10000) + 19
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ISTLINE_END_OF_STATIC = FID_ISTLINE_Narrative


Public Const FID_STOCKWOF_BatchNo = (CLASSID_STOCKWOF * &H10000) + 1
Public Const FID_STOCKWOF_LineNo = (CLASSID_STOCKWOF * &H10000) + 2
Public Const FID_STOCKWOF_AdjustmentCode = (CLASSID_STOCKWOF * &H10000) + 3
Public Const FID_STOCKWOF_PartCode = (CLASSID_STOCKWOF * &H10000) + 4
Public Const FID_STOCKWOF_DateCreated = (CLASSID_STOCKWOF * &H10000) + 5
Public Const FID_STOCKWOF_AuthorisedBy = (CLASSID_STOCKWOF * &H10000) + 6
Public Const FID_STOCKWOF_AuthorisedDate = (CLASSID_STOCKWOF * &H10000) + 7
Public Const FID_STOCKWOF_Quantity = (CLASSID_STOCKWOF * &H10000) + 8
Public Const FID_STOCKWOF_Price = (CLASSID_STOCKWOF * &H10000) + 9
Public Const FID_STOCKWOF_Cost = (CLASSID_STOCKWOF * &H10000) + 10
Public Const FID_STOCKWOF_AdjustedBy = (CLASSID_STOCKWOF * &H10000) + 11
Public Const FID_STOCKWOF_Comment = (CLASSID_STOCKWOF * &H10000) + 12
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKWOF_END_OF_STATIC = FID_STOCKWOF_Comment


Public Const FID_STOCKCOUNTHEADER_LocationNumber = (CLASSID_STOCKCOUNTHEADER * &H10000) + 1
Public Const FID_STOCKCOUNTHEADER_ItemCount = (CLASSID_STOCKCOUNTHEADER * &H10000) + 2
Public Const FID_STOCKCOUNTHEADER_AppliedCount = (CLASSID_STOCKCOUNTHEADER * &H10000) + 3
Public Const FID_STOCKCOUNTHEADER_DateFrozen = (CLASSID_STOCKCOUNTHEADER * &H10000) + 4
Public Const FID_STOCKCOUNTHEADER_TimeFrozen = (CLASSID_STOCKCOUNTHEADER * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKCOUNTHEADER_END_OF_STATIC = FID_STOCKCOUNTHEADER_TimeFrozen


Public Const FID_STOCKCOUNTLINE_LocationNumber = (CLASSID_STOCKCOUNTLINE * &H10000) + 1
Public Const FID_STOCKCOUNTLINE_SequenceNumber = (CLASSID_STOCKCOUNTLINE * &H10000) + 2
Public Const FID_STOCKCOUNTLINE_PartCode = (CLASSID_STOCKCOUNTLINE * &H10000) + 3
Public Const FID_STOCKCOUNTLINE_DepartmentNumber = (CLASSID_STOCKCOUNTLINE * &H10000) + 4
Public Const FID_STOCKCOUNTLINE_QuantityCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 5
Public Const FID_STOCKCOUNTLINE_OldDate = (CLASSID_STOCKCOUNTLINE * &H10000) + 6
Public Const FID_STOCKCOUNTLINE_TimeCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 7
Public Const FID_STOCKCOUNTLINE_QuantitySold = (CLASSID_STOCKCOUNTLINE * &H10000) + 8
Public Const FID_STOCKCOUNTLINE_DisplayQuantityCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 9
Public Const FID_STOCKCOUNTLINE_DateCounted = (CLASSID_STOCKCOUNTLINE * &H10000) + 10
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKCOUNTLINE_END_OF_STATIC = FID_STOCKCOUNTLINE_DateCounted


Public Const FID_STOCKADJUST_AdjustmentDate = (CLASSID_STOCKADJUST * &H10000) + 1
Public Const FID_STOCKADJUST_AdjustmentCode = (CLASSID_STOCKADJUST * &H10000) + 2
Public Const FID_STOCKADJUST_PartCode = (CLASSID_STOCKADJUST * &H10000) + 3
Public Const FID_STOCKADJUST_CommNo = (CLASSID_STOCKADJUST * &H10000) + 4
Public Const FID_STOCKADJUST_AdjustmentBy = (CLASSID_STOCKADJUST * &H10000) + 5
Public Const FID_STOCKADJUST_DepartmentNo = (CLASSID_STOCKADJUST * &H10000) + 6
Public Const FID_STOCKADJUST_OpeningQuantity = (CLASSID_STOCKADJUST * &H10000) + 7
Public Const FID_STOCKADJUST_AdjustmentQuantity = (CLASSID_STOCKADJUST * &H10000) + 8
Public Const FID_STOCKADJUST_Price = (CLASSID_STOCKADJUST * &H10000) + 9
Public Const FID_STOCKADJUST_Cost = (CLASSID_STOCKADJUST * &H10000) + 10
Public Const FID_STOCKADJUST_AdustmentType = (CLASSID_STOCKADJUST * &H10000) + 11
Public Const FID_STOCKADJUST_TransferPartCode = (CLASSID_STOCKADJUST * &H10000) + 12
Public Const FID_STOCKADJUST_TransferKeyPrice = (CLASSID_STOCKADJUST * &H10000) + 13
Public Const FID_STOCKADJUST_TransferMarkdownValue = (CLASSID_STOCKADJUST * &H10000) + 14
Public Const FID_STOCKADJUST_Comment = (CLASSID_STOCKADJUST * &H10000) + 15
Public Const FID_STOCKADJUST_DRLNo = (CLASSID_STOCKADJUST * &H10000) + 16
Public Const FID_STOCKADJUST_SequenceNo = (CLASSID_STOCKADJUST * &H10000) + 17
Public Const FID_STOCKADJUST_QuantityPerUnit = (CLASSID_STOCKADJUST * &H10000) + 18
Public Const FID_STOCKADJUST_Information2 = (CLASSID_STOCKADJUST * &H10000) + 19
Public Const FID_STOCKADJUST_AuthorisedBy = (CLASSID_STOCKADJUST * &H10000) + 20
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKADJUST_END_OF_STATIC = FID_STOCKADJUST_AuthorisedBy


Public Const FID_STOCKADJPEND_PartCode = (CLASSID_STOCKADJPEND * &H10000) + 1
Public Const FID_STOCKADJPEND_TotalQuantity = (CLASSID_STOCKADJPEND * &H10000) + 2
Public Const FID_STOCKADJPEND_CountedQuantity = (CLASSID_STOCKADJPEND * &H10000) + 3
Public Const FID_STOCKADJPEND_OverStockQuantity = (CLASSID_STOCKADJPEND * &H10000) + 4
Public Const FID_STOCKADJPEND_Comment = (CLASSID_STOCKADJPEND * &H10000) + 5
Public Const FID_STOCKADJPEND_AppliedCode = (CLASSID_STOCKADJPEND * &H10000) + 6
Public Const FID_STOCKADJPEND_VarianceQuantity = (CLASSID_STOCKADJPEND * &H10000) + 7
Public Const FID_STOCKADJPEND_VarianceDescription = (CLASSID_STOCKADJPEND * &H10000) + 8
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKADJPEND_END_OF_STATIC = FID_STOCKADJPEND_VarianceDescription


Public Const FID_STOCKLOCATION_PartCode = (CLASSID_STOCKLOCATION * &H10000) + 1
Public Const FID_STOCKLOCATION_LocationNo = (CLASSID_STOCKLOCATION * &H10000) + 2
Public Const FID_STOCKLOCATION_LastStockCountQuantity = (CLASSID_STOCKLOCATION * &H10000) + 3
Public Const FID_STOCKLOCATION_LastUpdated = (CLASSID_STOCKLOCATION * &H10000) + 4
Public Const FID_STOCKLOCATION_ChangedBy = (CLASSID_STOCKLOCATION * &H10000) + 5
Public Const FID_STOCKLOCATION_RFStockCheck = (CLASSID_STOCKLOCATION * &H10000) + 6
Public Const FID_STOCKLOCATION_SequenceInLocation = (CLASSID_STOCKLOCATION * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKLOCATION_END_OF_STATIC = FID_STOCKLOCATION_SequenceInLocation


Public Const FID_STOCKLOCATION2_Location = (CLASSID_STOCKLOCATION2 * &H10000) + 1
Public Const FID_STOCKLOCATION2_PartCode = (CLASSID_STOCKLOCATION2 * &H10000) + 2
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKLOCATION2_END_OF_STATIC = FID_STOCKLOCATION2_PartCode


Public Const FID_LOCATIONTYPE_LocationType = (CLASSID_LOCATIONTYPE * &H10000) + 1
Public Const FID_LOCATIONTYPE_Description = (CLASSID_LOCATIONTYPE * &H10000) + 2
Public Const FID_LOCATIONTYPE_POSMaterialPrint = (CLASSID_LOCATIONTYPE * &H10000) + 3
Public Const FID_LOCATIONTYPE_NoLocationsLimit = (CLASSID_LOCATIONTYPE * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_LOCATIONTYPE_END_OF_STATIC = FID_LOCATIONTYPE_NoLocationsLimit


Public Const FID_RELATEDITEM_ItemNo = (CLASSID_RELATEDITEM * &H10000) + 1
Public Const FID_RELATEDITEM_BulkItemNo = (CLASSID_RELATEDITEM * &H10000) + 2
Public Const FID_RELATEDITEM_SinglesPerPack = (CLASSID_RELATEDITEM * &H10000) + 3
Public Const FID_RELATEDITEM_SinglesSoldNotUpdated = (CLASSID_RELATEDITEM * &H10000) + 4
Public Const FID_RELATEDITEM_Deleted = (CLASSID_RELATEDITEM * &H10000) + 5
Public Const FID_RELATEDITEM_SinglesSoldToDate = (CLASSID_RELATEDITEM * &H10000) + 6
Public Const FID_RELATEDITEM_SinglesSoldToYear = (CLASSID_RELATEDITEM * &H10000) + 7
Public Const FID_RELATEDITEM_SinglesSoldPriorYear = (CLASSID_RELATEDITEM * &H10000) + 8
Public Const FID_RELATEDITEM_MarkUpTransThisWeek = (CLASSID_RELATEDITEM * &H10000) + 9
Public Const FID_RELATEDITEM_MarkUpTransPriorWeek = (CLASSID_RELATEDITEM * &H10000) + 10
Public Const FID_RELATEDITEM_MarkUpThisWeek = (CLASSID_RELATEDITEM * &H10000) + 11
Public Const FID_RELATEDITEM_MarkUpPriorWeek = (CLASSID_RELATEDITEM * &H10000) + 12
Public Const FID_RELATEDITEM_MarkUpThisPeriod = (CLASSID_RELATEDITEM * &H10000) + 13
Public Const FID_RELATEDITEM_MarkUpPriorPeriod = (CLASSID_RELATEDITEM * &H10000) + 14
Public Const FID_RELATEDITEM_MarkUpThisYear = (CLASSID_RELATEDITEM * &H10000) + 15
Public Const FID_RELATEDITEM_MarkUpPriorYear = (CLASSID_RELATEDITEM * &H10000) + 16
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RELATEDITEM_END_OF_STATIC = FID_RELATEDITEM_MarkUpPriorYear


Public Const FID_PRICECHANGE_PartCode = (CLASSID_PRICECHANGE * &H10000) + 1
Public Const FID_PRICECHANGE_EffectiveDate = (CLASSID_PRICECHANGE * &H10000) + 2
Public Const FID_PRICECHANGE_SupplierNo = (CLASSID_PRICECHANGE * &H10000) + 3
Public Const FID_PRICECHANGE_Location = (CLASSID_PRICECHANGE * &H10000) + 4
Public Const FID_PRICECHANGE_SequenceNo = (CLASSID_PRICECHANGE * &H10000) + 5
Public Const FID_PRICECHANGE_NewPrice = (CLASSID_PRICECHANGE * &H10000) + 6
Public Const FID_PRICECHANGE_NewRetailPrice = (CLASSID_PRICECHANGE * &H10000) + 7
Public Const FID_PRICECHANGE_NewOtherPrice = (CLASSID_PRICECHANGE * &H10000) + 8
Public Const FID_PRICECHANGE_NewSpecialPrice = (CLASSID_PRICECHANGE * &H10000) + 9
Public Const FID_PRICECHANGE_ChangeStatus = (CLASSID_PRICECHANGE * &H10000) + 10
Public Const FID_PRICECHANGE_PriceBand = (CLASSID_PRICECHANGE * &H10000) + 11
Public Const FID_PRICECHANGE_LabelPrinted = (CLASSID_PRICECHANGE * &H10000) + 12
Public Const FID_PRICECHANGE_AutoApplyDate = (CLASSID_PRICECHANGE * &H10000) + 13
Public Const FID_PRICECHANGE_AutoAppliedDate = (CLASSID_PRICECHANGE * &H10000) + 14
Public Const FID_PRICECHANGE_MarkUpChange = (CLASSID_PRICECHANGE * &H10000) + 15
Public Const FID_PRICECHANGE_NewUnitOfMeasure = (CLASSID_PRICECHANGE * &H10000) + 16
Public Const FID_PRICECHANGE_NewQuantityPerUnit = (CLASSID_PRICECHANGE * &H10000) + 17
Public Const FID_PRICECHANGE_NewBuyingUnit = (CLASSID_PRICECHANGE * &H10000) + 18
Public Const FID_PRICECHANGE_NewSellingUnit = (CLASSID_PRICECHANGE * &H10000) + 19
Public Const FID_PRICECHANGE_Type1Print = (CLASSID_PRICECHANGE * &H10000) + 20
Public Const FID_PRICECHANGE_Type2Print = (CLASSID_PRICECHANGE * &H10000) + 21
Public Const FID_PRICECHANGE_Type3Print = (CLASSID_PRICECHANGE * &H10000) + 22
Public Const FID_PRICECHANGE_Type4Print = (CLASSID_PRICECHANGE * &H10000) + 23
Public Const FID_PRICECHANGE_Type5Print = (CLASSID_PRICECHANGE * &H10000) + 24
Public Const FID_PRICECHANGE_Type6Print = (CLASSID_PRICECHANGE * &H10000) + 25
Public Const FID_PRICECHANGE_CommNo = (CLASSID_PRICECHANGE * &H10000) + 26
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PRICECHANGE_END_OF_STATIC = FID_PRICECHANGE_CommNo


Public Const FID_EAN_EANNumber = (CLASSID_EAN * &H10000) + 1
Public Const FID_EAN_PartCode = (CLASSID_EAN * &H10000) + 2
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EAN_END_OF_STATIC = FID_EAN_PartCode


Public Const FID_POSACTION_PartCode = (CLASSID_POSACTION * &H10000) + 1
Public Const FID_POSACTION_Location = (CLASSID_POSACTION * &H10000) + 2
Public Const FID_POSACTION_SelMissing = (CLASSID_POSACTION * &H10000) + 3
Public Const FID_POSACTION_RetailPrice = (CLASSID_POSACTION * &H10000) + 4
Public Const FID_POSACTION_SpecialPrice = (CLASSID_POSACTION * &H10000) + 5
Public Const FID_POSACTION_EANWrong = (CLASSID_POSACTION * &H10000) + 6
Public Const FID_POSACTION_OverShelfEANWrong = (CLASSID_POSACTION * &H10000) + 7
Public Const FID_POSACTION_POSMissing = (CLASSID_POSACTION * &H10000) + 8
Public Const FID_POSACTION_POSInfoIncorrect = (CLASSID_POSACTION * &H10000) + 9
Public Const FID_POSACTION_ItemNeedsTagging = (CLASSID_POSACTION * &H10000) + 10
Public Const FID_POSACTION_WrongLocation = (CLASSID_POSACTION * &H10000) + 11
Public Const FID_POSACTION_OverStockWrongLocation = (CLASSID_POSACTION * &H10000) + 12
Public Const FID_POSACTION_ShelfStockCount = (CLASSID_POSACTION * &H10000) + 13
Public Const FID_POSACTION_OverStockCount = (CLASSID_POSACTION * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSACTION_END_OF_STATIC = FID_POSACTION_OverStockCount


Public Const FID_POSMESSAGE_MessageNo = (CLASSID_POSMESSAGE * &H10000) + 1
Public Const FID_POSMESSAGE_MessageText = (CLASSID_POSMESSAGE * &H10000) + 2
Public Const FID_POSMESSAGE_MessageType = (CLASSID_POSMESSAGE * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSMESSAGE_END_OF_STATIC = FID_POSMESSAGE_MessageType


Public Const FID_STOCKADJCODE_AdjustmentNo = (CLASSID_STOCKADJCODE * &H10000) + 1
Public Const FID_STOCKADJCODE_Description = (CLASSID_STOCKADJCODE * &H10000) + 2
Public Const FID_STOCKADJCODE_AdjustmentType = (CLASSID_STOCKADJCODE * &H10000) + 3
Public Const FID_STOCKADJCODE_Sign = (CLASSID_STOCKADJCODE * &H10000) + 4
Public Const FID_STOCKADJCODE_SecurityLevel = (CLASSID_STOCKADJCODE * &H10000) + 5
Public Const FID_STOCKADJCODE_IsMarkDown = (CLASSID_STOCKADJCODE * &H10000) + 6
Public Const FID_STOCKADJCODE_IsWriteOff = (CLASSID_STOCKADJCODE * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKADJCODE_END_OF_STATIC = FID_STOCKADJCODE_IsWriteOff


Public Const FID_UNITOFMEASURE_UnitOfMeasureCode = (CLASSID_UNITOFMEASURE * &H10000) + 1
Public Const FID_UNITOFMEASURE_Description = (CLASSID_UNITOFMEASURE * &H10000) + 2
Public Const FID_UNITOFMEASURE_Deleted = (CLASSID_UNITOFMEASURE * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_UNITOFMEASURE_END_OF_STATIC = FID_UNITOFMEASURE_Deleted


Public Const FID_PRODUCTGROUP_DepartmentNo = (CLASSID_PRODUCTGROUP * &H10000) + 1
Public Const FID_PRODUCTGROUP_GroupNo = (CLASSID_PRODUCTGROUP * &H10000) + 2
Public Const FID_PRODUCTGROUP_SalesValue = (CLASSID_PRODUCTGROUP * &H10000) + 3
Public Const FID_PRODUCTGROUP_SalesCost = (CLASSID_PRODUCTGROUP * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PRODUCTGROUP_END_OF_STATIC = FID_PRODUCTGROUP_SalesCost


Public Const FID_ITEMGROUP_PartCode = (CLASSID_ITEMGROUP * &H10000) + 1
Public Const FID_ITEMGROUP_DepartmentNo = (CLASSID_ITEMGROUP * &H10000) + 2
Public Const FID_ITEMGROUP_GroupNo = (CLASSID_ITEMGROUP * &H10000) + 3
Public Const FID_ITEMGROUP_WeekSaleQuantity = (CLASSID_ITEMGROUP * &H10000) + 4
Public Const FID_ITEMGROUP_WeekSaleValue = (CLASSID_ITEMGROUP * &H10000) + 5
Public Const FID_ITEMGROUP_WeekSaleCost = (CLASSID_ITEMGROUP * &H10000) + 6
Public Const FID_ITEMGROUP_WeekCummCost = (CLASSID_ITEMGROUP * &H10000) + 7
Public Const FID_ITEMGROUP_WeekDaysOpen = (CLASSID_ITEMGROUP * &H10000) + 8
Public Const FID_ITEMGROUP_WeekDaysStockToSell = (CLASSID_ITEMGROUP * &H10000) + 9
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ITEMGROUP_END_OF_STATIC = FID_ITEMGROUP_WeekDaysStockToSell


'Public Const FID_DEPTGROUP_DepartmentNo = (CLASSID_DEPTGROUP * &H10000) + 1
'Public Const FID_DEPTGROUP_GroupNo = (CLASSID_DEPTGROUP * &H10000) + 2
'Public Const FID_DEPTGROUP_Description = (CLASSID_DEPTGROUP * &H10000) + 3
'Public Const FID_DEPTGROUP_CycleDayNo = (CLASSID_DEPTGROUP * &H10000) + 4
'Public Const FID_DEPTGROUP_WeekSalesValue = (CLASSID_DEPTGROUP * &H10000) + 5
'Public Const FID_DEPTGROUP_WeekLookUps = (CLASSID_DEPTGROUP * &H10000) + 6
'Public Const FID_DEPTGROUP_WeekOutOfStock = (CLASSID_DEPTGROUP * &H10000) + 7
'Public Const FID_DEPTGROUP_END_OF_STATIC = FID_DEPTGROUP_WeekOutOfStock


Public Const FID_CASHBALANCEHDR_FileKey = (CLASSID_CASHBALANCEHDR * &H10000) + 1
Public Const FID_CASHBALANCEHDR_CurrentDate = (CLASSID_CASHBALANCEHDR * &H10000) + 2
Public Const FID_CASHBALANCEHDR_StoreBankTotal = (CLASSID_CASHBALANCEHDR * &H10000) + 3
Public Const FID_CASHBALANCEHDR_GiftVoucherStock = (CLASSID_CASHBALANCEHDR * &H10000) + 4
Public Const FID_CASHBALANCEHDR_CustomerDeposits = (CLASSID_CASHBALANCEHDR * &H10000) + 5
Public Const FID_CASHBALANCEHDR_StoreFloatAmount = (CLASSID_CASHBALANCEHDR * &H10000) + 6
Public Const FID_CASHBALANCEHDR_DepositInDeptNo = (CLASSID_CASHBALANCEHDR * &H10000) + 7
Public Const FID_CASHBALANCEHDR_DepositOutDeptNo = (CLASSID_CASHBALANCEHDR * &H10000) + 8
Public Const FID_CASHBALANCEHDR_NoSummaryKept = (CLASSID_CASHBALANCEHDR * &H10000) + 9
Public Const FID_CASHBALANCEHDR_NoSummaryNow = (CLASSID_CASHBALANCEHDR * &H10000) + 10
Public Const FID_CASHBALANCEHDR_SalesOfGiftVoucher = (CLASSID_CASHBALANCEHDR * &H10000) + 11
Public Const FID_CASHBALANCEHDR_TenderType = (CLASSID_CASHBALANCEHDR * &H10000) + 12
Public Const FID_CASHBALANCEHDR_NoReturns = (CLASSID_CASHBALANCEHDR * &H10000) + 13
Public Const FID_CASHBALANCEHDR_DepositInBank = (CLASSID_CASHBALANCEHDR * &H10000) + 14
Public Const FID_CASHBALANCEHDR_SequenceTendered = (CLASSID_CASHBALANCEHDR * &H10000) + 15
Public Const FID_CASHBALANCEHDR_RequestSlipForTender = (CLASSID_CASHBALANCEHDR * &H10000) + 16
Public Const FID_CASHBALANCEHDR_MaximumPerSlip = (CLASSID_CASHBALANCEHDR * &H10000) + 17
Public Const FID_CASHBALANCEHDR_NoSummaryInFile = (CLASSID_CASHBALANCEHDR * &H10000) + 18
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CASHBALANCEHDR_END_OF_STATIC = FID_CASHBALANCEHDR_NoSummaryInFile


Public Const FID_SUPPLIERS_SupplierNo = (CLASSID_SUPPLIERS * &H10000) + 1
Public Const FID_SUPPLIERS_Name = (CLASSID_SUPPLIERS * &H10000) + 2
Public Const FID_SUPPLIERS_Deleted = (CLASSID_SUPPLIERS * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SUPPLIERS_END_OF_STATIC = FID_SUPPLIERS_Deleted


Public Const FID_INVENTORYS_PartCode = (CLASSID_INVENTORYS * &H10000) + 1
Public Const FID_INVENTORYS_Description = (CLASSID_INVENTORYS * &H10000) + 2
Public Const FID_INVENTORYS_Department = (CLASSID_INVENTORYS * &H10000) + 3
Public Const FID_INVENTORYS_SupplierNo = (CLASSID_INVENTORYS * &H10000) + 4
Public Const FID_INVENTORYS_Flag1 = (CLASSID_INVENTORYS * &H10000) + 5
Public Const FID_INVENTORYS_Flag2 = (CLASSID_INVENTORYS * &H10000) + 6
Public Const FID_INVENTORYS_Flag3 = (CLASSID_INVENTORYS * &H10000) + 7
Public Const FID_INVENTORYS_Flag4 = (CLASSID_INVENTORYS * &H10000) + 8
Public Const FID_INVENTORYS_Flag5 = (CLASSID_INVENTORYS * &H10000) + 9
Public Const FID_INVENTORYS_Warehouse = (CLASSID_INVENTORYS * &H10000) + 10
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_INVENTORYS_END_OF_STATIC = FID_INVENTORYS_Warehouse


Public Const FID_DESPATCHMETHOD_ID = (CLASSID_DESPATCHMETHOD * &H10000) + 1
Public Const FID_DESPATCHMETHOD_Description = (CLASSID_DESPATCHMETHOD * &H10000) + 2
Public Const FID_DESPATCHMETHOD_Active = (CLASSID_DESPATCHMETHOD * &H10000) + 3
Public Const FID_DESPATCHMETHOD_ReferenceRequired = (CLASSID_DESPATCHMETHOD * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_DESPATCHMETHOD_END_OF_STATIC = FID_DESPATCHMETHOD_ReferenceRequired


Public Const FID_PURHDRNARRATIVE_StoreNumber = (CLASSID_PURHDRNARRATIVE * &H10000) + 1
Public Const FID_PURHDRNARRATIVE_OrderID = (CLASSID_PURHDRNARRATIVE * &H10000) + 2
Public Const FID_PURHDRNARRATIVE_TextLine1 = (CLASSID_PURHDRNARRATIVE * &H10000) + 3
Public Const FID_PURHDRNARRATIVE_TextLine2 = (CLASSID_PURHDRNARRATIVE * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PURHDRNARRATIVE_END_OF_STATIC = FID_PURHDRNARRATIVE_TextLine2


Public Const FID_TYPESOFSALECONTROL_Code = (CLASSID_TYPESOFSALECONTROL * &H10000) + 1
Public Const FID_TYPESOFSALECONTROL_Description = (CLASSID_TYPESOFSALECONTROL * &H10000) + 2
Public Const FID_TYPESOFSALECONTROL_ValidForUse = (CLASSID_TYPESOFSALECONTROL * &H10000) + 3
Public Const FID_TYPESOFSALECONTROL_UpdateZReads = (CLASSID_TYPESOFSALECONTROL * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TYPESOFSALECONTROL_END_OF_STATIC = FID_TYPESOFSALECONTROL_UpdateZReads


Public Const FID_TYPESOFSALEOPTIONS_DisplaySeq = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 1
Public Const FID_TYPESOFSALEOPTIONS_Code = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 2
Public Const FID_TYPESOFSALEOPTIONS_Description = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 3
Public Const FID_TYPESOFSALEOPTIONS_RequestDocRef = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 4
Public Const FID_TYPESOFSALEOPTIONS_SpecialPrint = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 5
Public Const FID_TYPESOFSALEOPTIONS_OpenOnZeroTran = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 6
Public Const FID_TYPESOFSALEOPTIONS_AllowSpecialAccTender = (CLASSID_TYPESOFSALEOPTIONS * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TYPESOFSALEOPTIONS_END_OF_STATIC = FID_TYPESOFSALEOPTIONS_AllowSpecialAccTender


Public Const FID_TYPESOFSALETENDERS_TOSDisplaySeq = (CLASSID_TYPESOFSALETENDERS * &H10000) + 1
Public Const FID_TYPESOFSALETENDERS_TenderNo = (CLASSID_TYPESOFSALETENDERS * &H10000) + 2
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TYPESOFSALETENDERS_END_OF_STATIC = FID_TYPESOFSALETENDERS_TenderNo


Public Const FID_TENDERCONTROL_TendType = (CLASSID_TENDERCONTROL * &H10000) + 1
Public Const FID_TENDERCONTROL_Description = (CLASSID_TENDERCONTROL * &H10000) + 2
Public Const FID_TENDERCONTROL_ValidForUse = (CLASSID_TENDERCONTROL * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TENDERCONTROL_END_OF_STATIC = FID_TENDERCONTROL_ValidForUse


Public Const FID_TENDEROPTIONS_TenderNo = (CLASSID_TENDEROPTIONS * &H10000) + 1
Public Const FID_TENDEROPTIONS_TendType = (CLASSID_TENDEROPTIONS * &H10000) + 2
Public Const FID_TENDEROPTIONS_Description = (CLASSID_TENDEROPTIONS * &H10000) + 3
Public Const FID_TENDEROPTIONS_OverTenderAllowed = (CLASSID_TENDEROPTIONS * &H10000) + 4
Public Const FID_TENDEROPTIONS_CaptureCreditCard = (CLASSID_TENDEROPTIONS * &H10000) + 5
Public Const FID_TENDEROPTIONS_UseEFTPOS = (CLASSID_TENDEROPTIONS * &H10000) + 6
Public Const FID_TENDEROPTIONS_OpenCashDrawer = (CLASSID_TENDEROPTIONS * &H10000) + 7
Public Const FID_TENDEROPTIONS_DefaultRemainingAmount = (CLASSID_TENDEROPTIONS * &H10000) + 8
Public Const FID_TENDEROPTIONS_PrintChequeFront = (CLASSID_TENDEROPTIONS * &H10000) + 9
Public Const FID_TENDEROPTIONS_PrintChequeBack = (CLASSID_TENDEROPTIONS * &H10000) + 10
Public Const FID_TENDEROPTIONS_MinimumAccepted = (CLASSID_TENDEROPTIONS * &H10000) + 11
Public Const FID_TENDEROPTIONS_MaximumAccepted = (CLASSID_TENDEROPTIONS * &H10000) + 12
Public Const FID_TENDEROPTIONS_CreditCardFloorLimit = (CLASSID_TENDEROPTIONS * &H10000) + 13
Public Const FID_TENDEROPTIONS_DisplaySequence = (CLASSID_TENDEROPTIONS * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TENDEROPTIONS_END_OF_STATIC = FID_TENDEROPTIONS_DisplaySequence


Public Const FID_LINECOMMENTS_ID = (CLASSID_LINECOMMENTS * &H10000) + 1
Public Const FID_LINECOMMENTS_Description = (CLASSID_LINECOMMENTS * &H10000) + 2
Public Const FID_LINECOMMENTS_Active = (CLASSID_LINECOMMENTS * &H10000) + 3
Public Const FID_LINECOMMENTS_NoteType = (CLASSID_LINECOMMENTS * &H10000) + 4
Public Const FID_LINECOMMENTS_LineNote = (CLASSID_LINECOMMENTS * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_LINECOMMENTS_END_OF_STATIC = FID_LINECOMMENTS_LineNote


Public Const FID_DEPTANALYSIS_DeptCode = (CLASSID_DEPTANALYSIS * &H10000) + 1
Public Const FID_DEPTANALYSIS_TotalSales = (CLASSID_DEPTANALYSIS * &H10000) + 2
Public Const FID_DEPTANALYSIS_ValueSales = (CLASSID_DEPTANALYSIS * &H10000) + 3
Public Const FID_DEPTANALYSIS_NumberItems = (CLASSID_DEPTANALYSIS * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_DEPTANALYSIS_END_OF_STATIC = FID_DEPTANALYSIS_NumberItems


Public Const FID_VATRATES_ID = (CLASSID_VATRATES * &H10000) + 1
Public Const FID_VATRATES_VATRateCount = (CLASSID_VATRATES * &H10000) + 2
Public Const FID_VATRATES_VATRate = (CLASSID_VATRATES * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_VATRATES_END_OF_STATIC = FID_VATRATES_VATRate


Public Const FID_SYSTEMDATES_ID = (CLASSID_SYSTEMDATES * &H10000) + 1
Public Const FID_SYSTEMDATES_NoDaysOpen = (CLASSID_SYSTEMDATES * &H10000) + 2
Public Const FID_SYSTEMDATES_WeekEndingDay = (CLASSID_SYSTEMDATES * &H10000) + 3
Public Const FID_SYSTEMDATES_TodaysDate = (CLASSID_SYSTEMDATES * &H10000) + 4
Public Const FID_SYSTEMDATES_TodaysDayNo = (CLASSID_SYSTEMDATES * &H10000) + 5
Public Const FID_SYSTEMDATES_NextOpenDate = (CLASSID_SYSTEMDATES * &H10000) + 6
Public Const FID_SYSTEMDATES_NextOpenDayNo = (CLASSID_SYSTEMDATES * &H10000) + 7
Public Const FID_SYSTEMDATES_NoWeeksInCycle = (CLASSID_SYSTEMDATES * &H10000) + 8
Public Const FID_SYSTEMDATES_CurrentWeekNo = (CLASSID_SYSTEMDATES * &H10000) + 9
Public Const FID_SYSTEMDATES_CurrentPeriodEndDate = (CLASSID_SYSTEMDATES * &H10000) + 10
Public Const FID_SYSTEMDATES_PriorPeriodEndDate = (CLASSID_SYSTEMDATES * &H10000) + 11
Public Const FID_SYSTEMDATES_CurrentIsYearEnd = (CLASSID_SYSTEMDATES * &H10000) + 12
Public Const FID_SYSTEMDATES_PeriodEndNotProcessed = (CLASSID_SYSTEMDATES * &H10000) + 13
Public Const FID_SYSTEMDATES_WeekEndNotProcessed = (CLASSID_SYSTEMDATES * &H10000) + 14
Public Const FID_SYSTEMDATES_NightTaskSetNo = (CLASSID_SYSTEMDATES * &H10000) + 15
Public Const FID_SYSTEMDATES_CurrentTaskNo = (CLASSID_SYSTEMDATES * &H10000) + 16
Public Const FID_SYSTEMDATES_InRetryMode = (CLASSID_SYSTEMDATES * &H10000) + 17
Public Const FID_SYSTEMDATES_StoreLiveDate = (CLASSID_SYSTEMDATES * &H10000) + 18
Public Const FID_SYSTEMDATES_UsePeriodSetNo = (CLASSID_SYSTEMDATES * &H10000) + 19
Public Const FID_SYSTEMDATES_Period1EndDates = (CLASSID_SYSTEMDATES * &H10000) + 20
Public Const FID_SYSTEMDATES_Period1Ended = (CLASSID_SYSTEMDATES * &H10000) + 21
Public Const FID_SYSTEMDATES_Period1EndProc = (CLASSID_SYSTEMDATES * &H10000) + 22
Public Const FID_SYSTEMDATES_Period2EndDates = (CLASSID_SYSTEMDATES * &H10000) + 23
Public Const FID_SYSTEMDATES_Period2Ended = (CLASSID_SYSTEMDATES * &H10000) + 24
Public Const FID_SYSTEMDATES_Period2EndProc = (CLASSID_SYSTEMDATES * &H10000) + 25
Public Const FID_SYSTEMDATES_WeekEndDates = (CLASSID_SYSTEMDATES * &H10000) + 26
Public Const FID_SYSTEMDATES_SOQCycleLength = (CLASSID_SYSTEMDATES * &H10000) + 27
Public Const FID_SYSTEMDATES_CurrentSOQWeekNo = (CLASSID_SYSTEMDATES * &H10000) + 28
Public Const FID_SYSTEMDATES_LastStockDate = (CLASSID_SYSTEMDATES * &H10000) + 29
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SYSTEMDATES_END_OF_STATIC = FID_SYSTEMDATES_LastStockDate


Public Const FID_RETURNCODE_CodeType = (CLASSID_RETURNCODE * &H10000) + 1
Public Const FID_RETURNCODE_Code = (CLASSID_RETURNCODE * &H10000) + 2
Public Const FID_RETURNCODE_Description = (CLASSID_RETURNCODE * &H10000) + 3
Public Const FID_RETURNCODE_NormalSupplierReturn = (CLASSID_RETURNCODE * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETURNCODE_END_OF_STATIC = FID_RETURNCODE_NormalSupplierReturn


Public Const FID_PRICEOVERRIDE_CodeType = (CLASSID_PRICEOVERRIDE * &H10000) + 1
Public Const FID_PRICEOVERRIDE_Code = (CLASSID_PRICEOVERRIDE * &H10000) + 2
Public Const FID_PRICEOVERRIDE_Description = (CLASSID_PRICEOVERRIDE * &H10000) + 3
Public Const FID_PRICEOVERRIDE_IsRefundPriceOverride = (CLASSID_PRICEOVERRIDE * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PRICEOVERRIDE_END_OF_STATIC = FID_PRICEOVERRIDE_IsRefundPriceOverride


Public Const FID_STOCKLOG_Key = (CLASSID_STOCKLOG * &H10000) + 1
Public Const FID_STOCKLOG_LogDate = (CLASSID_STOCKLOG * &H10000) + 2
Public Const FID_STOCKLOG_LogTime = (CLASSID_STOCKLOG * &H10000) + 3
Public Const FID_STOCKLOG_PartCode = (CLASSID_STOCKLOG * &H10000) + 4
Public Const FID_STOCKLOG_MovementType = (CLASSID_STOCKLOG * &H10000) + 5
Public Const FID_STOCKLOG_TransactionKey = (CLASSID_STOCKLOG * &H10000) + 6
Public Const FID_STOCKLOG_CommedToHO = (CLASSID_STOCKLOG * &H10000) + 7
Public Const FID_STOCKLOG_EmployeeID = (CLASSID_STOCKLOG * &H10000) + 8
Public Const FID_STOCKLOG_StartStockQty = (CLASSID_STOCKLOG * &H10000) + 9
Public Const FID_STOCKLOG_StartOpenReturnQty = (CLASSID_STOCKLOG * &H10000) + 10
Public Const FID_STOCKLOG_StartPrice = (CLASSID_STOCKLOG * &H10000) + 11
Public Const FID_STOCKLOG_StartMarkDownSOH = (CLASSID_STOCKLOG * &H10000) + 12
Public Const FID_STOCKLOG_StartWriteOffSOH = (CLASSID_STOCKLOG * &H10000) + 13
Public Const FID_STOCKLOG_EndStockQty = (CLASSID_STOCKLOG * &H10000) + 14
Public Const FID_STOCKLOG_EndOpenReturnQty = (CLASSID_STOCKLOG * &H10000) + 15
Public Const FID_STOCKLOG_EndPrice = (CLASSID_STOCKLOG * &H10000) + 16
Public Const FID_STOCKLOG_EndMarkDownSOH = (CLASSID_STOCKLOG * &H10000) + 17
Public Const FID_STOCKLOG_EndWriteOffSOH = (CLASSID_STOCKLOG * &H10000) + 18
Public Const FID_STOCKLOG_DayNumber = (CLASSID_STOCKLOG * &H10000) + 19
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKLOG_END_OF_STATIC = FID_STOCKLOG_DayNumber


Public Const FID_ISTINHEADER_DocNo = (CLASSID_ISTINHEADER * &H10000) + 1
Public Const FID_ISTINHEADER_DocType = (CLASSID_ISTINHEADER * &H10000) + 2
Public Const FID_ISTINHEADER_Value = (CLASSID_ISTINHEADER * &H10000) + 3
Public Const FID_ISTINHEADER_Cost = (CLASSID_ISTINHEADER * &H10000) + 4
Public Const FID_ISTINHEADER_DelNoteDate = (CLASSID_ISTINHEADER * &H10000) + 5
Public Const FID_ISTINHEADER_Comm = (CLASSID_ISTINHEADER * &H10000) + 6
Public Const FID_ISTINHEADER_OrigDocKey = (CLASSID_ISTINHEADER * &H10000) + 7
Public Const FID_ISTINHEADER_EnteredBy = (CLASSID_ISTINHEADER * &H10000) + 8
Public Const FID_ISTINHEADER_Comment = (CLASSID_ISTINHEADER * &H10000) + 9
Public Const FID_ISTINHEADER_ConsignmentRef = (CLASSID_ISTINHEADER * &H10000) + 10
Public Const FID_ISTINHEADER_Source = (CLASSID_ISTINHEADER * &H10000) + 11
Public Const FID_ISTINHEADER_StoreNumber = (CLASSID_ISTINHEADER * &H10000) + 12
Public Const FID_ISTINHEADER_IBT = (CLASSID_ISTINHEADER * &H10000) + 13
Public Const FID_ISTINHEADER_Printed = (CLASSID_ISTINHEADER * &H10000) + 14
Public Const FID_ISTINHEADER_ConsignmentKey = (CLASSID_ISTINHEADER * &H10000) + 15
Public Const FID_ISTINHEADER_InvCrnMatched = (CLASSID_ISTINHEADER * &H10000) + 16
Public Const FID_ISTINHEADER_InvCrnDate = (CLASSID_ISTINHEADER * &H10000) + 17
Public Const FID_ISTINHEADER_InvCrnNumber = (CLASSID_ISTINHEADER * &H10000) + 18
Public Const FID_ISTINHEADER_VarianceValue = (CLASSID_ISTINHEADER * &H10000) + 19
Public Const FID_ISTINHEADER_InvoiceValue = (CLASSID_ISTINHEADER * &H10000) + 20
Public Const FID_ISTINHEADER_InvoiceVAT = (CLASSID_ISTINHEADER * &H10000) + 21
Public Const FID_ISTINHEADER_MatchingComment = (CLASSID_ISTINHEADER * &H10000) + 22
Public Const FID_ISTINHEADER_MatchingDate = (CLASSID_ISTINHEADER * &H10000) + 23
Public Const FID_ISTINHEADER_ReceiptDate = (CLASSID_ISTINHEADER * &H10000) + 24
Public Const FID_ISTINHEADER_CarraigeValue = (CLASSID_ISTINHEADER * &H10000) + 25
Public Const FID_ISTINHEADER_Commit = (CLASSID_ISTINHEADER * &H10000) + 26
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ISTINHEADER_END_OF_STATIC = FID_ISTINHEADER_Commit


Public Const FID_STOCKTAKE_ID = (CLASSID_STOCKTAKE * &H10000) + 1
Public Const FID_STOCKTAKE_State = (CLASSID_STOCKTAKE * &H10000) + 2
Public Const FID_STOCKTAKE_FreezeDate = (CLASSID_STOCKTAKE * &H10000) + 3
Public Const FID_STOCKTAKE_AdjustDate = (CLASSID_STOCKTAKE * &H10000) + 4
Public Const FID_STOCKTAKE_Password = (CLASSID_STOCKTAKE * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKTAKE_END_OF_STATIC = FID_STOCKTAKE_Password


Public Const FID_RETNOTEHEADER_DocNo = (CLASSID_RETNOTEHEADER * &H10000) + 1
Public Const FID_RETNOTEHEADER_DocType = (CLASSID_RETNOTEHEADER * &H10000) + 2
Public Const FID_RETNOTEHEADER_Value = (CLASSID_RETNOTEHEADER * &H10000) + 3
Public Const FID_RETNOTEHEADER_Cost = (CLASSID_RETNOTEHEADER * &H10000) + 4
Public Const FID_RETNOTEHEADER_DelNoteDate = (CLASSID_RETNOTEHEADER * &H10000) + 5
Public Const FID_RETNOTEHEADER_Comm = (CLASSID_RETNOTEHEADER * &H10000) + 6
Public Const FID_RETNOTEHEADER_OrigDocKey = (CLASSID_RETNOTEHEADER * &H10000) + 7
Public Const FID_RETNOTEHEADER_EnteredBy = (CLASSID_RETNOTEHEADER * &H10000) + 8
Public Const FID_RETNOTEHEADER_Comment = (CLASSID_RETNOTEHEADER * &H10000) + 9
Public Const FID_RETNOTEHEADER_SupplierNumber = (CLASSID_RETNOTEHEADER * &H10000) + 10
Public Const FID_RETNOTEHEADER_ReturnDate = (CLASSID_RETNOTEHEADER * &H10000) + 11
Public Const FID_RETNOTEHEADER_OrigPONumber = (CLASSID_RETNOTEHEADER * &H10000) + 12
Public Const FID_RETNOTEHEADER_SupplierReturnNumber = (CLASSID_RETNOTEHEADER * &H10000) + 13
Public Const FID_RETNOTEHEADER_InvCrnMatched = (CLASSID_RETNOTEHEADER * &H10000) + 14
Public Const FID_RETNOTEHEADER_InvCrnDate = (CLASSID_RETNOTEHEADER * &H10000) + 15
Public Const FID_RETNOTEHEADER_InvCrnNumber = (CLASSID_RETNOTEHEADER * &H10000) + 16
Public Const FID_RETNOTEHEADER_VarianceValue = (CLASSID_RETNOTEHEADER * &H10000) + 17
Public Const FID_RETNOTEHEADER_InvoiceValue = (CLASSID_RETNOTEHEADER * &H10000) + 18
Public Const FID_RETNOTEHEADER_InvoiceVAT = (CLASSID_RETNOTEHEADER * &H10000) + 19
Public Const FID_RETNOTEHEADER_MatchingComment = (CLASSID_RETNOTEHEADER * &H10000) + 20
Public Const FID_RETNOTEHEADER_MatchingDate = (CLASSID_RETNOTEHEADER * &H10000) + 21
Public Const FID_RETNOTEHEADER_ReceiptDate = (CLASSID_RETNOTEHEADER * &H10000) + 22
Public Const FID_RETNOTEHEADER_RestockCharge = (CLASSID_RETNOTEHEADER * &H10000) + 23
Public Const FID_RETNOTEHEADER_Commit = (CLASSID_RETNOTEHEADER * &H10000) + 24
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETNOTEHEADER_END_OF_STATIC = FID_RETNOTEHEADER_Commit


Public Const FID_PARAMETER_ID = (CLASSID_PARAMETER * &H10000) + 1
Public Const FID_PARAMETER_Description = (CLASSID_PARAMETER * &H10000) + 2
Public Const FID_PARAMETER_String = (CLASSID_PARAMETER * &H10000) + 3
Public Const FID_PARAMETER_Long = (CLASSID_PARAMETER * &H10000) + 4
Public Const FID_PARAMETER_Boolean = (CLASSID_PARAMETER * &H10000) + 5
Public Const FID_PARAMETER_Decimal = (CLASSID_PARAMETER * &H10000) + 6
Public Const FID_PARAMETER_Type = (CLASSID_PARAMETER * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PARAMETER_END_OF_STATIC = FID_PARAMETER_Type


'Public Const FID_PARAMETER_ID = (CLASSID_PARAMETER * &H10000) + 1
'Public Const FID_PARAMETER_Description = (CLASSID_PARAMETER * &H10000) + 2
'Public Const FID_PARAMETER_String = (CLASSID_PARAMETER * &H10000) + 3
'Public Const FID_PARAMETER_Long = (CLASSID_PARAMETER * &H10000) + 4
'Public Const FID_PARAMETER_Boolean = (CLASSID_PARAMETER * &H10000) + 5
'Public Const FID_PARAMETER_Decimal = (CLASSID_PARAMETER * &H10000) + 6
'Public Const FID_PARAMETER_Type = (CLASSID_PARAMETER * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_PARAMETER_END_OF_STATIC = FID_PARAMETER_Type


Public Const FID_WORKSTATIONCONFIG_id = (CLASSID_WORKSTATIONCONFIG * &H10000) + 1
Public Const FID_WORKSTATIONCONFIG_Description = (CLASSID_WORKSTATIONCONFIG * &H10000) + 2
Public Const FID_WORKSTATIONCONFIG_OutletFunction = (CLASSID_WORKSTATIONCONFIG * &H10000) + 3
Public Const FID_WORKSTATIONCONFIG_Active = (CLASSID_WORKSTATIONCONFIG * &H10000) + 4
Public Const FID_WORKSTATIONCONFIG_TenderGroups = (CLASSID_WORKSTATIONCONFIG * &H10000) + 5
Public Const FID_WORKSTATIONCONFIG_GroupLevels = (CLASSID_WORKSTATIONCONFIG * &H10000) + 6
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_WORKSTATIONCONFIG_END_OF_STATIC = FID_WORKSTATIONCONFIG_GroupLevels


Public Const FID_USER_EmployeeID = (CLASSID_USER * &H10000) + 1
Public Const FID_USER_FullName = (CLASSID_USER * &H10000) + 2
Public Const FID_USER_Initials = (CLASSID_USER * &H10000) + 3
Public Const FID_USER_Position = (CLASSID_USER * &H10000) + 4
Public Const FID_USER_PayrollNumber = (CLASSID_USER * &H10000) + 5
Public Const FID_USER_Password = (CLASSID_USER * &H10000) + 6
Public Const FID_USER_PasswordValidTill = (CLASSID_USER * &H10000) + 7
Public Const FID_USER_Supervisor = (CLASSID_USER * &H10000) + 8
Public Const FID_USER_AuthorisationCode = (CLASSID_USER * &H10000) + 9
Public Const FID_USER_AuthorisationValidTill = (CLASSID_USER * &H10000) + 10
Public Const FID_USER_CurrentOutlet = (CLASSID_USER * &H10000) + 11
Public Const FID_USER_GroupLevels = (CLASSID_USER * &H10000) + 12
Public Const FID_USER_SecurityLevels = (CLASSID_USER * &H10000) + 13
Public Const FID_USER_Deleted = (CLASSID_USER * &H10000) + 14
Public Const FID_USER_DateDeleted = (CLASSID_USER * &H10000) + 15
Public Const FID_USER_TimeDeleted = (CLASSID_USER * &H10000) + 16
Public Const FID_USER_DeletedBy = (CLASSID_USER * &H10000) + 17
Public Const FID_USER_DeletedOutlet = (CLASSID_USER * &H10000) + 18
Public Const FID_USER_TillReceiptName = (CLASSID_USER * &H10000) + 19
Public Const FID_USER_FloatAmount = (CLASSID_USER * &H10000) + 20
Public Const FID_USER_LanguageCode = (CLASSID_USER * &H10000) + 21
Public Const FID_USER_Manager = (CLASSID_USER * &H10000) + 22
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_USER_END_OF_STATIC = FID_USER_Manager


Public Const FID_RETAILEROPTIONS_ID = (CLASSID_RETAILEROPTIONS * &H10000) + 1
Public Const FID_RETAILEROPTIONS_StoreNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 2
Public Const FID_RETAILEROPTIONS_StoreName = (CLASSID_RETAILEROPTIONS * &H10000) + 3
Public Const FID_RETAILEROPTIONS_AddressLine1 = (CLASSID_RETAILEROPTIONS * &H10000) + 4
Public Const FID_RETAILEROPTIONS_AddressLine2 = (CLASSID_RETAILEROPTIONS * &H10000) + 5
Public Const FID_RETAILEROPTIONS_AddressLine3 = (CLASSID_RETAILEROPTIONS * &H10000) + 6
Public Const FID_RETAILEROPTIONS_VatRatesInclusive = (CLASSID_RETAILEROPTIONS * &H10000) + 7
Public Const FID_RETAILEROPTIONS_AccountabilityType = (CLASSID_RETAILEROPTIONS * &H10000) + 8
Public Const FID_RETAILEROPTIONS_MaxValidCashierNumber = (CLASSID_RETAILEROPTIONS * &H10000) + 9
Public Const FID_RETAILEROPTIONS_KeepSalesPeriod = (CLASSID_RETAILEROPTIONS * &H10000) + 10
Public Const FID_RETAILEROPTIONS_EmployeeDiscount = (CLASSID_RETAILEROPTIONS * &H10000) + 11
Public Const FID_RETAILEROPTIONS_DefaultFloatAmount = (CLASSID_RETAILEROPTIONS * &H10000) + 12
Public Const FID_RETAILEROPTIONS_MaximumCashGiven = (CLASSID_RETAILEROPTIONS * &H10000) + 13
Public Const FID_RETAILEROPTIONS_MaxTillDrawerFloat = (CLASSID_RETAILEROPTIONS * &H10000) + 14
Public Const FID_RETAILEROPTIONS_RRReportsDate = (CLASSID_RETAILEROPTIONS * &H10000) + 15
Public Const FID_RETAILEROPTIONS_RRDateLastRun = (CLASSID_RETAILEROPTIONS * &H10000) + 16
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETAILEROPTIONS_END_OF_STATIC = FID_RETAILEROPTIONS_RRDateLastRun


Public Const FID_SYSTEMOPTIONS_ID = (CLASSID_SYSTEMOPTIONS * &H10000) + 1
Public Const FID_SYSTEMOPTIONS_StoreNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 2
Public Const FID_SYSTEMOPTIONS_StoreName = (CLASSID_SYSTEMOPTIONS * &H10000) + 3
Public Const FID_SYSTEMOPTIONS_HeadOfficeName = (CLASSID_SYSTEMOPTIONS * &H10000) + 4
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress1 = (CLASSID_SYSTEMOPTIONS * &H10000) + 5
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress2 = (CLASSID_SYSTEMOPTIONS * &H10000) + 6
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress3 = (CLASSID_SYSTEMOPTIONS * &H10000) + 7
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress4 = (CLASSID_SYSTEMOPTIONS * &H10000) + 8
Public Const FID_SYSTEMOPTIONS_HeadOfficeAddress5 = (CLASSID_SYSTEMOPTIONS * &H10000) + 9
Public Const FID_SYSTEMOPTIONS_HeadOfficePostCode = (CLASSID_SYSTEMOPTIONS * &H10000) + 10
Public Const FID_SYSTEMOPTIONS_HeadOfficePhoneNo = (CLASSID_SYSTEMOPTIONS * &H10000) + 11
Public Const FID_SYSTEMOPTIONS_HeadOfficeFaxNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 12
Public Const FID_SYSTEMOPTIONS_VATNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 13
Public Const FID_SYSTEMOPTIONS_MasterOutletNumber = (CLASSID_SYSTEMOPTIONS * &H10000) + 14
Public Const FID_SYSTEMOPTIONS_HighestValidFunction = (CLASSID_SYSTEMOPTIONS * &H10000) + 15
Public Const FID_SYSTEMOPTIONS_MultipleSignOns = (CLASSID_SYSTEMOPTIONS * &H10000) + 16
Public Const FID_SYSTEMOPTIONS_PasswordValidFor = (CLASSID_SYSTEMOPTIONS * &H10000) + 17
Public Const FID_SYSTEMOPTIONS_AuthorisationValidFor = (CLASSID_SYSTEMOPTIONS * &H10000) + 18
Public Const FID_SYSTEMOPTIONS_TrackStockOnHand = (CLASSID_SYSTEMOPTIONS * &H10000) + 19
Public Const FID_SYSTEMOPTIONS_UseOtherInvoiceAddress = (CLASSID_SYSTEMOPTIONS * &H10000) + 20
Public Const FID_SYSTEMOPTIONS_CostFigureAccessLevel = (CLASSID_SYSTEMOPTIONS * &H10000) + 21
Public Const FID_SYSTEMOPTIONS_AddressStyle = (CLASSID_SYSTEMOPTIONS * &H10000) + 22
Public Const FID_SYSTEMOPTIONS_PICDayCode = (CLASSID_SYSTEMOPTIONS * &H10000) + 23
Public Const FID_SYSTEMOPTIONS_PICUseHandhelds = (CLASSID_SYSTEMOPTIONS * &H10000) + 24
Public Const FID_SYSTEMOPTIONS_PICLastNoOfItems = (CLASSID_SYSTEMOPTIONS * &H10000) + 25
Public Const FID_SYSTEMOPTIONS_PICSampleFrequency = (CLASSID_SYSTEMOPTIONS * &H10000) + 26
Public Const FID_SYSTEMOPTIONS_AutoApplyPriceChanges = (CLASSID_SYSTEMOPTIONS * &H10000) + 27
Public Const FID_SYSTEMOPTIONS_AutoApplyNoDays = (CLASSID_SYSTEMOPTIONS * &H10000) + 28
Public Const FID_SYSTEMOPTIONS_AutoPrintShelfLabels = (CLASSID_SYSTEMOPTIONS * &H10000) + 29
Public Const FID_SYSTEMOPTIONS_BulkUpdates = (CLASSID_SYSTEMOPTIONS * &H10000) + 30
Public Const FID_SYSTEMOPTIONS_LocalPricingAllowed = (CLASSID_SYSTEMOPTIONS * &H10000) + 31
Public Const FID_SYSTEMOPTIONS_CardType = (CLASSID_SYSTEMOPTIONS * &H10000) + 32
Public Const FID_SYSTEMOPTIONS_CompanyGroup = (CLASSID_SYSTEMOPTIONS * &H10000) + 33
Public Const FID_SYSTEMOPTIONS_TillLocalDrive = (CLASSID_SYSTEMOPTIONS * &H10000) + 34
Public Const FID_SYSTEMOPTIONS_TillRemoteDrive = (CLASSID_SYSTEMOPTIONS * &H10000) + 35
Public Const FID_SYSTEMOPTIONS_TillPathName = (CLASSID_SYSTEMOPTIONS * &H10000) + 36
Public Const FID_SYSTEMOPTIONS_NoCopiesPricingDocs = (CLASSID_SYSTEMOPTIONS * &H10000) + 37
Public Const FID_SYSTEMOPTIONS_StoreMaintMinShelf = (CLASSID_SYSTEMOPTIONS * &H10000) + 38
Public Const FID_SYSTEMOPTIONS_StoreMaintMaxShelf = (CLASSID_SYSTEMOPTIONS * &H10000) + 39
Public Const FID_SYSTEMOPTIONS_MaximumShelfMultiplier = (CLASSID_SYSTEMOPTIONS * &H10000) + 40
Public Const FID_SYSTEMOPTIONS_AllowReceiptMaint = (CLASSID_SYSTEMOPTIONS * &H10000) + 41
Public Const FID_SYSTEMOPTIONS_ReceiptMaintGrace = (CLASSID_SYSTEMOPTIONS * &H10000) + 42
Public Const FID_SYSTEMOPTIONS_RunHierarchyLinkBuild = (CLASSID_SYSTEMOPTIONS * &H10000) + 43
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SYSTEMOPTIONS_END_OF_STATIC = FID_SYSTEMOPTIONS_RunHierarchyLinkBuild


Public Const FID_DELNOTENARRATIVE_StoreNumber = (CLASSID_DELNOTENARRATIVE * &H10000) + 1
Public Const FID_DELNOTENARRATIVE_DocumentNumber = (CLASSID_DELNOTENARRATIVE * &H10000) + 2
Public Const FID_DELNOTENARRATIVE_PartCode = (CLASSID_DELNOTENARRATIVE * &H10000) + 3
Public Const FID_DELNOTENARRATIVE_SequenceNumber = (CLASSID_DELNOTENARRATIVE * &H10000) + 4
Public Const FID_DELNOTENARRATIVE_NarrativeText1 = (CLASSID_DELNOTENARRATIVE * &H10000) + 5
Public Const FID_DELNOTENARRATIVE_NarrativeText2 = (CLASSID_DELNOTENARRATIVE * &H10000) + 6
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_DELNOTENARRATIVE_END_OF_STATIC = FID_DELNOTENARRATIVE_NarrativeText2


Public Const FID_COUNTEDSKU_PartCode = (CLASSID_COUNTEDSKU * &H10000) + 1
Public Const FID_COUNTEDSKU_NumberCounted = (CLASSID_COUNTEDSKU * &H10000) + 2
Public Const FID_COUNTEDSKU_DisplayCounted = (CLASSID_COUNTEDSKU * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_COUNTEDSKU_END_OF_STATIC = FID_COUNTEDSKU_DisplayCounted


Public Const FID_RETURNNOTE_DocNo = (CLASSID_RETURNNOTE * &H10000) + 1
Public Const FID_RETURNNOTE_DocType = (CLASSID_RETURNNOTE * &H10000) + 2
Public Const FID_RETURNNOTE_Value = (CLASSID_RETURNNOTE * &H10000) + 3
Public Const FID_RETURNNOTE_Cost = (CLASSID_RETURNNOTE * &H10000) + 4
Public Const FID_RETURNNOTE_DelNoteDate = (CLASSID_RETURNNOTE * &H10000) + 5
Public Const FID_RETURNNOTE_Comm = (CLASSID_RETURNNOTE * &H10000) + 6
Public Const FID_RETURNNOTE_OrigDocKey = (CLASSID_RETURNNOTE * &H10000) + 7
Public Const FID_RETURNNOTE_EnteredBy = (CLASSID_RETURNNOTE * &H10000) + 8
Public Const FID_RETURNNOTE_Comment = (CLASSID_RETURNNOTE * &H10000) + 9
Public Const FID_RETURNNOTE_SupplierNumber = (CLASSID_RETURNNOTE * &H10000) + 10
Public Const FID_RETURNNOTE_ReturnDate = (CLASSID_RETURNNOTE * &H10000) + 11
Public Const FID_RETURNNOTE_OrigPONumber = (CLASSID_RETURNNOTE * &H10000) + 12
Public Const FID_RETURNNOTE_SupplierReturnNumber = (CLASSID_RETURNNOTE * &H10000) + 13
Public Const FID_RETURNNOTE_InvCrnMatched = (CLASSID_RETURNNOTE * &H10000) + 14
Public Const FID_RETURNNOTE_InvCrnDate = (CLASSID_RETURNNOTE * &H10000) + 15
Public Const FID_RETURNNOTE_InvCrnNumber = (CLASSID_RETURNNOTE * &H10000) + 16
Public Const FID_RETURNNOTE_VarianceValue = (CLASSID_RETURNNOTE * &H10000) + 17
Public Const FID_RETURNNOTE_InvoiceValue = (CLASSID_RETURNNOTE * &H10000) + 18
Public Const FID_RETURNNOTE_InvoiceVAT = (CLASSID_RETURNNOTE * &H10000) + 19
Public Const FID_RETURNNOTE_MatchingComment = (CLASSID_RETURNNOTE * &H10000) + 20
Public Const FID_RETURNNOTE_MatchingDate = (CLASSID_RETURNNOTE * &H10000) + 21
Public Const FID_RETURNNOTE_ReceiptDate = (CLASSID_RETURNNOTE * &H10000) + 22
Public Const FID_RETURNNOTE_RestockCharge = (CLASSID_RETURNNOTE * &H10000) + 23
Public Const FID_RETURNNOTE_Commit = (CLASSID_RETURNNOTE * &H10000) + 24
Public Const FID_RETURNNOTE_CollectionNoteNumber = (CLASSID_RETURNNOTE * &H10000) + 25
Public Const FID_RETURNNOTE_OriginalDelNoteDRL = (CLASSID_RETURNNOTE * &H10000) + 26
Public Const FID_RETURNNOTE_SecondarySupplierNumber = (CLASSID_RETURNNOTE * &H10000) + 27
Public Const FID_RETURNNOTE_ShortageNote = (CLASSID_RETURNNOTE * &H10000) + 28
Public Const FID_RETURNNOTE_ReleaseNumber = (CLASSID_RETURNNOTE * &H10000) + 29
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETURNNOTE_END_OF_STATIC = FID_RETURNNOTE_ReleaseNumber


Public Const FID_RETURNNOTELINE_RetNoteKey = (CLASSID_RETURNNOTELINE * &H10000) + 1
Public Const FID_RETURNNOTELINE_LineNo = (CLASSID_RETURNNOTELINE * &H10000) + 2
Public Const FID_RETURNNOTELINE_PartCode = (CLASSID_RETURNNOTELINE * &H10000) + 3
Public Const FID_RETURNNOTELINE_PartCodeAlphaCode = (CLASSID_RETURNNOTELINE * &H10000) + 4
Public Const FID_RETURNNOTELINE_OrderQty = (CLASSID_RETURNNOTELINE * &H10000) + 5
Public Const FID_RETURNNOTELINE_ReturnedQty = (CLASSID_RETURNNOTELINE * &H10000) + 6
Public Const FID_RETURNNOTELINE_OrderPrice = (CLASSID_RETURNNOTELINE * &H10000) + 7
Public Const FID_RETURNNOTELINE_BlanketDiscDesc = (CLASSID_RETURNNOTELINE * &H10000) + 8
Public Const FID_RETURNNOTELINE_BlanketPartCode = (CLASSID_RETURNNOTELINE * &H10000) + 9
Public Const FID_RETURNNOTELINE_CurrentPrice = (CLASSID_RETURNNOTELINE * &H10000) + 10
Public Const FID_RETURNNOTELINE_CurrentCost = (CLASSID_RETURNNOTELINE * &H10000) + 11
Public Const FID_RETURNNOTELINE_IBTQuantity = (CLASSID_RETURNNOTELINE * &H10000) + 12
Public Const FID_RETURNNOTELINE_ReturnQty = (CLASSID_RETURNNOTELINE * &H10000) + 13
Public Const FID_RETURNNOTELINE_ToFollowQty = (CLASSID_RETURNNOTELINE * &H10000) + 14
Public Const FID_RETURNNOTELINE_ReturnReasonCode = (CLASSID_RETURNNOTELINE * &H10000) + 15
Public Const FID_RETURNNOTELINE_POLineNumber = (CLASSID_RETURNNOTELINE * &H10000) + 16
Public Const FID_RETURNNOTELINE_Checked = (CLASSID_RETURNNOTELINE * &H10000) + 17
Public Const FID_RETURNNOTELINE_SinglesSellingUnit = (CLASSID_RETURNNOTELINE * &H10000) + 18
Public Const FID_RETURNNOTELINE_Narrative = (CLASSID_RETURNNOTELINE * &H10000) + 19
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_RETURNNOTELINE_END_OF_STATIC = FID_RETURNNOTELINE_Narrative


Public Const FID_SALETOTALS_SaleTotalKey = (CLASSID_SALETOTALS * &H10000) + 1
Public Const FID_SALETOTALS_FloatAmount = (CLASSID_SALETOTALS * &H10000) + 2
Public Const FID_SALETOTALS_PickUpAmount = (CLASSID_SALETOTALS * &H10000) + 3
Public Const FID_SALETOTALS_TransactionCount = (CLASSID_SALETOTALS * &H10000) + 4
Public Const FID_SALETOTALS_TranLastTime = (CLASSID_SALETOTALS * &H10000) + 5
Public Const FID_SALETOTALS_TransactionValue = (CLASSID_SALETOTALS * &H10000) + 6
Public Const FID_SALETOTALS_TenderTypesCount = (CLASSID_SALETOTALS * &H10000) + 7
Public Const FID_SALETOTALS_TenderTypesValue = (CLASSID_SALETOTALS * &H10000) + 8
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SALETOTALS_END_OF_STATIC = FID_SALETOTALS_TenderTypesValue


Public Const FID_TILLTOTALS_TillNumber = (CLASSID_TILLTOTALS * &H10000) + 1
Public Const FID_TILLTOTALS_SaleTotalKey = (CLASSID_TILLTOTALS * &H10000) + 2
Public Const FID_TILLTOTALS_CashierNumber = (CLASSID_TILLTOTALS * &H10000) + 3
Public Const FID_TILLTOTALS_PreviousCashierNumber = (CLASSID_TILLTOTALS * &H10000) + 4
Public Const FID_TILLTOTALS_TimeLastSignedOff = (CLASSID_TILLTOTALS * &H10000) + 5
Public Const FID_TILLTOTALS_TransactionCount = (CLASSID_TILLTOTALS * &H10000) + 6
Public Const FID_TILLTOTALS_TransactionValues = (CLASSID_TILLTOTALS * &H10000) + 7
Public Const FID_TILLTOTALS_CurrentTransactionNumber = (CLASSID_TILLTOTALS * &H10000) + 8
Public Const FID_TILLTOTALS_TillZReadDone = (CLASSID_TILLTOTALS * &H10000) + 9
Public Const FID_TILLTOTALS_TillCheckingEvents = (CLASSID_TILLTOTALS * &H10000) + 10
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TILLTOTALS_END_OF_STATIC = FID_TILLTOTALS_TillCheckingEvents


Public Const FID_CASHIERTOTALS_CashierNumber = (CLASSID_CASHIERTOTALS * &H10000) + 1
Public Const FID_CASHIERTOTALS_SaleTotalsKey = (CLASSID_CASHIERTOTALS * &H10000) + 2
Public Const FID_CASHIERTOTALS_CurrentTillNumber = (CLASSID_CASHIERTOTALS * &H10000) + 3
Public Const FID_CASHIERTOTALS_CashierName = (CLASSID_CASHIERTOTALS * &H10000) + 4
Public Const FID_CASHIERTOTALS_SecurityCode = (CLASSID_CASHIERTOTALS * &H10000) + 5
Public Const FID_CASHIERTOTALS_TransactionCount = (CLASSID_CASHIERTOTALS * &H10000) + 6
Public Const FID_CASHIERTOTALS_TransactionValue = (CLASSID_CASHIERTOTALS * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CASHIERTOTALS_END_OF_STATIC = FID_CASHIERTOTALS_TransactionValue


Public Const FID_ISTINEDIHEADER_SourceStoreNumber = (CLASSID_ISTINEDIHEADER * &H10000) + 1
Public Const FID_ISTINEDIHEADER_ISTOutNumber = (CLASSID_ISTINEDIHEADER * &H10000) + 2
Public Const FID_ISTINEDIHEADER_SentDate = (CLASSID_ISTINEDIHEADER * &H10000) + 3
Public Const FID_ISTINEDIHEADER_ReceivedDate = (CLASSID_ISTINEDIHEADER * &H10000) + 4
Public Const FID_ISTINEDIHEADER_ReceiptDate = (CLASSID_ISTINEDIHEADER * &H10000) + 5
Public Const FID_ISTINEDIHEADER_TotalQuantity = (CLASSID_ISTINEDIHEADER * &H10000) + 6
Public Const FID_ISTINEDIHEADER_TotalValue = (CLASSID_ISTINEDIHEADER * &H10000) + 7
Public Const FID_ISTINEDIHEADER_TotalCost = (CLASSID_ISTINEDIHEADER * &H10000) + 8
Public Const FID_ISTINEDIHEADER_Received = (CLASSID_ISTINEDIHEADER * &H10000) + 9
Public Const FID_ISTINEDIHEADER_Cancelled = (CLASSID_ISTINEDIHEADER * &H10000) + 10
Public Const FID_ISTINEDIHEADER_ReceivedBy = (CLASSID_ISTINEDIHEADER * &H10000) + 11
Public Const FID_ISTINEDIHEADER_ISTInQuantity = (CLASSID_ISTINEDIHEADER * &H10000) + 12
Public Const FID_ISTINEDIHEADER_ISTOutCancelled = (CLASSID_ISTINEDIHEADER * &H10000) + 13
Public Const FID_ISTINEDIHEADER_ISTInNumber = (CLASSID_ISTINEDIHEADER * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ISTINEDIHEADER_END_OF_STATIC = FID_ISTINEDIHEADER_ISTInNumber


Public Const FID_ISTINEDILINE_SourceStoreNumber = (CLASSID_ISTINEDILINE * &H10000) + 1
Public Const FID_ISTINEDILINE_ISTOutNumber = (CLASSID_ISTINEDILINE * &H10000) + 2
Public Const FID_ISTINEDILINE_PartCode = (CLASSID_ISTINEDILINE * &H10000) + 3
Public Const FID_ISTINEDILINE_LineNumber = (CLASSID_ISTINEDILINE * &H10000) + 4
Public Const FID_ISTINEDILINE_Quantity = (CLASSID_ISTINEDILINE * &H10000) + 5
Public Const FID_ISTINEDILINE_Price = (CLASSID_ISTINEDILINE * &H10000) + 6
Public Const FID_ISTINEDILINE_Cost = (CLASSID_ISTINEDILINE * &H10000) + 7
Public Const FID_ISTINEDILINE_QuantityPerSellingUnit = (CLASSID_ISTINEDILINE * &H10000) + 8
Public Const FID_ISTINEDILINE_QuantityReceived = (CLASSID_ISTINEDILINE * &H10000) + 9
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ISTINEDILINE_END_OF_STATIC = FID_ISTINEDILINE_QuantityReceived


Public Const FID_PURCHASECONSLINE_PurchaseOrderKey = (CLASSID_PURCHASECONSLINE * &H10000) + 1
Public Const FID_PURCHASECONSLINE_LineNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 2
Public Const FID_PURCHASECONSLINE_PurchaseOrderNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 3
Public Const FID_PURCHASECONSLINE_PurchaseOrderLineNo = (CLASSID_PURCHASECONSLINE * &H10000) + 4
Public Const FID_PURCHASECONSLINE_CustOrderNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 5
Public Const FID_PURCHASECONSLINE_CustOrderLineNumber = (CLASSID_PURCHASECONSLINE * &H10000) + 6
Public Const FID_PURCHASECONSLINE_PartCode = (CLASSID_PURCHASECONSLINE * &H10000) + 7
Public Const FID_PURCHASECONSLINE_ProductCode = (CLASSID_PURCHASECONSLINE * &H10000) + 8
Public Const FID_PURCHASECONSLINE_Quantity = (CLASSID_PURCHASECONSLINE * &H10000) + 9
Public Const FID_PURCHASECONSLINE_Price = (CLASSID_PURCHASECONSLINE * &H10000) + 10
Public Const FID_PURCHASECONSLINE_Cost = (CLASSID_PURCHASECONSLINE * &H10000) + 11
Public Const FID_PURCHASECONSLINE_QuantityPerUnit = (CLASSID_PURCHASECONSLINE * &H10000) + 12
Public Const FID_PURCHASECONSLINE_Complete = (CLASSID_PURCHASECONSLINE * &H10000) + 13
Public Const FID_PURCHASECONSLINE_OrderDate = (CLASSID_PURCHASECONSLINE * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PURCHASECONSLINE_END_OF_STATIC = FID_PURCHASECONSLINE_OrderDate


Public Const FID_INVENTORYINFO_PartCode = (CLASSID_INVENTORYINFO * &H10000) + 1
Public Const FID_INVENTORYINFO_SequenceNumber = (CLASSID_INVENTORYINFO * &H10000) + 2
Public Const FID_INVENTORYINFO_TextBody = (CLASSID_INVENTORYINFO * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_INVENTORYINFO_END_OF_STATIC = FID_INVENTORYINFO_TextBody


Public Const FID_STOCKADJREASON_AdjustmentNo = (CLASSID_STOCKADJREASON * &H10000) + 1
Public Const FID_STOCKADJREASON_AdjustmentCode = (CLASSID_STOCKADJREASON * &H10000) + 2
Public Const FID_STOCKADJREASON_Description = (CLASSID_STOCKADJREASON * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKADJREASON_END_OF_STATIC = FID_STOCKADJREASON_Description


Public Const FID_ACTIVITYLOG_Key = (CLASSID_ACTIVITYLOG * &H10000) + 1
Public Const FID_ACTIVITYLOG_SystemDate = (CLASSID_ACTIVITYLOG * &H10000) + 2
Public Const FID_ACTIVITYLOG_EmployeeID = (CLASSID_ACTIVITYLOG * &H10000) + 3
Public Const FID_ACTIVITYLOG_WorkstationID = (CLASSID_ACTIVITYLOG * &H10000) + 4
Public Const FID_ACTIVITYLOG_GroupID = (CLASSID_ACTIVITYLOG * &H10000) + 5
Public Const FID_ACTIVITYLOG_ProgramID = (CLASSID_ACTIVITYLOG * &H10000) + 6
Public Const FID_ACTIVITYLOG_LoggingIn = (CLASSID_ACTIVITYLOG * &H10000) + 7
Public Const FID_ACTIVITYLOG_AutoLoggedOut = (CLASSID_ACTIVITYLOG * &H10000) + 8
Public Const FID_ACTIVITYLOG_StartTime = (CLASSID_ACTIVITYLOG * &H10000) + 9
Public Const FID_ACTIVITYLOG_EndTime = (CLASSID_ACTIVITYLOG * &H10000) + 10
Public Const FID_ACTIVITYLOG_EndDate = (CLASSID_ACTIVITYLOG * &H10000) + 11
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ACTIVITYLOG_END_OF_STATIC = FID_ACTIVITYLOG_EndDate


Public Const FID_STORESTATUS_Key = (CLASSID_STORESTATUS * &H10000) + 1
Public Const FID_STORESTATUS_MasterPCOpened = (CLASSID_STORESTATUS * &H10000) + 2
Public Const FID_STORESTATUS_DateOpened = (CLASSID_STORESTATUS * &H10000) + 3
Public Const FID_STORESTATUS_SlavePCOpened = (CLASSID_STORESTATUS * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STORESTATUS_END_OF_STATIC = FID_STORESTATUS_SlavePCOpened


Public Const FID_EDIFILECONTROL_FileCode = (CLASSID_EDIFILECONTROL * &H10000) + 1
Public Const FID_EDIFILECONTROL_Description = (CLASSID_EDIFILECONTROL * &H10000) + 2
Public Const FID_EDIFILECONTROL_LastSequenceNo = (CLASSID_EDIFILECONTROL * &H10000) + 3
Public Const FID_EDIFILECONTROL_DoneSequenceNo = (CLASSID_EDIFILECONTROL * &H10000) + 4
Public Const FID_EDIFILECONTROL_ProcessingType = (CLASSID_EDIFILECONTROL * &H10000) + 5
Public Const FID_EDIFILECONTROL_EndToEndApplies = (CLASSID_EDIFILECONTROL * &H10000) + 6
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIFILECONTROL_END_OF_STATIC = FID_EDIFILECONTROL_EndToEndApplies

Public Const FID_SALESLEDGER_EntryID = (CLASSID_SALESLEDGER * &H10000) + 1
Public Const FID_SALESLEDGER_StoreNumber = (CLASSID_SALESLEDGER * &H10000) + 2
Public Const FID_SALESLEDGER_CustomerNumber = (CLASSID_SALESLEDGER * &H10000) + 3
Public Const FID_SALESLEDGER_TransactionDate = (CLASSID_SALESLEDGER * &H10000) + 4
Public Const FID_SALESLEDGER_DeliveryDate = (CLASSID_SALESLEDGER * &H10000) + 5
Public Const FID_SALESLEDGER_PostingDate = (CLASSID_SALESLEDGER * &H10000) + 6
Public Const FID_SALESLEDGER_PaymentDueDate = (CLASSID_SALESLEDGER * &H10000) + 7
Public Const FID_SALESLEDGER_PaidDate = (CLASSID_SALESLEDGER * &H10000) + 8
Public Const FID_SALESLEDGER_PeriodStart = (CLASSID_SALESLEDGER * &H10000) + 9
Public Const FID_SALESLEDGER_DocumentNumber = (CLASSID_SALESLEDGER * &H10000) + 10
Public Const FID_SALESLEDGER_OrderNumber = (CLASSID_SALESLEDGER * &H10000) + 11
Public Const FID_SALESLEDGER_TransactionType = (CLASSID_SALESLEDGER * &H10000) + 12
Public Const FID_SALESLEDGER_TotalValue = (CLASSID_SALESLEDGER * &H10000) + 13
Public Const FID_SALESLEDGER_TotalVAT = (CLASSID_SALESLEDGER * &H10000) + 14
Public Const FID_SALESLEDGER_OutstandingValue = (CLASSID_SALESLEDGER * &H10000) + 15
Public Const FID_SALESLEDGER_Disputed = (CLASSID_SALESLEDGER * &H10000) + 16
Public Const FID_SALESLEDGER_StatementPrinted = (CLASSID_SALESLEDGER * &H10000) + 17
Public Const FID_SALESLEDGER_UserInitials = (CLASSID_SALESLEDGER * &H10000) + 18
Public Const FID_SALESLEDGER_DateCommed = (CLASSID_SALESLEDGER * &H10000) + 19
Public Const FID_SALESLEDGER_Memo1 = (CLASSID_SALESLEDGER * &H10000) + 20
Public Const FID_SALESLEDGER_Memo2 = (CLASSID_SALESLEDGER * &H10000) + 21
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SALESLEDGER_END_OF_STATIC = FID_SALESLEDGER_Memo2


Public Const FID_TCFMASTER_FileCode = (CLASSID_TCFMASTER * &H10000) + 1
Public Const FID_TCFMASTER_SequenceNumber = (CLASSID_TCFMASTER * &H10000) + 2
Public Const FID_TCFMASTER_SourceSequenceNumber = (CLASSID_TCFMASTER * &H10000) + 3
Public Const FID_TCFMASTER_DateCreated = (CLASSID_TCFMASTER * &H10000) + 4
Public Const FID_TCFMASTER_TransmissionDate = (CLASSID_TCFMASTER * &H10000) + 5
Public Const FID_TCFMASTER_TransmissionTime = (CLASSID_TCFMASTER * &H10000) + 6
Public Const FID_TCFMASTER_SentRecordCount = (CLASSID_TCFMASTER * &H10000) + 7
Public Const FID_TCFMASTER_ActualRecordCount = (CLASSID_TCFMASTER * &H10000) + 8
Public Const FID_TCFMASTER_EndToEnd = (CLASSID_TCFMASTER * &H10000) + 9
Public Const FID_TCFMASTER_ReadyToProcess = (CLASSID_TCFMASTER * &H10000) + 10
Public Const FID_TCFMASTER_DateReady = (CLASSID_TCFMASTER * &H10000) + 11
Public Const FID_TCFMASTER_TimeReady = (CLASSID_TCFMASTER * &H10000) + 12
Public Const FID_TCFMASTER_Processed = (CLASSID_TCFMASTER * &H10000) + 13
Public Const FID_TCFMASTER_DateProcessed = (CLASSID_TCFMASTER * &H10000) + 14
Public Const FID_TCFMASTER_TimeProcessed = (CLASSID_TCFMASTER * &H10000) + 15
Public Const FID_TCFMASTER_ErrorCode = (CLASSID_TCFMASTER * &H10000) + 16
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TCFMASTER_END_OF_STATIC = FID_TCFMASTER_ErrorCode


Public Const FID_CASHBALANCECONTROL_ControlDate = (CLASSID_CASHBALANCECONTROL * &H10000) + 1
Public Const FID_CASHBALANCECONTROL_DayBalanced = (CLASSID_CASHBALANCECONTROL * &H10000) + 2
Public Const FID_CASHBALANCECONTROL_CommNumber = (CLASSID_CASHBALANCECONTROL * &H10000) + 3
Public Const FID_CASHBALANCECONTROL_OtherIncome = (CLASSID_CASHBALANCECONTROL * &H10000) + 4
Public Const FID_CASHBALANCECONTROL_MiscOutgoings = (CLASSID_CASHBALANCECONTROL * &H10000) + 5
Public Const FID_CASHBALANCECONTROL_TotalDeposits = (CLASSID_CASHBALANCECONTROL * &H10000) + 6
Public Const FID_CASHBALANCECONTROL_TotalVouchers = (CLASSID_CASHBALANCECONTROL * &H10000) + 7
Public Const FID_CASHBALANCECONTROL_TotalFloats = (CLASSID_CASHBALANCECONTROL * &H10000) + 8
Public Const FID_CASHBALANCECONTROL_BankingAmount = (CLASSID_CASHBALANCECONTROL * &H10000) + 9
Public Const FID_CASHBALANCECONTROL_BankSlipNumbers = (CLASSID_CASHBALANCECONTROL * &H10000) + 10
Public Const FID_CASHBALANCECONTROL_Comment1 = (CLASSID_CASHBALANCECONTROL * &H10000) + 11
Public Const FID_CASHBALANCECONTROL_Comment2 = (CLASSID_CASHBALANCECONTROL * &H10000) + 12
Public Const FID_CASHBALANCECONTROL_Comment3 = (CLASSID_CASHBALANCECONTROL * &H10000) + 13
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CASHBALANCECONTROL_END_OF_STATIC = FID_CASHBALANCECONTROL_Comment3


Public Const FID_CASHBALANCESUMMARY_Date = (CLASSID_CASHBALANCESUMMARY * &H10000) + 1
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CASHBALANCESUMMARY_END_OF_STATIC = FID_CASHBALANCESUMMARY_Date


Public Const FID_STOCKLEVEL_PartCode = (CLASSID_STOCKLEVEL * &H10000) + 1
Public Const FID_STOCKLEVEL_Description = (CLASSID_STOCKLEVEL * &H10000) + 2
Public Const FID_STOCKLEVEL_DepartmentCode = (CLASSID_STOCKLEVEL * &H10000) + 3
Public Const FID_STOCKLEVEL_QuantityToUOM = (CLASSID_STOCKLEVEL * &H10000) + 4
Public Const FID_STOCKLEVEL_SellInUnitOccNo = (CLASSID_STOCKLEVEL * &H10000) + 5
Public Const FID_STOCKLEVEL_CostPrice = (CLASSID_STOCKLEVEL * &H10000) + 6
Public Const FID_STOCKLEVEL_QuantityAtHand = (CLASSID_STOCKLEVEL * &H10000) + 7
Public Const FID_STOCKLEVEL_QuantityOnOrder = (CLASSID_STOCKLEVEL * &H10000) + 8
Public Const FID_STOCKLEVEL_QuantityOnDisplay = (CLASSID_STOCKLEVEL * &H10000) + 9
Public Const FID_STOCKLEVEL_QuantityCustOrder = (CLASSID_STOCKLEVEL * &H10000) + 10
Public Const FID_STOCKLEVEL_MinQuantity = (CLASSID_STOCKLEVEL * &H10000) + 11
Public Const FID_STOCKLEVEL_MaxQuantity = (CLASSID_STOCKLEVEL * &H10000) + 12
Public Const FID_STOCKLEVEL_SizeDescription = (CLASSID_STOCKLEVEL * &H10000) + 13
Public Const FID_STOCKLEVEL_Flag5 = (CLASSID_STOCKLEVEL * &H10000) + 14
Public Const FID_STOCKLEVEL_FreeStock = (CLASSID_STOCKLEVEL * &H10000) + 15
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_STOCKLEVEL_END_OF_STATIC = FID_STOCKLEVEL_FreeStock


Public Const FID_POLINES_PurchaseOrderNo = (CLASSID_POLINES * &H10000) + 1
Public Const FID_POLINES_PartCode = (CLASSID_POLINES * &H10000) + 2
Public Const FID_POLINES_Complete = (CLASSID_POLINES * &H10000) + 3
Public Const FID_POLINES_OrderDate = (CLASSID_POLINES * &H10000) + 4
Public Const FID_POLINES_DueDate = (CLASSID_POLINES * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POLINES_END_OF_STATIC = FID_POLINES_DueDate


Public Const FID_OPENDRAWERCODE_CodeType = (CLASSID_OPENDRAWERCODE * &H10000) + 1
Public Const FID_OPENDRAWERCODE_Code = (CLASSID_OPENDRAWERCODE * &H10000) + 2
Public Const FID_OPENDRAWERCODE_Description = (CLASSID_OPENDRAWERCODE * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_OPENDRAWERCODE_END_OF_STATIC = FID_OPENDRAWERCODE_Description


Public Const FID_PAIDINCODE_CodeType = (CLASSID_PAIDINCODE * &H10000) + 1
Public Const FID_PAIDINCODE_Code = (CLASSID_PAIDINCODE * &H10000) + 2
Public Const FID_PAIDINCODE_Description = (CLASSID_PAIDINCODE * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PAIDINCODE_END_OF_STATIC = FID_PAIDINCODE_Description


Public Const FID_PAIDOUTCODE_CodeType = (CLASSID_PAIDOUTCODE * &H10000) + 1
Public Const FID_PAIDOUTCODE_Code = (CLASSID_PAIDOUTCODE * &H10000) + 2
Public Const FID_PAIDOUTCODE_Description = (CLASSID_PAIDOUTCODE * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PAIDOUTCODE_END_OF_STATIC = FID_PAIDOUTCODE_Description


Public Const FID_TRANSACTION_DISCOUNT_CodeType = (CLASSID_TRANSACTION_DISCOUNT * &H10000) + 1
Public Const FID_TRANSACTION_DISCOUNT_Code = (CLASSID_TRANSACTION_DISCOUNT * &H10000) + 2
Public Const FID_TRANSACTION_DISCOUNT_Description = (CLASSID_TRANSACTION_DISCOUNT * &H10000) + 3
Public Const FID_TRANSACTION_DISCOUNT_StockAttribute = (CLASSID_TRANSACTION_DISCOUNT * &H10000) + 4
Public Const FID_TRANSACTION_DISCOUNT_PrintReturnLabel = (CLASSID_TRANSACTION_DISCOUNT * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TRANSACTION_DISCOUNT_END_OF_STATIC = FID_TRANSACTION_DISCOUNT_PrintReturnLabel


Public Const FID_SALESCUSTOMER_TranDate = (CLASSID_SALESCUSTOMER * &H10000) + 1
Public Const FID_SALESCUSTOMER_TillID = (CLASSID_SALESCUSTOMER * &H10000) + 2
Public Const FID_SALESCUSTOMER_TransactionNo = (CLASSID_SALESCUSTOMER * &H10000) + 3
Public Const FID_SALESCUSTOMER_TransactionTime = (CLASSID_SALESCUSTOMER * &H10000) + 4
Public Const FID_SALESCUSTOMER_TransactionCode = (CLASSID_SALESCUSTOMER * &H10000) + 5
Public Const FID_SALESCUSTOMER_ContactName = (CLASSID_SALESCUSTOMER * &H10000) + 6
Public Const FID_SALESCUSTOMER_PostCode = (CLASSID_SALESCUSTOMER * &H10000) + 7
Public Const FID_SALESCUSTOMER_TotalSaleAmount = (CLASSID_SALESCUSTOMER * &H10000) + 8
Public Const FID_SALESCUSTOMER_Voided = (CLASSID_SALESCUSTOMER * &H10000) + 9
Public Const FID_SALESCUSTOMER_TranParked = (CLASSID_SALESCUSTOMER * &H10000) + 10
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_SALESCUSTOMER_END_OF_STATIC = FID_SALESCUSTOMER_TranParked


Public Const FID_EDIDELHEADER_StoreNumber = (CLASSID_EDIDELHEADER * &H10000) + 1
Public Const FID_EDIDELHEADER_PurchaseOrderNumber = (CLASSID_EDIDELHEADER * &H10000) + 2
Public Const FID_EDIDELHEADER_SupplierCode = (CLASSID_EDIDELHEADER * &H10000) + 3
Public Const FID_EDIDELHEADER_DeliveryNoteNumber = (CLASSID_EDIDELHEADER * &H10000) + 4
Public Const FID_EDIDELHEADER_DeliveryDate = (CLASSID_EDIDELHEADER * &H10000) + 5
Public Const FID_EDIDELHEADER_CarraigeValue = (CLASSID_EDIDELHEADER * &H10000) + 6
Public Const FID_EDIDELHEADER_ReceivedInDate = (CLASSID_EDIDELHEADER * &H10000) + 7
Public Const FID_EDIDELHEADER_CommNo = (CLASSID_EDIDELHEADER * &H10000) + 8
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIDELHEADER_END_OF_STATIC = FID_EDIDELHEADER_CommNo


Public Const FID_EDIDELLINE_StoreNumber = (CLASSID_EDIDELLINE * &H10000) + 1
Public Const FID_EDIDELLINE_PurchaseOrderNumber = (CLASSID_EDIDELLINE * &H10000) + 2
Public Const FID_EDIDELLINE_DeliveryNoteNumber = (CLASSID_EDIDELLINE * &H10000) + 3
Public Const FID_EDIDELLINE_PartCode = (CLASSID_EDIDELLINE * &H10000) + 4
Public Const FID_EDIDELLINE_DeliveryQuantity = (CLASSID_EDIDELLINE * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIDELLINE_END_OF_STATIC = FID_EDIDELLINE_DeliveryQuantity


Public Const FID_EDICHANGECODE_CodeType = (CLASSID_EDICHANGECODE * &H10000) + 1
Public Const FID_EDICHANGECODE_Code = (CLASSID_EDICHANGECODE * &H10000) + 2
Public Const FID_EDICHANGECODE_Description = (CLASSID_EDICHANGECODE * &H10000) + 3
Public Const FID_EDICHANGECODE_KeyInDescription = (CLASSID_EDICHANGECODE * &H10000) + 4
Public Const FID_EDICHANGECODE_MarginErosionCode = (CLASSID_EDICHANGECODE * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDICHANGECODE_END_OF_STATIC = FID_EDICHANGECODE_MarginErosionCode


Public Const FID_EDIPOLINES_PurchaseOrderNumber = (CLASSID_EDIPOLINES * &H10000) + 1
Public Const FID_EDIPOLINES_SupplierNo = (CLASSID_EDIPOLINES * &H10000) + 2
Public Const FID_EDIPOLINES_DueDate = (CLASSID_EDIPOLINES * &H10000) + 3
Public Const FID_EDIPOLINES_EDIConfDeliveryDate = (CLASSID_EDIPOLINES * &H10000) + 4
Public Const FID_EDIPOLINES_PartCode = (CLASSID_EDIPOLINES * &H10000) + 5
Public Const FID_EDIPOLINES_Description = (CLASSID_EDIPOLINES * &H10000) + 6
Public Const FID_EDIPOLINES_SizeDescription = (CLASSID_EDIPOLINES * &H10000) + 7
Public Const FID_EDIPOLINES_OrderQuantity = (CLASSID_EDIPOLINES * &H10000) + 8
Public Const FID_EDIPOLINES_EDIConfirmedQuantity = (CLASSID_EDIPOLINES * &H10000) + 9
Public Const FID_EDIPOLINES_CustomerOrderNumber = (CLASSID_EDIPOLINES * &H10000) + 10
Public Const FID_EDIPOLINES_ManufProductCode = (CLASSID_EDIPOLINES * &H10000) + 11
Public Const FID_EDIPOLINES_DelNoteReceivedDate = (CLASSID_EDIPOLINES * &H10000) + 12
Public Const FID_EDIPOLINES_QtyOnDelNote = (CLASSID_EDIPOLINES * &H10000) + 13
Public Const FID_EDIPOLINES_DeliveryNoteNumber = (CLASSID_EDIPOLINES * &H10000) + 14
Public Const FID_EDIPOLINES_HOSuppConfDate = (CLASSID_EDIPOLINES * &H10000) + 15
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIPOLINES_END_OF_STATIC = FID_EDIPOLINES_HOSuppConfDate

Public Const FID_EDIPOEARLY_PurchaseOrderNumber = (CLASSID_EDIPOEARLY * &H10000) + 1
Public Const FID_EDIPOEARLY_SupplierNo = (CLASSID_EDIPOEARLY * &H10000) + 2
Public Const FID_EDIPOEARLY_SupplierName = (CLASSID_EDIPOEARLY * &H10000) + 3
Public Const FID_EDIPOEARLY_DueDate = (CLASSID_EDIPOEARLY * &H10000) + 4
Public Const FID_EDIPOEARLY_EDIConfDeliveryDate = (CLASSID_EDIPOEARLY * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIPOEARLY_END_OF_STATIC = FID_EDIPOEARLY_EDIConfDeliveryDate


Public Const FID_EDIPOLATE_PurchaseOrderNumber = (CLASSID_EDIPOLATE * &H10000) + 1
Public Const FID_EDIPOLATE_SupplierNo = (CLASSID_EDIPOLATE * &H10000) + 2
Public Const FID_EDIPOLATE_SupplierName = (CLASSID_EDIPOLATE * &H10000) + 3
Public Const FID_EDIPOLATE_DueDate = (CLASSID_EDIPOLATE * &H10000) + 4
Public Const FID_EDIPOLATE_EDIConfDeliveryDate = (CLASSID_EDIPOLATE * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIPOLATE_END_OF_STATIC = FID_EDIPOLATE_EDIConfDeliveryDate


Public Const FID_EDIPOQTYDISCREPANCY_PurchaseOrderNumber = (CLASSID_EDIPOQTYDISCREPANCY * &H10000) + 1
Public Const FID_EDIPOQTYDISCREPANCY_SupplierNo = (CLASSID_EDIPOQTYDISCREPANCY * &H10000) + 2
Public Const FID_EDIPOQTYDISCREPANCY_SupplierName = (CLASSID_EDIPOQTYDISCREPANCY * &H10000) + 3
Public Const FID_EDIPOQTYDISCREPANCY_DueDate = (CLASSID_EDIPOQTYDISCREPANCY * &H10000) + 4
Public Const FID_EDIPOQTYDISCREPANCY_EDIConfDeliveryDate = (CLASSID_EDIPOQTYDISCREPANCY * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIPOQTYDISCREPANCY_END_OF_STATIC = FID_EDIPOQTYDISCREPANCY_EDIConfDeliveryDate


Public Const FID_EDIPOQTYDISCREPANCYLINES_PurchaseOrderNumber = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 1
Public Const FID_EDIPOQTYDISCREPANCYLINES_SupplierNo = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 2
Public Const FID_EDIPOQTYDISCREPANCYLINES_SupplierName = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 3
Public Const FID_EDIPOQTYDISCREPANCYLINES_DueDate = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 4
Public Const FID_EDIPOQTYDISCREPANCYLINES_EDIConfDeliveryDate = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 5
Public Const FID_EDIPOQTYDISCREPANCYLINES_PartCode = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 6
Public Const FID_EDIPOQTYDISCREPANCYLINES_Description = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 7
Public Const FID_EDIPOQTYDISCREPANCYLINES_SizeDescription = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 8
Public Const FID_EDIPOQTYDISCREPANCYLINES_OrderQuantity = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 9
Public Const FID_EDIPOQTYDISCREPANCYLINES_EDIConfirmedQuantity = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 10
Public Const FID_EDIPOQTYDISCREPANCYLINES_Discrepancy = (CLASSID_EDIPOQTYDISCREPANCYLINES * &H10000) + 11
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_EDIPOQTYDISCREPANCYLINES_END_OF_STATIC = FID_EDIPOQTYDISCREPANCYLINES_Discrepancy


Public Const FID_GROSSMARGINS_PartCode = (CLASSID_GROSSMARGINS * &H10000) + 1
Public Const FID_GROSSMARGINS_DepartmentCode = (CLASSID_GROSSMARGINS * &H10000) + 2
Public Const FID_GROSSMARGINS_Group = (CLASSID_GROSSMARGINS * &H10000) + 3
Public Const FID_GROSSMARGINS_SalesUnits = (CLASSID_GROSSMARGINS * &H10000) + 4
Public Const FID_GROSSMARGINS_SalesValue = (CLASSID_GROSSMARGINS * &H10000) + 5
Public Const FID_GROSSMARGINS_SalesCost = (CLASSID_GROSSMARGINS * &H10000) + 6
Public Const FID_GROSSMARGINS_StockHoldingAtCost = (CLASSID_GROSSMARGINS * &H10000) + 7
Public Const FID_GROSSMARGINS_DaysStoreOpen = (CLASSID_GROSSMARGINS * &H10000) + 8
Public Const FID_GROSSMARGINS_DaysStockHeld = (CLASSID_GROSSMARGINS * &H10000) + 9
Public Const FID_GROSSMARGINS_END_OF_STATIC = FID_GROSSMARGINS_DaysStockHeld

'Item Messages
'Public Const FID_ITEMMESSAGES_PromptKey = (CLASSID_ITEMMESSAGES * &H10000) + 1
'Public Const FID_ITEMMESSAGES_PromptDescription = (CLASSID_ITEMMESSAGES * &H10000) + 2
'Public Const FID_ITEMMESSAGES_AgePrompt = (CLASSID_ITEMMESSAGES * &H10000) + 3
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
'Public Const FID_ITEMMESSAGES_END_OF_STATIC = FID_ITEMMESSAGES_AgePrompt

'Card Info
Public Const FID_CARDINFO_TransactionDate = (CLASSID_CARDINFO * &H10000) + 1
Public Const FID_CARDINFO_TillID = (CLASSID_CARDINFO * &H10000) + 2
Public Const FID_CARDINFO_TransactionNumber = (CLASSID_CARDINFO * &H10000) + 3
Public Const FID_CARDINFO_SequenceNumber = (CLASSID_CARDINFO * &H10000) + 4
Public Const FID_CARDINFO_TenderType = (CLASSID_CARDINFO * &H10000) + 5
Public Const FID_CARDINFO_Track2 = (CLASSID_CARDINFO * &H10000) + 6
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CARDINFO_END_OF_STATIC = FID_CARDINFO_Track2

Public Const FID_OPMASTER_EventType = (CLASSID_OPMASTER * &H10000) + 1
Public Const FID_OPMASTER_EventKey = (CLASSID_OPMASTER * &H10000) + 2
Public Const FID_OPMASTER_EventNumber = (CLASSID_OPMASTER * &H10000) + 3
Public Const FID_OPMASTER_Priority = (CLASSID_OPMASTER * &H10000) + 4
Public Const FID_OPMASTER_EventKey2 = (CLASSID_OPMASTER * &H10000) + 5
Public Const FID_OPMASTER_DiscountPercent = (CLASSID_OPMASTER * &H10000) + 6
Public Const FID_OPMASTER_Deleted = (CLASSID_OPMASTER * &H10000) + 7
Public Const FID_OPMASTER_TimeOrDayRelated = (CLASSID_OPMASTER * &H10000) + 8
Public Const FID_OPMASTER_StartDate = (CLASSID_OPMASTER * &H10000) + 9
Public Const FID_OPMASTER_EndDate = (CLASSID_OPMASTER * &H10000) + 10
Public Const FID_OPMASTER_SpecialPrice = (CLASSID_OPMASTER * &H10000) + 11
Public Const FID_OPMASTER_BuyQuantity = (CLASSID_OPMASTER * &H10000) + 12
Public Const FID_OPMASTER_GetQuantity = (CLASSID_OPMASTER * &H10000) + 13
Public Const FID_OPMASTER_DiscountAmount = (CLASSID_OPMASTER * &H10000) + 14
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_OPMASTER_END_OF_STATIC = FID_OPMASTER_DiscountAmount


Public Const FID_OPHEADER_EventNumber = (CLASSID_OPHEADER * &H10000) + 1
Public Const FID_OPHEADER_Priority = (CLASSID_OPHEADER * &H10000) + 2
Public Const FID_OPHEADER_Description = (CLASSID_OPHEADER * &H10000) + 3
Public Const FID_OPHEADER_StartDate = (CLASSID_OPHEADER * &H10000) + 4
Public Const FID_OPHEADER_StartTime = (CLASSID_OPHEADER * &H10000) + 5
Public Const FID_OPHEADER_EndDate = (CLASSID_OPHEADER * &H10000) + 6
Public Const FID_OPHEADER_EndTime = (CLASSID_OPHEADER * &H10000) + 7
Public Const FID_OPHEADER_ActiveDays = (CLASSID_OPHEADER * &H10000) + 8
Public Const FID_OPHEADER_Deleted = (CLASSID_OPHEADER * &H10000) + 9
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_OPHEADER_END_OF_STATIC = FID_OPHEADER_Deleted


Public Const FID_OPDEALGROUP_EventNumber = (CLASSID_OPDEALGROUP * &H10000) + 1
Public Const FID_OPDEALGROUP_DealGroupNumber = (CLASSID_OPDEALGROUP * &H10000) + 2
Public Const FID_OPDEALGROUP_DealType = (CLASSID_OPDEALGROUP * &H10000) + 3
Public Const FID_OPDEALGROUP_ItemKey = (CLASSID_OPDEALGROUP * &H10000) + 4
Public Const FID_OPDEALGROUP_Deleted = (CLASSID_OPDEALGROUP * &H10000) + 5
Public Const FID_OPDEALGROUP_Quantity = (CLASSID_OPDEALGROUP * &H10000) + 6
Public Const FID_OPDEALGROUP_ErosionValue = (CLASSID_OPDEALGROUP * &H10000) + 7
Public Const FID_OPDEALGROUP_ErosionPercentage = (CLASSID_OPDEALGROUP * &H10000) + 8
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_OPDEALGROUP_END_OF_STATIC = FID_OPDEALGROUP_ErosionPercentage


Public Const FID_OPMIXMATCH_MixMatchGroupID = (CLASSID_OPMIXMATCH * &H10000) + 1
Public Const FID_OPMIXMATCH_PartCode = (CLASSID_OPMIXMATCH * &H10000) + 2
Public Const FID_OPMIXMATCH_Deleted = (CLASSID_OPMIXMATCH * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_OPMIXMATCH_END_OF_STATIC = FID_OPMIXMATCH_Deleted


Public Const FID_CASHIERCASHBAL_TotalsDate = (CLASSID_CASHIERCASHBAL * &H10000) + 1
Public Const FID_CASHIERCASHBAL_CashierNumber = (CLASSID_CASHIERCASHBAL * &H10000) + 2
Public Const FID_CASHIERCASHBAL_GrossSales = (CLASSID_CASHIERCASHBAL * &H10000) + 3
Public Const FID_CASHIERCASHBAL_DiscountAmount = (CLASSID_CASHIERCASHBAL * &H10000) + 4
Public Const FID_CASHIERCASHBAL_NoOfTranTypes = (CLASSID_CASHIERCASHBAL * &H10000) + 5
Public Const FID_CASHIERCASHBAL_TranTypeTotals = (CLASSID_CASHIERCASHBAL * &H10000) + 6
Public Const FID_CASHIERCASHBAL_TranDiscountTotals = (CLASSID_CASHIERCASHBAL * &H10000) + 7
Public Const FID_CASHIERCASHBAL_FloatAmount = (CLASSID_CASHIERCASHBAL * &H10000) + 8
Public Const FID_CASHIERCASHBAL_PickUpTotal = (CLASSID_CASHIERCASHBAL * &H10000) + 9
Public Const FID_CASHIERCASHBAL_TenderTypeOccurence = (CLASSID_CASHIERCASHBAL * &H10000) + 10
Public Const FID_CASHIERCASHBAL_TenderTypeAmount = (CLASSID_CASHIERCASHBAL * &H10000) + 11
Public Const FID_CASHIERCASHBAL_TenderTypePickUps = (CLASSID_CASHIERCASHBAL * &H10000) + 12
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_CASHIERCASHBAL_END_OF_STATIC = FID_CASHIERCASHBAL_TenderTypePickUps

' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_PRICECUSINFO_TranDate = (CLASSID_PRICECUSINFO * &H10000) + 1
Public Const FID_PRICECUSINFO_TillID = (CLASSID_PRICECUSINFO * &H10000) + 2
Public Const FID_PRICECUSINFO_TransactionNo = (CLASSID_PRICECUSINFO * &H10000) + 3
Public Const FID_PRICECUSINFO_Title = (CLASSID_PRICECUSINFO * &H10000) + 4
Public Const FID_PRICECUSINFO_Initial = (CLASSID_PRICECUSINFO * &H10000) + 5
Public Const FID_PRICECUSINFO_Name = (CLASSID_PRICECUSINFO * &H10000) + 6
Public Const FID_PRICECUSINFO_Address1 = (CLASSID_PRICECUSINFO * &H10000) + 7
Public Const FID_PRICECUSINFO_Address2 = (CLASSID_PRICECUSINFO * &H10000) + 8
Public Const FID_PRICECUSINFO_Address3 = (CLASSID_PRICECUSINFO * &H10000) + 9
Public Const FID_PRICECUSINFO_Postcode = (CLASSID_PRICECUSINFO * &H10000) + 10
Public Const FID_PRICECUSINFO_Phone = (CLASSID_PRICECUSINFO * &H10000) + 11
Public Const FID_PRICECUSINFO_Fax = (CLASSID_PRICECUSINFO * &H10000) + 12
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PRICECUSINFO_END_OF_STATIC = FID_PRICECUSINFO_Fax


Public Const FID_POSEVENTLINE_TranDate = (CLASSID_POSEVENTLINE * &H10000) + 1
Public Const FID_POSEVENTLINE_TillID = (CLASSID_POSEVENTLINE * &H10000) + 2
Public Const FID_POSEVENTLINE_TransactionNo = (CLASSID_POSEVENTLINE * &H10000) + 3
Public Const FID_POSEVENTLINE_TransactionLineNo = (CLASSID_POSEVENTLINE * &H10000) + 4
Public Const FID_POSEVENTLINE_EventSeqNo = (CLASSID_POSEVENTLINE * &H10000) + 5
Public Const FID_POSEVENTLINE_EventType = (CLASSID_POSEVENTLINE * &H10000) + 6
Public Const FID_POSEVENTLINE_DiscountAmount = (CLASSID_POSEVENTLINE * &H10000) + 7
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSEVENTLINE_END_OF_STATIC = FID_POSEVENTLINE_DiscountAmount
    
' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_PRICELINEINFO_TranDate = (CLASSID_PRICELINEINFO * &H10000) + 1
Public Const FID_PRICELINEINFO_TillID = (CLASSID_PRICELINEINFO * &H10000) + 2
Public Const FID_PRICELINEINFO_TransactionNo = (CLASSID_PRICELINEINFO * &H10000) + 3
Public Const FID_PRICELINEINFO_SequenceNo = (CLASSID_PRICELINEINFO * &H10000) + 4
Public Const FID_PRICELINEINFO_CompetitorName = (CLASSID_PRICELINEINFO * &H10000) + 5
Public Const FID_PRICELINEINFO_CompetitorAddress1 = (CLASSID_PRICELINEINFO * &H10000) + 6
Public Const FID_PRICELINEINFO_CompetitorAddress2 = (CLASSID_PRICELINEINFO * &H10000) + 7
Public Const FID_PRICELINEINFO_CompetitorAddress3 = (CLASSID_PRICELINEINFO * &H10000) + 8
Public Const FID_PRICELINEINFO_CompetitorPostcode = (CLASSID_PRICELINEINFO * &H10000) + 9
Public Const FID_PRICELINEINFO_CompetitorPhone = (CLASSID_PRICELINEINFO * &H10000) + 10
Public Const FID_PRICELINEINFO_CompetitorFax = (CLASSID_PRICELINEINFO * &H10000) + 11
Public Const FID_PRICELINEINFO_EnteredPrice = (CLASSID_PRICELINEINFO * &H10000) + 12
Public Const FID_PRICELINEINFO_EnteredDelivery = (CLASSID_PRICELINEINFO * &H10000) + 13
Public Const FID_PRICELINEINFO_IncludeVAT = (CLASSID_PRICELINEINFO * &H10000) + 14
Public Const FID_PRICELINEINFO_ConvertedPrice = (CLASSID_PRICELINEINFO * &H10000) + 15
Public Const FID_PRICELINEINFO_ConvertedDelivery = (CLASSID_PRICELINEINFO * &H10000) + 16
Public Const FID_PRICELINEINFO_Match = (CLASSID_PRICELINEINFO * &H10000) + 17
Public Const FID_PRICELINEINFO_Previous = (CLASSID_PRICELINEINFO * &H10000) + 18
Public Const FID_PRICELINEINFO_OrigStoreNumber = (CLASSID_PRICELINEINFO * &H10000) + 19
Public Const FID_PRICELINEINFO_OrigTillNumber = (CLASSID_PRICELINEINFO * &H10000) + 20
Public Const FID_PRICELINEINFO_OrigTransactionNumber = (CLASSID_PRICELINEINFO * &H10000) + 21
Public Const FID_PRICELINEINFO_OrigTransDate = (CLASSID_PRICELINEINFO * &H10000) + 22
Public Const FID_PRICELINEINFO_OrigOWSellingPrice = (CLASSID_PRICELINEINFO * &H10000) + 23
Public Const FID_PRICELINEINFO_OrigPriceIncVAT = (CLASSID_PRICELINEINFO * &H10000) + 24
Public Const FID_PRICELINEINFO_ConvertedVATMethod = (CLASSID_PRICELINEINFO * &H10000) + 25
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_PRICELINEINFO_END_OF_STATIC = FID_PRICELINEINFO_ConvertedVATMethod

' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_HIERARCHYCATEGORY_Category = (CLASSID_HIERARCHYCATEGORY * &H10000) + 1
Public Const FID_HIERARCHYCATEGORY_Description = (CLASSID_HIERARCHYCATEGORY * &H10000) + 2
Public Const FID_HIERARCHYCATEGORY_Alpha = (CLASSID_HIERARCHYCATEGORY * &H10000) + 3
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_HIERARCHYCATEGORY_END_OF_STATIC = FID_HIERARCHYCATEGORY_Alpha
    
' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_HIERARCHYGROUP_Category = (CLASSID_HIERARCHYCATEGORY * &H10000) + 1
Public Const FID_HIERARCHYGROUP_Group = (CLASSID_HIERARCHYCATEGORY * &H10000) + 2
Public Const FID_HIERARCHYGROUP_Description = (CLASSID_HIERARCHYCATEGORY * &H10000) + 3
Public Const FID_HIERARCHYGROUP_Alpha = (CLASSID_HIERARCHYCATEGORY * &H10000) + 4
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_HIERARCHYGROUP_END_OF_STATIC = FID_HIERARCHYGROUP_Alpha
    
' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_HIERARCHYSUBGROUP_Category = (CLASSID_HIERARCHYCATEGORY * &H10000) + 1
Public Const FID_HIERARCHYSUBGROUP_Group = (CLASSID_HIERARCHYCATEGORY * &H10000) + 2
Public Const FID_HIERARCHYSUBGROUP_SubGroup = (CLASSID_HIERARCHYCATEGORY * &H10000) + 3
Public Const FID_HIERARCHYSUBGROUP_Description = (CLASSID_HIERARCHYCATEGORY * &H10000) + 4
Public Const FID_HIERARCHYSUBGROUP_Alpha = (CLASSID_HIERARCHYCATEGORY * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_HIERARCHYSUBGROUP_END_OF_STATIC = FID_HIERARCHYSUBGROUP_Alpha
    
' For each class, the Property Id #1 must be the key or row Id field
Public Const FID_HIERARCHYSTYLE_Category = (CLASSID_HIERARCHYCATEGORY * &H10000) + 1
Public Const FID_HIERARCHYSTYLE_Group = (CLASSID_HIERARCHYCATEGORY * &H10000) + 2
Public Const FID_HIERARCHYSTYLE_SubGroup = (CLASSID_HIERARCHYCATEGORY * &H10000) + 3
Public Const FID_HIERARCHYSTYLE_Style = (CLASSID_HIERARCHYCATEGORY * &H10000) + 4
Public Const FID_HIERARCHYSTYLE_Description = (CLASSID_HIERARCHYCATEGORY * &H10000) + 5
Public Const FID_HIERARCHYSTYLE_Alpha = (CLASSID_HIERARCHYCATEGORY * &H10000) + 6
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_HIERARCHYSTYLE_END_OF_STATIC = FID_HIERARCHYSTYLE_Alpha
    
    
Public Const FID_INVENTORYWICKES_PartCode = (CLASSID_INVENTORYWICKES * &H10000) + 1
Public Const FID_INVENTORYWICKES_HierCategory = (CLASSID_INVENTORYWICKES * &H10000) + 2
Public Const FID_INVENTORYWICKES_HierGroup = (CLASSID_INVENTORYWICKES * &H10000) + 3
Public Const FID_INVENTORYWICKES_HierSubGroup = (CLASSID_INVENTORYWICKES * &H10000) + 4
Public Const FID_INVENTORYWICKES_HierStyle = (CLASSID_INVENTORYWICKES * &H10000) + 5
Public Const FID_INVENTORYWICKES_OffensiveWeaponAge = (CLASSID_INVENTORYWICKES * &H10000) + 6
Public Const FID_INVENTORYWICKES_SolventAge = (CLASSID_INVENTORYWICKES * &H10000) + 7
Public Const FID_INVENTORYWICKES_QuarantineFlag = (CLASSID_INVENTORYWICKES * &H10000) + 8
Public Const FID_INVENTORYWICKES_PricingDiscrepency = (CLASSID_INVENTORYWICKES * &H10000) + 9
Public Const FID_INVENTORYWICKES_SaleTypeAttribute = (CLASSID_INVENTORYWICKES * &H10000) + 10
Public Const FID_INVENTORYWICKES_PalletedSku = (CLASSID_INVENTORYWICKES * &H10000) + 11
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_INVENTORYWICKES_END_OF_STATIC = FID_INVENTORYWICKES_PalletedSku
    
Public Const FID_POSMISSINGEAN_TransactionDate = (CLASSID_POSMISSINGEAN * &H10000) + 1
Public Const FID_POSMISSINGEAN_TillID = (CLASSID_POSMISSINGEAN * &H10000) + 2
Public Const FID_POSMISSINGEAN_TransactionNumber = (CLASSID_POSMISSINGEAN * &H10000) + 3
Public Const FID_POSMISSINGEAN_LineNumber = (CLASSID_POSMISSINGEAN * &H10000) + 4
Public Const FID_POSMISSINGEAN_LineSequenceNumber = (CLASSID_POSMISSINGEAN * &H10000) + 5
Public Const FID_POSMISSINGEAN_EANScannedAndNoF = (CLASSID_POSMISSINGEAN * &H10000) + 6
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_POSMISSINGEAN_END_OF_STATIC = FID_POSMISSINGEAN_EANScannedAndNoF


Public Const FID_REFUNDCODE_CodeType = (CLASSID_REFUNDCODE * &H10000) + 1
Public Const FID_REFUNDCODE_Code = (CLASSID_REFUNDCODE * &H10000) + 2
Public Const FID_REFUNDCODE_Description = (CLASSID_REFUNDCODE * &H10000) + 3
Public Const FID_REFUNDCODE_StockAttribute = (CLASSID_REFUNDCODE * &H10000) + 4
Public Const FID_REFUNDCODE_PrintReturnLabel = (CLASSID_REFUNDCODE * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_REFUNDCODE_END_OF_STATIC = FID_REFUNDCODE_PrintReturnLabel


Public Const FID_TENDEROVERRIDE_CodeType = (CLASSID_TENDEROVERRIDE * &H10000) + 1
Public Const FID_TENDEROVERRIDE_Code = (CLASSID_TENDEROVERRIDE * &H10000) + 2
Public Const FID_TENDEROVERRIDE_Description = (CLASSID_TENDEROVERRIDE * &H10000) + 3
Public Const FID_TENDEROVERRIDE_StockAttribute = (CLASSID_TENDEROVERRIDE * &H10000) + 4
Public Const FID_TENDEROVERRIDE_PrintReturnLabel = (CLASSID_TENDEROVERRIDE * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_TENDEROVERRIDE_END_OF_STATIC = FID_TENDEROVERRIDE_PrintReturnLabel


Public Const FID_ZREADS_TillID = (CLASSID_ZREADS * &H10000) + 1
Public Const FID_ZREADS_TypeCode = (CLASSID_ZREADS * &H10000) + 2
Public Const FID_ZREADS_SubType = (CLASSID_ZREADS * &H10000) + 3
Public Const FID_ZREADS_Count = (CLASSID_ZREADS * &H10000) + 4
Public Const FID_ZREADS_Amount = (CLASSID_ZREADS * &H10000) + 5
'---------- Insert new Field IDs immediately before here,
'---------- incrementing the number and making FID_{CLASSNAME}_END_OF_STATIC the same
Public Const FID_ZREADS_END_OF_STATIC = FID_ZREADS_Amount

' The enBoInterfaceType lists the types of interface supported by IBo.Interface()
Public Enum enStockTakeState
    STOCKTAKE_UNSTARTED = 0
    STOCKTAKE_PRECOUNT = 1
    STOCKTAKE_COUNTING = 2
    STOCKTAKE_POSTCOUNT = 3
    STOCKTAKE_COMPLETE = 4
End Enum

Public Const AGG_MAX As Long = 1
Public Const AGG_MIN As Long = 2
Public Const AGG_COUNT As Long = 3
Public Const AGG_AVG As Long = 4
Public Const AGG_SUM As Long = 5
Public Const AGG_DIST As Long = 6

Public Const PRM_EDI_DATE_FORMAT     As Long = 126

Public Const PRM_TXOUT_FOLDER        As Long = 150
Public Const PRM_TXOUT_BACKUP_FOLDER As Long = 151
Public Const PRM_TXIN_FOLDER         As Long = 152
Public Const PRM_TXIN_BACKUP_FOLDER  As Long = 153
Public Const PRM_TXERR_FOLDER        As Long = 154
Public Const PRM_COMMS_FOLDER        As Long = 155


