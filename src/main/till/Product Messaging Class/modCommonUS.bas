Attribute VB_Name = "modCommonUS"
Option Explicit
'<CAMH>****************************************************************************************
'* Module : modCommonUS
'* Date   : 01/10/02
'* Author : mauricem
' $Archive: /Projects/OasysV2/VB/Common/modCommonUS.bas $
'**********************************************************************************************
'* Summary: Provides common routines used by the User interfaces
'**********************************************************************************************
'* $Revision: 1 $ $ Date: 1/22/03 5:21p $ $Author: Damians $
'* Versions:
'* 01/10/02    mauricem
'*             Header added.
'* 14/05/03    KeithB Functions added - GetFullName GetInitials & GetTillReceiptName
'* 19/04/04    KeithB Function added  - ConvertEDIToDate (copied from HO system)
'*
'</CAMH>***************************************************************************************

Const MODULE_NAME As String = "modCommonUS"

Public Const SYSTEM_NAME As String = "Oasys Tile"

Global goSession    As Session
Global goRoot       As OasysRoot
Global goDatabase   As IBoDatabase

Public Err As VbUtils_Wickes.ErrorObject

Public Const RGB_BLACK As Long = 0
Public Const RGB_BLUE As Long = 16711680
Public Const RGB_LTRED As Long = 12632319
Public Const RGB_RED As Long = 255
Public Const RGB_LTGREY As Long = 14737632
Public Const RGB_GREY As Long = 12632256
Public Const RGB_WHITE As Long = 16777215
Public Const RGB_YELLOW As Long = 65535
Public Const RGB_LTYELLOW As Long = 12648447

Public Const PANEL_VERNO As Integer = 2
Public Const PANEL_INFO As Integer = 3
Public Const PANEL_WSID As Integer = 4
Public Const PANEL_DATE As Integer = 5

Public Const PRM_BACKCOLOUR As Integer = 130
Public Const PRM_EDITCOLOUR As Integer = 123
Public Const PRM_MSGBOX_WARN_COLOUR As Integer = 127
Public Const PRM_MSGBOX_PROMPT_COLOUR As Integer = 128
Public Const PRM_QUERY_BACKCOLOUR As Integer = 135
Public Const PRM_QUERY_GRIDEVENCOLOUR As Integer = 136
Public Const PRM_QUERY_GRIDODDCOLOUR As Integer = 137
Public Const PRM_QUERY_BORDERCOLOUR As Integer = 141
Public Const PRM_QUERY_ROWHEIGHT As Integer = 142
Public Const PRM_LOGOFILENAME As Integer = 131
Public Const PRM_ENQUIRY_EXE As Long = 139 'added 5/5/03 to get path to SKU enquiry

Public Const PRM_QTY_DEC_PLACES As Long = 101
Public Const PRM_VALUE_DEC_PLACES As Long = 102
Public Const PRM_DATE_FORMAT As Long = 125

Public Const PARTCODE_LEN As Long = 6
Public Const PARTCODE_PAD As String = "000000"
Public Const DOCNO_LEN As Long = 6
Public Const DOCNO_PAD As String = "000000"
Public Const BAD_DATE As String = "N/A"

Public Const PRM_SPCCOM As Integer = 655
Public mstrSPCCOM_Location As String

Public mstrDateFmt As String
Public RGBEdit_Colour As Long
Public RGBQuery_BackColour As Long
Public RGBQuery_GridColour As Long
Public RGBMsgBox_WarnColour As Long
Public RGBMSGBox_PromptColour As Long

Public Const DEST_REPORT_CODE_PREVIEW As String = "S"
Public Const DEST_REPORT_CODE_PRINTER As String = "P"
Public Const DEST_REPORT_CODE_FILE As String = "F"

Public Const ORDER_STATUS As String = "ST"
Public Const GROUP_BY As String = "GR"
Public Const DEST_CODE As String = "DC"
Public Const DISP_LINES As String = "DL"

Public Const KEY_DROPDOWN As String = "%{Down}"

Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Public Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long

Const SW_RESTORE = 9

Public mstrEDIDateFmt       As String

Private mcolTillReceiptName As New Collection
Private mcolFullName        As New Collection
Private mcolInitials        As New Collection
Private mcolFullNameInit    As New Collection

Public Enum enActionType
    enatPurchaseOrder = 0
    enatDeliveryNote = 1
    enatStockWriteOff = 2
    enatAdjustment = 3
    enatReturn = 4
    enatISTIn = 5
End Enum

Public Sub Display_NoMatches(ByRef MatchesLabel As Label)

    MatchesLabel.BackColor = RGB_RED
    MatchesLabel.ForeColor = RGB_WHITE
    MatchesLabel.FontBold = True
    MatchesLabel.Caption = "NO MATCHES"

End Sub


Public Function GetRoot() As IOasysRoot
    
Dim oStart     As OasysStartCom_Wickes.OasysStart
Dim oOasysRoot As OasysRoot
    
    Call CreateErrorObject(SYSTEM_NAME & "{" & App.Title & "}", VBA.Err, Err)
    
    Call DebugMsg(MODULE_NAME, "GetRoot", endlDebug, "Starting Session - " & App.EXEName)
    Set oStart = New OasysStart
    Set oOasysRoot = oStart.GetRoot(Command)
    Set oStart = Nothing
    Set GetRoot = oOasysRoot
    
    If goSession Is Nothing Then
        Call DebugMsg(MODULE_NAME, "GetRoot", endlDebug, "Creating Session - " & App.EXEName)
        Set goSession = GetRoot.CreateSession(Command)
    End If
    Call DebugMsg(MODULE_NAME, "GetRoot", endlDebug, "Open Database - " & App.EXEName)
    Set goDatabase = goSession.Database
    Call DebugMsg(MODULE_NAME, "GetRoot", endlDebug, "Session Started - " & App.EXEName)

End Function

Public Sub Process_Combo_VirtualRequest(ofpCombo As Object, oListObj As Object, ActionRequested As Integer, RowFirst As Long, RowCount As Long, pos As Long, ret As Long)

' FpCombo Box VirtualRequest values
Const CMB_VIRTACTREQ_ISHOME = 0
Const CMB_VIRTACTREQ_ISEND = 1
Const CMB_VIRTACTREQ_DOWN = 2
Const CMB_VIRTACTREQ_UP = 3
Const CMB_VIRTACTREQ_HOME = 4
Const CMB_VIRTACTREQ_END = 5
Const CMB_VIRTACTREQ_REFRESH = 6
Const CMB_VIRTACTREQ_ROWSREMOVED = 7
Const CMB_VIRTACTREQ_POS = 8

Dim lRowStart   As Long
Dim lRowsRead   As Long
Dim lInc        As Long
Dim lRow        As Long
Dim blnStatus   As Boolean
Dim vData       As Variant
Dim lPos        As Long
Dim lItemData   As Long
    
    lRowStart = RowFirst
    lRowsRead = 0
    lInc = 1
    lItemData = 1
    blnStatus = False

    Select Case ActionRequested
        Case CMB_VIRTACTREQ_ISHOME
            ofpCombo.Row = 0
            lItemData = ofpCombo.ItemData
            Call oListObj.SeekPosition(lItemData)
            ret = oListObj.StartOfList
            Exit Sub

        Case CMB_VIRTACTREQ_ISEND
            ofpCombo.Row = ofpCombo.ListCount - 1
            lItemData = ofpCombo.ItemData
            Call oListObj.SeekPosition(lItemData)
            ret = oListObj.EndOfList
            Exit Sub
 
        Case CMB_VIRTACTREQ_HOME
            Call oListObj.MoveFirst
            lItemData = 1
            blnStatus = True
            
        Case CMB_VIRTACTREQ_END
            Call oListObj.MoveLast
            lItemData = oListObj.Count
            blnStatus = True
            lInc = -1
            lRowStart = RowFirst + RowCount - 1
    
        Case CMB_VIRTACTREQ_DOWN
            ofpCombo.Row = RowFirst - 1
            lItemData = ofpCombo.ItemData + 1
            Call oListObj.SeekPosition(lItemData)
            blnStatus = Not oListObj.EndOfList
    
        Case CMB_VIRTACTREQ_UP
            ofpCombo.Row = RowFirst + RowCount
            lRowStart = RowFirst + RowCount - 1
            lItemData = ofpCombo.ItemData - 1
            Call oListObj.SeekPosition(lItemData)
            blnStatus = Not oListObj.StartOfList
            lInc = -1
            lRowStart = RowFirst + RowCount - 1
    
        Case CMB_VIRTACTREQ_REFRESH
            'Bookmark = Bookmarksave-CHECK THIS
            blnStatus = True

        Case CMB_VIRTACTREQ_ROWSREMOVED
            ofpCombo.Row = ofpCombo.TopIndex
            
        Case CMB_VIRTACTREQ_POS
            'DBVirt_GetBookmarkPos(Bookmark, pos) - CHECK THIS
            blnStatus = True
            
    End Select

    If blnStatus = True Then
        lRow = lRowStart
        For lPos = 0 To RowCount - 1
            
            'Move to previous or next value to add to combo
            If lInc = 1 Then
                If lPos > 0 Then Call oListObj.MoveNext
            Else
                If lPos > 0 Then Call oListObj.MovePrev
            End If
            
            If blnStatus = True Then
               ofpCombo.Row = lRow
                If lRow >= ofpCombo.ListCount Then
                    ofpCombo.AddItem lItemData & "|" & oListObj.EntryString
                    ofpCombo.ItemData = lItemData
                Else
                    ofpCombo.List = lItemData & "|" & oListObj.EntryString
                    ofpCombo.ItemData = lItemData
                End If

                lRowsRead = lRowsRead + 1
                'Moved to previous or next value so check for BOF or EOF
                If lInc = 1 Then
                    blnStatus = Not oListObj.EndOfList
                Else
                    blnStatus = Not oListObj.StartOfList
                End If
            End If
            
            If blnStatus = False Then Exit For 'if BOF or EOF then exit
            lRow = lRow + lInc
            lItemData = lItemData + lInc
        Next lPos
    End If
    
    ret = lRowsRead

End Sub

Public Function DisplayDate(ByVal dtInDate As Date, ByVal blnShowTime As Boolean) As String

Dim sTimeFmt As String
    
    If LenB(mstrDateFmt) = 0 Then mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    sTimeFmt = mstrDateFmt & " HH:NN:SS"
    
    'check if date is invalid and so return N/A
    If Format$(dtInDate, "DD/MM/YY HH:NN:SS") = "30/12/99 00:00:00" Then
        DisplayDate = BAD_DATE
    Else
        'return date in specified format, with time if required
        If blnShowTime = True Then
            DisplayDate = Format$(dtInDate, sTimeFmt)
        Else
            DisplayDate = Format$(dtInDate, mstrDateFmt)
        End If
    End If
    
End Function

Public Function ConvertEDIToDate(strEDIDate As String) As Date

Dim lDayPos    As Integer
Dim lDayLen    As Integer
Dim lMonthPos  As Integer
Dim lMonthlen  As Integer
Dim lYearPos   As Integer
Dim lYearLen   As Integer
Dim sTempDate  As String
Dim dteOutDate As Date
              
    On Error GoTo BAD_DATE
    
    If LenB(mstrEDIDateFmt) = 0 Then mstrEDIDateFmt = UCase$(goSession.GetParameter(PRM_EDI_DATE_FORMAT))
    If (LenB(strEDIDate) = 0) Or (strEDIDate = "--/--/--") Then Exit Function
    lDayLen = 1
    lDayPos = InStr(mstrEDIDateFmt, "D")
    If InStr(mstrEDIDateFmt, "DD") > 0 Then lDayLen = 2
    
    lMonthlen = 1
    lMonthPos = InStr(mstrEDIDateFmt, "M")
    If InStr(mstrEDIDateFmt, "MM") > 0 Then lMonthlen = 2
    
    lYearLen = 2
    lYearPos = InStr(mstrEDIDateFmt, "YY")
    If InStr(mstrEDIDateFmt, "YYYY") > 0 Then lYearLen = 4
    
    lDayPos = CInt(Mid$(strEDIDate, lDayPos, lDayLen))
    lMonthPos = CInt(Mid$(strEDIDate, lMonthPos, lMonthlen))
    lYearPos = CInt(Mid$(strEDIDate, lYearPos, lYearLen))
    
    'check if date is invalid and so return N/A
    sTempDate = lDayPos & "/" & MonthStr(lMonthPos) & "/" & lYearPos
    If IsDate(sTempDate) Then ConvertEDIToDate = CDate(sTempDate)
    
    Exit Function
    
BAD_DATE:

    'if error then set date to default starting date (01/01/1899)
    ConvertEDIToDate = dteOutDate
    Exit Function


End Function

Public Function GetDate(ByVal strInDate As String) As Date

Dim lDayPos    As Integer
Dim lDayLen    As Integer
Dim lMonthPos  As Integer
Dim lMonthlen  As Integer
Dim lYearPos   As Integer
Dim lYearLen   As Integer
Dim sTempDate  As String
Dim dteOutDate As Date
    
              
    On Error GoTo BAD_DATE
    
    If LenB(mstrDateFmt) = 0 Then mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    If (LenB(strInDate) = 0) Or (strInDate = BAD_DATE) Then Exit Function
    lDayLen = 1
    lDayPos = InStr(mstrDateFmt, "D")
    If InStr(mstrDateFmt, "DD") > 0 Then lDayLen = 2
    
    lMonthlen = 1
    lMonthPos = InStr(mstrDateFmt, "M")
    If InStr(mstrDateFmt, "MM") > 0 Then lMonthlen = 2
    
    lYearLen = 2
    lYearPos = InStr(mstrDateFmt, "YY")
    If InStr(mstrDateFmt, "YYYY") > 0 Then lYearLen = 4
    
    lDayPos = CInt(Mid$(strInDate, lDayPos, lDayLen))
    lMonthPos = CInt(Mid$(strInDate, lMonthPos, lMonthlen))
    lYearPos = CInt(Mid$(strInDate, lYearPos, lYearLen))
    
    'check if date is invalid and so return N/A
    sTempDate = lDayPos & "/" & MonthStr(lMonthPos) & "/" & lYearPos
    If IsDate(sTempDate) Then GetDate = CDate(sTempDate)
    
    Exit Function
    
BAD_DATE:

    'if error then set date to default starting date (01/01/1899)
    GetDate = dteOutDate
    Exit Function
    
End Function
Public Function GetTime(ByVal strInTime As String, blnContainsDate As Boolean) As String

    On Error GoTo BAD_DATE
    
    If LenB(mstrDateFmt) = 0 Then mstrDateFmt = UCase$(goSession.GetParameter(PRM_DATE_FORMAT))
    If (LenB(strInTime) = 0) Or (strInTime = BAD_DATE) Then Exit Function
    
    If blnContainsDate = True Then strInTime = Mid$(strInTime, Len(mstrDateFmt) + 1)
    
    While InStr(strInTime, ":") > 0
        strInTime = Left$(strInTime, InStr(strInTime, ":") - 1) & Mid$(strInTime, InStr(strInTime, ":") + 1)
    Wend
    
    GetTime = Trim$(strInTime)
    
    Exit Function
    
BAD_DATE:

    'if error then set date to default starting date (01/01/1899)
    GetTime = "000000"
    Exit Function
    
End Function
Public Function MonthStr(ByVal lMonthNo As Long) As String

    Select Case (lMonthNo)
        Case (1): MonthStr = "Jan"
        Case (2): MonthStr = "Feb"
        Case (3): MonthStr = "Mar"
        Case (4): MonthStr = "Apr"
        Case (5): MonthStr = "May"
        Case (6): MonthStr = "Jun"
        Case (7): MonthStr = "Jul"
        Case (8): MonthStr = "Aug"
        Case (9): MonthStr = "Sept"
        Case (10): MonthStr = "Oct"
        Case (11): MonthStr = "Nov"
        Case (12): MonthStr = "Dec"
    End Select
    
End Function
Public Function ColToText(ByVal lColNo As Long) As String

Dim sOutStr   As String
Dim lSubValue As Long

    While lColNo > 0 'while still values to convert
        lSubValue = (lColNo - 1) Mod 26 'get lowest part of numbers not converted
        sOutStr = Chr$(lSubValue + 65) + sOutStr 'prefix onto return string
        lColNo = (lColNo - 1) \ 26 'Remove converted number from Col No
    Wend

    ColToText = sOutStr 'pass out value

End Function 'ColToText

Public Function CentreForm(ByRef oForm As Form)

    If oForm.WindowState <> vbMaximized Then
        oForm.Left = (Screen.Width - oForm.Width) / 2
        oForm.Top = (Screen.Height - oForm.Height) / 2
    End If

End Function

Public Sub InitialiseStatusBar(ByRef sbInfo As StatusBar)
    
    sbInfo.Panels(PANEL_VERNO).Text = "Ver. " & App.Major & "." & App.Minor & "." & App.Revision
    sbInfo.Panels(PANEL_VERNO).ToolTipText = "Program Version Number"
    sbInfo.Panels(PANEL_WSID).Text = goSession.CurrentEnterprise.IEnterprise_WorkstationID & " "
    sbInfo.Panels(PANEL_WSID).ToolTipText = "Current Work-Station Number"
    sbInfo.Panels(PANEL_DATE).Text = Format$(Date, "DD-MMM-YY")

End Sub

Public Function GetParamDate(strYYYYMMDD As String) As Date

Dim strTempDate As String
    
    strTempDate = Right$(strYYYYMMDD, 2) & "/" & MonthStr(Mid$(strYYYYMMDD, 5, 2)) & "/" & Left$(strYYYYMMDD, 4)
    GetParamDate = CDate(strTempDate)

End Function

Public Function GetFullName(ByVal strEmployeeId As String) As String
    
    If mcolFullName.Count = 0 Then
        Call CreateFullNameCollection
    End If
    
    On Error Resume Next
    'retrieve the FullName for the Employee ID
    GetFullName = mcolFullName(strEmployeeId)
        
End Function
Public Function GetFullNameByInit(ByVal strInitials As String) As String
    
    If mcolFullNameInit.Count = 0 Then
        Call CreateFullNameInitCollection
    End If
    
    On Error Resume Next
    'retrieve the FullName for the Employee ID
    GetFullNameByInit = mcolFullNameInit(strInitials)
        
End Function

Public Function GetTillReceiptName(ByVal strEmployeeId As String) As String
    
    If mcolTillReceiptName.Count = 0 Then
        Call CreateTillReceiptNameCollection
    End If
    
    On Error Resume Next
    'retrieve the TillReceiptName for the Employee ID
    GetTillReceiptName = mcolTillReceiptName(strEmployeeId)
        
End Function

Public Function GetInitials(ByVal strEmployeeId As String) As String
    
    If mcolInitials.Count = 0 Then
        Call CreateInitialsCollection
    End If
    
    On Error Resume Next
    'retrieve the Initials for the Employee ID
    GetInitials = mcolInitials(strEmployeeId)
        
End Function

Private Sub CreateFullNameCollection()

Dim oUser    As cUser
Dim colList  As Collection
        
    Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oUser.AddLoadField(FID_USER_EmployeeID)
    Call oUser.AddLoadField(FID_USER_FullName)
    
    Set colList = oUser.LoadMatches
    Set mcolFullName = New Collection
    
    For Each oUser In colList
        ' add full name to the collection mcolFullName
        ' Note: key is Employee Id
        '       e.g.  "Trainee" with employee id of "099"
        Call mcolFullName.Add(oUser.FullName, oUser.EmployeeID)
    Next oUser
    
    On Error Resume Next
    
    Set colList = Nothing
    Set oUser = Nothing
    
End Sub

Private Sub CreateFullNameInitCollection()

Dim oUser    As cUser
Dim colList  As Collection
        
    Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oUser.AddLoadField(FID_USER_EmployeeID)
    Call oUser.AddLoadField(FID_USER_Initials)
    Call oUser.AddLoadField(FID_USER_FullName)
    
    Set colList = oUser.LoadMatches
    Set mcolFullNameInit = New Collection
    
    On Error Resume Next 'throw away duplicate Initials
    For Each oUser In colList
        ' add full name to the collection mcolFullName
        ' Note: key is Employee Id
        '       e.g.  "Trainee" with employee id of "099"
        Call mcolFullNameInit.Add(oUser.FullName, oUser.Initials)
    Next oUser
    
    On Error GoTo 0
    
    Set colList = Nothing
    Set oUser = Nothing
    
End Sub

Private Sub CreateTillReceiptNameCollection()

Dim oUser    As cUser
Dim colList  As Collection
        
    Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oUser.AddLoadField(FID_USER_EmployeeID)
    Call oUser.AddLoadField(FID_USER_TillReceiptName)
    
    Set colList = oUser.LoadMatches
    Set mcolTillReceiptName = New Collection
    
    For Each oUser In colList
        ' add till receipt name to the collection mcolTillReceiptName
        ' Note: key is Employee Id
        '       e.g.  "John Coles" with employee id of "001"
        Call mcolTillReceiptName.Add(oUser.TillReceiptName, oUser.EmployeeID)
    Next oUser
    
    On Error Resume Next
    
    Set colList = Nothing
    Set oUser = Nothing
    
End Sub

Private Sub CreateInitialsCollection()

Dim oUser    As cUser
Dim colList  As Collection
        
    Set oUser = goDatabase.CreateBusinessObject(CLASSID_USER)
    Call oUser.AddLoadField(FID_USER_EmployeeID)
    Call oUser.AddLoadField(FID_USER_Initials)
    
    Set colList = oUser.LoadMatches
    Set mcolInitials = New Collection
    
    For Each oUser In colList
        ' add Initials to the collection mcolInitials
        ' Note: key is Employee Id
        '       e.g.  "JF" with employee id of "001"
        Call mcolInitials.Add(oUser.Initials, oUser.EmployeeID)
    Next oUser
    
    On Error Resume Next
    
    Set colList = Nothing
    Set oUser = Nothing
    
End Sub


Public Function GetFooter() As String

Dim strFooter As String
    
    strFooter = "( V" & App.Major & "." & App.Minor & "." & App.Revision & " )" & _
                Space$(5) & "Printed - " & Format$(Now(), "DD/MM/YY HH:NN")
    ' /l is left align
    GetFooter = "/l" & strFooter & "/r Page /p of /pc"
    
End Function


Public Function GetLastValidUserID()

Dim oLookUp As cRetailOptions

    'Retrieve last Cashier ID, all cashiers above this are Training Codes
    Set oLookUp = goDatabase.CreateBusinessObject(CLASSID_RETAILEROPTIONS)
    Call oLookUp.AddLoadField(FID_RETAILEROPTIONS_MaxValidCashierNumber)
    Call oLookUp.LoadMatches
    GetLastValidUserID = oLookUp.MaxValidCashierNumber
    Set oLookUp = Nothing

End Function

