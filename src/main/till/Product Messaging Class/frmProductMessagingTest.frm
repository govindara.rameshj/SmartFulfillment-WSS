VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProductMsgingTest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Product Messaging Test"
   ClientHeight    =   4545
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6570
   Icon            =   "frmProductMessagingTest.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4545
   ScaleWidth      =   6570
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtTransactionNo 
      Height          =   375
      Left            =   3120
      TabIndex        =   13
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CommandButton cmdGetStatus 
      Caption         =   "&Test"
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   3600
      Width           =   1095
   End
   Begin MSComctlLib.StatusBar sbStatus 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   4170
      Width           =   6570
      _ExtentX        =   11589
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1764
            MinWidth        =   1764
            Text            =   "Ver"
            TextSave        =   "Ver"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4233
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            Key             =   "WID"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   1773
            MinWidth        =   1764
            Text            =   "01-Sept-02"
            TextSave        =   "01-Sept-02"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            Object.Width           =   1588
            MinWidth        =   1587
            TextSave        =   "16:03"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lbltitle 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Lowest Age Failed:"
      Height          =   255
      Index           =   1
      Left            =   600
      TabIndex        =   11
      Top             =   720
      Width           =   1635
   End
   Begin VB.Label lbltitle 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Highest Age Passed:"
      Height          =   285
      Index           =   0
      Left            =   600
      TabIndex        =   10
      Top             =   360
      Width           =   1635
   End
   Begin VB.Label lblLowestAgeFailed 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "LowestAgeFailed"
      Height          =   285
      Left            =   2340
      TabIndex        =   9
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label lblHighestAgePassed 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "HighestAgePassed"
      Height          =   285
      Left            =   2340
      TabIndex        =   8
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label lblSkuData 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2340
      TabIndex        =   7
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Label lblStatusData 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2340
      TabIndex        =   5
      Top             =   1680
      Width           =   3435
   End
   Begin VB.Label lblMessageData 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Height          =   1065
      Left            =   2340
      TabIndex        =   4
      Top             =   2040
      Width           =   3435
   End
   Begin VB.Label lblSkun 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Item SKU:"
      Height          =   285
      Left            =   1440
      TabIndex        =   1
      Top             =   1320
      Width           =   795
   End
   Begin VB.Label lblStatus 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Item Status:"
      Height          =   285
      Left            =   1320
      TabIndex        =   2
      Top             =   1680
      Width           =   915
   End
   Begin VB.Label lblMessage 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "Item Message:"
      Height          =   285
      Left            =   1020
      TabIndex        =   3
      Top             =   2040
      Width           =   1215
   End
   Begin VB.Label lblTransactionNo 
      BackStyle       =   0  'Transparent
      Caption         =   "Transaction Number:"
      Height          =   285
      Left            =   1560
      TabIndex        =   12
      Top             =   3660
      Width           =   1515
   End
End
Attribute VB_Name = "frmProductMsgingTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdGetStatus_Click()
Dim cItem           As cInventory
Dim cPosItem        As cPOSLine
Dim ItemsCollection As Collection
Dim PosCollection   As Collection
Dim cproductmsg     As clsProductMessaging
Dim i               As Long
Dim lngLowestAgeFailed    As Long
Dim lngHighestAgePassed   As Long
Dim lngResponse     As Long
    
    Set cPosItem = goDatabase.CreateBusinessObject(CLASSID_POSLINE)
    Call cPosItem.AddLoadFilter(CMP_EQUAL, FID_POSLINE_TransactionNumber, txtTransactionNo.Text)
    Set PosCollection = cPosItem.LoadMatches
    
    lngLowestAgeFailed = 0
    lngHighestAgePassed = 0
    
    For Each cPosItem In PosCollection
        
        lblSkuData.Caption = ""
        lblStatusData.Caption = ""
        lblMessageData.Caption = ""
        lblHighestAgePassed.Caption = CStr(lngHighestAgePassed)
        lblLowestAgeFailed.Caption = CStr(lngLowestAgeFailed)
        
        Set cItem = goDatabase.CreateBusinessObject(CLASSID_INVENTORY)
        
        Call cItem.AddLoadFilter(CMP_EQUAL, FID_INVENTORY_PartCode, cPosItem.PartCode)
        Set ItemsCollection = cItem.LoadMatches
        Set cItem = ItemsCollection(1)
        
        Set cproductmsg = New clsProductMessaging
        If ItemsCollection.Count = 1 Then
            
            If lngHighestAgePassed > 0 Then
                cproductmsg.HighestAgePassed = lngHighestAgePassed
            End If
            If lngLowestAgeFailed > 0 Then
                cproductmsg.LowestAgeFailed = lngLowestAgeFailed
            End If
           
            Set cproductmsg.Product = cItem
            
            lblSkuData.Caption = cItem.PartCode
        
            Set cproductmsg = Nothing
            
        End If
        
        Set cItem = Nothing
        Set ItemsCollection = Nothing
        
    Next cPosItem

End Sub

Private Sub Form_Load()
    
    GetRoot
    Call InitialiseStatusBar(sbStatus)
    
    Me.BackColor = goSession.GetParameter(PRM_BACKCOLOUR)
    
End Sub
