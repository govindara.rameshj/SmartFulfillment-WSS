DROP LOGIN Fitnesse
Go
CREATE LOGIN Fitnesse WITH PASSWORD = 'F1tn3ss3';
Go
Use Oasys
Drop User Fitnesse
Go
CREATE USER Fitnesse FOR LOGIN Fitnesse With DEFAULT_SCHEMA=Oasys;
Go
Use Oasys
Exec sp_addrolemember N'db_datareader', N'Fitnesse'
Exec sp_addrolemember N'db_datawriter', N'Fitnesse'
Exec sp_addrolemember N'role_execproc', N'Fitnesse'
Exec sp_addrolemember N'role_legacy', N'Fitnesse'
Go