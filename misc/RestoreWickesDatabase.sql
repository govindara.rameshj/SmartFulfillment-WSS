declare @DefaultDataDirectory nvarchar(max)
declare @WixMaster            nvarchar(max)
declare @WixMasterOne         nvarchar(max)
declare @WixMasterLog         nvarchar(max)

set @DefaultDataDirectory = (select replace(physical_name, 'master.mdf', '') from master.sys.master_files where database_id = 1 and [file_id] = 1)
set @WixMaster            = @DefaultDataDirectory + 'Oasys.mdf'
set @WixMasterOne         = @DefaultDataDirectory + 'Oasys_1.mdf'
set @WixMasterLog         = @DefaultDataDirectory + 'Oasys_2.mdf'

restore database Oasys from disk = $(WickesBackup) with file = 1,
                       move 'WixMaster'     to @WixMaster,
                       move 'WixMaster1'    to @WixMasterOne,
                       move 'WixMaster_log' to @WixMasterLog,
                       replace