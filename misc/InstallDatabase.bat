sqlcmd -E -S oasys-fitnesse-dbserver -Q "drop database Oasys"

set backup="'%cd%\EmptyDatabaseWithPaidOutCreated.bak'"

sqlcmd -E -S oasys-fitnesse-dbserver -v WickesBackup = %backup% -i RestoreWickesDatabase.sql